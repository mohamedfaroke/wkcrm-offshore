<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppointmentToolTip.ascx.cs" Inherits="Shared_AppointmentToolTip" %>
<p>
    <asp:Label ID="title" runat="server" Font-Bold="True" Width="133px" Font-Size="Medium"></asp:Label>&nbsp;<asp:Image
        ID="iconImage" runat="server" ImageAlign="Right" /></p>
<p>
    <strong>Date/Time:</strong>
    <asp:Label ID="startDate" runat="server"></asp:Label></p>
<p><asp:Label ID="nameLabel" runat="server" Font-Bold="True"></asp:Label><asp:Label ID="nameField" runat="server"></asp:Label></p>
<p><asp:Label ID="accountLabel" runat="server" Font-Bold="True"></asp:Label><asp:Label ID="accountName" runat="server"></asp:Label></p>
<p><asp:Label ID="extraLabel1" runat="server" Font-Bold="True"></asp:Label><asp:Label ID="extraLabel1Detail" runat="server"></asp:Label></p>
<p><asp:Label ID="extraLabel2" runat="server" Font-Bold="True"></asp:Label><asp:Label ID="extraLabel2Detail" runat="server"></asp:Label>&nbsp;</p>
<p>&nbsp;</p>
