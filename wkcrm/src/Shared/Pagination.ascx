﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Pagination.ascx.cs" Inherits="WKCRM.UI.Pagination" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="ThemeButton.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<table cellspacing="0" cellpadding="0" border="0">
 <tr>
	<td>
	<img src="../Images/space.gif" width="15" height="1" alt=""/>
	</td>
	<td class="prl">
	<asp:ImageButton runat="server" id="_FirstPage" CausesValidation="False" CommandName="FirstPage" CssClass="prbf" ImageURL="../Images/space.gif" AlternateText="">

</asp:ImageButton>
	</td>
	<td class="prl">
	<asp:ImageButton runat="server" id="_PreviousPage" CausesValidation="False" CommandName="PreviousPage" CssClass="prbp" ImageURL="../Images/space.gif" AlternateText="">

</asp:ImageButton>
	</td>
	<td class="prl" style="padding-right: 6px;"><%# GetResourceValue("Txt:Page", "WKCRM") %>
	<b>
	<asp:Label runat="server" id="_CurrentPage">
</asp:Label>
	</b><%# GetResourceValue("Txt:Of", "WKCRM") %>
	<b>
	<asp:Label runat="server" id="_TotalPages">
</asp:Label>
	</b></td>
	<td class="prl">
	<asp:ImageButton runat="server" id="_NextPage" CausesValidation="False" CommandName="NextPage" CssClass="prbn" ImageURL="../Images/space.gif" AlternateText="">

</asp:ImageButton>
	</td>
	<td class="prl">
	<asp:ImageButton runat="server" id="_LastPage" CausesValidation="False" CommandName="LastPage" CssClass="prbl" ImageURL="../Images/space.gif" AlternateText="">

</asp:ImageButton>
	</td>
	<td class="prl"><%# GetResourceValue("Txt:PageSize", "WKCRM") %> <%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("_PageSizeButton")) %>
	<asp:TextBox runat="server" id="_PageSize" CssClass="Pagination_Input" MaxLength="3" Size="3" onkeyup="adjustPageSize(this, event.keyCode, 1, 999);">
</asp:TextBox><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("_PageSizeButton")) %></td>
<%= SystemUtils.GenerateIncrementDecrementButtons(true, FindControl("_PageSize"),"PaginationTextBox","","","") %>

	<td class="prl">
	<WKCRM:ThemeButton runat="server" id="_PageSizeButton" Button-CausesValidation="False" Button-CommandName="PageSize" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Go&quot;, &quot;WKCRM&quot;) %>">
</WKCRM:ThemeButton>
	</td>
	<td class="prl"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>  <asp:Label runat="server" id="_TotalItems">
</asp:Label></td>
 </tr>
</table>
