﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SignIn_Control.ascx.cs" Inherits="WKCRM.UI.SignIn_Control" %>
<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.SignIn_Control" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>

<WKCRM:SignInControl id="SignInControl" runat="server">
<table class="edit_label" cellspacing="5" cellpadding="5" border="0" align="center">
<head id="Head2">
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/bootstrap.min.css"/>
	</head>
	<tbody>
		<tr>
			<td colspan="2" class="edit_label">
				<br />
				<asp:Label id="LoginMsg" runat="server" />
			</td>
		</tr>
		<tr>
			<td colspan="2" height="5"></td>
		</tr>
		<tr>
			<td class="field_label"><asp:Label id="UsernameLbl" runat="server" Text='<%# GetResourceValue("Txt:UserName", "WKCRM")%>'/>&nbsp;</td>
			<td class="field_value">
				<asp:TextBox id="UserName" class="form-control" placeholder="Email" runat="server" type="email" Text="">
				</asp:TextBox>
               <%-- <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus="">--%>
				<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter your user name." ControlToValidate="UserName" Display="None"></asp:RequiredFieldValidator>
			</td>
		</tr>
		<tr>
			<td class="field_label"><asp:Label id="PasswordLbl" runat="server" Text='<%# GetResourceValue("Txt:Password", "WKCRM")%>'/>&nbsp;</td>
			<td class="field_value">
				<asp:TextBox id="Password" placeholder="password" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
				<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter your password." ControlToValidate="Password" Display="None"></asp:RequiredFieldValidator>
			</td>
		</tr>
	</tbody>
</table>
</WKCRM:SignInControl>