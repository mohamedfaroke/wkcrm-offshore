﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="ThemeButton.ascx.cs" Inherits="WKCRM.UI.ThemeButton" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<table cellpadding="0" cellspacing="0" border="0" onmouseover="this.style.cursor='pointer'; return true;" onclick="clickLinkButtonText(this, event);">
<head id="Head2">
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/bootstrap.min.css"/>
	</head>
 <tr>
	<td class="bTL"><img src="../Images/space.gif" height="6" width="6" alt=""/></td>
	<td class="bT"><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td class="bTR"><img src="../Images/space.gif" height="6" width="6" alt=""/></td>
 </tr>
 <tr>
	<td class="bL"><img src="../Images/space.gif" height="7" width="6" alt=""/></td>
	<td class="bC"><asp:LinkButton CommandName="Redirect" runat="server" id="_Button" CssClass="btn btn-lg btn-success btn-block">

</asp:LinkButton></td>
	<td class="bR"><img src="../Images/space.gif" height="7" width="6" alt=""/></td>
 </tr>
 <tr>
	<td class="bBL"><img src="../Images/space.gif" height="7" width="6" alt=""/></td>
	<td class="bB"><img src="../Images/space.gif" height="7" width="3" alt=""/></td>
	<td class="bBR"><img src="../Images/space.gif" height="7" width="6" alt=""/></td>
 </tr>
</table>
