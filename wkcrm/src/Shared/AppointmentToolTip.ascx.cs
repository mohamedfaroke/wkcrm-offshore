using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class Shared_AppointmentToolTip : System.Web.UI.UserControl
{
    private AppointmentBase apt;

    public AppointmentBase TargetAppointment
    {
        get { return apt; }
        set { apt = value; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        extraLabel1.Visible = true;
        extraLabel1Detail.Visible = true;
        extraLabel2.Visible = true;
        extraLabel2Detail.Visible = true;
        startDate.Text = String.Format("{0} - {1}", apt.Start.ToString("dd/MM/yy h:mm tt"), apt.End.ToString("h:mm tt"));
        if (apt.AppointmentType == AppointmentType.LEADMEETING)
        {
            AppointmentLeadWrapper myApt = apt as AppointmentLeadWrapper;
            title.Text = "Lead Meeting";
            iconImage.ImageUrl = Globals.GetAppointmentImage("meeting", true);
            nameLabel.Text = "Designer: ";
            nameField.Text = myApt.DesignerName;
            accountLabel.Text = "Account: ";
            accountName.Text = myApt.AccName;

            extraLabel1.Text = "Location: ";
            extraLabel1Detail.Text = myApt.Location;

            extraLabel2.Text = "Contact Details: ";
            extraLabel2Detail.Text = myApt.HomePhone;
            if (!String.IsNullOrEmpty(extraLabel2Detail.Text) && !String.IsNullOrEmpty(myApt.Mobile))
                extraLabel2Detail.Text += " / ";
            extraLabel2Detail.Text += myApt.Mobile;

        }
        else if (apt.AppointmentType == AppointmentType.PROJECTTASK)
        {
            AppointmentTaskWrapper myApt = apt as AppointmentTaskWrapper;
            title.Text = "Project Task";
            nameLabel.Text = "Tradesman: ";
            nameField.Text = myApt.Tradesman;
            accountLabel.Text = "Account: ";
            accountName.Text = myApt.AccountName;
            extraLabel1.Text = "Task Type: ";
            extraLabel1Detail.Text = myApt.TaskType;
            string taskType = myApt.TaskType.ToLower();
            iconImage.ImageUrl = Globals.GetAppointmentImage(taskType, true);

            if (!string.IsNullOrEmpty(myApt.Notes))
            {
                extraLabel2.Text = "Notes: ";
                extraLabel2Detail.Text = myApt.Notes;
            }
            else
            {
                extraLabel2.Visible = false;
                extraLabel2Detail.Visible = false;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }




}
