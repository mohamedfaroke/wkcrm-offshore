﻿<%@ Register Tagprefix="WKCRM" TagName="Menu_Item" Src="../Shared/Menu_Item.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Menu1.ascx.cs" Inherits="WKCRM.UI.Menu1" %>
<%@ Register Tagprefix="WKCRM" TagName="Menu_Item_Highlighted" Src="../Shared/Menu_Item_Highlighted.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>

<table cellspacing="0" cellpadding="0" border="0">
 <tr>
	<td class="menus">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="mel"><img src="../Images/space.gif" height="1" width="39" alt=""/></td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu1MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../contactSource/ShowContactSourceTablePage.aspx" Button-Text="Contact Source">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu1MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../contactSource/ShowContactSourceTablePage.aspx" Button-Text="Contact Source" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu2MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../employee/ShowEmployeeTablePage.aspx" Button-Text="Employees">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu2MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../employee/ShowEmployeeTablePage.aspx" Button-Text="Employees" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu3MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../employeeRoles/ShowEmployeeRolesTablePage.aspx" Button-Text="Employee Roles">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu3MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../employeeRoles/ShowEmployeeRolesTablePage.aspx" Button-Text="Employee Roles" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu5MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../opportunityCategory/ShowOpportunityCategoryTablePage.aspx" Button-Text="Opp Categories">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu5MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../opportunityCategory/ShowOpportunityCategoryTablePage.aspx" Button-Text="Opp Categories" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu4MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../opportunitySource/ShowOpportunitySourceTablePage.aspx" Button-Text="Opp Sources">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu4MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../opportunitySource/ShowOpportunitySourceTablePage.aspx" Button-Text="Opp Sources" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu8MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Reports/OutstandingTasks.aspx" Button-Text="Outstanding Tasks">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu8MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Reports/OutstandingTasks.aspx" Button-Text="Outstanding Tasks" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu7MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Reports/Overview.aspx" Button-Text="Overview">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu7MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Reports/Overview.aspx" Button-Text="Overview" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu6MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../tradesmen/ShowTradesmenTablePage.aspx" Button-Text="Tradesmen">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu6MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../tradesmen/ShowTradesmenTablePage.aspx" Button-Text="Tradesmen" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu9MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ExportPriceList.aspx" Button-Text="Export Price List">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu9MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ExportPriceList.aspx" Button-Text="Export Price List" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu11MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ExportCRMOptions.aspx" Button-Text="Export CRM Options">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu11MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ExportCRMOptions.aspx" Button-Text="Export CRM Options" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu10MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ImportPriceList.aspx" Button-Text="Import Price List">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu10MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ImportPriceList.aspx" Button-Text="Import Price List" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu12MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ImportCRMOptions.aspx" Button-Text="Import CRM Options">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu12MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Utilities/ImportCRMOptions.aspx" Button-Text="Import CRM Options" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td class="mer"><img src="../Images/space.gif" height="1" width="39" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="mbbg"><img src="../Images/space.gif" height="1" width="1" alt=""/></td>
 </tr>
</table>
