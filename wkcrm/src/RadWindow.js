﻿// JScript File

// This function is a standard
// utility funciton to open a named window
// with the specified url
// It works even though there is no RadWindowManager on the page
function OpenWindow(url, name) {
    var oWnd = $find(name);
    if (oWnd) {
        oWnd.setUrl(url);
        oWnd.Show();
        oWnd.Center();
    }
}

// This function will try to open 
// a radwindow with id EditWindow to 
// the url provided and will set the width 
// to the one provided

function EditWindow(url, myWidth,title) {
    var oWnd = window.radopen(url, 'EditWindow');
    oWnd.SetWidth(myWidth);
    oWnd.SetTitle(title);
    oWnd.Center();
}

// THis function will load the window manager
// and then attempt to open the radwindow called StandardWindow
// And then ensure it sets the width to the default
// The width can be overriden by the page that is opened inside the radwinow later
function StandardWindow(url) {
    var oManager = GetWindowManager(); //This function must be specified in the aspx page
    var oWnd = window.radopen(url, 'StandardWindow');

    oWnd.SetWidth(oManager.GetWidth());
    oWnd.Center();
}

function StandardWindowWidth(url,width) {
    var oManager = GetWindowManager(); //This function must be specified in the aspx page
    var oWnd = window.radopen(url, 'StandardWindow');

    oWnd.SetWidth(width);
    oWnd.Center();
}

// THis function will load the window manager
// and then attempt to open the radwindow called StandardWindow
// And then ensure it sets the width to the default
// The width can be overriden by the page that is opened inside the radwinow later
function StandardWindowWithTitle(url,title) {
    var oManager = GetWindowManager(); //This function must be specified in the aspx page
    var oWnd = window.radopen(url, 'StandardWindow');
    oWnd.SetWidth(oManager.GetWidth());
    oWnd.SetTitle(title);
    oWnd.Center();
}
// Gets the value of the selected tab, which should be the
// relative url, including any arguments	
function TabSelected(sender, eventArgs) {
    var tabStrip = sender;
    var tab = eventArgs.get_tab();
    tab.set_selected(false); //To allow the to click again right away
    StandardWindowWithTitle(tab.get_value(),tab.get_text());
}
// The functions below are mainly used for pages that are
// loaded inside a rad window

function ShowRadConfirm(text, callBackFn, oTitle) {
    // var oBrowserWnd = GetRadWindow().BrowserWindow;
    radconfirm(text, callBackFn, 300, 150, null, oTitle);
}


// This gets the instance of the radwindow for the page that is loaded inside it
function GetRadWindow() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}
// This function sets the width and height of the radwinow
function ResizeWindow(width, height) {
    var oWindow = GetRadWindow();
    if (width)
        oWindow.SetWidth(width);
    if (height)
        oWindow.SetHeight(height);
    oWindow.Center();
}
// This closes it ... d`uh
function CloseWindow() {
    var oWindow = GetRadWindow();
    oWindow.close("Refresh");
}
// This forces the parent page to refresh (and hence close the rad window)
function RefreshParentPage() {
    GetRadWindow().BrowserWindow.location.reload(true);
}
// Redirect the parent page to a new url
function RedirectParentPage(url) {
    GetRadWindow().BrowserWindow.location = url;
}


/// Functions for formatting rad dates 
function formatDate(date) {
    var year = padNumber(date.getUTCFullYear(), 4);
    var month = padNumber(date.getUTCMonth() + 1, 2);
    var day = padNumber(date.getUTCDate(), 2);
    var hour = padNumber(date.getUTCHours(), 2);
    var minute = padNumber(date.getUTCMinutes(), 2);

    return year + month + day + hour + minute;
}

function padNumber(number, totalDigits) {
    number = number.toString();
    var padding = '';
    if (totalDigits > number.length) {
        for (i = 0; i < (totalDigits - number.length); i++) {
            padding += '0';
        }
    }
    return padding + number.toString();
}