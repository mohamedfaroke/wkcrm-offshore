﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="AddTradesmenPage.aspx.cs" Inherits="WKCRM.UI.AddTradesmenPage" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.AddTradesmenPage" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu1" Src="../Menu Panels/Menu1.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu11MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td>&nbsp;<WKCRM:Menu1 runat="server" id="Menu1" HiliteSettings="Menu6MenuItem">
		</WKCRM:Menu1></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<asp:UpdateProgress runat="server" id="tradesmenRecordControlUpdateProgress" AssociatedUpdatePanelID="tradesmenRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="tradesmenRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:tradesmenRecordControl runat="server" id="tradesmenRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="tradesmenDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="tradesmenDialogTitle" Text="Add Tradesmen">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="Label" Text="Entity Name">
														</asp:Label></td>
	<td class="dfv"><asp:DropDownList runat="server" id="TradesmanType">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="TradesmanTypeFvLlsHyperLink" ControlToUpdate="TradesmanType" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="TradesmanType" Field="TradesmanType_.TradesmanTypeId" DisplayField="TradesmanType_.TradesmanType">														</Selectors:FvLlsHyperLink></td>
	</tr><tr>
	<td class="fls">
		<asp:Literal runat="server" id="nameLabel" Text="Contact Name">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="name" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="nameRequiredFieldValidator" ControlToValidate="name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Name&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="nameTextBoxMaxLengthValidator" ControlToValidate="name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Name&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="Label1" Text="ABN/ACN">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="ABN" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="ABNTextBoxMaxLengthValidator" ControlToValidate="ABN" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Abn&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="phoneLabel" Text="Phone">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="phone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="phoneTextBoxMaxLengthValidator" ControlToValidate="phone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="mobileLabel" Text="Mobile">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="mobile" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="mobileTextBoxMaxLengthValidator" ControlToValidate="mobile" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Mobile&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="faxLabel" Text="Fax">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="fax" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="faxTextBoxMaxLengthValidator" ControlToValidate="fax" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Fax&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="addressLabel" Text="Address">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="address" MaxLength="50" Columns="60" CssClass="field_input" Rows="4" TextMode="MultiLine">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="addressTextBoxMaxLengthValidator" ControlToValidate="address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Address&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="emailLabel" Text="Email">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="email" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="emailTextBoxMaxLengthValidator" ControlToValidate="email" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Email&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="send_email_notificationsLabel" Text="Send Email Notifications">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:CheckBox runat="server" id="send_email_notifications" UncheckedValue="No">
														</asp:CheckBox>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="entity_type_idLabel" Text="Entity Type">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:DropDownList runat="server" id="entity_type_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="entity_type_idFvLlsHyperLink" ControlToUpdate="entity_type_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="tradeEntityTypes" Field="TradeEntityTypes_.id" DisplayField="TradeEntityTypes_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="entity_type_idRequiredFieldValidator" ControlToValidate="entity_type_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Entity Type&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="Label2" Text="Public Liability Company">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="PublicLiabilityCompany" Columns="50" MaxLength="50">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="PublicLiabilityCompanyTextBoxMaxLengthValidator" ControlToValidate="PublicLiabilityCompany" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Public Liability Company&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls">
		<asp:Literal runat="server" id="public_liability_numberLabel" Text="Public Liability Number">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="public_liability_number" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="public_liability_numberTextBoxMaxLengthValidator" ControlToValidate="public_liability_number" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Public Liability Number&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="public_liability_expiry_dateLabel" Text="Public Liability Expiration Date">
														</asp:Literal>
	</td>
	<td class="dfv">
		<table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="public_liability_expiry_date" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("public_liability_expiry_date"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="public_liability_expiry_dateFvDsHyperLink" ControlToUpdate="public_liability_expiry_date" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="public_liability_expiry_dateTextBoxMaxLengthValidator" ControlToValidate="public_liability_expiry_date" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Public Liability Expiration Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														
	</td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="Label3" Text="Workers Comp Company">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="WorkersCompensationCompany" Columns="50" MaxLength="50">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="WorkersCompensationCompanyTextBoxMaxLengthValidator" ControlToValidate="WorkersCompensationCompany" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Workers Company Company&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls">
		<asp:Literal runat="server" id="workers_comp_numberLabel" Text="Workers Comp Number">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="workers_comp_number" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="workers_comp_numberTextBoxMaxLengthValidator" ControlToValidate="workers_comp_number" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Workers Company Number&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="workers_comp_expiry_dateLabel" Text="Workers Comp Expiration Date">
														</asp:Literal>
	</td>
	<td class="dfv">
		<table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="workers_comp_expiry_date" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("workers_comp_expiry_date"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="workers_comp_expiry_dateFvDsHyperLink" ControlToUpdate="workers_comp_expiry_date" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="workers_comp_expiry_dateTextBoxMaxLengthValidator" ControlToValidate="workers_comp_expiry_date" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Workers Company Expiration Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="trade_licence_numberLabel" Text="Trade Licence Number">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="trade_licence_number" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="trade_licence_numberTextBoxMaxLengthValidator" ControlToValidate="trade_licence_number" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Trade Licence Number&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="trade_licence_expiry_dateLabel" Text="Trade Licence Expiration Date">
														</asp:Literal>
	</td>
	<td class="dfv">
		<table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="trade_licence_expiry_date" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("trade_licence_expiry_date"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="trade_licence_expiry_dateFvDsHyperLink" ControlToUpdate="trade_licence_expiry_date" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="trade_licence_expiry_dateTextBoxMaxLengthValidator" ControlToValidate="trade_licence_expiry_date" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Trade Licence Expiration Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														
	</td>
	</tr>
	
	
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:tradesmenRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									
<br/>
<br/>


<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-RedirectArgument="updatedata" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table>

<br/>
<br/>

							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
									</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



