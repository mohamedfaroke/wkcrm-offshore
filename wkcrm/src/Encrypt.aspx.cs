using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Impowa.Dev.Security;
public partial class Encrypt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        this.test.PreRender += new EventHandler(test_PreRender);
    }

    void test_PreRender(object sender, EventArgs e)
    {     
        try
        {

            test2.Text += "<p>" + Web.ProtectWebConfig(Server.MapPath(@"~")) + "<p>";
        }
        catch (Exception ez)
        {
            test2.Text += "<p>ERROR WITH ENCRYPTION<p>";
            test2.Text += ez.Message + " " + ez.Source + " " + ez.StackTrace + "<p>";
        }
    }
}
