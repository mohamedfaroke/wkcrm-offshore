﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Header.ascx.cs" Inherits="WKCRM.UI.Header" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>

<%--<style>

body
{
    margin: 0px;
    text-align:center;
}

table
{
    border-collapse: collapse;
}

td.logout
{
    vertical-align: bottom;
    background-color: #E6D2BA;
    text-align: right;
    padding-right: 10px;
    padding-bottom: 10px;
}

td.footer
{
    font-family: Verdana;
    font-size: 10px;
    color: #6F6F6F;
}

table.box
{
    width: 400px;
    border: 2px solid #E2E2E2;
}

table.box td.title
{
    font: bold 12px/20px Verdana;
    text-transform: uppercase;
    background-color: #D3E3F3;
    padding-left: 10px;
    color: #5C5C5C;
    border: 1px solid #E2E2E2;
}

</style>--%>
<%--<table cellspacing="0" cellpadding="0" border="0" width="100%">
 <tr>
	<td width="720">
	<asp:Image ImageUrl="../Images/Header.png" runat="server" id="_Logo" AlternateText="">

</asp:Image>
	</td>
	<td class="logout">
        <asp:ImageButton ID="logoutButton" runat="server" ImageUrl="../Images/logout.png" BorderWidth="0"/>
        </td>
	
 </tr>
 <tr> 
 <td> <asp:Label ID="welcomeMsg" runat="server" />
 </td>
 </tr>
</table>--%>

       

<html>
    <head>
    <title></title>
      <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/bootstrap.min.css"/>
      <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/bootstrap.css"/>
      <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/sb-admin-2.css"/>
      <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/font-awesome/css/font-awesome.min.css"/>
       <link rel="stylesheet" rev="stylesheet" type="text/javascript" href="../js/jquery.js"/>
        <link rel="stylesheet" rev="stylesheet" type="text/javascript" href="../js/bootstrap.min.js"/>
        <link rel="stylesheet" rev="stylesheet" type="text/javascript" href="../js/plugins/metisMenu/metisMenu.min.js"/>
        <link rel="stylesheet" rev="stylesheet" type="text/javascript" href="../js/plugins/morris/raphael.min.js"/>
        <link rel="stylesheet" rev="stylesheet" type="text/javascript" href="../js/plugins/morris/morris.min.js"/>
        <link rel="stylesheet" rev="stylesheet" type="text/javascript" href="../js/plugins/morris/morris-data.js"/>
        <link rel="stylesheet" rev="stylesheet" type="text/javascript" href="../js/sb-admin-2.js"/>
     </head>
        <body>
        
       
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">WonderFull Kitchen</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>Abdullah</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>Abdullah</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>Abdullah</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href=""><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                           <%-- <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>--%>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a class="active" href=""><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../account/LeadReviews.aspx"><i class="fa fa-search"></i> Lead Reviews</a>
                            
                            
                            <%--<span class="fa arrow"></span></a>--%>
                            <%--<ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="">Morris.js Charts</a>
                                </li>
                            </ul>--%>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="../Reports/Overview.aspx"><i class="fa fa-calendar"></i> OverViews</a>
                        </li>
                        <li>
                            <a href="../Reports/ShowReportsTablePage.aspx"><i class="fa fa-book"></i> Reports</a>
                        </li>
                        <li>
                            <a href="../contactSource/ShowContactSourceTablePage.aspx"><i class="fa fa-wrench fa-fw"></i> Adminstration</a>
                            <%--<span class="fa arrow"></span></a>--%>
                            <%--<ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="">Buttons</a>
                                </li>
                                <li>
                                    <a href="">Notifications</a>
                                </li>
                                <li>
                                    <a href="">Typography</a>
                                </li>
                                <li>
                                    <a href=""> Icons</a>
                                </li>
                                <li>
                                    <a href="">Grid</a>
                                </li>
                            </ul>
                            --%><!-- /.nav-second-level -->
                        </li>
                       <%-- <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Family Folder<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level collapse">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Subjects<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="">Blank Page</a>
                                </li>
                                <li>
                                    <a href="">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>--%>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
     
<br />
</body>
</html>