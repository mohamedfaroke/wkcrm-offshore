﻿<%--<%@ Page Language="C#" MasterPageFile="~/Master pages/WkCRMHeader.master" AutoEventWireup="true" CodeFile="SignIn.aspx.cs"
    Inherits="WKCRM.UI.SignIn" Title="Log-In" %>--%>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="SignIn_Control" Src="../Shared/SignIn_Control.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="SignIn.aspx.cs" Inherits="WKCRM.UI.SignIn" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/bootstrap.min.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server"></BaseClasses:BasePageSettings>		
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->

<%--
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
	<WKCRM:SignIn_Control runat="server" id="SignInControl">
		</WKCRM:SignIn_Control>
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
	
     <div align="center" > 
	<WKCRM:ThemeButton runat="server" id="OKButton" Button-CausesValidation="True" Button-CommandName="Login" Button-Text="&lt;%# GetResourceValue(&quot;Btn:LOGIN&quot;, &quot;WKCRM&quot;) %>">
			</WKCRM:ThemeButton>
     </div>

	
--%>
    <div class="col-md-4 col-md-offset-4">

        <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
        <fieldset>
        <%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
	<WKCRM:SignIn_Control runat="server" id="SignInControl">
		</WKCRM:SignIn_Control>
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
     <div class="checkbox">
     <label>
     <input name="remember" type="checkbox" value="Remember Me"/>Remember Me
      </label>
      </div>
	
     <div align="center" > 
	<WKCRM:ThemeButton runat="server" id="OKButton" Button-CausesValidation="True" Button-CommandName="Login" Button-Text="&lt;%# GetResourceValue(&quot;LOGIN&quot;, &quot;WKCRM&quot;) %>">
			</WKCRM:ThemeButton>
     </div>
     </fieldset>
	</div>
    </div>
    </div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



