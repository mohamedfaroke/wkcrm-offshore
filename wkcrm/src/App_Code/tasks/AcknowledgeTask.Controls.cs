﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// AcknowledgeTask.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.AcknowledgeTask
{
  

#region "Section 1: Place your customizations here."


    public class tasksRecordControl : BasetasksRecordControl
    {

        // The BasetasksRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

        /// <summary>
        /// Acknowledge that the user has viewed the record
        /// </summary>
        public override void DataBind()
        {
            base.DataBind();
            if (this.DataSource != null && !this.DataSource.acknowledged)
            {
                try
                {
                    string id = this.DataSource.id0.ToString();
                    TasksRecord rec = TasksTable.GetRecord(id, true);
                    rec.acknowledged = true;
                    rec.Save();
                    DbUtils.CommitTransaction();
                }
                catch
                {
                }
            }
        }
    }


  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the tasksRecordControl control on the AcknowledgeTask page.
// Do not modify this class. Instead override any method in tasksRecordControl.
public class BasetasksRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BasetasksRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in tasksRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in tasksRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in tasksRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = TasksTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new TasksRecord();
                return;
            }

            // Retrieve the record from the database.
            TasksRecord[] recList = TasksTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = TasksTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in tasksRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(TasksTable.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime1.Text = formattedValue;
            } else {  
                this.datetime1.Text = TasksTable.datetime0.Format(TasksTable.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime1.Text == null ||
                this.datetime1.Text.Trim().Length == 0) {
                this.datetime1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(TasksTable.notes);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.TasksTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"notes\\\", \\\"Notes\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.notes.Text = formattedValue;
            } else {  
                this.notes.Text = TasksTable.notes.Format(TasksTable.notes.DefaultValue);
            }
                    
            if (this.notes.Text == null ||
                this.notes.Text.Trim().Length == 0) {
                this.notes.Text = "&nbsp;";
            }
                  
            if (this.DataSource.task_type_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(TasksTable.task_type_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.task_type_id.Text = formattedValue;
            } else {  
                this.task_type_id.Text = TasksTable.task_type_id.Format(TasksTable.task_type_id.DefaultValue);
            }
                    
            if (this.task_type_id.Text == null ||
                this.task_type_id.Text.Trim().Length == 0) {
                this.task_type_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.tradesman_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(TasksTable.tradesman_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.tradesman_id.Text = formattedValue;
            } else {  
                this.tradesman_id.Text = TasksTable.tradesman_id.Format(TasksTable.tradesman_id.DefaultValue);
            }
                    
            if (this.tradesman_id.Text == null ||
                this.tradesman_id.Text.Trim().Length == 0) {
                this.tradesman_id.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in tasksRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in tasksRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in tasksRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in tasksRecordControl.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in tasksRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            TasksTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["Tasks"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "Tasks"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(TasksTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(TasksTable.id0).ToString());
            } else {
                
                wc.iAND(TasksTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in tasksRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            TasksTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasetasksRecordControl_Rec"];
            }
            set {
                this.ViewState["BasetasksRecordControl_Rec"] = value;
            }
        }
        
        private TasksRecord _DataSource;
        public TasksRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal datetime1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime1");
            }
        }
        
        public System.Web.UI.WebControls.Literal datetimeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal notes {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notes");
            }
        }
        
        public System.Web.UI.WebControls.Literal notesLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notesLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal task_type_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "task_type_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal task_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "task_type_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal tasksDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tasksDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.Literal tradesman_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesman_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal tradesman_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesman_idLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            TasksRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public TasksRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return TasksTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  