﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowTasksTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowTasksTablePage
{
  

#region "Section 1: Place your customizations here."

    
//public class tasksTableControlRow : BasetasksTableControlRow
//{
//      
//        // The BasetasksTableControlRow implements code for a ROW within the
//        // the tasksTableControl table.  The BasetasksTableControlRow implements the DataBind and SaveData methods.
//        // The loading of data is actually performed by the LoadData method in the base class of tasksTableControl.
//
//        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
//        // SaveData, GetUIData, and Validate methods.
//        
//
//}
//

  

//public class tasksTableControl : BasetasksTableControl
//{
//        // The BasetasksTableControl class implements the LoadData, DataBind, CreateWhereClause
//        // and other methods to load and display the data in a table control.
//
//        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
//        // The tasksTableControlRow class offers another place where you can customize
//        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.
//
//}
//

  

public class View_TradesmenTasksTableControl : BaseView_TradesmenTasksTableControl
{
        // The BaseView_TradesmenTasksTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The View_TradesmenTasksTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class View_TradesmenTasksTableControlRow : BaseView_TradesmenTasksTableControlRow
{
      
        // The BaseView_TradesmenTasksTableControlRow implements code for a ROW within the
        // the View_TradesmenTasksTableControl table.  The BaseView_TradesmenTasksTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of View_TradesmenTasksTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}
#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the View_TradesmenTasksTableControlRow control on the ShowTasksTablePage page.
// Do not modify this class. Instead override any method in View_TradesmenTasksTableControlRow.
public class BaseView_TradesmenTasksTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseView_TradesmenTasksTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in View_TradesmenTasksTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.View_TradesmenTasksRecordRowEditButton.Click += new ImageClickEventHandler(View_TradesmenTasksRecordRowEditButton_Click);
        }

        // To customize, override this method in View_TradesmenTasksTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in View_TradesmenTasksTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_TradesmenTasksView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseView_TradesmenTasksTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_TradesmenTasksRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in View_TradesmenTasksTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(View_TradesmenTasksView.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime1.Text = formattedValue;
            } else {  
                this.datetime1.Text = View_TradesmenTasksView.datetime0.Format(View_TradesmenTasksView.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime1.Text == null ||
                this.datetime1.Text.Trim().Length == 0) {
                this.datetime1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_TradesmenTasksView.notes);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.View_TradesmenTasksView, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"notes\\\", \\\"Notes\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.notes.Text = formattedValue;
            } else {  
                this.notes.Text = View_TradesmenTasksView.notes.Format(View_TradesmenTasksView.notes.DefaultValue);
            }
                    
            if (this.notes.Text == null ||
                this.notes.Text.Trim().Length == 0) {
                this.notes.Text = "&nbsp;";
            }
                  
            if (this.DataSource.project_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_TradesmenTasksView.project_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.project_id.Text = formattedValue;
            } else {  
                this.project_id.Text = View_TradesmenTasksView.project_id.Format(View_TradesmenTasksView.project_id.DefaultValue);
            }
                    
            if (this.project_id.Text == null ||
                this.project_id.Text.Trim().Length == 0) {
                this.project_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.task_type_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_TradesmenTasksView.task_type_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.task_type_id.Text = formattedValue;
            } else {  
                this.task_type_id.Text = View_TradesmenTasksView.task_type_id.Format(View_TradesmenTasksView.task_type_id.DefaultValue);
            }
                    
            if (this.task_type_id.Text == null ||
                this.task_type_id.Text.Trim().Length == 0) {
                this.task_type_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.TaskAcknowledgedSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_TradesmenTasksView.TaskAcknowledged);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.TaskAcknowledged.Text = formattedValue;
            } else {  
                this.TaskAcknowledged.Text = View_TradesmenTasksView.TaskAcknowledged.Format(View_TradesmenTasksView.TaskAcknowledged.DefaultValue);
            }
                    
            if (this.TaskAcknowledged.Text == null ||
                this.TaskAcknowledged.Text.Trim().Length == 0) {
                this.TaskAcknowledged.Text = "&nbsp;";
            }
                  
            if (this.DataSource.tradesman_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_TradesmenTasksView.tradesman_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.tradesman_id.Text = formattedValue;
            } else {  
                this.tradesman_id.Text = View_TradesmenTasksView.tradesman_id.Format(View_TradesmenTasksView.tradesman_id.DefaultValue);
            }
                    
            if (this.tradesman_id.Text == null ||
                this.tradesman_id.Text.Trim().Length == 0) {
                this.tradesman_id.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in View_TradesmenTasksTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in View_TradesmenTasksTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in View_TradesmenTasksTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((View_TradesmenTasksTableControl)MiscUtils.GetParentControlObject(this, "View_TradesmenTasksTableControl")).DataChanged = true;
                ((View_TradesmenTasksTableControl)MiscUtils.GetParentControlObject(this, "View_TradesmenTasksTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in View_TradesmenTasksTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in View_TradesmenTasksTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in View_TradesmenTasksTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_TradesmenTasksView.DeleteRecord(pk);

          
            ((View_TradesmenTasksTableControl)MiscUtils.GetParentControlObject(this, "View_TradesmenTasksTableControl")).DataChanged = true;
            ((View_TradesmenTasksTableControl)MiscUtils.GetParentControlObject(this, "View_TradesmenTasksTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void View_TradesmenTasksRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"EditTasksPage.aspx?Tasks={View_TradesmenTasksTableControlRow:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseView_TradesmenTasksTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseView_TradesmenTasksTableControlRow_Rec"] = value;
            }
        }
        
        private View_TradesmenTasksRecord _DataSource;
        public View_TradesmenTasksRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal datetime1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime1");
            }
        }
           
        public System.Web.UI.WebControls.Literal notes {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notes");
            }
        }
           
        public System.Web.UI.WebControls.Literal project_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "project_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal task_type_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "task_type_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal TaskAcknowledged {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TaskAcknowledged");
            }
        }
           
        public System.Web.UI.WebControls.Literal tradesman_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesman_id");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton View_TradesmenTasksRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksRecordRowEditButton");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox View_TradesmenTasksRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksRecordRowSelection");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_TradesmenTasksRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_TradesmenTasksRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_TradesmenTasksView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the View_TradesmenTasksTableControl control on the ShowTasksTablePage page.
// Do not modify this class. Instead override any method in View_TradesmenTasksTableControl.
public class BaseView_TradesmenTasksTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseView_TradesmenTasksTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.View_TradesmenTasksPagination.FirstPage.Click += new ImageClickEventHandler(View_TradesmenTasksPagination_FirstPage_Click);
            this.View_TradesmenTasksPagination.LastPage.Click += new ImageClickEventHandler(View_TradesmenTasksPagination_LastPage_Click);
            this.View_TradesmenTasksPagination.NextPage.Click += new ImageClickEventHandler(View_TradesmenTasksPagination_NextPage_Click);
            this.View_TradesmenTasksPagination.PageSizeButton.Button.Click += new EventHandler(View_TradesmenTasksPagination_PageSizeButton_Click);
            this.View_TradesmenTasksPagination.PreviousPage.Click += new ImageClickEventHandler(View_TradesmenTasksPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.datetimeLabel.Click += new EventHandler(datetimeLabel_Click);

            // Setup the button events.
        
            this.View_TradesmenTasksDeleteButton.Button.Click += new EventHandler(View_TradesmenTasksDeleteButton_Click);
            this.View_TradesmenTasksEditButton.Button.Click += new EventHandler(View_TradesmenTasksEditButton_Click);

            // Setup the filter and search events.
        
            this.project_idFilter.SelectedIndexChanged += new EventHandler(project_idFilter_SelectedIndexChanged);
            this.TaskAcknowledgedFilter.SelectedIndexChanged += new EventHandler(TaskAcknowledgedFilter_SelectedIndexChanged);
            this.tradesman_idFilter.SelectedIndexChanged += new EventHandler(tradesman_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.project_idFilter)) {
                this.project_idFilter.Items.Add(new ListItem(this.GetFromSession(this.project_idFilter), this.GetFromSession(this.project_idFilter)));
                this.project_idFilter.SelectedValue = this.GetFromSession(this.project_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.TaskAcknowledgedFilter)) {
                this.TaskAcknowledgedFilter.Items.Add(new ListItem(this.GetFromSession(this.TaskAcknowledgedFilter), this.GetFromSession(this.TaskAcknowledgedFilter)));
                this.TaskAcknowledgedFilter.SelectedValue = this.GetFromSession(this.TaskAcknowledgedFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.tradesman_idFilter)) {
                this.tradesman_idFilter.Items.Add(new ListItem(this.GetFromSession(this.tradesman_idFilter), this.GetFromSession(this.tradesman_idFilter)));
                this.tradesman_idFilter.SelectedValue = this.GetFromSession(this.tradesman_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_TradesmenTasksView.datetime0, OrderByItem.OrderDir.Desc);
        
                this.CurrentSortOrder.Add(View_TradesmenTasksView.project_id, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.View_TradesmenTasksDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_TradesmenTasksRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_TradesmenTasksRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_TradesmenTasksView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_TradesmenTasksRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_TradesmenTasksRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (View_TradesmenTasksTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_TradesmenTasksRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_TradesmenTasksRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_TradesmenTasksView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateproject_idFilter(MiscUtils.GetSelectedValue(this.project_idFilter, this.GetFromSession(this.project_idFilter)), 500);
            this.PopulateTaskAcknowledgedFilter(MiscUtils.GetSelectedValue(this.TaskAcknowledgedFilter, this.GetFromSession(this.TaskAcknowledgedFilter)), 500);
            this.Populatetradesman_idFilter(MiscUtils.GetSelectedValue(this.tradesman_idFilter, this.GetFromSession(this.tradesman_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_TradesmenTasksTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                View_TradesmenTasksTableControlRow recControl = (View_TradesmenTasksTableControlRow)(repItem.FindControl("View_TradesmenTasksTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_TradesmenTasksView.project_id, this.DataSource);
            this.Page.PregetDfkaRecords(View_TradesmenTasksView.task_type_id, this.DataSource);
            this.Page.PregetDfkaRecords(View_TradesmenTasksView.tradesman_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for View_TradesmenTasksTableControl pagination.
        
            this.View_TradesmenTasksPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.View_TradesmenTasksPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.View_TradesmenTasksPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.View_TradesmenTasksPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.View_TradesmenTasksPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.View_TradesmenTasksPagination.CurrentPage.Text = "0";
            }
            this.View_TradesmenTasksPagination.PageSize.Text = this.PageSize.ToString();
            this.View_TradesmenTasksTotalItems.Text = this.TotalRecords.ToString();
            this.View_TradesmenTasksPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.View_TradesmenTasksPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (View_TradesmenTasksTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_TradesmenTasksView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.project_idFilter)) {
                wc.iAND(View_TradesmenTasksView.project_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.project_idFilter, this.GetFromSession(this.project_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.TaskAcknowledgedFilter)) {
                wc.iAND(View_TradesmenTasksView.TaskAcknowledged, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.TaskAcknowledgedFilter, this.GetFromSession(this.TaskAcknowledgedFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.tradesman_idFilter)) {
                wc.iAND(View_TradesmenTasksView.tradesman_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.tradesman_idFilter, this.GetFromSession(this.tradesman_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.View_TradesmenTasksPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.View_TradesmenTasksPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_TradesmenTasksTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    View_TradesmenTasksTableControlRow recControl = (View_TradesmenTasksTableControlRow)(repItem.FindControl("View_TradesmenTasksTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_TradesmenTasksRecord rec = new View_TradesmenTasksRecord();
        
                        if (recControl.datetime1.Text != "") {
                            rec.Parse(recControl.datetime1.Text, View_TradesmenTasksView.datetime0);
                        }
                        if (recControl.notes.Text != "") {
                            rec.Parse(recControl.notes.Text, View_TradesmenTasksView.notes);
                        }
                        if (recControl.project_id.Text != "") {
                            rec.Parse(recControl.project_id.Text, View_TradesmenTasksView.project_id);
                        }
                        if (recControl.task_type_id.Text != "") {
                            rec.Parse(recControl.task_type_id.Text, View_TradesmenTasksView.task_type_id);
                        }
                        if (recControl.TaskAcknowledged.Text != "") {
                            rec.Parse(recControl.TaskAcknowledged.Text, View_TradesmenTasksView.TaskAcknowledged);
                        }
                        if (recControl.tradesman_id.Text != "") {
                            rec.Parse(recControl.tradesman_id.Text, View_TradesmenTasksView.tradesman_id);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_TradesmenTasksRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_TradesmenTasksRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_TradesmenTasksRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(View_TradesmenTasksTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(View_TradesmenTasksTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for project_idFilter.
        protected virtual void Populateproject_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(ProjectTable.id0, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.project_idFilter.Items.Clear();
            foreach (ProjectRecord itemValue in ProjectTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(ProjectTable.id0);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.project_idFilter.Items.IndexOf(item) < 0) {
                    this.project_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.project_idFilter, selectedValue);

            // Add the All item.
            this.project_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for TaskAcknowledgedFilter.
        protected virtual void PopulateTaskAcknowledgedFilter(string selectedValue, int maxItems)
        {
              
            // Setup the WHERE clause, including the base table if needed.
                
            WhereClause wc = new WhereClause();
                  
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(View_TradesmenTasksView.TaskAcknowledged, OrderByItem.OrderDir.Asc);

            string[] list = View_TradesmenTasksView.GetValues(View_TradesmenTasksView.TaskAcknowledged, wc, orderBy, maxItems);
            
            this.TaskAcknowledgedFilter.Items.Clear();
            foreach (string itemValue in list)
            {
                // Create the item and add to the list.
                string fvalue = View_TradesmenTasksView.TaskAcknowledged.Format(itemValue);
                ListItem item = new ListItem(fvalue, itemValue);
                this.TaskAcknowledgedFilter.Items.Add(item);
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.TaskAcknowledgedFilter, selectedValue);

            // Add the All item.
            this.TaskAcknowledgedFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for tradesman_idFilter.
        protected virtual void Populatetradesman_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TradesmenTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.tradesman_idFilter.Items.Clear();
            foreach (TradesmenRecord itemValue in TradesmenTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(TradesmenTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.tradesman_idFilter.Items.IndexOf(item) < 0) {
                    this.tradesman_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.tradesman_idFilter, selectedValue);

            // Add the All item.
            this.tradesman_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter project_idFilter.
        public virtual WhereClause CreateWhereClause_project_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.TaskAcknowledgedFilter)) {
                wc.iAND(View_TradesmenTasksView.TaskAcknowledged, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.TaskAcknowledgedFilter, this.GetFromSession(this.TaskAcknowledgedFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.tradesman_idFilter)) {
                wc.iAND(View_TradesmenTasksView.tradesman_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.tradesman_idFilter, this.GetFromSession(this.tradesman_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter TaskAcknowledgedFilter.
        public virtual WhereClause CreateWhereClause_TaskAcknowledgedFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.project_idFilter)) {
                wc.iAND(View_TradesmenTasksView.project_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.project_idFilter, this.GetFromSession(this.project_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.tradesman_idFilter)) {
                wc.iAND(View_TradesmenTasksView.tradesman_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.tradesman_idFilter, this.GetFromSession(this.tradesman_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter tradesman_idFilter.
        public virtual WhereClause CreateWhereClause_tradesman_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.project_idFilter)) {
                wc.iAND(View_TradesmenTasksView.project_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.project_idFilter, this.GetFromSession(this.project_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.TaskAcknowledgedFilter)) {
                wc.iAND(View_TradesmenTasksView.TaskAcknowledged, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.TaskAcknowledgedFilter, this.GetFromSession(this.TaskAcknowledgedFilter)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.project_idFilter, this.project_idFilter.SelectedValue);
            this.SaveToSession(this.TaskAcknowledgedFilter, this.TaskAcknowledgedFilter.SelectedValue);
            this.SaveToSession(this.tradesman_idFilter, this.tradesman_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.project_idFilter);
            this.RemoveFromSession(this.TaskAcknowledgedFilter);
            this.RemoveFromSession(this.tradesman_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["View_TradesmenTasksTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["View_TradesmenTasksTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void View_TradesmenTasksPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_TradesmenTasksPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_TradesmenTasksPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void View_TradesmenTasksPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_TradesmenTasksPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void datetimeLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_TradesmenTasksView.datetime0);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_TradesmenTasksView.datetime0, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void View_TradesmenTasksDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void View_TradesmenTasksEditButton_Click(object sender, EventArgs args)
        {
            
            string url = @"EditTasksPage.aspx?Tasks={View_TradesmenTasksTableControlRow:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void project_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void TaskAcknowledgedFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void tradesman_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_TradesmenTasksRecord[] _DataSource = null;
        public  View_TradesmenTasksRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.LinkButton datetimeLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal notesLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notesLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList project_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "project_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal project_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "project_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal project_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "project_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal task_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "task_type_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList TaskAcknowledgedFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TaskAcknowledgedFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal TaskAcknowledgedLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TaskAcknowledgedLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal TaskAcknowledgedLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TaskAcknowledgedLabel1");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList tradesman_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesman_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal tradesman_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesman_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal tradesman_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesman_idLabel1");
            }
        }
        
        public WKCRM.UI.IThemeButton View_TradesmenTasksDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton View_TradesmenTasksEditButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksEditButton");
            }
        }
        
        public WKCRM.UI.IPagination View_TradesmenTasksPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksPagination");
            }
        }
        
        public System.Web.UI.WebControls.Literal View_TradesmenTasksTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox View_TradesmenTasksToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label View_TradesmenTasksTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_TradesmenTasksTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                View_TradesmenTasksTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_TradesmenTasksRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (View_TradesmenTasksTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_TradesmenTasksRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public View_TradesmenTasksTableControlRow GetSelectedRecordControl()
        {
        View_TradesmenTasksTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public View_TradesmenTasksTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (View_TradesmenTasksTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_TradesmenTasksRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (View_TradesmenTasksTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowTasksTablePage.View_TradesmenTasksTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            View_TradesmenTasksTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (View_TradesmenTasksTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.View_TradesmenTasksRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public View_TradesmenTasksTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("View_TradesmenTasksTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                View_TradesmenTasksTableControlRow recControl = (View_TradesmenTasksTableControlRow)repItem.FindControl("View_TradesmenTasksTableControlRow");
                recList.Add(recControl);
            }

            return (View_TradesmenTasksTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowTasksTablePage.View_TradesmenTasksTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  