﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// EditAccountPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI.Controls.EditAccountPage
{
  

#region "Section 1: Place your customizations here."

    

  
public class accountRecordControl : BaseaccountRecordControl
{
      
        // The BaseaccountRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

    public accountRecordControl()
    {
        this.Load += new EventHandler(accountRecordControl_Load);
    }

    void accountRecordControl_Load(object sender, EventArgs e)
    {
        this.designer_id.AutoPostBack = true;
        this.designer_id.SelectedIndexChanged += new EventHandler(designer_id_SelectedIndexChanged);
        this.account_status_id.AutoPostBack = true;
        this.account_status_id.SelectedIndexChanged += new EventHandler(account_status_id_SelectedIndexChanged);
        Globals.SetDesignerScheduleLabel(scheduleLabel, designer_id);
    }
    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null && this.DataSource.account_status_id == Globals.ACCOUNT_NOTINTERESTED)
        {
            this.Page.Session["NotInterested"] = true;
        }
        else
        {
            this.Page.Session["NotInterested"] = false;
        }
        if (this.DataSource != null)
        {
            RadComboBox suburbCombo = this.Page.FindControlRecursively("SuburbCombo") as RadComboBox;
            if (suburbCombo != null)
            {
                suburbCombo.Text = this.DataSource.suburb;
            }
        }
    }


    void account_status_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool notInterested = (bool)this.Page.Session["NotInterested"];
        if (notInterested != null && notInterested && this.account_status_id.SelectedValue == Globals.ACCOUNT_OPEN.ToString())
        {
            this.has_been_revived.Checked = true;
            this.Page.Session["NotInterested"] = false;
        }
    }

    void designer_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        Globals.SetDesignerScheduleLabel(scheduleLabel, designer_id);
    }

    public override WhereClause CreateWhereClause_designer_idDropDownList()
    {
        return Globals.BuildDesignerWC();
    }

    public override WhereClause CreateWhereClause_contact_source_idDropDownList()
    {
        return Globals.BuildContactSourceWC();
    }

    public override WhereClause CreateWhereClause_opportunity_source_idDropDownList()
    {
        return Globals.BuildOpSourceWC();
    }

    public override WhereClause CreateWhereClause_account_status_idDropDownList()
    {
        return Globals.BuildAccountStatusWC();
    }

    public override void Validate()
    {
        base.Validate();
        if (String.IsNullOrEmpty(home_phone.Text) && String.IsNullOrEmpty(business_phone.Text)
           && String.IsNullOrEmpty(mobile.Text) && String.IsNullOrEmpty(business_phone2.Text) 
           && String.IsNullOrEmpty(mobile2.Text) && String.IsNullOrEmpty(builders_phone.Text))
        {
            throw new Exception("Must supply at least one contact number");
        }
        RadComboBox suburbCombo = this.Page.FindControlRecursively("SuburbCombo") as RadComboBox;
        if (suburbCombo != null && String.IsNullOrEmpty(suburbCombo.Text))
        {
            throw new Exception("Please select a suburb");
        }

        if (this.opportunity_source_id.SelectedItem.Text == "Referral" &&
            String.IsNullOrEmpty(OpportunityDetails.Text))
        {
            throw new Exception("Please enter opportunity details for referral");
        }

        int accountStatus = Convert.ToInt32(this.account_status_id.SelectedValue);
        if(accountStatus != Globals.ACCOUNT_COMPLETED && accountStatus != Globals.ACCOUNT_OPEN)
        {
            DateTime dateDied = DateTime.MinValue;
            if(!DateTime.TryParse(this.date_died.Text,out dateDied))
            {
                throw new Exception("A valid value for date died is required if account status is neither open nor completed");
            }
        }
    }

    public override void SaveData()
    {
        base.SaveData();
        Globals.GenerateAccountNotifications(this.DataSource);
    }

    public override void GetUIData()
    {
        base.GetUIData();
        RadComboBox suburbCombo = this.Page.FindControlRecursively("SuburbCombo") as RadComboBox;
        if (suburbCombo != null)
        {
            this.DataSource.Parse(suburbCombo.Text, AccountTable.suburb);
        }
        
        int accountStatus = this.DataSource.account_status_id;
        if(!this.DataSource.date_diedSpecified && accountStatus != Globals.ACCOUNT_OPEN && accountStatus != Globals.ACCOUNT_COMPLETED)
        {
            //Lead dead
            this.DataSource.date_died = DateTime.Today;
        }

    }
}

  

public class leadMeetingsTableControl : BaseleadMeetingsTableControl
{
        // The BaseleadMeetingsTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The leadMeetingsTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class leadMeetingsTableControlRow : BaseleadMeetingsTableControlRow
{
      
        // The BaseleadMeetingsTableControlRow implements code for a ROW within the
        // the leadMeetingsTableControl table.  The BaseleadMeetingsTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of leadMeetingsTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void EmailButton_Click(object sender, ImageClickEventArgs args)
    {
        //base.EmailButton_Click(sender, args);
        string id = Globals.GetRecordIDFromXML(this.RecordUniqueId, "id");
        LeadMeetingsRecord rec = LeadMeetingsTable.GetRecord(id, false);
        string msg = "";
        bool success = Globals.SendLeadMeetingEmail(rec, out msg);

        if(String.IsNullOrEmpty(msg))
        {
            msg = success ? "Email succesfully sent" : "Emails failed to send";
        }

        MiscUtils.RegisterJScriptAlert(this.Page, "EMAILCONFIRM", msg);
    }
}
public class communicationTableControl : BasecommunicationTableControl
{
        // The BasecommunicationTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The communicationTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class communicationTableControlRow : BasecommunicationTableControlRow
{
      
        // The BasecommunicationTableControlRow implements code for a ROW within the
        // the communicationTableControl table.  The BasecommunicationTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of communicationTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}
public class attachmentTableControl : BaseattachmentTableControl
{
        // The BaseattachmentTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The attachmentTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class attachmentTableControlRow : BaseattachmentTableControlRow
{
      
        // The BaseattachmentTableControlRow implements code for a ROW within the
        // the attachmentTableControl table.  The BaseattachmentTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of attachmentTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public attachmentTableControlRow()
    {
        this.PreRender += new EventHandler(attachmentTableControlRow_PreRender);
    }

    void attachmentTableControlRow_PreRender(object sender, EventArgs e)
    {
        if (this.DataSource != null)
        {
            string imageName = Globals.GetAttachmentImage(this.DataSource.fileName, this.Page);

            this.ViewAttachmentLabel.Text = "<a href='../attachment/ViewAttachment.aspx?AttachmentId=" + this.DataSource.id0 + "' target='Attachments'><img src='" + imageName + "' border = 0></img></a>";
        }

    }
}

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the attachmentTableControlRow control on the EditAccountPage page.
// Do not modify this class. Instead override any method in attachmentTableControlRow.
public class BaseattachmentTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseattachmentTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in attachmentTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in attachmentTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in attachmentTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = AttachmentTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseattachmentTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new AttachmentRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in attachmentTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AttachmentTable.name);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.AttachmentTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"name\\\", \\\"Name\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.name1.Text = formattedValue;
            } else {  
                this.name1.Text = AttachmentTable.name.Format(AttachmentTable.name.DefaultValue);
            }
                    
            if (this.name1.Text == null ||
                this.name1.Text.Trim().Length == 0) {
                this.name1.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in attachmentTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // account in accountRecordControl is One To Many to attachmentTableControl.
                    
            // Setup the parent id in the record.
            accountRecordControl recaccountRecordControl = (accountRecordControl)this.Page.FindControlRecursively("accountRecordControl");
            if (recaccountRecordControl != null && recaccountRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                recaccountRecordControl.LoadData();
            }
            if (recaccountRecordControl == null || recaccountRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.account_id = recaccountRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in attachmentTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in attachmentTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((attachmentTableControl)MiscUtils.GetParentControlObject(this, "attachmentTableControl")).DataChanged = true;
                ((attachmentTableControl)MiscUtils.GetParentControlObject(this, "attachmentTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in attachmentTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in attachmentTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in attachmentTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            AttachmentTable.DeleteRecord(pk);

          
            ((attachmentTableControl)MiscUtils.GetParentControlObject(this, "attachmentTableControl")).DataChanged = true;
            ((attachmentTableControl)MiscUtils.GetParentControlObject(this, "attachmentTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseattachmentTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseattachmentTableControlRow_Rec"] = value;
            }
        }
        
        private AttachmentRecord _DataSource;
        public AttachmentRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.CheckBox attachmentRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "attachmentRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Literal name1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name1");
            }
        }
        
        public System.Web.UI.WebControls.Label ViewAttachmentLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "ViewAttachmentLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            AttachmentRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public AttachmentRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return AttachmentTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the attachmentTableControl control on the EditAccountPage page.
// Do not modify this class. Instead override any method in attachmentTableControl.
public class BaseattachmentTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseattachmentTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.attachmentDeleteButton.Button.Click += new EventHandler(attachmentDeleteButton_Click);
            this.attachmentNewButton.Button.Click += new EventHandler(attachmentNewButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(AttachmentTable.name, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.attachmentDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (AttachmentRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.AttachmentRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = AttachmentTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (AttachmentRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.AttachmentRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (attachmentTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (AttachmentRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.AttachmentRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = AttachmentTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("attachmentTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                attachmentTableControlRow recControl = (attachmentTableControlRow)(repItem.FindControl("attachmentTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for attachmentTableControl pagination.
        

            // Bind the pagination labels.
        
            this.attachmentTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (attachmentTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            AttachmentTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        accountRecordControl parentRecordControl = (accountRecordControl)(this.Page.FindControlRecursively("accountRecordControl"));
            AccountRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(AttachmentTable.account_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("attachmentTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    attachmentTableControlRow recControl = (attachmentTableControlRow)(repItem.FindControl("attachmentTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        AttachmentRecord rec = new AttachmentRecord();
        
                        if (recControl.name1.Text != "") {
                            rec.Parse(recControl.name1.Text, AttachmentTable.name);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new AttachmentRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (AttachmentRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.AttachmentRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(attachmentTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(attachmentTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["attachmentTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["attachmentTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void attachmentDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void attachmentNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../attachment/AddAttachmentPage.aspx?accid={accountRecordControl:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private AttachmentRecord[] _DataSource = null;
        public  AttachmentRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public WKCRM.UI.IThemeButton attachmentDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "attachmentDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton attachmentNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "attachmentNewButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal attachmentTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "attachmentTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox attachmentToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "attachmentToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label attachmentTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "attachmentTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal nameLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                attachmentTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                AttachmentRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (attachmentTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.attachmentRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public attachmentTableControlRow GetSelectedRecordControl()
        {
        attachmentTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public attachmentTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (attachmentTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.attachmentRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (attachmentTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.EditAccountPage.attachmentTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            attachmentTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (attachmentTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.attachmentRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public attachmentTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("attachmentTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                attachmentTableControlRow recControl = (attachmentTableControlRow)repItem.FindControl("attachmentTableControlRow");
                recList.Add(recControl);
            }

            return (attachmentTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.EditAccountPage.attachmentTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the communicationTableControlRow control on the EditAccountPage page.
// Do not modify this class. Instead override any method in communicationTableControlRow.
public class BasecommunicationTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecommunicationTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in communicationTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.communicationRecordRowEditButton.Click += new ImageClickEventHandler(communicationRecordRowEditButton_Click);
        }

        // To customize, override this method in communicationTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in communicationTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = CommunicationTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasecommunicationTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new CommunicationRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in communicationTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(CommunicationTable.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime2.Text = formattedValue;
            } else {  
                this.datetime2.Text = CommunicationTable.datetime0.Format(CommunicationTable.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime2.Text == null ||
                this.datetime2.Text.Trim().Length == 0) {
                this.datetime2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.employee_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(CommunicationTable.employee_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.employee_id.Text = formattedValue;
            } else {  
                this.employee_id.Text = CommunicationTable.employee_id.Format(CommunicationTable.employee_id.DefaultValue);
            }
                    
            if (this.employee_id.Text == null ||
                this.employee_id.Text.Trim().Length == 0) {
                this.employee_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(CommunicationTable.notes);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.CommunicationTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"notes\\\", \\\"Notes\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.notes.Text = formattedValue;
            } else {  
                this.notes.Text = CommunicationTable.notes.Format(CommunicationTable.notes.DefaultValue);
            }
                    
            if (this.notes.Text == null ||
                this.notes.Text.Trim().Length == 0) {
                this.notes.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in communicationTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // account in accountRecordControl is One To Many to communicationTableControl.
                    
            // Setup the parent id in the record.
            accountRecordControl recaccountRecordControl = (accountRecordControl)this.Page.FindControlRecursively("accountRecordControl");
            if (recaccountRecordControl != null && recaccountRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                recaccountRecordControl.LoadData();
            }
            if (recaccountRecordControl == null || recaccountRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.account_id = recaccountRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in communicationTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in communicationTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((communicationTableControl)MiscUtils.GetParentControlObject(this, "communicationTableControl")).DataChanged = true;
                ((communicationTableControl)MiscUtils.GetParentControlObject(this, "communicationTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in communicationTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in communicationTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in communicationTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            CommunicationTable.DeleteRecord(pk);

          
            ((communicationTableControl)MiscUtils.GetParentControlObject(this, "communicationTableControl")).DataChanged = true;
            ((communicationTableControl)MiscUtils.GetParentControlObject(this, "communicationTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void communicationRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../communication/EditcommunicationPage.aspx?Communication={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecommunicationTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasecommunicationTableControlRow_Rec"] = value;
            }
        }
        
        private CommunicationRecord _DataSource;
        public CommunicationRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.ImageButton communicationRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "communicationRecordRowEditButton");
            }
        }
           
        public System.Web.UI.WebControls.Literal datetime2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime2");
            }
        }
           
        public System.Web.UI.WebControls.Literal employee_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal notes {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notes");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            CommunicationRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public CommunicationRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return CommunicationTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the communicationTableControl control on the EditAccountPage page.
// Do not modify this class. Instead override any method in communicationTableControl.
public class BasecommunicationTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasecommunicationTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        
            this.datetimeLabel1.Click += new EventHandler(datetimeLabel1_Click);

            // Setup the button events.
        
            this.communicationNewButton.Button.Click += new EventHandler(communicationNewButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(CommunicationTable.datetime0, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (CommunicationRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.CommunicationRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = CommunicationTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (CommunicationRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.CommunicationRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (communicationTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (CommunicationRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.CommunicationRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = CommunicationTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("communicationTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                communicationTableControlRow recControl = (communicationTableControlRow)(repItem.FindControl("communicationTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(CommunicationTable.employee_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for communicationTableControl pagination.
        

            // Bind the pagination labels.
        
            this.communicationTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (communicationTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            CommunicationTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        accountRecordControl parentRecordControl = (accountRecordControl)(this.Page.FindControlRecursively("accountRecordControl"));
            AccountRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(CommunicationTable.account_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("communicationTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    communicationTableControlRow recControl = (communicationTableControlRow)(repItem.FindControl("communicationTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        CommunicationRecord rec = new CommunicationRecord();
        
                        if (recControl.datetime2.Text != "") {
                            rec.Parse(recControl.datetime2.Text, CommunicationTable.datetime0);
                        }
                        if (recControl.employee_id.Text != "") {
                            rec.Parse(recControl.employee_id.Text, CommunicationTable.employee_id);
                        }
                        if (recControl.notes.Text != "") {
                            rec.Parse(recControl.notes.Text, CommunicationTable.notes);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new CommunicationRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (CommunicationRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.CommunicationRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(communicationTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(communicationTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["communicationTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["communicationTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void datetimeLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(CommunicationTable.datetime0);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(CommunicationTable.datetime0, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void communicationNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../communication/AddcommunicationPage.aspx?accid={accountRecordControl:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private CommunicationRecord[] _DataSource = null;
        public  CommunicationRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public WKCRM.UI.IThemeButton communicationNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "communicationNewButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal communicationTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "communicationTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label communicationTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "communicationTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton datetimeLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal notesLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notesLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                communicationTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                CommunicationRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public communicationTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public communicationTableControlRow[] GetSelectedRecordControls()
        {
        
            return (communicationTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.EditAccountPage.communicationTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            communicationTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (communicationTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public communicationTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("communicationTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                communicationTableControlRow recControl = (communicationTableControlRow)repItem.FindControl("communicationTableControlRow");
                recList.Add(recControl);
            }

            return (communicationTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.EditAccountPage.communicationTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the leadMeetingsTableControlRow control on the EditAccountPage page.
// Do not modify this class. Instead override any method in leadMeetingsTableControlRow.
public class BaseleadMeetingsTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseleadMeetingsTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in leadMeetingsTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.EmailButton.Click += new ImageClickEventHandler(EmailButton_Click);
            this.ImageButton.Click += new ImageClickEventHandler(ImageButton_Click);
            this.leadMeetingsRecordRowEditButton.Click += new ImageClickEventHandler(leadMeetingsRecordRowEditButton_Click);
        }

        // To customize, override this method in leadMeetingsTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in leadMeetingsTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = LeadMeetingsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseleadMeetingsTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new LeadMeetingsRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in leadMeetingsTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.address);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.LeadMeetingsTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"address\\\", \\\"Address\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.address1.Text = formattedValue;
            } else {  
                this.address1.Text = LeadMeetingsTable.address.Format(LeadMeetingsTable.address.DefaultValue);
            }
                    
            if (this.address1.Text == null ||
                this.address1.Text.Trim().Length == 0) {
                this.address1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime1.Text = formattedValue;
            } else {  
                this.datetime1.Text = LeadMeetingsTable.datetime0.Format(LeadMeetingsTable.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime1.Text == null ||
                this.datetime1.Text.Trim().Length == 0) {
                this.datetime1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.designer_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.designer_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.designer_id1.Text = formattedValue;
            } else {  
                this.designer_id1.Text = LeadMeetingsTable.designer_id.Format(LeadMeetingsTable.designer_id.DefaultValue);
            }
                    
            if (this.designer_id1.Text == null ||
                this.designer_id1.Text.Trim().Length == 0) {
                this.designer_id1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.onsiteSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.onsite, @"Showroom,Onsite");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.onsite.Text = formattedValue;
            } else {  
                this.onsite.Text = LeadMeetingsTable.onsite.Format(LeadMeetingsTable.onsite.DefaultValue, @"Showroom,Onsite");
            }
                    
            if (this.onsite.Text == null ||
                this.onsite.Text.Trim().Length == 0) {
                this.onsite.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // account in accountRecordControl is One To Many to leadMeetingsTableControl.
                    
            // Setup the parent id in the record.
            accountRecordControl recaccountRecordControl = (accountRecordControl)this.Page.FindControlRecursively("accountRecordControl");
            if (recaccountRecordControl != null && recaccountRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                recaccountRecordControl.LoadData();
            }
            if (recaccountRecordControl == null || recaccountRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.account_id = recaccountRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in leadMeetingsTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in leadMeetingsTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).DataChanged = true;
                ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            LeadMeetingsTable.DeleteRecord(pk);

          
            ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).DataChanged = true;
            ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void EmailButton_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void ImageButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../Reports/ShowReport.aspx?reportID=11&recordId={leadMeetingsTableControlRow:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for ImageButton
        public virtual void leadMeetingsRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../leadMeetings/EditleadMeetingsPage.aspx?LeadMeetings={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseleadMeetingsTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseleadMeetingsTableControlRow_Rec"] = value;
            }
        }
        
        private LeadMeetingsRecord _DataSource;
        public LeadMeetingsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal address1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address1");
            }
        }
           
        public System.Web.UI.WebControls.Literal datetime1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime1");
            }
        }
           
        public System.Web.UI.WebControls.Literal designer_id1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_id1");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton EmailButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EmailButton");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton ImageButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "ImageButton");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton leadMeetingsRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsRecordRowEditButton");
            }
        }
           
        public System.Web.UI.WebControls.Literal onsite {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "onsite");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            LeadMeetingsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public LeadMeetingsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return LeadMeetingsTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the leadMeetingsTableControl control on the EditAccountPage page.
// Do not modify this class. Instead override any method in leadMeetingsTableControl.
public class BaseleadMeetingsTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseleadMeetingsTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        
            this.datetimeLabel.Click += new EventHandler(datetimeLabel_Click);

            // Setup the button events.
        
            this.leadMeetingsNewButton.Button.Click += new EventHandler(leadMeetingsNewButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(LeadMeetingsTable.datetime0, OrderByItem.OrderDir.Desc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (LeadMeetingsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = LeadMeetingsTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (LeadMeetingsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (leadMeetingsTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (LeadMeetingsRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = LeadMeetingsTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("leadMeetingsTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                leadMeetingsTableControlRow recControl = (leadMeetingsTableControlRow)(repItem.FindControl("leadMeetingsTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(LeadMeetingsTable.designer_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for leadMeetingsTableControl pagination.
        

            // Bind the pagination labels.
        
            this.leadMeetingsTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (leadMeetingsTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            LeadMeetingsTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        accountRecordControl parentRecordControl = (accountRecordControl)(this.Page.FindControlRecursively("accountRecordControl"));
            AccountRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(LeadMeetingsTable.account_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("leadMeetingsTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    leadMeetingsTableControlRow recControl = (leadMeetingsTableControlRow)(repItem.FindControl("leadMeetingsTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        LeadMeetingsRecord rec = new LeadMeetingsRecord();
        
                        if (recControl.address1.Text != "") {
                            rec.Parse(recControl.address1.Text, LeadMeetingsTable.address);
                        }
                        if (recControl.datetime1.Text != "") {
                            rec.Parse(recControl.datetime1.Text, LeadMeetingsTable.datetime0);
                        }
                        if (recControl.designer_id1.Text != "") {
                            rec.Parse(recControl.designer_id1.Text, LeadMeetingsTable.designer_id);
                        }
                        if (recControl.onsite.Text != "") {
                            rec.Parse(recControl.onsite.Text, LeadMeetingsTable.onsite);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new LeadMeetingsRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (LeadMeetingsRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(leadMeetingsTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(leadMeetingsTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["leadMeetingsTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["leadMeetingsTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void datetimeLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(LeadMeetingsTable.datetime0);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(LeadMeetingsTable.datetime0, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void leadMeetingsNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../leadMeetings/AddLeadMeeting.aspx?accid={accountRecordControl:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private LeadMeetingsRecord[] _DataSource = null;
        public  LeadMeetingsRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal addressLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel1");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton datetimeLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel1");
            }
        }
        
        public WKCRM.UI.IThemeButton leadMeetingsNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsNewButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal leadMeetingsTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label leadMeetingsTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal onsiteLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "onsiteLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                leadMeetingsTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                LeadMeetingsRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public leadMeetingsTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public leadMeetingsTableControlRow[] GetSelectedRecordControls()
        {
        
            return (leadMeetingsTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.EditAccountPage.leadMeetingsTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            leadMeetingsTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (leadMeetingsTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public leadMeetingsTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("leadMeetingsTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                leadMeetingsTableControlRow recControl = (leadMeetingsTableControlRow)repItem.FindControl("leadMeetingsTableControlRow");
                recList.Add(recControl);
            }

            return (leadMeetingsTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.EditAccountPage.leadMeetingsTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the accountRecordControl control on the EditAccountPage page.
// Do not modify this class. Instead override any method in accountRecordControl.
public class BaseaccountRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseaccountRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in accountRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in accountRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in accountRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = AccountTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new AccountRecord();
                return;
            }

            // Retrieve the record from the database.
            AccountRecord[] recList = AccountTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = AccountTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in accountRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.account_status_idSpecified) {
                this.Populateaccount_status_idDropDownList(this.DataSource.account_status_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateaccount_status_idDropDownList(AccountTable.account_status_id.DefaultValue, 100);
                } else {
                this.Populateaccount_status_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.address);
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = AccountTable.address.Format(AccountTable.address.DefaultValue);
            }
                    
            if (this.DataSource.benchtop_typeSpecified) {
                this.Populatebenchtop_typeDropDownList(this.DataSource.benchtop_type.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatebenchtop_typeDropDownList(AccountTable.benchtop_type.DefaultValue, 100);
                } else {
                this.Populatebenchtop_typeDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.builders_nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.builders_name);
                this.builders_name.Text = formattedValue;
            } else {  
                this.builders_name.Text = AccountTable.builders_name.Format(AccountTable.builders_name.DefaultValue);
            }
                    
            if (this.DataSource.builders_phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.builders_phone);
                this.builders_phone.Text = formattedValue;
            } else {  
                this.builders_phone.Text = AccountTable.builders_phone.Format(AccountTable.builders_phone.DefaultValue);
            }
                    
            if (this.DataSource.business_phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.business_phone);
                this.business_phone.Text = formattedValue;
            } else {  
                this.business_phone.Text = AccountTable.business_phone.Format(AccountTable.business_phone.DefaultValue);
            }
                    
            if (this.DataSource.business_phone2Specified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.business_phone2);
                this.business_phone2.Text = formattedValue;
            } else {  
                this.business_phone2.Text = AccountTable.business_phone2.Format(AccountTable.business_phone2.DefaultValue);
            }
                    
            if (this.DataSource.contact_source_idSpecified) {
                this.Populatecontact_source_idDropDownList(this.DataSource.contact_source_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatecontact_source_idDropDownList(AccountTable.contact_source_id.DefaultValue, 100);
                } else {
                this.Populatecontact_source_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.date_createdSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.date_created);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.CreatedDate.Text = formattedValue;
            } else {  
                this.CreatedDate.Text = AccountTable.date_created.Format(AccountTable.date_created.DefaultValue);
            }
                    
            this.date_died.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.date_died.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.date_diedSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.date_died);
                this.date_died.Text = formattedValue;
            } else {  
                this.date_died.Text = AccountTable.date_died.Format(AccountTable.date_died.DefaultValue);
            }
                    
            if (this.DataSource.designer_idSpecified) {
                this.Populatedesigner_idDropDownList(this.DataSource.designer_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatedesigner_idDropDownList(AccountTable.designer_id.DefaultValue, 100);
                } else {
                this.Populatedesigner_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.door_typeSpecified) {
                this.Populatedoor_typeDropDownList(this.DataSource.door_type.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatedoor_typeDropDownList(AccountTable.door_type.DefaultValue, 100);
                } else {
                this.Populatedoor_typeDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.emailSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.email);
                this.email.Text = formattedValue;
            } else {  
                this.email.Text = AccountTable.email.Format(AccountTable.email.DefaultValue);
            }
                    
            if (this.DataSource.email2Specified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.email2);
                this.email2.Text = formattedValue;
            } else {  
                this.email2.Text = AccountTable.email2.Format(AccountTable.email2.DefaultValue);
            }
                    
            if (this.DataSource.faxSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.fax);
                this.fax.Text = formattedValue;
            } else {  
                this.fax.Text = AccountTable.fax.Format(AccountTable.fax.DefaultValue);
            }
                    
            if (this.DataSource.has_been_revivedSpecified) {
                this.has_been_revived.Checked = this.DataSource.has_been_revived;
            } else {
                if (!this.DataSource.IsCreated) {
                    this.has_been_revived.Checked = AccountTable.has_been_revived.ParseValue(AccountTable.has_been_revived.DefaultValue).ToBoolean();
                }
            }
                    
            if (this.DataSource.home_phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.home_phone);
                this.home_phone.Text = formattedValue;
            } else {  
                this.home_phone.Text = AccountTable.home_phone.Format(AccountTable.home_phone.DefaultValue);
            }
                    
            this.homeWarrantyDate.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.homeWarrantyDate.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.home_warranty_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.home_warranty_date);
                this.homeWarrantyDate.Text = formattedValue;
            } else {  
                this.homeWarrantyDate.Text = AccountTable.home_warranty_date.Format(AccountTable.home_warranty_date.DefaultValue);
            }
                    
            if (this.DataSource.home_warranty_policySpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.home_warranty_policy);
                this.homeWarrantyPolicy.Text = formattedValue;
            } else {  
                this.homeWarrantyPolicy.Text = AccountTable.home_warranty_policy.Format(AccountTable.home_warranty_policy.DefaultValue);
            }
                    
            if (this.DataSource.LastNameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.LastName);
                this.LastName.Text = formattedValue;
            } else {  
                this.LastName.Text = AccountTable.LastName.Format(AccountTable.LastName.DefaultValue);
            }
                    
            if (this.DataSource.left_reasonSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.left_reason);
                this.left_reason.Text = formattedValue;
            } else {  
                this.left_reason.Text = AccountTable.left_reason.Format(AccountTable.left_reason.DefaultValue);
            }
                    
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.mobile);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = AccountTable.mobile.Format(AccountTable.mobile.DefaultValue);
            }
                    
            if (this.DataSource.mobile2Specified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.mobile2);
                this.mobile2.Text = formattedValue;
            } else {  
                this.mobile2.Text = AccountTable.mobile2.Format(AccountTable.mobile2.DefaultValue);
            }
                    
            if (this.DataSource.FirstNameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.FirstName);
                this.Name.Text = formattedValue;
            } else {  
                this.Name.Text = AccountTable.FirstName.Format(AccountTable.FirstName.DefaultValue);
            }
                    
            this.next_review_date.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.next_review_date.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.next_review_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.next_review_date);
                this.next_review_date.Text = formattedValue;
            } else {  
                this.next_review_date.Text = AccountTable.next_review_date.Format(AccountTable.next_review_date.DefaultValue);
            }
                    
            if (this.DataSource.opportunity_source_idSpecified) {
                this.Populateopportunity_source_idDropDownList(this.DataSource.opportunity_source_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateopportunity_source_idDropDownList(AccountTable.opportunity_source_id.DefaultValue, 100);
                } else {
                this.Populateopportunity_source_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.opportunity_detailsSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.opportunity_details);
                this.OpportunityDetails.Text = formattedValue;
            } else {  
                this.OpportunityDetails.Text = AccountTable.opportunity_details.Format(AccountTable.opportunity_details.DefaultValue);
            }
                    
            if (this.DataSource.postal_addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.postal_address);
                this.postal_address.Text = formattedValue;
            } else {  
                this.postal_address.Text = AccountTable.postal_address.Format(AccountTable.postal_address.DefaultValue);
            }
                    
            if (this.DataSource.postcodeSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.postcode);
                this.postcode.Text = formattedValue;
            } else {  
                this.postcode.Text = AccountTable.postcode.Format(AccountTable.postcode.DefaultValue);
            }
                    
            if (this.DataSource.price_rangeSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.price_range);
                this.price_range.Text = formattedValue;
            } else {  
                this.price_range.Text = AccountTable.price_range.Format(AccountTable.price_range.DefaultValue);
            }
                    
            if (this.DataSource.revived_reasonSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.revived_reason);
                this.revived_reason.Text = formattedValue;
            } else {  
                this.revived_reason.Text = AccountTable.revived_reason.Format(AccountTable.revived_reason.DefaultValue);
            }
                    
            this.sale_date.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.sale_date.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.sale_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.sale_date);
                this.sale_date.Text = formattedValue;
            } else {  
                this.sale_date.Text = AccountTable.sale_date.Format(AccountTable.sale_date.DefaultValue);
            }
                    
            if (this.DataSource.time_frameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.time_frame);
                this.time_frame.Text = formattedValue;
            } else {  
                this.time_frame.Text = AccountTable.time_frame.Format(AccountTable.time_frame.DefaultValue);
            }
                    
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in accountRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in accountRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in accountRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in accountRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.account_status_id), AccountTable.account_status_id);
                  
            this.DataSource.Parse(this.address.Text, AccountTable.address);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.benchtop_type), AccountTable.benchtop_type);
                  
            this.DataSource.Parse(this.builders_name.Text, AccountTable.builders_name);
                          
            this.DataSource.Parse(this.builders_phone.Text, AccountTable.builders_phone);
                          
            this.DataSource.Parse(this.business_phone.Text, AccountTable.business_phone);
                          
            this.DataSource.Parse(this.business_phone2.Text, AccountTable.business_phone2);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.contact_source_id), AccountTable.contact_source_id);
                  
            this.DataSource.Parse(this.date_died.Text, AccountTable.date_died);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.designer_id), AccountTable.designer_id);
                  
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.door_type), AccountTable.door_type);
                  
            this.DataSource.Parse(this.email.Text, AccountTable.email);
                          
            this.DataSource.Parse(this.email2.Text, AccountTable.email2);
                          
            this.DataSource.Parse(this.fax.Text, AccountTable.fax);
                          
            this.DataSource.has_been_revived = this.has_been_revived.Checked;
                    
            this.DataSource.Parse(this.home_phone.Text, AccountTable.home_phone);
                          
            this.DataSource.Parse(this.homeWarrantyDate.Text, AccountTable.home_warranty_date);
                          
            this.DataSource.Parse(this.homeWarrantyPolicy.Text, AccountTable.home_warranty_policy);
                          
            this.DataSource.Parse(this.LastName.Text, AccountTable.LastName);
                          
            this.DataSource.Parse(this.left_reason.Text, AccountTable.left_reason);
                          
            this.DataSource.Parse(this.mobile.Text, AccountTable.mobile);
                          
            this.DataSource.Parse(this.mobile2.Text, AccountTable.mobile2);
                          
            this.DataSource.Parse(this.Name.Text, AccountTable.FirstName);
                          
            this.DataSource.Parse(this.next_review_date.Text, AccountTable.next_review_date);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.opportunity_source_id), AccountTable.opportunity_source_id);
                  
            this.DataSource.Parse(this.OpportunityDetails.Text, AccountTable.opportunity_details);
                          
            this.DataSource.Parse(this.postal_address.Text, AccountTable.postal_address);
                          
            this.DataSource.Parse(this.postcode.Text, AccountTable.postcode);
                          
            this.DataSource.Parse(this.price_range.Text, AccountTable.price_range);
                          
            this.DataSource.Parse(this.revived_reason.Text, AccountTable.revived_reason);
                          
            this.DataSource.Parse(this.sale_date.Text, AccountTable.sale_date);
                          
            this.DataSource.Parse(this.time_frame.Text, AccountTable.time_frame);
                          
        }

        //  To customize, override this method in accountRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            AccountTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["Account"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "Account"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(AccountTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(AccountTable.id0).ToString());
            } else {
                
                wc.iAND(AccountTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in accountRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            AccountTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_account_status_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_benchtop_typeDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_contact_source_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_designer_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_door_typeDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_opportunity_source_idDropDownList() {
            return new WhereClause();
        }
                
        // Fill the account_status_id list.
        protected virtual void Populateaccount_status_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_account_status_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(AccountStatusTable.name, OrderByItem.OrderDir.Asc);

                      this.account_status_id.Items.Clear();
            foreach (AccountStatusRecord itemValue in AccountStatusTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(AccountStatusTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.account_status_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.account_status_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.account_status_id, AccountTable.account_status_id.Format(selectedValue))) {
                string fvalue = AccountTable.account_status_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.account_status_id.Items.Insert(0, item);
            }

                  
            this.account_status_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the benchtop_type list.
        protected virtual void Populatebenchtop_typeDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_benchtop_typeDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(AccountBenchtopTypeTable.AccountBenchtopType, OrderByItem.OrderDir.Asc);

                      this.benchtop_type.Items.Clear();
            foreach (AccountBenchtopTypeRecord itemValue in AccountBenchtopTypeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.AccountBenchtopTypeIdSpecified) {
                    cvalue = itemValue.AccountBenchtopTypeId.ToString();
                    fvalue = itemValue.Format(AccountBenchtopTypeTable.AccountBenchtopType);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.benchtop_type.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.benchtop_type, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.benchtop_type, AccountTable.benchtop_type.Format(selectedValue))) {
                string fvalue = AccountTable.benchtop_type.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.benchtop_type.Items.Insert(0, item);
            }

                  
            this.benchtop_type.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the contact_source_id list.
        protected virtual void Populatecontact_source_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_contact_source_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(ContactSourceTable.name, OrderByItem.OrderDir.Asc);

                      this.contact_source_id.Items.Clear();
            foreach (ContactSourceRecord itemValue in ContactSourceTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(ContactSourceTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.contact_source_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.contact_source_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.contact_source_id, AccountTable.contact_source_id.Format(selectedValue))) {
                string fvalue = AccountTable.contact_source_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.contact_source_id.Items.Insert(0, item);
            }

                  
            this.contact_source_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the designer_id list.
        protected virtual void Populatedesigner_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_designer_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

                      this.designer_id.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.designer_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.designer_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.designer_id, AccountTable.designer_id.Format(selectedValue))) {
                string fvalue = AccountTable.designer_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.designer_id.Items.Insert(0, item);
            }

                  
            this.designer_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the door_type list.
        protected virtual void Populatedoor_typeDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_door_typeDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(AccountDoorTypeTable.AccountDoorType, OrderByItem.OrderDir.Asc);

                      this.door_type.Items.Clear();
            foreach (AccountDoorTypeRecord itemValue in AccountDoorTypeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.AccountDoorTypeIdSpecified) {
                    cvalue = itemValue.AccountDoorTypeId.ToString();
                    fvalue = itemValue.Format(AccountDoorTypeTable.AccountDoorType);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.door_type.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.door_type, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.door_type, AccountTable.door_type.Format(selectedValue))) {
                string fvalue = AccountTable.door_type.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.door_type.Items.Insert(0, item);
            }

                  
            this.door_type.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the opportunity_source_id list.
        protected virtual void Populateopportunity_source_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_opportunity_source_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(OpportunitySourceTable.name, OrderByItem.OrderDir.Asc);

                      this.opportunity_source_id.Items.Clear();
            foreach (OpportunitySourceRecord itemValue in OpportunitySourceTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(OpportunitySourceTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.opportunity_source_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.opportunity_source_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.opportunity_source_id, AccountTable.opportunity_source_id.Format(selectedValue))) {
                string fvalue = AccountTable.opportunity_source_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.opportunity_source_id.Items.Insert(0, item);
            }

                  
            this.opportunity_source_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseaccountRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseaccountRecordControl_Rec"] = value;
            }
        }
        
        private AccountRecord _DataSource;
        public AccountRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.DropDownList account_status_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_status_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal account_status_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_status_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image accountDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "accountDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal accountDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "accountDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.TextBox address {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList benchtop_type {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "benchtop_type");
            }
        }
        
        public System.Web.UI.WebControls.Literal benchtop_typeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "benchtop_typeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox builders_name {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "builders_name");
            }
        }
        
        public System.Web.UI.WebControls.Literal builders_nameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "builders_nameLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox builders_phone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "builders_phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal builders_phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "builders_phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox business_phone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "business_phone");
            }
        }
           
        public System.Web.UI.WebControls.TextBox business_phone2 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "business_phone2");
            }
        }
        
        public System.Web.UI.WebControls.Literal business_phone2Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "business_phone2Label");
            }
        }
        
        public System.Web.UI.WebControls.Literal business_phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "business_phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList contact_source_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contact_source_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal contact_source_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contact_source_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label CreatedDate {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "CreatedDate");
            }
        }
           
        public System.Web.UI.WebControls.TextBox date_died {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_died");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList designer_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList door_type {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "door_type");
            }
        }
        
        public System.Web.UI.WebControls.Literal door_typeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "door_typeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox email {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email");
            }
        }
           
        public System.Web.UI.WebControls.TextBox email2 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email2");
            }
        }
        
        public System.Web.UI.WebControls.Literal emailLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "emailLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox fax {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "fax");
            }
        }
        
        public System.Web.UI.WebControls.Literal faxLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "faxLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox has_been_revived {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "has_been_revived");
            }
        }
        
        public System.Web.UI.WebControls.Literal has_been_revivedLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "has_been_revivedLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox home_phone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal home_phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox homeWarrantyDate {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "homeWarrantyDate");
            }
        }
           
        public System.Web.UI.WebControls.TextBox homeWarrantyPolicy {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "homeWarrantyPolicy");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.Label Label1 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label1");
            }
        }
        
        public System.Web.UI.WebControls.Label Label2 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label2");
            }
        }
        
        public System.Web.UI.WebControls.Label Label3 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label3");
            }
        }
        
        public System.Web.UI.WebControls.Label Label4 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label4");
            }
        }
        
        public System.Web.UI.WebControls.Label Label5 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label5");
            }
        }
        
        public System.Web.UI.WebControls.Label Label6 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label6");
            }
        }
        
        public System.Web.UI.WebControls.Label Label7 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label7");
            }
        }
           
        public System.Web.UI.WebControls.TextBox LastName {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "LastName");
            }
        }
           
        public System.Web.UI.WebControls.TextBox left_reason {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "left_reason");
            }
        }
        
        public System.Web.UI.WebControls.Literal left_reasonLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "left_reasonLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox mobile {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
           
        public System.Web.UI.WebControls.TextBox mobile2 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile2");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobile2Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile2Label");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox Name {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Name");
            }
        }
        
        public System.Web.UI.WebControls.Literal NameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "NameLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox next_review_date {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "next_review_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal next_review_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "next_review_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList opportunity_source_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunity_source_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox OpportunityDetails {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OpportunityDetails");
            }
        }
           
        public System.Web.UI.WebControls.TextBox postal_address {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "postal_address");
            }
        }
        
        public System.Web.UI.WebControls.Literal postal_addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "postal_addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox postcode {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "postcode");
            }
        }
        
        public System.Web.UI.WebControls.Literal postcodeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "postcodeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox price_range {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "price_range");
            }
        }
        
        public System.Web.UI.WebControls.Literal price_rangeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "price_rangeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox revived_reason {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "revived_reason");
            }
        }
        
        public System.Web.UI.WebControls.Literal revived_reasonLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "revived_reasonLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox sale_date {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "sale_date");
            }
        }
        
        public System.Web.UI.WebControls.Label scheduleLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "scheduleLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label SuburbLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "SuburbLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox time_frame {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "time_frame");
            }
        }
        
        public System.Web.UI.WebControls.Literal time_frameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "time_frameLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            AccountRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public AccountRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return AccountTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  