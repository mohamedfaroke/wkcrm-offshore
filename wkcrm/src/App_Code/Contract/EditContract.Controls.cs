﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// EditContract.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.EditContract
{
  

#region "Section 1: Place your customizations here."

    
public class contractMainRecordControl : BasecontractMainRecordControl
{
      
        // The BasecontractMainRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            accountLabel.Text = this.DataSource.Format(ContractMainTable.account_id);
            
            second_check_measure_feeLabel.Text = MyContract.SetSecondCheckMeasureFeeLabel();
            if (this.DataSource.second_check_measure_fee > 0)
                second_check_measure_fee.Checked = true;

            difficult_delivery_feeLabel.Text = MyContract.SetDifficultDeliveryFeeLabel();
            if (this.DataSource.difficult_delivery_fee > 0)
                difficult_delivery_fee.Checked = true;

            dual_finishes_feeLabel.Text = MyContract.SetDualFinishesFeeLabel();
            if (this.DataSource.dual_finishes_fee > 0)
                dual_finishes_fee.Checked = true;

            highrise_feeLabel.Text = MyContract.SetHighRiseFeeLabel();
            if (this.DataSource.highrise_fee > 0)
                highrise_fee.Checked = true;

            dual_finishes_lessthan_feeLabel.Text = MyContract.SetDualFinishesLessThanFeeLabel();
            if (this.DataSource.dual_finishes_lessthan_fee > 0)
                dual_finishes_lessthan_fee.Checked = true;

            Label2.Text = MyContract.SetKickboardsAfterTimberFeeLabel();
            if (this.DataSource.kickboardsAfterTimber_fee > 0)
                KickboardAfterTimberFee.Checked = true;

            Label3.Text = MyContract.SetKickboardsWasherAfterTimberFeeLabel();
            if (this.DataSource.kickboardsWasherAfterTimber_fee > 0)
            {
                KickboardsWasherAfterTimberFee.Checked = true;
            }

            //DropDownList deliveryFeeList = this.Page.FindControlRecursively("DeliveryFeeList") as DropDownList;
            //if (deliveryFeeList != null)
            //{
            //    MyContract.PopulateDeliveryFeeDropdown(this.DataSource.non_metro_delivery_fee.ToString("0.00"),deliveryFeeList);
            //}

            if(this.DataSource.contract_type_id == Globals.ACCOUNTPHASE_PROJ)
            {
                designer_fee.Enabled = false;
                consultation_fee.Enabled = false;
            }
        }
    }

   

    public override void GetUIData()
    {
        this.DataSource.Parse(this.consultation_fee.Text, ContractMainTable.consultation_fee);
        this.DataSource.Parse(this.designer_fee.Text, ContractMainTable.designer_fee);
        this.DataSource.Parse(this.estimated_completion_date.Text, ContractMainTable.estimated_completion_date);
        this.DataSource.Parse(this.home_insurance_policy.Text, ContractMainTable.home_insurance_policy);
        this.DataSource.home_insurance_sent = this.home_insurance_sent.Checked;
        this.DataSource.Parse(DateTime.Now, ContractMainTable.date_modified);
        this.DataSource.Parse(this.Page.SystemUtils.GetUserID(), ContractMainTable.modified_by);

        if (difficult_delivery_fee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.difficultDeliveryFee, ContractMainTable.difficult_delivery_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.difficult_delivery_fee);

        if (dual_finishes_fee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.dualFinishes, ContractMainTable.dual_finishes_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.dual_finishes_fee);

        if (dual_finishes_lessthan_fee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.dualFinishLessFee, ContractMainTable.dual_finishes_lessthan_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.dual_finishes_lessthan_fee);

        if (highrise_fee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.highRise, ContractMainTable.highrise_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.highrise_fee);

        if (second_check_measure_fee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.secondCheckMeasureFee, ContractMainTable.second_check_measure_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.second_check_measure_fee);

        if (KickboardAfterTimberFee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.kickboardsAfterTimber, ContractMainTable.kickboardsAfterTimber_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.kickboardsAfterTimber_fee);

        if (KickboardsWasherAfterTimberFee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.kickboardsWasherAfterTimber, ContractMainTable.kickboardsWasherAfterTimber_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.kickboardsWasherAfterTimber_fee);


        //DropDownList DeliveryFeeList = this.Page.FindControlRecursively("DeliveryFeeList") as DropDownList;
        //if (DeliveryFeeList != null)
        //{
        //    this.DataSource.Parse(DeliveryFeeList.SelectedValue, ContractMainTable.non_metro_delivery_fee);
        //}
        //else
        //    this.DataSource.Parse(0, ContractMainTable.non_metro_delivery_fee);

    }

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the contractMainRecordControl control on the EditContract page.
// Do not modify this class. Instead override any method in contractMainRecordControl.
public class BasecontractMainRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecontractMainRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in contractMainRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in contractMainRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in contractMainRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = ContractMainTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new ContractMainRecord();
                return;
            }

            // Retrieve the record from the database.
            ContractMainRecord[] recList = ContractMainTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = ContractMainTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in contractMainRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.consultation_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.consultation_fee);
                this.consultation_fee.Text = formattedValue;
            } else {  
                this.consultation_fee.Text = ContractMainTable.consultation_fee.Format(ContractMainTable.consultation_fee.DefaultValue);
            }
                    
            if (this.DataSource.designer_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.designer_fee);
                this.designer_fee.Text = formattedValue;
            } else {  
                this.designer_fee.Text = ContractMainTable.designer_fee.Format(ContractMainTable.designer_fee.DefaultValue);
            }
                    
            if (this.DataSource.difficult_delivery_feeSpecified) {
                if (this.DataSource.difficult_delivery_fee.ToString() == "Yes") {
                    this.difficult_delivery_fee.Checked = true;
                } else {
                    this.difficult_delivery_fee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.difficult_delivery_fee.Checked = ContractMainTable.difficult_delivery_fee.ParseValue(ContractMainTable.difficult_delivery_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.dual_finishes_feeSpecified) {
                if (this.DataSource.dual_finishes_fee.ToString() == "Yes") {
                    this.dual_finishes_fee.Checked = true;
                } else {
                    this.dual_finishes_fee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.dual_finishes_fee.Checked = ContractMainTable.dual_finishes_fee.ParseValue(ContractMainTable.dual_finishes_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.dual_finishes_lessthan_feeSpecified) {
                if (this.DataSource.dual_finishes_lessthan_fee.ToString() == "Yes") {
                    this.dual_finishes_lessthan_fee.Checked = true;
                } else {
                    this.dual_finishes_lessthan_fee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.dual_finishes_lessthan_fee.Checked = ContractMainTable.dual_finishes_lessthan_fee.ParseValue(ContractMainTable.dual_finishes_lessthan_fee.DefaultValue).ToBoolean();
                }
            }
                        
            this.estimated_completion_date.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.estimated_completion_date.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.estimated_completion_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.estimated_completion_date);
                this.estimated_completion_date.Text = formattedValue;
            } else {  
                this.estimated_completion_date.Text = ContractMainTable.estimated_completion_date.Format(ContractMainTable.estimated_completion_date.DefaultValue);
            }
                    
            if (this.DataSource.highrise_feeSpecified) {
                if (this.DataSource.highrise_fee.ToString() == "Yes") {
                    this.highrise_fee.Checked = true;
                } else {
                    this.highrise_fee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.highrise_fee.Checked = ContractMainTable.highrise_fee.ParseValue(ContractMainTable.highrise_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.home_insurance_policySpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.home_insurance_policy);
                this.home_insurance_policy.Text = formattedValue;
            } else {  
                this.home_insurance_policy.Text = ContractMainTable.home_insurance_policy.Format(ContractMainTable.home_insurance_policy.DefaultValue);
            }
                    
            if (this.DataSource.home_insurance_sentSpecified) {
                this.home_insurance_sent.Checked = this.DataSource.home_insurance_sent;
            } else {
                if (!this.DataSource.IsCreated) {
                    this.home_insurance_sent.Checked = ContractMainTable.home_insurance_sent.ParseValue(ContractMainTable.home_insurance_sent.DefaultValue).ToBoolean();
                }
            }
                    
            if (this.DataSource.kickboardsAfterTimber_feeSpecified) {
                if (this.DataSource.kickboardsAfterTimber_fee.ToString() == "") {
                    this.KickboardAfterTimberFee.Checked = true;
                } else {
                    this.KickboardAfterTimberFee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.KickboardAfterTimberFee.Checked = ContractMainTable.kickboardsAfterTimber_fee.ParseValue(ContractMainTable.kickboardsAfterTimber_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.kickboardsWasherAfterTimber_feeSpecified) {
                if (this.DataSource.kickboardsWasherAfterTimber_fee.ToString() == "") {
                    this.KickboardsWasherAfterTimberFee.Checked = true;
                } else {
                    this.KickboardsWasherAfterTimberFee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.KickboardsWasherAfterTimberFee.Checked = ContractMainTable.kickboardsWasherAfterTimber_fee.ParseValue(ContractMainTable.kickboardsWasherAfterTimber_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.second_check_measure_feeSpecified) {
                if (this.DataSource.second_check_measure_fee.ToString() == "Yes") {
                    this.second_check_measure_fee.Checked = true;
                } else {
                    this.second_check_measure_fee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.second_check_measure_fee.Checked = ContractMainTable.second_check_measure_fee.ParseValue(ContractMainTable.second_check_measure_fee.DefaultValue).ToBoolean();
                }
            }
                        
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in contractMainRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in contractMainRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.consultation_fee.Text, ContractMainTable.consultation_fee);
                          
            this.DataSource.Parse(this.designer_fee.Text, ContractMainTable.designer_fee);
                          
            if (this.difficult_delivery_fee.Checked) {
                this.DataSource.Parse("Yes", ContractMainTable.difficult_delivery_fee);
            } else {
                this.DataSource.Parse("No", ContractMainTable.difficult_delivery_fee);
            }
                    
            if (this.dual_finishes_fee.Checked) {
                this.DataSource.Parse("Yes", ContractMainTable.dual_finishes_fee);
            } else {
                this.DataSource.Parse("No", ContractMainTable.dual_finishes_fee);
            }
                    
            if (this.dual_finishes_lessthan_fee.Checked) {
                this.DataSource.Parse("Yes", ContractMainTable.dual_finishes_lessthan_fee);
            } else {
                this.DataSource.Parse("No", ContractMainTable.dual_finishes_lessthan_fee);
            }
                    
            this.DataSource.Parse(this.estimated_completion_date.Text, ContractMainTable.estimated_completion_date);
                          
            if (this.highrise_fee.Checked) {
                this.DataSource.Parse("Yes", ContractMainTable.highrise_fee);
            } else {
                this.DataSource.Parse("No", ContractMainTable.highrise_fee);
            }
                    
            this.DataSource.Parse(this.home_insurance_policy.Text, ContractMainTable.home_insurance_policy);
                          
            this.DataSource.home_insurance_sent = this.home_insurance_sent.Checked;
                    
            if (this.KickboardAfterTimberFee.Checked) {
                this.DataSource.Parse("", ContractMainTable.kickboardsAfterTimber_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.kickboardsAfterTimber_fee);
            }
                    
            if (this.KickboardsWasherAfterTimberFee.Checked) {
                this.DataSource.Parse("", ContractMainTable.kickboardsWasherAfterTimber_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.kickboardsWasherAfterTimber_fee);
            }
                    
            if (this.second_check_measure_fee.Checked) {
                this.DataSource.Parse("Yes", ContractMainTable.second_check_measure_fee);
            } else {
                this.DataSource.Parse("No", ContractMainTable.second_check_measure_fee);
            }
                    
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            ContractMainTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["Contract"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "Contract"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(ContractMainTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(ContractMainTable.id0).ToString());
            } else {
                
                wc.iAND(ContractMainTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in contractMainRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractMainTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecontractMainRecordControl_Rec"];
            }
            set {
                this.ViewState["BasecontractMainRecordControl_Rec"] = value;
            }
        }
        
        private ContractMainRecord _DataSource;
        public ContractMainRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Label accountLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "accountLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox consultation_fee {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "consultation_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal consultation_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "consultation_feeLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image contractMainDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractMainDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal contractMainDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractMainDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.TextBox designer_fee {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox difficult_delivery_fee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "difficult_delivery_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal difficult_delivery_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "difficult_delivery_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox dual_finishes_fee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dual_finishes_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal dual_finishes_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dual_finishes_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox dual_finishes_lessthan_fee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dual_finishes_lessthan_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal dual_finishes_lessthan_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dual_finishes_lessthan_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox estimated_completion_date {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "estimated_completion_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal estimated_completion_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "estimated_completion_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox highrise_fee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "highrise_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal highrise_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "highrise_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox home_insurance_policy {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_policy");
            }
        }
        
        public System.Web.UI.WebControls.Literal home_insurance_policyLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_policyLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox home_insurance_sent {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_sent");
            }
        }
        
        public System.Web.UI.WebControls.Literal home_insurance_sentLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_sentLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox KickboardAfterTimberFee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "KickboardAfterTimberFee");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox KickboardsWasherAfterTimberFee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "KickboardsWasherAfterTimberFee");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.Label Label2 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label2");
            }
        }
        
        public System.Web.UI.WebControls.Label Label3 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label3");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox second_check_measure_fee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "second_check_measure_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal second_check_measure_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "second_check_measure_feeLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            ContractMainRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public ContractMainRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return ContractMainTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  