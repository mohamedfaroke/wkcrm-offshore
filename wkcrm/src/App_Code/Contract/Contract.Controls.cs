﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// Contract.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI.Controls.Contract
{
  

#region "Section 1: Place your customizations here."

    
public class contractMainRecordControl : BasecontractMainRecordControl
{
      
        // The BasecontractMainRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

    public contractMainRecordControl()
    {
        this.PreRender += new EventHandler(contractMainRecordControl_PreRender);
    }

    void contractMainRecordControl_PreRender(object sender, EventArgs e)
    {
        string contractID = Globals.GetRecordIDFromXML(this.RecordUniqueId, "id");
        //Only visible when not in quote phase
        //viewContractButton.Visible = !Globals.IsQuotePhase(contractID);
        //viewContractLabel.Visible = viewContractButton.Visible;

        View_ContractItemTotalsRecord myRec = View_ContractItemTotalsView.GetRecord("contract_id=" + contractID);
        if (myRec != null)
        {
            this.contractTotal.Text = Globals.AddGST(myRec.TotalAmount).ToString("C");
        }
    }

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            string url = "./EditContract.aspx?Contract=" + this.DataSource.id0.ToString();
            editContract.Button.OnClientClick = "StandardWindowWidth('" + url + "',600)";
            View_ContractItemTotalsRecord myRec = View_ContractItemTotalsView.GetRecord("contract_id=" + this.DataSource.id0.ToString());
            if (myRec != null)
                this.contractTotal.Text = Globals.AddGST(myRec.TotalAmount).ToString("C");
        }
    }

    public override void editContract_Click(object sender, EventArgs args)
    {
        base.editContract_Click(sender, args);
        Label scriptHack = this.Page.FindControl("scriptHack") as Label;
        if (scriptHack != null)
            scriptHack.Text = "";
    }
}

  

public class contractDoorItemTableControl : BasecontractDoorItemTableControl
{
        // The BasecontractDoorItemTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The contractDoorItemTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class contractDoorItemTableControlRow : BasecontractDoorItemTableControlRow
{
      
        // The BasecontractDoorItemTableControlRow implements code for a ROW within the
        // the contractDoorItemTableControl table.  The BasecontractDoorItemTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of contractDoorItemTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            string contractID = this.Page.Request["Contract"];
            if (!Globals.IsQuotePhase(contractID))
            {
                doorRowEdit.Visible = false;
                contractDoorItemRecordRowSelection.Visible = false;
            }
            else
            {
                string url = "DoorSelection.aspx?Contract=" + this.DataSource.contract_id.ToString() + "&Door=" + this.DataSource.id0.ToString();
                string width = "400";
                RadWindowManager winManager = this.Page.FindControl("RadWindowManager1") as RadWindowManager;
                if (winManager != null)
                    width = winManager.Width.Value.ToString();
                string onClientClick = "EditWindow('" + url + "'," + width + ",'Doors')";
                doorRowEdit.Text = "<a href=\"javascript:" + onClientClick + "\"><img src=\"../Images/Icon_edit.gif\" border=0></a>";
            }
        }
    }

    public override void Delete()
    {
        if (!this.IsNewRecord)
        {
            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractDoorItemRecord rec = ContractDoorItemTable.GetRecord(pk, false);
            string contractID = rec.contract_id.ToString();
            base.Delete();
            if (!String.IsNullOrEmpty(contractID))
            {
                ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
                Globals.UpdateContract(contract, this.Page.SystemUtils.GetUserID());

                contractMainRecordControl cntrl = this.Page.FindControl("contractMainRecordControl") as contractMainRecordControl;
                if (cntrl != null)
                {
                    cntrl.ResetData = true;
                    cntrl.DataChanged = true;
                }
            }
        }
    }

}
public class contractBenchtopItemTableControl : BasecontractBenchtopItemTableControl
{
        // The BasecontractBenchtopItemTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The contractBenchtopItemTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class contractBenchtopItemTableControlRow : BasecontractBenchtopItemTableControlRow
{
      
        // The BasecontractBenchtopItemTableControlRow implements code for a ROW within the
        // the contractBenchtopItemTableControl table.  The BasecontractBenchtopItemTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of contractBenchtopItemTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            string contractID = this.Page.Request["Contract"];
            if (!Globals.IsQuotePhase(contractID))
            {
                benchRowEdit.Visible = false;
                contractBenchtopItemRecordRowSelection.Visible = false;
            }
            else
            {
                string url = "BenchtopSelection.aspx?Contract=" + this.DataSource.contract_id.ToString() + "&Bench=" + this.DataSource.id0.ToString();
                //We want the width to be always 600px for this kind of window
                string onClientClick = "EditWindow('" + url + "',600,'Benchtops')";
                benchRowEdit.Text = "<a href=\"javascript:" + onClientClick + "\"><img src=\"../Images/Icon_edit.gif\" border=0></a>";
            }
        }
    }

    public override void Delete()
    {
        if (!this.IsNewRecord)
        {
            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractBenchtopItemRecord rec = ContractBenchtopItemTable.GetRecord(pk, false);
            string contractID = rec.contract_id.ToString();
            //Delete all the benchtop option items, if any
            ContractBenchtopOptionItemTable.DeleteRecords("contract_benchtop_item=" + rec.id0.ToString());
            base.Delete();
            if (!String.IsNullOrEmpty(contractID))
            {
                ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
                Globals.UpdateContract(contract, this.Page.SystemUtils.GetUserID());

                contractMainRecordControl cntrl = this.Page.FindControl("contractMainRecordControl") as contractMainRecordControl;
                if (cntrl != null)
                {
                    cntrl.ResetData = true;
                    cntrl.DataChanged = true;
                }
            }
        }
    }

}
public class contractOtherItemTableControl : BasecontractOtherItemTableControl
{
        // The BasecontractOtherItemTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The contractOtherItemTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

    public override WhereClause CreateWhereClause()
    {
        WhereClause wc= base.CreateWhereClause();
        wc.iAND("contract_item_type !=" + Globals.CONTRACTITEM_SPLASHBACK.ToString());
        return wc;
    }
    
}
public class contractOtherItemTableControlRow : BasecontractOtherItemTableControlRow
{
      
        // The BasecontractOtherItemTableControlRow implements code for a ROW within the
        // the contractOtherItemTableControl table.  The BasecontractOtherItemTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of contractOtherItemTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            string contractID = this.Page.Request["Contract"];
            if (!Globals.IsQuotePhase(contractID))
            {
                otherRowEdit.Visible = false;
                contractOtherItemRecordRowSelection.Visible = false;
            }
            else
            {
                otherRowEdit.Text = MyContract.SetOtherItemEditLink(this.DataSource, this.Page);
            }
        }
    }


    public override void Delete()
    {
        if (this.IsNewRecord)
        {
            return;
        }

        KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
        ContractOtherItemRecord rec = ContractOtherItemTable.GetRecord(pk, false);
        string contractID = rec.contract_id.ToString();

        ContractOtherItemTable.DeleteRecord(pk);

        if (!String.IsNullOrEmpty(contractID))
        {
            ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
            Globals.UpdateContract(contract, this.Page.SystemUtils.GetUserID());

            contractMainRecordControl cntrl = this.Page.FindControl("contractMainRecordControl") as contractMainRecordControl;
            if (cntrl != null)
            {
                cntrl.ResetData = true;
                cntrl.DataChanged = true;
            }
        }

        ((contractOtherItemTableControl)MiscUtils.GetParentControlObject(this, "contractOtherItemTableControl")).DataChanged = true;
        ((contractOtherItemTableControl)MiscUtils.GetParentControlObject(this, "contractOtherItemTableControl")).ResetData = true;
    }

}
public class View_ContractTradeItemAndTradeTypeTableControl : BaseView_ContractTradeItemAndTradeTypeTableControl
{
        // The BaseView_ContractTradeItemAndTradeTypeTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The View_ContractTradeItemAndTradeTypeTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
    public class View_ContractTradeItemAndTradeTypeTableControlRow : BaseView_ContractTradeItemAndTradeTypeTableControlRow
    {

        // The BaseView_ContractTradeItemAndTradeTypeTableControlRow implements code for a ROW within the
        // the View_ContractTradeItemAndTradeTypeTableControl table.  The BaseView_ContractTradeItemAndTradeTypeTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of View_ContractTradeItemAndTradeTypeTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

        public override void DataBind()
        {
            base.DataBind();
            if (this.DataSource != null)
            {
                string contractID = this.Page.Request["Contract"];
                if (!Globals.IsQuotePhase(contractID))
                {
                    tradeRowEdit.Visible = false;
                    View_ContractTradeItemAndTradeTypeRecordRowSelection.Visible = false;
                }
                else
                {
                    string itemParams = "&task=" + this.DataSource.id0.ToString();
                    string url = "TradeSelection.aspx?Contract=" + this.DataSource.contract_id.ToString() + itemParams;
                    string width = "400";
                    RadWindowManager winManager = this.Page.FindControl("RadWindowManager1") as RadWindowManager;
                    if (winManager != null)
                        width = winManager.Width.Value.ToString();
                    string onClientClick = "EditWindow('" + url + "'," + width + ",'Trades')";
                    tradeRowEdit.Text = "<a href=\"javascript:" + onClientClick + "\"><img src=\"../Images/Icon_edit.gif\" border=0></a>";
                }
            }
        }

        public override void Delete()
        {
            // base.Delete();

            if (this.IsNewRecord)
            {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractTradeItemRecord rec = ContractTradeItemTable.GetRecord(pk, false);
            string contractID = rec.contract_id.ToString();


            ContractTradeItemTable.DeleteRecord(pk);

            if (!String.IsNullOrEmpty(contractID))
            {
                ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
                Globals.UpdateContract(contract, this.Page.SystemUtils.GetUserID());

                contractMainRecordControl cntrl = this.Page.FindControl("contractMainRecordControl") as contractMainRecordControl;
                if (cntrl != null)
                {
                    cntrl.ResetData = true;
                    cntrl.DataChanged = true;
                }
            }

            ((View_ContractTradeItemAndTradeTypeTableControl)MiscUtils.GetParentControlObject(this, "View_ContractTradeItemAndTradeTypeTableControl")).DataChanged = true;
            ((View_ContractTradeItemAndTradeTypeTableControl)MiscUtils.GetParentControlObject(this, "View_ContractTradeItemAndTradeTypeTableControl")).ResetData = true;
        }
    }
public class View_ContractOtherItemWithTypesTableControl : BaseView_ContractOtherItemWithTypesTableControl
{
        // The BaseView_ContractOtherItemWithTypesTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The View_ContractOtherItemWithTypesTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.
    public override WhereClause CreateWhereClause()
    {
        WhereClause wc = base.CreateWhereClause();
        wc.iAND("contract_item_type=" + Globals.CONTRACTITEM_SPLASHBACK.ToString());
        return wc;
    }
}
public class View_ContractOtherItemWithTypesTableControlRow : BaseView_ContractOtherItemWithTypesTableControlRow
{
      
        // The BaseView_ContractOtherItemWithTypesTableControlRow implements code for a ROW within the
        // the View_ContractOtherItemWithTypesTableControl table.  The BaseView_ContractOtherItemWithTypesTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of View_ContractOtherItemWithTypesTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            string contractID = this.Page.Request["Contract"];
            if (!Globals.IsQuotePhase(contractID))
            {
                splashbackRowEdit.Visible = false;
                View_ContractOtherItemWithTypesRecordRowSelection.Visible = false;
            }
            else
            {
                splashbackRowEdit.Text = MyContract.SetOtherItemEditLink(this.DataSource, this.Page);
            }
        }
    }

    public override void Delete()
    {
        if (this.IsNewRecord)
        {
            return;
        }

        KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
        ContractOtherItemRecord rec = ContractOtherItemTable.GetRecord(pk, false);
        string contractID = rec.contract_id.ToString();

        ContractOtherItemTable.DeleteRecord(pk);

        if (!String.IsNullOrEmpty(contractID))
        {
            ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
            Globals.UpdateContract(contract, this.Page.SystemUtils.GetUserID());

            contractMainRecordControl cntrl = this.Page.FindControl("contractMainRecordControl") as contractMainRecordControl;
            if (cntrl != null)
            {
                cntrl.ResetData = true;
                cntrl.DataChanged = true;
            }
        }

        ((View_ContractOtherItemWithTypesTableControl)MiscUtils.GetParentControlObject(this, "View_ContractOtherItemWithTypesTableControl")).DataChanged = true;
        ((View_ContractOtherItemWithTypesTableControl)MiscUtils.GetParentControlObject(this, "View_ContractOtherItemWithTypesTableControl")).ResetData = true;
    }

}
#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the contractBenchtopItemTableControlRow control on the Contract page.
// Do not modify this class. Instead override any method in contractBenchtopItemTableControlRow.
public class BasecontractBenchtopItemTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecontractBenchtopItemTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in contractBenchtopItemTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in contractBenchtopItemTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in contractBenchtopItemTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = ContractBenchtopItemTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasecontractBenchtopItemTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new ContractBenchtopItemRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in contractBenchtopItemTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractBenchtopItemTable.amount, @"C");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.amount1.Text = formattedValue;
            } else {  
                this.amount1.Text = ContractBenchtopItemTable.amount.Format(ContractBenchtopItemTable.amount.DefaultValue, @"C");
            }
                    
            if (this.amount1.Text == null ||
                this.amount1.Text.Trim().Length == 0) {
                this.amount1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.item_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractBenchtopItemTable.item_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.item_id1.Text = formattedValue;
            } else {  
                this.item_id1.Text = ContractBenchtopItemTable.item_id.Format(ContractBenchtopItemTable.item_id.DefaultValue);
            }
                    
            if (this.item_id1.Text == null ||
                this.item_id1.Text.Trim().Length == 0) {
                this.item_id1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.options_amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractBenchtopItemTable.options_amount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.options_amount.Text = formattedValue;
            } else {  
                this.options_amount.Text = ContractBenchtopItemTable.options_amount.Format(ContractBenchtopItemTable.options_amount.DefaultValue);
            }
                    
            if (this.options_amount.Text == null ||
                this.options_amount.Text.Trim().Length == 0) {
                this.options_amount.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unit_priceSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractBenchtopItemTable.unit_price);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.unit_price1.Text = formattedValue;
            } else {  
                this.unit_price1.Text = ContractBenchtopItemTable.unit_price.Format(ContractBenchtopItemTable.unit_price.DefaultValue);
            }
                    
            if (this.unit_price1.Text == null ||
                this.unit_price1.Text.Trim().Length == 0) {
                this.unit_price1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unitsSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractBenchtopItemTable.units);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.units1.Text = formattedValue;
            } else {  
                this.units1.Text = ContractBenchtopItemTable.units.Format(ContractBenchtopItemTable.units.DefaultValue);
            }
                    
            if (this.units1.Text == null ||
                this.units1.Text.Trim().Length == 0) {
                this.units1.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in contractBenchtopItemTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // contractMain in contractMainRecordControl is One To Many to contractBenchtopItemTableControl.
                    
            // Setup the parent id in the record.
            contractMainRecordControl reccontractMainRecordControl = (contractMainRecordControl)this.Page.FindControlRecursively("contractMainRecordControl");
            if (reccontractMainRecordControl != null && reccontractMainRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                reccontractMainRecordControl.LoadData();
            }
            if (reccontractMainRecordControl == null || reccontractMainRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.contract_id = reccontractMainRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in contractBenchtopItemTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in contractBenchtopItemTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((contractBenchtopItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopItemTableControl")).DataChanged = true;
                ((contractBenchtopItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopItemTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in contractBenchtopItemTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in contractBenchtopItemTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in contractBenchtopItemTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractBenchtopItemTable.DeleteRecord(pk);

          
            ((contractBenchtopItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopItemTableControl")).DataChanged = true;
            ((contractBenchtopItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopItemTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecontractBenchtopItemTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasecontractBenchtopItemTableControlRow_Rec"] = value;
            }
        }
        
        private ContractBenchtopItemRecord _DataSource;
        public ContractBenchtopItemRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal amount1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount1");
            }
        }
        
        public System.Web.UI.WebControls.Label benchRowEdit {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "benchRowEdit");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractBenchtopItemRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopItemRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Literal item_id1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_id1");
            }
        }
           
        public System.Web.UI.WebControls.Literal options_amount {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "options_amount");
            }
        }
           
        public System.Web.UI.WebControls.Literal unit_price1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_price1");
            }
        }
           
        public System.Web.UI.WebControls.Literal units1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "units1");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            ContractBenchtopItemRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public ContractBenchtopItemRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return ContractBenchtopItemTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the contractBenchtopItemTableControl control on the Contract page.
// Do not modify this class. Instead override any method in contractBenchtopItemTableControl.
public class BasecontractBenchtopItemTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasecontractBenchtopItemTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.contractBenchtopItemDeleteButton.Button.Click += new EventHandler(contractBenchtopItemDeleteButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.contractBenchtopItemDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (ContractBenchtopItemRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopItemRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = ContractBenchtopItemTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (ContractBenchtopItemRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopItemRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (contractBenchtopItemTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (ContractBenchtopItemRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopItemRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = ContractBenchtopItemTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractBenchtopItemTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                contractBenchtopItemTableControlRow recControl = (contractBenchtopItemTableControlRow)(repItem.FindControl("contractBenchtopItemTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(ContractBenchtopItemTable.item_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for contractBenchtopItemTableControl pagination.
        

            // Bind the pagination labels.
        
            this.contractBenchtopItemTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (contractBenchtopItemTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            ContractBenchtopItemTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        contractMainRecordControl parentRecordControl = (contractMainRecordControl)(this.Page.FindControlRecursively("contractMainRecordControl"));
            ContractMainRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(ContractBenchtopItemTable.contract_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractBenchtopItemTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    contractBenchtopItemTableControlRow recControl = (contractBenchtopItemTableControlRow)(repItem.FindControl("contractBenchtopItemTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        ContractBenchtopItemRecord rec = new ContractBenchtopItemRecord();
        
                        if (recControl.amount1.Text != "") {
                            rec.Parse(recControl.amount1.Text, ContractBenchtopItemTable.amount);
                        }
                        if (recControl.item_id1.Text != "") {
                            rec.Parse(recControl.item_id1.Text, ContractBenchtopItemTable.item_id);
                        }
                        if (recControl.options_amount.Text != "") {
                            rec.Parse(recControl.options_amount.Text, ContractBenchtopItemTable.options_amount);
                        }
                        if (recControl.unit_price1.Text != "") {
                            rec.Parse(recControl.unit_price1.Text, ContractBenchtopItemTable.unit_price);
                        }
                        if (recControl.units1.Text != "") {
                            rec.Parse(recControl.units1.Text, ContractBenchtopItemTable.units);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new ContractBenchtopItemRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (ContractBenchtopItemRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopItemRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(contractBenchtopItemTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(contractBenchtopItemTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["contractBenchtopItemTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["contractBenchtopItemTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void contractBenchtopItemDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private ContractBenchtopItemRecord[] _DataSource = null;
        public  ContractBenchtopItemRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal amount1Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount1Label");
            }
        }
        
        public WKCRM.UI.IThemeButton contractBenchtopItemDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopItemDeleteButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal contractBenchtopItemTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopItemTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractBenchtopItemToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopItemToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label contractBenchtopItemTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopItemTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal item_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal options_amountLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "options_amountLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal unit_priceLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_priceLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal unitsLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unitsLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                contractBenchtopItemTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                ContractBenchtopItemRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (contractBenchtopItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractBenchtopItemRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public contractBenchtopItemTableControlRow GetSelectedRecordControl()
        {
        contractBenchtopItemTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public contractBenchtopItemTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (contractBenchtopItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractBenchtopItemRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (contractBenchtopItemTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.contractBenchtopItemTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            contractBenchtopItemTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (contractBenchtopItemTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.contractBenchtopItemRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public contractBenchtopItemTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("contractBenchtopItemTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                contractBenchtopItemTableControlRow recControl = (contractBenchtopItemTableControlRow)repItem.FindControl("contractBenchtopItemTableControlRow");
                recList.Add(recControl);
            }

            return (contractBenchtopItemTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.contractBenchtopItemTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the contractDoorItemTableControlRow control on the Contract page.
// Do not modify this class. Instead override any method in contractDoorItemTableControlRow.
public class BasecontractDoorItemTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecontractDoorItemTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in contractDoorItemTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in contractDoorItemTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in contractDoorItemTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = ContractDoorItemTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasecontractDoorItemTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new ContractDoorItemRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in contractDoorItemTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractDoorItemTable.amount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.amount.Text = formattedValue;
            } else {  
                this.amount.Text = ContractDoorItemTable.amount.Format(ContractDoorItemTable.amount.DefaultValue);
            }
                    
            if (this.amount.Text == null ||
                this.amount.Text.Trim().Length == 0) {
                this.amount.Text = "&nbsp;";
            }
                  
            if (this.DataSource.item_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractDoorItemTable.item_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.item_id.Text = formattedValue;
            } else {  
                this.item_id.Text = ContractDoorItemTable.item_id.Format(ContractDoorItemTable.item_id.DefaultValue);
            }
                    
            if (this.item_id.Text == null ||
                this.item_id.Text.Trim().Length == 0) {
                this.item_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unit_priceSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractDoorItemTable.unit_price);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.unit_price.Text = formattedValue;
            } else {  
                this.unit_price.Text = ContractDoorItemTable.unit_price.Format(ContractDoorItemTable.unit_price.DefaultValue);
            }
                    
            if (this.unit_price.Text == null ||
                this.unit_price.Text.Trim().Length == 0) {
                this.unit_price.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unitsSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractDoorItemTable.units);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.units.Text = formattedValue;
            } else {  
                this.units.Text = ContractDoorItemTable.units.Format(ContractDoorItemTable.units.DefaultValue);
            }
                    
            if (this.units.Text == null ||
                this.units.Text.Trim().Length == 0) {
                this.units.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in contractDoorItemTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // contractMain in contractMainRecordControl is One To Many to contractDoorItemTableControl.
                    
            // Setup the parent id in the record.
            contractMainRecordControl reccontractMainRecordControl = (contractMainRecordControl)this.Page.FindControlRecursively("contractMainRecordControl");
            if (reccontractMainRecordControl != null && reccontractMainRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                reccontractMainRecordControl.LoadData();
            }
            if (reccontractMainRecordControl == null || reccontractMainRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.contract_id = reccontractMainRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in contractDoorItemTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in contractDoorItemTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((contractDoorItemTableControl)MiscUtils.GetParentControlObject(this, "contractDoorItemTableControl")).DataChanged = true;
                ((contractDoorItemTableControl)MiscUtils.GetParentControlObject(this, "contractDoorItemTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in contractDoorItemTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in contractDoorItemTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in contractDoorItemTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractDoorItemTable.DeleteRecord(pk);

          
            ((contractDoorItemTableControl)MiscUtils.GetParentControlObject(this, "contractDoorItemTableControl")).DataChanged = true;
            ((contractDoorItemTableControl)MiscUtils.GetParentControlObject(this, "contractDoorItemTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecontractDoorItemTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasecontractDoorItemTableControlRow_Rec"] = value;
            }
        }
        
        private ContractDoorItemRecord _DataSource;
        public ContractDoorItemRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal amount {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractDoorItemRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractDoorItemRecordRowSelection");
            }
        }
        
        public System.Web.UI.WebControls.Label doorRowEdit {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "doorRowEdit");
            }
        }
           
        public System.Web.UI.WebControls.Literal item_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal unit_price {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_price");
            }
        }
           
        public System.Web.UI.WebControls.Literal units {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "units");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            ContractDoorItemRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public ContractDoorItemRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return ContractDoorItemTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the contractDoorItemTableControl control on the Contract page.
// Do not modify this class. Instead override any method in contractDoorItemTableControl.
public class BasecontractDoorItemTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasecontractDoorItemTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.contractDoorItemDeleteButton.Button.Click += new EventHandler(contractDoorItemDeleteButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.contractDoorItemDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (ContractDoorItemRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.ContractDoorItemRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = ContractDoorItemTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (ContractDoorItemRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.ContractDoorItemRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (contractDoorItemTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (ContractDoorItemRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.ContractDoorItemRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = ContractDoorItemTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractDoorItemTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                contractDoorItemTableControlRow recControl = (contractDoorItemTableControlRow)(repItem.FindControl("contractDoorItemTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(ContractDoorItemTable.item_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for contractDoorItemTableControl pagination.
        

            // Bind the pagination labels.
        
            this.contractDoorItemTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (contractDoorItemTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            ContractDoorItemTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        contractMainRecordControl parentRecordControl = (contractMainRecordControl)(this.Page.FindControlRecursively("contractMainRecordControl"));
            ContractMainRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(ContractDoorItemTable.contract_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractDoorItemTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    contractDoorItemTableControlRow recControl = (contractDoorItemTableControlRow)(repItem.FindControl("contractDoorItemTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        ContractDoorItemRecord rec = new ContractDoorItemRecord();
        
                        if (recControl.amount.Text != "") {
                            rec.Parse(recControl.amount.Text, ContractDoorItemTable.amount);
                        }
                        if (recControl.item_id.Text != "") {
                            rec.Parse(recControl.item_id.Text, ContractDoorItemTable.item_id);
                        }
                        if (recControl.unit_price.Text != "") {
                            rec.Parse(recControl.unit_price.Text, ContractDoorItemTable.unit_price);
                        }
                        if (recControl.units.Text != "") {
                            rec.Parse(recControl.units.Text, ContractDoorItemTable.units);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new ContractDoorItemRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (ContractDoorItemRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.ContractDoorItemRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(contractDoorItemTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(contractDoorItemTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["contractDoorItemTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["contractDoorItemTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void contractDoorItemDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private ContractDoorItemRecord[] _DataSource = null;
        public  ContractDoorItemRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal amountLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amountLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton contractDoorItemDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractDoorItemDeleteButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal contractDoorItemTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractDoorItemTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractDoorItemToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractDoorItemToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label contractDoorItemTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractDoorItemTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal item_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal unit_priceLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_priceLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal unitsLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unitsLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                contractDoorItemTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                ContractDoorItemRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (contractDoorItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractDoorItemRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public contractDoorItemTableControlRow GetSelectedRecordControl()
        {
        contractDoorItemTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public contractDoorItemTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (contractDoorItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractDoorItemRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (contractDoorItemTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.contractDoorItemTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            contractDoorItemTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (contractDoorItemTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.contractDoorItemRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public contractDoorItemTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("contractDoorItemTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                contractDoorItemTableControlRow recControl = (contractDoorItemTableControlRow)repItem.FindControl("contractDoorItemTableControlRow");
                recList.Add(recControl);
            }

            return (contractDoorItemTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.contractDoorItemTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the contractOtherItemTableControlRow control on the Contract page.
// Do not modify this class. Instead override any method in contractOtherItemTableControlRow.
public class BasecontractOtherItemTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecontractOtherItemTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in contractOtherItemTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in contractOtherItemTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in contractOtherItemTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_ContractOtherItemWithTypesView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasecontractOtherItemTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_ContractOtherItemWithTypesRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in contractOtherItemTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.amount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.amount3.Text = formattedValue;
            } else {  
                this.amount3.Text = View_ContractOtherItemWithTypesView.amount.Format(View_ContractOtherItemWithTypesView.amount.DefaultValue);
            }
                    
            if (this.amount3.Text == null ||
                this.amount3.Text.Trim().Length == 0) {
                this.amount3.Text = "&nbsp;";
            }
                  
            if (this.DataSource.contract_item_typeSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.contract_item_type);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.contract_item_type.Text = formattedValue;
            } else {  
                this.contract_item_type.Text = View_ContractOtherItemWithTypesView.contract_item_type.Format(View_ContractOtherItemWithTypesView.contract_item_type.DefaultValue);
            }
                    
            if (this.contract_item_type.Text == null ||
                this.contract_item_type.Text.Trim().Length == 0) {
                this.contract_item_type.Text = "&nbsp;";
            }
                  
            if (this.DataSource.item_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.item_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.item_id3.Text = formattedValue;
            } else {  
                this.item_id3.Text = View_ContractOtherItemWithTypesView.item_id.Format(View_ContractOtherItemWithTypesView.item_id.DefaultValue);
            }
                    
            if (this.item_id3.Text == null ||
                this.item_id3.Text.Trim().Length == 0) {
                this.item_id3.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unit_priceSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.unit_price);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.unit_price3.Text = formattedValue;
            } else {  
                this.unit_price3.Text = View_ContractOtherItemWithTypesView.unit_price.Format(View_ContractOtherItemWithTypesView.unit_price.DefaultValue);
            }
                    
            if (this.unit_price3.Text == null ||
                this.unit_price3.Text.Trim().Length == 0) {
                this.unit_price3.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unitsSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.units);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.units3.Text = formattedValue;
            } else {  
                this.units3.Text = View_ContractOtherItemWithTypesView.units.Format(View_ContractOtherItemWithTypesView.units.DefaultValue);
            }
                    
            if (this.units3.Text == null ||
                this.units3.Text.Trim().Length == 0) {
                this.units3.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in contractOtherItemTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // contractMain in contractMainRecordControl is One To Many to contractOtherItemTableControl.
                    
            // Setup the parent id in the record.
            contractMainRecordControl reccontractMainRecordControl = (contractMainRecordControl)this.Page.FindControlRecursively("contractMainRecordControl");
            if (reccontractMainRecordControl != null && reccontractMainRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                reccontractMainRecordControl.LoadData();
            }
            if (reccontractMainRecordControl == null || reccontractMainRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.contract_id = reccontractMainRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in contractOtherItemTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in contractOtherItemTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((contractOtherItemTableControl)MiscUtils.GetParentControlObject(this, "contractOtherItemTableControl")).DataChanged = true;
                ((contractOtherItemTableControl)MiscUtils.GetParentControlObject(this, "contractOtherItemTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in contractOtherItemTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in contractOtherItemTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in contractOtherItemTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_ContractOtherItemWithTypesView.DeleteRecord(pk);

          
            ((contractOtherItemTableControl)MiscUtils.GetParentControlObject(this, "contractOtherItemTableControl")).DataChanged = true;
            ((contractOtherItemTableControl)MiscUtils.GetParentControlObject(this, "contractOtherItemTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecontractOtherItemTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasecontractOtherItemTableControlRow_Rec"] = value;
            }
        }
        
        private View_ContractOtherItemWithTypesRecord _DataSource;
        public View_ContractOtherItemWithTypesRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal amount3 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount3");
            }
        }
           
        public System.Web.UI.WebControls.Literal contract_item_type {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_item_type");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractOtherItemRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractOtherItemRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Literal item_id3 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_id3");
            }
        }
        
        public System.Web.UI.WebControls.Label otherRowEdit {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "otherRowEdit");
            }
        }
           
        public System.Web.UI.WebControls.Literal unit_price3 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_price3");
            }
        }
           
        public System.Web.UI.WebControls.Literal units3 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "units3");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_ContractOtherItemWithTypesRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_ContractOtherItemWithTypesRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_ContractOtherItemWithTypesView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the contractOtherItemTableControl control on the Contract page.
// Do not modify this class. Instead override any method in contractOtherItemTableControl.
public class BasecontractOtherItemTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasecontractOtherItemTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.contractOtherItemDeleteButton.Button.Click += new EventHandler(contractOtherItemDeleteButton_Click);

            // Setup the filter and search events.
        
            this.contract_item_typeFilter.SelectedIndexChanged += new EventHandler(contract_item_typeFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.contract_item_typeFilter)) {
                this.contract_item_typeFilter.Items.Add(new ListItem(this.GetFromSession(this.contract_item_typeFilter), this.GetFromSession(this.contract_item_typeFilter)));
                this.contract_item_typeFilter.SelectedValue = this.GetFromSession(this.contract_item_typeFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_ContractOtherItemWithTypesView.contract_item_type, OrderByItem.OrderDir.Asc);
        
                this.CurrentSortOrder.Add(View_ContractOtherItemWithTypesView.id0, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.contractOtherItemDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ContractOtherItemWithTypesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_ContractOtherItemWithTypesView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ContractOtherItemWithTypesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (contractOtherItemTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_ContractOtherItemWithTypesRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_ContractOtherItemWithTypesView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populatecontract_item_typeFilter(MiscUtils.GetSelectedValue(this.contract_item_typeFilter, this.GetFromSession(this.contract_item_typeFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractOtherItemTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                contractOtherItemTableControlRow recControl = (contractOtherItemTableControlRow)(repItem.FindControl("contractOtherItemTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_ContractOtherItemWithTypesView.contract_item_type, this.DataSource);
            this.Page.PregetDfkaRecords(View_ContractOtherItemWithTypesView.item_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for contractOtherItemTableControl pagination.
        

            // Bind the pagination labels.
        
            this.contractOtherItemTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (contractOtherItemTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_ContractOtherItemWithTypesView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        contractMainRecordControl parentRecordControl = (contractMainRecordControl)(this.Page.FindControlRecursively("contractMainRecordControl"));
            ContractMainRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(View_ContractOtherItemWithTypesView.contract_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            if (MiscUtils.IsValueSelected(this.contract_item_typeFilter)) {
                wc.iAND(View_ContractOtherItemWithTypesView.contract_item_type, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.contract_item_typeFilter, this.GetFromSession(this.contract_item_typeFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractOtherItemTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    contractOtherItemTableControlRow recControl = (contractOtherItemTableControlRow)(repItem.FindControl("contractOtherItemTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_ContractOtherItemWithTypesRecord rec = new View_ContractOtherItemWithTypesRecord();
        
                        if (recControl.amount3.Text != "") {
                            rec.Parse(recControl.amount3.Text, View_ContractOtherItemWithTypesView.amount);
                        }
                        if (recControl.contract_item_type.Text != "") {
                            rec.Parse(recControl.contract_item_type.Text, View_ContractOtherItemWithTypesView.contract_item_type);
                        }
                        if (recControl.item_id3.Text != "") {
                            rec.Parse(recControl.item_id3.Text, View_ContractOtherItemWithTypesView.item_id);
                        }
                        if (recControl.unit_price3.Text != "") {
                            rec.Parse(recControl.unit_price3.Text, View_ContractOtherItemWithTypesView.unit_price);
                        }
                        if (recControl.units3.Text != "") {
                            rec.Parse(recControl.units3.Text, View_ContractOtherItemWithTypesView.units);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_ContractOtherItemWithTypesRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_ContractOtherItemWithTypesRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(contractOtherItemTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(contractOtherItemTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for contract_item_typeFilter.
        protected virtual void Populatecontract_item_typeFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(ContractItemTypeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.contract_item_typeFilter.Items.Clear();
            foreach (ContractItemTypeRecord itemValue in ContractItemTypeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(ContractItemTypeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.contract_item_typeFilter.Items.IndexOf(item) < 0) {
                    this.contract_item_typeFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.contract_item_typeFilter, selectedValue);

            // Add the All item.
            this.contract_item_typeFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter contract_item_typeFilter.
        public virtual WhereClause CreateWhereClause_contract_item_typeFilter()
        {
              
            WhereClause wc = new WhereClause();
                  contractMainRecordControl parentRecordControl = (contractMainRecordControl)this.Page.FindControlRecursively("contractMainRecordControl");
            ContractMainRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
                    
            if (parentRec.id0Specified) {
                wc.iAND(View_ContractOtherItemWithTypesView.contract_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.contract_item_typeFilter, this.contract_item_typeFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.contract_item_typeFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["contractOtherItemTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["contractOtherItemTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void contractOtherItemDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void contract_item_typeFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_ContractOtherItemWithTypesRecord[] _DataSource = null;
        public  View_ContractOtherItemWithTypesRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal amount2Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount2Label");
            }
        }
        
        public System.Web.UI.WebControls.Literal contract_item_type1Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_item_type1Label");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList contract_item_typeFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_item_typeFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal contract_item_typeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_item_typeLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton contractOtherItemDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractOtherItemDeleteButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal contractOtherItemTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractOtherItemTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractOtherItemToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractOtherItemToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label contractOtherItemTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractOtherItemTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal item_id1Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_id1Label");
            }
        }
        
        public System.Web.UI.WebControls.Literal unit_price1Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_price1Label");
            }
        }
        
        public System.Web.UI.WebControls.Literal units1Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "units1Label");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                contractOtherItemTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_ContractOtherItemWithTypesRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (contractOtherItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractOtherItemRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public contractOtherItemTableControlRow GetSelectedRecordControl()
        {
        contractOtherItemTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public contractOtherItemTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (contractOtherItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractOtherItemRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (contractOtherItemTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.contractOtherItemTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            contractOtherItemTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (contractOtherItemTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.contractOtherItemRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public contractOtherItemTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("contractOtherItemTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                contractOtherItemTableControlRow recControl = (contractOtherItemTableControlRow)repItem.FindControl("contractOtherItemTableControlRow");
                recList.Add(recControl);
            }

            return (contractOtherItemTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.contractOtherItemTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the View_ContractOtherItemWithTypesTableControlRow control on the Contract page.
// Do not modify this class. Instead override any method in View_ContractOtherItemWithTypesTableControlRow.
public class BaseView_ContractOtherItemWithTypesTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseView_ContractOtherItemWithTypesTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_ContractOtherItemWithTypesView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseView_ContractOtherItemWithTypesTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_ContractOtherItemWithTypesRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.amount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.amount4.Text = formattedValue;
            } else {  
                this.amount4.Text = View_ContractOtherItemWithTypesView.amount.Format(View_ContractOtherItemWithTypesView.amount.DefaultValue);
            }
                    
            if (this.amount4.Text == null ||
                this.amount4.Text.Trim().Length == 0) {
                this.amount4.Text = "&nbsp;";
            }
                  
            if (this.DataSource.item_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.item_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.item_id2.Text = formattedValue;
            } else {  
                this.item_id2.Text = View_ContractOtherItemWithTypesView.item_id.Format(View_ContractOtherItemWithTypesView.item_id.DefaultValue);
            }
                    
            if (this.item_id2.Text == null ||
                this.item_id2.Text.Trim().Length == 0) {
                this.item_id2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unit_priceSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.unit_price);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.unit_price2.Text = formattedValue;
            } else {  
                this.unit_price2.Text = View_ContractOtherItemWithTypesView.unit_price.Format(View_ContractOtherItemWithTypesView.unit_price.DefaultValue);
            }
                    
            if (this.unit_price2.Text == null ||
                this.unit_price2.Text.Trim().Length == 0) {
                this.unit_price2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unitsSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractOtherItemWithTypesView.units);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.units4.Text = formattedValue;
            } else {  
                this.units4.Text = View_ContractOtherItemWithTypesView.units.Format(View_ContractOtherItemWithTypesView.units.DefaultValue);
            }
                    
            if (this.units4.Text == null ||
                this.units4.Text.Trim().Length == 0) {
                this.units4.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // contractMain in contractMainRecordControl is One To Many to View_ContractOtherItemWithTypesTableControl.
                    
            // Setup the parent id in the record.
            contractMainRecordControl reccontractMainRecordControl = (contractMainRecordControl)this.Page.FindControlRecursively("contractMainRecordControl");
            if (reccontractMainRecordControl != null && reccontractMainRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                reccontractMainRecordControl.LoadData();
            }
            if (reccontractMainRecordControl == null || reccontractMainRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.contract_id = reccontractMainRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in View_ContractOtherItemWithTypesTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in View_ContractOtherItemWithTypesTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((View_ContractOtherItemWithTypesTableControl)MiscUtils.GetParentControlObject(this, "View_ContractOtherItemWithTypesTableControl")).DataChanged = true;
                ((View_ContractOtherItemWithTypesTableControl)MiscUtils.GetParentControlObject(this, "View_ContractOtherItemWithTypesTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in View_ContractOtherItemWithTypesTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_ContractOtherItemWithTypesView.DeleteRecord(pk);

          
            ((View_ContractOtherItemWithTypesTableControl)MiscUtils.GetParentControlObject(this, "View_ContractOtherItemWithTypesTableControl")).DataChanged = true;
            ((View_ContractOtherItemWithTypesTableControl)MiscUtils.GetParentControlObject(this, "View_ContractOtherItemWithTypesTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseView_ContractOtherItemWithTypesTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseView_ContractOtherItemWithTypesTableControlRow_Rec"] = value;
            }
        }
        
        private View_ContractOtherItemWithTypesRecord _DataSource;
        public View_ContractOtherItemWithTypesRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal amount4 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount4");
            }
        }
           
        public System.Web.UI.WebControls.Literal item_id2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_id2");
            }
        }
        
        public System.Web.UI.WebControls.Label splashbackRowEdit {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "splashbackRowEdit");
            }
        }
           
        public System.Web.UI.WebControls.Literal unit_price2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_price2");
            }
        }
           
        public System.Web.UI.WebControls.Literal units4 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "units4");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox View_ContractOtherItemWithTypesRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractOtherItemWithTypesRecordRowSelection");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_ContractOtherItemWithTypesRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_ContractOtherItemWithTypesRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_ContractOtherItemWithTypesView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the View_ContractOtherItemWithTypesTableControl control on the Contract page.
// Do not modify this class. Instead override any method in View_ContractOtherItemWithTypesTableControl.
public class BaseView_ContractOtherItemWithTypesTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseView_ContractOtherItemWithTypesTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.View_ContractOtherItemWithTypesDeleteButton.Button.Click += new EventHandler(View_ContractOtherItemWithTypesDeleteButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_ContractOtherItemWithTypesView.id0, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.View_ContractOtherItemWithTypesDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ContractOtherItemWithTypesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_ContractOtherItemWithTypesView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ContractOtherItemWithTypesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (View_ContractOtherItemWithTypesTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_ContractOtherItemWithTypesRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_ContractOtherItemWithTypesView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_ContractOtherItemWithTypesTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                View_ContractOtherItemWithTypesTableControlRow recControl = (View_ContractOtherItemWithTypesTableControlRow)(repItem.FindControl("View_ContractOtherItemWithTypesTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_ContractOtherItemWithTypesView.item_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for View_ContractOtherItemWithTypesTableControl pagination.
        

            // Bind the pagination labels.
        
            this.View_ContractOtherItemWithTypesTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (View_ContractOtherItemWithTypesTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_ContractOtherItemWithTypesView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        contractMainRecordControl parentRecordControl = (contractMainRecordControl)(this.Page.FindControlRecursively("contractMainRecordControl"));
            ContractMainRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(View_ContractOtherItemWithTypesView.contract_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            // Get the static clause defined at design time on the Table Panel Wizard
            WhereClause qc = this.CreateQueryClause();
            if (qc != null) {
                wc.iAND(qc);
            }
        
            return (wc);
        }
        
        // Create a where clause for the Static clause defined at design time.
        protected virtual WhereClause CreateQueryClause()
        {
            return null;
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_ContractOtherItemWithTypesTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    View_ContractOtherItemWithTypesTableControlRow recControl = (View_ContractOtherItemWithTypesTableControlRow)(repItem.FindControl("View_ContractOtherItemWithTypesTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_ContractOtherItemWithTypesRecord rec = new View_ContractOtherItemWithTypesRecord();
        
                        if (recControl.amount4.Text != "") {
                            rec.Parse(recControl.amount4.Text, View_ContractOtherItemWithTypesView.amount);
                        }
                        if (recControl.item_id2.Text != "") {
                            rec.Parse(recControl.item_id2.Text, View_ContractOtherItemWithTypesView.item_id);
                        }
                        if (recControl.unit_price2.Text != "") {
                            rec.Parse(recControl.unit_price2.Text, View_ContractOtherItemWithTypesView.unit_price);
                        }
                        if (recControl.units4.Text != "") {
                            rec.Parse(recControl.units4.Text, View_ContractOtherItemWithTypesView.units);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_ContractOtherItemWithTypesRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_ContractOtherItemWithTypesRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_ContractOtherItemWithTypesRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(View_ContractOtherItemWithTypesTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(View_ContractOtherItemWithTypesTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["View_ContractOtherItemWithTypesTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["View_ContractOtherItemWithTypesTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void View_ContractOtherItemWithTypesDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_ContractOtherItemWithTypesRecord[] _DataSource = null;
        public  View_ContractOtherItemWithTypesRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal amountLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amountLabel2");
            }
        }
        
        public System.Web.UI.WebControls.Literal item_idLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "item_idLabel2");
            }
        }
        
        public System.Web.UI.WebControls.Literal unit_priceLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unit_priceLabel2");
            }
        }
        
        public System.Web.UI.WebControls.Literal unitsLabel3 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unitsLabel3");
            }
        }
        
        public WKCRM.UI.IThemeButton View_ContractOtherItemWithTypesDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractOtherItemWithTypesDeleteButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal View_ContractOtherItemWithTypesTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractOtherItemWithTypesTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox View_ContractOtherItemWithTypesToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractOtherItemWithTypesToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label View_ContractOtherItemWithTypesTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractOtherItemWithTypesTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                View_ContractOtherItemWithTypesTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_ContractOtherItemWithTypesRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (View_ContractOtherItemWithTypesTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_ContractOtherItemWithTypesRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public View_ContractOtherItemWithTypesTableControlRow GetSelectedRecordControl()
        {
        View_ContractOtherItemWithTypesTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public View_ContractOtherItemWithTypesTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (View_ContractOtherItemWithTypesTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_ContractOtherItemWithTypesRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (View_ContractOtherItemWithTypesTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.View_ContractOtherItemWithTypesTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            View_ContractOtherItemWithTypesTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (View_ContractOtherItemWithTypesTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.View_ContractOtherItemWithTypesRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public View_ContractOtherItemWithTypesTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("View_ContractOtherItemWithTypesTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                View_ContractOtherItemWithTypesTableControlRow recControl = (View_ContractOtherItemWithTypesTableControlRow)repItem.FindControl("View_ContractOtherItemWithTypesTableControlRow");
                recList.Add(recControl);
            }

            return (View_ContractOtherItemWithTypesTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.View_ContractOtherItemWithTypesTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the View_ContractTradeItemAndTradeTypeTableControlRow control on the Contract page.
// Do not modify this class. Instead override any method in View_ContractTradeItemAndTradeTypeTableControlRow.
public class BaseView_ContractTradeItemAndTradeTypeTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseView_ContractTradeItemAndTradeTypeTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_ContractTradeItemAndTradeTypeView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseView_ContractTradeItemAndTradeTypeTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_ContractTradeItemAndTradeTypeRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractTradeItemAndTradeTypeView.amount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.amount2.Text = formattedValue;
            } else {  
                this.amount2.Text = View_ContractTradeItemAndTradeTypeView.amount.Format(View_ContractTradeItemAndTradeTypeView.amount.DefaultValue);
            }
                    
            if (this.amount2.Text == null ||
                this.amount2.Text.Trim().Length == 0) {
                this.amount2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.priceSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractTradeItemAndTradeTypeView.price);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.price.Text = formattedValue;
            } else {  
                this.price.Text = View_ContractTradeItemAndTradeTypeView.price.Format(View_ContractTradeItemAndTradeTypeView.price.DefaultValue);
            }
                    
            if (this.price.Text == null ||
                this.price.Text.Trim().Length == 0) {
                this.price.Text = "&nbsp;";
            }
                  
            if (this.DataSource.TradeSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractTradeItemAndTradeTypeView.Trade);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.Trade.Text = formattedValue;
            } else {  
                this.Trade.Text = View_ContractTradeItemAndTradeTypeView.Trade.Format(View_ContractTradeItemAndTradeTypeView.Trade.DefaultValue);
            }
                    
            if (this.Trade.Text == null ||
                this.Trade.Text.Trim().Length == 0) {
                this.Trade.Text = "&nbsp;";
            }
                  
            if (this.DataSource.trade_task_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractTradeItemAndTradeTypeView.trade_task_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.trade_task_id.Text = formattedValue;
            } else {  
                this.trade_task_id.Text = View_ContractTradeItemAndTradeTypeView.trade_task_id.Format(View_ContractTradeItemAndTradeTypeView.trade_task_id.DefaultValue);
            }
                    
            if (this.trade_task_id.Text == null ||
                this.trade_task_id.Text.Trim().Length == 0) {
                this.trade_task_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.unitsSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ContractTradeItemAndTradeTypeView.units);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.units2.Text = formattedValue;
            } else {  
                this.units2.Text = View_ContractTradeItemAndTradeTypeView.units.Format(View_ContractTradeItemAndTradeTypeView.units.DefaultValue);
            }
                    
            if (this.units2.Text == null ||
                this.units2.Text.Trim().Length == 0) {
                this.units2.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // contractMain in contractMainRecordControl is One To Many to View_ContractTradeItemAndTradeTypeTableControl.
                    
            // Setup the parent id in the record.
            contractMainRecordControl reccontractMainRecordControl = (contractMainRecordControl)this.Page.FindControlRecursively("contractMainRecordControl");
            if (reccontractMainRecordControl != null && reccontractMainRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                reccontractMainRecordControl.LoadData();
            }
            if (reccontractMainRecordControl == null || reccontractMainRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.contract_id = reccontractMainRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in View_ContractTradeItemAndTradeTypeTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in View_ContractTradeItemAndTradeTypeTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((View_ContractTradeItemAndTradeTypeTableControl)MiscUtils.GetParentControlObject(this, "View_ContractTradeItemAndTradeTypeTableControl")).DataChanged = true;
                ((View_ContractTradeItemAndTradeTypeTableControl)MiscUtils.GetParentControlObject(this, "View_ContractTradeItemAndTradeTypeTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in View_ContractTradeItemAndTradeTypeTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_ContractTradeItemAndTradeTypeView.DeleteRecord(pk);

          
            ((View_ContractTradeItemAndTradeTypeTableControl)MiscUtils.GetParentControlObject(this, "View_ContractTradeItemAndTradeTypeTableControl")).DataChanged = true;
            ((View_ContractTradeItemAndTradeTypeTableControl)MiscUtils.GetParentControlObject(this, "View_ContractTradeItemAndTradeTypeTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseView_ContractTradeItemAndTradeTypeTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseView_ContractTradeItemAndTradeTypeTableControlRow_Rec"] = value;
            }
        }
        
        private View_ContractTradeItemAndTradeTypeRecord _DataSource;
        public View_ContractTradeItemAndTradeTypeRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal amount2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount2");
            }
        }
           
        public System.Web.UI.WebControls.Literal price {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "price");
            }
        }
           
        public System.Web.UI.WebControls.Literal Trade {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Trade");
            }
        }
           
        public System.Web.UI.WebControls.Literal trade_task_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_task_id");
            }
        }
        
        public System.Web.UI.WebControls.Label tradeRowEdit {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradeRowEdit");
            }
        }
           
        public System.Web.UI.WebControls.Literal units2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "units2");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox View_ContractTradeItemAndTradeTypeRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractTradeItemAndTradeTypeRecordRowSelection");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_ContractTradeItemAndTradeTypeRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_ContractTradeItemAndTradeTypeRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_ContractTradeItemAndTradeTypeView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the View_ContractTradeItemAndTradeTypeTableControl control on the Contract page.
// Do not modify this class. Instead override any method in View_ContractTradeItemAndTradeTypeTableControl.
public class BaseView_ContractTradeItemAndTradeTypeTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseView_ContractTradeItemAndTradeTypeTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.View_ContractTradeItemAndTradeTypeDeleteButton.Button.Click += new EventHandler(View_ContractTradeItemAndTradeTypeDeleteButton_Click);

            // Setup the filter and search events.
        
            this.TradeFilter.SelectedIndexChanged += new EventHandler(TradeFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.TradeFilter)) {
                this.TradeFilter.Items.Add(new ListItem(this.GetFromSession(this.TradeFilter), this.GetFromSession(this.TradeFilter)));
                this.TradeFilter.SelectedValue = this.GetFromSession(this.TradeFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_ContractTradeItemAndTradeTypeView.Trade, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.View_ContractTradeItemAndTradeTypeDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ContractTradeItemAndTradeTypeRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ContractTradeItemAndTradeTypeRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_ContractTradeItemAndTradeTypeView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ContractTradeItemAndTradeTypeRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ContractTradeItemAndTradeTypeRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (View_ContractTradeItemAndTradeTypeTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_ContractTradeItemAndTradeTypeRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_ContractTradeItemAndTradeTypeRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_ContractTradeItemAndTradeTypeView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.PopulateTradeFilter(MiscUtils.GetSelectedValue(this.TradeFilter, this.GetFromSession(this.TradeFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_ContractTradeItemAndTradeTypeTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                View_ContractTradeItemAndTradeTypeTableControlRow recControl = (View_ContractTradeItemAndTradeTypeTableControlRow)(repItem.FindControl("View_ContractTradeItemAndTradeTypeTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_ContractTradeItemAndTradeTypeView.Trade, this.DataSource);
            this.Page.PregetDfkaRecords(View_ContractTradeItemAndTradeTypeView.trade_task_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for View_ContractTradeItemAndTradeTypeTableControl pagination.
        

            // Bind the pagination labels.
        
            this.View_ContractTradeItemAndTradeTypeTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (View_ContractTradeItemAndTradeTypeTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_ContractTradeItemAndTradeTypeView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        contractMainRecordControl parentRecordControl = (contractMainRecordControl)(this.Page.FindControlRecursively("contractMainRecordControl"));
            ContractMainRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(View_ContractTradeItemAndTradeTypeView.contract_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            if (MiscUtils.IsValueSelected(this.TradeFilter)) {
                wc.iAND(View_ContractTradeItemAndTradeTypeView.Trade, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.TradeFilter, this.GetFromSession(this.TradeFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_ContractTradeItemAndTradeTypeTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    View_ContractTradeItemAndTradeTypeTableControlRow recControl = (View_ContractTradeItemAndTradeTypeTableControlRow)(repItem.FindControl("View_ContractTradeItemAndTradeTypeTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_ContractTradeItemAndTradeTypeRecord rec = new View_ContractTradeItemAndTradeTypeRecord();
        
                        if (recControl.amount2.Text != "") {
                            rec.Parse(recControl.amount2.Text, View_ContractTradeItemAndTradeTypeView.amount);
                        }
                        if (recControl.price.Text != "") {
                            rec.Parse(recControl.price.Text, View_ContractTradeItemAndTradeTypeView.price);
                        }
                        if (recControl.Trade.Text != "") {
                            rec.Parse(recControl.Trade.Text, View_ContractTradeItemAndTradeTypeView.Trade);
                        }
                        if (recControl.trade_task_id.Text != "") {
                            rec.Parse(recControl.trade_task_id.Text, View_ContractTradeItemAndTradeTypeView.trade_task_id);
                        }
                        if (recControl.units2.Text != "") {
                            rec.Parse(recControl.units2.Text, View_ContractTradeItemAndTradeTypeView.units);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_ContractTradeItemAndTradeTypeRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_ContractTradeItemAndTradeTypeRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_ContractTradeItemAndTradeTypeRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(View_ContractTradeItemAndTradeTypeTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(View_ContractTradeItemAndTradeTypeTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for TradeFilter.
        protected virtual void PopulateTradeFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(ContractTradesTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.TradeFilter.Items.Clear();
            foreach (ContractTradesRecord itemValue in ContractTradesTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(ContractTradesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.TradeFilter.Items.IndexOf(item) < 0) {
                    this.TradeFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.TradeFilter, selectedValue);

            // Add the All item.
            this.TradeFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter TradeFilter.
        public virtual WhereClause CreateWhereClause_TradeFilter()
        {
              
            WhereClause wc = new WhereClause();
                  contractMainRecordControl parentRecordControl = (contractMainRecordControl)this.Page.FindControlRecursively("contractMainRecordControl");
            ContractMainRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
                    
            if (parentRec.id0Specified) {
                wc.iAND(View_ContractTradeItemAndTradeTypeView.contract_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.TradeFilter, this.TradeFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.TradeFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["View_ContractTradeItemAndTradeTypeTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["View_ContractTradeItemAndTradeTypeTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void View_ContractTradeItemAndTradeTypeDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void TradeFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_ContractTradeItemAndTradeTypeRecord[] _DataSource = null;
        public  View_ContractTradeItemAndTradeTypeRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal amountLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amountLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal priceLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "priceLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal trade_task_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_task_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList TradeFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TradeFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal TradeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TradeLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal TradeLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TradeLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal unitsLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unitsLabel2");
            }
        }
        
        public WKCRM.UI.IThemeButton View_ContractTradeItemAndTradeTypeDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractTradeItemAndTradeTypeDeleteButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal View_ContractTradeItemAndTradeTypeTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractTradeItemAndTradeTypeTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox View_ContractTradeItemAndTradeTypeToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractTradeItemAndTradeTypeToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label View_ContractTradeItemAndTradeTypeTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ContractTradeItemAndTradeTypeTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                View_ContractTradeItemAndTradeTypeTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_ContractTradeItemAndTradeTypeRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (View_ContractTradeItemAndTradeTypeTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_ContractTradeItemAndTradeTypeRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public View_ContractTradeItemAndTradeTypeTableControlRow GetSelectedRecordControl()
        {
        View_ContractTradeItemAndTradeTypeTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public View_ContractTradeItemAndTradeTypeTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (View_ContractTradeItemAndTradeTypeTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_ContractTradeItemAndTradeTypeRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (View_ContractTradeItemAndTradeTypeTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.View_ContractTradeItemAndTradeTypeTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            View_ContractTradeItemAndTradeTypeTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (View_ContractTradeItemAndTradeTypeTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.View_ContractTradeItemAndTradeTypeRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public View_ContractTradeItemAndTradeTypeTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("View_ContractTradeItemAndTradeTypeTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                View_ContractTradeItemAndTradeTypeTableControlRow recControl = (View_ContractTradeItemAndTradeTypeTableControlRow)repItem.FindControl("View_ContractTradeItemAndTradeTypeTableControlRow");
                recList.Add(recControl);
            }

            return (View_ContractTradeItemAndTradeTypeTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.Contract.View_ContractTradeItemAndTradeTypeTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the contractMainRecordControl control on the Contract page.
// Do not modify this class. Instead override any method in contractMainRecordControl.
public class BasecontractMainRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecontractMainRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in contractMainRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.viewContractButton.Click += new ImageClickEventHandler(viewContractButton_Click);
            this.viewCostingsButton.Click += new ImageClickEventHandler(viewCostingsButton_Click);
            this.editContract.Button.Click += new EventHandler(editContract_Click);
        }

        // To customize, override this method in contractMainRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in contractMainRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = ContractMainTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new ContractMainRecord();
                return;
            }

            // Retrieve the record from the database.
            ContractMainRecord[] recList = ContractMainTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = ContractMainTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in contractMainRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.account_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.account_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.account_id.Text = formattedValue;
            } else {  
                this.account_id.Text = ContractMainTable.account_id.Format(ContractMainTable.account_id.DefaultValue);
            }
                    
            if (this.account_id.Text == null ||
                this.account_id.Text.Trim().Length == 0) {
                this.account_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.bench_threshold_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.bench_threshold_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.bench_threshold_fee.Text = formattedValue;
            } else {  
                this.bench_threshold_fee.Text = ContractMainTable.bench_threshold_fee.Format(ContractMainTable.bench_threshold_fee.DefaultValue);
            }
                    
            if (this.DataSource.check_measure_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.check_measure_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.check_measure_fee.Text = formattedValue;
            } else {  
                this.check_measure_fee.Text = ContractMainTable.check_measure_fee.Format(ContractMainTable.check_measure_fee.DefaultValue);
            }
                    
            if (this.check_measure_fee.Text == null ||
                this.check_measure_fee.Text.Trim().Length == 0) {
                this.check_measure_fee.Text = "&nbsp;";
            }
                  
            if (this.DataSource.consultation_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.consultation_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.consultation_fee.Text = formattedValue;
            } else {  
                this.consultation_fee.Text = ContractMainTable.consultation_fee.Format(ContractMainTable.consultation_fee.DefaultValue);
            }
                    
            if (this.DataSource.contract_type_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.contract_type_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.contract_type_id.Text = formattedValue;
            } else {  
                this.contract_type_id.Text = ContractMainTable.contract_type_id.Format(ContractMainTable.contract_type_id.DefaultValue);
            }
                    
            if (this.contract_type_id.Text == null ||
                this.contract_type_id.Text.Trim().Length == 0) {
                this.contract_type_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.created_bySpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.created_by);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.created_by.Text = formattedValue;
            } else {  
                this.created_by.Text = ContractMainTable.created_by.Format(ContractMainTable.created_by.DefaultValue);
            }
                    
            if (this.created_by.Text == null ||
                this.created_by.Text.Trim().Length == 0) {
                this.created_by.Text = "&nbsp;";
            }
                  
            if (this.DataSource.date_createdSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.date_created, @"G");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.date_created.Text = formattedValue;
            } else {  
                this.date_created.Text = ContractMainTable.date_created.Format(ContractMainTable.date_created.DefaultValue, @"G");
            }
                    
            if (this.date_created.Text == null ||
                this.date_created.Text.Trim().Length == 0) {
                this.date_created.Text = "&nbsp;";
            }
                  
            if (this.DataSource.date_modifiedSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.date_modified, @"G");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.date_modified.Text = formattedValue;
            } else {  
                this.date_modified.Text = ContractMainTable.date_modified.Format(ContractMainTable.date_modified.DefaultValue, @"G");
            }
                    
            if (this.date_modified.Text == null ||
                this.date_modified.Text.Trim().Length == 0) {
                this.date_modified.Text = "&nbsp;";
            }
                  
            if (this.DataSource.designer_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.designer_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.designer_fee.Text = formattedValue;
            } else {  
                this.designer_fee.Text = ContractMainTable.designer_fee.Format(ContractMainTable.designer_fee.DefaultValue);
            }
                    
            if (this.DataSource.difficult_delivery_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.difficult_delivery_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.difficult_delivery_fee.Text = formattedValue;
            } else {  
                this.difficult_delivery_fee.Text = ContractMainTable.difficult_delivery_fee.Format(ContractMainTable.difficult_delivery_fee.DefaultValue);
            }
                    
            if (this.difficult_delivery_fee.Text == null ||
                this.difficult_delivery_fee.Text.Trim().Length == 0) {
                this.difficult_delivery_fee.Text = "&nbsp;";
            }
                  
            if (this.DataSource.door_threshold_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.door_threshold_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.door_threshold_fee.Text = formattedValue;
            } else {  
                this.door_threshold_fee.Text = ContractMainTable.door_threshold_fee.Format(ContractMainTable.door_threshold_fee.DefaultValue);
            }
                    
            if (this.door_threshold_fee.Text == null ||
                this.door_threshold_fee.Text.Trim().Length == 0) {
                this.door_threshold_fee.Text = "&nbsp;";
            }
                  
            if (this.DataSource.dual_finishes_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.dual_finishes_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.dual_finish.Text = formattedValue;
            } else {  
                this.dual_finish.Text = ContractMainTable.dual_finishes_fee.Format(ContractMainTable.dual_finishes_fee.DefaultValue);
            }
                    
            if (this.DataSource.dual_finishes_lessthan_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.dual_finishes_lessthan_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.dual_finishes_lessthan_fee.Text = formattedValue;
            } else {  
                this.dual_finishes_lessthan_fee.Text = ContractMainTable.dual_finishes_lessthan_fee.Format(ContractMainTable.dual_finishes_lessthan_fee.DefaultValue);
            }
                    
            if (this.DataSource.estimated_completion_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.estimated_completion_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.estimated_completion_date.Text = formattedValue;
            } else {  
                this.estimated_completion_date.Text = ContractMainTable.estimated_completion_date.Format(ContractMainTable.estimated_completion_date.DefaultValue);
            }
                    
            if (this.estimated_completion_date.Text == null ||
                this.estimated_completion_date.Text.Trim().Length == 0) {
                this.estimated_completion_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.highrise_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.highrise_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.highrise_fee.Text = formattedValue;
            } else {  
                this.highrise_fee.Text = ContractMainTable.highrise_fee.Format(ContractMainTable.highrise_fee.DefaultValue);
            }
                    
            if (this.DataSource.home_insurance_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.home_insurance_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.home_insurance_fee.Text = formattedValue;
            } else {  
                this.home_insurance_fee.Text = ContractMainTable.home_insurance_fee.Format(ContractMainTable.home_insurance_fee.DefaultValue);
            }
                    
            if (this.home_insurance_fee.Text == null ||
                this.home_insurance_fee.Text.Trim().Length == 0) {
                this.home_insurance_fee.Text = "&nbsp;";
            }
                  
            if (this.DataSource.home_insurance_policySpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.home_insurance_policy);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.home_insurance_policy.Text = formattedValue;
            } else {  
                this.home_insurance_policy.Text = ContractMainTable.home_insurance_policy.Format(ContractMainTable.home_insurance_policy.DefaultValue);
            }
                    
            if (this.home_insurance_policy.Text == null ||
                this.home_insurance_policy.Text.Trim().Length == 0) {
                this.home_insurance_policy.Text = "&nbsp;";
            }
                  
            if (this.DataSource.home_insurance_sentSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.home_insurance_sent);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.home_insurance_sent.Text = formattedValue;
            } else {  
                this.home_insurance_sent.Text = ContractMainTable.home_insurance_sent.Format(ContractMainTable.home_insurance_sent.DefaultValue);
            }
                    
            if (this.home_insurance_sent.Text == null ||
                this.home_insurance_sent.Text.Trim().Length == 0) {
                this.home_insurance_sent.Text = "&nbsp;";
            }
                  
            if (this.DataSource.kickboardsAfterTimber_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.kickboardsAfterTimber_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.kickboardsAfterTimberFee.Text = formattedValue;
            } else {  
                this.kickboardsAfterTimberFee.Text = ContractMainTable.kickboardsAfterTimber_fee.Format(ContractMainTable.kickboardsAfterTimber_fee.DefaultValue);
            }
                    
            if (this.DataSource.kickboardsWasherAfterTimber_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.kickboardsWasherAfterTimber_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.kickboardsWasherAfterTimber_fee.Text = formattedValue;
            } else {  
                this.kickboardsWasherAfterTimber_fee.Text = ContractMainTable.kickboardsWasherAfterTimber_fee.Format(ContractMainTable.kickboardsWasherAfterTimber_fee.DefaultValue);
            }
                    
            if (this.DataSource.modified_bySpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.modified_by);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.modified_by.Text = formattedValue;
            } else {  
                this.modified_by.Text = ContractMainTable.modified_by.Format(ContractMainTable.modified_by.DefaultValue);
            }
                    
            if (this.modified_by.Text == null ||
                this.modified_by.Text.Trim().Length == 0) {
                this.modified_by.Text = "&nbsp;";
            }
                  
            if (this.DataSource.non_metro_delivery_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.non_metro_delivery_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.non_metro_delivery_fee.Text = formattedValue;
            } else {  
                this.non_metro_delivery_fee.Text = ContractMainTable.non_metro_delivery_fee.Format(ContractMainTable.non_metro_delivery_fee.DefaultValue);
            }
                    
            if (this.non_metro_delivery_fee.Text == null ||
                this.non_metro_delivery_fee.Text.Trim().Length == 0) {
                this.non_metro_delivery_fee.Text = "&nbsp;";
            }
                  
            if (this.DataSource.original_totalSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.original_total);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.original_total.Text = formattedValue;
            } else {  
                this.original_total.Text = ContractMainTable.original_total.Format(ContractMainTable.original_total.DefaultValue);
            }
                    
            if (this.original_total.Text == null ||
                this.original_total.Text.Trim().Length == 0) {
                this.original_total.Text = "&nbsp;";
            }
                  
            if (this.DataSource.second_check_measure_feeSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.second_check_measure_fee);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.second_check_measure_fee.Text = formattedValue;
            } else {  
                this.second_check_measure_fee.Text = ContractMainTable.second_check_measure_fee.Format(ContractMainTable.second_check_measure_fee.DefaultValue);
            }
                    
            if (this.DataSource.signed_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractMainTable.signed_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.signed_date.Text = formattedValue;
            } else {  
                this.signed_date.Text = ContractMainTable.signed_date.Format(ContractMainTable.signed_date.DefaultValue);
            }
                    
            if (this.signed_date.Text == null ||
                this.signed_date.Text.Trim().Length == 0) {
                this.signed_date.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in contractMainRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in contractMainRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            ContractMainTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["Contract"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "Contract"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(ContractMainTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(ContractMainTable.id0).ToString());
            } else {
                
                wc.iAND(ContractMainTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in contractMainRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractMainTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void viewContractButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../Reports/ShowReport.aspx?reportId=5&recordId={contractMainRecordControl:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for ImageButton
        public virtual void viewCostingsButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../Reports/ShowReport.aspx?reportId=12&recordId={contractMainRecordControl:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void editContract_Click(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecontractMainRecordControl_Rec"];
            }
            set {
                this.ViewState["BasecontractMainRecordControl_Rec"] = value;
            }
        }
        
        private ContractMainRecord _DataSource;
        public ContractMainRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal account_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal account_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label bench_threshold_fee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "bench_threshold_fee");
            }
        }
           
        public System.Web.UI.WebControls.Literal check_measure_fee {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "check_measure_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal check_measure_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "check_measure_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label consultation_fee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "consultation_fee");
            }
        }
           
        public System.Web.UI.WebControls.Literal contract_type_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_type_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal contract_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_type_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image contractMainDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractMainDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal contractMainDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractMainDialogTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label contractTotal {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractTotal");
            }
        }
           
        public System.Web.UI.WebControls.Literal created_by {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "created_by");
            }
        }
        
        public System.Web.UI.WebControls.Literal created_byLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "created_byLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal date_created {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_created");
            }
        }
        
        public System.Web.UI.WebControls.Literal date_createdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_createdLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal date_modified {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_modified");
            }
        }
        
        public System.Web.UI.WebControls.Literal date_modifiedLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_modifiedLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label designer_fee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_fee");
            }
        }
           
        public System.Web.UI.WebControls.Literal difficult_delivery_fee {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "difficult_delivery_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal difficult_delivery_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "difficult_delivery_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal door_threshold_fee {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "door_threshold_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal door_threshold_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "door_threshold_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label dual_finish {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dual_finish");
            }
        }
           
        public System.Web.UI.WebControls.Label dual_finishes_lessthan_fee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dual_finishes_lessthan_fee");
            }
        }
        
        public WKCRM.UI.IThemeButton editContract {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "editContract");
            }
        }
           
        public System.Web.UI.WebControls.Literal estimated_completion_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "estimated_completion_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal estimated_completion_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "estimated_completion_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label highrise_fee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "highrise_fee");
            }
        }
        
        public System.Web.UI.WebControls.Label HighRiseLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "HighRiseLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal home_insurance_fee {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal home_insurance_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal home_insurance_policy {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_policy");
            }
        }
        
        public System.Web.UI.WebControls.Literal home_insurance_policyLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_policyLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal home_insurance_sent {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_sent");
            }
        }
        
        public System.Web.UI.WebControls.Literal home_insurance_sentLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_insurance_sentLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label kickboardsAfterTimberFee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "kickboardsAfterTimberFee");
            }
        }
           
        public System.Web.UI.WebControls.Label kickboardsWasherAfterTimber_fee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "kickboardsWasherAfterTimber_fee");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.Label Label1 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label1");
            }
        }
        
        public System.Web.UI.WebControls.Label Label2 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label2");
            }
        }
        
        public System.Web.UI.WebControls.Label Label3 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label3");
            }
        }
        
        public System.Web.UI.WebControls.Label Label4 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label4");
            }
        }
        
        public System.Web.UI.WebControls.Label Label5 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label5");
            }
        }
        
        public System.Web.UI.WebControls.Label Label6 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label6");
            }
        }
        
        public System.Web.UI.WebControls.Label Label7 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label7");
            }
        }
        
        public System.Web.UI.WebControls.Label Label8 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label8");
            }
        }
        
        public System.Web.UI.WebControls.Label Label9 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label9");
            }
        }
           
        public System.Web.UI.WebControls.Literal modified_by {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "modified_by");
            }
        }
        
        public System.Web.UI.WebControls.Literal modified_byLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "modified_byLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal non_metro_delivery_fee {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "non_metro_delivery_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal non_metro_delivery_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "non_metro_delivery_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal original_total {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "original_total");
            }
        }
        
        public System.Web.UI.WebControls.Label original_totalLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "original_totalLabel");
            }
        }
           
        public System.Web.UI.WebControls.Label second_check_measure_fee {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "second_check_measure_fee");
            }
        }
           
        public System.Web.UI.WebControls.Literal signed_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "signed_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal signed_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "signed_dateLabel");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton viewContractButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "viewContractButton");
            }
        }
        
        public System.Web.UI.WebControls.Label viewContractLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "viewContractLabel");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton viewCostingsButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "viewCostingsButton");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            ContractMainRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public ContractMainRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return ContractMainTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  