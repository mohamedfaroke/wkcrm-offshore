﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// AddContractPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.AddContractPage
{
  

#region "Section 1: Place your customizations here."

    
public class contractMainRecordControl : BasecontractMainRecordControl
{
      
        // The BasecontractMainRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            string accID = this.Page.Request["AccID"];
            if (!String.IsNullOrEmpty(accID))
            {
                this.accountName.Text = this.DataSource.Format(ContractMainTable.account_id);
            }
            //check_measure_fee.Text = Globals.CrmOptions.checkMeasureFee.ToString("$0.00");
            SecondCheckMeasureFeeLabel.Text = MyContract.SetSecondCheckMeasureFeeLabel();
            difficult_delivery_feeLabel.Text = MyContract.SetDifficultDeliveryFeeLabel();
            dualFinishes_label.Text = MyContract.SetDualFinishesFeeLabel();
            HighRiseLabel.Text = MyContract.SetHighRiseFeeLabel();
            dualFinishesLessthanLabel.Text = MyContract.SetDualFinishesLessThanFeeLabel();
            kickboardsAfterTimberLabel.Text = MyContract.SetKickboardsAfterTimberFeeLabel();
            kickboardsWasherAfterTimberLabel.Text = MyContract.SetKickboardsWasherAfterTimberFeeLabel();
          //  non_metro_delivery_feeLabel.Text += " (" + Globals.CrmOptions.deliveryoutsideMetro.ToString("$0.00") + ")";
            //DropDownList deliveryFeeList = this.Page.FindControlRecursively("DeliveryFeeList") as DropDownList;
            //if (deliveryFeeList != null)
            //{
            //    MyContract.PopulateDeliveryFeeDropdown("",deliveryFeeList);
            //}
        }
    }

    public override void Validate()
    {
        base.Validate();
        string accID = this.Page.Request["AccID"];
        string wc = String.Format("account_id={0}", accID);
        if(ContractMainTable.GetRecordCount(wc) != 0)
        {
            throw new Exception("Contract already exists for this account");
        }
    }

    public override void GetUIData()
    {
       // base.GetUIData();
        string accID = this.Page.Request["AccID"];
        this.DataSource.Parse(accID, ContractMainTable.account_id);
        this.DataSource.Parse(DateTime.Now, ContractMainTable.date_created);
        this.DataSource.Parse(DateTime.Now, ContractMainTable.date_modified);
        this.DataSource.Parse(this.Page.SystemUtils.GetUserID(), ContractMainTable.created_by);
        this.DataSource.Parse(this.Page.SystemUtils.GetUserID(), ContractMainTable.modified_by);
        this.DataSource.Parse("1000", ContractMainTable.contract_type_id);
        this.DataSource.Parse(true, ContractMainTable.status_id);

        this.DataSource.Parse(Globals.CalculateCheckMeasureFee(this.DataSource), ContractMainTable.check_measure_fee);
        this.DataSource.Parse(Globals.CalculateDeliveryFee(this.DataSource), ContractMainTable.non_metro_delivery_fee);
        this.DataSource.Parse(Globals.CalculateDoorThresholdFee(this.DataSource), ContractMainTable.door_threshold_fee);

        if(difficult_delivery_fee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.difficultDeliveryFee, ContractMainTable.difficult_delivery_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.difficult_delivery_fee);

        if(dualFinishes.Checked)
            this.DataSource.Parse(Globals.CrmOptions.dualFinishes, ContractMainTable.dual_finishes_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.dual_finishes_fee);

        if(dualFinishesLessThan.Checked)
            this.DataSource.Parse(Globals.CrmOptions.dualFinishLessFee, ContractMainTable.dual_finishes_lessthan_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.dual_finishes_lessthan_fee);

        if(HighRise.Checked)
            this.DataSource.Parse(Globals.CrmOptions.highRise, ContractMainTable.highrise_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.highrise_fee);
        
        if (SecondCheckMeasureFee.Checked)
            this.DataSource.Parse(Globals.CrmOptions.secondCheckMeasureFee,ContractMainTable.second_check_measure_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.second_check_measure_fee);

        if (KickboardsAfterTimber.Checked)
            this.DataSource.Parse(Globals.CrmOptions.kickboardsAfterTimber, ContractMainTable.kickboardsAfterTimber_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.kickboardsAfterTimber_fee);

        if (KickboardWasherAfterTimber.Checked)
            this.DataSource.Parse(Globals.CrmOptions.kickboardsWasherAfterTimber, ContractMainTable.kickboardsWasherAfterTimber_fee);
        else
            this.DataSource.Parse(0, ContractMainTable.kickboardsWasherAfterTimber_fee);


        //DropDownList DeliveryFeeList = this.Page.FindControlRecursively("DeliveryFeeList") as DropDownList;
        //if (DeliveryFeeList != null)
        //{
        //    this.DataSource.Parse(DeliveryFeeList.SelectedValue, ContractMainTable.non_metro_delivery_fee);
        //}
        //else
        //    this.DataSource.Parse(0, ContractMainTable.non_metro_delivery_fee);
    }
}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the contractMainRecordControl control on the AddContractPage page.
// Do not modify this class. Instead override any method in contractMainRecordControl.
public class BasecontractMainRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecontractMainRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in contractMainRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in contractMainRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in contractMainRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = ContractMainTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new ContractMainRecord();
                return;
            }

            // Retrieve the record from the database.
            ContractMainRecord[] recList = ContractMainTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = ContractMainTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in contractMainRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.difficult_delivery_feeSpecified) {
                if (this.DataSource.difficult_delivery_fee.ToString() == "Yes") {
                    this.difficult_delivery_fee.Checked = true;
                } else {
                    this.difficult_delivery_fee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.difficult_delivery_fee.Checked = ContractMainTable.difficult_delivery_fee.ParseValue(ContractMainTable.difficult_delivery_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.dual_finishes_feeSpecified) {
                if (this.DataSource.dual_finishes_fee.ToString() == "") {
                    this.dualFinishes.Checked = true;
                } else {
                    this.dualFinishes.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.dualFinishes.Checked = ContractMainTable.dual_finishes_fee.ParseValue(ContractMainTable.dual_finishes_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.dual_finishes_lessthan_feeSpecified) {
                if (this.DataSource.dual_finishes_lessthan_fee.ToString() == "") {
                    this.dualFinishesLessThan.Checked = true;
                } else {
                    this.dualFinishesLessThan.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.dualFinishesLessThan.Checked = ContractMainTable.dual_finishes_lessthan_fee.ParseValue(ContractMainTable.dual_finishes_lessthan_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.highrise_feeSpecified) {
                if (this.DataSource.highrise_fee.ToString() == "") {
                    this.HighRise.Checked = true;
                } else {
                    this.HighRise.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.HighRise.Checked = ContractMainTable.highrise_fee.ParseValue(ContractMainTable.highrise_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.kickboardsAfterTimber_feeSpecified) {
                if (this.DataSource.kickboardsAfterTimber_fee.ToString() == "") {
                    this.KickboardsAfterTimber.Checked = true;
                } else {
                    this.KickboardsAfterTimber.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.KickboardsAfterTimber.Checked = ContractMainTable.kickboardsAfterTimber_fee.ParseValue(ContractMainTable.kickboardsAfterTimber_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.kickboardsWasherAfterTimber_feeSpecified) {
                if (this.DataSource.kickboardsWasherAfterTimber_fee.ToString() == "") {
                    this.KickboardWasherAfterTimber.Checked = true;
                } else {
                    this.KickboardWasherAfterTimber.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.KickboardWasherAfterTimber.Checked = ContractMainTable.kickboardsWasherAfterTimber_fee.ParseValue(ContractMainTable.kickboardsWasherAfterTimber_fee.DefaultValue).ToBoolean();
                }
            }
                        
            if (this.DataSource.second_check_measure_feeSpecified) {
                if (this.DataSource.second_check_measure_fee.ToString() == "") {
                    this.SecondCheckMeasureFee.Checked = true;
                } else {
                    this.SecondCheckMeasureFee.Checked = false;
                }
            } else {
                if (!this.DataSource.IsCreated) {
                    this.SecondCheckMeasureFee.Checked = ContractMainTable.second_check_measure_fee.ParseValue(ContractMainTable.second_check_measure_fee.DefaultValue).ToBoolean();
                }
            }
                        
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in contractMainRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in contractMainRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual void GetUIData()
        {
        
            if (this.difficult_delivery_fee.Checked) {
                this.DataSource.Parse("Yes", ContractMainTable.difficult_delivery_fee);
            } else {
                this.DataSource.Parse("No", ContractMainTable.difficult_delivery_fee);
            }
                    
            if (this.dualFinishes.Checked) {
                this.DataSource.Parse("", ContractMainTable.dual_finishes_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.dual_finishes_fee);
            }
                    
            if (this.dualFinishesLessThan.Checked) {
                this.DataSource.Parse("", ContractMainTable.dual_finishes_lessthan_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.dual_finishes_lessthan_fee);
            }
                    
            if (this.HighRise.Checked) {
                this.DataSource.Parse("", ContractMainTable.highrise_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.highrise_fee);
            }
                    
            if (this.KickboardsAfterTimber.Checked) {
                this.DataSource.Parse("", ContractMainTable.kickboardsAfterTimber_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.kickboardsAfterTimber_fee);
            }
                    
            if (this.KickboardWasherAfterTimber.Checked) {
                this.DataSource.Parse("", ContractMainTable.kickboardsWasherAfterTimber_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.kickboardsWasherAfterTimber_fee);
            }
                    
            if (this.SecondCheckMeasureFee.Checked) {
                this.DataSource.Parse("", ContractMainTable.second_check_measure_fee);
            } else {
                this.DataSource.Parse("", ContractMainTable.second_check_measure_fee);
            }
                    
        }

        //  To customize, override this method in contractMainRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in contractMainRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractMainTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecontractMainRecordControl_Rec"];
            }
            set {
                this.ViewState["BasecontractMainRecordControl_Rec"] = value;
            }
        }
        
        private ContractMainRecord _DataSource;
        public ContractMainRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Label accountName {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "accountName");
            }
        }
        
        public System.Web.UI.WebControls.Image contractMainDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractMainDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal contractMainDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractMainDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox difficult_delivery_fee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "difficult_delivery_fee");
            }
        }
        
        public System.Web.UI.WebControls.Literal difficult_delivery_feeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "difficult_delivery_feeLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox dualFinishes {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dualFinishes");
            }
        }
        
        public System.Web.UI.WebControls.Label dualFinishes_label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dualFinishes_label");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox dualFinishesLessThan {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dualFinishesLessThan");
            }
        }
        
        public System.Web.UI.WebControls.Label dualFinishesLessthanLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dualFinishesLessthanLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox HighRise {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "HighRise");
            }
        }
        
        public System.Web.UI.WebControls.Label HighRiseLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "HighRiseLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox KickboardsAfterTimber {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "KickboardsAfterTimber");
            }
        }
        
        public System.Web.UI.WebControls.Label kickboardsAfterTimberLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "kickboardsAfterTimberLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label kickboardsWasherAfterTimberLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "kickboardsWasherAfterTimberLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox KickboardWasherAfterTimber {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "KickboardWasherAfterTimber");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox SecondCheckMeasureFee {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "SecondCheckMeasureFee");
            }
        }
        
        public System.Web.UI.WebControls.Label SecondCheckMeasureFeeLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "SecondCheckMeasureFeeLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            ContractMainRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public ContractMainRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new ContractMainRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  