﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// Payment.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.Payment
{
  

#region "Section 1: Place your customizations here."

    
public class paymentsRecordControl : BasepaymentsRecordControl
{
      
        // The BasepaymentsRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            string paymentScheduleID = this.Page.Request["PaymentSchedule"];
            string paymentStageID = this.Page.Request["PaymentStage"];
            this.DataSource.Parse(paymentStageID, PaymentsTable.payment_stages_id);
            paymentStage.Text = this.DataSource.Format(PaymentsTable.payment_stages_id);
            //Also want to display the amount outstanding
            KeyValue psID = new KeyValue();
            psID.AddElement("payment_schedule_id", paymentScheduleID);
            psID.AddElement("payment_stage_id", paymentStageID);
            View_PaymentScheduleOverviewRecord paySchedule = View_PaymentScheduleOverviewView.GetRecord(psID, false);
            string outstandingAmt = paySchedule.Outstanding.ToString("$0.00");
            paymentStage.Text += " (" + outstandingAmt + " outstanding)";
        }
    }

    public override void GetUIData()
    {
        base.GetUIData();

        string paymentScheduleID = this.Page.Request["PaymentSchedule"];
        string paymentStageID = this.Page.Request["PaymentStage"];

        this.DataSource.Parse(paymentStageID, PaymentsTable.payment_stages_id);
        this.DataSource.Parse(paymentScheduleID, PaymentsTable.payment_schedule_id);
        this.DataSource.Parse(DateTime.Now, PaymentsTable.datetime0);
        this.DataSource.Parse(this.Page.SystemUtils.GetUserID(), PaymentsTable.employee_id);
    }

    public override void Validate()
    {
        base.Validate();
        string paymentScheduleID = this.Page.Request["PaymentSchedule"];
        string paymentStageID = this.Page.Request["PaymentStage"];
        KeyValue psID = new KeyValue();
        psID.AddElement("payment_schedule_id", paymentScheduleID);
        psID.AddElement("payment_stage_id", paymentStageID);
        View_PaymentScheduleOverviewRecord paySchedule = View_PaymentScheduleOverviewView.GetRecord(psID, false);
        if (Convert.ToDecimal(amount.Text) > paySchedule.Outstanding)
            throw new Exception("Payment cannot be greater than outstanding amount");
    }

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the paymentsRecordControl control on the Payment page.
// Do not modify this class. Instead override any method in paymentsRecordControl.
public class BasepaymentsRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BasepaymentsRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in paymentsRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in paymentsRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in paymentsRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = PaymentsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new PaymentsRecord();
                return;
            }

            // Retrieve the record from the database.
            PaymentsRecord[] recList = PaymentsTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = PaymentsTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in paymentsRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.amount);
                this.amount.Text = formattedValue;
            } else {  
                this.amount.Text = PaymentsTable.amount.Format(PaymentsTable.amount.DefaultValue);
            }
                    
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.notes);
                this.notes.Text = formattedValue;
            } else {  
                this.notes.Text = PaymentsTable.notes.Format(PaymentsTable.notes.DefaultValue);
            }
                    
            if (this.DataSource.payment_type_idSpecified) {
                this.Populatepayment_type_idDropDownList(this.DataSource.payment_type_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatepayment_type_idDropDownList(PaymentsTable.payment_type_id.DefaultValue, 100);
                } else {
                this.Populatepayment_type_idDropDownList(null, 100);
                }
            }
                
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in paymentsRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in paymentsRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in paymentsRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in paymentsRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.amount.Text, PaymentsTable.amount);
                          
            this.DataSource.Parse(this.notes.Text, PaymentsTable.notes);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.payment_type_id), PaymentsTable.payment_type_id);
                  
        }

        //  To customize, override this method in paymentsRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in paymentsRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            PaymentsTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_payment_type_idDropDownList() {
            return new WhereClause();
        }
                
        // Fill the payment_type_id list.
        protected virtual void Populatepayment_type_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_payment_type_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(PaymentTypesTable.name, OrderByItem.OrderDir.Asc);

                      this.payment_type_id.Items.Clear();
            foreach (PaymentTypesRecord itemValue in PaymentTypesTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(PaymentTypesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.payment_type_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.payment_type_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.payment_type_id, PaymentsTable.payment_type_id.Format(selectedValue))) {
                string fvalue = PaymentsTable.payment_type_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.payment_type_id.Items.Insert(0, item);
            }

                  
            this.payment_type_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasepaymentsRecordControl_Rec"];
            }
            set {
                this.ViewState["BasepaymentsRecordControl_Rec"] = value;
            }
        }
        
        private PaymentsRecord _DataSource;
        public PaymentsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.TextBox amount {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount");
            }
        }
        
        public System.Web.UI.WebControls.Literal amountLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amountLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
           
        public System.Web.UI.WebControls.TextBox notes {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notes");
            }
        }
        
        public System.Web.UI.WebControls.Literal notesLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notesLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList payment_type_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_type_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_type_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal paymentsDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentsDialogTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label paymentStage {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentStage");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            PaymentsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public PaymentsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new PaymentsRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  