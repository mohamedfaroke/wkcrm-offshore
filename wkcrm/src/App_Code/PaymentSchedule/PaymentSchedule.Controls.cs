﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// PaymentSchedule.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.PaymentSchedule
{
  

#region "Section 1: Place your customizations here."

    
public class View_PaymentScheduleOverviewTableControlRow : BaseView_PaymentScheduleOverviewTableControlRow
{
      
        // The BaseView_PaymentScheduleOverviewTableControlRow implements code for a ROW within the
        // the View_PaymentScheduleOverviewTableControl table.  The BaseView_PaymentScheduleOverviewTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of View_PaymentScheduleOverviewTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            if (Math.Floor(this.DataSource.Outstanding) > 0)
            {
                string args = "?PaymentSchedule=" + this.DataSource.payment_schedule_id.ToString() 
                              + "&PaymentStage=" + this.DataSource.payment_stage_id.ToString();
                paymentButton.OnClientClick = "OpenWindow('./Payment.aspx" + args + "','StandardWindow')";
                variationsButton.OnClientClick = "OpenWindow('./Variation.aspx" + args + "','StandardWindow')";
                //Do it here cause cant be stuffed to change via ironspeed
                variationsButton.ImageUrl = "../Images/hammer.png";
            }
            else
            {
                // Can't make payments or variations of total has already been fully paid
                paymentButton.ImageUrl = "../Images/payment_disabled.gif";
                paymentButton.Enabled = false;
                variationsButton.ImageUrl = "../Images/hammer_disabled.png";
                variationsButton.Enabled = false;
            }
        }
    }
}

  

public class View_PaymentScheduleOverviewTableControl : BaseView_PaymentScheduleOverviewTableControl
{
        // The BaseView_PaymentScheduleOverviewTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The View_PaymentScheduleOverviewTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  

public class paymentVariationsTableControl : BasepaymentVariationsTableControl
{
        // The BasepaymentVariationsTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The paymentVariationsTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class paymentVariationsTableControlRow : BasepaymentVariationsTableControlRow
{
      
        // The BasepaymentVariationsTableControlRow implements code for a ROW within the
        // the paymentVariationsTableControl table.  The BasepaymentVariationsTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of paymentVariationsTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}
public class paymentsTableControl : BasepaymentsTableControl
{
        // The BasepaymentsTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The paymentsTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class paymentsTableControlRow : BasepaymentsTableControlRow
{
      
        // The BasepaymentsTableControlRow implements code for a ROW within the
        // the paymentsTableControl table.  The BasepaymentsTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of paymentsTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}
#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the paymentsTableControlRow control on the PaymentSchedule page.
// Do not modify this class. Instead override any method in paymentsTableControlRow.
public class BasepaymentsTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasepaymentsTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in paymentsTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.paymentReportButton.Click += new ImageClickEventHandler(paymentReportButton_Click);
        }

        // To customize, override this method in paymentsTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in paymentsTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = PaymentsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasepaymentsTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new PaymentsRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in paymentsTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.amount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.amount1.Text = formattedValue;
            } else {  
                this.amount1.Text = PaymentsTable.amount.Format(PaymentsTable.amount.DefaultValue);
            }
                    
            if (this.amount1.Text == null ||
                this.amount1.Text.Trim().Length == 0) {
                this.amount1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime2.Text = formattedValue;
            } else {  
                this.datetime2.Text = PaymentsTable.datetime0.Format(PaymentsTable.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime2.Text == null ||
                this.datetime2.Text.Trim().Length == 0) {
                this.datetime2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.employee_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.employee_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.employee_id1.Text = formattedValue;
            } else {  
                this.employee_id1.Text = PaymentsTable.employee_id.Format(PaymentsTable.employee_id.DefaultValue);
            }
                    
            if (this.employee_id1.Text == null ||
                this.employee_id1.Text.Trim().Length == 0) {
                this.employee_id1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.notes);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.PaymentsTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"notes\\\", \\\"Notes\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.notes1.Text = formattedValue;
            } else {  
                this.notes1.Text = PaymentsTable.notes.Format(PaymentsTable.notes.DefaultValue);
            }
                    
            if (this.notes1.Text == null ||
                this.notes1.Text.Trim().Length == 0) {
                this.notes1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.payment_stages_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.payment_stages_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.payment_stages_id.Text = formattedValue;
            } else {  
                this.payment_stages_id.Text = PaymentsTable.payment_stages_id.Format(PaymentsTable.payment_stages_id.DefaultValue);
            }
                    
            if (this.payment_stages_id.Text == null ||
                this.payment_stages_id.Text.Trim().Length == 0) {
                this.payment_stages_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.payment_type_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentsTable.payment_type_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.payment_type_id.Text = formattedValue;
            } else {  
                this.payment_type_id.Text = PaymentsTable.payment_type_id.Format(PaymentsTable.payment_type_id.DefaultValue);
            }
                    
            if (this.payment_type_id.Text == null ||
                this.payment_type_id.Text.Trim().Length == 0) {
                this.payment_type_id.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in paymentsTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in paymentsTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in paymentsTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((paymentsTableControl)MiscUtils.GetParentControlObject(this, "paymentsTableControl")).DataChanged = true;
                ((paymentsTableControl)MiscUtils.GetParentControlObject(this, "paymentsTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in paymentsTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in paymentsTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in paymentsTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            PaymentsTable.DeleteRecord(pk);

          
            ((paymentsTableControl)MiscUtils.GetParentControlObject(this, "paymentsTableControl")).DataChanged = true;
            ((paymentsTableControl)MiscUtils.GetParentControlObject(this, "paymentsTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void paymentReportButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../Reports/ShowReport.aspx?recordId={paymentsTableControlRow:FV:id}&reportID=4";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasepaymentsTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasepaymentsTableControlRow_Rec"] = value;
            }
        }
        
        private PaymentsRecord _DataSource;
        public PaymentsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal amount1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount1");
            }
        }
           
        public System.Web.UI.WebControls.Literal datetime2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime2");
            }
        }
           
        public System.Web.UI.WebControls.Literal employee_id1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_id1");
            }
        }
           
        public System.Web.UI.WebControls.Literal notes1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notes1");
            }
        }
           
        public System.Web.UI.WebControls.Literal payment_stages_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stages_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal payment_type_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_type_id");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton paymentReportButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentReportButton");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            PaymentsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public PaymentsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return PaymentsTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the paymentsTableControl control on the PaymentSchedule page.
// Do not modify this class. Instead override any method in paymentsTableControl.
public class BasepaymentsTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasepaymentsTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        
            this.datetimeLabel1.Click += new EventHandler(datetimeLabel1_Click);

            // Setup the button events.
        

            // Setup the filter and search events.
        
            this.payment_stages_idFilter.SelectedIndexChanged += new EventHandler(payment_stages_idFilter_SelectedIndexChanged);
            this.payment_type_idFilter.SelectedIndexChanged += new EventHandler(payment_type_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.payment_stages_idFilter)) {
                this.payment_stages_idFilter.Items.Add(new ListItem(this.GetFromSession(this.payment_stages_idFilter), this.GetFromSession(this.payment_stages_idFilter)));
                this.payment_stages_idFilter.SelectedValue = this.GetFromSession(this.payment_stages_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.payment_type_idFilter)) {
                this.payment_type_idFilter.Items.Add(new ListItem(this.GetFromSession(this.payment_type_idFilter), this.GetFromSession(this.payment_type_idFilter)));
                this.payment_type_idFilter.SelectedValue = this.GetFromSession(this.payment_type_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(PaymentsTable.datetime0, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (PaymentsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.PaymentsRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = PaymentsTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (PaymentsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.PaymentsRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (paymentsTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (PaymentsRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.PaymentsRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = PaymentsTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            this.amountGrandTotal.Text = this.GetamountGrandTotal();
            if (this.amountGrandTotal.Text == null || 
                this.amountGrandTotal.Text.Length == 0) {
                this.amountGrandTotal.Text = "&nbsp;";
            }
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populatepayment_stages_idFilter(MiscUtils.GetSelectedValue(this.payment_stages_idFilter, this.GetFromSession(this.payment_stages_idFilter)), 500);
            this.Populatepayment_type_idFilter(MiscUtils.GetSelectedValue(this.payment_type_idFilter, this.GetFromSession(this.payment_type_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("paymentsTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                paymentsTableControlRow recControl = (paymentsTableControlRow)(repItem.FindControl("paymentsTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(PaymentsTable.employee_id, this.DataSource);
            this.Page.PregetDfkaRecords(PaymentsTable.payment_stages_id, this.DataSource);
            this.Page.PregetDfkaRecords(PaymentsTable.payment_type_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for paymentsTableControl pagination.
        

            // Bind the pagination labels.
        
            this.paymentsTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (paymentsTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            PaymentsTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            // Get the static clause defined at design time on the Table Panel Wizard
            WhereClause qc = this.CreateQueryClause();
            if (qc != null) {
                wc.iAND(qc);
            }
        
            if (MiscUtils.IsValueSelected(this.payment_stages_idFilter)) {
                wc.iAND(PaymentsTable.payment_stages_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.payment_stages_idFilter, this.GetFromSession(this.payment_stages_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.payment_type_idFilter)) {
                wc.iAND(PaymentsTable.payment_type_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.payment_type_idFilter, this.GetFromSession(this.payment_type_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        // Create a where clause for the Static clause defined at design time.
        protected virtual WhereClause CreateQueryClause()
        {
            CompoundFilter filter = new CompoundFilter(CompoundFilter.CompoundingOperators.And_Operator, null);

            filter.AddFilter(new BaseClasses.Data.ParameterValueFilter(BaseClasses.Data.BaseTable.CreateInstance(@"WKCRM.Business.PaymentsTable, App_Code").TableDefinition.ColumnList.GetByUniqueName(@"Payments_.payment_schedule_id"), @"PaymentSchedule", BaseClasses.Data.BaseFilter.ComparisonOperator.EqualsTo, false, new BaseClasses.Data.IdentifierAliasInfo(@"", null)));

            WhereClause whereClause = new WhereClause();
            whereClause.AddFilter(filter, CompoundFilter.CompoundingOperators.And_Operator);

            return whereClause;

        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("paymentsTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    paymentsTableControlRow recControl = (paymentsTableControlRow)(repItem.FindControl("paymentsTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        PaymentsRecord rec = new PaymentsRecord();
        
                        if (recControl.amount1.Text != "") {
                            rec.Parse(recControl.amount1.Text, PaymentsTable.amount);
                        }
                        if (recControl.datetime2.Text != "") {
                            rec.Parse(recControl.datetime2.Text, PaymentsTable.datetime0);
                        }
                        if (recControl.employee_id1.Text != "") {
                            rec.Parse(recControl.employee_id1.Text, PaymentsTable.employee_id);
                        }
                        if (recControl.notes1.Text != "") {
                            rec.Parse(recControl.notes1.Text, PaymentsTable.notes);
                        }
                        if (recControl.payment_stages_id.Text != "") {
                            rec.Parse(recControl.payment_stages_id.Text, PaymentsTable.payment_stages_id);
                        }
                        if (recControl.payment_type_id.Text != "") {
                            rec.Parse(recControl.payment_type_id.Text, PaymentsTable.payment_type_id);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new PaymentsRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (PaymentsRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.PaymentsRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(paymentsTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(paymentsTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for payment_stages_idFilter.
        protected virtual void Populatepayment_stages_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(PaymentStagesTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.payment_stages_idFilter.Items.Clear();
            foreach (PaymentStagesRecord itemValue in PaymentStagesTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(PaymentStagesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.payment_stages_idFilter.Items.IndexOf(item) < 0) {
                    this.payment_stages_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.payment_stages_idFilter, selectedValue);

            // Add the All item.
            this.payment_stages_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for payment_type_idFilter.
        protected virtual void Populatepayment_type_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(PaymentTypesTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.payment_type_idFilter.Items.Clear();
            foreach (PaymentTypesRecord itemValue in PaymentTypesTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(PaymentTypesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.payment_type_idFilter.Items.IndexOf(item) < 0) {
                    this.payment_type_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.payment_type_idFilter, selectedValue);

            // Add the All item.
            this.payment_type_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter payment_stages_idFilter.
        public virtual WhereClause CreateWhereClause_payment_stages_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.payment_type_idFilter)) {
                wc.iAND(PaymentsTable.payment_type_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.payment_type_idFilter, this.GetFromSession(this.payment_type_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter payment_type_idFilter.
        public virtual WhereClause CreateWhereClause_payment_type_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.payment_stages_idFilter)) {
                wc.iAND(PaymentsTable.payment_stages_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.payment_stages_idFilter, this.GetFromSession(this.payment_stages_idFilter)), false, false);
            }
                  
            return wc;
        }
            
        protected virtual string GetamountGrandTotal()
        {
            WhereClause wc = this.CreateWhereClause();
            OrderBy orderBy = this.CreateOrderBy();
              
            string amountSum = PaymentsTable.GetSum(PaymentsTable.amount, wc, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
                
            return PaymentsTable.amount.Format(amountSum);
        }
          
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.payment_stages_idFilter, this.payment_stages_idFilter.SelectedValue);
            this.SaveToSession(this.payment_type_idFilter, this.payment_type_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.payment_stages_idFilter);
            this.RemoveFromSession(this.payment_type_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["paymentsTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["paymentsTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void datetimeLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(PaymentsTable.datetime0);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(PaymentsTable.datetime0, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void payment_stages_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void payment_type_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private PaymentsRecord[] _DataSource = null;
        public  PaymentsRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Label amountGrandTotal {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amountGrandTotal");
            }
        }
        
        public System.Web.UI.WebControls.Literal amountLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amountLabel1");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton datetimeLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel2");
            }
        }
        
        public System.Web.UI.WebControls.Literal notesLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notesLabel1");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList payment_stages_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stages_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_stages_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stages_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_stages_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stages_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList payment_type_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_type_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_type_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_type_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_type_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal paymentsTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentsTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label paymentsTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentsTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                paymentsTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                PaymentsRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public paymentsTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public paymentsTableControlRow[] GetSelectedRecordControls()
        {
        
            return (paymentsTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.PaymentSchedule.paymentsTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            paymentsTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (paymentsTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public paymentsTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("paymentsTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                paymentsTableControlRow recControl = (paymentsTableControlRow)repItem.FindControl("paymentsTableControlRow");
                recList.Add(recControl);
            }

            return (paymentsTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.PaymentSchedule.paymentsTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the paymentVariationsTableControlRow control on the PaymentSchedule page.
// Do not modify this class. Instead override any method in paymentVariationsTableControlRow.
public class BasepaymentVariationsTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasepaymentVariationsTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in paymentVariationsTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.variationReportButton.Click += new ImageClickEventHandler(variationReportButton_Click);
        }

        // To customize, override this method in paymentVariationsTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in paymentVariationsTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = PaymentVariationsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasepaymentVariationsTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new PaymentVariationsRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in paymentVariationsTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.amountSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentVariationsTable.amount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.amount.Text = formattedValue;
            } else {  
                this.amount.Text = PaymentVariationsTable.amount.Format(PaymentVariationsTable.amount.DefaultValue);
            }
                    
            if (this.amount.Text == null ||
                this.amount.Text.Trim().Length == 0) {
                this.amount.Text = "&nbsp;";
            }
                  
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(PaymentVariationsTable.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime1.Text = formattedValue;
            } else {  
                this.datetime1.Text = PaymentVariationsTable.datetime0.Format(PaymentVariationsTable.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime1.Text == null ||
                this.datetime1.Text.Trim().Length == 0) {
                this.datetime1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.employee_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentVariationsTable.employee_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.employee_id.Text = formattedValue;
            } else {  
                this.employee_id.Text = PaymentVariationsTable.employee_id.Format(PaymentVariationsTable.employee_id.DefaultValue);
            }
                    
            if (this.employee_id.Text == null ||
                this.employee_id.Text.Trim().Length == 0) {
                this.employee_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentVariationsTable.notes);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.PaymentVariationsTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"notes\\\", \\\"Notes\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.notes.Text = formattedValue;
            } else {  
                this.notes.Text = PaymentVariationsTable.notes.Format(PaymentVariationsTable.notes.DefaultValue);
            }
                    
            if (this.notes.Text == null ||
                this.notes.Text.Trim().Length == 0) {
                this.notes.Text = "&nbsp;";
            }
                  
            if (this.DataSource.payment_stage_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(PaymentVariationsTable.payment_stage_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.payment_stage_id1.Text = formattedValue;
            } else {  
                this.payment_stage_id1.Text = PaymentVariationsTable.payment_stage_id.Format(PaymentVariationsTable.payment_stage_id.DefaultValue);
            }
                    
            if (this.payment_stage_id1.Text == null ||
                this.payment_stage_id1.Text.Trim().Length == 0) {
                this.payment_stage_id1.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in paymentVariationsTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in paymentVariationsTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in paymentVariationsTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((paymentVariationsTableControl)MiscUtils.GetParentControlObject(this, "paymentVariationsTableControl")).DataChanged = true;
                ((paymentVariationsTableControl)MiscUtils.GetParentControlObject(this, "paymentVariationsTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in paymentVariationsTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in paymentVariationsTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in paymentVariationsTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            PaymentVariationsTable.DeleteRecord(pk);

          
            ((paymentVariationsTableControl)MiscUtils.GetParentControlObject(this, "paymentVariationsTableControl")).DataChanged = true;
            ((paymentVariationsTableControl)MiscUtils.GetParentControlObject(this, "paymentVariationsTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void variationReportButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../Reports/ShowReport.aspx?reportID=3&recordId={paymentVariationsTableControlRow:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasepaymentVariationsTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasepaymentVariationsTableControlRow_Rec"] = value;
            }
        }
        
        private PaymentVariationsRecord _DataSource;
        public PaymentVariationsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal amount {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amount");
            }
        }
           
        public System.Web.UI.WebControls.Literal datetime1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime1");
            }
        }
           
        public System.Web.UI.WebControls.Literal employee_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal notes {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notes");
            }
        }
           
        public System.Web.UI.WebControls.Literal payment_stage_id1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stage_id1");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton variationReportButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "variationReportButton");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            PaymentVariationsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public PaymentVariationsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return PaymentVariationsTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the paymentVariationsTableControl control on the PaymentSchedule page.
// Do not modify this class. Instead override any method in paymentVariationsTableControl.
public class BasepaymentVariationsTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasepaymentVariationsTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        
            this.datetimeLabel.Click += new EventHandler(datetimeLabel_Click);

            // Setup the button events.
        

            // Setup the filter and search events.
        
            this.employee_idFilter.SelectedIndexChanged += new EventHandler(employee_idFilter_SelectedIndexChanged);
            this.payment_stage_idFilter.SelectedIndexChanged += new EventHandler(payment_stage_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.employee_idFilter)) {
                this.employee_idFilter.Items.Add(new ListItem(this.GetFromSession(this.employee_idFilter), this.GetFromSession(this.employee_idFilter)));
                this.employee_idFilter.SelectedValue = this.GetFromSession(this.employee_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.payment_stage_idFilter)) {
                this.payment_stage_idFilter.Items.Add(new ListItem(this.GetFromSession(this.payment_stage_idFilter), this.GetFromSession(this.payment_stage_idFilter)));
                this.payment_stage_idFilter.SelectedValue = this.GetFromSession(this.payment_stage_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(PaymentVariationsTable.datetime0, OrderByItem.OrderDir.Desc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (PaymentVariationsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.PaymentVariationsRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = PaymentVariationsTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (PaymentVariationsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.PaymentVariationsRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (paymentVariationsTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (PaymentVariationsRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.PaymentVariationsRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = PaymentVariationsTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateemployee_idFilter(MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), 500);
            this.Populatepayment_stage_idFilter(MiscUtils.GetSelectedValue(this.payment_stage_idFilter, this.GetFromSession(this.payment_stage_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("paymentVariationsTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                paymentVariationsTableControlRow recControl = (paymentVariationsTableControlRow)(repItem.FindControl("paymentVariationsTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(PaymentVariationsTable.employee_id, this.DataSource);
            this.Page.PregetDfkaRecords(PaymentVariationsTable.payment_stage_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for paymentVariationsTableControl pagination.
        

            // Bind the pagination labels.
        
            this.paymentVariationsTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (paymentVariationsTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            PaymentVariationsTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            // Get the static clause defined at design time on the Table Panel Wizard
            WhereClause qc = this.CreateQueryClause();
            if (qc != null) {
                wc.iAND(qc);
            }
        
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(PaymentVariationsTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.payment_stage_idFilter)) {
                wc.iAND(PaymentVariationsTable.payment_stage_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.payment_stage_idFilter, this.GetFromSession(this.payment_stage_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        // Create a where clause for the Static clause defined at design time.
        protected virtual WhereClause CreateQueryClause()
        {
            CompoundFilter filter = new CompoundFilter(CompoundFilter.CompoundingOperators.And_Operator, null);

            filter.AddFilter(new BaseClasses.Data.ParameterValueFilter(BaseClasses.Data.BaseTable.CreateInstance(@"WKCRM.Business.PaymentVariationsTable, App_Code").TableDefinition.ColumnList.GetByUniqueName(@"PaymentVariations_.payment_schedule_id"), @"PaymentSchedule", BaseClasses.Data.BaseFilter.ComparisonOperator.EqualsTo, false, new BaseClasses.Data.IdentifierAliasInfo(@"", null)));

            WhereClause whereClause = new WhereClause();
            whereClause.AddFilter(filter, CompoundFilter.CompoundingOperators.And_Operator);

            return whereClause;

        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("paymentVariationsTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    paymentVariationsTableControlRow recControl = (paymentVariationsTableControlRow)(repItem.FindControl("paymentVariationsTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        PaymentVariationsRecord rec = new PaymentVariationsRecord();
        
                        if (recControl.amount.Text != "") {
                            rec.Parse(recControl.amount.Text, PaymentVariationsTable.amount);
                        }
                        if (recControl.datetime1.Text != "") {
                            rec.Parse(recControl.datetime1.Text, PaymentVariationsTable.datetime0);
                        }
                        if (recControl.employee_id.Text != "") {
                            rec.Parse(recControl.employee_id.Text, PaymentVariationsTable.employee_id);
                        }
                        if (recControl.notes.Text != "") {
                            rec.Parse(recControl.notes.Text, PaymentVariationsTable.notes);
                        }
                        if (recControl.payment_stage_id1.Text != "") {
                            rec.Parse(recControl.payment_stage_id1.Text, PaymentVariationsTable.payment_stage_id);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new PaymentVariationsRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (PaymentVariationsRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.PaymentVariationsRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(paymentVariationsTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(paymentVariationsTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for employee_idFilter.
        protected virtual void Populateemployee_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.employee_idFilter.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.employee_idFilter.Items.IndexOf(item) < 0) {
                    this.employee_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.employee_idFilter, selectedValue);

            // Add the All item.
            this.employee_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for payment_stage_idFilter.
        protected virtual void Populatepayment_stage_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(PaymentStagesTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.payment_stage_idFilter.Items.Clear();
            foreach (PaymentStagesRecord itemValue in PaymentStagesTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(PaymentStagesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.payment_stage_idFilter.Items.IndexOf(item) < 0) {
                    this.payment_stage_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.payment_stage_idFilter, selectedValue);

            // Add the All item.
            this.payment_stage_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter employee_idFilter.
        public virtual WhereClause CreateWhereClause_employee_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.payment_stage_idFilter)) {
                wc.iAND(PaymentVariationsTable.payment_stage_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.payment_stage_idFilter, this.GetFromSession(this.payment_stage_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter payment_stage_idFilter.
        public virtual WhereClause CreateWhereClause_payment_stage_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(PaymentVariationsTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.employee_idFilter, this.employee_idFilter.SelectedValue);
            this.SaveToSession(this.payment_stage_idFilter, this.payment_stage_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.employee_idFilter);
            this.RemoveFromSession(this.payment_stage_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["paymentVariationsTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["paymentVariationsTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void datetimeLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(PaymentVariationsTable.datetime0);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(PaymentVariationsTable.datetime0, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void employee_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void payment_stage_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private PaymentVariationsRecord[] _DataSource = null;
        public  PaymentVariationsRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal amountLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "amountLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton datetimeLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList employee_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal notesLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "notesLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList payment_stage_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stage_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_stage_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stage_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_stage_idLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stage_idLabel2");
            }
        }
        
        public System.Web.UI.WebControls.Literal paymentVariationsTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentVariationsTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label paymentVariationsTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentVariationsTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                paymentVariationsTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                PaymentVariationsRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public paymentVariationsTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public paymentVariationsTableControlRow[] GetSelectedRecordControls()
        {
        
            return (paymentVariationsTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.PaymentSchedule.paymentVariationsTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            paymentVariationsTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (paymentVariationsTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public paymentVariationsTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("paymentVariationsTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                paymentVariationsTableControlRow recControl = (paymentVariationsTableControlRow)repItem.FindControl("paymentVariationsTableControlRow");
                recList.Add(recControl);
            }

            return (paymentVariationsTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.PaymentSchedule.paymentVariationsTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the View_PaymentScheduleOverviewTableControlRow control on the PaymentSchedule page.
// Do not modify this class. Instead override any method in View_PaymentScheduleOverviewTableControlRow.
public class BaseView_PaymentScheduleOverviewTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseView_PaymentScheduleOverviewTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.paymentButton.Click += new ImageClickEventHandler(paymentButton_Click);
            this.variationsButton.Click += new ImageClickEventHandler(variationsButton_Click);
        }

        // To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_PaymentScheduleOverviewView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseView_PaymentScheduleOverviewTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_PaymentScheduleOverviewRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.OutstandingSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_PaymentScheduleOverviewView.Outstanding);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.Outstanding.Text = formattedValue;
            } else {  
                this.Outstanding.Text = View_PaymentScheduleOverviewView.Outstanding.Format(View_PaymentScheduleOverviewView.Outstanding.DefaultValue);
            }
                    
            if (this.Outstanding.Text == null ||
                this.Outstanding.Text.Trim().Length == 0) {
                this.Outstanding.Text = "&nbsp;";
            }
                  
            if (this.DataSource.payment_stage_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_PaymentScheduleOverviewView.payment_stage_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.payment_stage_id.Text = formattedValue;
            } else {  
                this.payment_stage_id.Text = View_PaymentScheduleOverviewView.payment_stage_id.Format(View_PaymentScheduleOverviewView.payment_stage_id.DefaultValue);
            }
                    
            if (this.payment_stage_id.Text == null ||
                this.payment_stage_id.Text.Trim().Length == 0) {
                this.payment_stage_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.TotalReceivedSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_PaymentScheduleOverviewView.TotalReceived);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.Received.Text = formattedValue;
            } else {  
                this.Received.Text = View_PaymentScheduleOverviewView.TotalReceived.Format(View_PaymentScheduleOverviewView.TotalReceived.DefaultValue);
            }
                    
            if (this.DataSource.TotalAmountSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_PaymentScheduleOverviewView.TotalAmount);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.TotalAmount.Text = formattedValue;
            } else {  
                this.TotalAmount.Text = View_PaymentScheduleOverviewView.TotalAmount.Format(View_PaymentScheduleOverviewView.TotalAmount.DefaultValue);
            }
                    
            if (this.TotalAmount.Text == null ||
                this.TotalAmount.Text.Trim().Length == 0) {
                this.TotalAmount.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in View_PaymentScheduleOverviewTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in View_PaymentScheduleOverviewTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((View_PaymentScheduleOverviewTableControl)MiscUtils.GetParentControlObject(this, "View_PaymentScheduleOverviewTableControl")).DataChanged = true;
                ((View_PaymentScheduleOverviewTableControl)MiscUtils.GetParentControlObject(this, "View_PaymentScheduleOverviewTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in View_PaymentScheduleOverviewTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_PaymentScheduleOverviewView.DeleteRecord(pk);

          
            ((View_PaymentScheduleOverviewTableControl)MiscUtils.GetParentControlObject(this, "View_PaymentScheduleOverviewTableControl")).DataChanged = true;
            ((View_PaymentScheduleOverviewTableControl)MiscUtils.GetParentControlObject(this, "View_PaymentScheduleOverviewTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void paymentButton_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void variationsButton_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseView_PaymentScheduleOverviewTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseView_PaymentScheduleOverviewTableControlRow_Rec"] = value;
            }
        }
        
        private View_PaymentScheduleOverviewRecord _DataSource;
        public View_PaymentScheduleOverviewRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal Outstanding {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Outstanding");
            }
        }
           
        public System.Web.UI.WebControls.Literal payment_stage_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stage_id");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton paymentButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "paymentButton");
            }
        }
           
        public System.Web.UI.WebControls.Literal Received {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Received");
            }
        }
           
        public System.Web.UI.WebControls.Literal TotalAmount {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TotalAmount");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton variationsButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "variationsButton");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_PaymentScheduleOverviewRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_PaymentScheduleOverviewRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_PaymentScheduleOverviewView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the View_PaymentScheduleOverviewTableControl control on the PaymentSchedule page.
// Do not modify this class. Instead override any method in View_PaymentScheduleOverviewTableControl.
public class BaseView_PaymentScheduleOverviewTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseView_PaymentScheduleOverviewTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_PaymentScheduleOverviewView.displayOrder, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_PaymentScheduleOverviewRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_PaymentScheduleOverviewRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_PaymentScheduleOverviewView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_PaymentScheduleOverviewRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_PaymentScheduleOverviewRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (View_PaymentScheduleOverviewTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_PaymentScheduleOverviewRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_PaymentScheduleOverviewRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_PaymentScheduleOverviewView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            this.OutstandingGrandTotal.Text = this.GetOutstandingGrandTotal();
            if (this.OutstandingGrandTotal.Text == null || 
                this.OutstandingGrandTotal.Text.Length == 0) {
                this.OutstandingGrandTotal.Text = "&nbsp;";
            }
            this.ReceivedGrandTotal.Text = this.GetReceivedGrandTotal();
            if (this.ReceivedGrandTotal.Text == null || 
                this.ReceivedGrandTotal.Text.Length == 0) {
                this.ReceivedGrandTotal.Text = "&nbsp;";
            }
            this.TotalAmountGrandTotal.Text = this.GetTotalAmountGrandTotal();
            if (this.TotalAmountGrandTotal.Text == null || 
                this.TotalAmountGrandTotal.Text.Length == 0) {
                this.TotalAmountGrandTotal.Text = "&nbsp;";
            }
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_PaymentScheduleOverviewTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                View_PaymentScheduleOverviewTableControlRow recControl = (View_PaymentScheduleOverviewTableControlRow)(repItem.FindControl("View_PaymentScheduleOverviewTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_PaymentScheduleOverviewView.payment_stage_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for View_PaymentScheduleOverviewTableControl pagination.
        

            // Bind the pagination labels.
        
            this.View_PaymentScheduleOverviewTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (View_PaymentScheduleOverviewTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_PaymentScheduleOverviewView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            // Get the static clause defined at design time on the Table Panel Wizard
            WhereClause qc = this.CreateQueryClause();
            if (qc != null) {
                wc.iAND(qc);
            }
        
            return (wc);
        }
        
        // Create a where clause for the Static clause defined at design time.
        protected virtual WhereClause CreateQueryClause()
        {
            CompoundFilter filter = new CompoundFilter(CompoundFilter.CompoundingOperators.And_Operator, null);

            filter.AddFilter(new BaseClasses.Data.ParameterValueFilter(BaseClasses.Data.BaseTable.CreateInstance(@"WKCRM.Business.View_PaymentScheduleOverviewView, App_Code").TableDefinition.ColumnList.GetByUniqueName(@"View_PaymentScheduleOverview_.payment_schedule_id"), @"PaymentSchedule", BaseClasses.Data.BaseFilter.ComparisonOperator.EqualsTo, false, new BaseClasses.Data.IdentifierAliasInfo(@"", null)));

            WhereClause whereClause = new WhereClause();
            whereClause.AddFilter(filter, CompoundFilter.CompoundingOperators.And_Operator);

            return whereClause;

        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_PaymentScheduleOverviewTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    View_PaymentScheduleOverviewTableControlRow recControl = (View_PaymentScheduleOverviewTableControlRow)(repItem.FindControl("View_PaymentScheduleOverviewTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_PaymentScheduleOverviewRecord rec = new View_PaymentScheduleOverviewRecord();
        
                        if (recControl.Outstanding.Text != "") {
                            rec.Parse(recControl.Outstanding.Text, View_PaymentScheduleOverviewView.Outstanding);
                        }
                        if (recControl.payment_stage_id.Text != "") {
                            rec.Parse(recControl.payment_stage_id.Text, View_PaymentScheduleOverviewView.payment_stage_id);
                        }
                        if (recControl.Received.Text != "") {
                            rec.Parse(recControl.Received.Text, View_PaymentScheduleOverviewView.TotalReceived);
                        }
                        if (recControl.TotalAmount.Text != "") {
                            rec.Parse(recControl.TotalAmount.Text, View_PaymentScheduleOverviewView.TotalAmount);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_PaymentScheduleOverviewRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_PaymentScheduleOverviewRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_PaymentScheduleOverviewRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(View_PaymentScheduleOverviewTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(View_PaymentScheduleOverviewTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        protected virtual string GetOutstandingGrandTotal()
        {
            WhereClause wc = this.CreateWhereClause();
            OrderBy orderBy = this.CreateOrderBy();
              
            string OutstandingSum = View_PaymentScheduleOverviewView.GetSum(View_PaymentScheduleOverviewView.Outstanding, wc, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
                
            return View_PaymentScheduleOverviewView.Outstanding.Format(OutstandingSum);
        }
          
        protected virtual string GetReceivedGrandTotal()
        {
            WhereClause wc = this.CreateWhereClause();
            OrderBy orderBy = this.CreateOrderBy();
              
            string TotalReceivedSum = View_PaymentScheduleOverviewView.GetSum(View_PaymentScheduleOverviewView.TotalReceived, wc, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
                
            return View_PaymentScheduleOverviewView.TotalReceived.Format(TotalReceivedSum);
        }
          
        protected virtual string GetTotalAmountGrandTotal()
        {
            WhereClause wc = this.CreateWhereClause();
            OrderBy orderBy = this.CreateOrderBy();
              
            string TotalAmountSum = View_PaymentScheduleOverviewView.GetSum(View_PaymentScheduleOverviewView.TotalAmount, wc, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
                
            return View_PaymentScheduleOverviewView.TotalAmount.Format(TotalAmountSum);
        }
          
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["View_PaymentScheduleOverviewTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["View_PaymentScheduleOverviewTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_PaymentScheduleOverviewRecord[] _DataSource = null;
        public  View_PaymentScheduleOverviewRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Label OutstandingGrandTotal {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OutstandingGrandTotal");
            }
        }
        
        public System.Web.UI.WebControls.Literal OutstandingLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OutstandingLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal payment_stage_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "payment_stage_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label ReceivedGrandTotal {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "ReceivedGrandTotal");
            }
        }
        
        public System.Web.UI.WebControls.Literal ReceivedLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "ReceivedLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label TotalAmountGrandTotal {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TotalAmountGrandTotal");
            }
        }
        
        public System.Web.UI.WebControls.Literal TotalAmountLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TotalAmountLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal View_PaymentScheduleOverviewTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_PaymentScheduleOverviewTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label View_PaymentScheduleOverviewTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_PaymentScheduleOverviewTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                View_PaymentScheduleOverviewTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_PaymentScheduleOverviewRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public View_PaymentScheduleOverviewTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public View_PaymentScheduleOverviewTableControlRow[] GetSelectedRecordControls()
        {
        
            return (View_PaymentScheduleOverviewTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.PaymentSchedule.View_PaymentScheduleOverviewTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            View_PaymentScheduleOverviewTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (View_PaymentScheduleOverviewTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public View_PaymentScheduleOverviewTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("View_PaymentScheduleOverviewTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                View_PaymentScheduleOverviewTableControlRow recControl = (View_PaymentScheduleOverviewTableControlRow)repItem.FindControl("View_PaymentScheduleOverviewTableControlRow");
                recList.Add(recControl);
            }

            return (View_PaymentScheduleOverviewTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.PaymentSchedule.View_PaymentScheduleOverviewTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  