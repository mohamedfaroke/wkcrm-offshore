using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WKCRM.Business;
using Telerik.Web.UI;
using BaseClasses.Utils;

/// <summary>
/// Summary description for AppointmentLeadWrapper
/// </summary>
public class AppointmentLeadWrapper : AppointmentBase
{
    private string _designerLoc;
    private string _designer;
    private string _hostess;
    private string _designerName;
    private string _accName;

    public int AccountStatus { get; set; }

    public string AccName
    {
        get { return _accName; }
    }
    public string DesignerName
    {
        get { return _designerName; }
    }
    public string Designer
    {
        get { return _designer; }
        set { _designer = value; }
    }
    public string Hostess
    {
        get { return _hostess; }
        set { _hostess = value; }
    }

    public string DesignerLocation
    {
        get { return _designerLoc; }
    }

    public string Mobile { get; set; }
    public string HomePhone { get; set; }
    public string Location { get; set; }

    public AppointmentLeadWrapper(LeadMeetingsRecord rec)
    {
        _type = AppointmentType.LEADMEETING;
        _id = rec.id0.ToString();
        setupDetails(rec);
    }

    public AppointmentLeadWrapper(Appointment apt)
    {
        _id = apt.ID.ToString();
        _type = AppointmentType.LEADMEETING;
        LeadMeetingsRecord rec = LeadMeetingsTable.GetRecord(_id, false);    
        setupDetails(rec);
    }

    private void setupDetails(LeadMeetingsRecord rec)
    {
        if (rec == null)
            return;
        _start = rec.datetime0;
        if (!rec.end_datetimeSpecified)
            End = _start.AddHours(1);
        else
            End = rec.end_datetime;
        _designer = rec.designer_id.ToString();
        _hostess = rec.hostess_id.ToString();
        //ONSITE IS FALSE, SHOWROOM IS TRUE
        Location = rec.onsite ? "Showroom" : "On Site";
        AccountRecord accRec = AccountTable.GetRecord(rec.account_id.ToString(), false);
        EmployeeRecord empRec = EmployeeTable.GetRecord(_designer, false);

        if (accRec != null && empRec != null)
        {
            HomePhone = accRec.home_phone;
            Mobile = accRec.mobile;
            _designerName = empRec.name;
            _accName = accRec.Name;
            _designerLoc = empRec.location_id.ToString();
            _subject = empRec.name;
            AccountStatus = accRec.account_status_id;
        }
    }
   
}


