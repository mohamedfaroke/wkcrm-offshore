using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using CrystalDecisions.Shared;
using WKCRM.Business;
using Telerik.Web.UI;
using BaseClasses.Data;
using System.Drawing;
using System.IO;
using Impowa.Dev.Utilities;
using System.Drawing.Drawing2D;
using BaseClasses.Utils;
using System.Text.RegularExpressions;
using CrystalDecisions.CrystalReports.Engine;
using ImageFormat = System.Drawing.Imaging.ImageFormat;

/// <summary>
/// Summary description for Globals
/// </summary>
public class Globals
{
    private static CrmOptionsRecord _options;

    public const string UPLOADSTAG = "UploadsDir";
    public const string DATADIRTAG = "DatafilesDir";
    public const string WEBADDRESS = "WebAddress";

    private const string WEBSITE_EMAIL_ADDRESS = "info@wonderfulkitchens.com.au";
    private const bool DEBUG = false;
    private const decimal imageWidth = 1024;

    public const string APPLEADMEETING_PREFIX = "lm_";
    public const string APPPROJECTTASK_PREFIX = "t_";

    public const string INJECTSCRIPT_START = "<script>Sys.Application.add_load(function(){";
    public const string INJECTSCRIPT_START_REMOVE = "<script>Sys.Application.remove_load(function(){";
    public const string INJECTSCRIPT_END = "})</script>";

    #region DATABASE FIELD VALUES
    public const int CONTACT_UNKNOWN = 1020;
    public const int OPPORTUNITY_UNKNOWN = 1085;


    public const int TASKNOTIFICATION_GENERAL = 1000;
    public const int TASKNOTIFICATION_NOTINTERESTED = 1015;
    public const int TASKNOTIFICATION_DEAD = 1025;
    public const int TASKNOTIFICATION_IMAGE = 1030;

    public const int ACCOUNT_OPEN = 1000;
    public const int ACCOUNT_NOTINTERESTED = 1010;
    public const int ACCOUNT_COMPLETED = 1020;
    public const int ACCOUNT_DEACTIVATED = 1025;
    public const int ACCOUNT_LEADCANCELLED = 1030;
    public const int ACCOUNT_SIGNEDOTHERS = 1035;
    public const int ACCOUNT_NORESPONSE = 1040;

    public const int ACCOUNTPHASE_LEAD = 1000;
    public const int ACCOUNTPHASE_PROJ = 1005;

    public const int ATTACHTYPE_IMAGE = 1000;
    /// <summary>
    /// Used when building the dynamic forms for contract items such as doors
    /// </summary>
    public const int FIELDTYPE_FIXED = 1000;
    public const int FIELDTYPE_TEXT = 2000;
    public const int FIELDTYPE_COMBO = 3000;
    /// <summary>
    /// Type of pricing for Contract Items
    /// </summary>
    public const int PRICETYPE_FIXED = 1000;
    public const int PRICETYPE_QUOTE = 1005;
    public const int PRICETYPE_UNITS = 1010;

    /// <summary>
    /// Contract Item Types
    /// </summary>
    public const int CONTRACT_KITCHEN_HEIGHTS = -9999;//Hack, not in db
    public const int CONTRACTITEM_DOORS = 1000;
    public const int CONTRACTITEM_BENCHTOPS = 1005;
    public const int CONTRACTITEM_TRADES = 2010;
    public const int CONTRACTITEM_HANDLES = 1035;
    public const int CONTRACTITEM_ACCESSORIES = 1055;
    public const int CONTRACTITEM_MISC = 2005;
    public const int CONTRACTITEM_SPLASHBACK = 1015;
    //The following types will display in the MISC template
    public const int CONTRACTITEM_DRAWERS = 2045;
    public const int CONTRACTITEM_FITTINGS = 2050;
    public const int CONTRACTITEM_BLUMPRODUCTS = 2035;
    public const int CONTRACTITEM_CABINETS = 2040;
    public const int CONTRACTITEM_BINS = 2030;
    public const int CONTRACTITEM_GLASS = 1040;
    public const int CONTRACTITEM_WARDROBES = 2015;

    public const int CONTRACTTYPE_QUOTE = 1000;
    public const int CONTRACTTYPE_PROJ = 1005;

    #endregion

    public Globals()
	{

    }

    public static CrmOptionsRecord CrmOptions
    {
        get
        {
            if (_options == null)
            {
                _options = CrmOptionsTable.GetRecord("");
            }
            return _options;
            
        }
    }


    #region ACCOUNT METHODS
    /// <summary>
    /// At least one number must be supplied for this function to return true
    /// </summary>
    /// <param name="rec">The account record, null will return false</param>
    /// <returns></returns>
    public static bool HasValidPhoneNumber(AccountRecord rec)
    {
        bool valid = true;
        if (rec == null || 
            (String.IsNullOrEmpty(rec.home_phone) && String.IsNullOrEmpty(rec.business_phone) && String.IsNullOrEmpty(rec.business_phone2)
            && String.IsNullOrEmpty(rec.mobile)&& String.IsNullOrEmpty(rec.mobile2) && String.IsNullOrEmpty(rec.builders_phone)))
            valid = false;
        return valid;
    }




    #endregion

    #region EMPLOYEE METHODS

    private static string FindFreeEmployee(string locationID, string roleID)
    {
        string employee = "";
        View_EmployeeTotalOutstandingTasksRecord rec;
        string whereClause = "role_id=" + roleID;
        if (!String.IsNullOrEmpty(locationID))
        {
            rec = View_EmployeeTotalOutstandingTasksView.GetRecord(whereClause + "AND location_id=" + locationID);
        }
        else
        {
            rec = View_EmployeeTotalOutstandingTasksView.GetRecord(whereClause);
        }
        if (rec != null)
            employee = rec.id0.ToString();
        return employee;
    }

    public static string FindHostess(string locationID)
    {
        return (FindFreeEmployee(locationID, "5010"));
    }

    public static string FindHostess()
    {
        return (FindHostess(""));
    }
    /// <summary>
    /// Returns the lead designer at a particular location with the lowest number
    /// of tasks
    /// </summary>
    /// <param name="locationID"></param>
    /// <returns></returns>
    ///
    public static string FindLeadDesigner(string locationID)
    {
        return (FindFreeEmployee(locationID, "5020"));
    }
    /// <summary>
    /// Returns any lead designer with the lowest number of tasks
    /// </summary>
    /// <returns></returns>
    public static string FindLeadDesigner()
    {
        return (FindLeadDesigner(""));
    }
    #endregion

    #region NOTIFICATION METHODS

    /// <summary>
    /// This method will generate any notifications required whenever an 
    /// account will be saved. It will first check to see if there is existing
    /// notifications for this account that are not completed. If there is, it will
    /// not generate duplicate notifications. However, if there are notifications that
    /// are marked as completed but the condition for the notification has not been met,
    /// a new notification will be generated.
    /// </summary>
    /// <param name="rec">The account record for which notifications will be generated</param>
    public static void GenerateAccountNotifications(AccountRecord rec)
    {
        if (rec == null)
            return;
        // Need to create a reminder for the designer to add an image attachment to the account
        if (rec.id0Specified)
        {
            decimal accID = rec.id0;
            if (!NotificationExists(Globals.TASKNOTIFICATION_IMAGE,accID))
                DesignImageNotification(accID, rec.designer_id);

            if (rec.account_status_id == Globals.ACCOUNT_NOTINTERESTED && !NotificationExists(Globals.TASKNOTIFICATION_NOTINTERESTED, accID))
                AccountNotInterestedNotification(accID, rec.designer_id);

            //if (rec.account_status_id == Globals.ACCOUNT_DEACTIVATED && !NotificationExists(Globals.TASKNOTIFICATION_DEAD, accID))
            //    AddNotification(TASKNOTIFICATION_DEAD, accID, rec.designer_id, "Lead has been deactivated", true);
        }
    }
    /// <summary>
    /// Will return true if an uncompleted notification of this particular type
    /// exists for the reference id
    /// </summary>
    /// <param name="notificationType"></param>
    /// <param name="refID"></param>
    /// <returns></returns>
    private static bool NotificationExists(int notificationType, decimal refID)
    {
        WhereClause wc = new WhereClause(TaskNotificationTable.taskNotificationType,
                                         BaseFilter.ComparisonOperator.EqualsTo, notificationType.ToString());
        wc.iAND(TaskNotificationTable.reference_id, BaseFilter.ComparisonOperator.EqualsTo, refID.ToString());
        WhereClause wcCompleted = new WhereClause("completed IS NULL OR completed=0");
        wc.iAND(wcCompleted);
        return (TaskNotificationTable.GetRecordCount(wc) > 0);
    }

    /// <summary>
    /// Generate a notification if we cant find an image type attachment for this record
    /// </summary>
    /// <param name="accountID"></param>
    /// <param name="designer"></param>
    private static void DesignImageNotification(decimal accountID, decimal designer)
    {       
        //If no image type attachments, we generate this notification
        WhereClause wc = new WhereClause(AttachmentTable.account_id, BaseFilter.ComparisonOperator.EqualsTo, accountID.ToString());
        wc.iAND(AttachmentTable.attachmentType_id, BaseFilter.ComparisonOperator.EqualsTo, Globals.ATTACHTYPE_IMAGE.ToString());
        if (AttachmentTable.GetRecordCount(wc) <= 0)
        {
            TaskNotificationRecord taskNotification = new TaskNotificationRecord();
            taskNotification.date_created = DateTime.Now;
            taskNotification.employee_id = designer;
            taskNotification.taskNotificationType = Globals.TASKNOTIFICATION_IMAGE;
            taskNotification.TaskDescription = "This lead requires a design image to be uploaded";
            taskNotification.TaskLink = "../account/EditAccountPage.aspx?account=" + accountID.ToString();
            taskNotification.reference_id = accountID;
            taskNotification.Save();
        }
    }
   
    /// <summary>
    /// Generate a notification if the account goes into the not interested state
    /// </summary>
    /// <param name="accountID"></param>
    /// <param name="designer"></param>
    private static void AccountNotInterestedNotification(decimal accountID,decimal designer)
    {
        string locId = "";
        EmployeeRecord emp = EmployeeTable.GetRecord(designer.ToString(), false);
        if (emp != null)
            locId = emp.location_id.ToString();
        string leadDesigner = FindLeadDesigner(locId);
        if (!String.IsNullOrEmpty(leadDesigner))
        {
            TaskNotificationRecord taskNotification = new TaskNotificationRecord();
            taskNotification.date_created = DateTime.Now;
            taskNotification.employee_id = Convert.ToDecimal(leadDesigner);
            taskNotification.taskNotificationType = Globals.TASKNOTIFICATION_NOTINTERESTED;
            taskNotification.TaskDescription = "Please follow up this lead";
            taskNotification.TaskLink = "../account/EditAccountPage.aspx?account=" + accountID.ToString();
            taskNotification.reference_id = accountID;
            taskNotification.Save();
        }
    }
    /// <summary>
    /// Create a generic notification
    /// </summary>
    /// <param name="notificationType"></param>
    /// <param name="refID">The record this notification refers to</param>
    /// <param name="employee">The employee that this notifcation should be logged to</param>
    /// <param name="msg">The human readable message to accompany the notification</param>
    /// <param name="logOnly">True if to mark notification as already completed</param>
    private static void AddNotification(int notificationType, decimal refID,decimal employee, string msg,bool logOnly)
    {
        TaskNotificationRecord taskNotification = new TaskNotificationRecord();
        taskNotification.date_created = DateTime.Now;
        taskNotification.employee_id = employee;
        taskNotification.taskNotificationType = notificationType;
        taskNotification.TaskDescription = msg;
        taskNotification.reference_id = refID;
        taskNotification.completed = logOnly;
        taskNotification.Save();
    }

    #endregion

    #region SCHEDULER METHODS


    public static string GetAppointmentImage(string task, bool largeImage)
    {
        string imgPrefix = "1";
        string taskType = task.ToLower();
        string imageURL = "";
        string suffix = "_Large.png";
        if (!largeImage)
            suffix = "_Small.png";

        if (taskType.Contains("delivery"))
        {
            imgPrefix = "2";
        }
        else if (taskType.Contains("install"))
        {
            imgPrefix = "3";
        }
        else if (taskType.Contains("meeting"))
        {
            imgPrefix = "4";
        }
        else if (taskType.Contains("plumbing"))
        {
            imgPrefix = "5";
        }
        else if (taskType.Contains("electricity"))
        {
            imgPrefix = "6";
        }
        imageURL = "../Images/Icons/" + imgPrefix + suffix;
        return imageURL;
    }

    public static ArrayList GetAllAppointments()
    {
        ArrayList myList = new ArrayList(100);

        foreach (LeadMeetingsRecord rec in LeadMeetingsTable.GetRecords(""))
        {
            string id = APPLEADMEETING_PREFIX + rec.id0.ToString();
            AppointmentLeadWrapper lw = new AppointmentLeadWrapper(rec);
            lw.SetupDodgyID(id);
            myList.Add(lw);
        }

        foreach (TasksRecord rec in TasksTable.GetRecords(""))
        {
            string id = APPPROJECTTASK_PREFIX + rec.id0.ToString();
            AppointmentTaskWrapper task = new AppointmentTaskWrapper(rec);
            task.SetupDodgyID(id);
            myList.Add(task);
        }
        return myList;
    }

    public static ArrayList GetAllLeadMeetings()
    {
        return GetAllLeadMeetings("-1","-1");
    }
    /// <summary>
    /// If locid is not specified, and designer id is not specified we get all meetings
    /// If designer is specified, we use designer only (locid will still be filtered on)
    /// If loc id is specified and designer is not specified, we show only designers in that loc
    /// </summary>
    /// <param name="id">Designer id</param>
    /// <param name="locid">Location id</param>
    /// <returns>A list of lead meetings</returns>
    public static ArrayList GetAllLeadMeetings(string id,string locid)
    {
        ArrayList myList = new ArrayList();
        string where = "";
        bool checkLoc = (!String.IsNullOrEmpty(locid) && locid != "-1");
        if (!String.IsNullOrEmpty(id) && id != "-1")
            where = "designer_id=" + id;
        foreach (LeadMeetingsRecord rec in LeadMeetingsTable.GetRecords(where))
        {
            AppointmentLeadWrapper myApt = new AppointmentLeadWrapper(rec);
            if (checkLoc && locid != myApt.DesignerLocation)
                continue;
            if (myApt.AccountStatus != Globals.ACCOUNT_LEADCANCELLED)
                myList.Add(myApt);
        }
        return myList;
    }


    public static AppointmentLeadWrapper FindLeadMeeting(ArrayList meetings, string id)
    {
        if (meetings == null)
            return null;
        foreach (AppointmentLeadWrapper lead in meetings)
        {
            if (lead.ID == id)
                return lead;
        }
        return null;
    }

    public static AppointmentTaskWrapper FindTask(ArrayList tasks, string id)
    {
        if (tasks == null)
            return null;
        foreach (AppointmentTaskWrapper task in tasks)
        {
            if (task.ID == id)
                return task;
        }
        return null;
    }

    public static ArrayList GetAllProjectTasks()
    {
        return GetAllProjectTasks("");
    }

    public static ArrayList GetAllProjectTasks(string projectID)
    {
        ArrayList myList = new ArrayList();
        string where = "";
        if (!String.IsNullOrEmpty(projectID) && projectID != "-1")
            where = "project_id=" + projectID;
        foreach (TasksRecord rec in TasksTable.GetRecords(where))
        {
            myList.Add(new AppointmentTaskWrapper(rec));
        }
        return myList;
    }

    public static DateTime GetEarliestTask(string projectID)
    {
        OrderBy orderby = new OrderBy(false, true);
        orderby.Add(TasksTable.datetime0, OrderByItem.OrderDir.Asc);
        string where = "";
        if (!String.IsNullOrEmpty(projectID) && projectID != "-1")
            where = "project_id=" + projectID;
        TasksRecord rec = TasksTable.GetRecord(where, orderby);
        DateTime start = DateTime.Today;
        if (rec != null)
            start = rec.datetime0;
        return start;
    }

    public static void SetSchedulerSettings(RadScheduler RadScheduler1)
    {
        SetSchedulerSettings(RadScheduler1, Globals.GetAllLeadMeetings());
    }

    public static void SetSchedulerSettings(RadScheduler RadScheduler1, object dataSource)
    {
        RadScheduler1.Skin = "Vista"; //Getting weird behaviour with this skin
        RadScheduler1.EnableEmbeddedSkins = true;
       // RadScheduler1.TimeZoneOffset = new TimeSpan(10, 0, 0);
        RadScheduler1.DataSource = dataSource;
        RadScheduler1.SelectedView = SchedulerViewType.WeekView;
        RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
        RadScheduler1.DayStartTime = new TimeSpan(6, 0, 0);
        RadScheduler1.DayEndTime = new TimeSpan(18, 0, 0);
        RadScheduler1.ReadOnly = true;
        RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;
        RadScheduler1.AdvancedForm.Enabled = false;
        RadScheduler1.MinutesPerRow = 30;
        RadScheduler1.ShowAllDayRow = false;
        RadScheduler1.OverflowBehavior = OverflowBehavior.Scroll;
        RadScheduler1.WeekView.HeaderDateFormat = "dd/MM/yyyy";
        RadScheduler1.TimelineView.HeaderDateFormat = "dd/MM/yyyy";
        RadScheduler1.TimelineView.ColumnHeaderDateFormat = "dd/MM/yyyy";

    }
    /// <summary>
    /// Builds a link to display a designer meeting schedule
    /// </summary>
    /// <param name="scheduleLabel">The label which will contain the link</param>
    /// <param name="designerList">The drop down list which will specify what designer to default to</param>
    public static void SetDesignerScheduleLabel(Label scheduleLabel, DropDownList designerList)
    {
        if (designerList == null || designerList.SelectedIndex == 0)
        {
            SetDesignerScheduleLabel(scheduleLabel, "");
        }
        else
        {
            SetDesignerScheduleLabel(scheduleLabel, designerList.SelectedValue);
        }
    }

    public static void SetDesignerScheduleLabel(Label scheduleLabel, string designerID)
    {
        string linkStartText = "<a href=\"javascript:\"onclick=\"OpenDesignerSchedule(";
        string linkEndText = "); return false;\">Show Designer Schedules...</a>";
        if (String.IsNullOrEmpty(designerID))
            scheduleLabel.Text = linkStartText + linkEndText;
        else
            scheduleLabel.Text = linkStartText + designerID + linkEndText;
    }
    #endregion
  
    /// <summary>
/// The following region contains methods which are used to filter the values that are displayed in various drop down filters
/// They are usually called in an overriden CreateWhereClause method in the appcode classes for that particular record
/// </summary>
    #region DROP DOWN FILTERS

    public static ArrayList Designers
    {
        get
        {
            return EmployeeList("5015");
        }
    }

    public static ArrayList Hostesses
    {
        get
        {
            return EmployeeList("5010");
        }
    }

    public static WhereClause BuildDesignerWC()
    {
        WhereClause clause = new WhereClause();

        foreach (EmployeeRecord emp in Globals.Designers)
        {
            WhereClause desinger = new WhereClause(EmployeeTable.id0, BaseFilter.ComparisonOperator.EqualsTo, emp.id0.ToString());
            clause.iOR(desinger);
        }
        return clause;
    }

    public static WhereClause BuildHostessWC()
    {
        WhereClause clause = new WhereClause();

        foreach (EmployeeRecord emp in Globals.Hostesses)
        {
            WhereClause hostess = new WhereClause(EmployeeTable.id0, BaseFilter.ComparisonOperator.EqualsTo, emp.id0.ToString());
            clause.iOR(hostess);
        }
        return clause;
    }

    public static WhereClause BuildContactSourceWC()
    {
        WhereClause clause = new WhereClause(ContactSourceTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, "1");
        return clause;
    }

    public static WhereClause BuildOpCategoryWC()
    {
        WhereClause clause = new WhereClause(OpportunityCategoryTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, "1");
        return clause;
    }

    public static WhereClause BuildOpSourceWC()
    {
        WhereClause clause = new WhereClause(OpportunitySourceTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, "1");
        return clause;
    }

    public static WhereClause BuildAccountStatusWC()
    {
        WhereClause clause = new WhereClause(AccountStatusTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, "1");
        return clause;
    }

    public static void PopulateAccountStatusDropdowList(string selectedValue, DropDownList ddList)
    {
        ddList.Items.Clear();
        OrderBy ob = new OrderBy(false, true);
        ob.Add(AccountStatusTable.name, OrderByItem.OrderDir.Asc);
        AccountStatusRecord[] recs = AccountStatusTable.GetRecords(BuildAccountStatusWC(), ob, 0, 100);

        foreach (AccountStatusRecord rec in recs)
        {
            ListItem itm = new ListItem(rec.name, rec.id0.ToString());
            ddList.Items.Add(itm);
        }

        // Add the All item.
        ddList.Items.Insert(0, new ListItem("All", "--ANY--"));

        if (!String.IsNullOrEmpty(selectedValue))
        {
            ListItem selectedItem = ddList.Items.FindByValue(selectedValue);
            if (selectedItem != null)
            {
                selectedItem.Selected = true;
            }
        }
    }

    #endregion

    #region ATTACHMENT METHODS
    public static string ResizeImage(string imagePath)
    {
        try
        {
            Bitmap bigImage = new Bitmap(imagePath);
            Size origSize = bigImage.Size;
            string filename = Path.ChangeExtension(imagePath, ".png");
            
            decimal resizeFactor = Globals.imageWidth/origSize.Width;
            int newWidth = Convert.ToInt32(Math.Round(origSize.Width * resizeFactor, 0));
            int newHeight = Convert.ToInt32(Math.Round(origSize.Height * resizeFactor, 0));
           
            Bitmap smallbmp = new Bitmap(newWidth,newHeight);
            Graphics smallImage = Graphics.FromImage(smallbmp);
            smallImage.InterpolationMode = InterpolationMode.HighQualityBicubic;
            smallImage.SmoothingMode = SmoothingMode.HighQuality;
            smallImage.PixelOffsetMode = PixelOffsetMode.HighQuality;
            smallImage.CompositingQuality = CompositingQuality.HighQuality;

            smallImage.DrawImage(bigImage, 0, 0, newWidth, newHeight);
            smallbmp.Save(filename, ImageFormat.Png);
            smallbmp.Dispose();
            smallImage.Dispose();
            bigImage.Dispose();
            return filename;
        }
        catch
        {
            return imagePath;
        }
    }

    /// <summary>
    /// Fetchs the name of the image for the attached file that should be displayed to the user
    /// </summary>
    /// <param name="filename">The attachment</param>
    /// <param name="myPage">The calling page</param>
    /// <returns></returns>
    public static string GetAttachmentImage(string filename, Page myPage)
    {
        string checkImageDir = myPage.Server.MapPath("~/Images/");
        string imageDir = "../Images/";
        string fileExtension = Path.GetExtension(filename).ToLower().Replace(".", "");
        string imageFileName = "file_small_" + fileExtension + ".gif";
        string imageName = Path.Combine(checkImageDir, imageFileName);
        string realImageLoc = Path.Combine(imageDir, imageFileName);
        if (!File.Exists(imageName))
            realImageLoc = Path.Combine(imageDir, "file_small.gif");
        return realImageLoc;
    }

    public static void ViewAttachement(string fullFileNamePath, Page thisPage)
    {
        string filename = "";
        System.IO.FileStream fileStream;
        long fileSize;

        try
        {
            if (!String.IsNullOrEmpty(fullFileNamePath))
            {
                //Get the file name from the whole thing
                string[] fileNameSplit = fullFileNamePath.Split('\\');
                if (fileNameSplit != null && fileNameSplit.Length > 0)
                {
                    //file name should be the last bit of this filepath
                    filename = fileNameSplit[fileNameSplit.Length - 1];
                }
                else
                {
                    //Better to display something simple then something horrible or unexpected
                    filename = "Attachment";
                }

                //Test if exists on drive using full path e.g. C:\.....
                if (System.IO.File.Exists(fullFileNamePath))
                {
                    fileStream = new System.IO.FileStream(fullFileNamePath, System.IO.FileMode.Open);
                    fileSize = fileStream.Length;

                    //Allocate size for our buffer array
                    byte[] Buffer = new byte[(int)fileSize];
                    fileStream.Read(Buffer, 0, (int)fileSize);
                    fileStream.Close();

                    //Do buffer cleanup
                    thisPage.Response.Buffer = true;
                    thisPage.Response.Clear();

                    //Add headers so that the browser doesnt think its a ASPX file or something
                    thisPage.Response.ContentType = "application";
                    thisPage.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename.Replace(" ", "_"));

                    //Stream it out via a Binary Write
                    thisPage.Response.BinaryWrite(Buffer);

                    thisPage.Response.End();
                }
            }
        }
        catch
        {
            //do nothing, confuse hackers
        }
    }

    #endregion

    #region PRIVATE METHODS

    private static ArrayList EmployeeList(string roleType)
    {
        ArrayList myList = new ArrayList();
        myList.Clear();
        foreach (EmployeeRolesRecord employee in EmployeeRolesTable.GetRecords("role_id=" + roleType))
        {
            EmployeeRecord emp = EmployeeTable.GetRecord(employee.employee_id.ToString(), false);
            myList.Add(emp);
        }
        return myList;
    }

    #endregion

    #region EMAIL METHODS
    /// <summary>
    /// This sends an email to the specified address. All the other email methods use this method to send
    /// the email. If the DEBUG variable is specified, all emails will be sent to fayezm@impowa.com.au =)
    /// </summary>
    /// <param name="emailAddress"></param>
    /// <param name="subject"></param>
    /// <param name="body"></param>
    /// <returns></returns>
    public static bool SendEmail(string emailAddress, string subject, string body)
    {
        return (SendEmail(emailAddress, subject, body, ""));
    }

    public static bool SendEmail(string emailAddress, string subject, string body,string attachmentFileName)
    {
        bool success = false;
        string toEmail = emailAddress;
        if (DEBUG)
            toEmail = "link64@gmail.com";
        try
        {
            BaseClasses.Utils.MailSender email = new BaseClasses.Utils.MailSender();
            email.AddFrom(Globals.WEBSITE_EMAIL_ADDRESS);
            email.AddTo(toEmail);
            email.SetSubject(subject);
            email.SetContent(body);
            if (!String.IsNullOrEmpty(attachmentFileName))
                email.AddAttachment(attachmentFileName);
            email.SendMessage();

            //Everything has gone OK
            success = true;
        }
        catch (System.Exception ex)
        {
            success = false;
        }
        return success;
    }

    public static bool SendLeadMeetingEmail(LeadMeetingsRecord leadMeeting,out string msg)
    {
        bool success = false;
        msg = "";
        AccountRecord accRec = AccountTable.GetRecord(leadMeeting.account_id.ToString(), false);
        EmployeeRecord empRec = EmployeeTable.GetRecord(leadMeeting.designer_id.ToString(), false);
        try
        {
            
            string reportId = "11";
            ReportsRecord rec = ReportsTable.GetRecord("id = " + reportId);
            string reportName = rec.filename;
            string viewName = rec.viewname;

            string reportTitle = rec.name;

            // Create the report object
            ReportDocument currentReport = new ReportDocument();

            string reportsPath = HttpContext.Current.Server.MapPath("..") + "\\Reports\\rpt\\";

            currentReport.Load(reportsPath + reportName);
            //Hack
            currentReport.DataDefinition.RecordSelectionFormula = "1=1 " + getClause("id", leadMeeting.id0.ToString(), " = ", "", viewName);

            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.UserID = Web.GetFromWebConfig("ReportUserName");
            connectionInfo.Password = Web.GetFromWebConfig("ReportPassword");
            connectionInfo.ServerName = Web.GetFromWebConfig("ReportDatabaseFile");
            connectionInfo.DatabaseName = Web.GetFromWebConfig("ReportDatabase");
            SetDBLogonForReport(connectionInfo, currentReport);
            SetDBLogonForSubreports(connectionInfo, currentReport);

            string fileName = Path.Combine(Path.GetTempPath(), "MeetingSummary.pdf");
            currentReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

            string subject = "Wonderful Kitchens Meeting Reminder";
            string body = "Please find attached the details of the meeting";

            if (accRec != null && accRec.emailSpecified && Validator.IsValidEmailAddress(accRec.email))
            {
                string email = accRec.email;
                bool mySuccess = SendEmail(email, subject, body, fileName);
                success = mySuccess;
                if (accRec.email2Specified && Validator.IsValidEmailAddress(accRec.email2))
                    SendEmail(accRec.email2, subject, body, fileName);
                msg += mySuccess ? "Email sent to account.\r\n" : "Email failed to send to account.\r\n";
            }
            else
            {
                msg += "No email provided for account.\r\n";
            }

            if(empRec != null && empRec.emailSpecified && Validator.IsValidEmailAddress(empRec.email))
            {
                string email = empRec.email;
                bool mySuccess = SendEmail(email, subject, body, fileName);
                msg += mySuccess ? "Email sent to designer.\r\n" : "Email failed to send to designer.\r\n";
                success = success && mySuccess;
            }
            else
            {
                msg += "No email provided for designer.\r\n";
            }
        }
        catch
        {
            success = false;
        }

        return success;
    }

    public static bool SendTradesmanEmail(TasksRecord task)
    {
        bool success = false;
        try
        {
            if (task != null)
            {
                TradesmenRecord tradey = TradesmenTable.GetRecord(task.tradesman_id.ToString(), false);

                if (tradey != null && !String.IsNullOrEmpty(tradey.email) && tradey.send_email_notifications)
                {
                    View_ProjectWithAccNameRecord accView = View_ProjectWithAccNameView.GetRecord(task.project_id.ToString(), false);
                    AccountRecord account = AccountTable.GetRecord(accView.AccId.ToString(), false);
                    TradeTaskTypesRecord taskType = TradeTaskTypesTable.GetRecord(task.task_type_id.ToString(), false);
                    //Add tradetaskstype record
                    string website = Impowa.Dev.Utilities.Web.GetFromWebConfig(Globals.WEBADDRESS);
                    if (account != null && taskType != null)
                    {
                        string subject = "Wonderful Kitchens Project Task";
                        string body = "";
                        body += "Hi " + tradey.name + ",\n";
                        body += "You have been assigned a task. Details are below: \n\n";
                        body += "Date/Time: " + task.datetime0.ToString("dd/MM/yyyy hh:mm tt") + "\n";
                        body += "Client: " + account.Name + "\n";
                        body += "Address: " + account.address + "," + account.suburb + "," + account.postcode + "\n";
                        body += "Phone: " + account.home_phone + " Mobile: " + account.mobile + "\n";
                        body += "Task: " + taskType.name + "\n";
                        body += "Task Notes: " + task.notes + "\n\n";
                        body += "Please verify this task by clicking on " + website + "/tasks/AcknowledgeTask.aspx?tasks=" + task.id0.ToString() + "\n\n";
                        body += "Regards,\nWonderful Kitchens";
                        success = SendEmail(tradey.email, subject, body);
                    }
                }
            }
        }
        catch
        {
            success = false;
        }
        return success;
    }

    public static bool SendProjectScheduleEmail(string projectId,out string msg)
    {
        bool success = false;
        msg = "";
        
        View_ProjectWithAccNameRecord pRec = View_ProjectWithAccNameView.GetRecord(projectId, false);
        AccountRecord accRec = AccountTable.GetRecord(pRec.AccId.ToString(), false);
        try
        {
            //Project Shcedule Report, enums in phase 3.... maybe
            string reportId = "14";
            ReportsRecord rec = ReportsTable.GetRecord("id = " + reportId);
            string reportName = rec.filename;
            string viewName = rec.viewname;

            string reportTitle = rec.name;

            // Create the report object
            ReportDocument currentReport = new ReportDocument();

            string reportsPath = HttpContext.Current.Server.MapPath("..") + "\\Reports\\rpt\\";

            currentReport.Load(reportsPath + reportName);
            //Hack
            currentReport.DataDefinition.RecordSelectionFormula = "1=1 " + getClause("id", projectId, " = ", "", viewName);

            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.UserID = Web.GetFromWebConfig("ReportUserName");
            connectionInfo.Password = Web.GetFromWebConfig("ReportPassword");
            connectionInfo.ServerName = Web.GetFromWebConfig("ReportDatabaseFile");
            connectionInfo.DatabaseName = Web.GetFromWebConfig("ReportDatabase");
            SetDBLogonForReport(connectionInfo, currentReport);
            SetDBLogonForSubreports(connectionInfo, currentReport);

            string fileName = Path.Combine(Path.GetTempPath(), "ProjectScheduleSummary.pdf");
            currentReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

            string subject = "Wonderful Kitchens Project Schedule";
            string body = "Please find the attached project schedule.";

            if (accRec != null && accRec.emailSpecified && Validator.IsValidEmailAddress(accRec.email))
            {
                string email = accRec.email;
                bool mySuccess = SendEmail(email, subject, body, fileName);
                success = mySuccess;
                if (accRec.email2Specified && Validator.IsValidEmailAddress(accRec.email2))
                    SendEmail(accRec.email2, subject, body, fileName);
                msg += mySuccess ? "Email sent to account.\r\n" : "Email failed to send to account.\r\n";
            }
            else
            {
                msg += "No email provided for account.\r\n";
            }
        }
        catch
        {
            success = false;
        }

        return success;
    }

    #endregion

    #region Report Utilities
    private static void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument reportDocument)
    {
        Tables tables = reportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
        {
            TableLogOnInfo tableLogonInfo = table.LogOnInfo;
            tableLogonInfo.ConnectionInfo = connectionInfo;
            table.ApplyLogOnInfo(tableLogonInfo);
        }
    }

    private static void SetDBLogonForSubreports(ConnectionInfo connectionInfo, ReportDocument reportDocument)
    {
        Sections sections = reportDocument.ReportDefinition.Sections;
        foreach (Section section in sections)
        {
            ReportObjects reportObjects = section.ReportObjects;
            foreach (ReportObject reportObject in reportObjects)
            {
                if (reportObject.Kind == ReportObjectKind.SubreportObject)
                {
                    SubreportObject subreportObject = (SubreportObject)reportObject;
                    ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
                    SetDBLogonForReport(connectionInfo, subReportDocument);
                }
            }
        }
    }

    private static String getClause(String col, String value,string viewName)
    {
        return getClause(col, value, " = ", "",viewName);
    }

    private static String getClause(String col, String value, String sign, String quotes,string viewName)
    {
        String comp = "";
        if (value != null && !value.Equals("") && !value.Equals("-1"))
        {
            comp = " and {" + viewName + "." + col + "} " + sign + " " + quotes + value + quotes;
        }
        return comp;
    }

    #endregion

    #region SURVEY METHODS
    /// <summary>
    /// This will go through a project and create the questions that are required for it
    /// This method should be called within a StartTransaction EndTransaction block
    /// A survey will only be created if the amount outstanding on the contract is 0
    /// It will also mark the account as completed if the outstanding amount is 0
    /// </summary>
    /// <param name="projectID"></param>
    public static void CreateSurvey(string projectID)
    {
        ProjectRecord project = ProjectTable.GetRecord(projectID, false);
        if (project != null)
        {
            View_AccountContractProjectRecord acp = View_AccountContractProjectView.GetRecord("ProjID=" + projectID);
            View_PaymentScheduleTotalOutstandingRecord psTot = View_PaymentScheduleTotalOutstandingView.GetRecord(acp.PaymentScheduleID.ToString(), false);
            //Only proceed if the total outstanding is 0
            if (Math.Round(psTot.TotalOutStanding, 2) <= CrmOptions.completionThreshold)
            {
                AccountRecord account = AccountTable.GetRecord(acp.AccID.ToString(), true);
                EmployeeRecord designer = EmployeeTable.GetRecord(account.designer_id.ToString(), false);
                ProjectEvaluationsRecord projEval = new ProjectEvaluationsRecord();

                //Mark the account as completed
                account.account_status_id = Globals.ACCOUNT_COMPLETED;
                account.Save();

                projEval.project_id = project.id0;
                projEval.CreationDate = DateTime.Now;
                //Need to add an employee to assign the survey to?! perhaps not, but need to assign a task to someone
                EvaluationsRecord evals = EvaluationsTable.GetRecord("");//There should only be one evaluation at this point
                projEval.EvaluationId = evals.EvaluationID;
                projEval.Save();

                // string hostessID = FindHostess(designer.location_id.ToString());

                foreach (EvaluationQuestionRecord question in EvaluationQuestionTable.GetRecords("EvaluationId=" + evals.EvaluationID))
                {
                    ProjectEvaluationAnswersRecord unAnswered = new ProjectEvaluationAnswersRecord();
                    unAnswered.ProjectEvaluationId = projEval.ProjectEvaluationId;
                    unAnswered.QuestionId = question.QuestionID;
                    if (question.designer_question)
                    {
                        unAnswered.designerId = designer.id0;
                    }
                    else if (question.associated_taskSpecified)
                    {
                        string whereStr = "project_id=" + project.id0.ToString() + " AND task_type_id=" + question.associated_task.ToString();
                        TasksRecord associatedTask = TasksTable.GetRecord(whereStr);
                        if (associatedTask != null)
                        {
                            unAnswered.tradesmanId = associatedTask.tradesman_id;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    unAnswered.Save();
                }
            }
        }
    }

    public static WhereClause BuildQuestionOptionsWC(string questionID)
    {
        string whereClause = "OptionId IN (";
        bool first = true;
        foreach (EvaluationQuestionOptionsRecord rec in EvaluationQuestionOptionsTable.GetRecords("QuestionId=" + questionID))
        {
            if (!first)
            {
                whereClause += ",";
                
            }
            whereClause += rec.OptionId.ToString();
            first = false;
        }
        whereClause += ")";
        if (first)// If there is no options, return an empty whereClause
            whereClause = "";
        WhereClause wc = new WhereClause(whereClause);
        return wc;
    }

    #endregion

    #region CONTRACT METHODS


    public static void TelerikFocusHack(Page myPage, string clientID)
    {
        myPage.Form.Controls.Add(new LiteralControl(
            String.Format(@"<script type='text/javascript'>
                            window.setTimeout(function()
                            {{
                                $find('{0}').Focus();
                            }}, 10);
                           </script>",clientID)));
    }

    public static void BuildContractDoorForm(int fieldType, Telerik.Web.UI.RadTextBox textbox, RadComboBox combo, string defValue, object[] dataSource)
    {
        // There are three types:
        // Fixed - We display the value in the textbox, but not allow them to edit it
        // Textbox - We display an empty textbox and allow user to edit it
        // Combobox - We display a combobox and allow user to select an item from the list
        if (fieldType == Globals.FIELDTYPE_COMBO)
        {
            textbox.Enabled = false;
            textbox.Visible = false;

            combo.Enabled = true;
            combo.Visible = true;
            combo.DataSource = dataSource;
            combo.DataTextField = "name";
            combo.DataValueField = "id0";
            combo.DataBind();
        }
        else
        {
            combo.Visible = false;
            combo.Enabled = false;
            textbox.Visible = true;
            textbox.Enabled = true;
            if (fieldType == Globals.FIELDTYPE_TEXT)
            {
                textbox.ReadOnly = false;
                textbox.Text = "";

            }
            else
            {
                textbox.Text = defValue;
                textbox.ReadOnly = true;
            }
        }
    }

    public static string GetDoorAttribute(RadComboBox combo, Telerik.Web.UI.RadTextBox textbox)
    {
        string attr = "";
        if (textbox != null && textbox.Enabled)
            attr = textbox.Text;
        else if (combo != null)
            attr = combo.SelectedItem.Text;
        return attr;
    }

    public static void SetDoorAttribute(RadComboBox combo, Telerik.Web.UI.RadTextBox textbox, string attr)
    {
        if (textbox != null && textbox.Enabled)
            textbox.Text = attr;
        else if(combo != null && combo.Enabled)//Combo box
        {
            RadComboBoxItem boxItem = combo.FindItemByText(attr);
            if (boxItem != null)
            {
                boxItem.Selected = true;
            }
        }
    }

    public static string GetUnitDescription(int paymentType)
    {
        string unitDesc = "";
        switch (paymentType)
        {
            case (Globals.PRICETYPE_FIXED):
                unitDesc = "Per Job";
                break;
            case (Globals.PRICETYPE_QUOTE):
                unitDesc = "Quote";
                break;
            case (Globals.PRICETYPE_UNITS):
                unitDesc = "Per Unit";
                break;
            default:
                unitDesc = "";
                break;
        }
        return unitDesc;
    }
    /// <summary>
    /// This method will update a contracts modified on and by fields
    /// It will also update the check measure fee and door threshold fee to the appropriate values
    /// It should be called only within a StartTransaction Endtransactionblock
    /// </summary>
    /// <param name="contract">A mutable ContractMainRecord</param>
    /// <param name="userID">The logged in userID</param>
    public static void UpdateContract(ContractMainRecord contract,string userID)
    {
        if (contract == null)
            return;
        contract.check_measure_fee = CalculateCheckMeasureFee(contract);
        contract.non_metro_delivery_fee = CalculateDeliveryFee(contract);
        contract.door_threshold_fee = CalculateDoorThresholdFee(contract);
        contract.bench_threshold_fee = CalculateBenchtopThresholdFee(contract);
        contract.home_insurance_fee = CalculateHomeInsuranceFee(contract);
        contract.Parse(userID, ContractMainTable.modified_by);
        contract.date_modified = DateTime.Now;
        contract.Save();
    }

    public static decimal CalculateHomeInsuranceFee(ContractMainRecord contract)
    {
        decimal fee = 0;

        View_ContractItemTotalsRecord homeIns = View_ContractItemTotalsView.GetRecord("contract_id=" + contract.id0.ToString());
        var totalAmt = homeIns.TotalAmount;
        var totalAmtIncGst = AddGST(totalAmt);

        if (homeIns != null && totalAmtIncGst >= CrmOptions.insuranceThreshold)//If its not in this query, it means we do not need home insurance
        {
            fee = CrmOptions.insuranceFee;
        }
        return fee;
    }

    public static decimal CalculateBenchtopThresholdFee(ContractMainRecord contract)
    {
        decimal bench_threshold_fee = 0;
        decimal benchQty = 0;
        bool haveAtleastOneBench = false;
        string whereStr = String.Format("contract_id={0}",contract.id0);
        foreach (ContractBenchtopItemRecord benchRec in ContractBenchtopItemTable.GetRecords(whereStr))
        {
            ContractBenchtopsRecord bench = ContractBenchtopsTable.GetRecord("id=" + benchRec.item_id.ToString());
            if (bench.BenchtopThresholdApplies)
            {
                haveAtleastOneBench = true;
                benchQty += benchRec.units;
            }
        }
        if (haveAtleastOneBench && (benchQty < CrmOptions.benchtopQtyThreshold))
            bench_threshold_fee = CrmOptions.benchtopQtyExtraCost;
        return bench_threshold_fee;
    }

    public static decimal CalculateDoorThresholdFee(ContractMainRecord contract)
    {
        decimal door_threshold_fee = 0;
        decimal doorQty = 0;
        bool haveAtleastOneDoor = false;
        foreach (ContractDoorItemRecord doorRec in ContractDoorItemTable.GetRecords("contract_id=" + contract.id0.ToString()))
        {
            haveAtleastOneDoor = true;
            doorQty += doorRec.units;
        }

        if (haveAtleastOneDoor && (doorQty < CrmOptions.doorQtyThreshold))
            door_threshold_fee = CrmOptions.doorQtyExtraCost;
        return door_threshold_fee;
    }

    public static bool IsQuotePhase(string contractID)
    {
        bool isQuote = true;
        ContractMainRecord contract = ContractMainTable.GetRecord(contractID, false);
        if (contract != null && contract.contract_type_id == CONTRACTTYPE_PROJ)
            isQuote = false;
        else
            isQuote = true;
        return isQuote;
    }

    public static decimal CalculateCheckMeasureFee(ContractMainRecord contract)
    {
        decimal checkMeasureFee = 0;
        decimal contractTotal = 0;
        View_ContractItemTotalsRecord total = View_ContractItemTotalsView.GetRecord("contract_id=" + contract.id0.ToString());
        //We dont want to include the current check measure fee as part of the total so remove it
        if (total != null)
            contractTotal = total.TotalAmount - contract.check_measure_fee; 
        
        foreach (View_CheckMeasureFeesRecord rec in View_CheckMeasureFeesView.GetRecords(""))
        {
            if (rec.upper_limitSpecified)
            {
                if (contractTotal >= rec.lower_limit && contractTotal <= rec.upper_limit)
                {
                    checkMeasureFee = rec.checkmeasure_fee;
                    break;
                }
            }
            else if(contractTotal >= rec.lower_limit)
            {
                checkMeasureFee = rec.checkmeasure_fee;
                break;
            }
        }
        return checkMeasureFee;
    }

    public static decimal CalculateDeliveryFee(ContractMainRecord contract)
    {
        decimal deliveryFee = 0;
        decimal contractTotal = 0;
        View_ContractItemTotalsRecord total = View_ContractItemTotalsView.GetRecord("contract_id=" + contract.id0.ToString());
        //We dont want to include the current check measure fee as part of the total so remove it
        if (total != null)
            contractTotal = total.TotalAmount - contract.non_metro_delivery_fee;
        var ob = new OrderBy(true,true);
        ob.Add(DeliveryFeesTable.lower_limit, OrderByItem.OrderDir.Asc);
      //  ob.Add();
        foreach (DeliveryFeesRecord rec in DeliveryFeesTable.GetRecords("",ob))
        {
            if (rec.upper_limitSpecified)
            {
                if (contractTotal >= rec.lower_limit && contractTotal <= rec.upper_limit)
                {
                    deliveryFee = rec.delivery_fee;
                    break;
                }
            }
            else if (contractTotal >= rec.lower_limit)
            {
                deliveryFee = rec.delivery_fee;
                break;
            }
        }
        return deliveryFee;
    }

    #endregion

    #region PROJECT PHASE METHODS

    /// <summary>
    /// Adds the GST component for the supplied amount
    /// </summary>
    /// <param name="totalEx">An amount ex-gst</param>
    /// <returns>The amount inc gst</returns>
    public static decimal AddGST(decimal totalEx)
    {
       return(totalEx * (decimal)(((100.00 + (double)CrmOptions.GST)) / 100));
    }

    /// <summary>
    /// This method is similar to StartProjectPhase, the only difference
    /// is that it is a 'dry run'. No records are generated, but we just check
    /// for all the conditions that would normally stop the process from happening
    /// </summary>
    /// <param name="accID">The id of the account that will be turned into a project</param>
    /// <param name="errorMsg">A string in which an error message will be stored that may be displayed to the user</param>
    /// <returns>A boolean indicating success. If false, errorMsg with contain the reason for failure</returns>
    public static bool CanStartProjectPhase(string accID, out string errorMsg)
    {
        bool success = false;
        errorMsg = "";
        if (String.IsNullOrEmpty(accID))
        {
            errorMsg = "Invalid Account";
            return success;
        }
        AccountRecord account = AccountTable.GetRecord(accID, false);
        if (account != null && account.account_type_id != ACCOUNTPHASE_PROJ)
        {
            ContractMainRecord contract = ContractMainTable.GetRecord("account_id=" + account.id0.ToString());
            if (contract != null)
            {
                // We have a contract ... check to see if home insurance is required, and if it is, are the
                // relavent fields filled in

                if (ContractHasValidInsurance(contract.id0.ToString(), false))
                {
                    success = true;
                }
                else
                {
                    errorMsg = "Contract requires Home Insurance";
                }
            }
            else
            {
                errorMsg = "Lead must have a quote before it can become a project";
            }
        }
        else
        {
            if (account != null)
                errorMsg = "Account already in project phase!";
            else
                errorMsg = "Unable to retrieve account record";
        }
        return success;
    }

    /// <summary>
    /// This method will handle the process of going from
    /// a lead to a project. It will create and/or update all the neccassary
    /// records.
    /// </summary>
    /// <param name="accID">The id of the account that will be turned into a project</param>
    /// <param name="userID">The id of the currently logged in user</param>
    /// <param name="errorMsg">A string in which an error message will be stored that may be displayed to the user</param>
    /// <param name="newAmount">The value of the contract, this can be different to the value of all the components</param>
    /// <returns>A boolean indicating success. If false, errorMsg with contain the reason for failure</returns>
    public static bool StartProjectPhase(string accID,string userID,out string errorMsg,decimal newAmount)
    {
        bool success = false;
        errorMsg = "";
        if (String.IsNullOrEmpty(accID))
        {
            errorMsg = "Invalid Account";
            return success;
        }
        try
        {
            DbUtils.StartTransaction();
            //First things first, retrieve the account and attempt to set it to project phase
            AccountRecord account = AccountTable.GetRecord(accID, true);
            if (account != null && account.account_type_id != ACCOUNTPHASE_PROJ)
            {
                //Now we need to get the contract for the account and put it into the project phase also
                //At this stage, we are assuming that there will only ever be one contract per lead
                ContractMainRecord contract = ContractMainTable.GetRecord("account_id=" + account.id0.ToString());
                if (contract != null)
                {
                    // We have a contract ... check to see if home insurance is required, and if it is, are the
                    // relavent fields filled in

                    if (ContractHasValidInsurance(contract.id0.ToString(),false))
                    {
                        account.account_type_id = ACCOUNTPHASE_PROJ;
                        account.Save();
                        #region Contract Update
                        //Reget the contract as a mutable record
                        contract = ContractMainTable.GetRecord(contract.id0.ToString(), true);
                        //We need to record the original total, so read it from this view and save it
                        View_ContractItemTotalsRecord contractTotal = View_ContractItemTotalsView.GetRecord(contract.id0.ToString(), false);
                        contract.contract_type_id = CONTRACTTYPE_PROJ;
                        contract.original_total = newAmount;//AddGST(contractTotal.TotalAmount);
                        contract.signed_date = DateTime.Today;
                        contract.Save();
                        #endregion
                        //We now create the project
                        #region Project Creation
                        ProjectRecord project = new ProjectRecord();
                        project.contract_id = contract.id0;
                        project.Parse(userID, ProjectTable.created_by);
                        project.modified_by = project.created_by;
                        project.date_created = DateTime.Now;
                        project.date_modified = project.date_created;
                        project.project_manager = account.designer_id;
                        project.Save();
                        #endregion
                        //We now create the payment schedule
                        //There are a couple of steps
                        //1. We create the paymentSchedule record
                        //2. We create entries in the paymentScheduleStages record
                        #region PaymentSchedule Creation
                        PaymentScheduleRecord paymentSchedule = new PaymentScheduleRecord();
                        paymentSchedule.contract_id = contract.id0;
                        paymentSchedule.status_id = true;
                        paymentSchedule.Save();
                        decimal scheduleID = paymentSchedule.id0;
                        //We now need to create the paymentScheduleStages
                        foreach (PaymentStagesRecord stages in PaymentStagesTable.GetRecords(""))
                        {
                            PaymentScheduleStagesRecord scheduleStage = new PaymentScheduleStagesRecord();
                            scheduleStage.payment_schedule_id = scheduleID;
                            scheduleStage.payment_stage_id = stages.id0;
                            //Work out the amount based on the percentages and thersholds
                            decimal percentage = stages.percentage / 100;
                            //If we've specified a positive threshold, and our total is greater than or equal ot this threshold
                            // AND if we've specified an alterate percentage, we will use that percentage
                            if (stages.thresholdSpecified && stages.threshold > 0 && contract.original_total >= stages.threshold
                               && stages.alt_percentageSpecified)
                            {
                                percentage = stages.alt_percentage / 100;
                            }
                            scheduleStage.amount = Math.Floor((percentage * contract.original_total));
                            scheduleStage.Save();
                        }

                        #endregion

                        success = true;
                    }
                    else
                    {
                        errorMsg = "Contract requires Home Insurance";
                    }
                }
                else
                {
                    errorMsg = "Lead must have a quote before it can become a project";
                }
            }
            else
            {
                if (account != null)
                    errorMsg = "Account already in project phase!";
                else
                    errorMsg = "Unable to retrieve account record";
            }
            if(success)
                DbUtils.CommitTransaction();
        }
        catch(Exception ex)
        {
            success = false;
            errorMsg = ex.Message;
            DbUtils.RollBackTransaction();
        }
        finally
        {
            DbUtils.EndTransaction();
        }
        return success;
    }

    /// <summary>
    /// Check if the specifed contract has valid home insurance on it
    /// The information that determines whether or not the home insurance is valid is controlled
    /// by an argument. A contract that does not required insurance will always return true
    /// </summary>
    /// <param name="contractID">The contract id to check</param>
    /// <param name="detailsRequired">True if a policy number is required, false if only confirmation that a policy has been filled out</param>
    /// <returns>A boolean indication whether or not the contract has valid insurance</returns>
    public static bool ContractHasValidInsurance(string contractID, bool detailsRequired)
    {
        bool validInsurance = false;
        View_ContractHomeInsuranceRecord homeIns = View_ContractHomeInsuranceView.GetRecord("id=" + contractID);
        if (homeIns == null)//If its not in this query, it means its valid (at this point in time!)
            validInsurance = true;
        else
        {
            // Okay so we know that the total is over the threshold, we now need to figure out if there are atleast some
            // promising info to show that the insurance process has begun

            //If we dont need full details at this stage, we just see if form has been submitted
            if (!detailsRequired)
                validInsurance = (homeIns.home_insurance_sentSpecified && homeIns.home_insurance_sent);
            else
            {//Otherwise we need a policy number!
                validInsurance = (homeIns.home_insurance_sentSpecified && homeIns.home_insurance_sent)
                                    && !String.IsNullOrEmpty(homeIns.home_insurance_policy);
            }
        }
        return validInsurance;
    }


    /// <summary>
    /// Find the payment schedule ID based on the account
    /// Assumptions are one contract and one payment schedule per account
    /// But this may change in the future
    /// </summary>
    /// <param name="accID">The account id</param>
    /// <returns>The id of the payment schedule, or empty</returns>
    public static string GetPaymentScheduleID(string accID)
    {
        string psID = "";
        if (!String.IsNullOrEmpty(accID))
        {
            ContractMainRecord contract = ContractMainTable.GetRecord("account_id=" + accID);
            if (contract != null)
            {
                PaymentScheduleRecord ps = PaymentScheduleTable.GetRecord("contract_id=" + contract.id0.ToString());
                if (ps != null)
                    psID = ps.id0.ToString();
            }
        }
        return psID;
    }

    #endregion

    /// <summary>
    /// Combine two datetimes into one
    /// </summary>
    /// <param name="mDate">The date instance</param>
    /// <param name="mTime">The time instance</param>
    /// <returns>The new date and time</returns>
    public static DateTime CombineDateTime(DateTime mDate, DateTime mTime)
    {
        DateTime myDateTime = new DateTime(mDate.Year, mDate.Month, mDate.Day, mTime.Hour, mTime.Minute, mTime.Second);
        return myDateTime;
    }

    /// <summary>
    /// Provided a data row xml key, it will get the primary key ID of that row.
    /// </summary>
    /// <param name="xmlKey">is the Row Unique ID</param>
    /// <param name="idField">if the field of the primary key you want the value for</param>
    /// <returns></returns>
    public static string GetRecordIDFromXML(string xmlKey, string idField)
    {
        string recordID = string.Empty;

        if (xmlKey != null && xmlKey != "" & idField != null & idField != "")
        {

            try
            {
                string tmpID = BaseClasses.Data.KeyValue.XmlToKey(xmlKey).ColumnValueByName(idField);
                if (tmpID != null)
                {
                    recordID = tmpID;
                }
            }
            catch (Exception ex)
            {
                recordID = string.Empty;
            }

        }
        return recordID;
    }


    public static string InjectJavascript(string javascript)
    {
        return (INJECTSCRIPT_START + javascript + INJECTSCRIPT_END);
    }


    public static bool IsHostess(string roleId)
    {
        return (roleId == "NOT_ANONYMOUS;5010");
        //    this.Response.Redirect("../Portal/HostessHome.aspx");
        //else if (roleId == "NOT_ANONYMOUS;5015" || roleId == "NOT_ANONYMOUS;5025")
        //    this.Response.Redirect("../Portal/DesignerHome.aspx");
        //else if (roleId == "NOT_ANONYMOUS;5020")
        //{
        //    AdminCell.InnerText = "";
        //    ImageButton3.Visible = false;
        //}
    }
}
