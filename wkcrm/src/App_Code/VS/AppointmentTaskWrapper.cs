using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WKCRM.Business;
using Telerik.Web.UI;
using BaseClasses.Utils;

/// <summary>
/// Summary description for AppointmentTaskWrapper
/// </summary>
public class AppointmentTaskWrapper : AppointmentBase
{
    private string _tradename;
    private string _taskType;
    private string _accName;
    private string _notes;

    public string Notes
    {
        get { return _notes; }
    }

    public string AccountName
    {
        get { return _accName; }
    }

    public string Tradesman
    {
        get { return _tradename; }
    }
    public string TaskType
    {
        get { return _taskType; }
    }

    private DateTime _endDate;
    public DateTime EndDate
    {
        get { return _endDate; }
    }

    public AppointmentTaskWrapper(TasksRecord rec)
	{
        _type = AppointmentType.PROJECTTASK;
        _id = rec.id0.ToString();
        _start = rec.datetime0;
        _notes = rec.notes;
        _endDate = rec.datetime_end;
        //Set up the correct end date on the scheduler
        End = rec.datetime_end;

        TradesmenRecord trade = TradesmenTable.GetRecord(rec.tradesman_id.ToString(), false);
        TradeTaskTypesRecord taskType = TradeTaskTypesTable.GetRecord(rec.task_type_id.ToString(), false);
        View_ProjectWithAccNameRecord proj = View_ProjectWithAccNameView.GetRecord(rec.project_id.ToString(), false);

        if (trade != null && taskType != null && proj != null)
        {
            _accName = proj.Name;
            _tradename = trade.name;
            _taskType = taskType.name;
            _subject = trade.name;
        }
	}

    public void Update(Appointment ModifiedAppointment)
    {
        if (ModifiedAppointment == null)
            return;
        try
        {
            DbUtils.StartTransaction();
            TasksRecord rec = TasksTable.GetRecord(_id, true);
            rec.datetime0 = ModifiedAppointment.Start;
            _start = ModifiedAppointment.Start;
            rec.Save();
            DbUtils.CommitTransaction();
        }
        catch
        {
            DbUtils.RollBackTransaction();
        }
        finally
        {
            DbUtils.EndTransaction();
        }
    }
}
