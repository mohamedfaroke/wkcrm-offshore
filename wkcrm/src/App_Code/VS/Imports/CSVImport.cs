﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using Telerik.Web.UI;
using EmpowerIT.Applications;

/// <summary>
/// Summary description for CSVImport
/// </summary>
public abstract class CSVImport
{

    protected enum RecordImportStatus
    {
        NONE = 0,
        UPDATED = 1,
        IGNORED = 2,
        FAILED = 3,
    } ;

    public string ImportFilePath;

    public string ErrorMessage;

    public string ResultMessage;

    protected CsvReader CSVReader;

    /// <summary>
    /// Only true if all records succesfully imported
    /// </summary>
    public bool ImportSuccessful;

    public bool PartiallySuccessful
    {
        get
        {
            return (ProcessedRecords >= 1 &&
                    UpdatedRecords >= 1);
        }
    }

    /// <summary>
    /// Records that have been succesfully created
    /// or updated
    /// </summary>
    protected int UpdatedRecords;

    /// <summary>
    /// Records that have been ignored based on
    /// custom logic
    /// </summary>
    protected int IgnoredRecords;

    /// <summary>
    /// Records that failed to import due to an exception of some
    /// sort
    /// </summary>
    protected int FailedRecords;

    /// <summary>
    /// Records that have no data for required fields
    /// </summary>
    private int MissingInformationRecords;

    /// <summary>
    /// Total records that have been processed
    /// </summary>
    private int ProcessedRecords;

	public CSVImport(string filepath)
	{
	    ImportFilePath = filepath;
	}

    public bool ProcessCSV()
    {
        
        StreamReader csvFile = null;
        try
        {
            if (!File.Exists(ImportFilePath))
                throw new Exception(String.Format("File {0} does not exist", ImportFilePath));
            csvFile = new StreamReader(ImportFilePath);
            CSVReader = new CsvReader(csvFile, true);
            CSVReader.DefaultParseErrorAction = ParseErrorAction.ThrowException;
            CSVReader.MissingFieldAction = MissingFieldAction.ParseError;
            CSVReader.SkipEmptyLines = true;
            CSVReader.SupportsMultiline = false;
            // Check to see that the headers match .. at the very least
            string[] csvHeaders = CSVReader.GetFieldHeaders();
            //A tuple consisting of the column name and a boolean specifiying if it is required
            ColumnDefinition[] realCols = GetColumnNames();

            if (csvHeaders.Length < realCols.Length)
                throw new Exception("CSV file headers do not match");
            for (int i = 0; i < realCols.Length; i++)
            {
                if (csvHeaders[i] != realCols[i].Name)
                {
                    throw new Exception(String.Format("Field names do not match. Expecting {0} Found: {1}", realCols[i].Name, csvHeaders[i])  );
                }
            }
            //Always assume import succesfull, sub class should
            //set this if anything fails
            ImportSuccessful = true;

            while (CSVReader.ReadNextRecord())
            {
                ProcessedRecords++;

                if (CompulsoryFieldsAvailable())
                {
                    RecordImportStatus lineStatus = ProcessCSVLine();
                    switch(lineStatus)
                    {
                        case RecordImportStatus.UPDATED:
                            {
                                UpdatedRecords++;
                                break;
                            }
                        case RecordImportStatus.IGNORED:
                            {
                                IgnoredRecords++;
                                break;
                            }
                        case RecordImportStatus.FAILED:
                            {
                                ImportSuccessful = false;
                                FailedRecords++;
                                break;
                            }
                        case RecordImportStatus.NONE:
                            {
                                throw new Exception("Invalid Implementation of CSVImport subclass. Please contact support");
                            }
                    }

                }
                else
                    MissingInformationRecords++;
            }
        }
        catch (Exception ex)
        {
            ImportSuccessful = false;
            ErrorMessage += String.Format("An error has occurred: {0}\n", ex.Message);
        }
        finally
        {
            if (csvFile != null)
            {
                csvFile.Close();
                csvFile = null;
            }
        }

        ResultMessage += String.Format("\n{0} records processed\n{1} records updated\n{2} records missing information\n{3} records ignored\n{4} records failed\n",
                                       ProcessedRecords,
                                       UpdatedRecords,
                                       MissingInformationRecords,
                                       IgnoredRecords,
                                       FailedRecords);

        return ImportSuccessful;

    }

    protected virtual bool CompulsoryFieldsAvailable()
    {
        bool available = true;
        ColumnDefinition[] allFields = GetColumnNames();
        foreach(ColumnDefinition f in allFields)
        {
            if (f.Required && String.IsNullOrEmpty(CSVReader[f.Name]))
            {
                available = false;
                break;
            }
        }
        return available;
    }

    protected abstract ColumnDefinition[] GetColumnNames();

    protected abstract RecordImportStatus ProcessCSVLine();

    public static List<string> RadUploadSaveFile(RadUpload fileUpload)
    {
        List<string> fileNamesWithPath = new List<string>();

        string errorMessage = "";

        //There is a file that is selected in the rad upload box
        if (fileUpload.UploadedFiles.Count > 0)
        {
            //The file that has been selected is INVALID according to size etc
            if (fileUpload.InvalidFiles.Count > 0)
            {
                errorMessage = "The specified files are not valid in requirements (wrong extension, size etc)";
            }
            else
            {
                try
                {
                    foreach (UploadedFile newFile in fileUpload.UploadedFiles)
                    {
                        if (newFile.ContentLength > 0)
                        {
                            ////THis badly named method realy just builds a temporary path
                            string fileNameWithFullPath = Path.Combine(Path.GetTempPath(), newFile.GetName());

                            ////Save the actual file to the hard drive, with Overwrite set to true
                            newFile.SaveAs(fileNameWithFullPath, true);

                            fileNamesWithPath.Add(fileNameWithFullPath);
                        }
                        else
                        {
                            throw new Exception(String.Format("File {0} is empty", newFile.GetName()));
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorMessage = "Could not save file. Reasons: " + ex.Message;
                }
            }
        }
        else
        {
            errorMessage = "No Files have been selected to upload";
        }

        if (!String.IsNullOrEmpty(errorMessage))
            throw new Exception(errorMessage);

        return fileNamesWithPath;
    }


    public static void SaveCSVToDB(string filepath,string employeeId)
    {
        string csvFileContents = File.ReadAllText(filepath);
        string sql = String.Format("INSERT INTO CsvImports (EmployeeId,ImportFileContents) VALUES ({0},'{1}')", employeeId, csvFileContents);
        DataAccess.ExecuteNonQuerySQLStatement(sql);
    }
}

public class ColumnDefinition
{
    public string Name;
    public bool Required;

    public ColumnDefinition(string name,bool required)
    {
        this.Name = name;
        this.Required = required;
    }

    public static ColumnDefinition Create(string name,bool required)
    {
        return new ColumnDefinition(name, required);
    }
}