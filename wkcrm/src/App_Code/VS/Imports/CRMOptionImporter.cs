﻿using System;
using System.Collections.Generic;
using System.Web;
using BaseClasses.Utils;
using WKCRM.Business;

/// <summary>
/// Summary description for CRMOptionImporter
/// </summary>
public class CRMOptionImporter : CSVImport
{
    private ColumnDefinition[] colNames =
        {
            ColumnDefinition.Create("id", true), //0
            ColumnDefinition.Create("door Qty Threshold", true), //1
            ColumnDefinition.Create("door Qty Extra Cost", true), //2
            ColumnDefinition.Create("difficult Delivery Fee", true), //3
            ColumnDefinition.Create("second Check Measure Fee", true), //4
            ColumnDefinition.Create("GST", true), //5
            ColumnDefinition.Create("insurance Threshold", true), //6
            ColumnDefinition.Create("insurance Fee", true), //7
            ColumnDefinition.Create("benchtop Qty Threshold", true), //8
            ColumnDefinition.Create("benchtop Qty Extra Cost", true), //9
            ColumnDefinition.Create("dual Finishes", true), //10
            ColumnDefinition.Create("dual Finish Less Fee", true), //11
            ColumnDefinition.Create("dual Finish Less Fee Threshold", true), //12
            ColumnDefinition.Create("high Rise", true), //13
            ColumnDefinition.Create("kickboards After Timber", true), //14
            ColumnDefinition.Create("kickboards Washer After Timber", true), //15
            ColumnDefinition.Create("completion Threshold", true), //16
        };



	public CRMOptionImporter(string filepath) : base(filepath)
	{
		
	}

    protected override ColumnDefinition[] GetColumnNames()
    {
        return colNames;
    }

    protected override CSVImport.RecordImportStatus ProcessCSVLine()
    {
        RecordImportStatus status = RecordImportStatus.NONE;
        try
        {
            DbUtils.StartTransaction();
            string id = CSVReader["id"];
            CrmOptionsRecord rec = CrmOptionsTable.GetRecord(id, true);

            rec.doorQtyThreshold = Convert.ToDecimal(CSVReader[1]);
            rec.doorQtyExtraCost = Convert.ToDecimal(CSVReader[2]);
            rec.difficultDeliveryFee = Convert.ToDecimal(CSVReader[3]);
            rec.secondCheckMeasureFee = Convert.ToDecimal(CSVReader[4]);
            rec.GST = Convert.ToDecimal(CSVReader[5]);
            rec.insuranceThreshold = Convert.ToDecimal(CSVReader[6]);
            rec.insuranceFee = Convert.ToDecimal(CSVReader[7]);
            rec.benchtopQtyThreshold = Convert.ToDecimal(CSVReader[8]);
            rec.benchtopQtyExtraCost = Convert.ToDecimal(CSVReader[9]);
            rec.dualFinishes = Convert.ToDecimal(CSVReader[10]);
            rec.dualFinishLessFee = Convert.ToDecimal(CSVReader[11]);
            rec.dualFinishLessFeeThreshold = Convert.ToDecimal(CSVReader[12]);
            rec.highRise = Convert.ToDecimal(CSVReader[13]);
            rec.kickboardsAfterTimber = Convert.ToDecimal(CSVReader[14]);
            rec.kickboardsWasherAfterTimber = Convert.ToDecimal(CSVReader[15]);
            rec.completionThreshold = Convert.ToDecimal(CSVReader[16]);

            rec.Save();
            status = RecordImportStatus.UPDATED;
            DbUtils.CommitTransaction();
        }
        catch (Exception ex)
        {
            DbUtils.RollBackTransaction();
            ErrorMessage += String.Format("Record {0} - Error has occurred: {1}\n", CSVReader.CurrentRecordIndex, ex.Message);
            status = RecordImportStatus.FAILED;
        }
        finally
        {
            DbUtils.EndTransaction();
        }
        return status;
    }
}