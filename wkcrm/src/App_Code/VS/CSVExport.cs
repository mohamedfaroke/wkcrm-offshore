﻿using System;
using System.Collections.Generic;
using System.Web;
using Telerik.Web.UI;
using System.Web.UI;

namespace EmpowerIT.Applications
{
    /// <summary>
    /// Generate a CSV file from a provided datasource or sql statement
    /// </summary>
    public class CSVExport
    {
        private RadGrid _grid;
        private Page _page;
        private object _ds;
        private string _filename;


        public object DataSource
        {

            get { return _ds; }
            set { _ds = value; }
        }
        public string FileName
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public delegate void ColumnCreatedHandler(GridBoundColumn col);

        public event ColumnCreatedHandler ColumnCreated;


        public delegate string GridExportingHandler(string exportOutput);

        public event GridExportingHandler DataExporting;


        public CSVExport(Page page)
        {
            _grid = new RadGrid();
            _page = page;
            _grid.ColumnCreated += new GridColumnCreatedEventHandler(_grid_ColumnCreated);
            _grid.GridExporting += _grid_GridExporting;
        }

        private void _grid_GridExporting(object sender, GridExportingArgs e)
        {
            if(DataExporting != null)
            {
                e.ExportOutput = DataExporting(e.ExportOutput);
            }
        }

        void _grid_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            if (ColumnCreated != null)
            {
                if (e.Column is GridBoundColumn)
                {
                    GridBoundColumn col = e.Column as GridBoundColumn;
                    ColumnCreated(col);
                }
            }
        }

        public void ExportCSV()
        {
            if(DataSource == null)
            {
                throw new Exception("Datasource not specified");
            }
            if(_grid != null)
            {
                _grid.ExportSettings.FileName = String.IsNullOrEmpty(FileName) ? "DataExport" : FileName;
                _grid.ExportSettings.OpenInNewWindow = true;
                _grid.AutoGenerateColumns = true;
                _page.Controls.Add(_grid);
                _grid.MasterTableView.DataSource = DataSource;
                _grid.DataBind();
                _grid.MasterTableView.ExportToCSV();
            }
        }
    }
}