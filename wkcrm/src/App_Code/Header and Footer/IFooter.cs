﻿
using Microsoft.VisualBasic;
  
namespace WKCRM.UI
{

  

    public interface IFooter {

#region Interface Properties
        
        System.Web.UI.WebControls.Literal Copyright {get;}
                
        System.Web.UI.WebControls.Image Image {get;}
                

#endregion

    }

  
}
  