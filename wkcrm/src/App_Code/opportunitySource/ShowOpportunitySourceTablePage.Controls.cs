﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowOpportunitySourceTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowOpportunitySourceTablePage
{
  

#region "Section 1: Place your customizations here."

    
public class opportunitySourceTableControlRow : BaseopportunitySourceTableControlRow
{
      
        // The BaseopportunitySourceTableControlRow implements code for a ROW within the
        // the opportunitySourceTableControl table.  The BaseopportunitySourceTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of opportunitySourceTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void Delete()
    {
        if (this.IsNewRecord)
        {
            return;
        }

        KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
        OpportunitySourceRecord rec = OpportunitySourceTable.GetRecord(pk,true);
        rec.status_id = false;
        rec.Save();

        ((opportunitySourceTableControl)MiscUtils.GetParentControlObject(this, "opportunitySourceTableControl")).DataChanged = true;
        ((opportunitySourceTableControl)MiscUtils.GetParentControlObject(this, "opportunitySourceTableControl")).ResetData = true;
    }

}

  

public class opportunitySourceTableControl : BaseopportunitySourceTableControl
{
        // The BaseopportunitySourceTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The opportunitySourceTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

    public override void LoadData()
    {
        if (!this.Page.IsPostBack)
            Populatestatus_idFilter("Yes", 100);
        base.LoadData();
    }
}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the opportunitySourceTableControlRow control on the ShowOpportunitySourceTablePage page.
// Do not modify this class. Instead override any method in opportunitySourceTableControlRow.
public class BaseopportunitySourceTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseopportunitySourceTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in opportunitySourceTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.opportunitySourceRecordRowEditButton.Click += new ImageClickEventHandler(opportunitySourceRecordRowEditButton_Click);
        }

        // To customize, override this method in opportunitySourceTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in opportunitySourceTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = OpportunitySourceTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseopportunitySourceTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new OpportunitySourceRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in opportunitySourceTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.display_orderSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunitySourceTable.display_order);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.display_order.Text = formattedValue;
            } else {  
                this.display_order.Text = OpportunitySourceTable.display_order.Format(OpportunitySourceTable.display_order.DefaultValue);
            }
                    
            if (this.display_order.Text == null ||
                this.display_order.Text.Trim().Length == 0) {
                this.display_order.Text = "&nbsp;";
            }
                  
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunitySourceTable.name);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = OpportunitySourceTable.name.Format(OpportunitySourceTable.name.DefaultValue);
            }
                    
            if (this.name.Text == null ||
                this.name.Text.Trim().Length == 0) {
                this.name.Text = "&nbsp;";
            }
                  
            if (this.DataSource.opportunity_category_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunitySourceTable.opportunity_category_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.opportunity_category_id.Text = formattedValue;
            } else {  
                this.opportunity_category_id.Text = OpportunitySourceTable.opportunity_category_id.Format(OpportunitySourceTable.opportunity_category_id.DefaultValue);
            }
                    
            if (this.opportunity_category_id.Text == null ||
                this.opportunity_category_id.Text.Trim().Length == 0) {
                this.opportunity_category_id.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in opportunitySourceTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in opportunitySourceTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in opportunitySourceTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((opportunitySourceTableControl)MiscUtils.GetParentControlObject(this, "opportunitySourceTableControl")).DataChanged = true;
                ((opportunitySourceTableControl)MiscUtils.GetParentControlObject(this, "opportunitySourceTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in opportunitySourceTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in opportunitySourceTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in opportunitySourceTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            OpportunitySourceTable.DeleteRecord(pk);

          
            ((opportunitySourceTableControl)MiscUtils.GetParentControlObject(this, "opportunitySourceTableControl")).DataChanged = true;
            ((opportunitySourceTableControl)MiscUtils.GetParentControlObject(this, "opportunitySourceTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void opportunitySourceRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../opportunitySource/EditOpportunitySourcePage.aspx?OpportunitySource={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseopportunitySourceTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseopportunitySourceTableControlRow_Rec"] = value;
            }
        }
        
        private OpportunitySourceRecord _DataSource;
        public OpportunitySourceRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal display_order {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "display_order");
            }
        }
           
        public System.Web.UI.WebControls.Literal name {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
           
        public System.Web.UI.WebControls.Literal opportunity_category_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_category_id");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton opportunitySourceRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceRecordRowEditButton");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox opportunitySourceRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceRecordRowSelection");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            OpportunitySourceRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public OpportunitySourceRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return OpportunitySourceTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the opportunitySourceTableControl control on the ShowOpportunitySourceTablePage page.
// Do not modify this class. Instead override any method in opportunitySourceTableControl.
public class BaseopportunitySourceTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseopportunitySourceTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        
            this.nameLabel1.Click += new EventHandler(nameLabel1_Click);
            this.opportunity_category_idLabel1.Click += new EventHandler(opportunity_category_idLabel1_Click);

            // Setup the button events.
        
            this.opportunitySourceDeleteButton.Button.Click += new EventHandler(opportunitySourceDeleteButton_Click);
            this.opportunitySourceEditButton.Button.Click += new EventHandler(opportunitySourceEditButton_Click);
            this.opportunitySourceExportButton.Button.Click += new EventHandler(opportunitySourceExportButton_Click);
            this.opportunitySourceNewButton.Button.Click += new EventHandler(opportunitySourceNewButton_Click);
            this.opportunitySourceSearchButton.Button.Click += new EventHandler(opportunitySourceSearchButton_Click);

            // Setup the filter and search events.
        
            this.opportunity_category_idFilter.SelectedIndexChanged += new EventHandler(opportunity_category_idFilter_SelectedIndexChanged);
            this.status_idFilter.SelectedIndexChanged += new EventHandler(status_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.opportunity_category_idFilter)) {
                this.opportunity_category_idFilter.Items.Add(new ListItem(this.GetFromSession(this.opportunity_category_idFilter), this.GetFromSession(this.opportunity_category_idFilter)));
                this.opportunity_category_idFilter.SelectedValue = this.GetFromSession(this.opportunity_category_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.opportunitySourceSearchArea)) {
                
                this.opportunitySourceSearchArea.Text = this.GetFromSession(this.opportunitySourceSearchArea);
            }
            if (!this.Page.IsPostBack && this.InSession(this.status_idFilter)) {
                this.status_idFilter.Items.Add(new ListItem(this.GetFromSession(this.status_idFilter), this.GetFromSession(this.status_idFilter)));
                this.status_idFilter.SelectedValue = this.GetFromSession(this.status_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(OpportunitySourceTable.opportunity_category_id, OrderByItem.OrderDir.Asc);
        
                this.CurrentSortOrder.Add(OpportunitySourceTable.name, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "500"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.opportunitySourceDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (OpportunitySourceRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.OpportunitySourceRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = OpportunitySourceTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (OpportunitySourceRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.OpportunitySourceRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (opportunitySourceTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (OpportunitySourceRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.OpportunitySourceRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = OpportunitySourceTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateopportunity_category_idFilter(MiscUtils.GetSelectedValue(this.opportunity_category_idFilter, this.GetFromSession(this.opportunity_category_idFilter)), 500);
            this.Populatestatus_idFilter(MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("opportunitySourceTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                opportunitySourceTableControlRow recControl = (opportunitySourceTableControlRow)(repItem.FindControl("opportunitySourceTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(OpportunitySourceTable.opportunity_category_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for opportunitySourceTableControl pagination.
        

            // Bind the pagination labels.
        
            this.opportunitySourceTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (opportunitySourceTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            OpportunitySourceTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.opportunity_category_idFilter)) {
                wc.iAND(OpportunitySourceTable.opportunity_category_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.opportunity_category_idFilter, this.GetFromSession(this.opportunity_category_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.opportunitySourceSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(OpportunitySourceTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.opportunitySourceSearchArea, this.GetFromSession(this.opportunitySourceSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.status_idFilter)) {
                wc.iAND(OpportunitySourceTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("opportunitySourceTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    opportunitySourceTableControlRow recControl = (opportunitySourceTableControlRow)(repItem.FindControl("opportunitySourceTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        OpportunitySourceRecord rec = new OpportunitySourceRecord();
        
                        if (recControl.display_order.Text != "") {
                            rec.Parse(recControl.display_order.Text, OpportunitySourceTable.display_order);
                        }
                        if (recControl.name.Text != "") {
                            rec.Parse(recControl.name.Text, OpportunitySourceTable.name);
                        }
                        if (recControl.opportunity_category_id.Text != "") {
                            rec.Parse(recControl.opportunity_category_id.Text, OpportunitySourceTable.opportunity_category_id);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new OpportunitySourceRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (OpportunitySourceRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.OpportunitySourceRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(opportunitySourceTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(opportunitySourceTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for opportunity_category_idFilter.
        protected virtual void Populateopportunity_category_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(OpportunityCategoryTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.opportunity_category_idFilter.Items.Clear();
            foreach (OpportunityCategoryRecord itemValue in OpportunityCategoryTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(OpportunityCategoryTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.opportunity_category_idFilter.Items.IndexOf(item) < 0) {
                    this.opportunity_category_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.opportunity_category_idFilter, selectedValue);

            // Add the All item.
            this.opportunity_category_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for status_idFilter.
        protected virtual void Populatestatus_idFilter(string selectedValue, int maxItems)
        {
              
            // Setup the WHERE clause, including the base table if needed.
                
            WhereClause wc = new WhereClause();
                  
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(OpportunitySourceTable.status_id, OrderByItem.OrderDir.Asc);

            string[] list = OpportunitySourceTable.GetValues(OpportunitySourceTable.status_id, wc, orderBy, maxItems);
            
            this.status_idFilter.Items.Clear();
            foreach (string itemValue in list)
            {
                // Create the item and add to the list.
                string fvalue = OpportunitySourceTable.status_id.Format(itemValue);
                ListItem item = new ListItem(fvalue, itemValue);
                this.status_idFilter.Items.Add(item);
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.status_idFilter, selectedValue);

            // Add the All item.
            this.status_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter opportunity_category_idFilter.
        public virtual WhereClause CreateWhereClause_opportunity_category_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.opportunitySourceSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(OpportunitySourceTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.opportunitySourceSearchArea, this.GetFromSession(this.opportunitySourceSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.status_idFilter)) {
                wc.iAND(OpportunitySourceTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter opportunitySourceSearchArea.
        public virtual WhereClause CreateWhereClause_opportunitySourceSearchArea()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.opportunity_category_idFilter)) {
                wc.iAND(OpportunitySourceTable.opportunity_category_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.opportunity_category_idFilter, this.GetFromSession(this.opportunity_category_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.status_idFilter)) {
                wc.iAND(OpportunitySourceTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter status_idFilter.
        public virtual WhereClause CreateWhereClause_status_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.opportunity_category_idFilter)) {
                wc.iAND(OpportunitySourceTable.opportunity_category_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.opportunity_category_idFilter, this.GetFromSession(this.opportunity_category_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.opportunitySourceSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(OpportunitySourceTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.opportunitySourceSearchArea, this.GetFromSession(this.opportunitySourceSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.opportunity_category_idFilter, this.opportunity_category_idFilter.SelectedValue);
            this.SaveToSession(this.opportunitySourceSearchArea, this.opportunitySourceSearchArea.Text);
            this.SaveToSession(this.status_idFilter, this.status_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.opportunity_category_idFilter);
            this.RemoveFromSession(this.opportunitySourceSearchArea);
            this.RemoveFromSession(this.status_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["opportunitySourceTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["opportunitySourceTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void nameLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(OpportunitySourceTable.name);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(OpportunitySourceTable.name, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void opportunity_category_idLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(OpportunitySourceTable.opportunity_category_id);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(OpportunitySourceTable.opportunity_category_id, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void opportunitySourceDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void opportunitySourceEditButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../opportunitySource/EditOpportunitySourcePage.aspx?OpportunitySource={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void opportunitySourceExportButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            WhereClause wc = this.CreateWhereClause();
            String exportedData = OpportunitySourceTable.Export(wc);
            BaseClasses.Utils.NetUtils.WriteResponseTextAttachment(this.Page.Response, "OpportunitySourceTable.csv", exportedData);
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void opportunitySourceNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../opportunitySource/AddOpportunitySourcePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void opportunitySourceSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void opportunity_category_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void status_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private OpportunitySourceRecord[] _DataSource = null;
        public  OpportunitySourceRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal display_orderLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "display_orderLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton nameLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel1");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList opportunity_category_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_category_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunity_category_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_category_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton opportunity_category_idLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_category_idLabel1");
            }
        }
        
        public WKCRM.UI.IThemeButton opportunitySourceDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton opportunitySourceEditButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceEditButton");
            }
        }
        
        public WKCRM.UI.IThemeButton opportunitySourceExportButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceExportButton");
            }
        }
        
        public WKCRM.UI.IThemeButton opportunitySourceNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceNewButton");
            }
        }
        
        public System.Web.UI.WebControls.TextBox opportunitySourceSearchArea {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceSearchArea");
            }
        }
        
        public WKCRM.UI.IThemeButton opportunitySourceSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunitySourceTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox opportunitySourceToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label opportunitySourceTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList status_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal status_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_idLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                opportunitySourceTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                OpportunitySourceRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (opportunitySourceTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.opportunitySourceRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public opportunitySourceTableControlRow GetSelectedRecordControl()
        {
        opportunitySourceTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public opportunitySourceTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (opportunitySourceTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.opportunitySourceRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (opportunitySourceTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowOpportunitySourceTablePage.opportunitySourceTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            opportunitySourceTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (opportunitySourceTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.opportunitySourceRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public opportunitySourceTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("opportunitySourceTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                opportunitySourceTableControlRow recControl = (opportunitySourceTableControlRow)repItem.FindControl("opportunitySourceTableControlRow");
                recList.Add(recControl);
            }

            return (opportunitySourceTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowOpportunitySourceTablePage.opportunitySourceTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  