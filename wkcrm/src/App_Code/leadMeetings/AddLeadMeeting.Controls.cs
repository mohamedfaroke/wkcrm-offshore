﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// AddLeadMeeting.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI.Controls.AddLeadMeeting
{
  

#region "Section 1: Place your customizations here."


    public class leadMeetingsRecordControl : BaseleadMeetingsRecordControl
    {

        // The BaseleadMeetingsRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        public leadMeetingsRecordControl()
        {
            this.Load += new EventHandler(leadMeetingsRecordControl_Load);
        }

        public override void DataBind()
        {
            base.DataBind();
            if (this.DataSource != null)
            {
                string accid = this.Page.Request.QueryString["accid"];
                if (String.IsNullOrEmpty(accid))
                    throw new Exception("Account Id parameter is missing");
                AccountRecord rec = AccountTable.GetRecord(accid, false);
                if (rec != null)
                {
                    if (rec.designer_idSpecified)
                        MiscUtils.SetSelectedValue(this.designer_id, rec.designer_id.ToString());
                    this.address.Text = rec.address + " " + rec.suburb + " " + rec.postcode;
                    this.address.Text.Trim();
                }
            }
        }

        void leadMeetingsRecordControl_Load(object sender, EventArgs e)
        {
            this.designer_id.AutoPostBack = true;
        }

        public override void GetUIData()
        {
            base.GetUIData();
            string accid = this.Page.Request.QueryString["accid"];
            if (String.IsNullOrEmpty(accid))
                throw new Exception("Account Id parameter is missing");
            else
                this.DataSource.Parse(accid, LeadMeetingsTable.account_id);
            this.DataSource.Parse(this.Page.SystemUtils.GetUserID(), LeadMeetingsTable.hostess_id);

            RadDatePicker datePicker = this.Page.FindControl("meetingDate") as RadDatePicker;
            RadTimePicker timePicker = this.Page.FindControl("meetingTime") as RadTimePicker;
            RadTimePicker endTimePicker = this.Page.FindControl("meetingEndTime") as RadTimePicker;
            if (datePicker != null && timePicker != null && endTimePicker != null)
            {
                if (datePicker.SelectedDate.HasValue && timePicker.SelectedDate.HasValue && endTimePicker.SelectedDate.HasValue)
                {
                    //Extract the date from the two differnet controls
                    DateTime mDate = datePicker.SelectedDate.Value;
                    DateTime mTime = timePicker.SelectedDate.Value;
                    DateTime eTime = endTimePicker.SelectedDate.Value;
                    this.DataSource.Parse(Globals.CombineDateTime(mDate,mTime), LeadMeetingsTable.datetime0);
                    this.DataSource.Parse(Globals.CombineDateTime(mDate, eTime), LeadMeetingsTable.end_datetime);
                }
            }
        }

        public override WhereClause CreateWhereClause_designer_idDropDownList()
        {
            return Globals.BuildDesignerWC();
        }

        protected override void PopulateonsiteDropDownList(string selectedValue, int maxItems)
        {
            //base.PopulateonsiteDropDownList(selectedValue, maxItems);
            this.onsite.Items.Clear();
            this.onsite.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
            this.onsite.Items.Add(new ListItem("Showroom", "True"));
            this.onsite.Items.Add(new ListItem("Onsite", "False"));

            if (!String.IsNullOrEmpty(selectedValue))
            {
                ListItem itm = onsite.Items.FindByValue(selectedValue);
                if (itm != null)
                    itm.Selected = true;
            }
        }

        public override void Validate()
        {
            base.Validate();
            if (onsite.SelectedItem.Text == "Showroom" &&
                location.SelectedIndex == 0)
            {
                throw new Exception("A location must be selected for a showroom meeting");
            }
            RadDatePicker datePicker = this.Page.FindControl("meetingDate") as RadDatePicker;
            RadTimePicker timePicker = this.Page.FindControl("meetingTime") as RadTimePicker;
            RadTimePicker endTimePicker = this.Page.FindControl("meetingEndTime") as RadTimePicker;
            if(timePicker != null && endTimePicker != null && datePicker != null)
            {
                if (!timePicker.SelectedDate.HasValue || !endTimePicker.SelectedDate.HasValue || !datePicker.SelectedDate.HasValue)
                    throw new Exception("Please select a valid date and time for the meeting");

                if (timePicker.SelectedDate.Value >= endTimePicker.SelectedDate.Value)
                    throw new Exception("Meeting end time must be after start time");
            }
        }
    }

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the leadMeetingsRecordControl control on the AddLeadMeeting page.
// Do not modify this class. Instead override any method in leadMeetingsRecordControl.
public class BaseleadMeetingsRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseleadMeetingsRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in leadMeetingsRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in leadMeetingsRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in leadMeetingsRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = LeadMeetingsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new LeadMeetingsRecord();
                return;
            }

            // Retrieve the record from the database.
            LeadMeetingsRecord[] recList = LeadMeetingsTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = LeadMeetingsTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in leadMeetingsRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.address);
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = LeadMeetingsTable.address.Format(LeadMeetingsTable.address.DefaultValue);
            }
                    
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.notes);
                this.bench_and_benchtop_selection.Text = formattedValue;
            } else {  
                this.bench_and_benchtop_selection.Text = LeadMeetingsTable.notes.Format(LeadMeetingsTable.notes.DefaultValue);
            }
                    
            if (this.DataSource.designer_idSpecified) {
                this.Populatedesigner_idDropDownList(this.DataSource.designer_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatedesigner_idDropDownList(LeadMeetingsTable.designer_id.DefaultValue, 100);
                } else {
                this.Populatedesigner_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.location_idSpecified) {
                this.PopulatelocationDropDownList(this.DataSource.location_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulatelocationDropDownList(LeadMeetingsTable.location_id.DefaultValue, 100);
                } else {
                this.PopulatelocationDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.onsiteSpecified) {
                this.PopulateonsiteDropDownList(this.DataSource.onsite.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateonsiteDropDownList(LeadMeetingsTable.onsite.DefaultValue, 100);
                } else {
                this.PopulateonsiteDropDownList(null, 100);
                }
            }
                
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in leadMeetingsRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in leadMeetingsRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in leadMeetingsRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in leadMeetingsRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.address.Text, LeadMeetingsTable.address);
                          
            this.DataSource.Parse(this.bench_and_benchtop_selection.Text, LeadMeetingsTable.notes);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.designer_id), LeadMeetingsTable.designer_id);
                  
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.location), LeadMeetingsTable.location_id);
                  
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.onsite), LeadMeetingsTable.onsite);
                  
        }

        //  To customize, override this method in leadMeetingsRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in leadMeetingsRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            LeadMeetingsTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_designer_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_locationDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_onsiteDropDownList() {
            return new WhereClause();
        }
                
        // Fill the designer_id list.
        protected virtual void Populatedesigner_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_designer_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

                      this.designer_id.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.designer_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.designer_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.designer_id, LeadMeetingsTable.designer_id.Format(selectedValue))) {
                string fvalue = LeadMeetingsTable.designer_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.designer_id.Items.Insert(0, item);
            }

                  
            this.designer_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the location list.
        protected virtual void PopulatelocationDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_locationDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(LocationTable.name, OrderByItem.OrderDir.Asc);

                      this.location.Items.Clear();
            foreach (LocationRecord itemValue in LocationTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(LocationTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.location.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.location, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.location, LeadMeetingsTable.location_id.Format(selectedValue))) {
                string fvalue = LeadMeetingsTable.location_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.location.Items.Insert(0, item);
            }

                  
            this.location.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the onsite list.
        protected virtual void PopulateonsiteDropDownList
                (string selectedValue, int maxItems) {
                  
            // Create the WHERE clause to filter the list.
            WhereClause wc  = this.CreateWhereClause_onsiteDropDownList();

            // Create the ORDER BY clause to sort based on the displayed value.
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(LeadMeetingsTable.onsite, OrderByItem.OrderDir.Asc);

            // Populate the dropdown list in the sort order specified above.
            this.onsite.Items.Clear();
            foreach (string itemValue in LeadMeetingsTable.GetValues(LeadMeetingsTable.onsite, wc, orderBy, maxItems)) {
                // Create the dropdown list item and add it to the list.
                string fvalue = LeadMeetingsTable.onsite.Format(itemValue, @"Showroom,Onsite");
                ListItem item = new ListItem(fvalue, itemValue);
                this.onsite.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.onsite, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.onsite, LeadMeetingsTable.onsite.Format(selectedValue, @"Showroom,Onsite"))) {
                string fvalue = LeadMeetingsTable.onsite.Format(selectedValue, @"Showroom,Onsite");
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.onsite.Items.Insert(0, item);
            }

                  
            this.onsite.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseleadMeetingsRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseleadMeetingsRecordControl_Rec"] = value;
            }
        }
        
        private LeadMeetingsRecord _DataSource;
        public LeadMeetingsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.TextBox address {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox bench_and_benchtop_selection {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "bench_and_benchtop_selection");
            }
        }
        
        public System.Web.UI.WebControls.Literal bench_and_benchtop_selectionLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "bench_and_benchtop_selectionLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList designer_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.Label Label1 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label1");
            }
        }
        
        public System.Web.UI.WebControls.Label Label2 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label2");
            }
        }
        
        public System.Web.UI.WebControls.Label Label3 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label3");
            }
        }
        
        public System.Web.UI.WebControls.Image leadMeetingsDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal leadMeetingsDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList location {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location");
            }
        }
        
        public System.Web.UI.WebControls.Label locationLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "locationLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList onsite {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "onsite");
            }
        }
        
        public System.Web.UI.WebControls.Literal onsiteLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "onsiteLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            LeadMeetingsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public LeadMeetingsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new LeadMeetingsRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  