﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// AddEvaluationQuestionOptionsPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.AddEvaluationQuestionOptionsPage
{
  

#region "Section 1: Place your customizations here."

    
public class EvaluationQuestionOptionsRecordControl : BaseEvaluationQuestionOptionsRecordControl
{
      
        // The BaseEvaluationQuestionOptionsRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the EvaluationQuestionOptionsRecordControl control on the AddEvaluationQuestionOptionsPage page.
// Do not modify this class. Instead override any method in EvaluationQuestionOptionsRecordControl.
public class BaseEvaluationQuestionOptionsRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseEvaluationQuestionOptionsRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in EvaluationQuestionOptionsRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in EvaluationQuestionOptionsRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EvaluationQuestionOptionsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new EvaluationQuestionOptionsRecord();
                return;
            }

            // Retrieve the record from the database.
            EvaluationQuestionOptionsRecord[] recList = EvaluationQuestionOptionsTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = EvaluationQuestionOptionsTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.OptionIdSpecified) {
                this.PopulateOptionIdDropDownList(this.DataSource.OptionId.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateOptionIdDropDownList(EvaluationQuestionOptionsTable.OptionId.DefaultValue, 100);
                } else {
                this.PopulateOptionIdDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.QuestionIdSpecified) {
                this.PopulateQuestionIdDropDownList(this.DataSource.QuestionId.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateQuestionIdDropDownList(EvaluationQuestionOptionsTable.QuestionId.DefaultValue, 100);
                } else {
                this.PopulateQuestionIdDropDownList(null, 100);
                }
            }
                
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in EvaluationQuestionOptionsRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in EvaluationQuestionOptionsRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.OptionId), EvaluationQuestionOptionsTable.OptionId);
                  
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.QuestionId), EvaluationQuestionOptionsTable.QuestionId);
                  
        }

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EvaluationQuestionOptionsTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_OptionIdDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_QuestionIdDropDownList() {
            return new WhereClause();
        }
                
        // Fill the OptionId list.
        protected virtual void PopulateOptionIdDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_OptionIdDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(QuestionOptionTable.OptionText, OrderByItem.OrderDir.Asc);

                      this.OptionId.Items.Clear();
            foreach (QuestionOptionRecord itemValue in QuestionOptionTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.OptionIDSpecified) {
                    cvalue = itemValue.OptionID.ToString();
                    fvalue = itemValue.Format(QuestionOptionTable.OptionText);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.OptionId.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.OptionId, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.OptionId, EvaluationQuestionOptionsTable.OptionId.Format(selectedValue))) {
                string fvalue = EvaluationQuestionOptionsTable.OptionId.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.OptionId.Items.Insert(0, item);
            }

                  
            this.OptionId.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the QuestionId list.
        protected virtual void PopulateQuestionIdDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_QuestionIdDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EvaluationQuestionTable.Question, OrderByItem.OrderDir.Asc);

                      this.QuestionId.Items.Clear();
            foreach (EvaluationQuestionRecord itemValue in EvaluationQuestionTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.QuestionIDSpecified) {
                    cvalue = itemValue.QuestionID.ToString();
                    fvalue = itemValue.Format(EvaluationQuestionTable.Question);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.QuestionId.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.QuestionId, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.QuestionId, EvaluationQuestionOptionsTable.QuestionId.Format(selectedValue))) {
                string fvalue = EvaluationQuestionOptionsTable.QuestionId.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.QuestionId.Items.Insert(0, item);
            }

                  
            this.QuestionId.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseEvaluationQuestionOptionsRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseEvaluationQuestionOptionsRecordControl_Rec"] = value;
            }
        }
        
        private EvaluationQuestionOptionsRecord _DataSource;
        public EvaluationQuestionOptionsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Image EvaluationQuestionOptionsDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal EvaluationQuestionOptionsDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList OptionId {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionId");
            }
        }
        
        public System.Web.UI.WebControls.Literal OptionIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionIdLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList QuestionId {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionId");
            }
        }
        
        public System.Web.UI.WebControls.Literal QuestionIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionIdLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EvaluationQuestionOptionsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EvaluationQuestionOptionsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new EvaluationQuestionOptionsRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  