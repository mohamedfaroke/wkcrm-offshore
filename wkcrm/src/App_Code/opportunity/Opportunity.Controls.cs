﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// Opportunity.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.Opportunity
{
  

#region "Section 1: Place your customizations here."

    
public class opportunityRecordControl : BaseopportunityRecordControl
{
      
        // The BaseopportunityRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
    
    protected override void Populatelocation_idDropDownList(string selectedValue, int maxItems)
    {
        string userId = this.Page.SystemUtils.GetUserID();
        string locid = this.Page.Request["locid"];
        string useLocid = "";
        if (String.IsNullOrEmpty(locid))
        {
            EmployeeRecord employee = EmployeeTable.GetRecord(userId, false);
            useLocid = employee.location_id.ToString();
        }
        else
        {
            useLocid = locid;
        }
        base.Populatelocation_idDropDownList(useLocid, maxItems);
    }


    protected override void Populateopportunity_source_idDropDownList(string selectedValue, int maxItems)
    {

        //Setup the WHERE clause.
        WhereClause wc = CreateWhereClause_opportunity_source_idDropDownList();
        OrderBy orderBy = new OrderBy(false, true);
        orderBy.Add(OpportunitySourceTable.display_order, OrderByItem.OrderDir.Asc);
        orderBy.Add(OpportunitySourceTable.name, OrderByItem.OrderDir.Asc);

        this.opportunity_source_id.Items.Clear();
        foreach (OpportunitySourceRecord itemValue in OpportunitySourceTable.GetRecords(wc, orderBy, 0, maxItems))
        {
            // Create the item and add to the list.
            string cvalue = null;
            string fvalue = null;
            if (itemValue.id0Specified)
            {
                cvalue = itemValue.id0.ToString();
                fvalue = itemValue.Format(OpportunitySourceTable.name);
            }

            ListItem item = new ListItem(fvalue, cvalue);
            this.opportunity_source_id.Items.Add(item);
        }

        // Setup the selected item.
        if (selectedValue != null &&
            selectedValue.Length > 0 &&
            !MiscUtils.SetSelectedValue(this.opportunity_source_id, selectedValue) &&
            !MiscUtils.SetSelectedValue(this.opportunity_source_id, OpportunityTable.opportunity_source_id.Format(selectedValue)))
        {
            string fvalue = OpportunityTable.opportunity_source_id.Format(selectedValue);
            ListItem item = new ListItem(fvalue, selectedValue);
            item.Selected = true;
            this.opportunity_source_id.Items.Insert(0, item);
        }


        this.opportunity_source_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
    }

    //public override WhereClause CreateWhereClause_contact_source_idDropDownList()
    //{
    //    return Globals.BuildContactSourceWC();
    //}

    public override WhereClause CreateWhereClause_opportunity_source_idDropDownList()
    {
        return Globals.BuildOpSourceWC();
    }

    public override void GetUIData()
    {
        base.GetUIData();
        this.DataSource.Parse(DateTime.Now, OpportunityTable.datetime0);
        this.DataSource.Parse(this.Page.SystemUtils.GetUserID(), OpportunityTable.employee_id);
        if (!this.DataSource.contact_source_idSpecified)
            this.DataSource.contact_source_id = Globals.CONTACT_UNKNOWN;
        if (!this.DataSource.opportunity_source_idSpecified)
            this.DataSource.opportunity_source_id = Globals.OPPORTUNITY_UNKNOWN;
    }

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the opportunityRecordControl control on the Opportunity page.
// Do not modify this class. Instead override any method in opportunityRecordControl.
public class BaseopportunityRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseopportunityRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in opportunityRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in opportunityRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in opportunityRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = OpportunityTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new OpportunityRecord();
                return;
            }

            // Retrieve the record from the database.
            OpportunityRecord[] recList = OpportunityTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = OpportunityTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in opportunityRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.location_idSpecified) {
                this.Populatelocation_idDropDownList(this.DataSource.location_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatelocation_idDropDownList(OpportunityTable.location_id.DefaultValue, 100);
                } else {
                this.Populatelocation_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.opportunity_source_idSpecified) {
                this.Populateopportunity_source_idDropDownList(this.DataSource.opportunity_source_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateopportunity_source_idDropDownList(OpportunityTable.opportunity_source_id.DefaultValue, 100);
                } else {
                this.Populateopportunity_source_idDropDownList(null, 100);
                }
            }
                
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in opportunityRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in opportunityRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in opportunityRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in opportunityRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.location_id), OpportunityTable.location_id);
                  
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.opportunity_source_id), OpportunityTable.opportunity_source_id);
                  
        }

        //  To customize, override this method in opportunityRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in opportunityRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            OpportunityTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_location_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_opportunity_source_idDropDownList() {
            return new WhereClause();
        }
                
        // Fill the location_id list.
        protected virtual void Populatelocation_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_location_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(LocationTable.name, OrderByItem.OrderDir.Asc);

                      this.location_id.Items.Clear();
            foreach (LocationRecord itemValue in LocationTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(LocationTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.location_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.location_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.location_id, OpportunityTable.location_id.Format(selectedValue))) {
                string fvalue = OpportunityTable.location_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.location_id.Items.Insert(0, item);
            }

                  
            this.location_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the opportunity_source_id list.
        protected virtual void Populateopportunity_source_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_opportunity_source_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(OpportunitySourceTable.name, OrderByItem.OrderDir.Asc);

                      this.opportunity_source_id.Items.Clear();
            foreach (OpportunitySourceRecord itemValue in OpportunitySourceTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(OpportunitySourceTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.opportunity_source_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.opportunity_source_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.opportunity_source_id, OpportunityTable.opportunity_source_id.Format(selectedValue))) {
                string fvalue = OpportunityTable.opportunity_source_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.opportunity_source_id.Items.Insert(0, item);
            }

                  
            this.opportunity_source_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseopportunityRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseopportunityRecordControl_Rec"] = value;
            }
        }
        
        private OpportunityRecord _DataSource;
        public OpportunityRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.DropDownList location_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal location_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList opportunity_source_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunity_source_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image opportunityDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunityDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunityDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunityDialogTitle");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            OpportunityRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public OpportunityRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new OpportunityRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  