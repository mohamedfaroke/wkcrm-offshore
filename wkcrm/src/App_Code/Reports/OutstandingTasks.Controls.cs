﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// OutstandingTasks.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.OutstandingTasks
{
  

#region "Section 1: Place your customizations here."

    
public class taskNotificationTableControlRow : BasetaskNotificationTableControlRow
{
      
        // The BasetaskNotificationTableControlRow implements code for a ROW within the
        // the taskNotificationTableControl table.  The BasetaskNotificationTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of taskNotificationTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class taskNotificationTableControl : BasetaskNotificationTableControl
{
        // The BasetaskNotificationTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The taskNotificationTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the taskNotificationTableControlRow control on the OutstandingTasks page.
// Do not modify this class. Instead override any method in taskNotificationTableControlRow.
public class BasetaskNotificationTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasetaskNotificationTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in taskNotificationTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in taskNotificationTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in taskNotificationTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = TaskNotificationTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasetaskNotificationTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new TaskNotificationRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in taskNotificationTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.completedSpecified) {
                      
                string formattedValue = this.DataSource.Format(TaskNotificationTable.completed);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.completed.Text = formattedValue;
            } else {  
                this.completed.Text = TaskNotificationTable.completed.Format(TaskNotificationTable.completed.DefaultValue);
            }
                    
            if (this.completed.Text == null ||
                this.completed.Text.Trim().Length == 0) {
                this.completed.Text = "&nbsp;";
            }
                  
            if (this.DataSource.date_completedSpecified) {
                      
                string formattedValue = this.DataSource.Format(TaskNotificationTable.date_completed);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.date_completed.Text = formattedValue;
            } else {  
                this.date_completed.Text = TaskNotificationTable.date_completed.Format(TaskNotificationTable.date_completed.DefaultValue);
            }
                    
            if (this.date_completed.Text == null ||
                this.date_completed.Text.Trim().Length == 0) {
                this.date_completed.Text = "&nbsp;";
            }
                  
            if (this.DataSource.date_createdSpecified) {
                      
                string formattedValue = this.DataSource.Format(TaskNotificationTable.date_created);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.date_created.Text = formattedValue;
            } else {  
                this.date_created.Text = TaskNotificationTable.date_created.Format(TaskNotificationTable.date_created.DefaultValue);
            }
                    
            if (this.date_created.Text == null ||
                this.date_created.Text.Trim().Length == 0) {
                this.date_created.Text = "&nbsp;";
            }
                  
            if (this.DataSource.date_viewedSpecified) {
                      
                string formattedValue = this.DataSource.Format(TaskNotificationTable.date_viewed);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.date_viewed.Text = formattedValue;
            } else {  
                this.date_viewed.Text = TaskNotificationTable.date_viewed.Format(TaskNotificationTable.date_viewed.DefaultValue);
            }
                    
            if (this.date_viewed.Text == null ||
                this.date_viewed.Text.Trim().Length == 0) {
                this.date_viewed.Text = "&nbsp;";
            }
                  
            if (this.DataSource.employee_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(TaskNotificationTable.employee_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.employee_id.Text = formattedValue;
            } else {  
                this.employee_id.Text = TaskNotificationTable.employee_id.Format(TaskNotificationTable.employee_id.DefaultValue);
            }
                    
            if (this.employee_id.Text == null ||
                this.employee_id.Text.Trim().Length == 0) {
                this.employee_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.TaskDescriptionSpecified) {
                      
                string formattedValue = this.DataSource.Format(TaskNotificationTable.TaskDescription);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.TaskNotificationTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"TaskDescription\\\", \\\"Task Description\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.TaskDescription.Text = formattedValue;
            } else {  
                this.TaskDescription.Text = TaskNotificationTable.TaskDescription.Format(TaskNotificationTable.TaskDescription.DefaultValue);
            }
                    
            if (this.TaskDescription.Text == null ||
                this.TaskDescription.Text.Trim().Length == 0) {
                this.TaskDescription.Text = "&nbsp;";
            }
                  
            if (this.DataSource.taskNotificationTypeSpecified) {
                      
                string formattedValue = this.DataSource.Format(TaskNotificationTable.taskNotificationType);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.taskNotificationType.Text = formattedValue;
            } else {  
                this.taskNotificationType.Text = TaskNotificationTable.taskNotificationType.Format(TaskNotificationTable.taskNotificationType.DefaultValue);
            }
                    
            if (this.taskNotificationType.Text == null ||
                this.taskNotificationType.Text.Trim().Length == 0) {
                this.taskNotificationType.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in taskNotificationTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in taskNotificationTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in taskNotificationTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((taskNotificationTableControl)MiscUtils.GetParentControlObject(this, "taskNotificationTableControl")).DataChanged = true;
                ((taskNotificationTableControl)MiscUtils.GetParentControlObject(this, "taskNotificationTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in taskNotificationTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in taskNotificationTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in taskNotificationTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            TaskNotificationTable.DeleteRecord(pk);

          
            ((taskNotificationTableControl)MiscUtils.GetParentControlObject(this, "taskNotificationTableControl")).DataChanged = true;
            ((taskNotificationTableControl)MiscUtils.GetParentControlObject(this, "taskNotificationTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasetaskNotificationTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasetaskNotificationTableControlRow_Rec"] = value;
            }
        }
        
        private TaskNotificationRecord _DataSource;
        public TaskNotificationRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal completed {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "completed");
            }
        }
           
        public System.Web.UI.WebControls.Literal date_completed {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_completed");
            }
        }
           
        public System.Web.UI.WebControls.Literal date_created {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_created");
            }
        }
           
        public System.Web.UI.WebControls.Literal date_viewed {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_viewed");
            }
        }
           
        public System.Web.UI.WebControls.Literal employee_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal TaskDescription {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TaskDescription");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox taskNotificationRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Literal taskNotificationType {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationType");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            TaskNotificationRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public TaskNotificationRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return TaskNotificationTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the taskNotificationTableControl control on the OutstandingTasks page.
// Do not modify this class. Instead override any method in taskNotificationTableControl.
public class BasetaskNotificationTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasetaskNotificationTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.taskNotificationPagination.FirstPage.Click += new ImageClickEventHandler(taskNotificationPagination_FirstPage_Click);
            this.taskNotificationPagination.LastPage.Click += new ImageClickEventHandler(taskNotificationPagination_LastPage_Click);
            this.taskNotificationPagination.NextPage.Click += new ImageClickEventHandler(taskNotificationPagination_NextPage_Click);
            this.taskNotificationPagination.PageSizeButton.Button.Click += new EventHandler(taskNotificationPagination_PageSizeButton_Click);
            this.taskNotificationPagination.PreviousPage.Click += new ImageClickEventHandler(taskNotificationPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.date_completedLabel.Click += new EventHandler(date_completedLabel_Click);
            this.date_createdLabel.Click += new EventHandler(date_createdLabel_Click);
            this.date_viewedLabel.Click += new EventHandler(date_viewedLabel_Click);

            // Setup the button events.
        
            this.taskNotificationDeleteButton.Button.Click += new EventHandler(taskNotificationDeleteButton_Click);
            this.taskNotificationSearchButton.Button.Click += new EventHandler(taskNotificationSearchButton_Click);

            // Setup the filter and search events.
        
            this.completedFilter.SelectedIndexChanged += new EventHandler(completedFilter_SelectedIndexChanged);
            this.employee_idFilter.SelectedIndexChanged += new EventHandler(employee_idFilter_SelectedIndexChanged);
            this.taskNotificationTypeFilter.SelectedIndexChanged += new EventHandler(taskNotificationTypeFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.completedFilter)) {
                this.completedFilter.Items.Add(new ListItem(this.GetFromSession(this.completedFilter), this.GetFromSession(this.completedFilter)));
                this.completedFilter.SelectedValue = this.GetFromSession(this.completedFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.employee_idFilter)) {
                this.employee_idFilter.Items.Add(new ListItem(this.GetFromSession(this.employee_idFilter), this.GetFromSession(this.employee_idFilter)));
                this.employee_idFilter.SelectedValue = this.GetFromSession(this.employee_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.taskNotificationSearchArea)) {
                
                this.taskNotificationSearchArea.Text = this.GetFromSession(this.taskNotificationSearchArea);
            }
            if (!this.Page.IsPostBack && this.InSession(this.taskNotificationTypeFilter)) {
                this.taskNotificationTypeFilter.Items.Add(new ListItem(this.GetFromSession(this.taskNotificationTypeFilter), this.GetFromSession(this.taskNotificationTypeFilter)));
                this.taskNotificationTypeFilter.SelectedValue = this.GetFromSession(this.taskNotificationTypeFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(TaskNotificationTable.date_created, OrderByItem.OrderDir.Desc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.taskNotificationDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (TaskNotificationRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.TaskNotificationRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = TaskNotificationTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (TaskNotificationRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.TaskNotificationRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (taskNotificationTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (TaskNotificationRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.TaskNotificationRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = TaskNotificationTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.PopulatecompletedFilter(MiscUtils.GetSelectedValue(this.completedFilter, this.GetFromSession(this.completedFilter)), 500);
            this.Populateemployee_idFilter(MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), 500);
            this.PopulatetaskNotificationTypeFilter(MiscUtils.GetSelectedValue(this.taskNotificationTypeFilter, this.GetFromSession(this.taskNotificationTypeFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("taskNotificationTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                taskNotificationTableControlRow recControl = (taskNotificationTableControlRow)(repItem.FindControl("taskNotificationTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(TaskNotificationTable.employee_id, this.DataSource);
            this.Page.PregetDfkaRecords(TaskNotificationTable.taskNotificationType, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for taskNotificationTableControl pagination.
        
            this.taskNotificationPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.taskNotificationPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.taskNotificationPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.taskNotificationPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.taskNotificationPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.taskNotificationPagination.CurrentPage.Text = "0";
            }
            this.taskNotificationPagination.PageSize.Text = this.PageSize.ToString();
            this.taskNotificationTotalItems.Text = this.TotalRecords.ToString();
            this.taskNotificationPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.taskNotificationPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (taskNotificationTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            TaskNotificationTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.completedFilter)) {
                wc.iAND(TaskNotificationTable.completed, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.completedFilter, this.GetFromSession(this.completedFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(TaskNotificationTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(TaskNotificationTable.TaskDescription, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.taskNotificationSearchArea, this.GetFromSession(this.taskNotificationSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationTypeFilter)) {
                wc.iAND(TaskNotificationTable.taskNotificationType, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.taskNotificationTypeFilter, this.GetFromSession(this.taskNotificationTypeFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.taskNotificationPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.taskNotificationPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("taskNotificationTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    taskNotificationTableControlRow recControl = (taskNotificationTableControlRow)(repItem.FindControl("taskNotificationTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        TaskNotificationRecord rec = new TaskNotificationRecord();
        
                        if (recControl.completed.Text != "") {
                            rec.Parse(recControl.completed.Text, TaskNotificationTable.completed);
                        }
                        if (recControl.date_completed.Text != "") {
                            rec.Parse(recControl.date_completed.Text, TaskNotificationTable.date_completed);
                        }
                        if (recControl.date_created.Text != "") {
                            rec.Parse(recControl.date_created.Text, TaskNotificationTable.date_created);
                        }
                        if (recControl.date_viewed.Text != "") {
                            rec.Parse(recControl.date_viewed.Text, TaskNotificationTable.date_viewed);
                        }
                        if (recControl.employee_id.Text != "") {
                            rec.Parse(recControl.employee_id.Text, TaskNotificationTable.employee_id);
                        }
                        if (recControl.TaskDescription.Text != "") {
                            rec.Parse(recControl.TaskDescription.Text, TaskNotificationTable.TaskDescription);
                        }
                        if (recControl.taskNotificationType.Text != "") {
                            rec.Parse(recControl.taskNotificationType.Text, TaskNotificationTable.taskNotificationType);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new TaskNotificationRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (TaskNotificationRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.TaskNotificationRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(taskNotificationTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(taskNotificationTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for completedFilter.
        protected virtual void PopulatecompletedFilter(string selectedValue, int maxItems)
        {
              
            // Setup the WHERE clause, including the base table if needed.
                
            WhereClause wc = new WhereClause();
                  
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TaskNotificationTable.completed, OrderByItem.OrderDir.Asc);

            string[] list = TaskNotificationTable.GetValues(TaskNotificationTable.completed, wc, orderBy, maxItems);
            
            this.completedFilter.Items.Clear();
            foreach (string itemValue in list)
            {
                // Create the item and add to the list.
                string fvalue = TaskNotificationTable.completed.Format(itemValue);
                ListItem item = new ListItem(fvalue, itemValue);
                this.completedFilter.Items.Add(item);
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.completedFilter, selectedValue);

            // Add the All item.
            this.completedFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for employee_idFilter.
        protected virtual void Populateemployee_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.employee_idFilter.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.employee_idFilter.Items.IndexOf(item) < 0) {
                    this.employee_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.employee_idFilter, selectedValue);

            // Add the All item.
            this.employee_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for taskNotificationTypeFilter.
        protected virtual void PopulatetaskNotificationTypeFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TaskNotificationTypeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.taskNotificationTypeFilter.Items.Clear();
            foreach (TaskNotificationTypeRecord itemValue in TaskNotificationTypeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(TaskNotificationTypeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.taskNotificationTypeFilter.Items.IndexOf(item) < 0) {
                    this.taskNotificationTypeFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.taskNotificationTypeFilter, selectedValue);

            // Add the All item.
            this.taskNotificationTypeFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter completedFilter.
        public virtual WhereClause CreateWhereClause_completedFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(TaskNotificationTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(TaskNotificationTable.TaskDescription, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.taskNotificationSearchArea, this.GetFromSession(this.taskNotificationSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationTypeFilter)) {
                wc.iAND(TaskNotificationTable.taskNotificationType, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.taskNotificationTypeFilter, this.GetFromSession(this.taskNotificationTypeFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter employee_idFilter.
        public virtual WhereClause CreateWhereClause_employee_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.completedFilter)) {
                wc.iAND(TaskNotificationTable.completed, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.completedFilter, this.GetFromSession(this.completedFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(TaskNotificationTable.TaskDescription, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.taskNotificationSearchArea, this.GetFromSession(this.taskNotificationSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationTypeFilter)) {
                wc.iAND(TaskNotificationTable.taskNotificationType, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.taskNotificationTypeFilter, this.GetFromSession(this.taskNotificationTypeFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter taskNotificationSearchArea.
        public virtual WhereClause CreateWhereClause_taskNotificationSearchArea()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.completedFilter)) {
                wc.iAND(TaskNotificationTable.completed, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.completedFilter, this.GetFromSession(this.completedFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(TaskNotificationTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationTypeFilter)) {
                wc.iAND(TaskNotificationTable.taskNotificationType, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.taskNotificationTypeFilter, this.GetFromSession(this.taskNotificationTypeFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter taskNotificationTypeFilter.
        public virtual WhereClause CreateWhereClause_taskNotificationTypeFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.completedFilter)) {
                wc.iAND(TaskNotificationTable.completed, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.completedFilter, this.GetFromSession(this.completedFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(TaskNotificationTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.taskNotificationSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(TaskNotificationTable.TaskDescription, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.taskNotificationSearchArea, this.GetFromSession(this.taskNotificationSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.completedFilter, this.completedFilter.SelectedValue);
            this.SaveToSession(this.employee_idFilter, this.employee_idFilter.SelectedValue);
            this.SaveToSession(this.taskNotificationSearchArea, this.taskNotificationSearchArea.Text);
            this.SaveToSession(this.taskNotificationTypeFilter, this.taskNotificationTypeFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.completedFilter);
            this.RemoveFromSession(this.employee_idFilter);
            this.RemoveFromSession(this.taskNotificationSearchArea);
            this.RemoveFromSession(this.taskNotificationTypeFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["taskNotificationTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["taskNotificationTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void taskNotificationPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void taskNotificationPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void taskNotificationPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void taskNotificationPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void taskNotificationPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void date_completedLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(TaskNotificationTable.date_completed);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(TaskNotificationTable.date_completed, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void date_createdLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(TaskNotificationTable.date_created);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(TaskNotificationTable.date_created, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void date_viewedLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(TaskNotificationTable.date_viewed);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(TaskNotificationTable.date_viewed, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void taskNotificationDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void taskNotificationSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void completedFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void employee_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void taskNotificationTypeFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private TaskNotificationRecord[] _DataSource = null;
        public  TaskNotificationRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.DropDownList completedFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "completedFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal completedLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "completedLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal completedLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "completedLabel1");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton date_completedLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_completedLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton date_createdLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_createdLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton date_viewedLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_viewedLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList employee_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal TaskDescriptionLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TaskDescriptionLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton taskNotificationDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationDeleteButton");
            }
        }
        
        public WKCRM.UI.IPagination taskNotificationPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationPagination");
            }
        }
        
        public System.Web.UI.WebControls.TextBox taskNotificationSearchArea {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationSearchArea");
            }
        }
        
        public WKCRM.UI.IThemeButton taskNotificationSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal taskNotificationTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox taskNotificationToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label taskNotificationTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList taskNotificationTypeFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationTypeFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal taskNotificationTypeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationTypeLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal taskNotificationTypeLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "taskNotificationTypeLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                taskNotificationTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                TaskNotificationRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (taskNotificationTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.taskNotificationRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public taskNotificationTableControlRow GetSelectedRecordControl()
        {
        taskNotificationTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public taskNotificationTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (taskNotificationTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.taskNotificationRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (taskNotificationTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.OutstandingTasks.taskNotificationTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            taskNotificationTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (taskNotificationTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.taskNotificationRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public taskNotificationTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("taskNotificationTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                taskNotificationTableControlRow recControl = (taskNotificationTableControlRow)repItem.FindControl("taskNotificationTableControlRow");
                recList.Add(recControl);
            }

            return (taskNotificationTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.OutstandingTasks.taskNotificationTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  