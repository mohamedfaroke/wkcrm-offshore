﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowEmployeePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowEmployeePage
{
  

#region "Section 1: Place your customizations here."

    
public class employeeRolesTableControlRow : BaseemployeeRolesTableControlRow
{
      
        // The BaseemployeeRolesTableControlRow implements code for a ROW within the
        // the employeeRolesTableControl table.  The BaseemployeeRolesTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of employeeRolesTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class employeeRolesTableControl : BaseemployeeRolesTableControl
{
        // The BaseemployeeRolesTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The employeeRolesTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  
public class employeeRecordControl : BaseemployeeRecordControl
{
      
        // The BaseemployeeRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the employeeRolesTableControlRow control on the ShowEmployeePage page.
// Do not modify this class. Instead override any method in employeeRolesTableControlRow.
public class BaseemployeeRolesTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseemployeeRolesTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in employeeRolesTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in employeeRolesTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in employeeRolesTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EmployeeRolesTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseemployeeRolesTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new EmployeeRolesRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in employeeRolesTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.role_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeRolesTable.role_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.role_id.Text = formattedValue;
            } else {  
                this.role_id.Text = EmployeeRolesTable.role_id.Format(EmployeeRolesTable.role_id.DefaultValue);
            }
                    
            if (this.role_id.Text == null ||
                this.role_id.Text.Trim().Length == 0) {
                this.role_id.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // employee in employeeRecordControl is One To Many to employeeRolesTableControl.
                    
            // Setup the parent id in the record.
            employeeRecordControl recemployeeRecordControl = (employeeRecordControl)this.Page.FindControlRecursively("employeeRecordControl");
            if (recemployeeRecordControl != null && recemployeeRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                recemployeeRecordControl.LoadData();
            }
            if (recemployeeRecordControl == null || recemployeeRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.employee_id = recemployeeRecordControl.DataSource.id0;
            
            // 2. Validate the data.  Override in employeeRolesTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in employeeRolesTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).DataChanged = true;
                ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EmployeeRolesTable.DeleteRecord(pk);

          
            ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).DataChanged = true;
            ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseemployeeRolesTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseemployeeRolesTableControlRow_Rec"] = value;
            }
        }
        
        private EmployeeRolesRecord _DataSource;
        public EmployeeRolesRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal role_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "role_id");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EmployeeRolesRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EmployeeRolesRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return EmployeeRolesTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the employeeRolesTableControl control on the ShowEmployeePage page.
// Do not modify this class. Instead override any method in employeeRolesTableControl.
public class BaseemployeeRolesTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseemployeeRolesTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(EmployeeRolesTable.role_id, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EmployeeRolesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = EmployeeRolesTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EmployeeRolesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (employeeRolesTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (EmployeeRolesRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = EmployeeRolesTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("employeeRolesTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                employeeRolesTableControlRow recControl = (employeeRolesTableControlRow)(repItem.FindControl("employeeRolesTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(EmployeeRolesTable.role_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for employeeRolesTableControl pagination.
        

            // Bind the pagination labels.
        
            this.employeeRolesTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (employeeRolesTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            EmployeeRolesTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        employeeRecordControl parentRecordControl = (employeeRecordControl)(this.Page.FindControlRecursively("employeeRecordControl"));
            EmployeeRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.id0Specified) {
                wc.iAND(EmployeeRolesTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, parentRec.id0.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("employeeRolesTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    employeeRolesTableControlRow recControl = (employeeRolesTableControlRow)(repItem.FindControl("employeeRolesTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        EmployeeRolesRecord rec = new EmployeeRolesRecord();
        
                        if (recControl.role_id.Text != "") {
                            rec.Parse(recControl.role_id.Text, EmployeeRolesTable.role_id);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new EmployeeRolesRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (EmployeeRolesRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(employeeRolesTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(employeeRolesTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["employeeRolesTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["employeeRolesTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private EmployeeRolesRecord[] _DataSource = null;
        public  EmployeeRolesRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal employeeRolesTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label employeeRolesTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal role_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "role_idLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                employeeRolesTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                EmployeeRolesRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public employeeRolesTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public employeeRolesTableControlRow[] GetSelectedRecordControls()
        {
        
            return (employeeRolesTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.ShowEmployeePage.employeeRolesTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            employeeRolesTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (employeeRolesTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public employeeRolesTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("employeeRolesTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                employeeRolesTableControlRow recControl = (employeeRolesTableControlRow)repItem.FindControl("employeeRolesTableControlRow");
                recList.Add(recControl);
            }

            return (employeeRolesTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowEmployeePage.employeeRolesTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the employeeRecordControl control on the ShowEmployeePage page.
// Do not modify this class. Instead override any method in employeeRecordControl.
public class BaseemployeeRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseemployeeRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in employeeRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in employeeRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in employeeRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EmployeeTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new EmployeeRecord();
                return;
            }

            // Retrieve the record from the database.
            EmployeeRecord[] recList = EmployeeTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = EmployeeTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in employeeRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.address);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.EmployeeTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"address\\\", \\\"Address\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = EmployeeTable.address.Format(EmployeeTable.address.DefaultValue);
            }
                    
            if (this.address.Text == null ||
                this.address.Text.Trim().Length == 0) {
                this.address.Text = "&nbsp;";
            }
                  
            if (this.DataSource.dobSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.dob);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.dob.Text = formattedValue;
            } else {  
                this.dob.Text = EmployeeTable.dob.Format(EmployeeTable.dob.DefaultValue);
            }
                    
            if (this.dob.Text == null ||
                this.dob.Text.Trim().Length == 0) {
                this.dob.Text = "&nbsp;";
            }
                  
            if (this.DataSource.emailSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.email);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.EmployeeTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"email\\\", \\\"Email\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.email.Text = formattedValue;
            } else {  
                this.email.Text = EmployeeTable.email.Format(EmployeeTable.email.DefaultValue);
            }
                    
            if (this.email.Text == null ||
                this.email.Text.Trim().Length == 0) {
                this.email.Text = "&nbsp;";
            }
                  
            if (this.DataSource.location_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.location_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.location_id.Text = formattedValue;
            } else {  
                this.location_id.Text = EmployeeTable.location_id.Format(EmployeeTable.location_id.DefaultValue);
            }
                    
            if (this.location_id.Text == null ||
                this.location_id.Text.Trim().Length == 0) {
                this.location_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.mobile);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = EmployeeTable.mobile.Format(EmployeeTable.mobile.DefaultValue);
            }
                    
            if (this.mobile.Text == null ||
                this.mobile.Text.Trim().Length == 0) {
                this.mobile.Text = "&nbsp;";
            }
                  
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.name);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.EmployeeTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"name\\\", \\\"Name\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = EmployeeTable.name.Format(EmployeeTable.name.DefaultValue);
            }
                    
            if (this.name.Text == null ||
                this.name.Text.Trim().Length == 0) {
                this.name.Text = "&nbsp;";
            }
                  
            if (this.DataSource.passwordSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.password);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.password.Text = formattedValue;
            } else {  
                this.password.Text = EmployeeTable.password.Format(EmployeeTable.password.DefaultValue);
            }
                    
            if (this.password.Text == null ||
                this.password.Text.Trim().Length == 0) {
                this.password.Text = "&nbsp;";
            }
                  
            if (this.DataSource.phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.phone);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.phone.Text = formattedValue;
            } else {  
                this.phone.Text = EmployeeTable.phone.Format(EmployeeTable.phone.DefaultValue);
            }
                    
            if (this.phone.Text == null ||
                this.phone.Text.Trim().Length == 0) {
                this.phone.Text = "&nbsp;";
            }
                  
            if (this.DataSource.UserName0Specified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.UserName0);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.UserName1.Text = formattedValue;
            } else {  
                this.UserName1.Text = EmployeeTable.UserName0.Format(EmployeeTable.UserName0.DefaultValue);
            }
                    
            if (this.UserName1.Text == null ||
                this.UserName1.Text.Trim().Length == 0) {
                this.UserName1.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in employeeRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in employeeRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in employeeRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in employeeRecordControl.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in employeeRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            EmployeeTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["Employee"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "Employee"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(EmployeeTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(EmployeeTable.id0).ToString());
            } else {
                
                wc.iAND(EmployeeTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in employeeRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EmployeeTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseemployeeRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseemployeeRecordControl_Rec"] = value;
            }
        }
        
        private EmployeeRecord _DataSource;
        public EmployeeRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal address {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal dob {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dob");
            }
        }
        
        public System.Web.UI.WebControls.Literal dobLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dobLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal email {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email");
            }
        }
        
        public System.Web.UI.WebControls.Literal emailLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "emailLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image employeeDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal employeeDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.Literal location_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal location_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal mobile {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal name {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
        
        public System.Web.UI.WebControls.Literal nameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal password {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "password");
            }
        }
        
        public System.Web.UI.WebControls.Literal passwordLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "passwordLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal phone {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal UserName1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "UserName1");
            }
        }
        
        public System.Web.UI.WebControls.Literal usernameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "usernameLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EmployeeRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EmployeeRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return EmployeeTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  