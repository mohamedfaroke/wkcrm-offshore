﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// AddEmployeePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.AddEmployeePage
{
  

#region "Section 1: Place your customizations here."

    
//public class employeeRolesTableControlRow : BaseemployeeRolesTableControlRow
//{
//      
//        // The BaseemployeeRolesTableControlRow implements code for a ROW within the
//        // the employeeRolesTableControl table.  The BaseemployeeRolesTableControlRow implements the DataBind and SaveData methods.
//        // The loading of data is actually performed by the LoadData method in the base class of employeeRolesTableControl.
//
//        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
//        // SaveData, GetUIData, and Validate methods.
//        
//
//}
//

  

//public class employeeRolesTableControl : BaseemployeeRolesTableControl
//{
//        // The BaseemployeeRolesTableControl class implements the LoadData, DataBind, CreateWhereClause
//        // and other methods to load and display the data in a table control.
//
//        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
//        // The employeeRolesTableControlRow class offers another place where you can customize
//        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.
//
//}
//

  
public class employeeRecordControl : BaseemployeeRecordControl
{
      
        // The BaseemployeeRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the employeeRecordControl control on the AddEmployeePage page.
// Do not modify this class. Instead override any method in employeeRecordControl.
public class BaseemployeeRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseemployeeRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in employeeRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in employeeRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in employeeRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EmployeeTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new EmployeeRecord();
                return;
            }

            // Retrieve the record from the database.
            EmployeeRecord[] recList = EmployeeTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = EmployeeTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in employeeRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.address);
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = EmployeeTable.address.Format(EmployeeTable.address.DefaultValue);
            }
                    
            this.dob.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.dob.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.dobSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.dob);
                this.dob.Text = formattedValue;
            } else {  
                this.dob.Text = EmployeeTable.dob.Format(EmployeeTable.dob.DefaultValue);
            }
                    
            if (this.DataSource.emailSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.email);
                this.email.Text = formattedValue;
            } else {  
                this.email.Text = EmployeeTable.email.Format(EmployeeTable.email.DefaultValue);
            }
                    
            if (this.DataSource.location_idSpecified) {
                this.Populatelocation_idDropDownList(this.DataSource.location_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatelocation_idDropDownList(EmployeeTable.location_id.DefaultValue, 100);
                } else {
                this.Populatelocation_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.mobile);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = EmployeeTable.mobile.Format(EmployeeTable.mobile.DefaultValue);
            }
                    
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.name);
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = EmployeeTable.name.Format(EmployeeTable.name.DefaultValue);
            }
                    
            if (this.DataSource.passwordSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.password);
                this.password.Text = formattedValue;
            } else {  
                this.password.Text = EmployeeTable.password.Format(EmployeeTable.password.DefaultValue);
            }
                    
            if (this.DataSource.phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.phone);
                this.phone.Text = formattedValue;
            } else {  
                this.phone.Text = EmployeeTable.phone.Format(EmployeeTable.phone.DefaultValue);
            }
                    
            if (this.DataSource.UserName0Specified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.UserName0);
                this.UserName1.Text = formattedValue;
            } else {  
                this.UserName1.Text = EmployeeTable.UserName0.Format(EmployeeTable.UserName0.DefaultValue);
            }
                    
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in employeeRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in employeeRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in employeeRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in employeeRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.address.Text, EmployeeTable.address);
                          
            this.DataSource.Parse(this.dob.Text, EmployeeTable.dob);
                          
            this.DataSource.Parse(this.email.Text, EmployeeTable.email);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.location_id), EmployeeTable.location_id);
                  
            this.DataSource.Parse(this.mobile.Text, EmployeeTable.mobile);
                          
            this.DataSource.Parse(this.name.Text, EmployeeTable.name);
                          
            if (!(this.password.TextMode == TextBoxMode.Password) || !(this.password.Text.Trim() == "")) {
                        
                string passwordformattedValue = this.DataSource.Format(EmployeeTable.password);
                          
                if (this.password.Text.Trim() != passwordformattedValue) {
                        
                    this.DataSource.Parse(this.password.Text, EmployeeTable.password);
                          
                }
            }
                      
            this.DataSource.Parse(this.phone.Text, EmployeeTable.phone);
                          
            this.DataSource.Parse(this.UserName1.Text, EmployeeTable.UserName0);
                          
        }

        //  To customize, override this method in employeeRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in employeeRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EmployeeTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_location_idDropDownList() {
            return new WhereClause();
        }
                
        // Fill the location_id list.
        protected virtual void Populatelocation_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_location_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(LocationTable.name, OrderByItem.OrderDir.Asc);

                      this.location_id.Items.Clear();
            foreach (LocationRecord itemValue in LocationTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(LocationTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.location_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.location_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.location_id, EmployeeTable.location_id.Format(selectedValue))) {
                string fvalue = EmployeeTable.location_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.location_id.Items.Insert(0, item);
            }

                  
            this.location_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseemployeeRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseemployeeRecordControl_Rec"] = value;
            }
        }
        
        private EmployeeRecord _DataSource;
        public EmployeeRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.TextBox address {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox dob {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dob");
            }
        }
        
        public System.Web.UI.WebControls.Literal dobLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dobLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox email {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email");
            }
        }
        
        public System.Web.UI.WebControls.Literal emailLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "emailLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image employeeDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal employeeDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList location_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal location_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox mobile {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox name {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
        
        public System.Web.UI.WebControls.Literal nameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox password {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "password");
            }
        }
        
        public System.Web.UI.WebControls.Literal passwordLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "passwordLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox phone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox UserName1 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "UserName1");
            }
        }
        
        public System.Web.UI.WebControls.Literal usernameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "usernameLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EmployeeRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EmployeeRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new EmployeeRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  