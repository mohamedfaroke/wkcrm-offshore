﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractOtherItemWithTypesRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractOtherItemWithTypesRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractOtherItemWithTypesView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractOtherItemWithTypesView"></seealso>
/// <seealso cref="View_ContractOtherItemWithTypesRecord"></seealso>
public class BaseView_ContractOtherItemWithTypesRecord : PrimaryKeyRecord
{

	public readonly static View_ContractOtherItemWithTypesView TableUtils = View_ContractOtherItemWithTypesView.Instance;

	// Constructors
 
	protected BaseView_ContractOtherItemWithTypesRecord() : base(TableUtils)
	{
	}

	protected BaseView_ContractOtherItemWithTypesRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public void Setid0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public void Setid0FieldValue(string val)
	{
		this.SetString(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public void Setid0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public void Setid0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public void Setid0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public ColumnValue Getcontract_idValue()
	{
		return this.GetValue(TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public Decimal Getcontract_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public ColumnValue Getitem_idValue()
	{
		return this.GetValue(TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public Int32 Getitem_idFieldValue()
	{
		return this.GetValue(TableUtils.item_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.item_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public ColumnValue Getunit_priceValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public Decimal Getunit_priceFieldValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(string val)
	{
		this.SetString(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public ColumnValue GetunitsValue()
	{
		return this.GetValue(TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public Decimal GetunitsFieldValue()
	{
		return this.GetValue(TableUtils.unitsColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public void SetunitsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public void SetunitsFieldValue(string val)
	{
		this.SetString(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public void SetunitsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public void SetunitsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public void SetunitsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public ColumnValue GetamountValue()
	{
		return this.GetValue(TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public Decimal GetamountFieldValue()
	{
		return this.GetValue(TableUtils.amountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public void SetamountFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public void SetamountFieldValue(string val)
	{
		this.SetString(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public void SetamountFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public void SetamountFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public void SetamountFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public ColumnValue Getcontract_item_typeValue()
	{
		return this.GetValue(TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public Int32 Getcontract_item_typeFieldValue()
	{
		return this.GetValue(TableUtils.contract_item_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public Decimal contract_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_id field.
	/// </summary>
	public string contract_idDefault
	{
		get
		{
			return TableUtils.contract_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public Int32 item_id
	{
		get
		{
			return this.GetValue(TableUtils.item_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.item_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool item_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.item_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.item_id field.
	/// </summary>
	public string item_idDefault
	{
		get
		{
			return TableUtils.item_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public Decimal unit_price
	{
		get
		{
			return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unit_priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unit_priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unit_priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.unit_price field.
	/// </summary>
	public string unit_priceDefault
	{
		get
		{
			return TableUtils.unit_priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public Decimal units
	{
		get
		{
			return this.GetValue(TableUtils.unitsColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unitsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unitsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unitsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.units field.
	/// </summary>
	public string unitsDefault
	{
		get
		{
			return TableUtils.unitsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public Decimal amount
	{
		get
		{
			return this.GetValue(TableUtils.amountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.amountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool amountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.amountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.amount field.
	/// </summary>
	public string amountDefault
	{
		get
		{
			return TableUtils.amountColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public Int32 contract_item_type
	{
		get
		{
			return this.GetValue(TableUtils.contract_item_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_item_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_item_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_item_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractOtherItemWithTypes_.contract_item_type field.
	/// </summary>
	public string contract_item_typeDefault
	{
		get
		{
			return TableUtils.contract_item_typeColumn.DefaultValue;
		}
	}


#endregion
}

}
