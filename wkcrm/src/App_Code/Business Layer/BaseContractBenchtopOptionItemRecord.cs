﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopOptionItemRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopOptionItemRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopOptionItemTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopOptionItemTable"></seealso>
/// <seealso cref="ContractBenchtopOptionItemRecord"></seealso>
public class BaseContractBenchtopOptionItemRecord : PrimaryKeyRecord
{

	public readonly static ContractBenchtopOptionItemTable TableUtils = ContractBenchtopOptionItemTable.Instance;

	// Constructors
 
	protected BaseContractBenchtopOptionItemRecord() : base(TableUtils)
	{
	}

	protected BaseContractBenchtopOptionItemRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public ColumnValue Getcontract_benchtop_itemValue()
	{
		return this.GetValue(TableUtils.contract_benchtop_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public Decimal Getcontract_benchtop_itemFieldValue()
	{
		return this.GetValue(TableUtils.contract_benchtop_itemColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public void Setcontract_benchtop_itemFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_benchtop_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public void Setcontract_benchtop_itemFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_benchtop_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public void Setcontract_benchtop_itemFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_benchtop_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public void Setcontract_benchtop_itemFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_benchtop_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public void Setcontract_benchtop_itemFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_benchtop_itemColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public ColumnValue Getcontract_benchtop_option_itemValue()
	{
		return this.GetValue(TableUtils.contract_benchtop_option_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public Decimal Getcontract_benchtop_option_itemFieldValue()
	{
		return this.GetValue(TableUtils.contract_benchtop_option_itemColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public void Setcontract_benchtop_option_itemFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_benchtop_option_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public void Setcontract_benchtop_option_itemFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_benchtop_option_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public void Setcontract_benchtop_option_itemFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_benchtop_option_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public void Setcontract_benchtop_option_itemFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_benchtop_option_itemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public void Setcontract_benchtop_option_itemFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_benchtop_option_itemColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public ColumnValue GetunitsValue()
	{
		return this.GetValue(TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public Int32 GetunitsFieldValue()
	{
		return this.GetValue(TableUtils.unitsColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(string val)
	{
		this.SetString(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public ColumnValue GetpriceValue()
	{
		return this.GetValue(TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public Decimal GetpriceFieldValue()
	{
		return this.GetValue(TableUtils.priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public void SetpriceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public void SetpriceFieldValue(string val)
	{
		this.SetString(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public void SetpriceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public void SetpriceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public void SetpriceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.amount field.
	/// </summary>
	public ColumnValue GetamountValue()
	{
		return this.GetValue(TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptionItem_.amount field.
	/// </summary>
	public Decimal GetamountFieldValue()
	{
		return this.GetValue(TableUtils.amountColumn).ToDecimal();
	}



#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptionItem_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public Decimal contract_benchtop_item
	{
		get
		{
			return this.GetValue(TableUtils.contract_benchtop_itemColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_benchtop_itemColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_benchtop_itemSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_benchtop_itemColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_item field.
	/// </summary>
	public string contract_benchtop_itemDefault
	{
		get
		{
			return TableUtils.contract_benchtop_itemColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public Decimal contract_benchtop_option_item
	{
		get
		{
			return this.GetValue(TableUtils.contract_benchtop_option_itemColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_benchtop_option_itemColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_benchtop_option_itemSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_benchtop_option_itemColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.contract_benchtop_option_item field.
	/// </summary>
	public string contract_benchtop_option_itemDefault
	{
		get
		{
			return TableUtils.contract_benchtop_option_itemColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public Int32 units
	{
		get
		{
			return this.GetValue(TableUtils.unitsColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unitsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unitsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unitsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.units field.
	/// </summary>
	public string unitsDefault
	{
		get
		{
			return TableUtils.unitsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public Decimal price
	{
		get
		{
			return this.GetValue(TableUtils.priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.price field.
	/// </summary>
	public string priceDefault
	{
		get
		{
			return TableUtils.priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptionItem_.amount field.
	/// </summary>
	public Decimal amount
	{
		get
		{
			return this.GetValue(TableUtils.amountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.amountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool amountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.amountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptionItem_.amount field.
	/// </summary>
	public string amountDefault
	{
		get
		{
			return TableUtils.amountColumn.DefaultValue;
		}
	}


#endregion
}

}
