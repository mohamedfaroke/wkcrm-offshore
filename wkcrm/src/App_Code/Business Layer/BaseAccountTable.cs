﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountTable.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="AccountTable"></see> class.
/// Provides access to the schema information and record data of a database table or view named account.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="AccountTable.Instance">AccountTable.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="AccountTable"></seealso>
[SerializableAttribute()]
public class BaseAccountTable : PrimaryKeyTable
{

    private readonly string TableDefinitionString = AccountDefinition.GetXMLString();







    protected BaseAccountTable()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.AccountTable");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.AccountRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new AccountSqlTable();
        ((AccountSqlTable)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn id0Column
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn id0
    {
        get
        {
            return AccountTable.Instance.id0Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.FirstName column object.
    /// </summary>
    public BaseClasses.Data.StringColumn FirstNameColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.FirstName column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn FirstName
    {
        get
        {
            return AccountTable.Instance.FirstNameColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.LastName column object.
    /// </summary>
    public BaseClasses.Data.StringColumn LastNameColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.LastName column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn LastName
    {
        get
        {
            return AccountTable.Instance.LastNameColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.Name column object.
    /// </summary>
    public BaseClasses.Data.StringColumn NameColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.Name column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn Name
    {
        get
        {
            return AccountTable.Instance.NameColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.home_phone column object.
    /// </summary>
    public BaseClasses.Data.StringColumn home_phoneColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.home_phone column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn home_phone
    {
        get
        {
            return AccountTable.Instance.home_phoneColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.business_phone column object.
    /// </summary>
    public BaseClasses.Data.StringColumn business_phoneColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.business_phone column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn business_phone
    {
        get
        {
            return AccountTable.Instance.business_phoneColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.mobile column object.
    /// </summary>
    public BaseClasses.Data.StringColumn mobileColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.mobile column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn mobile
    {
        get
        {
            return AccountTable.Instance.mobileColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.mobile2 column object.
    /// </summary>
    public BaseClasses.Data.StringColumn mobile2Column
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[7];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.mobile2 column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn mobile2
    {
        get
        {
            return AccountTable.Instance.mobile2Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.business_phone2 column object.
    /// </summary>
    public BaseClasses.Data.StringColumn business_phone2Column
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[8];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.business_phone2 column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn business_phone2
    {
        get
        {
            return AccountTable.Instance.business_phone2Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.builders_phone column object.
    /// </summary>
    public BaseClasses.Data.StringColumn builders_phoneColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[9];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.builders_phone column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn builders_phone
    {
        get
        {
            return AccountTable.Instance.builders_phoneColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.postal_address column object.
    /// </summary>
    public BaseClasses.Data.StringColumn postal_addressColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[10];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.postal_address column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn postal_address
    {
        get
        {
            return AccountTable.Instance.postal_addressColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.builders_name column object.
    /// </summary>
    public BaseClasses.Data.StringColumn builders_nameColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[11];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.builders_name column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn builders_name
    {
        get
        {
            return AccountTable.Instance.builders_nameColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.designer_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn designer_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[12];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.designer_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn designer_id
    {
        get
        {
            return AccountTable.Instance.designer_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.address column object.
    /// </summary>
    public BaseClasses.Data.StringColumn addressColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[13];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.address column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn address
    {
        get
        {
            return AccountTable.Instance.addressColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.suburb column object.
    /// </summary>
    public BaseClasses.Data.StringColumn suburbColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[14];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.suburb column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn suburb
    {
        get
        {
            return AccountTable.Instance.suburbColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.postcode column object.
    /// </summary>
    public BaseClasses.Data.StringColumn postcodeColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[15];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.postcode column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn postcode
    {
        get
        {
            return AccountTable.Instance.postcodeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.email column object.
    /// </summary>
    public BaseClasses.Data.EmailColumn emailColumn
    {
        get
        {
            return (BaseClasses.Data.EmailColumn)this.TableDefinition.ColumnList[16];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.email column object.
    /// </summary>
    public static BaseClasses.Data.EmailColumn email
    {
        get
        {
            return AccountTable.Instance.emailColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.email2 column object.
    /// </summary>
    public BaseClasses.Data.EmailColumn email2Column
    {
        get
        {
            return (BaseClasses.Data.EmailColumn)this.TableDefinition.ColumnList[17];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.email2 column object.
    /// </summary>
    public static BaseClasses.Data.EmailColumn email2
    {
        get
        {
            return AccountTable.Instance.email2Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.fax column object.
    /// </summary>
    public BaseClasses.Data.StringColumn faxColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[18];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.fax column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn fax
    {
        get
        {
            return AccountTable.Instance.faxColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.account_type_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn account_type_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[19];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.account_type_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn account_type_id
    {
        get
        {
            return AccountTable.Instance.account_type_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.account_status_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn account_status_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[20];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.account_status_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn account_status_id
    {
        get
        {
            return AccountTable.Instance.account_status_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.has_been_revived column object.
    /// </summary>
    public BaseClasses.Data.BooleanColumn has_been_revivedColumn
    {
        get
        {
            return (BaseClasses.Data.BooleanColumn)this.TableDefinition.ColumnList[21];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.has_been_revived column object.
    /// </summary>
    public static BaseClasses.Data.BooleanColumn has_been_revived
    {
        get
        {
            return AccountTable.Instance.has_been_revivedColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.next_review_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn next_review_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[22];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.next_review_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn next_review_date
    {
        get
        {
            return AccountTable.Instance.next_review_dateColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.contact_source_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn contact_source_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[23];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.contact_source_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn contact_source_id
    {
        get
        {
            return AccountTable.Instance.contact_source_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.opportunity_source_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn opportunity_source_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[24];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.opportunity_source_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn opportunity_source_id
    {
        get
        {
            return AccountTable.Instance.opportunity_source_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.opportunity_details column object.
    /// </summary>
    public BaseClasses.Data.StringColumn opportunity_detailsColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[25];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.opportunity_details column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn opportunity_details
    {
        get
        {
            return AccountTable.Instance.opportunity_detailsColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.revived_reason column object.
    /// </summary>
    public BaseClasses.Data.StringColumn revived_reasonColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[26];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.revived_reason column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn revived_reason
    {
        get
        {
            return AccountTable.Instance.revived_reasonColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.left_reason column object.
    /// </summary>
    public BaseClasses.Data.StringColumn left_reasonColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[27];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.left_reason column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn left_reason
    {
        get
        {
            return AccountTable.Instance.left_reasonColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.date_created column object.
    /// </summary>
    public BaseClasses.Data.DateColumn date_createdColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[28];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.date_created column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn date_created
    {
        get
        {
            return AccountTable.Instance.date_createdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.created_by_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn created_by_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[29];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.created_by_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn created_by_id
    {
        get
        {
            return AccountTable.Instance.created_by_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.sale_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn sale_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[30];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.sale_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn sale_date
    {
        get
        {
            return AccountTable.Instance.sale_dateColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.weeks_open column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn weeks_openColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[31];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.weeks_open column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn weeks_open
    {
        get
        {
            return AccountTable.Instance.weeks_openColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.door_type column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn door_typeColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[32];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.door_type column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn door_type
    {
        get
        {
            return AccountTable.Instance.door_typeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.benchtop_type column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn benchtop_typeColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[33];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.benchtop_type column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn benchtop_type
    {
        get
        {
            return AccountTable.Instance.benchtop_typeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.price_range column object.
    /// </summary>
    public BaseClasses.Data.StringColumn price_rangeColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[34];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.price_range column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn price_range
    {
        get
        {
            return AccountTable.Instance.price_rangeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.time_frame column object.
    /// </summary>
    public BaseClasses.Data.StringColumn time_frameColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[35];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.time_frame column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn time_frame
    {
        get
        {
            return AccountTable.Instance.time_frameColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.date_died column object.
    /// </summary>
    public BaseClasses.Data.DateColumn date_diedColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[36];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.date_died column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn date_died
    {
        get
        {
            return AccountTable.Instance.date_diedColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.home_warranty_policy column object.
    /// </summary>
    public BaseClasses.Data.StringColumn home_warranty_policyColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[37];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.home_warranty_policy column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn home_warranty_policy
    {
        get
        {
            return AccountTable.Instance.home_warranty_policyColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.home_warranty_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn home_warranty_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[38];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Account_.home_warranty_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn home_warranty_date
    {
        get
        {
            return AccountTable.Instance.home_warranty_dateColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of AccountRecord records using a where clause.
    /// </summary>
    public static AccountRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of AccountRecord records using a where and order by clause.
    /// </summary>
    public static AccountRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of AccountRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static AccountRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = AccountTable.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (AccountRecord[])recList.ToArray(Type.GetType("WKCRM.Business.AccountRecord"));
    }   
    
    public static AccountRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = AccountTable.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (AccountRecord[])recList.ToArray(Type.GetType("WKCRM.Business.AccountRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)AccountTable.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)AccountTable.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a AccountRecord record using a where clause.
    /// </summary>
    public static AccountRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a AccountRecord record using a where and order by clause.
    /// </summary>
    public static AccountRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = AccountTable.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        AccountRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (AccountRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return AccountTable.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        AccountRecord[] recs = GetRecords(where);
        return  AccountTable.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        AccountRecord[] recs = GetRecords(where, orderBy);
        return  AccountTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        AccountRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  AccountTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        AccountTable.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  AccountTable.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return AccountTable.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return AccountTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return AccountTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return AccountTable.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return AccountTable.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return AccountTable.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return AccountTable.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = AccountTable.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static AccountRecord GetRecord(string id, bool bMutable)
        {
            return (AccountRecord)AccountTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static AccountRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (AccountRecord)AccountTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string FirstNameValue, 
        string LastNameValue, 
        string home_phoneValue, 
        string business_phoneValue, 
        string mobileValue, 
        string mobile2Value, 
        string business_phone2Value, 
        string builders_phoneValue, 
        string postal_addressValue, 
        string builders_nameValue, 
        string designer_idValue, 
        string addressValue, 
        string suburbValue, 
        string postcodeValue, 
        string emailValue, 
        string email2Value, 
        string faxValue, 
        string account_type_idValue, 
        string account_status_idValue, 
        string has_been_revivedValue, 
        string next_review_dateValue, 
        string contact_source_idValue, 
        string opportunity_source_idValue, 
        string opportunity_detailsValue, 
        string revived_reasonValue, 
        string left_reasonValue, 
        string date_createdValue, 
        string created_by_idValue, 
        string sale_dateValue, 
        string door_typeValue, 
        string benchtop_typeValue, 
        string price_rangeValue, 
        string time_frameValue, 
        string date_diedValue, 
        string home_warranty_policyValue, 
        string home_warranty_dateValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(FirstNameValue, FirstNameColumn);
        rec.SetString(LastNameValue, LastNameColumn);
        rec.SetString(home_phoneValue, home_phoneColumn);
        rec.SetString(business_phoneValue, business_phoneColumn);
        rec.SetString(mobileValue, mobileColumn);
        rec.SetString(mobile2Value, mobile2Column);
        rec.SetString(business_phone2Value, business_phone2Column);
        rec.SetString(builders_phoneValue, builders_phoneColumn);
        rec.SetString(postal_addressValue, postal_addressColumn);
        rec.SetString(builders_nameValue, builders_nameColumn);
        rec.SetString(designer_idValue, designer_idColumn);
        rec.SetString(addressValue, addressColumn);
        rec.SetString(suburbValue, suburbColumn);
        rec.SetString(postcodeValue, postcodeColumn);
        rec.SetString(emailValue, emailColumn);
        rec.SetString(email2Value, email2Column);
        rec.SetString(faxValue, faxColumn);
        rec.SetString(account_type_idValue, account_type_idColumn);
        rec.SetString(account_status_idValue, account_status_idColumn);
        rec.SetString(has_been_revivedValue, has_been_revivedColumn);
        rec.SetString(next_review_dateValue, next_review_dateColumn);
        rec.SetString(contact_source_idValue, contact_source_idColumn);
        rec.SetString(opportunity_source_idValue, opportunity_source_idColumn);
        rec.SetString(opportunity_detailsValue, opportunity_detailsColumn);
        rec.SetString(revived_reasonValue, revived_reasonColumn);
        rec.SetString(left_reasonValue, left_reasonColumn);
        rec.SetString(date_createdValue, date_createdColumn);
        rec.SetString(created_by_idValue, created_by_idColumn);
        rec.SetString(sale_dateValue, sale_dateColumn);
        rec.SetString(door_typeValue, door_typeColumn);
        rec.SetString(benchtop_typeValue, benchtop_typeColumn);
        rec.SetString(price_rangeValue, price_rangeColumn);
        rec.SetString(time_frameValue, time_frameColumn);
        rec.SetString(date_diedValue, date_diedColumn);
        rec.SetString(home_warranty_policyValue, home_warranty_policyColumn);
        rec.SetString(home_warranty_dateValue, home_warranty_dateColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			AccountTable.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				AccountTable.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(AccountTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return AccountTable.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(AccountTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = AccountTable.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = AccountTable.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (AccountTable.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = AccountTable.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
