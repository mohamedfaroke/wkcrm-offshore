﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorItemRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorItemRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorItemTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorItemTable"></seealso>
/// <seealso cref="ContractDoorItemRecord"></seealso>
public class BaseContractDoorItemRecord : PrimaryKeyRecord
{

	public readonly static ContractDoorItemTable TableUtils = ContractDoorItemTable.Instance;

	// Constructors
 
	protected BaseContractDoorItemRecord() : base(TableUtils)
	{
	}

	protected BaseContractDoorItemRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public ColumnValue Getcontract_idValue()
	{
		return this.GetValue(TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public Decimal Getcontract_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public ColumnValue Getitem_idValue()
	{
		return this.GetValue(TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public Int32 Getitem_idFieldValue()
	{
		return this.GetValue(TableUtils.item_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.item_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public void Setitem_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.item_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public ColumnValue Getunit_priceValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public Decimal Getunit_priceFieldValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(string val)
	{
		this.SetString(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public ColumnValue Getadditional_costValue()
	{
		return this.GetValue(TableUtils.additional_costColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public Decimal Getadditional_costFieldValue()
	{
		return this.GetValue(TableUtils.additional_costColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public void Setadditional_costFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.additional_costColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public void Setadditional_costFieldValue(string val)
	{
		this.SetString(val, TableUtils.additional_costColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public void Setadditional_costFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.additional_costColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public void Setadditional_costFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.additional_costColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public void Setadditional_costFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.additional_costColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public ColumnValue GetunitsValue()
	{
		return this.GetValue(TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public Decimal GetunitsFieldValue()
	{
		return this.GetValue(TableUtils.unitsColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(string val)
	{
		this.SetString(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public void SetunitsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.amount field.
	/// </summary>
	public ColumnValue GetamountValue()
	{
		return this.GetValue(TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.amount field.
	/// </summary>
	public Decimal GetamountFieldValue()
	{
		return this.GetValue(TableUtils.amountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.style field.
	/// </summary>
	public ColumnValue Getstyle0Value()
	{
		return this.GetValue(TableUtils.style0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.style field.
	/// </summary>
	public string Getstyle0FieldValue()
	{
		return this.GetValue(TableUtils.style0Column).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.style field.
	/// </summary>
	public void Setstyle0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.style0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.style field.
	/// </summary>
	public void Setstyle0FieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.style0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.edge_profile field.
	/// </summary>
	public ColumnValue Getedge_profileValue()
	{
		return this.GetValue(TableUtils.edge_profileColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.edge_profile field.
	/// </summary>
	public string Getedge_profileFieldValue()
	{
		return this.GetValue(TableUtils.edge_profileColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.edge_profile field.
	/// </summary>
	public void Setedge_profileFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.edge_profileColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.edge_profile field.
	/// </summary>
	public void Setedge_profileFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.edge_profileColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.face field.
	/// </summary>
	public ColumnValue GetfaceValue()
	{
		return this.GetValue(TableUtils.faceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.face field.
	/// </summary>
	public string GetfaceFieldValue()
	{
		return this.GetValue(TableUtils.faceColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.face field.
	/// </summary>
	public void SetfaceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.faceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.face field.
	/// </summary>
	public void SetfaceFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.faceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.finish field.
	/// </summary>
	public ColumnValue GetfinishValue()
	{
		return this.GetValue(TableUtils.finishColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.finish field.
	/// </summary>
	public string GetfinishFieldValue()
	{
		return this.GetValue(TableUtils.finishColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.finish field.
	/// </summary>
	public void SetfinishFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.finishColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.finish field.
	/// </summary>
	public void SetfinishFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.finishColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.colour field.
	/// </summary>
	public ColumnValue GetcolourValue()
	{
		return this.GetValue(TableUtils.colourColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.colour field.
	/// </summary>
	public string GetcolourFieldValue()
	{
		return this.GetValue(TableUtils.colourColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.colour field.
	/// </summary>
	public void SetcolourFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.colourColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.colour field.
	/// </summary>
	public void SetcolourFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.colourColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.veneer field.
	/// </summary>
	public ColumnValue GetveneerValue()
	{
		return this.GetValue(TableUtils.veneerColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.veneer field.
	/// </summary>
	public string GetveneerFieldValue()
	{
		return this.GetValue(TableUtils.veneerColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.veneer field.
	/// </summary>
	public void SetveneerFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.veneerColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.veneer field.
	/// </summary>
	public void SetveneerFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.veneerColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.supplier field.
	/// </summary>
	public ColumnValue GetsupplierValue()
	{
		return this.GetValue(TableUtils.supplierColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.supplier field.
	/// </summary>
	public string GetsupplierFieldValue()
	{
		return this.GetValue(TableUtils.supplierColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.supplier field.
	/// </summary>
	public void SetsupplierFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.supplierColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.supplier field.
	/// </summary>
	public void SetsupplierFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.supplierColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoorItem_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public Decimal contract_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.contract_id field.
	/// </summary>
	public string contract_idDefault
	{
		get
		{
			return TableUtils.contract_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public Int32 item_id
	{
		get
		{
			return this.GetValue(TableUtils.item_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.item_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool item_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.item_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.item_id field.
	/// </summary>
	public string item_idDefault
	{
		get
		{
			return TableUtils.item_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public Decimal unit_price
	{
		get
		{
			return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unit_priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unit_priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unit_priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.unit_price field.
	/// </summary>
	public string unit_priceDefault
	{
		get
		{
			return TableUtils.unit_priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public Decimal additional_cost
	{
		get
		{
			return this.GetValue(TableUtils.additional_costColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.additional_costColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool additional_costSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.additional_costColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.additional_cost field.
	/// </summary>
	public string additional_costDefault
	{
		get
		{
			return TableUtils.additional_costColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public Decimal units
	{
		get
		{
			return this.GetValue(TableUtils.unitsColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unitsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unitsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unitsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.units field.
	/// </summary>
	public string unitsDefault
	{
		get
		{
			return TableUtils.unitsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.amount field.
	/// </summary>
	public Decimal amount
	{
		get
		{
			return this.GetValue(TableUtils.amountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.amountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool amountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.amountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.amount field.
	/// </summary>
	public string amountDefault
	{
		get
		{
			return TableUtils.amountColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.style field.
	/// </summary>
	public string style0
	{
		get
		{
			return this.GetValue(TableUtils.style0Column).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.style0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool style0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.style0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.style field.
	/// </summary>
	public string style0Default
	{
		get
		{
			return TableUtils.style0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.edge_profile field.
	/// </summary>
	public string edge_profile
	{
		get
		{
			return this.GetValue(TableUtils.edge_profileColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.edge_profileColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool edge_profileSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.edge_profileColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.edge_profile field.
	/// </summary>
	public string edge_profileDefault
	{
		get
		{
			return TableUtils.edge_profileColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.face field.
	/// </summary>
	public string face
	{
		get
		{
			return this.GetValue(TableUtils.faceColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.faceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool faceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.faceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.face field.
	/// </summary>
	public string faceDefault
	{
		get
		{
			return TableUtils.faceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.finish field.
	/// </summary>
	public string finish
	{
		get
		{
			return this.GetValue(TableUtils.finishColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.finishColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool finishSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.finishColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.finish field.
	/// </summary>
	public string finishDefault
	{
		get
		{
			return TableUtils.finishColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.colour field.
	/// </summary>
	public string colour
	{
		get
		{
			return this.GetValue(TableUtils.colourColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.colourColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool colourSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.colourColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.colour field.
	/// </summary>
	public string colourDefault
	{
		get
		{
			return TableUtils.colourColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.veneer field.
	/// </summary>
	public string veneer
	{
		get
		{
			return this.GetValue(TableUtils.veneerColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.veneerColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool veneerSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.veneerColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.veneer field.
	/// </summary>
	public string veneerDefault
	{
		get
		{
			return TableUtils.veneerColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.supplier field.
	/// </summary>
	public string supplier
	{
		get
		{
			return this.GetValue(TableUtils.supplierColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.supplierColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool supplierSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.supplierColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.supplier field.
	/// </summary>
	public string supplierDefault
	{
		get
		{
			return TableUtils.supplierColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoorItem_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoorItem_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}


#endregion
}

}
