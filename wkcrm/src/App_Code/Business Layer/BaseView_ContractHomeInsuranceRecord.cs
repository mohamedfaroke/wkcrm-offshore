﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractHomeInsuranceRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractHomeInsuranceRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractHomeInsuranceView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractHomeInsuranceView"></seealso>
/// <seealso cref="View_ContractHomeInsuranceRecord"></seealso>
public class BaseView_ContractHomeInsuranceRecord : PrimaryKeyRecord
{

	public readonly static View_ContractHomeInsuranceView TableUtils = View_ContractHomeInsuranceView.Instance;

	// Constructors
 
	protected BaseView_ContractHomeInsuranceRecord() : base(TableUtils)
	{
	}

	protected BaseView_ContractHomeInsuranceRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public void Setid0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public void Setid0FieldValue(string val)
	{
		this.SetString(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public void Setid0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public void Setid0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public void Setid0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.signed_date field.
	/// </summary>
	public ColumnValue Getsigned_dateValue()
	{
		return this.GetValue(TableUtils.signed_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.signed_date field.
	/// </summary>
	public DateTime Getsigned_dateFieldValue()
	{
		return this.GetValue(TableUtils.signed_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.signed_date field.
	/// </summary>
	public void Setsigned_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.signed_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.signed_date field.
	/// </summary>
	public void Setsigned_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.signed_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.signed_date field.
	/// </summary>
	public void Setsigned_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.signed_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.estimated_completion_date field.
	/// </summary>
	public ColumnValue Getestimated_completion_dateValue()
	{
		return this.GetValue(TableUtils.estimated_completion_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.estimated_completion_date field.
	/// </summary>
	public DateTime Getestimated_completion_dateFieldValue()
	{
		return this.GetValue(TableUtils.estimated_completion_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.estimated_completion_date field.
	/// </summary>
	public void Setestimated_completion_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.estimated_completion_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.estimated_completion_date field.
	/// </summary>
	public void Setestimated_completion_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.estimated_completion_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.estimated_completion_date field.
	/// </summary>
	public void Setestimated_completion_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.estimated_completion_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.home_insurance_sent field.
	/// </summary>
	public ColumnValue Gethome_insurance_sentValue()
	{
		return this.GetValue(TableUtils.home_insurance_sentColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.home_insurance_sent field.
	/// </summary>
	public bool Gethome_insurance_sentFieldValue()
	{
		return this.GetValue(TableUtils.home_insurance_sentColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.home_insurance_sent field.
	/// </summary>
	public void Sethome_insurance_sentFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.home_insurance_sentColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.home_insurance_sent field.
	/// </summary>
	public void Sethome_insurance_sentFieldValue(string val)
	{
		this.SetString(val, TableUtils.home_insurance_sentColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.home_insurance_sent field.
	/// </summary>
	public void Sethome_insurance_sentFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_insurance_sentColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.home_insurance_policy field.
	/// </summary>
	public ColumnValue Gethome_insurance_policyValue()
	{
		return this.GetValue(TableUtils.home_insurance_policyColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.home_insurance_policy field.
	/// </summary>
	public string Gethome_insurance_policyFieldValue()
	{
		return this.GetValue(TableUtils.home_insurance_policyColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.home_insurance_policy field.
	/// </summary>
	public void Sethome_insurance_policyFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.home_insurance_policyColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.home_insurance_policy field.
	/// </summary>
	public void Sethome_insurance_policyFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_insurance_policyColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public ColumnValue GetTotalAmountValue()
	{
		return this.GetValue(TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public Decimal GetTotalAmountFieldValue()
	{
		return this.GetValue(TableUtils.TotalAmountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(string val)
	{
		this.SetString(val, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractHomeInsurance_.signed_date field.
	/// </summary>
	public DateTime signed_date
	{
		get
		{
			return this.GetValue(TableUtils.signed_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.signed_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool signed_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.signed_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.signed_date field.
	/// </summary>
	public string signed_dateDefault
	{
		get
		{
			return TableUtils.signed_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractHomeInsurance_.estimated_completion_date field.
	/// </summary>
	public DateTime estimated_completion_date
	{
		get
		{
			return this.GetValue(TableUtils.estimated_completion_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.estimated_completion_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool estimated_completion_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.estimated_completion_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.estimated_completion_date field.
	/// </summary>
	public string estimated_completion_dateDefault
	{
		get
		{
			return TableUtils.estimated_completion_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractHomeInsurance_.home_insurance_sent field.
	/// </summary>
	public bool home_insurance_sent
	{
		get
		{
			return this.GetValue(TableUtils.home_insurance_sentColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.home_insurance_sentColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool home_insurance_sentSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.home_insurance_sentColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.home_insurance_sent field.
	/// </summary>
	public string home_insurance_sentDefault
	{
		get
		{
			return TableUtils.home_insurance_sentColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractHomeInsurance_.home_insurance_policy field.
	/// </summary>
	public string home_insurance_policy
	{
		get
		{
			return this.GetValue(TableUtils.home_insurance_policyColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.home_insurance_policyColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool home_insurance_policySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.home_insurance_policyColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.home_insurance_policy field.
	/// </summary>
	public string home_insurance_policyDefault
	{
		get
		{
			return TableUtils.home_insurance_policyColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public Decimal TotalAmount
	{
		get
		{
			return this.GetValue(TableUtils.TotalAmountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TotalAmountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TotalAmountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TotalAmountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractHomeInsurance_.TotalAmount field.
	/// </summary>
	public string TotalAmountDefault
	{
		get
		{
			return TableUtils.TotalAmountColumn.DefaultValue;
		}
	}


#endregion
}

}
