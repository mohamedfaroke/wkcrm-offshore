﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_PaymentScheduleOverviewRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_PaymentScheduleOverviewRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_PaymentScheduleOverviewView"></see> class.
/// </remarks>
/// <seealso cref="View_PaymentScheduleOverviewView"></seealso>
/// <seealso cref="View_PaymentScheduleOverviewRecord"></seealso>
public class BaseView_PaymentScheduleOverviewRecord : PrimaryKeyRecord
{

	public readonly static View_PaymentScheduleOverviewView TableUtils = View_PaymentScheduleOverviewView.Instance;

	// Constructors
 
	protected BaseView_PaymentScheduleOverviewRecord() : base(TableUtils)
	{
	}

	protected BaseView_PaymentScheduleOverviewRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public ColumnValue Getpayment_schedule_idValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public Decimal Getpayment_schedule_idFieldValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public ColumnValue Getpayment_stage_idValue()
	{
		return this.GetValue(TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public Int32 Getpayment_stage_idFieldValue()
	{
		return this.GetValue(TableUtils.payment_stage_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public ColumnValue GetTotalAmountValue()
	{
		return this.GetValue(TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public Decimal GetTotalAmountFieldValue()
	{
		return this.GetValue(TableUtils.TotalAmountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(string val)
	{
		this.SetString(val, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public ColumnValue GetTotalReceivedValue()
	{
		return this.GetValue(TableUtils.TotalReceivedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public Decimal GetTotalReceivedFieldValue()
	{
		return this.GetValue(TableUtils.TotalReceivedColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public void SetTotalReceivedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TotalReceivedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public void SetTotalReceivedFieldValue(string val)
	{
		this.SetString(val, TableUtils.TotalReceivedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public void SetTotalReceivedFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalReceivedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public void SetTotalReceivedFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalReceivedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public void SetTotalReceivedFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalReceivedColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public ColumnValue GetOutstandingValue()
	{
		return this.GetValue(TableUtils.OutstandingColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public Decimal GetOutstandingFieldValue()
	{
		return this.GetValue(TableUtils.OutstandingColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public void SetOutstandingFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OutstandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public void SetOutstandingFieldValue(string val)
	{
		this.SetString(val, TableUtils.OutstandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public void SetOutstandingFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OutstandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public void SetOutstandingFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OutstandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public void SetOutstandingFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OutstandingColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public ColumnValue GetdisplayOrderValue()
	{
		return this.GetValue(TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public Int32 GetdisplayOrderFieldValue()
	{
		return this.GetValue(TableUtils.displayOrderColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(string val)
	{
		this.SetString(val, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public Decimal payment_schedule_id
	{
		get
		{
			return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.payment_schedule_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool payment_schedule_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.payment_schedule_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_schedule_id field.
	/// </summary>
	public string payment_schedule_idDefault
	{
		get
		{
			return TableUtils.payment_schedule_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public Int32 payment_stage_id
	{
		get
		{
			return this.GetValue(TableUtils.payment_stage_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.payment_stage_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool payment_stage_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.payment_stage_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.payment_stage_id field.
	/// </summary>
	public string payment_stage_idDefault
	{
		get
		{
			return TableUtils.payment_stage_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public Decimal TotalAmount
	{
		get
		{
			return this.GetValue(TableUtils.TotalAmountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TotalAmountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TotalAmountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TotalAmountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalAmount field.
	/// </summary>
	public string TotalAmountDefault
	{
		get
		{
			return TableUtils.TotalAmountColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public Decimal TotalReceived
	{
		get
		{
			return this.GetValue(TableUtils.TotalReceivedColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TotalReceivedColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TotalReceivedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TotalReceivedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.TotalReceived field.
	/// </summary>
	public string TotalReceivedDefault
	{
		get
		{
			return TableUtils.TotalReceivedColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public Decimal Outstanding
	{
		get
		{
			return this.GetValue(TableUtils.OutstandingColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OutstandingColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OutstandingSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OutstandingColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.Outstanding field.
	/// </summary>
	public string OutstandingDefault
	{
		get
		{
			return TableUtils.OutstandingColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public Int32 displayOrder
	{
		get
		{
			return this.GetValue(TableUtils.displayOrderColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.displayOrderColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool displayOrderSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.displayOrderColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleOverview_.displayOrder field.
	/// </summary>
	public string displayOrderDefault
	{
		get
		{
			return TableUtils.displayOrderColumn.DefaultValue;
		}
	}


#endregion
}

}
