﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ProjectRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ProjectRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ProjectTable"></see> class.
/// </remarks>
/// <seealso cref="ProjectTable"></seealso>
/// <seealso cref="ProjectRecord"></seealso>
public class BaseProjectRecord : PrimaryKeyRecord
{

	public readonly static ProjectTable TableUtils = ProjectTable.Instance;

	// Constructors
 
	protected BaseProjectRecord() : base(TableUtils)
	{
	}

	protected BaseProjectRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.contract_id field.
	/// </summary>
	public ColumnValue Getcontract_idValue()
	{
		return this.GetValue(TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.contract_id field.
	/// </summary>
	public Decimal Getcontract_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.date_created field.
	/// </summary>
	public ColumnValue Getdate_createdValue()
	{
		return this.GetValue(TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.date_created field.
	/// </summary>
	public DateTime Getdate_createdFieldValue()
	{
		return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_createdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.created_by field.
	/// </summary>
	public ColumnValue Getcreated_byValue()
	{
		return this.GetValue(TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.created_by field.
	/// </summary>
	public Decimal Getcreated_byFieldValue()
	{
		return this.GetValue(TableUtils.created_byColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(string val)
	{
		this.SetString(val, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.created_byColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.date_modified field.
	/// </summary>
	public ColumnValue Getdate_modifiedValue()
	{
		return this.GetValue(TableUtils.date_modifiedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.date_modified field.
	/// </summary>
	public DateTime Getdate_modifiedFieldValue()
	{
		return this.GetValue(TableUtils.date_modifiedColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_modified field.
	/// </summary>
	public void Setdate_modifiedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_modifiedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_modified field.
	/// </summary>
	public void Setdate_modifiedFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_modifiedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_modified field.
	/// </summary>
	public void Setdate_modifiedFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_modifiedColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.modified_by field.
	/// </summary>
	public ColumnValue Getmodified_byValue()
	{
		return this.GetValue(TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.modified_by field.
	/// </summary>
	public Decimal Getmodified_byFieldValue()
	{
		return this.GetValue(TableUtils.modified_byColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(string val)
	{
		this.SetString(val, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.modified_byColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.project_manager field.
	/// </summary>
	public ColumnValue Getproject_managerValue()
	{
		return this.GetValue(TableUtils.project_managerColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Project_.project_manager field.
	/// </summary>
	public Decimal Getproject_managerFieldValue()
	{
		return this.GetValue(TableUtils.project_managerColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.project_manager field.
	/// </summary>
	public void Setproject_managerFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.project_managerColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.project_manager field.
	/// </summary>
	public void Setproject_managerFieldValue(string val)
	{
		this.SetString(val, TableUtils.project_managerColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.project_manager field.
	/// </summary>
	public void Setproject_managerFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_managerColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.project_manager field.
	/// </summary>
	public void Setproject_managerFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_managerColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.project_manager field.
	/// </summary>
	public void Setproject_managerFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_managerColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Project_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Project_.contract_id field.
	/// </summary>
	public Decimal contract_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.contract_id field.
	/// </summary>
	public string contract_idDefault
	{
		get
		{
			return TableUtils.contract_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Project_.date_created field.
	/// </summary>
	public DateTime date_created
	{
		get
		{
			return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_createdColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_createdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_createdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_created field.
	/// </summary>
	public string date_createdDefault
	{
		get
		{
			return TableUtils.date_createdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Project_.created_by field.
	/// </summary>
	public Decimal created_by
	{
		get
		{
			return this.GetValue(TableUtils.created_byColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.created_byColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool created_bySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.created_byColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.created_by field.
	/// </summary>
	public string created_byDefault
	{
		get
		{
			return TableUtils.created_byColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Project_.date_modified field.
	/// </summary>
	public DateTime date_modified
	{
		get
		{
			return this.GetValue(TableUtils.date_modifiedColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_modifiedColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_modifiedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_modifiedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.date_modified field.
	/// </summary>
	public string date_modifiedDefault
	{
		get
		{
			return TableUtils.date_modifiedColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Project_.modified_by field.
	/// </summary>
	public Decimal modified_by
	{
		get
		{
			return this.GetValue(TableUtils.modified_byColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.modified_byColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool modified_bySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.modified_byColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.modified_by field.
	/// </summary>
	public string modified_byDefault
	{
		get
		{
			return TableUtils.modified_byColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Project_.project_manager field.
	/// </summary>
	public Decimal project_manager
	{
		get
		{
			return this.GetValue(TableUtils.project_managerColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.project_managerColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool project_managerSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.project_managerColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Project_.project_manager field.
	/// </summary>
	public string project_managerDefault
	{
		get
		{
			return TableUtils.project_managerColumn.DefaultValue;
		}
	}


#endregion
}

}
