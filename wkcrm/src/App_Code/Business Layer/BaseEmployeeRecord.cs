﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EmployeeRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="EmployeeRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EmployeeTable"></see> class.
/// </remarks>
/// <seealso cref="EmployeeTable"></seealso>
/// <seealso cref="EmployeeRecord"></seealso>
public class BaseEmployeeRecord : PrimaryKeyRecord, IUserIdentityRecord
{

	public readonly static EmployeeTable TableUtils = EmployeeTable.Instance;

	// Constructors
 
	protected BaseEmployeeRecord() : base(TableUtils)
	{
	}

	protected BaseEmployeeRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}

#region "IUserRecord Members"

	//Get the user's unique identifier
	public string GetUserId()
	{
		return this.GetString(((BaseClasses.IUserTable)this.TableAccess).UserIdColumn);
	}

#endregion


#region "IUserIdentityRecord Members"

	//Get the user's name
	public string GetUserName()
	{
		return this.GetString(((BaseClasses.IUserIdentityTable)this.TableAccess).UserNameColumn);
	}

	//Get the user's password
	public string GetUserPassword()
	{
		return this.GetString(((BaseClasses.IUserIdentityTable)this.TableAccess).UserPasswordColumn);
	}

	//Get the user's email address
	public string GetUserEmail()
	{
		return this.GetString(((BaseClasses.IUserIdentityTable)this.TableAccess).UserEmailColumn);
	}

	//Get a list of roles to which the user belongs
	public string[] GetUserRoles()
	{
		string[] roles;
		if ((this as BaseClasses.IUserRoleRecord) != null)
		{
			string role = ((BaseClasses.IUserRoleRecord)this).GetUserRole();
			roles = new string[]{role};
		}
		else
		{
			BaseClasses.IUserRoleTable roleTable = 
				((BaseClasses.IUserIdentityTable)this.TableAccess).GetUserRoleTable();
			if (roleTable == null)
			{
				return null;
			}
			else
			{
				ColumnValueFilter filter = BaseFilter.CreateUserIdFilter(roleTable, this.GetUserId());
				BaseClasses.Data.OrderBy order = new BaseClasses.Data.OrderBy(false, false);
				ArrayList roleRecords = roleTable.GetRecordList(
					filter, 
					order, 
					BaseClasses.Data.BaseTable.MIN_PAGE_NUMBER, 
					BaseClasses.Data.BaseTable.MAX_BATCH_SIZE);
				ArrayList roleList = new ArrayList(roleRecords.Count);
				foreach (BaseClasses.IUserRoleRecord roleRecord in roleRecords)
				{
					roleList.Add(roleRecord.GetUserRole());
				}
				roles = (string[])roleList.ToArray(typeof(string));
			}
		}
		return roles;
	}

#endregion




#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.location_id field.
	/// </summary>
	public ColumnValue Getlocation_idValue()
	{
		return this.GetValue(TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.location_id field.
	/// </summary>
	public Int32 Getlocation_idFieldValue()
	{
		return this.GetValue(TableUtils.location_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.dob field.
	/// </summary>
	public ColumnValue GetdobValue()
	{
		return this.GetValue(TableUtils.dobColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.dob field.
	/// </summary>
	public DateTime GetdobFieldValue()
	{
		return this.GetValue(TableUtils.dobColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.dob field.
	/// </summary>
	public void SetdobFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.dobColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.dob field.
	/// </summary>
	public void SetdobFieldValue(string val)
	{
		this.SetString(val, TableUtils.dobColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.dob field.
	/// </summary>
	public void SetdobFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dobColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.phone field.
	/// </summary>
	public ColumnValue GetphoneValue()
	{
		return this.GetValue(TableUtils.phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.phone field.
	/// </summary>
	public string GetphoneFieldValue()
	{
		return this.GetValue(TableUtils.phoneColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.phone field.
	/// </summary>
	public void SetphoneFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.phone field.
	/// </summary>
	public void SetphoneFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.phoneColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.mobile field.
	/// </summary>
	public ColumnValue GetmobileValue()
	{
		return this.GetValue(TableUtils.mobileColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.mobile field.
	/// </summary>
	public string GetmobileFieldValue()
	{
		return this.GetValue(TableUtils.mobileColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.mobile field.
	/// </summary>
	public void SetmobileFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.mobileColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.mobile field.
	/// </summary>
	public void SetmobileFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.mobileColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.address field.
	/// </summary>
	public ColumnValue GetaddressValue()
	{
		return this.GetValue(TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.address field.
	/// </summary>
	public string GetaddressFieldValue()
	{
		return this.GetValue(TableUtils.addressColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.address field.
	/// </summary>
	public void SetaddressFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.address field.
	/// </summary>
	public void SetaddressFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.addressColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.email field.
	/// </summary>
	public ColumnValue GetemailValue()
	{
		return this.GetValue(TableUtils.emailColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.email field.
	/// </summary>
	public string GetemailFieldValue()
	{
		return this.GetValue(TableUtils.emailColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.email field.
	/// </summary>
	public void SetemailFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.emailColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.email field.
	/// </summary>
	public void SetemailFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.emailColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.username field.
	/// </summary>
	public ColumnValue GetUserName0Value()
	{
		return this.GetValue(TableUtils.UserName0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.username field.
	/// </summary>
	public string GetUserName0FieldValue()
	{
		return this.GetValue(TableUtils.UserName0Column).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.username field.
	/// </summary>
	public void SetUserName0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.UserName0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.username field.
	/// </summary>
	public void SetUserName0FieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.UserName0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.password field.
	/// </summary>
	public ColumnValue GetpasswordValue()
	{
		return this.GetValue(TableUtils.passwordColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.password field.
	/// </summary>
	public string GetpasswordFieldValue()
	{
		return this.GetValue(TableUtils.passwordColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.password field.
	/// </summary>
	public void SetpasswordFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.passwordColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.password field.
	/// </summary>
	public void SetpasswordFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.passwordColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Employee_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.location_id field.
	/// </summary>
	public Int32 location_id
	{
		get
		{
			return this.GetValue(TableUtils.location_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.location_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool location_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.location_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.location_id field.
	/// </summary>
	public string location_idDefault
	{
		get
		{
			return TableUtils.location_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.dob field.
	/// </summary>
	public DateTime dob
	{
		get
		{
			return this.GetValue(TableUtils.dobColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.dobColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool dobSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.dobColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.dob field.
	/// </summary>
	public string dobDefault
	{
		get
		{
			return TableUtils.dobColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.phone field.
	/// </summary>
	public string phone
	{
		get
		{
			return this.GetValue(TableUtils.phoneColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.phoneColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool phoneSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.phoneColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.phone field.
	/// </summary>
	public string phoneDefault
	{
		get
		{
			return TableUtils.phoneColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.mobile field.
	/// </summary>
	public string mobile
	{
		get
		{
			return this.GetValue(TableUtils.mobileColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.mobileColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool mobileSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.mobileColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.mobile field.
	/// </summary>
	public string mobileDefault
	{
		get
		{
			return TableUtils.mobileColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.address field.
	/// </summary>
	public string address
	{
		get
		{
			return this.GetValue(TableUtils.addressColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.addressColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool addressSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.addressColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.address field.
	/// </summary>
	public string addressDefault
	{
		get
		{
			return TableUtils.addressColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.email field.
	/// </summary>
	public string email
	{
		get
		{
			return this.GetValue(TableUtils.emailColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.emailColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool emailSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.emailColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.email field.
	/// </summary>
	public string emailDefault
	{
		get
		{
			return TableUtils.emailColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.username field.
	/// </summary>
	public string UserName0
	{
		get
		{
			return this.GetValue(TableUtils.UserName0Column).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.UserName0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool UserName0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.UserName0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.username field.
	/// </summary>
	public string UserName0Default
	{
		get
		{
			return TableUtils.UserName0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.password field.
	/// </summary>
	public string password
	{
		get
		{
			return this.GetValue(TableUtils.passwordColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.passwordColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool passwordSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.passwordColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.password field.
	/// </summary>
	public string passwordDefault
	{
		get
		{
			return TableUtils.passwordColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Employee_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Employee_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
