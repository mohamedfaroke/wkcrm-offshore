﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopsTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopsTable"></seealso>
/// <seealso cref="ContractBenchtopsRecord"></seealso>
public class BaseContractBenchtopsRecord : PrimaryKeyRecord
{

	public readonly static ContractBenchtopsTable TableUtils = ContractBenchtopsTable.Instance;

	// Constructors
 
	protected BaseContractBenchtopsRecord() : base(TableUtils)
	{
	}

	protected BaseContractBenchtopsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public ColumnValue Getcontract_payment_type_idValue()
	{
		return this.GetValue(TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public Int32 Getcontract_payment_type_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_payment_type_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public ColumnValue Getunit_priceValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public Decimal Getunit_priceFieldValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(string val)
	{
		this.SetString(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public ColumnValue Getorder_byValue()
	{
		return this.GetValue(TableUtils.order_byColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public Int32 Getorder_byFieldValue()
	{
		return this.GetValue(TableUtils.order_byColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public void Setorder_byFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.order_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public void Setorder_byFieldValue(string val)
	{
		this.SetString(val, TableUtils.order_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public void Setorder_byFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.order_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public void Setorder_byFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.order_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public void Setorder_byFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.order_byColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.BenchtopThresholdApplies field.
	/// </summary>
	public ColumnValue GetBenchtopThresholdAppliesValue()
	{
		return this.GetValue(TableUtils.BenchtopThresholdAppliesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtops_.BenchtopThresholdApplies field.
	/// </summary>
	public bool GetBenchtopThresholdAppliesFieldValue()
	{
		return this.GetValue(TableUtils.BenchtopThresholdAppliesColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.BenchtopThresholdApplies field.
	/// </summary>
	public void SetBenchtopThresholdAppliesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.BenchtopThresholdAppliesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.BenchtopThresholdApplies field.
	/// </summary>
	public void SetBenchtopThresholdAppliesFieldValue(string val)
	{
		this.SetString(val, TableUtils.BenchtopThresholdAppliesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.BenchtopThresholdApplies field.
	/// </summary>
	public void SetBenchtopThresholdAppliesFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.BenchtopThresholdAppliesColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtops_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public Int32 contract_payment_type_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_payment_type_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_payment_type_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_payment_type_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.contract_payment_type_id field.
	/// </summary>
	public string contract_payment_type_idDefault
	{
		get
		{
			return TableUtils.contract_payment_type_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtops_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public Decimal unit_price
	{
		get
		{
			return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unit_priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unit_priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unit_priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.unit_price field.
	/// </summary>
	public string unit_priceDefault
	{
		get
		{
			return TableUtils.unit_priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtops_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public Int32 order_by
	{
		get
		{
			return this.GetValue(TableUtils.order_byColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.order_byColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool order_bySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.order_byColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.order_by field.
	/// </summary>
	public string order_byDefault
	{
		get
		{
			return TableUtils.order_byColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtops_.BenchtopThresholdApplies field.
	/// </summary>
	public bool BenchtopThresholdApplies
	{
		get
		{
			return this.GetValue(TableUtils.BenchtopThresholdAppliesColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.BenchtopThresholdAppliesColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool BenchtopThresholdAppliesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.BenchtopThresholdAppliesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtops_.BenchtopThresholdApplies field.
	/// </summary>
	public string BenchtopThresholdAppliesDefault
	{
		get
		{
			return TableUtils.BenchtopThresholdAppliesColumn.DefaultValue;
		}
	}


#endregion
}

}
