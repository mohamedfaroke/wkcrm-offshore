﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentScheduleRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="PaymentScheduleRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentScheduleTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentScheduleTable"></seealso>
/// <seealso cref="PaymentScheduleRecord"></seealso>
public class BasePaymentScheduleRecord : PrimaryKeyRecord
{

	public readonly static PaymentScheduleTable TableUtils = PaymentScheduleTable.Instance;

	// Constructors
 
	protected BasePaymentScheduleRecord() : base(TableUtils)
	{
	}

	protected BasePaymentScheduleRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentSchedule_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentSchedule_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public ColumnValue Getcontract_idValue()
	{
		return this.GetValue(TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public Decimal Getcontract_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentSchedule_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentSchedule_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentSchedule_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public Decimal contract_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.contract_id field.
	/// </summary>
	public string contract_idDefault
	{
		get
		{
			return TableUtils.contract_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentSchedule_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentSchedule_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
