﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopOptionsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopOptionsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopOptionsTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopOptionsTable"></seealso>
/// <seealso cref="ContractBenchtopOptionsRecord"></seealso>
public class BaseContractBenchtopOptionsRecord : PrimaryKeyRecord
{

	public readonly static ContractBenchtopOptionsTable TableUtils = ContractBenchtopOptionsTable.Instance;

	// Constructors
 
	protected BaseContractBenchtopOptionsRecord() : base(TableUtils)
	{
	}

	protected BaseContractBenchtopOptionsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public ColumnValue Getcontract_payment_typeValue()
	{
		return this.GetValue(TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public Int32 Getcontract_payment_typeFieldValue()
	{
		return this.GetValue(TableUtils.contract_payment_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public ColumnValue GetpriceValue()
	{
		return this.GetValue(TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public Decimal GetpriceFieldValue()
	{
		return this.GetValue(TableUtils.priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public void SetpriceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public void SetpriceFieldValue(string val)
	{
		this.SetString(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public void SetpriceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public void SetpriceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public void SetpriceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractBenchtopOptions_.status_id field.
	/// </summary>
	public string Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptions_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptions_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public Int32 contract_payment_type
	{
		get
		{
			return this.GetValue(TableUtils.contract_payment_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_payment_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_payment_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_payment_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.contract_payment_type field.
	/// </summary>
	public string contract_payment_typeDefault
	{
		get
		{
			return TableUtils.contract_payment_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public Decimal price
	{
		get
		{
			return this.GetValue(TableUtils.priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.price field.
	/// </summary>
	public string priceDefault
	{
		get
		{
			return TableUtils.priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractBenchtopOptions_.status_id field.
	/// </summary>
	public string status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.status_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractBenchtopOptions_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
