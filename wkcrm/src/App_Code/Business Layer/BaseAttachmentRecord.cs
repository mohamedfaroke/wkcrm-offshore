﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AttachmentRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="AttachmentRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AttachmentTable"></see> class.
/// </remarks>
/// <seealso cref="AttachmentTable"></seealso>
/// <seealso cref="AttachmentRecord"></seealso>
public class BaseAttachmentRecord : PrimaryKeyRecord
{

	public readonly static AttachmentTable TableUtils = AttachmentTable.Instance;

	// Constructors
 
	protected BaseAttachmentRecord() : base(TableUtils)
	{
	}

	protected BaseAttachmentRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.account_id field.
	/// </summary>
	public ColumnValue Getaccount_idValue()
	{
		return this.GetValue(TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.account_id field.
	/// </summary>
	public Decimal Getaccount_idFieldValue()
	{
		return this.GetValue(TableUtils.account_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public ColumnValue GetattachmentType_idValue()
	{
		return this.GetValue(TableUtils.attachmentType_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public Int32 GetattachmentType_idFieldValue()
	{
		return this.GetValue(TableUtils.attachmentType_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public void SetattachmentType_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.attachmentType_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public void SetattachmentType_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.attachmentType_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public void SetattachmentType_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.attachmentType_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public void SetattachmentType_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.attachmentType_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public void SetattachmentType_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.attachmentType_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.fileName field.
	/// </summary>
	public ColumnValue GetfileNameValue()
	{
		return this.GetValue(TableUtils.fileNameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.fileName field.
	/// </summary>
	public string GetfileNameFieldValue()
	{
		return this.GetValue(TableUtils.fileNameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.fileName field.
	/// </summary>
	public void SetfileNameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.fileNameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.fileName field.
	/// </summary>
	public void SetfileNameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.fileNameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Attachment_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Attachment_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Attachment_.account_id field.
	/// </summary>
	public Decimal account_id
	{
		get
		{
			return this.GetValue(TableUtils.account_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.account_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool account_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.account_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.account_id field.
	/// </summary>
	public string account_idDefault
	{
		get
		{
			return TableUtils.account_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public Int32 attachmentType_id
	{
		get
		{
			return this.GetValue(TableUtils.attachmentType_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.attachmentType_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool attachmentType_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.attachmentType_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.attachmentType_id field.
	/// </summary>
	public string attachmentType_idDefault
	{
		get
		{
			return TableUtils.attachmentType_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Attachment_.fileName field.
	/// </summary>
	public string fileName
	{
		get
		{
			return this.GetValue(TableUtils.fileNameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.fileNameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool fileNameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.fileNameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.fileName field.
	/// </summary>
	public string fileNameDefault
	{
		get
		{
			return TableUtils.fileNameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Attachment_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Attachment_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}


#endregion
}

}
