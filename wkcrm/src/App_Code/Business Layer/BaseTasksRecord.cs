﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TasksRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="TasksRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TasksTable"></see> class.
/// </remarks>
/// <seealso cref="TasksTable"></seealso>
/// <seealso cref="TasksRecord"></seealso>
public class BaseTasksRecord : PrimaryKeyRecord
{

	public readonly static TasksTable TableUtils = TasksTable.Instance;

	// Constructors
 
	protected BaseTasksRecord() : base(TableUtils)
	{
	}

	protected BaseTasksRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.project_id field.
	/// </summary>
	public ColumnValue Getproject_idValue()
	{
		return this.GetValue(TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.project_id field.
	/// </summary>
	public Decimal Getproject_idFieldValue()
	{
		return this.GetValue(TableUtils.project_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public ColumnValue Gettradesman_idValue()
	{
		return this.GetValue(TableUtils.tradesman_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public Decimal Gettradesman_idFieldValue()
	{
		return this.GetValue(TableUtils.tradesman_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public void Settradesman_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.tradesman_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public void Settradesman_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.tradesman_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public void Settradesman_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.tradesman_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public void Settradesman_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.tradesman_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public void Settradesman_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.tradesman_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public ColumnValue Gettask_type_idValue()
	{
		return this.GetValue(TableUtils.task_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public Int32 Gettask_type_idFieldValue()
	{
		return this.GetValue(TableUtils.task_type_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public void Settask_type_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.task_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public void Settask_type_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.task_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public void Settask_type_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.task_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public void Settask_type_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.task_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public void Settask_type_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.task_type_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.datetime field.
	/// </summary>
	public ColumnValue Getdatetime0Value()
	{
		return this.GetValue(TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.datetime field.
	/// </summary>
	public DateTime Getdatetime0FieldValue()
	{
		return this.GetValue(TableUtils.datetime0Column).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(string val)
	{
		this.SetString(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.datetime0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.datetime_end field.
	/// </summary>
	public ColumnValue Getdatetime_endValue()
	{
		return this.GetValue(TableUtils.datetime_endColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.datetime_end field.
	/// </summary>
	public DateTime Getdatetime_endFieldValue()
	{
		return this.GetValue(TableUtils.datetime_endColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime_end field.
	/// </summary>
	public void Setdatetime_endFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.datetime_endColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime_end field.
	/// </summary>
	public void Setdatetime_endFieldValue(string val)
	{
		this.SetString(val, TableUtils.datetime_endColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime_end field.
	/// </summary>
	public void Setdatetime_endFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.datetime_endColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.acknowledged field.
	/// </summary>
	public ColumnValue GetacknowledgedValue()
	{
		return this.GetValue(TableUtils.acknowledgedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tasks_.acknowledged field.
	/// </summary>
	public bool GetacknowledgedFieldValue()
	{
		return this.GetValue(TableUtils.acknowledgedColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.acknowledged field.
	/// </summary>
	public void SetacknowledgedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.acknowledgedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.acknowledged field.
	/// </summary>
	public void SetacknowledgedFieldValue(string val)
	{
		this.SetString(val, TableUtils.acknowledgedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.acknowledged field.
	/// </summary>
	public void SetacknowledgedFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.acknowledgedColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.project_id field.
	/// </summary>
	public Decimal project_id
	{
		get
		{
			return this.GetValue(TableUtils.project_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.project_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool project_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.project_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.project_id field.
	/// </summary>
	public string project_idDefault
	{
		get
		{
			return TableUtils.project_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public Decimal tradesman_id
	{
		get
		{
			return this.GetValue(TableUtils.tradesman_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.tradesman_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool tradesman_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.tradesman_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.tradesman_id field.
	/// </summary>
	public string tradesman_idDefault
	{
		get
		{
			return TableUtils.tradesman_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public Int32 task_type_id
	{
		get
		{
			return this.GetValue(TableUtils.task_type_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.task_type_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool task_type_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.task_type_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.task_type_id field.
	/// </summary>
	public string task_type_idDefault
	{
		get
		{
			return TableUtils.task_type_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.datetime field.
	/// </summary>
	public DateTime datetime0
	{
		get
		{
			return this.GetValue(TableUtils.datetime0Column).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.datetime0Column);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool datetime0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.datetime0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime field.
	/// </summary>
	public string datetime0Default
	{
		get
		{
			return TableUtils.datetime0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.datetime_end field.
	/// </summary>
	public DateTime datetime_end
	{
		get
		{
			return this.GetValue(TableUtils.datetime_endColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.datetime_endColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool datetime_endSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.datetime_endColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.datetime_end field.
	/// </summary>
	public string datetime_endDefault
	{
		get
		{
			return TableUtils.datetime_endColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tasks_.acknowledged field.
	/// </summary>
	public bool acknowledged
	{
		get
		{
			return this.GetValue(TableUtils.acknowledgedColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.acknowledgedColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool acknowledgedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.acknowledgedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tasks_.acknowledged field.
	/// </summary>
	public string acknowledgedDefault
	{
		get
		{
			return TableUtils.acknowledgedColumn.DefaultValue;
		}
	}


#endregion
}

}
