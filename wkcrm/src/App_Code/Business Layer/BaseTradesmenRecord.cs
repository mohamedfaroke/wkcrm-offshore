﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradesmenRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="TradesmenRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TradesmenTable"></see> class.
/// </remarks>
/// <seealso cref="TradesmenTable"></seealso>
/// <seealso cref="TradesmenRecord"></seealso>
public class BaseTradesmenRecord : PrimaryKeyRecord
{

	public readonly static TradesmenTable TableUtils = TradesmenTable.Instance;

	// Constructors
 
	protected BaseTradesmenRecord() : base(TableUtils)
	{
	}

	protected BaseTradesmenRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public ColumnValue GetTradesmanTypeIdValue()
	{
		return this.GetValue(TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public Int32 GetTradesmanTypeIdFieldValue()
	{
		return this.GetValue(TableUtils.TradesmanTypeIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.ABN field.
	/// </summary>
	public ColumnValue GetABNValue()
	{
		return this.GetValue(TableUtils.ABNColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.ABN field.
	/// </summary>
	public string GetABNFieldValue()
	{
		return this.GetValue(TableUtils.ABNColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.ABN field.
	/// </summary>
	public void SetABNFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.ABNColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.ABN field.
	/// </summary>
	public void SetABNFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ABNColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.dob field.
	/// </summary>
	public ColumnValue GetdobValue()
	{
		return this.GetValue(TableUtils.dobColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.dob field.
	/// </summary>
	public DateTime GetdobFieldValue()
	{
		return this.GetValue(TableUtils.dobColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.dob field.
	/// </summary>
	public void SetdobFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.dobColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.dob field.
	/// </summary>
	public void SetdobFieldValue(string val)
	{
		this.SetString(val, TableUtils.dobColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.dob field.
	/// </summary>
	public void SetdobFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dobColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.phone field.
	/// </summary>
	public ColumnValue GetphoneValue()
	{
		return this.GetValue(TableUtils.phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.phone field.
	/// </summary>
	public string GetphoneFieldValue()
	{
		return this.GetValue(TableUtils.phoneColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.phone field.
	/// </summary>
	public void SetphoneFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.phone field.
	/// </summary>
	public void SetphoneFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.phoneColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.mobile field.
	/// </summary>
	public ColumnValue GetmobileValue()
	{
		return this.GetValue(TableUtils.mobileColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.mobile field.
	/// </summary>
	public string GetmobileFieldValue()
	{
		return this.GetValue(TableUtils.mobileColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.mobile field.
	/// </summary>
	public void SetmobileFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.mobileColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.mobile field.
	/// </summary>
	public void SetmobileFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.mobileColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.fax field.
	/// </summary>
	public ColumnValue GetfaxValue()
	{
		return this.GetValue(TableUtils.faxColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.fax field.
	/// </summary>
	public string GetfaxFieldValue()
	{
		return this.GetValue(TableUtils.faxColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.fax field.
	/// </summary>
	public void SetfaxFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.faxColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.fax field.
	/// </summary>
	public void SetfaxFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.faxColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.address field.
	/// </summary>
	public ColumnValue GetaddressValue()
	{
		return this.GetValue(TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.address field.
	/// </summary>
	public string GetaddressFieldValue()
	{
		return this.GetValue(TableUtils.addressColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.address field.
	/// </summary>
	public void SetaddressFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.address field.
	/// </summary>
	public void SetaddressFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.addressColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.email field.
	/// </summary>
	public ColumnValue GetemailValue()
	{
		return this.GetValue(TableUtils.emailColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.email field.
	/// </summary>
	public string GetemailFieldValue()
	{
		return this.GetValue(TableUtils.emailColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.email field.
	/// </summary>
	public void SetemailFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.emailColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.email field.
	/// </summary>
	public void SetemailFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.emailColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.send_email_notifications field.
	/// </summary>
	public ColumnValue Getsend_email_notificationsValue()
	{
		return this.GetValue(TableUtils.send_email_notificationsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.send_email_notifications field.
	/// </summary>
	public bool Getsend_email_notificationsFieldValue()
	{
		return this.GetValue(TableUtils.send_email_notificationsColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.send_email_notifications field.
	/// </summary>
	public void Setsend_email_notificationsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.send_email_notificationsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.send_email_notifications field.
	/// </summary>
	public void Setsend_email_notificationsFieldValue(string val)
	{
		this.SetString(val, TableUtils.send_email_notificationsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.send_email_notifications field.
	/// </summary>
	public void Setsend_email_notificationsFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.send_email_notificationsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public ColumnValue Getentity_type_idValue()
	{
		return this.GetValue(TableUtils.entity_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public Int32 Getentity_type_idFieldValue()
	{
		return this.GetValue(TableUtils.entity_type_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public void Setentity_type_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.entity_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public void Setentity_type_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.entity_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public void Setentity_type_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.entity_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public void Setentity_type_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.entity_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public void Setentity_type_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.entity_type_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.public_liability_company field.
	/// </summary>
	public ColumnValue Getpublic_liability_companyValue()
	{
		return this.GetValue(TableUtils.public_liability_companyColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.public_liability_company field.
	/// </summary>
	public string Getpublic_liability_companyFieldValue()
	{
		return this.GetValue(TableUtils.public_liability_companyColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_company field.
	/// </summary>
	public void Setpublic_liability_companyFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.public_liability_companyColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_company field.
	/// </summary>
	public void Setpublic_liability_companyFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.public_liability_companyColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.public_liability_number field.
	/// </summary>
	public ColumnValue Getpublic_liability_numberValue()
	{
		return this.GetValue(TableUtils.public_liability_numberColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.public_liability_number field.
	/// </summary>
	public string Getpublic_liability_numberFieldValue()
	{
		return this.GetValue(TableUtils.public_liability_numberColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_number field.
	/// </summary>
	public void Setpublic_liability_numberFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.public_liability_numberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_number field.
	/// </summary>
	public void Setpublic_liability_numberFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.public_liability_numberColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.public_liability_expiry_date field.
	/// </summary>
	public ColumnValue Getpublic_liability_expiry_dateValue()
	{
		return this.GetValue(TableUtils.public_liability_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.public_liability_expiry_date field.
	/// </summary>
	public DateTime Getpublic_liability_expiry_dateFieldValue()
	{
		return this.GetValue(TableUtils.public_liability_expiry_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_expiry_date field.
	/// </summary>
	public void Setpublic_liability_expiry_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.public_liability_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_expiry_date field.
	/// </summary>
	public void Setpublic_liability_expiry_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.public_liability_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_expiry_date field.
	/// </summary>
	public void Setpublic_liability_expiry_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.public_liability_expiry_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.workers_comp_company field.
	/// </summary>
	public ColumnValue Getworkers_comp_companyValue()
	{
		return this.GetValue(TableUtils.workers_comp_companyColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.workers_comp_company field.
	/// </summary>
	public string Getworkers_comp_companyFieldValue()
	{
		return this.GetValue(TableUtils.workers_comp_companyColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_company field.
	/// </summary>
	public void Setworkers_comp_companyFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.workers_comp_companyColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_company field.
	/// </summary>
	public void Setworkers_comp_companyFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.workers_comp_companyColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.workers_comp_number field.
	/// </summary>
	public ColumnValue Getworkers_comp_numberValue()
	{
		return this.GetValue(TableUtils.workers_comp_numberColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.workers_comp_number field.
	/// </summary>
	public string Getworkers_comp_numberFieldValue()
	{
		return this.GetValue(TableUtils.workers_comp_numberColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_number field.
	/// </summary>
	public void Setworkers_comp_numberFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.workers_comp_numberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_number field.
	/// </summary>
	public void Setworkers_comp_numberFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.workers_comp_numberColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.workers_comp_expiry_date field.
	/// </summary>
	public ColumnValue Getworkers_comp_expiry_dateValue()
	{
		return this.GetValue(TableUtils.workers_comp_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.workers_comp_expiry_date field.
	/// </summary>
	public DateTime Getworkers_comp_expiry_dateFieldValue()
	{
		return this.GetValue(TableUtils.workers_comp_expiry_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_expiry_date field.
	/// </summary>
	public void Setworkers_comp_expiry_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.workers_comp_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_expiry_date field.
	/// </summary>
	public void Setworkers_comp_expiry_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.workers_comp_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_expiry_date field.
	/// </summary>
	public void Setworkers_comp_expiry_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.workers_comp_expiry_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.trade_licence_number field.
	/// </summary>
	public ColumnValue Gettrade_licence_numberValue()
	{
		return this.GetValue(TableUtils.trade_licence_numberColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.trade_licence_number field.
	/// </summary>
	public string Gettrade_licence_numberFieldValue()
	{
		return this.GetValue(TableUtils.trade_licence_numberColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.trade_licence_number field.
	/// </summary>
	public void Settrade_licence_numberFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.trade_licence_numberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.trade_licence_number field.
	/// </summary>
	public void Settrade_licence_numberFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.trade_licence_numberColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.trade_licence_expiry_date field.
	/// </summary>
	public ColumnValue Gettrade_licence_expiry_dateValue()
	{
		return this.GetValue(TableUtils.trade_licence_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.trade_licence_expiry_date field.
	/// </summary>
	public DateTime Gettrade_licence_expiry_dateFieldValue()
	{
		return this.GetValue(TableUtils.trade_licence_expiry_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.trade_licence_expiry_date field.
	/// </summary>
	public void Settrade_licence_expiry_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.trade_licence_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.trade_licence_expiry_date field.
	/// </summary>
	public void Settrade_licence_expiry_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.trade_licence_expiry_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.trade_licence_expiry_date field.
	/// </summary>
	public void Settrade_licence_expiry_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.trade_licence_expiry_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Tradesmen_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public Int32 TradesmanTypeId
	{
		get
		{
			return this.GetValue(TableUtils.TradesmanTypeIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TradesmanTypeIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TradesmanTypeIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.TradesmanTypeId field.
	/// </summary>
	public string TradesmanTypeIdDefault
	{
		get
		{
			return TableUtils.TradesmanTypeIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.ABN field.
	/// </summary>
	public string ABN
	{
		get
		{
			return this.GetValue(TableUtils.ABNColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.ABNColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool ABNSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.ABNColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.ABN field.
	/// </summary>
	public string ABNDefault
	{
		get
		{
			return TableUtils.ABNColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.dob field.
	/// </summary>
	public DateTime dob
	{
		get
		{
			return this.GetValue(TableUtils.dobColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.dobColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool dobSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.dobColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.dob field.
	/// </summary>
	public string dobDefault
	{
		get
		{
			return TableUtils.dobColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.phone field.
	/// </summary>
	public string phone
	{
		get
		{
			return this.GetValue(TableUtils.phoneColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.phoneColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool phoneSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.phoneColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.phone field.
	/// </summary>
	public string phoneDefault
	{
		get
		{
			return TableUtils.phoneColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.mobile field.
	/// </summary>
	public string mobile
	{
		get
		{
			return this.GetValue(TableUtils.mobileColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.mobileColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool mobileSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.mobileColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.mobile field.
	/// </summary>
	public string mobileDefault
	{
		get
		{
			return TableUtils.mobileColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.fax field.
	/// </summary>
	public string fax
	{
		get
		{
			return this.GetValue(TableUtils.faxColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.faxColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool faxSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.faxColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.fax field.
	/// </summary>
	public string faxDefault
	{
		get
		{
			return TableUtils.faxColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.address field.
	/// </summary>
	public string address
	{
		get
		{
			return this.GetValue(TableUtils.addressColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.addressColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool addressSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.addressColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.address field.
	/// </summary>
	public string addressDefault
	{
		get
		{
			return TableUtils.addressColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.email field.
	/// </summary>
	public string email
	{
		get
		{
			return this.GetValue(TableUtils.emailColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.emailColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool emailSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.emailColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.email field.
	/// </summary>
	public string emailDefault
	{
		get
		{
			return TableUtils.emailColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.send_email_notifications field.
	/// </summary>
	public bool send_email_notifications
	{
		get
		{
			return this.GetValue(TableUtils.send_email_notificationsColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.send_email_notificationsColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool send_email_notificationsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.send_email_notificationsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.send_email_notifications field.
	/// </summary>
	public string send_email_notificationsDefault
	{
		get
		{
			return TableUtils.send_email_notificationsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public Int32 entity_type_id
	{
		get
		{
			return this.GetValue(TableUtils.entity_type_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.entity_type_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool entity_type_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.entity_type_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.entity_type_id field.
	/// </summary>
	public string entity_type_idDefault
	{
		get
		{
			return TableUtils.entity_type_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.public_liability_company field.
	/// </summary>
	public string public_liability_company
	{
		get
		{
			return this.GetValue(TableUtils.public_liability_companyColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.public_liability_companyColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool public_liability_companySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.public_liability_companyColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_company field.
	/// </summary>
	public string public_liability_companyDefault
	{
		get
		{
			return TableUtils.public_liability_companyColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.public_liability_number field.
	/// </summary>
	public string public_liability_number
	{
		get
		{
			return this.GetValue(TableUtils.public_liability_numberColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.public_liability_numberColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool public_liability_numberSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.public_liability_numberColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_number field.
	/// </summary>
	public string public_liability_numberDefault
	{
		get
		{
			return TableUtils.public_liability_numberColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.public_liability_expiry_date field.
	/// </summary>
	public DateTime public_liability_expiry_date
	{
		get
		{
			return this.GetValue(TableUtils.public_liability_expiry_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.public_liability_expiry_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool public_liability_expiry_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.public_liability_expiry_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.public_liability_expiry_date field.
	/// </summary>
	public string public_liability_expiry_dateDefault
	{
		get
		{
			return TableUtils.public_liability_expiry_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.workers_comp_company field.
	/// </summary>
	public string workers_comp_company
	{
		get
		{
			return this.GetValue(TableUtils.workers_comp_companyColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.workers_comp_companyColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool workers_comp_companySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.workers_comp_companyColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_company field.
	/// </summary>
	public string workers_comp_companyDefault
	{
		get
		{
			return TableUtils.workers_comp_companyColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.workers_comp_number field.
	/// </summary>
	public string workers_comp_number
	{
		get
		{
			return this.GetValue(TableUtils.workers_comp_numberColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.workers_comp_numberColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool workers_comp_numberSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.workers_comp_numberColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_number field.
	/// </summary>
	public string workers_comp_numberDefault
	{
		get
		{
			return TableUtils.workers_comp_numberColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.workers_comp_expiry_date field.
	/// </summary>
	public DateTime workers_comp_expiry_date
	{
		get
		{
			return this.GetValue(TableUtils.workers_comp_expiry_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.workers_comp_expiry_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool workers_comp_expiry_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.workers_comp_expiry_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.workers_comp_expiry_date field.
	/// </summary>
	public string workers_comp_expiry_dateDefault
	{
		get
		{
			return TableUtils.workers_comp_expiry_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.trade_licence_number field.
	/// </summary>
	public string trade_licence_number
	{
		get
		{
			return this.GetValue(TableUtils.trade_licence_numberColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.trade_licence_numberColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool trade_licence_numberSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.trade_licence_numberColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.trade_licence_number field.
	/// </summary>
	public string trade_licence_numberDefault
	{
		get
		{
			return TableUtils.trade_licence_numberColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.trade_licence_expiry_date field.
	/// </summary>
	public DateTime trade_licence_expiry_date
	{
		get
		{
			return this.GetValue(TableUtils.trade_licence_expiry_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.trade_licence_expiry_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool trade_licence_expiry_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.trade_licence_expiry_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.trade_licence_expiry_date field.
	/// </summary>
	public string trade_licence_expiry_dateDefault
	{
		get
		{
			return TableUtils.trade_licence_expiry_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Tradesmen_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Tradesmen_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
