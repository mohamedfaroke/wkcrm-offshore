﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractOthersCategory_AccessoriesView.cs

using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractOthersCategory_AccessoriesView"></see> class.
/// Provides access to the schema information and record data of a database table or view named View_ContractOthersCategory_Accessories.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="View_ContractOthersCategory_AccessoriesView.Instance">View_ContractOthersCategory_AccessoriesView.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="View_ContractOthersCategory_AccessoriesView"></seealso>
[SerializableAttribute()]
public class BaseView_ContractOthersCategory_AccessoriesView : KeylessTable
{

	private readonly string TableDefinitionString = View_ContractOthersCategory_AccessoriesDefinition.GetXMLString();







	protected BaseView_ContractOthersCategory_AccessoriesView()
	{
		this.Initialize();
	}

	protected virtual void Initialize()
	{
		XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
		this.TableDefinition = new TableDefinition();
		this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.View_ContractOthersCategory_AccessoriesView");
		def.InitializeTableDefinition(this.TableDefinition);
		this.ConnectionName = def.GetConnectionName();
		this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.View_ContractOthersCategory_AccessoriesRecord");
		this.ApplicationName = "App_Code";
		this.DataAdapter = new View_ContractOthersCategory_AccessoriesSqlView();
		((View_ContractOthersCategory_AccessoriesSqlView)this.DataAdapter).ConnectionName = this.ConnectionName;
		
		this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
	}

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractOthersCategory_Accessories_.category column object.
    /// </summary>
    public BaseClasses.Data.StringColumn categoryColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractOthersCategory_Accessories_.category column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn category
    {
        get
        {
            return View_ContractOthersCategory_AccessoriesView.Instance.categoryColumn;
        }
    }
    
    


#endregion

#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of View_ContractOthersCategory_AccessoriesRecord records using a where clause.
    /// </summary>
    public static View_ContractOthersCategory_AccessoriesRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of View_ContractOthersCategory_AccessoriesRecord records using a where and order by clause.
    /// </summary>
    public static View_ContractOthersCategory_AccessoriesRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of View_ContractOthersCategory_AccessoriesRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static View_ContractOthersCategory_AccessoriesRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = View_ContractOthersCategory_AccessoriesView.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (View_ContractOthersCategory_AccessoriesRecord[])recList.ToArray(Type.GetType("WKCRM.Business.View_ContractOthersCategory_AccessoriesRecord"));
    }   
    
    public static View_ContractOthersCategory_AccessoriesRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = View_ContractOthersCategory_AccessoriesView.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (View_ContractOthersCategory_AccessoriesRecord[])recList.ToArray(Type.GetType("WKCRM.Business.View_ContractOthersCategory_AccessoriesRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)View_ContractOthersCategory_AccessoriesView.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)View_ContractOthersCategory_AccessoriesView.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a View_ContractOthersCategory_AccessoriesRecord record using a where clause.
    /// </summary>
    public static View_ContractOthersCategory_AccessoriesRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a View_ContractOthersCategory_AccessoriesRecord record using a where and order by clause.
    /// </summary>
    public static View_ContractOthersCategory_AccessoriesRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = View_ContractOthersCategory_AccessoriesView.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        View_ContractOthersCategory_AccessoriesRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (View_ContractOthersCategory_AccessoriesRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return View_ContractOthersCategory_AccessoriesView.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        View_ContractOthersCategory_AccessoriesRecord[] recs = GetRecords(where);
        return  View_ContractOthersCategory_AccessoriesView.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        View_ContractOthersCategory_AccessoriesRecord[] recs = GetRecords(where, orderBy);
        return  View_ContractOthersCategory_AccessoriesView.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        View_ContractOthersCategory_AccessoriesRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  View_ContractOthersCategory_AccessoriesView.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        View_ContractOthersCategory_AccessoriesView.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  View_ContractOthersCategory_AccessoriesView.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return View_ContractOthersCategory_AccessoriesView.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return View_ContractOthersCategory_AccessoriesView.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return View_ContractOthersCategory_AccessoriesView.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return View_ContractOthersCategory_AccessoriesView.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return View_ContractOthersCategory_AccessoriesView.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return View_ContractOthersCategory_AccessoriesView.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return View_ContractOthersCategory_AccessoriesView.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = View_ContractOthersCategory_AccessoriesView.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

#endregion
}

}
