﻿// This is a "safe" class, meaning that it is created once 
// and never overwritten. Any custom code you add to this class 
// will be preserved when you regenerate your application.
//
// Typical customizations that may be done in this class include
//  - adding custom event handlers
//  - overriding base class methods

using System;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// Provides access to the schema information and record data of a database table (or view).
/// See <see cref="BaseView_ContractOthersCategory_AccessoriesView"></see> for additional information.
/// </summary>
/// <remarks>
/// See <see cref="BaseView_ContractOthersCategory_AccessoriesView"></see> for additional information.
/// <para>
/// This class is implemented using the Singleton design pattern.
/// </para>
/// </remarks>
/// <seealso cref="BaseView_ContractOthersCategory_AccessoriesView"></seealso>
/// <seealso cref="BaseView_ContractOthersCategory_AccessoriesSqlView"></seealso>
/// <seealso cref="View_ContractOthersCategory_AccessoriesSqlView"></seealso>
/// <seealso cref="View_ContractOthersCategory_AccessoriesDefinition"></seealso>
/// <seealso cref="View_ContractOthersCategory_AccessoriesRecord"></seealso>
/// <seealso cref="BaseView_ContractOthersCategory_AccessoriesRecord"></seealso>
[SerializableAttribute()]
public class View_ContractOthersCategory_AccessoriesView : BaseView_ContractOthersCategory_AccessoriesView, System.Runtime.Serialization.ISerializable, ISingleton
{

#region "ISerializable Members"

	/// <summary>
	/// Overridden to use the <see cref="View_ContractOthersCategory_AccessoriesView_SerializationHelper"></see> class 
	/// for deserialization of <see cref="View_ContractOthersCategory_AccessoriesView"></see> data.
	/// </summary>
	/// <remarks>
	/// Since the <see cref="View_ContractOthersCategory_AccessoriesView"></see> class is implemented using the Singleton design pattern, 
	/// this method must be overridden to prevent additional instances from being created during deserialization.
	/// </remarks>
	void System.Runtime.Serialization.ISerializable.GetObjectData(
		System.Runtime.Serialization.SerializationInfo info, 
		System.Runtime.Serialization.StreamingContext context)
	{
		info.SetType(typeof(View_ContractOthersCategory_AccessoriesView_SerializationHelper)); //No other values need to be added
	}

#region "Class View_ContractOthersCategory_AccessoriesView_SerializationHelper"

	[SerializableAttribute()]
	private class View_ContractOthersCategory_AccessoriesView_SerializationHelper: System.Runtime.Serialization.IObjectReference
	{
		//Method called after this object is deserialized
		public virtual object GetRealObject(System.Runtime.Serialization.StreamingContext context)
		{
			return View_ContractOthersCategory_AccessoriesView.Instance;
		}
	}

#endregion

#endregion

	/// <summary>
	/// References the only instance of the <see cref="View_ContractOthersCategory_AccessoriesView"></see> class.
	/// </summary>
	/// <remarks>
	/// Since the <see cref="View_ContractOthersCategory_AccessoriesView"></see> class is implemented using the Singleton design pattern, 
	/// this field is the only way to access an instance of the class.
	/// </remarks>
	public readonly static View_ContractOthersCategory_AccessoriesView Instance = new View_ContractOthersCategory_AccessoriesView();

	private View_ContractOthersCategory_AccessoriesView()
	{
	}
}

}
