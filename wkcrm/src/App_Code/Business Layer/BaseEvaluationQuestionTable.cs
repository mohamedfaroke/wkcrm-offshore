﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EvaluationQuestionTable.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="EvaluationQuestionTable"></see> class.
/// Provides access to the schema information and record data of a database table or view named EvaluationQuestion.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="EvaluationQuestionTable.Instance">EvaluationQuestionTable.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="EvaluationQuestionTable"></seealso>
[SerializableAttribute()]
public class BaseEvaluationQuestionTable : PrimaryKeyTable
{

    private readonly string TableDefinitionString = EvaluationQuestionDefinition.GetXMLString();







    protected BaseEvaluationQuestionTable()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.EvaluationQuestionTable");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.EvaluationQuestionRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new EvaluationQuestionSqlTable();
        ((EvaluationQuestionSqlTable)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.QuestionID column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn QuestionIDColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.QuestionID column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn QuestionID
    {
        get
        {
            return EvaluationQuestionTable.Instance.QuestionIDColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.EvaluationID column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn EvaluationIDColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.EvaluationID column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn EvaluationID
    {
        get
        {
            return EvaluationQuestionTable.Instance.EvaluationIDColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.Question column object.
    /// </summary>
    public BaseClasses.Data.StringColumn QuestionColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.Question column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn Question
    {
        get
        {
            return EvaluationQuestionTable.Instance.QuestionColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.TypeID column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn TypeIDColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.TypeID column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn TypeID
    {
        get
        {
            return EvaluationQuestionTable.Instance.TypeIDColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.OrderNumber column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn OrderNumberColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.OrderNumber column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn OrderNumber
    {
        get
        {
            return EvaluationQuestionTable.Instance.OrderNumberColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.associated_task column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn associated_taskColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.associated_task column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn associated_task
    {
        get
        {
            return EvaluationQuestionTable.Instance.associated_taskColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.designer_question column object.
    /// </summary>
    public BaseClasses.Data.BooleanColumn designer_questionColumn
    {
        get
        {
            return (BaseClasses.Data.BooleanColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's EvaluationQuestion_.designer_question column object.
    /// </summary>
    public static BaseClasses.Data.BooleanColumn designer_question
    {
        get
        {
            return EvaluationQuestionTable.Instance.designer_questionColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of EvaluationQuestionRecord records using a where clause.
    /// </summary>
    public static EvaluationQuestionRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of EvaluationQuestionRecord records using a where and order by clause.
    /// </summary>
    public static EvaluationQuestionRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of EvaluationQuestionRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static EvaluationQuestionRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = EvaluationQuestionTable.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (EvaluationQuestionRecord[])recList.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionRecord"));
    }   
    
    public static EvaluationQuestionRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = EvaluationQuestionTable.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (EvaluationQuestionRecord[])recList.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)EvaluationQuestionTable.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)EvaluationQuestionTable.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a EvaluationQuestionRecord record using a where clause.
    /// </summary>
    public static EvaluationQuestionRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a EvaluationQuestionRecord record using a where and order by clause.
    /// </summary>
    public static EvaluationQuestionRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = EvaluationQuestionTable.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        EvaluationQuestionRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (EvaluationQuestionRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return EvaluationQuestionTable.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        EvaluationQuestionRecord[] recs = GetRecords(where);
        return  EvaluationQuestionTable.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        EvaluationQuestionRecord[] recs = GetRecords(where, orderBy);
        return  EvaluationQuestionTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        EvaluationQuestionRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  EvaluationQuestionTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        EvaluationQuestionTable.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  EvaluationQuestionTable.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return EvaluationQuestionTable.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return EvaluationQuestionTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return EvaluationQuestionTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return EvaluationQuestionTable.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return EvaluationQuestionTable.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return EvaluationQuestionTable.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return EvaluationQuestionTable.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = EvaluationQuestionTable.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static EvaluationQuestionRecord GetRecord(string id, bool bMutable)
        {
            return (EvaluationQuestionRecord)EvaluationQuestionTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static EvaluationQuestionRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (EvaluationQuestionRecord)EvaluationQuestionTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string EvaluationIDValue, 
        string QuestionValue, 
        string TypeIDValue, 
        string OrderNumberValue, 
        string associated_taskValue, 
        string designer_questionValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(EvaluationIDValue, EvaluationIDColumn);
        rec.SetString(QuestionValue, QuestionColumn);
        rec.SetString(TypeIDValue, TypeIDColumn);
        rec.SetString(OrderNumberValue, OrderNumberColumn);
        rec.SetString(associated_taskValue, associated_taskColumn);
        rec.SetString(designer_questionValue, designer_questionColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			EvaluationQuestionTable.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				EvaluationQuestionTable.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(EvaluationQuestionTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return EvaluationQuestionTable.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(EvaluationQuestionTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = EvaluationQuestionTable.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = EvaluationQuestionTable.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (EvaluationQuestionTable.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = EvaluationQuestionTable.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
