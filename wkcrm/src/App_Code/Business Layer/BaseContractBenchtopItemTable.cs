﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopItemTable.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopItemTable"></see> class.
/// Provides access to the schema information and record data of a database table or view named contractBenchtopItem.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="ContractBenchtopItemTable.Instance">ContractBenchtopItemTable.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="ContractBenchtopItemTable"></seealso>
[SerializableAttribute()]
public class BaseContractBenchtopItemTable : PrimaryKeyTable
{

    private readonly string TableDefinitionString = ContractBenchtopItemDefinition.GetXMLString();







    protected BaseContractBenchtopItemTable()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.ContractBenchtopItemTable");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.ContractBenchtopItemRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new ContractBenchtopItemSqlTable();
        ((ContractBenchtopItemSqlTable)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn id0Column
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn id0
    {
        get
        {
            return ContractBenchtopItemTable.Instance.id0Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.contract_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn contract_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.contract_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn contract_id
    {
        get
        {
            return ContractBenchtopItemTable.Instance.contract_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.item_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn item_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.item_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn item_id
    {
        get
        {
            return ContractBenchtopItemTable.Instance.item_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.supplier column object.
    /// </summary>
    public BaseClasses.Data.StringColumn supplierColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.supplier column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn supplier
    {
        get
        {
            return ContractBenchtopItemTable.Instance.supplierColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.unit_price column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn unit_priceColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.unit_price column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn unit_price
    {
        get
        {
            return ContractBenchtopItemTable.Instance.unit_priceColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.units column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn unitsColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.units column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn units
    {
        get
        {
            return ContractBenchtopItemTable.Instance.unitsColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.markup column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn markupColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.markup column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn markup
    {
        get
        {
            return ContractBenchtopItemTable.Instance.markupColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.amount column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn amountColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[7];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.amount column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn amount
    {
        get
        {
            return ContractBenchtopItemTable.Instance.amountColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.options_amount column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn options_amountColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[8];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.options_amount column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn options_amount
    {
        get
        {
            return ContractBenchtopItemTable.Instance.options_amountColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.edge_profile column object.
    /// </summary>
    public BaseClasses.Data.StringColumn edge_profileColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[9];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.edge_profile column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn edge_profile
    {
        get
        {
            return ContractBenchtopItemTable.Instance.edge_profileColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.colour column object.
    /// </summary>
    public BaseClasses.Data.StringColumn colourColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[10];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.colour column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn colour
    {
        get
        {
            return ContractBenchtopItemTable.Instance.colourColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.finish column object.
    /// </summary>
    public BaseClasses.Data.StringColumn finishColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[11];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.finish column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn finish
    {
        get
        {
            return ContractBenchtopItemTable.Instance.finishColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.edge_thickness column object.
    /// </summary>
    public BaseClasses.Data.StringColumn edge_thicknessColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[12];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.edge_thickness column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn edge_thickness
    {
        get
        {
            return ContractBenchtopItemTable.Instance.edge_thicknessColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.notes column object.
    /// </summary>
    public BaseClasses.Data.StringColumn notesColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[13];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractBenchtopItem_.notes column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn notes
    {
        get
        {
            return ContractBenchtopItemTable.Instance.notesColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of ContractBenchtopItemRecord records using a where clause.
    /// </summary>
    public static ContractBenchtopItemRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of ContractBenchtopItemRecord records using a where and order by clause.
    /// </summary>
    public static ContractBenchtopItemRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of ContractBenchtopItemRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static ContractBenchtopItemRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = ContractBenchtopItemTable.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (ContractBenchtopItemRecord[])recList.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopItemRecord"));
    }   
    
    public static ContractBenchtopItemRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = ContractBenchtopItemTable.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (ContractBenchtopItemRecord[])recList.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopItemRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)ContractBenchtopItemTable.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)ContractBenchtopItemTable.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a ContractBenchtopItemRecord record using a where clause.
    /// </summary>
    public static ContractBenchtopItemRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a ContractBenchtopItemRecord record using a where and order by clause.
    /// </summary>
    public static ContractBenchtopItemRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = ContractBenchtopItemTable.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        ContractBenchtopItemRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (ContractBenchtopItemRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return ContractBenchtopItemTable.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        ContractBenchtopItemRecord[] recs = GetRecords(where);
        return  ContractBenchtopItemTable.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        ContractBenchtopItemRecord[] recs = GetRecords(where, orderBy);
        return  ContractBenchtopItemTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        ContractBenchtopItemRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  ContractBenchtopItemTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        ContractBenchtopItemTable.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  ContractBenchtopItemTable.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return ContractBenchtopItemTable.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return ContractBenchtopItemTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return ContractBenchtopItemTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return ContractBenchtopItemTable.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return ContractBenchtopItemTable.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return ContractBenchtopItemTable.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return ContractBenchtopItemTable.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = ContractBenchtopItemTable.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static ContractBenchtopItemRecord GetRecord(string id, bool bMutable)
        {
            return (ContractBenchtopItemRecord)ContractBenchtopItemTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static ContractBenchtopItemRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (ContractBenchtopItemRecord)ContractBenchtopItemTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string contract_idValue, 
        string item_idValue, 
        string supplierValue, 
        string unit_priceValue, 
        string unitsValue, 
        string markupValue, 
        string options_amountValue, 
        string edge_profileValue, 
        string colourValue, 
        string finishValue, 
        string edge_thicknessValue, 
        string notesValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(contract_idValue, contract_idColumn);
        rec.SetString(item_idValue, item_idColumn);
        rec.SetString(supplierValue, supplierColumn);
        rec.SetString(unit_priceValue, unit_priceColumn);
        rec.SetString(unitsValue, unitsColumn);
        rec.SetString(markupValue, markupColumn);
        rec.SetString(options_amountValue, options_amountColumn);
        rec.SetString(edge_profileValue, edge_profileColumn);
        rec.SetString(colourValue, colourColumn);
        rec.SetString(finishValue, finishColumn);
        rec.SetString(edge_thicknessValue, edge_thicknessColumn);
        rec.SetString(notesValue, notesColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			ContractBenchtopItemTable.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				ContractBenchtopItemTable.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(ContractBenchtopItemTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return ContractBenchtopItemTable.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(ContractBenchtopItemTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = ContractBenchtopItemTable.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = ContractBenchtopItemTable.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (ContractBenchtopItemTable.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = ContractBenchtopItemTable.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
