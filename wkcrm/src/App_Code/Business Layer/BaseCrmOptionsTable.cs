﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in CrmOptionsTable.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="CrmOptionsTable"></see> class.
/// Provides access to the schema information and record data of a database table or view named CrmOptions.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="CrmOptionsTable.Instance">CrmOptionsTable.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="CrmOptionsTable"></seealso>
[SerializableAttribute()]
public class BaseCrmOptionsTable : PrimaryKeyTable
{

    private readonly string TableDefinitionString = CrmOptionsDefinition.GetXMLString();







    protected BaseCrmOptionsTable()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.CrmOptionsTable");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.CrmOptionsRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new CrmOptionsSqlTable();
        ((CrmOptionsSqlTable)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn id0Column
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn id0
    {
        get
        {
            return CrmOptionsTable.Instance.id0Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.doorQtyThreshold column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn doorQtyThresholdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.doorQtyThreshold column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn doorQtyThreshold
    {
        get
        {
            return CrmOptionsTable.Instance.doorQtyThresholdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.doorQtyExtraCost column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn doorQtyExtraCostColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.doorQtyExtraCost column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn doorQtyExtraCost
    {
        get
        {
            return CrmOptionsTable.Instance.doorQtyExtraCostColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.difficultDeliveryFee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn difficultDeliveryFeeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.difficultDeliveryFee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn difficultDeliveryFee
    {
        get
        {
            return CrmOptionsTable.Instance.difficultDeliveryFeeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.secondCheckMeasureFee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn secondCheckMeasureFeeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.secondCheckMeasureFee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn secondCheckMeasureFee
    {
        get
        {
            return CrmOptionsTable.Instance.secondCheckMeasureFeeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.GST column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn GSTColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.GST column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn GST
    {
        get
        {
            return CrmOptionsTable.Instance.GSTColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.insuranceThreshold column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn insuranceThresholdColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.insuranceThreshold column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn insuranceThreshold
    {
        get
        {
            return CrmOptionsTable.Instance.insuranceThresholdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.insuranceFee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn insuranceFeeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[7];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.insuranceFee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn insuranceFee
    {
        get
        {
            return CrmOptionsTable.Instance.insuranceFeeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.benchtopQtyThreshold column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn benchtopQtyThresholdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[8];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.benchtopQtyThreshold column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn benchtopQtyThreshold
    {
        get
        {
            return CrmOptionsTable.Instance.benchtopQtyThresholdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.benchtopQtyExtraCost column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn benchtopQtyExtraCostColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[9];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.benchtopQtyExtraCost column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn benchtopQtyExtraCost
    {
        get
        {
            return CrmOptionsTable.Instance.benchtopQtyExtraCostColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.dualFinishes column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn dualFinishesColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[10];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.dualFinishes column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn dualFinishes
    {
        get
        {
            return CrmOptionsTable.Instance.dualFinishesColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.dualFinishLessFee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn dualFinishLessFeeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[11];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.dualFinishLessFee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn dualFinishLessFee
    {
        get
        {
            return CrmOptionsTable.Instance.dualFinishLessFeeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.dualFinishLessFeeThreshold column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn dualFinishLessFeeThresholdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[12];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.dualFinishLessFeeThreshold column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn dualFinishLessFeeThreshold
    {
        get
        {
            return CrmOptionsTable.Instance.dualFinishLessFeeThresholdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.highRise column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn highRiseColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[13];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.highRise column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn highRise
    {
        get
        {
            return CrmOptionsTable.Instance.highRiseColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.kickboardsAfterTimber column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn kickboardsAfterTimberColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[14];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.kickboardsAfterTimber column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn kickboardsAfterTimber
    {
        get
        {
            return CrmOptionsTable.Instance.kickboardsAfterTimberColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.kickboardsWasherAfterTimber column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn kickboardsWasherAfterTimberColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[15];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.kickboardsWasherAfterTimber column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn kickboardsWasherAfterTimber
    {
        get
        {
            return CrmOptionsTable.Instance.kickboardsWasherAfterTimberColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.completionThreshold column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn completionThresholdColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[16];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's CrmOptions_.completionThreshold column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn completionThreshold
    {
        get
        {
            return CrmOptionsTable.Instance.completionThresholdColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of CrmOptionsRecord records using a where clause.
    /// </summary>
    public static CrmOptionsRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of CrmOptionsRecord records using a where and order by clause.
    /// </summary>
    public static CrmOptionsRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of CrmOptionsRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static CrmOptionsRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = CrmOptionsTable.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (CrmOptionsRecord[])recList.ToArray(Type.GetType("WKCRM.Business.CrmOptionsRecord"));
    }   
    
    public static CrmOptionsRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = CrmOptionsTable.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (CrmOptionsRecord[])recList.ToArray(Type.GetType("WKCRM.Business.CrmOptionsRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)CrmOptionsTable.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)CrmOptionsTable.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a CrmOptionsRecord record using a where clause.
    /// </summary>
    public static CrmOptionsRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a CrmOptionsRecord record using a where and order by clause.
    /// </summary>
    public static CrmOptionsRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = CrmOptionsTable.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        CrmOptionsRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (CrmOptionsRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return CrmOptionsTable.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        CrmOptionsRecord[] recs = GetRecords(where);
        return  CrmOptionsTable.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        CrmOptionsRecord[] recs = GetRecords(where, orderBy);
        return  CrmOptionsTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        CrmOptionsRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  CrmOptionsTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        CrmOptionsTable.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  CrmOptionsTable.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return CrmOptionsTable.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return CrmOptionsTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return CrmOptionsTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return CrmOptionsTable.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return CrmOptionsTable.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return CrmOptionsTable.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return CrmOptionsTable.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = CrmOptionsTable.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static CrmOptionsRecord GetRecord(string id, bool bMutable)
        {
            return (CrmOptionsRecord)CrmOptionsTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static CrmOptionsRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (CrmOptionsRecord)CrmOptionsTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string doorQtyThresholdValue, 
        string doorQtyExtraCostValue, 
        string difficultDeliveryFeeValue, 
        string secondCheckMeasureFeeValue, 
        string GSTValue, 
        string insuranceThresholdValue, 
        string insuranceFeeValue, 
        string benchtopQtyThresholdValue, 
        string benchtopQtyExtraCostValue, 
        string dualFinishesValue, 
        string dualFinishLessFeeValue, 
        string dualFinishLessFeeThresholdValue, 
        string highRiseValue, 
        string kickboardsAfterTimberValue, 
        string kickboardsWasherAfterTimberValue, 
        string completionThresholdValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(doorQtyThresholdValue, doorQtyThresholdColumn);
        rec.SetString(doorQtyExtraCostValue, doorQtyExtraCostColumn);
        rec.SetString(difficultDeliveryFeeValue, difficultDeliveryFeeColumn);
        rec.SetString(secondCheckMeasureFeeValue, secondCheckMeasureFeeColumn);
        rec.SetString(GSTValue, GSTColumn);
        rec.SetString(insuranceThresholdValue, insuranceThresholdColumn);
        rec.SetString(insuranceFeeValue, insuranceFeeColumn);
        rec.SetString(benchtopQtyThresholdValue, benchtopQtyThresholdColumn);
        rec.SetString(benchtopQtyExtraCostValue, benchtopQtyExtraCostColumn);
        rec.SetString(dualFinishesValue, dualFinishesColumn);
        rec.SetString(dualFinishLessFeeValue, dualFinishLessFeeColumn);
        rec.SetString(dualFinishLessFeeThresholdValue, dualFinishLessFeeThresholdColumn);
        rec.SetString(highRiseValue, highRiseColumn);
        rec.SetString(kickboardsAfterTimberValue, kickboardsAfterTimberColumn);
        rec.SetString(kickboardsWasherAfterTimberValue, kickboardsWasherAfterTimberColumn);
        rec.SetString(completionThresholdValue, completionThresholdColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			CrmOptionsTable.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				CrmOptionsTable.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(CrmOptionsTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return CrmOptionsTable.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(CrmOptionsTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = CrmOptionsTable.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = CrmOptionsTable.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (CrmOptionsTable.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = CrmOptionsTable.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
