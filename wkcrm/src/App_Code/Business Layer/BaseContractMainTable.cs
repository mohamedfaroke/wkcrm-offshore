﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractMainTable.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractMainTable"></see> class.
/// Provides access to the schema information and record data of a database table or view named contractMain.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="ContractMainTable.Instance">ContractMainTable.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="ContractMainTable"></seealso>
[SerializableAttribute()]
public class BaseContractMainTable : PrimaryKeyTable
{

    private readonly string TableDefinitionString = ContractMainDefinition.GetXMLString();







    protected BaseContractMainTable()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.ContractMainTable");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.ContractMainRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new ContractMainSqlTable();
        ((ContractMainSqlTable)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn id0Column
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn id0
    {
        get
        {
            return ContractMainTable.Instance.id0Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.account_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn account_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.account_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn account_id
    {
        get
        {
            return ContractMainTable.Instance.account_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.date_created column object.
    /// </summary>
    public BaseClasses.Data.DateColumn date_createdColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.date_created column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn date_created
    {
        get
        {
            return ContractMainTable.Instance.date_createdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.created_by column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn created_byColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.created_by column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn created_by
    {
        get
        {
            return ContractMainTable.Instance.created_byColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.date_modified column object.
    /// </summary>
    public BaseClasses.Data.DateColumn date_modifiedColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.date_modified column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn date_modified
    {
        get
        {
            return ContractMainTable.Instance.date_modifiedColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.modified_by column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn modified_byColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.modified_by column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn modified_by
    {
        get
        {
            return ContractMainTable.Instance.modified_byColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.signed_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn signed_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.signed_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn signed_date
    {
        get
        {
            return ContractMainTable.Instance.signed_dateColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.original_total column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn original_totalColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[7];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.original_total column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn original_total
    {
        get
        {
            return ContractMainTable.Instance.original_totalColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.check_measure_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn check_measure_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[8];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.check_measure_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn check_measure_fee
    {
        get
        {
            return ContractMainTable.Instance.check_measure_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.second_check_measure_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn second_check_measure_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[9];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.second_check_measure_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn second_check_measure_fee
    {
        get
        {
            return ContractMainTable.Instance.second_check_measure_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.bench_threshold_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn bench_threshold_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[10];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.bench_threshold_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn bench_threshold_fee
    {
        get
        {
            return ContractMainTable.Instance.bench_threshold_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.door_threshold_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn door_threshold_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[11];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.door_threshold_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn door_threshold_fee
    {
        get
        {
            return ContractMainTable.Instance.door_threshold_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.difficult_delivery_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn difficult_delivery_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[12];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.difficult_delivery_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn difficult_delivery_fee
    {
        get
        {
            return ContractMainTable.Instance.difficult_delivery_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.non_metro_delivery_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn non_metro_delivery_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[13];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.non_metro_delivery_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn non_metro_delivery_fee
    {
        get
        {
            return ContractMainTable.Instance.non_metro_delivery_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.home_insurance_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn home_insurance_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[14];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.home_insurance_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn home_insurance_fee
    {
        get
        {
            return ContractMainTable.Instance.home_insurance_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.dual_finishes_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn dual_finishes_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[15];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.dual_finishes_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn dual_finishes_fee
    {
        get
        {
            return ContractMainTable.Instance.dual_finishes_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.dual_finishes_lessthan_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn dual_finishes_lessthan_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[16];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.dual_finishes_lessthan_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn dual_finishes_lessthan_fee
    {
        get
        {
            return ContractMainTable.Instance.dual_finishes_lessthan_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.highrise_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn highrise_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[17];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.highrise_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn highrise_fee
    {
        get
        {
            return ContractMainTable.Instance.highrise_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.kickboardsAfterTimber_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn kickboardsAfterTimber_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[18];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.kickboardsAfterTimber_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn kickboardsAfterTimber_fee
    {
        get
        {
            return ContractMainTable.Instance.kickboardsAfterTimber_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.kickboardsWasherAfterTimber_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn kickboardsWasherAfterTimber_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[19];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.kickboardsWasherAfterTimber_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn kickboardsWasherAfterTimber_fee
    {
        get
        {
            return ContractMainTable.Instance.kickboardsWasherAfterTimber_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.contract_type_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn contract_type_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[20];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.contract_type_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn contract_type_id
    {
        get
        {
            return ContractMainTable.Instance.contract_type_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.estimated_completion_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn estimated_completion_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[21];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.estimated_completion_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn estimated_completion_date
    {
        get
        {
            return ContractMainTable.Instance.estimated_completion_dateColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.home_insurance_sent column object.
    /// </summary>
    public BaseClasses.Data.BooleanColumn home_insurance_sentColumn
    {
        get
        {
            return (BaseClasses.Data.BooleanColumn)this.TableDefinition.ColumnList[22];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.home_insurance_sent column object.
    /// </summary>
    public static BaseClasses.Data.BooleanColumn home_insurance_sent
    {
        get
        {
            return ContractMainTable.Instance.home_insurance_sentColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.home_insurance_policy column object.
    /// </summary>
    public BaseClasses.Data.StringColumn home_insurance_policyColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[23];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.home_insurance_policy column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn home_insurance_policy
    {
        get
        {
            return ContractMainTable.Instance.home_insurance_policyColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.designer_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn designer_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[24];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.designer_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn designer_fee
    {
        get
        {
            return ContractMainTable.Instance.designer_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.consultation_fee column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn consultation_feeColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[25];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.consultation_fee column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn consultation_fee
    {
        get
        {
            return ContractMainTable.Instance.consultation_feeColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_ceiling column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn height_ceilingColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[26];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_ceiling column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn height_ceiling
    {
        get
        {
            return ContractMainTable.Instance.height_ceilingColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_cabinet column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn height_cabinetColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[27];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_cabinet column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn height_cabinet
    {
        get
        {
            return ContractMainTable.Instance.height_cabinetColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_bench column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn height_benchColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[28];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_bench column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn height_bench
    {
        get
        {
            return ContractMainTable.Instance.height_benchColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_splashback column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn height_splashbackColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[29];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_splashback column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn height_splashback
    {
        get
        {
            return ContractMainTable.Instance.height_splashbackColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_wallcabinet column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn height_wallcabinetColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[30];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_wallcabinet column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn height_wallcabinet
    {
        get
        {
            return ContractMainTable.Instance.height_wallcabinetColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_microwave column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn height_microwaveColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[31];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_microwave column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn height_microwave
    {
        get
        {
            return ContractMainTable.Instance.height_microwaveColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_walloven column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn height_wallovenColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[32];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.height_walloven column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn height_walloven
    {
        get
        {
            return ContractMainTable.Instance.height_wallovenColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.status_id column object.
    /// </summary>
    public BaseClasses.Data.BooleanColumn status_idColumn
    {
        get
        {
            return (BaseClasses.Data.BooleanColumn)this.TableDefinition.ColumnList[33];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's ContractMain_.status_id column object.
    /// </summary>
    public static BaseClasses.Data.BooleanColumn status_id
    {
        get
        {
            return ContractMainTable.Instance.status_idColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of ContractMainRecord records using a where clause.
    /// </summary>
    public static ContractMainRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of ContractMainRecord records using a where and order by clause.
    /// </summary>
    public static ContractMainRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of ContractMainRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static ContractMainRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = ContractMainTable.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (ContractMainRecord[])recList.ToArray(Type.GetType("WKCRM.Business.ContractMainRecord"));
    }   
    
    public static ContractMainRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = ContractMainTable.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (ContractMainRecord[])recList.ToArray(Type.GetType("WKCRM.Business.ContractMainRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)ContractMainTable.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)ContractMainTable.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a ContractMainRecord record using a where clause.
    /// </summary>
    public static ContractMainRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a ContractMainRecord record using a where and order by clause.
    /// </summary>
    public static ContractMainRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = ContractMainTable.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        ContractMainRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (ContractMainRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return ContractMainTable.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        ContractMainRecord[] recs = GetRecords(where);
        return  ContractMainTable.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        ContractMainRecord[] recs = GetRecords(where, orderBy);
        return  ContractMainTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        ContractMainRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  ContractMainTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        ContractMainTable.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  ContractMainTable.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return ContractMainTable.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return ContractMainTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return ContractMainTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return ContractMainTable.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return ContractMainTable.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return ContractMainTable.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return ContractMainTable.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = ContractMainTable.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static ContractMainRecord GetRecord(string id, bool bMutable)
        {
            return (ContractMainRecord)ContractMainTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static ContractMainRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (ContractMainRecord)ContractMainTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string account_idValue, 
        string date_createdValue, 
        string created_byValue, 
        string date_modifiedValue, 
        string modified_byValue, 
        string signed_dateValue, 
        string original_totalValue, 
        string check_measure_feeValue, 
        string second_check_measure_feeValue, 
        string bench_threshold_feeValue, 
        string door_threshold_feeValue, 
        string difficult_delivery_feeValue, 
        string non_metro_delivery_feeValue, 
        string home_insurance_feeValue, 
        string dual_finishes_feeValue, 
        string dual_finishes_lessthan_feeValue, 
        string highrise_feeValue, 
        string kickboardsAfterTimber_feeValue, 
        string kickboardsWasherAfterTimber_feeValue, 
        string contract_type_idValue, 
        string estimated_completion_dateValue, 
        string home_insurance_sentValue, 
        string home_insurance_policyValue, 
        string designer_feeValue, 
        string consultation_feeValue, 
        string height_ceilingValue, 
        string height_cabinetValue, 
        string height_benchValue, 
        string height_splashbackValue, 
        string height_wallcabinetValue, 
        string height_microwaveValue, 
        string height_wallovenValue, 
        string status_idValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(account_idValue, account_idColumn);
        rec.SetString(date_createdValue, date_createdColumn);
        rec.SetString(created_byValue, created_byColumn);
        rec.SetString(date_modifiedValue, date_modifiedColumn);
        rec.SetString(modified_byValue, modified_byColumn);
        rec.SetString(signed_dateValue, signed_dateColumn);
        rec.SetString(original_totalValue, original_totalColumn);
        rec.SetString(check_measure_feeValue, check_measure_feeColumn);
        rec.SetString(second_check_measure_feeValue, second_check_measure_feeColumn);
        rec.SetString(bench_threshold_feeValue, bench_threshold_feeColumn);
        rec.SetString(door_threshold_feeValue, door_threshold_feeColumn);
        rec.SetString(difficult_delivery_feeValue, difficult_delivery_feeColumn);
        rec.SetString(non_metro_delivery_feeValue, non_metro_delivery_feeColumn);
        rec.SetString(home_insurance_feeValue, home_insurance_feeColumn);
        rec.SetString(dual_finishes_feeValue, dual_finishes_feeColumn);
        rec.SetString(dual_finishes_lessthan_feeValue, dual_finishes_lessthan_feeColumn);
        rec.SetString(highrise_feeValue, highrise_feeColumn);
        rec.SetString(kickboardsAfterTimber_feeValue, kickboardsAfterTimber_feeColumn);
        rec.SetString(kickboardsWasherAfterTimber_feeValue, kickboardsWasherAfterTimber_feeColumn);
        rec.SetString(contract_type_idValue, contract_type_idColumn);
        rec.SetString(estimated_completion_dateValue, estimated_completion_dateColumn);
        rec.SetString(home_insurance_sentValue, home_insurance_sentColumn);
        rec.SetString(home_insurance_policyValue, home_insurance_policyColumn);
        rec.SetString(designer_feeValue, designer_feeColumn);
        rec.SetString(consultation_feeValue, consultation_feeColumn);
        rec.SetString(height_ceilingValue, height_ceilingColumn);
        rec.SetString(height_cabinetValue, height_cabinetColumn);
        rec.SetString(height_benchValue, height_benchColumn);
        rec.SetString(height_splashbackValue, height_splashbackColumn);
        rec.SetString(height_wallcabinetValue, height_wallcabinetColumn);
        rec.SetString(height_microwaveValue, height_microwaveColumn);
        rec.SetString(height_wallovenValue, height_wallovenColumn);
        rec.SetString(status_idValue, status_idColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			ContractMainTable.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				ContractMainTable.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(ContractMainTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return ContractMainTable.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(ContractMainTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = ContractMainTable.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = ContractMainTable.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (ContractMainTable.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = ContractMainTable.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
