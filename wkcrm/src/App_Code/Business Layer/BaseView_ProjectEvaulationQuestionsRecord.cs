﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ProjectEvaulationQuestionsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ProjectEvaulationQuestionsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ProjectEvaulationQuestionsView"></see> class.
/// </remarks>
/// <seealso cref="View_ProjectEvaulationQuestionsView"></seealso>
/// <seealso cref="View_ProjectEvaulationQuestionsRecord"></seealso>
public class BaseView_ProjectEvaulationQuestionsRecord : PrimaryKeyRecord
{

	public readonly static View_ProjectEvaulationQuestionsView TableUtils = View_ProjectEvaulationQuestionsView.Instance;

	// Constructors
 
	protected BaseView_ProjectEvaulationQuestionsRecord() : base(TableUtils)
	{
	}

	protected BaseView_ProjectEvaulationQuestionsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public ColumnValue GetProjectEvaluationIdValue()
	{
		return this.GetValue(TableUtils.ProjectEvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public Int32 GetProjectEvaluationIdFieldValue()
	{
		return this.GetValue(TableUtils.ProjectEvaluationIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public void SetProjectEvaluationIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.ProjectEvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public void SetProjectEvaluationIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.ProjectEvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public void SetProjectEvaluationIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ProjectEvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public void SetProjectEvaluationIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ProjectEvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public void SetProjectEvaluationIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ProjectEvaluationIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public ColumnValue GetQuestionIdValue()
	{
		return this.GetValue(TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public Int32 GetQuestionIdFieldValue()
	{
		return this.GetValue(TableUtils.QuestionIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.QuestionIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public ColumnValue GetOptionIdValue()
	{
		return this.GetValue(TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public Int32 GetOptionIdFieldValue()
	{
		return this.GetValue(TableUtils.OptionIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.Answer field.
	/// </summary>
	public ColumnValue GetAnswerValue()
	{
		return this.GetValue(TableUtils.AnswerColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.Answer field.
	/// </summary>
	public string GetAnswerFieldValue()
	{
		return this.GetValue(TableUtils.AnswerColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.Answer field.
	/// </summary>
	public void SetAnswerFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.AnswerColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.Answer field.
	/// </summary>
	public void SetAnswerFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AnswerColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public ColumnValue GetdesignerIdValue()
	{
		return this.GetValue(TableUtils.designerIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public Decimal GetdesignerIdFieldValue()
	{
		return this.GetValue(TableUtils.designerIdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public void SetdesignerIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.designerIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public void SetdesignerIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.designerIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public void SetdesignerIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designerIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public void SetdesignerIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designerIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public void SetdesignerIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designerIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public ColumnValue GettradesmanIdValue()
	{
		return this.GetValue(TableUtils.tradesmanIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public Decimal GettradesmanIdFieldValue()
	{
		return this.GetValue(TableUtils.tradesmanIdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public void SettradesmanIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.tradesmanIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public void SettradesmanIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.tradesmanIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public void SettradesmanIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.tradesmanIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public void SettradesmanIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.tradesmanIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public void SettradesmanIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.tradesmanIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public ColumnValue GetTypeIDValue()
	{
		return this.GetValue(TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public Int32 GetTypeIDFieldValue()
	{
		return this.GetValue(TableUtils.TypeIDColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(string val)
	{
		this.SetString(val, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TypeIDColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public ColumnValue GetOrderNumberValue()
	{
		return this.GetValue(TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public Int32 GetOrderNumberFieldValue()
	{
		return this.GetValue(TableUtils.OrderNumberColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(string val)
	{
		this.SetString(val, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OrderNumberColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public Int32 ProjectEvaluationId
	{
		get
		{
			return this.GetValue(TableUtils.ProjectEvaluationIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.ProjectEvaluationIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool ProjectEvaluationIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.ProjectEvaluationIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.ProjectEvaluationId field.
	/// </summary>
	public string ProjectEvaluationIdDefault
	{
		get
		{
			return TableUtils.ProjectEvaluationIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public Int32 QuestionId
	{
		get
		{
			return this.GetValue(TableUtils.QuestionIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.QuestionIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool QuestionIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.QuestionIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.QuestionId field.
	/// </summary>
	public string QuestionIdDefault
	{
		get
		{
			return TableUtils.QuestionIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public Int32 OptionId
	{
		get
		{
			return this.GetValue(TableUtils.OptionIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OptionIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OptionIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OptionIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OptionId field.
	/// </summary>
	public string OptionIdDefault
	{
		get
		{
			return TableUtils.OptionIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.Answer field.
	/// </summary>
	public string Answer
	{
		get
		{
			return this.GetValue(TableUtils.AnswerColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.AnswerColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool AnswerSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.AnswerColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.Answer field.
	/// </summary>
	public string AnswerDefault
	{
		get
		{
			return TableUtils.AnswerColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public Decimal designerId
	{
		get
		{
			return this.GetValue(TableUtils.designerIdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.designerIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool designerIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.designerIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.designerId field.
	/// </summary>
	public string designerIdDefault
	{
		get
		{
			return TableUtils.designerIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public Decimal tradesmanId
	{
		get
		{
			return this.GetValue(TableUtils.tradesmanIdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.tradesmanIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool tradesmanIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.tradesmanIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.tradesmanId field.
	/// </summary>
	public string tradesmanIdDefault
	{
		get
		{
			return TableUtils.tradesmanIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public Int32 TypeID
	{
		get
		{
			return this.GetValue(TableUtils.TypeIDColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TypeIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TypeIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TypeIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.TypeID field.
	/// </summary>
	public string TypeIDDefault
	{
		get
		{
			return TableUtils.TypeIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public Int32 OrderNumber
	{
		get
		{
			return this.GetValue(TableUtils.OrderNumberColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OrderNumberColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OrderNumberSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OrderNumberColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ProjectEvaulationQuestions_.OrderNumber field.
	/// </summary>
	public string OrderNumberDefault
	{
		get
		{
			return TableUtils.OrderNumberColumn.DefaultValue;
		}
	}


#endregion
}

}
