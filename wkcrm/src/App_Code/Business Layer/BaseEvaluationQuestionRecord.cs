﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EvaluationQuestionRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="EvaluationQuestionRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EvaluationQuestionTable"></see> class.
/// </remarks>
/// <seealso cref="EvaluationQuestionTable"></seealso>
/// <seealso cref="EvaluationQuestionRecord"></seealso>
public class BaseEvaluationQuestionRecord : PrimaryKeyRecord
{

	public readonly static EvaluationQuestionTable TableUtils = EvaluationQuestionTable.Instance;

	// Constructors
 
	protected BaseEvaluationQuestionRecord() : base(TableUtils)
	{
	}

	protected BaseEvaluationQuestionRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.QuestionID field.
	/// </summary>
	public ColumnValue GetQuestionIDValue()
	{
		return this.GetValue(TableUtils.QuestionIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.QuestionID field.
	/// </summary>
	public Int32 GetQuestionIDFieldValue()
	{
		return this.GetValue(TableUtils.QuestionIDColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public ColumnValue GetEvaluationIDValue()
	{
		return this.GetValue(TableUtils.EvaluationIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public Int32 GetEvaluationIDFieldValue()
	{
		return this.GetValue(TableUtils.EvaluationIDColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public void SetEvaluationIDFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.EvaluationIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public void SetEvaluationIDFieldValue(string val)
	{
		this.SetString(val, TableUtils.EvaluationIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public void SetEvaluationIDFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public void SetEvaluationIDFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public void SetEvaluationIDFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationIDColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.Question field.
	/// </summary>
	public ColumnValue GetQuestionValue()
	{
		return this.GetValue(TableUtils.QuestionColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.Question field.
	/// </summary>
	public string GetQuestionFieldValue()
	{
		return this.GetValue(TableUtils.QuestionColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.Question field.
	/// </summary>
	public void SetQuestionFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.QuestionColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.Question field.
	/// </summary>
	public void SetQuestionFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.QuestionColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public ColumnValue GetTypeIDValue()
	{
		return this.GetValue(TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public Int32 GetTypeIDFieldValue()
	{
		return this.GetValue(TableUtils.TypeIDColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(string val)
	{
		this.SetString(val, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TypeIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public void SetTypeIDFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TypeIDColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public ColumnValue GetOrderNumberValue()
	{
		return this.GetValue(TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public Int32 GetOrderNumberFieldValue()
	{
		return this.GetValue(TableUtils.OrderNumberColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(string val)
	{
		this.SetString(val, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OrderNumberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public void SetOrderNumberFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OrderNumberColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public ColumnValue Getassociated_taskValue()
	{
		return this.GetValue(TableUtils.associated_taskColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public Int32 Getassociated_taskFieldValue()
	{
		return this.GetValue(TableUtils.associated_taskColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public void Setassociated_taskFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.associated_taskColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public void Setassociated_taskFieldValue(string val)
	{
		this.SetString(val, TableUtils.associated_taskColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public void Setassociated_taskFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.associated_taskColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public void Setassociated_taskFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.associated_taskColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public void Setassociated_taskFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.associated_taskColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.designer_question field.
	/// </summary>
	public ColumnValue Getdesigner_questionValue()
	{
		return this.GetValue(TableUtils.designer_questionColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestion_.designer_question field.
	/// </summary>
	public bool Getdesigner_questionFieldValue()
	{
		return this.GetValue(TableUtils.designer_questionColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.designer_question field.
	/// </summary>
	public void Setdesigner_questionFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.designer_questionColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.designer_question field.
	/// </summary>
	public void Setdesigner_questionFieldValue(string val)
	{
		this.SetString(val, TableUtils.designer_questionColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.designer_question field.
	/// </summary>
	public void Setdesigner_questionFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_questionColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestion_.QuestionID field.
	/// </summary>
	public Int32 QuestionID
	{
		get
		{
			return this.GetValue(TableUtils.QuestionIDColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.QuestionIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool QuestionIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.QuestionIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.QuestionID field.
	/// </summary>
	public string QuestionIDDefault
	{
		get
		{
			return TableUtils.QuestionIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public Int32 EvaluationID
	{
		get
		{
			return this.GetValue(TableUtils.EvaluationIDColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.EvaluationIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool EvaluationIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.EvaluationIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.EvaluationID field.
	/// </summary>
	public string EvaluationIDDefault
	{
		get
		{
			return TableUtils.EvaluationIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestion_.Question field.
	/// </summary>
	public string Question
	{
		get
		{
			return this.GetValue(TableUtils.QuestionColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.QuestionColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool QuestionSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.QuestionColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.Question field.
	/// </summary>
	public string QuestionDefault
	{
		get
		{
			return TableUtils.QuestionColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public Int32 TypeID
	{
		get
		{
			return this.GetValue(TableUtils.TypeIDColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TypeIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TypeIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TypeIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.TypeID field.
	/// </summary>
	public string TypeIDDefault
	{
		get
		{
			return TableUtils.TypeIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public Int32 OrderNumber
	{
		get
		{
			return this.GetValue(TableUtils.OrderNumberColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OrderNumberColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OrderNumberSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OrderNumberColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.OrderNumber field.
	/// </summary>
	public string OrderNumberDefault
	{
		get
		{
			return TableUtils.OrderNumberColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public Int32 associated_task
	{
		get
		{
			return this.GetValue(TableUtils.associated_taskColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.associated_taskColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool associated_taskSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.associated_taskColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.associated_task field.
	/// </summary>
	public string associated_taskDefault
	{
		get
		{
			return TableUtils.associated_taskColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestion_.designer_question field.
	/// </summary>
	public bool designer_question
	{
		get
		{
			return this.GetValue(TableUtils.designer_questionColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.designer_questionColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool designer_questionSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.designer_questionColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestion_.designer_question field.
	/// </summary>
	public string designer_questionDefault
	{
		get
		{
			return TableUtils.designer_questionColumn.DefaultValue;
		}
	}


#endregion
}

}
