﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountBenchtopTypeRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="AccountBenchtopTypeRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AccountBenchtopTypeTable"></see> class.
/// </remarks>
/// <seealso cref="AccountBenchtopTypeTable"></seealso>
/// <seealso cref="AccountBenchtopTypeRecord"></seealso>
public class BaseAccountBenchtopTypeRecord : PrimaryKeyRecord
{

	public readonly static AccountBenchtopTypeTable TableUtils = AccountBenchtopTypeTable.Instance;

	// Constructors
 
	protected BaseAccountBenchtopTypeRecord() : base(TableUtils)
	{
	}

	protected BaseAccountBenchtopTypeRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public ColumnValue GetAccountBenchtopTypeIdValue()
	{
		return this.GetValue(TableUtils.AccountBenchtopTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public Int32 GetAccountBenchtopTypeIdFieldValue()
	{
		return this.GetValue(TableUtils.AccountBenchtopTypeIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public void SetAccountBenchtopTypeIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.AccountBenchtopTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public void SetAccountBenchtopTypeIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.AccountBenchtopTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public void SetAccountBenchtopTypeIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountBenchtopTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public void SetAccountBenchtopTypeIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountBenchtopTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public void SetAccountBenchtopTypeIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountBenchtopTypeIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountBenchtopType_.AccountBenchtopType field.
	/// </summary>
	public ColumnValue GetAccountBenchtopTypeValue()
	{
		return this.GetValue(TableUtils.AccountBenchtopTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountBenchtopType_.AccountBenchtopType field.
	/// </summary>
	public string GetAccountBenchtopTypeFieldValue()
	{
		return this.GetValue(TableUtils.AccountBenchtopTypeColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopType field.
	/// </summary>
	public void SetAccountBenchtopTypeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.AccountBenchtopTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopType field.
	/// </summary>
	public void SetAccountBenchtopTypeFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountBenchtopTypeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountBenchtopType_.Status_id field.
	/// </summary>
	public ColumnValue GetStatus_idValue()
	{
		return this.GetValue(TableUtils.Status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountBenchtopType_.Status_id field.
	/// </summary>
	public bool GetStatus_idFieldValue()
	{
		return this.GetValue(TableUtils.Status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.Status_id field.
	/// </summary>
	public void SetStatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.Status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.Status_id field.
	/// </summary>
	public void SetStatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.Status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.Status_id field.
	/// </summary>
	public void SetStatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.Status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public Int32 AccountBenchtopTypeId
	{
		get
		{
			return this.GetValue(TableUtils.AccountBenchtopTypeIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.AccountBenchtopTypeIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool AccountBenchtopTypeIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.AccountBenchtopTypeIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopTypeId field.
	/// </summary>
	public string AccountBenchtopTypeIdDefault
	{
		get
		{
			return TableUtils.AccountBenchtopTypeIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's AccountBenchtopType_.AccountBenchtopType field.
	/// </summary>
	public string AccountBenchtopType
	{
		get
		{
			return this.GetValue(TableUtils.AccountBenchtopTypeColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.AccountBenchtopTypeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool AccountBenchtopTypeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.AccountBenchtopTypeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.AccountBenchtopType field.
	/// </summary>
	public string AccountBenchtopTypeDefault
	{
		get
		{
			return TableUtils.AccountBenchtopTypeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's AccountBenchtopType_.Status_id field.
	/// </summary>
	public bool Status_id
	{
		get
		{
			return this.GetValue(TableUtils.Status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.Status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool Status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.Status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountBenchtopType_.Status_id field.
	/// </summary>
	public string Status_idDefault
	{
		get
		{
			return TableUtils.Status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
