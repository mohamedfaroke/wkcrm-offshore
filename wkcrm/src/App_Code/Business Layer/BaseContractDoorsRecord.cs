﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorsTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorsTable"></seealso>
/// <seealso cref="ContractDoorsRecord"></seealso>
public class BaseContractDoorsRecord : PrimaryKeyRecord
{

	public readonly static ContractDoorsTable TableUtils = ContractDoorsTable.Instance;

	// Constructors
 
	protected BaseContractDoorsRecord() : base(TableUtils)
	{
	}

	protected BaseContractDoorsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public ColumnValue Getdoor_type_idValue()
	{
		return this.GetValue(TableUtils.door_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public Int32 Getdoor_type_idFieldValue()
	{
		return this.GetValue(TableUtils.door_type_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public void Setdoor_type_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.door_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public void Setdoor_type_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.door_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public void Setdoor_type_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public void Setdoor_type_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public void Setdoor_type_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_type_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.style field.
	/// </summary>
	public ColumnValue Getstyle0Value()
	{
		return this.GetValue(TableUtils.style0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.style field.
	/// </summary>
	public string Getstyle0FieldValue()
	{
		return this.GetValue(TableUtils.style0Column).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style field.
	/// </summary>
	public void Setstyle0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.style0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style field.
	/// </summary>
	public void Setstyle0FieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.style0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.edge_profile field.
	/// </summary>
	public ColumnValue Getedge_profileValue()
	{
		return this.GetValue(TableUtils.edge_profileColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.edge_profile field.
	/// </summary>
	public string Getedge_profileFieldValue()
	{
		return this.GetValue(TableUtils.edge_profileColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_profile field.
	/// </summary>
	public void Setedge_profileFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.edge_profileColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_profile field.
	/// </summary>
	public void Setedge_profileFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.edge_profileColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.face field.
	/// </summary>
	public ColumnValue GetfaceValue()
	{
		return this.GetValue(TableUtils.faceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.face field.
	/// </summary>
	public string GetfaceFieldValue()
	{
		return this.GetValue(TableUtils.faceColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face field.
	/// </summary>
	public void SetfaceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.faceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face field.
	/// </summary>
	public void SetfaceFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.faceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.finish field.
	/// </summary>
	public ColumnValue GetfinishValue()
	{
		return this.GetValue(TableUtils.finishColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.finish field.
	/// </summary>
	public string GetfinishFieldValue()
	{
		return this.GetValue(TableUtils.finishColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish field.
	/// </summary>
	public void SetfinishFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.finishColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish field.
	/// </summary>
	public void SetfinishFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.finishColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.colour field.
	/// </summary>
	public ColumnValue GetcolourValue()
	{
		return this.GetValue(TableUtils.colourColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.colour field.
	/// </summary>
	public string GetcolourFieldValue()
	{
		return this.GetValue(TableUtils.colourColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour field.
	/// </summary>
	public void SetcolourFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.colourColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour field.
	/// </summary>
	public void SetcolourFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.colourColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public ColumnValue Getunit_priceValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public Decimal Getunit_priceFieldValue()
	{
		return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(string val)
	{
		this.SetString(val, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public void Setunit_priceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unit_priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public ColumnValue Getstyle_typeValue()
	{
		return this.GetValue(TableUtils.style_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public Int32 Getstyle_typeFieldValue()
	{
		return this.GetValue(TableUtils.style_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public void Setstyle_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.style_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public void Setstyle_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.style_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public void Setstyle_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.style_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public void Setstyle_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.style_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public void Setstyle_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.style_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public ColumnValue Getedge_typeValue()
	{
		return this.GetValue(TableUtils.edge_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public Int32 Getedge_typeFieldValue()
	{
		return this.GetValue(TableUtils.edge_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public void Setedge_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.edge_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public void Setedge_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.edge_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public void Setedge_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.edge_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public void Setedge_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.edge_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public void Setedge_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.edge_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public ColumnValue Getface_typeValue()
	{
		return this.GetValue(TableUtils.face_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public Int32 Getface_typeFieldValue()
	{
		return this.GetValue(TableUtils.face_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public void Setface_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.face_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public void Setface_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.face_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public void Setface_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.face_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public void Setface_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.face_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public void Setface_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.face_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public ColumnValue Getfinish_typeValue()
	{
		return this.GetValue(TableUtils.finish_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public Int32 Getfinish_typeFieldValue()
	{
		return this.GetValue(TableUtils.finish_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public void Setfinish_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.finish_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public void Setfinish_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.finish_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public void Setfinish_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.finish_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public void Setfinish_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.finish_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public void Setfinish_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.finish_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public ColumnValue Getcolour_typeValue()
	{
		return this.GetValue(TableUtils.colour_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public Int32 Getcolour_typeFieldValue()
	{
		return this.GetValue(TableUtils.colour_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public void Setcolour_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.colour_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public void Setcolour_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.colour_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public void Setcolour_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.colour_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public void Setcolour_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.colour_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public void Setcolour_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.colour_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.show_extra_fields field.
	/// </summary>
	public ColumnValue Getshow_extra_fieldsValue()
	{
		return this.GetValue(TableUtils.show_extra_fieldsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.show_extra_fields field.
	/// </summary>
	public bool Getshow_extra_fieldsFieldValue()
	{
		return this.GetValue(TableUtils.show_extra_fieldsColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.show_extra_fields field.
	/// </summary>
	public void Setshow_extra_fieldsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.show_extra_fieldsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.show_extra_fields field.
	/// </summary>
	public void Setshow_extra_fieldsFieldValue(string val)
	{
		this.SetString(val, TableUtils.show_extra_fieldsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.show_extra_fields field.
	/// </summary>
	public void Setshow_extra_fieldsFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.show_extra_fieldsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractDoors_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public Int32 door_type_id
	{
		get
		{
			return this.GetValue(TableUtils.door_type_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.door_type_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool door_type_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.door_type_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.door_type_id field.
	/// </summary>
	public string door_type_idDefault
	{
		get
		{
			return TableUtils.door_type_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.style field.
	/// </summary>
	public string style0
	{
		get
		{
			return this.GetValue(TableUtils.style0Column).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.style0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool style0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.style0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style field.
	/// </summary>
	public string style0Default
	{
		get
		{
			return TableUtils.style0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.edge_profile field.
	/// </summary>
	public string edge_profile
	{
		get
		{
			return this.GetValue(TableUtils.edge_profileColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.edge_profileColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool edge_profileSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.edge_profileColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_profile field.
	/// </summary>
	public string edge_profileDefault
	{
		get
		{
			return TableUtils.edge_profileColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.face field.
	/// </summary>
	public string face
	{
		get
		{
			return this.GetValue(TableUtils.faceColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.faceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool faceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.faceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face field.
	/// </summary>
	public string faceDefault
	{
		get
		{
			return TableUtils.faceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.finish field.
	/// </summary>
	public string finish
	{
		get
		{
			return this.GetValue(TableUtils.finishColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.finishColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool finishSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.finishColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish field.
	/// </summary>
	public string finishDefault
	{
		get
		{
			return TableUtils.finishColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.colour field.
	/// </summary>
	public string colour
	{
		get
		{
			return this.GetValue(TableUtils.colourColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.colourColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool colourSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.colourColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour field.
	/// </summary>
	public string colourDefault
	{
		get
		{
			return TableUtils.colourColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public Decimal unit_price
	{
		get
		{
			return this.GetValue(TableUtils.unit_priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unit_priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unit_priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unit_priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.unit_price field.
	/// </summary>
	public string unit_priceDefault
	{
		get
		{
			return TableUtils.unit_priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public Int32 style_type
	{
		get
		{
			return this.GetValue(TableUtils.style_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.style_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool style_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.style_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.style_type field.
	/// </summary>
	public string style_typeDefault
	{
		get
		{
			return TableUtils.style_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public Int32 edge_type
	{
		get
		{
			return this.GetValue(TableUtils.edge_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.edge_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool edge_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.edge_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.edge_type field.
	/// </summary>
	public string edge_typeDefault
	{
		get
		{
			return TableUtils.edge_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public Int32 face_type
	{
		get
		{
			return this.GetValue(TableUtils.face_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.face_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool face_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.face_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.face_type field.
	/// </summary>
	public string face_typeDefault
	{
		get
		{
			return TableUtils.face_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public Int32 finish_type
	{
		get
		{
			return this.GetValue(TableUtils.finish_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.finish_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool finish_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.finish_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.finish_type field.
	/// </summary>
	public string finish_typeDefault
	{
		get
		{
			return TableUtils.finish_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public Int32 colour_type
	{
		get
		{
			return this.GetValue(TableUtils.colour_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.colour_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool colour_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.colour_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.colour_type field.
	/// </summary>
	public string colour_typeDefault
	{
		get
		{
			return TableUtils.colour_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.show_extra_fields field.
	/// </summary>
	public bool show_extra_fields
	{
		get
		{
			return this.GetValue(TableUtils.show_extra_fieldsColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.show_extra_fieldsColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool show_extra_fieldsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.show_extra_fieldsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.show_extra_fields field.
	/// </summary>
	public string show_extra_fieldsDefault
	{
		get
		{
			return TableUtils.show_extra_fieldsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractDoors_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractDoors_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
