﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_AccountReviewThisWeekRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_AccountReviewThisWeekRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_AccountReviewThisWeekView"></see> class.
/// </remarks>
/// <seealso cref="View_AccountReviewThisWeekView"></seealso>
/// <seealso cref="View_AccountReviewThisWeekRecord"></seealso>
public class BaseView_AccountReviewThisWeekRecord : PrimaryKeyRecord
{

	public readonly static View_AccountReviewThisWeekView TableUtils = View_AccountReviewThisWeekView.Instance;

	// Constructors
 
	protected BaseView_AccountReviewThisWeekRecord() : base(TableUtils)
	{
	}

	protected BaseView_AccountReviewThisWeekRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.Name field.
	/// </summary>
	public ColumnValue GetNameValue()
	{
		return this.GetValue(TableUtils.NameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.Name field.
	/// </summary>
	public string GetNameFieldValue()
	{
		return this.GetValue(TableUtils.NameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.Name field.
	/// </summary>
	public void SetNameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.NameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.Name field.
	/// </summary>
	public void SetNameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.NameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public ColumnValue Getdesigner_idValue()
	{
		return this.GetValue(TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public Decimal Getdesigner_idFieldValue()
	{
		return this.GetValue(TableUtils.designer_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.next_review_date field.
	/// </summary>
	public ColumnValue Getnext_review_dateValue()
	{
		return this.GetValue(TableUtils.next_review_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.next_review_date field.
	/// </summary>
	public DateTime Getnext_review_dateFieldValue()
	{
		return this.GetValue(TableUtils.next_review_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.next_review_date field.
	/// </summary>
	public void Setnext_review_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.next_review_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.next_review_date field.
	/// </summary>
	public void Setnext_review_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.next_review_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.next_review_date field.
	/// </summary>
	public void Setnext_review_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.next_review_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public ColumnValue GetDaysTillReviewValue()
	{
		return this.GetValue(TableUtils.DaysTillReviewColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public Int32 GetDaysTillReviewFieldValue()
	{
		return this.GetValue(TableUtils.DaysTillReviewColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public void SetDaysTillReviewFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.DaysTillReviewColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public void SetDaysTillReviewFieldValue(string val)
	{
		this.SetString(val, TableUtils.DaysTillReviewColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public void SetDaysTillReviewFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.DaysTillReviewColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public void SetDaysTillReviewFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.DaysTillReviewColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public void SetDaysTillReviewFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.DaysTillReviewColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public ColumnValue Getweeks_openValue()
	{
		return this.GetValue(TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public Int32 Getweeks_openFieldValue()
	{
		return this.GetValue(TableUtils.weeks_openColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(string val)
	{
		this.SetString(val, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.weeks_openColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountReviewThisWeek_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountReviewThisWeek_.Name field.
	/// </summary>
	public string Name
	{
		get
		{
			return this.GetValue(TableUtils.NameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.NameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool NameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.NameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.Name field.
	/// </summary>
	public string NameDefault
	{
		get
		{
			return TableUtils.NameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public Decimal designer_id
	{
		get
		{
			return this.GetValue(TableUtils.designer_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.designer_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool designer_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.designer_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.designer_id field.
	/// </summary>
	public string designer_idDefault
	{
		get
		{
			return TableUtils.designer_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountReviewThisWeek_.next_review_date field.
	/// </summary>
	public DateTime next_review_date
	{
		get
		{
			return this.GetValue(TableUtils.next_review_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.next_review_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool next_review_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.next_review_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.next_review_date field.
	/// </summary>
	public string next_review_dateDefault
	{
		get
		{
			return TableUtils.next_review_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public Int32 DaysTillReview
	{
		get
		{
			return this.GetValue(TableUtils.DaysTillReviewColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.DaysTillReviewColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool DaysTillReviewSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.DaysTillReviewColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.DaysTillReview field.
	/// </summary>
	public string DaysTillReviewDefault
	{
		get
		{
			return TableUtils.DaysTillReviewColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public Int32 weeks_open
	{
		get
		{
			return this.GetValue(TableUtils.weeks_openColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.weeks_openColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool weeks_openSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.weeks_openColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountReviewThisWeek_.weeks_open field.
	/// </summary>
	public string weeks_openDefault
	{
		get
		{
			return TableUtils.weeks_openColumn.DefaultValue;
		}
	}


#endregion
}

}
