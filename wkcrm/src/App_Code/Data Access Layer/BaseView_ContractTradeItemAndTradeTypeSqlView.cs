﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractTradeItemAndTradeTypeSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractTradeItemAndTradeTypeSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractTradeItemAndTradeTypeView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractTradeItemAndTradeTypeView"></seealso>
/// <seealso cref="View_ContractTradeItemAndTradeTypeSqlView"></seealso>
public class BaseView_ContractTradeItemAndTradeTypeSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ContractTradeItemAndTradeTypeSqlView()
	{
	}

	public BaseView_ContractTradeItemAndTradeTypeSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
