﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopEdgeProfilesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopEdgeProfilesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopEdgeProfilesTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopEdgeProfilesTable"></seealso>
/// <seealso cref="ContractBenchtopEdgeProfilesSqlTable"></seealso>
public class BaseContractBenchtopEdgeProfilesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractBenchtopEdgeProfilesSqlTable()
	{
	}

	public BaseContractBenchtopEdgeProfilesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
