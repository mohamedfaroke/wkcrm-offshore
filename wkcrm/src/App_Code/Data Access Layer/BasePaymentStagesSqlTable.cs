﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentStagesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="PaymentStagesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentStagesTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentStagesTable"></seealso>
/// <seealso cref="PaymentStagesSqlTable"></seealso>
public class BasePaymentStagesSqlTable : DynamicSQLServerAdapter
{
	
	public BasePaymentStagesSqlTable()
	{
	}

	public BasePaymentStagesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
