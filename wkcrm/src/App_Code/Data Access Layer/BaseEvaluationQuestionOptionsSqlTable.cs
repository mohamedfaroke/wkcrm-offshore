﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EvaluationQuestionOptionsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="EvaluationQuestionOptionsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EvaluationQuestionOptionsTable"></see> class.
/// </remarks>
/// <seealso cref="EvaluationQuestionOptionsTable"></seealso>
/// <seealso cref="EvaluationQuestionOptionsSqlTable"></seealso>
public class BaseEvaluationQuestionOptionsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseEvaluationQuestionOptionsSqlTable()
	{
	}

	public BaseEvaluationQuestionOptionsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
