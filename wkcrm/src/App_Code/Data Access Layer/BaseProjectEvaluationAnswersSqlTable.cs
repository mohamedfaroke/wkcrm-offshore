﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ProjectEvaluationAnswersSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ProjectEvaluationAnswersSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ProjectEvaluationAnswersTable"></see> class.
/// </remarks>
/// <seealso cref="ProjectEvaluationAnswersTable"></seealso>
/// <seealso cref="ProjectEvaluationAnswersSqlTable"></seealso>
public class BaseProjectEvaluationAnswersSqlTable : DynamicSQLServerAdapter
{
	
	public BaseProjectEvaluationAnswersSqlTable()
	{
	}

	public BaseProjectEvaluationAnswersSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
