﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopOptionItemSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopOptionItemSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopOptionItemTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopOptionItemTable"></seealso>
/// <seealso cref="ContractBenchtopOptionItemSqlTable"></seealso>
public class BaseContractBenchtopOptionItemSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractBenchtopOptionItemSqlTable()
	{
	}

	public BaseContractBenchtopOptionItemSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
