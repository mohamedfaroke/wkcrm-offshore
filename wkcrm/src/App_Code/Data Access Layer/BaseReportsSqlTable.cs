﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ReportsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ReportsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ReportsTable"></see> class.
/// </remarks>
/// <seealso cref="ReportsTable"></seealso>
/// <seealso cref="ReportsSqlTable"></seealso>
public class BaseReportsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseReportsSqlTable()
	{
	}

	public BaseReportsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
