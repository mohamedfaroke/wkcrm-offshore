﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_EmployeeTotalOutstandingTasksSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_EmployeeTotalOutstandingTasksSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_EmployeeTotalOutstandingTasksView"></see> class.
/// </remarks>
/// <seealso cref="View_EmployeeTotalOutstandingTasksView"></seealso>
/// <seealso cref="View_EmployeeTotalOutstandingTasksSqlView"></seealso>
public class BaseView_EmployeeTotalOutstandingTasksSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_EmployeeTotalOutstandingTasksSqlView()
	{
	}

	public BaseView_EmployeeTotalOutstandingTasksSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
