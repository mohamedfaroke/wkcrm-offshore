﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_AccountContractProjectSqlView.cs 

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_AccountContractProjectSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_AccountContractProjectView"></see> class.
/// </remarks>
/// <seealso cref="View_AccountContractProjectView"></seealso>
/// <seealso cref="View_AccountContractProjectSqlView"></seealso>
public class BaseView_AccountContractProjectSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_AccountContractProjectSqlView()
	{
	}

	public BaseView_AccountContractProjectSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
