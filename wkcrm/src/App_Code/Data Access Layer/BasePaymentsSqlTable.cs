﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="PaymentsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentsTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentsTable"></seealso>
/// <seealso cref="PaymentsSqlTable"></seealso>
public class BasePaymentsSqlTable : DynamicSQLServerAdapter
{
	
	public BasePaymentsSqlTable()
	{
	}

	public BasePaymentsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
