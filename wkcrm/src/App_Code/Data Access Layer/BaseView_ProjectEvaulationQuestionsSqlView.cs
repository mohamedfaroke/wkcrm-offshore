﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ProjectEvaulationQuestionsSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ProjectEvaulationQuestionsSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ProjectEvaulationQuestionsView"></see> class.
/// </remarks>
/// <seealso cref="View_ProjectEvaulationQuestionsView"></seealso>
/// <seealso cref="View_ProjectEvaulationQuestionsSqlView"></seealso>
public class BaseView_ProjectEvaulationQuestionsSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ProjectEvaulationQuestionsSqlView()
	{
	}

	public BaseView_ProjectEvaulationQuestionsSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
