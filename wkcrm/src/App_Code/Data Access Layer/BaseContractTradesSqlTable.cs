﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractTradesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractTradesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractTradesTable"></see> class.
/// </remarks>
/// <seealso cref="ContractTradesTable"></seealso>
/// <seealso cref="ContractTradesSqlTable"></seealso>
public class BaseContractTradesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractTradesSqlTable()
	{
	}

	public BaseContractTradesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
