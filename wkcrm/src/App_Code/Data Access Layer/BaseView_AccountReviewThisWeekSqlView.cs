﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_AccountReviewThisWeekSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_AccountReviewThisWeekSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_AccountReviewThisWeekView"></see> class.
/// </remarks>
/// <seealso cref="View_AccountReviewThisWeekView"></seealso>
/// <seealso cref="View_AccountReviewThisWeekSqlView"></seealso>
public class BaseView_AccountReviewThisWeekSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_AccountReviewThisWeekSqlView()
	{
	}

	public BaseView_AccountReviewThisWeekSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
