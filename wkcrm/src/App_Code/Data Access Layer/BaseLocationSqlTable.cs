﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in LocationSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="LocationSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="LocationTable"></see> class.
/// </remarks>
/// <seealso cref="LocationTable"></seealso>
/// <seealso cref="LocationSqlTable"></seealso>
public class BaseLocationSqlTable : DynamicSQLServerAdapter
{
	
	public BaseLocationSqlTable()
	{
	}

	public BaseLocationSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
