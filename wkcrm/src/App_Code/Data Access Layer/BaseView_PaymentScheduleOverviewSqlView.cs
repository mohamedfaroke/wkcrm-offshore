﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_PaymentScheduleOverviewSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_PaymentScheduleOverviewSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_PaymentScheduleOverviewView"></see> class.
/// </remarks>
/// <seealso cref="View_PaymentScheduleOverviewView"></seealso>
/// <seealso cref="View_PaymentScheduleOverviewSqlView"></seealso>
public class BaseView_PaymentScheduleOverviewSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_PaymentScheduleOverviewSqlView()
	{
	}

	public BaseView_PaymentScheduleOverviewSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
