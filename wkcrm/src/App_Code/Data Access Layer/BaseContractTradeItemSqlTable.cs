﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractTradeItemSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractTradeItemSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractTradeItemTable"></see> class.
/// </remarks>
/// <seealso cref="ContractTradeItemTable"></seealso>
/// <seealso cref="ContractTradeItemSqlTable"></seealso>
public class BaseContractTradeItemSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractTradeItemSqlTable()
	{
	}

	public BaseContractTradeItemSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
