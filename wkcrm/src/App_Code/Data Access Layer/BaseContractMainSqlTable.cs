﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractMainSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractMainSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractMainTable"></see> class.
/// </remarks>
/// <seealso cref="ContractMainTable"></seealso>
/// <seealso cref="ContractMainSqlTable"></seealso>
public class BaseContractMainSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractMainSqlTable()
	{
	}

	public BaseContractMainSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
