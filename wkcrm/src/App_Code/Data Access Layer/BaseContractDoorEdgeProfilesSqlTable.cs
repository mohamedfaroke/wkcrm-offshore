﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorEdgeProfilesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorEdgeProfilesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorEdgeProfilesTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorEdgeProfilesTable"></seealso>
/// <seealso cref="ContractDoorEdgeProfilesSqlTable"></seealso>
public class BaseContractDoorEdgeProfilesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractDoorEdgeProfilesSqlTable()
	{
	}

	public BaseContractDoorEdgeProfilesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
