﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountStatusSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="AccountStatusSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AccountStatusTable"></see> class.
/// </remarks>
/// <seealso cref="AccountStatusTable"></seealso>
/// <seealso cref="AccountStatusSqlTable"></seealso>
public class BaseAccountStatusSqlTable : DynamicSQLServerAdapter
{
	
	public BaseAccountStatusSqlTable()
	{
	}

	public BaseAccountStatusSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
