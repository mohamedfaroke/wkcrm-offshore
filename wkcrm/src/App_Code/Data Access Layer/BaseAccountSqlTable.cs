﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="AccountSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AccountTable"></see> class.
/// </remarks>
/// <seealso cref="AccountTable"></seealso>
/// <seealso cref="AccountSqlTable"></seealso>
public class BaseAccountSqlTable : DynamicSQLServerAdapter
{
	
	public BaseAccountSqlTable()
	{
	}

	public BaseAccountSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
