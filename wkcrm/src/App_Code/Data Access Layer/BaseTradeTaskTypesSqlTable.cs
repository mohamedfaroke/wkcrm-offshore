﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradeTaskTypesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="TradeTaskTypesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TradeTaskTypesTable"></see> class.
/// </remarks>
/// <seealso cref="TradeTaskTypesTable"></seealso>
/// <seealso cref="TradeTaskTypesSqlTable"></seealso>
public class BaseTradeTaskTypesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseTradeTaskTypesSqlTable()
	{
	}

	public BaseTradeTaskTypesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
