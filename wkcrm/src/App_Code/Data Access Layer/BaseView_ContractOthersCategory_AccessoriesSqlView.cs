﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractOthersCategory_AccessoriesSqlView.cs 

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractOthersCategory_AccessoriesSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractOthersCategory_AccessoriesView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractOthersCategory_AccessoriesView"></seealso>
/// <seealso cref="View_ContractOthersCategory_AccessoriesSqlView"></seealso>
public class BaseView_ContractOthersCategory_AccessoriesSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ContractOthersCategory_AccessoriesSqlView()
	{
	}

	public BaseView_ContractOthersCategory_AccessoriesSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
