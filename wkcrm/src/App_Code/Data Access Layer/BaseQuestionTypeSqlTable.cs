﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in QuestionTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="QuestionTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="QuestionTypeTable"></see> class.
/// </remarks>
/// <seealso cref="QuestionTypeTable"></seealso>
/// <seealso cref="QuestionTypeSqlTable"></seealso>
public class BaseQuestionTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseQuestionTypeSqlTable()
	{
	}

	public BaseQuestionTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
