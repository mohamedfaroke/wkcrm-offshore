﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in LeadMeetingsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="LeadMeetingsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="LeadMeetingsTable"></see> class.
/// </remarks>
/// <seealso cref="LeadMeetingsTable"></seealso>
/// <seealso cref="LeadMeetingsSqlTable"></seealso>
public class BaseLeadMeetingsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseLeadMeetingsSqlTable()
	{
	}

	public BaseLeadMeetingsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
