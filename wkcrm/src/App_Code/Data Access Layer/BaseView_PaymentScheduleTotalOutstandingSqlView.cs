﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_PaymentScheduleTotalOutstandingSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_PaymentScheduleTotalOutstandingSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_PaymentScheduleTotalOutstandingView"></see> class.
/// </remarks>
/// <seealso cref="View_PaymentScheduleTotalOutstandingView"></seealso>
/// <seealso cref="View_PaymentScheduleTotalOutstandingSqlView"></seealso>
public class BaseView_PaymentScheduleTotalOutstandingSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_PaymentScheduleTotalOutstandingSqlView()
	{
	}

	public BaseView_PaymentScheduleTotalOutstandingSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
