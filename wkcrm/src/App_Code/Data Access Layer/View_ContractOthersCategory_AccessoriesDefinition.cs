﻿using System;

namespace WKCRM.Business
{

/// <summary>
/// Contains embedded schema and configuration data that is used by the 
/// <see cref="View_ContractOthersCategory_AccessoriesView">WKCRM.View_ContractOthersCategory_AccessoriesView</see> class
/// to initialize the class's TableDefinition.
/// </summary>
/// <seealso cref="View_ContractOthersCategory_AccessoriesView"></seealso>
public class View_ContractOthersCategory_AccessoriesDefinition
{
#region "Definition (XML) for View_ContractOthersCategory_AccessoriesDefinition table"
	//Next 37 lines contain Table Definition (XML) for table "View_ContractOthersCategory_AccessoriesDefinition"
	private static string _DefinitionString = 
@"<XMLDefinition Generator=""Iron Speed Designer"" Version=""4.3"" Type=""VIEW"">" +
  @"<ColumnDefinition>" +
    @"<Column InternalName=""0"" Priority=""1"" ColumnNum=""0"">" +
      @"<columnName>category</columnName>" +
      @"<columnUIName>Category</columnUIName>" +
      @"<columnType>String</columnType>" +
      @"<columnDBType>varchar</columnDBType>" +
      @"<columnLengthSet>100</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>N</columnRequired>" +
      @"<columnNotNull>N</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
    "</Column>" +
  "</ColumnDefinition>" +
  @"<TableName>View_ContractOthersCategory_Accessories</TableName>" +
  @"<Version></Version>" +
  @"<Owner>dbo</Owner>" +
  @"<TableCodeName>View_ContractOthersCategory_Accessories</TableCodeName>" +
  @"<TableAliasName>View_ContractOthersCategory_Accessories_</TableAliasName>" +
  @"<ConnectionName>Databasewk1</ConnectionName>" +
  @"<canCreateRecords Source=""Database"">N</canCreateRecords>" +
  @"<canEditRecords Source=""Database"">N</canEditRecords>" +
  @"<canDeleteRecords Source=""Database"">N</canDeleteRecords>" +
  @"<canViewRecords Source=""Database"">N</canViewRecords>" +
  @"<AppShortName>WKCRM</AppShortName>" +
"</XMLDefinition>";
#endregion

	/// <summary>
	/// Gets the embedded schema and configuration data for the  
	/// <see cref="View_ContractOthersCategory_AccessoriesView"></see>
	/// class's TableDefinition.
	/// </summary>
	/// <remarks>This function is only called once at runtime.</remarks>
	/// <returns>An XML string.</returns>
	public static string GetXMLString()
	{
		return _DefinitionString;
	}
}

}
