﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentVariationsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="PaymentVariationsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentVariationsTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentVariationsTable"></seealso>
/// <seealso cref="PaymentVariationsSqlTable"></seealso>
public class BasePaymentVariationsSqlTable : DynamicSQLServerAdapter
{
	
	public BasePaymentVariationsSqlTable()
	{
	}

	public BasePaymentVariationsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
