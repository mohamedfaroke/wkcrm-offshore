﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorsTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorsTable"></seealso>
/// <seealso cref="ContractDoorsSqlTable"></seealso>
public class BaseContractDoorsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractDoorsSqlTable()
	{
	}

	public BaseContractDoorsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
