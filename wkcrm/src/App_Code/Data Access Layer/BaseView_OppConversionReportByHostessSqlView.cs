﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_OppConversionReportByHostessSqlView.cs 

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_OppConversionReportByHostessSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_OppConversionReportByHostessView"></see> class.
/// </remarks>
/// <seealso cref="View_OppConversionReportByHostessView"></seealso>
/// <seealso cref="View_OppConversionReportByHostessSqlView"></seealso>
public class BaseView_OppConversionReportByHostessSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_OppConversionReportByHostessSqlView()
	{
	}

	public BaseView_OppConversionReportByHostessSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
