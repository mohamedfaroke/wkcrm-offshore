﻿using System;

namespace WKCRM.Business
{

/// <summary>
/// Contains embedded schema and configuration data that is used by the 
/// <see cref="View_ContractOtherItemWithTypesView">WKCRM.View_ContractOtherItemWithTypesView</see> class
/// to initialize the class's TableDefinition.
/// </summary>
/// <seealso cref="View_ContractOtherItemWithTypesView"></seealso>
public class View_ContractOtherItemWithTypesDefinition
{
#region "Definition (XML) for View_ContractOtherItemWithTypesDefinition table"
	//Next 216 lines contain Table Definition (XML) for table "View_ContractOtherItemWithTypesDefinition"
	private static string _DefinitionString = 
@"<XMLDefinition Generator=""Iron Speed Designer"" Version=""4.3"" Type=""VIEW"">" +
  @"<ColumnDefinition>" +
    @"<Column InternalName=""0"" Priority=""1"" ColumnNum=""0"">" +
      @"<columnName>id</columnName>" +
      @"<columnUIName>ID</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>Y</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<columnVirtualPK Source=""User"">Y</columnVirtualPK>" +
    "</Column>" +
    @"<Column InternalName=""1"" Priority=""2"" ColumnNum=""1"">" +
      @"<columnName>contract_id</columnName>" +
      @"<columnUIName>Contract</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ContractOtherItemWithTypes_contract_id_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.ContractMainTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay></columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
    @"<Column InternalName=""2"" Priority=""3"" ColumnNum=""2"">" +
      @"<columnName>item_id</columnName>" +
      @"<columnUIName>Item</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>int</columnDBType>" +
      @"<columnLengthSet>10.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ContractOtherItemWithTypes_item_id_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.ContractOthersTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay>name</columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
    @"<Column InternalName=""3"" Priority=""4"" ColumnNum=""3"">" +
      @"<columnName>unit_price</columnName>" +
      @"<columnUIName>Unit Price</columnUIName>" +
      @"<columnType>Currency</columnType>" +
      @"<columnDBType>money</columnDBType>" +
      @"<columnLengthSet>19.4</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
    "</Column>" +
    @"<Column InternalName=""4"" Priority=""5"" ColumnNum=""4"">" +
      @"<columnName>units</columnName>" +
      @"<columnUIName>Units</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.2</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
    "</Column>" +
    @"<Column InternalName=""5"" Priority=""6"" ColumnNum=""5"">" +
      @"<columnName>amount</columnName>" +
      @"<columnUIName>Amount</columnUIName>" +
      @"<columnType>Currency</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>38.6</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>N</columnRequired>" +
      @"<columnNotNull>N</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
    "</Column>" +
    @"<Column InternalName=""6"" Priority=""7"" ColumnNum=""6"">" +
      @"<columnName>notes</columnName>" +
      @"<columnUIName>Notes</columnUIName>" +
      @"<columnType>String</columnType>" +
      @"<columnDBType>nvarchar</columnDBType>" +
      @"<columnLengthSet>200</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>N</columnRequired>" +
      @"<columnNotNull>N</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
    "</Column>" +
    @"<Column InternalName=""7"" Priority=""8"" ColumnNum=""7"">" +
      @"<columnName>contract_item_type</columnName>" +
      @"<columnUIName>Contract Item Type</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>int</columnDBType>" +
      @"<columnLengthSet>10.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ContractOtherItemWithTypes_contract_item_type_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.ContractItemTypeTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay>name</columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
  "</ColumnDefinition>" +
  @"<TableName>View_ContractOtherItemWithTypes</TableName>" +
  @"<Version>1</Version>" +
  @"<Owner>dbo</Owner>" +
  @"<TableCodeName>View_ContractOtherItemWithTypes</TableCodeName>" +
  @"<TableAliasName>View_ContractOtherItemWithTypes_</TableAliasName>" +
  @"<ConnectionName>Databasewk1</ConnectionName>" +
  @"<canCreateRecords Source=""User"">Y</canCreateRecords>" +
  @"<canEditRecords Source=""User"">Y</canEditRecords>" +
  @"<canDeleteRecords Source=""User"">Y</canDeleteRecords>" +
  @"<canViewRecords Source=""Database"">Y</canViewRecords>" +
  @"<AppShortName>WKCRM</AppShortName>" +
"</XMLDefinition>";
#endregion

	/// <summary>
	/// Gets the embedded schema and configuration data for the  
	/// <see cref="View_ContractOtherItemWithTypesView"></see>
	/// class's TableDefinition.
	/// </summary>
	/// <remarks>This function is only called once at runtime.</remarks>
	/// <returns>An XML string.</returns>
	public static string GetXMLString()
	{
		return _DefinitionString;
	}
}

}
