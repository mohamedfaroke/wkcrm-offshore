﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_OppConversionReportByLocSqlView.cs 

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_OppConversionReportByLocSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_OppConversionReportByLocView"></see> class.
/// </remarks>
/// <seealso cref="View_OppConversionReportByLocView"></seealso>
/// <seealso cref="View_OppConversionReportByLocSqlView"></seealso>
public class BaseView_OppConversionReportByLocSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_OppConversionReportByLocSqlView()
	{
	}

	public BaseView_OppConversionReportByLocSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
