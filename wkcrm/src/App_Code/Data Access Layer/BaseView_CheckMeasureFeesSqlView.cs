﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_CheckMeasureFeesSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_CheckMeasureFeesSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_CheckMeasureFeesView"></see> class.
/// </remarks>
/// <seealso cref="View_CheckMeasureFeesView"></seealso>
/// <seealso cref="View_CheckMeasureFeesSqlView"></seealso>
public class BaseView_CheckMeasureFeesSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_CheckMeasureFeesSqlView()
	{
	}

	public BaseView_CheckMeasureFeesSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
