﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorFinishesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorFinishesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorFinishesTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorFinishesTable"></seealso>
/// <seealso cref="ContractDoorFinishesSqlTable"></seealso>
public class BaseContractDoorFinishesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractDoorFinishesSqlTable()
	{
	}

	public BaseContractDoorFinishesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
