﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopOptionsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopOptionsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopOptionsTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopOptionsTable"></seealso>
/// <seealso cref="ContractBenchtopOptionsSqlTable"></seealso>
public class BaseContractBenchtopOptionsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractBenchtopOptionsSqlTable()
	{
	}

	public BaseContractBenchtopOptionsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
