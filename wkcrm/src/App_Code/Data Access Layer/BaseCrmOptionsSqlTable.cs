﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in CrmOptionsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="CrmOptionsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="CrmOptionsTable"></see> class.
/// </remarks>
/// <seealso cref="CrmOptionsTable"></seealso>
/// <seealso cref="CrmOptionsSqlTable"></seealso>
public class BaseCrmOptionsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseCrmOptionsSqlTable()
	{
	}

	public BaseCrmOptionsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
