﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorFacesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorFacesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorFacesTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorFacesTable"></seealso>
/// <seealso cref="ContractDoorFacesSqlTable"></seealso>
public class BaseContractDoorFacesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractDoorFacesSqlTable()
	{
	}

	public BaseContractDoorFacesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
