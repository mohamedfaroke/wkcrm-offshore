﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EmployeeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="EmployeeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EmployeeTable"></see> class.
/// </remarks>
/// <seealso cref="EmployeeTable"></seealso>
/// <seealso cref="EmployeeSqlTable"></seealso>
public class BaseEmployeeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseEmployeeSqlTable()
	{
	}

	public BaseEmployeeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
