﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in CommunicationSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="CommunicationSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="CommunicationTable"></see> class.
/// </remarks>
/// <seealso cref="CommunicationTable"></seealso>
/// <seealso cref="CommunicationSqlTable"></seealso>
public class BaseCommunicationSqlTable : DynamicSQLServerAdapter
{
	
	public BaseCommunicationSqlTable()
	{
	}

	public BaseCommunicationSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
