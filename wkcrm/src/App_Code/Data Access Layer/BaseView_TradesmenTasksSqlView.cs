﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_TradesmenTasksSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_TradesmenTasksSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_TradesmenTasksView"></see> class.
/// </remarks>
/// <seealso cref="View_TradesmenTasksView"></seealso>
/// <seealso cref="View_TradesmenTasksSqlView"></seealso>
public class BaseView_TradesmenTasksSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_TradesmenTasksSqlView()
	{
	}

	public BaseView_TradesmenTasksSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
