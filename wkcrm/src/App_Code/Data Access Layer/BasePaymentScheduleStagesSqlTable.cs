﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentScheduleStagesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="PaymentScheduleStagesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentScheduleStagesTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentScheduleStagesTable"></seealso>
/// <seealso cref="PaymentScheduleStagesSqlTable"></seealso>
public class BasePaymentScheduleStagesSqlTable : DynamicSQLServerAdapter
{
	
	public BasePaymentScheduleStagesSqlTable()
	{
	}

	public BasePaymentScheduleStagesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
