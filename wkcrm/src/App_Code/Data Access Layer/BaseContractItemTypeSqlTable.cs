﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractItemTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractItemTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractItemTypeTable"></see> class.
/// </remarks>
/// <seealso cref="ContractItemTypeTable"></seealso>
/// <seealso cref="ContractItemTypeSqlTable"></seealso>
public class BaseContractItemTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractItemTypeSqlTable()
	{
	}

	public BaseContractItemTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
