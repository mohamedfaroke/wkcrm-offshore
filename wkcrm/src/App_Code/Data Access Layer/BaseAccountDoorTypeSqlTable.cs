﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountDoorTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="AccountDoorTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AccountDoorTypeTable"></see> class.
/// </remarks>
/// <seealso cref="AccountDoorTypeTable"></seealso>
/// <seealso cref="AccountDoorTypeSqlTable"></seealso>
public class BaseAccountDoorTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseAccountDoorTypeSqlTable()
	{
	}

	public BaseAccountDoorTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
