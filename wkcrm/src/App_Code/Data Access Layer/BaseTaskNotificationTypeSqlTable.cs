﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TaskNotificationTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="TaskNotificationTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TaskNotificationTypeTable"></see> class.
/// </remarks>
/// <seealso cref="TaskNotificationTypeTable"></seealso>
/// <seealso cref="TaskNotificationTypeSqlTable"></seealso>
public class BaseTaskNotificationTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseTaskNotificationTypeSqlTable()
	{
	}

	public BaseTaskNotificationTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
