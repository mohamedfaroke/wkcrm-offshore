﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractOtherItemSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractOtherItemSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractOtherItemTable"></see> class.
/// </remarks>
/// <seealso cref="ContractOtherItemTable"></seealso>
/// <seealso cref="ContractOtherItemSqlTable"></seealso>
public class BaseContractOtherItemSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractOtherItemSqlTable()
	{
	}

	public BaseContractOtherItemSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
