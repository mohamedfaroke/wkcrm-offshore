﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AttachmentTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="AttachmentTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AttachmentTypeTable"></see> class.
/// </remarks>
/// <seealso cref="AttachmentTypeTable"></seealso>
/// <seealso cref="AttachmentTypeSqlTable"></seealso>
public class BaseAttachmentTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseAttachmentTypeSqlTable()
	{
	}

	public BaseAttachmentTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
