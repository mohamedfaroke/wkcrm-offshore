﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractOthersCategory_MiscSqlView.cs 

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractOthersCategory_MiscSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractOthersCategory_MiscView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractOthersCategory_MiscView"></seealso>
/// <seealso cref="View_ContractOthersCategory_MiscSqlView"></seealso>
public class BaseView_ContractOthersCategory_MiscSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ContractOthersCategory_MiscSqlView()
	{
	}

	public BaseView_ContractOthersCategory_MiscSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
