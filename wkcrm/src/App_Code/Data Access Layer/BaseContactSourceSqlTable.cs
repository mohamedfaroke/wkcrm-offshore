﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContactSourceSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContactSourceSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContactSourceTable"></see> class.
/// </remarks>
/// <seealso cref="ContactSourceTable"></seealso>
/// <seealso cref="ContactSourceSqlTable"></seealso>
public class BaseContactSourceSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContactSourceSqlTable()
	{
	}

	public BaseContactSourceSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
