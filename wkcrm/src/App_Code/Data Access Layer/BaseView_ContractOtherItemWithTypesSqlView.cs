﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractOtherItemWithTypesSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractOtherItemWithTypesSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractOtherItemWithTypesView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractOtherItemWithTypesView"></seealso>
/// <seealso cref="View_ContractOtherItemWithTypesSqlView"></seealso>
public class BaseView_ContractOtherItemWithTypesSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ContractOtherItemWithTypesSqlView()
	{
	}

	public BaseView_ContractOtherItemWithTypesSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
