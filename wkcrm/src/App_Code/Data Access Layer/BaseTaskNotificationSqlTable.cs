﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TaskNotificationSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="TaskNotificationSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TaskNotificationTable"></see> class.
/// </remarks>
/// <seealso cref="TaskNotificationTable"></seealso>
/// <seealso cref="TaskNotificationSqlTable"></seealso>
public class BaseTaskNotificationSqlTable : DynamicSQLServerAdapter
{
	
	public BaseTaskNotificationSqlTable()
	{
	}

	public BaseTaskNotificationSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
