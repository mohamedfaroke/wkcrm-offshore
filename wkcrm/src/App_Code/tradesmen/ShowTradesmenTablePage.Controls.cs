﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowTradesmenTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowTradesmenTablePage
{
  

#region "Section 1: Place your customizations here."

    
public class tradesmenTableControlRow : BasetradesmenTableControlRow
{
      
        // The BasetradesmenTableControlRow implements code for a ROW within the
        // the tradesmenTableControl table.  The BasetradesmenTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of tradesmenTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void Delete()
    {
          if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);

            TradesmenRecord rec = TradesmenTable.GetRecord(pk, true);
            rec.status_id = false;
            rec.Save();
          
            ((tradesmenTableControl)MiscUtils.GetParentControlObject(this, "tradesmenTableControl")).DataChanged = true;
            ((tradesmenTableControl)MiscUtils.GetParentControlObject(this, "tradesmenTableControl")).ResetData = true;
    }
}

  

public class tradesmenTableControl : BasetradesmenTableControl
{
        // The BasetradesmenTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The tradesmenTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.
    public override void LoadData()
    {
        if (!this.Page.IsPostBack)
            Populatestatus_idFilter("Yes", 100);
        base.LoadData();
    }
}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the tradesmenTableControlRow control on the ShowTradesmenTablePage page.
// Do not modify this class. Instead override any method in tradesmenTableControlRow.
public class BasetradesmenTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasetradesmenTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in tradesmenTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.tradesmenRecordRowEditButton.Click += new ImageClickEventHandler(tradesmenRecordRowEditButton_Click);
            this.tradesmenRecordRowViewButton.Click += new ImageClickEventHandler(tradesmenRecordRowViewButton_Click);
        }

        // To customize, override this method in tradesmenTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in tradesmenTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = TradesmenTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasetradesmenTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new TradesmenRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in tradesmenTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.entity_type_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.entity_type_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.entity_type_id.Text = formattedValue;
            } else {  
                this.entity_type_id.Text = TradesmenTable.entity_type_id.Format(TradesmenTable.entity_type_id.DefaultValue);
            }
                    
            if (this.entity_type_id.Text == null ||
                this.entity_type_id.Text.Trim().Length == 0) {
                this.entity_type_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.mobile);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = TradesmenTable.mobile.Format(TradesmenTable.mobile.DefaultValue);
            }
                    
            if (this.mobile.Text == null ||
                this.mobile.Text.Trim().Length == 0) {
                this.mobile.Text = "&nbsp;";
            }
                  
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.name);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = TradesmenTable.name.Format(TradesmenTable.name.DefaultValue);
            }
                    
            if (this.name.Text == null ||
                this.name.Text.Trim().Length == 0) {
                this.name.Text = "&nbsp;";
            }
                  
            if (this.DataSource.public_liability_companySpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_company);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.public_liability_company.Text = formattedValue;
            } else {  
                this.public_liability_company.Text = TradesmenTable.public_liability_company.Format(TradesmenTable.public_liability_company.DefaultValue);
            }
                    
            if (this.public_liability_company.Text == null ||
                this.public_liability_company.Text.Trim().Length == 0) {
                this.public_liability_company.Text = "&nbsp;";
            }
                  
            if (this.DataSource.public_liability_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_expiry_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.public_liability_expiry_date.Text = formattedValue;
            } else {  
                this.public_liability_expiry_date.Text = TradesmenTable.public_liability_expiry_date.Format(TradesmenTable.public_liability_expiry_date.DefaultValue);
            }
                    
            if (this.public_liability_expiry_date.Text == null ||
                this.public_liability_expiry_date.Text.Trim().Length == 0) {
                this.public_liability_expiry_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.public_liability_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_number);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.public_liability_number.Text = formattedValue;
            } else {  
                this.public_liability_number.Text = TradesmenTable.public_liability_number.Format(TradesmenTable.public_liability_number.DefaultValue);
            }
                    
            if (this.public_liability_number.Text == null ||
                this.public_liability_number.Text.Trim().Length == 0) {
                this.public_liability_number.Text = "&nbsp;";
            }
                  
            if (this.DataSource.trade_licence_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.trade_licence_expiry_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.trade_licence_expiry_date.Text = formattedValue;
            } else {  
                this.trade_licence_expiry_date.Text = TradesmenTable.trade_licence_expiry_date.Format(TradesmenTable.trade_licence_expiry_date.DefaultValue);
            }
                    
            if (this.trade_licence_expiry_date.Text == null ||
                this.trade_licence_expiry_date.Text.Trim().Length == 0) {
                this.trade_licence_expiry_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.trade_licence_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.trade_licence_number);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.trade_licence_number.Text = formattedValue;
            } else {  
                this.trade_licence_number.Text = TradesmenTable.trade_licence_number.Format(TradesmenTable.trade_licence_number.DefaultValue);
            }
                    
            if (this.trade_licence_number.Text == null ||
                this.trade_licence_number.Text.Trim().Length == 0) {
                this.trade_licence_number.Text = "&nbsp;";
            }
                  
            if (this.DataSource.workers_comp_companySpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_company);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.workers_comp_company.Text = formattedValue;
            } else {  
                this.workers_comp_company.Text = TradesmenTable.workers_comp_company.Format(TradesmenTable.workers_comp_company.DefaultValue);
            }
                    
            if (this.workers_comp_company.Text == null ||
                this.workers_comp_company.Text.Trim().Length == 0) {
                this.workers_comp_company.Text = "&nbsp;";
            }
                  
            if (this.DataSource.workers_comp_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_expiry_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.workers_comp_expiry_date.Text = formattedValue;
            } else {  
                this.workers_comp_expiry_date.Text = TradesmenTable.workers_comp_expiry_date.Format(TradesmenTable.workers_comp_expiry_date.DefaultValue);
            }
                    
            if (this.workers_comp_expiry_date.Text == null ||
                this.workers_comp_expiry_date.Text.Trim().Length == 0) {
                this.workers_comp_expiry_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.workers_comp_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_number);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.workers_comp_number.Text = formattedValue;
            } else {  
                this.workers_comp_number.Text = TradesmenTable.workers_comp_number.Format(TradesmenTable.workers_comp_number.DefaultValue);
            }
                    
            if (this.workers_comp_number.Text == null ||
                this.workers_comp_number.Text.Trim().Length == 0) {
                this.workers_comp_number.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in tradesmenTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in tradesmenTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in tradesmenTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((tradesmenTableControl)MiscUtils.GetParentControlObject(this, "tradesmenTableControl")).DataChanged = true;
                ((tradesmenTableControl)MiscUtils.GetParentControlObject(this, "tradesmenTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in tradesmenTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in tradesmenTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in tradesmenTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            TradesmenTable.DeleteRecord(pk);

          
            ((tradesmenTableControl)MiscUtils.GetParentControlObject(this, "tradesmenTableControl")).DataChanged = true;
            ((tradesmenTableControl)MiscUtils.GetParentControlObject(this, "tradesmenTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void tradesmenRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../tradesmen/EditTradesmenPage.aspx?Tradesmen={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for ImageButton
        public virtual void tradesmenRecordRowViewButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../tradesmen/ShowTradesmenPage.aspx?Tradesmen={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasetradesmenTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasetradesmenTableControlRow_Rec"] = value;
            }
        }
        
        private TradesmenRecord _DataSource;
        public TradesmenRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal entity_type_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal mobile {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
           
        public System.Web.UI.WebControls.Literal name {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
           
        public System.Web.UI.WebControls.Literal public_liability_company {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_company");
            }
        }
           
        public System.Web.UI.WebControls.Literal public_liability_expiry_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_expiry_date");
            }
        }
           
        public System.Web.UI.WebControls.Literal public_liability_number {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_number");
            }
        }
           
        public System.Web.UI.WebControls.Literal trade_licence_expiry_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_expiry_date");
            }
        }
           
        public System.Web.UI.WebControls.Literal trade_licence_number {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_number");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton tradesmenRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenRecordRowEditButton");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox tradesmenRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenRecordRowSelection");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton tradesmenRecordRowViewButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenRecordRowViewButton");
            }
        }
           
        public System.Web.UI.WebControls.Literal workers_comp_company {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_company");
            }
        }
           
        public System.Web.UI.WebControls.Literal workers_comp_expiry_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_expiry_date");
            }
        }
           
        public System.Web.UI.WebControls.Literal workers_comp_number {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_number");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            TradesmenRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public TradesmenRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return TradesmenTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the tradesmenTableControl control on the ShowTradesmenTablePage page.
// Do not modify this class. Instead override any method in tradesmenTableControl.
public class BasetradesmenTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasetradesmenTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.tradesmenPagination.FirstPage.Click += new ImageClickEventHandler(tradesmenPagination_FirstPage_Click);
            this.tradesmenPagination.LastPage.Click += new ImageClickEventHandler(tradesmenPagination_LastPage_Click);
            this.tradesmenPagination.NextPage.Click += new ImageClickEventHandler(tradesmenPagination_NextPage_Click);
            this.tradesmenPagination.PageSizeButton.Button.Click += new EventHandler(tradesmenPagination_PageSizeButton_Click);
            this.tradesmenPagination.PreviousPage.Click += new ImageClickEventHandler(tradesmenPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.nameLabel1.Click += new EventHandler(nameLabel1_Click);

            // Setup the button events.
        
            this.tradesmenDeleteButton.Button.Click += new EventHandler(tradesmenDeleteButton_Click);
            this.tradesmenEditButton.Button.Click += new EventHandler(tradesmenEditButton_Click);
            this.tradesmenExportButton.Button.Click += new EventHandler(tradesmenExportButton_Click);
            this.tradesmenNewButton.Button.Click += new EventHandler(tradesmenNewButton_Click);
            this.tradesmenSearchButton.Button.Click += new EventHandler(tradesmenSearchButton_Click);

            // Setup the filter and search events.
        
            this.entity_type_idFilter.SelectedIndexChanged += new EventHandler(entity_type_idFilter_SelectedIndexChanged);
            this.status_idFilter.SelectedIndexChanged += new EventHandler(status_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.entity_type_idFilter)) {
                this.entity_type_idFilter.Items.Add(new ListItem(this.GetFromSession(this.entity_type_idFilter), this.GetFromSession(this.entity_type_idFilter)));
                this.entity_type_idFilter.SelectedValue = this.GetFromSession(this.entity_type_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.status_idFilter)) {
                this.status_idFilter.Items.Add(new ListItem(this.GetFromSession(this.status_idFilter), this.GetFromSession(this.status_idFilter)));
                this.status_idFilter.SelectedValue = this.GetFromSession(this.status_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.tradesmenSearchArea)) {
                
                this.tradesmenSearchArea.Text = this.GetFromSession(this.tradesmenSearchArea);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(TradesmenTable.name, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.tradesmenDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (TradesmenRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.TradesmenRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = TradesmenTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (TradesmenRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.TradesmenRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (tradesmenTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (TradesmenRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.TradesmenRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = TradesmenTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateentity_type_idFilter(MiscUtils.GetSelectedValue(this.entity_type_idFilter, this.GetFromSession(this.entity_type_idFilter)), 500);
            this.Populatestatus_idFilter(MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("tradesmenTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                tradesmenTableControlRow recControl = (tradesmenTableControlRow)(repItem.FindControl("tradesmenTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(TradesmenTable.entity_type_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for tradesmenTableControl pagination.
        
            this.tradesmenPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.tradesmenPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.tradesmenPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.tradesmenPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.tradesmenPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.tradesmenPagination.CurrentPage.Text = "0";
            }
            this.tradesmenPagination.PageSize.Text = this.PageSize.ToString();
            this.tradesmenTotalItems.Text = this.TotalRecords.ToString();
            this.tradesmenPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.tradesmenPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (tradesmenTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            TradesmenTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.entity_type_idFilter)) {
                wc.iAND(TradesmenTable.entity_type_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.entity_type_idFilter, this.GetFromSession(this.entity_type_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.status_idFilter)) {
                wc.iAND(TradesmenTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.tradesmenSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(TradesmenTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.tradesmenSearchArea, this.GetFromSession(this.tradesmenSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.tradesmenPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.tradesmenPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("tradesmenTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    tradesmenTableControlRow recControl = (tradesmenTableControlRow)(repItem.FindControl("tradesmenTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        TradesmenRecord rec = new TradesmenRecord();
        
                        if (recControl.entity_type_id.Text != "") {
                            rec.Parse(recControl.entity_type_id.Text, TradesmenTable.entity_type_id);
                        }
                        if (recControl.mobile.Text != "") {
                            rec.Parse(recControl.mobile.Text, TradesmenTable.mobile);
                        }
                        if (recControl.name.Text != "") {
                            rec.Parse(recControl.name.Text, TradesmenTable.name);
                        }
                        if (recControl.public_liability_company.Text != "") {
                            rec.Parse(recControl.public_liability_company.Text, TradesmenTable.public_liability_company);
                        }
                        if (recControl.public_liability_expiry_date.Text != "") {
                            rec.Parse(recControl.public_liability_expiry_date.Text, TradesmenTable.public_liability_expiry_date);
                        }
                        if (recControl.public_liability_number.Text != "") {
                            rec.Parse(recControl.public_liability_number.Text, TradesmenTable.public_liability_number);
                        }
                        if (recControl.trade_licence_expiry_date.Text != "") {
                            rec.Parse(recControl.trade_licence_expiry_date.Text, TradesmenTable.trade_licence_expiry_date);
                        }
                        if (recControl.trade_licence_number.Text != "") {
                            rec.Parse(recControl.trade_licence_number.Text, TradesmenTable.trade_licence_number);
                        }
                        if (recControl.workers_comp_company.Text != "") {
                            rec.Parse(recControl.workers_comp_company.Text, TradesmenTable.workers_comp_company);
                        }
                        if (recControl.workers_comp_expiry_date.Text != "") {
                            rec.Parse(recControl.workers_comp_expiry_date.Text, TradesmenTable.workers_comp_expiry_date);
                        }
                        if (recControl.workers_comp_number.Text != "") {
                            rec.Parse(recControl.workers_comp_number.Text, TradesmenTable.workers_comp_number);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new TradesmenRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (TradesmenRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.TradesmenRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(tradesmenTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(tradesmenTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for entity_type_idFilter.
        protected virtual void Populateentity_type_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TradeEntityTypesTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.entity_type_idFilter.Items.Clear();
            foreach (TradeEntityTypesRecord itemValue in TradeEntityTypesTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(TradeEntityTypesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.entity_type_idFilter.Items.IndexOf(item) < 0) {
                    this.entity_type_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.entity_type_idFilter, selectedValue);

            // Add the All item.
            this.entity_type_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for status_idFilter.
        protected virtual void Populatestatus_idFilter(string selectedValue, int maxItems)
        {
              
            // Setup the WHERE clause, including the base table if needed.
                
            WhereClause wc = new WhereClause();
                  
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TradesmenTable.status_id, OrderByItem.OrderDir.Asc);

            string[] list = TradesmenTable.GetValues(TradesmenTable.status_id, wc, orderBy, maxItems);
            
            this.status_idFilter.Items.Clear();
            foreach (string itemValue in list)
            {
                // Create the item and add to the list.
                string fvalue = TradesmenTable.status_id.Format(itemValue);
                ListItem item = new ListItem(fvalue, itemValue);
                this.status_idFilter.Items.Add(item);
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.status_idFilter, selectedValue);

            // Add the All item.
            this.status_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter entity_type_idFilter.
        public virtual WhereClause CreateWhereClause_entity_type_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.status_idFilter)) {
                wc.iAND(TradesmenTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.tradesmenSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(TradesmenTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.tradesmenSearchArea, this.GetFromSession(this.tradesmenSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter status_idFilter.
        public virtual WhereClause CreateWhereClause_status_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.entity_type_idFilter)) {
                wc.iAND(TradesmenTable.entity_type_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.entity_type_idFilter, this.GetFromSession(this.entity_type_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.tradesmenSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(TradesmenTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.tradesmenSearchArea, this.GetFromSession(this.tradesmenSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter tradesmenSearchArea.
        public virtual WhereClause CreateWhereClause_tradesmenSearchArea()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.entity_type_idFilter)) {
                wc.iAND(TradesmenTable.entity_type_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.entity_type_idFilter, this.GetFromSession(this.entity_type_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.status_idFilter)) {
                wc.iAND(TradesmenTable.status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.status_idFilter, this.GetFromSession(this.status_idFilter)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.entity_type_idFilter, this.entity_type_idFilter.SelectedValue);
            this.SaveToSession(this.status_idFilter, this.status_idFilter.SelectedValue);
            this.SaveToSession(this.tradesmenSearchArea, this.tradesmenSearchArea.Text);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.entity_type_idFilter);
            this.RemoveFromSession(this.status_idFilter);
            this.RemoveFromSession(this.tradesmenSearchArea);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["tradesmenTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["tradesmenTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void tradesmenPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void tradesmenPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void tradesmenPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void tradesmenPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void tradesmenPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void nameLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(TradesmenTable.name);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(TradesmenTable.name, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void tradesmenDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void tradesmenEditButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../tradesmen/EditTradesmenPage.aspx?Tradesmen={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void tradesmenExportButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            WhereClause wc = this.CreateWhereClause();
            String exportedData = TradesmenTable.Export(wc);
            BaseClasses.Utils.NetUtils.WriteResponseTextAttachment(this.Page.Response, "TradesmenTable.csv", exportedData);
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void tradesmenNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../tradesmen/AddTradesmenPage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void tradesmenSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void entity_type_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void status_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private TradesmenRecord[] _DataSource = null;
        public  TradesmenRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.DropDownList entity_type_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal entity_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal entity_type_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton nameLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal public_liability_companyLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_companyLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal public_liability_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_expiry_dateLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal public_liability_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_numberLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal status_id1Label {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_id1Label");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList status_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal trade_licence_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_expiry_dateLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal trade_licence_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_numberLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton tradesmenDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton tradesmenEditButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenEditButton");
            }
        }
        
        public WKCRM.UI.IThemeButton tradesmenExportButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenExportButton");
            }
        }
        
        public WKCRM.UI.IThemeButton tradesmenNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenNewButton");
            }
        }
        
        public WKCRM.UI.IPagination tradesmenPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenPagination");
            }
        }
        
        public System.Web.UI.WebControls.TextBox tradesmenSearchArea {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenSearchArea");
            }
        }
        
        public WKCRM.UI.IThemeButton tradesmenSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal tradesmenTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox tradesmenToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label tradesmenTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal workers_comp_companyLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_companyLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal workers_comp_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_expiry_dateLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal workers_comp_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_numberLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                tradesmenTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                TradesmenRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (tradesmenTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.tradesmenRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public tradesmenTableControlRow GetSelectedRecordControl()
        {
        tradesmenTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public tradesmenTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (tradesmenTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.tradesmenRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (tradesmenTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowTradesmenTablePage.tradesmenTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            tradesmenTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (tradesmenTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.tradesmenRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public tradesmenTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("tradesmenTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                tradesmenTableControlRow recControl = (tradesmenTableControlRow)repItem.FindControl("tradesmenTableControlRow");
                recList.Add(recControl);
            }

            return (tradesmenTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowTradesmenTablePage.tradesmenTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  