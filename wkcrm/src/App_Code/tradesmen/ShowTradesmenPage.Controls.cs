﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowTradesmenPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowTradesmenPage
{
  

#region "Section 1: Place your customizations here."

    
public class tradesmenRecordControl : BasetradesmenRecordControl
{
      
        // The BasetradesmenRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the tradesmenRecordControl control on the ShowTradesmenPage page.
// Do not modify this class. Instead override any method in tradesmenRecordControl.
public class BasetradesmenRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BasetradesmenRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in tradesmenRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in tradesmenRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in tradesmenRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = TradesmenTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new TradesmenRecord();
                return;
            }

            // Retrieve the record from the database.
            TradesmenRecord[] recList = TradesmenTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = TradesmenTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in tradesmenRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.address);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.TradesmenTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"address\\\", \\\"Address\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = TradesmenTable.address.Format(TradesmenTable.address.DefaultValue);
            }
                    
            if (this.address.Text == null ||
                this.address.Text.Trim().Length == 0) {
                this.address.Text = "&nbsp;";
            }
                  
            if (this.DataSource.dobSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.dob);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.dob.Text = formattedValue;
            } else {  
                this.dob.Text = TradesmenTable.dob.Format(TradesmenTable.dob.DefaultValue);
            }
                    
            if (this.dob.Text == null ||
                this.dob.Text.Trim().Length == 0) {
                this.dob.Text = "&nbsp;";
            }
                  
            if (this.DataSource.emailSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.email);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.TradesmenTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"email\\\", \\\"Email\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.email.Text = formattedValue;
            } else {  
                this.email.Text = TradesmenTable.email.Format(TradesmenTable.email.DefaultValue);
            }
                    
            if (this.email.Text == null ||
                this.email.Text.Trim().Length == 0) {
                this.email.Text = "&nbsp;";
            }
                  
            if (this.DataSource.entity_type_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.entity_type_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.entity_type_id.Text = formattedValue;
            } else {  
                this.entity_type_id.Text = TradesmenTable.entity_type_id.Format(TradesmenTable.entity_type_id.DefaultValue);
            }
                    
            if (this.entity_type_id.Text == null ||
                this.entity_type_id.Text.Trim().Length == 0) {
                this.entity_type_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.faxSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.fax);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.fax.Text = formattedValue;
            } else {  
                this.fax.Text = TradesmenTable.fax.Format(TradesmenTable.fax.DefaultValue);
            }
                    
            if (this.fax.Text == null ||
                this.fax.Text.Trim().Length == 0) {
                this.fax.Text = "&nbsp;";
            }
                  
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.mobile);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = TradesmenTable.mobile.Format(TradesmenTable.mobile.DefaultValue);
            }
                    
            if (this.mobile.Text == null ||
                this.mobile.Text.Trim().Length == 0) {
                this.mobile.Text = "&nbsp;";
            }
                  
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.name);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = TradesmenTable.name.Format(TradesmenTable.name.DefaultValue);
            }
                    
            if (this.name.Text == null ||
                this.name.Text.Trim().Length == 0) {
                this.name.Text = "&nbsp;";
            }
                  
            if (this.DataSource.phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.phone);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.phone.Text = formattedValue;
            } else {  
                this.phone.Text = TradesmenTable.phone.Format(TradesmenTable.phone.DefaultValue);
            }
                    
            if (this.phone.Text == null ||
                this.phone.Text.Trim().Length == 0) {
                this.phone.Text = "&nbsp;";
            }
                  
            if (this.DataSource.public_liability_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_expiry_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.public_liability_expiry_date.Text = formattedValue;
            } else {  
                this.public_liability_expiry_date.Text = TradesmenTable.public_liability_expiry_date.Format(TradesmenTable.public_liability_expiry_date.DefaultValue);
            }
                    
            if (this.public_liability_expiry_date.Text == null ||
                this.public_liability_expiry_date.Text.Trim().Length == 0) {
                this.public_liability_expiry_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.public_liability_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_number);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.public_liability_number.Text = formattedValue;
            } else {  
                this.public_liability_number.Text = TradesmenTable.public_liability_number.Format(TradesmenTable.public_liability_number.DefaultValue);
            }
                    
            if (this.public_liability_number.Text == null ||
                this.public_liability_number.Text.Trim().Length == 0) {
                this.public_liability_number.Text = "&nbsp;";
            }
                  
            if (this.DataSource.send_email_notificationsSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.send_email_notifications);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.send_email_notifications.Text = formattedValue;
            } else {  
                this.send_email_notifications.Text = TradesmenTable.send_email_notifications.Format(TradesmenTable.send_email_notifications.DefaultValue);
            }
                    
            if (this.send_email_notifications.Text == null ||
                this.send_email_notifications.Text.Trim().Length == 0) {
                this.send_email_notifications.Text = "&nbsp;";
            }
                  
            if (this.DataSource.status_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.status_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.status_id.Text = formattedValue;
            } else {  
                this.status_id.Text = TradesmenTable.status_id.Format(TradesmenTable.status_id.DefaultValue);
            }
                    
            if (this.status_id.Text == null ||
                this.status_id.Text.Trim().Length == 0) {
                this.status_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.trade_licence_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.trade_licence_expiry_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.trade_licence_expiry_date.Text = formattedValue;
            } else {  
                this.trade_licence_expiry_date.Text = TradesmenTable.trade_licence_expiry_date.Format(TradesmenTable.trade_licence_expiry_date.DefaultValue);
            }
                    
            if (this.trade_licence_expiry_date.Text == null ||
                this.trade_licence_expiry_date.Text.Trim().Length == 0) {
                this.trade_licence_expiry_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.trade_licence_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.trade_licence_number);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.trade_licence_number.Text = formattedValue;
            } else {  
                this.trade_licence_number.Text = TradesmenTable.trade_licence_number.Format(TradesmenTable.trade_licence_number.DefaultValue);
            }
                    
            if (this.trade_licence_number.Text == null ||
                this.trade_licence_number.Text.Trim().Length == 0) {
                this.trade_licence_number.Text = "&nbsp;";
            }
                  
            if (this.DataSource.workers_comp_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_expiry_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.workers_comp_expiry_date.Text = formattedValue;
            } else {  
                this.workers_comp_expiry_date.Text = TradesmenTable.workers_comp_expiry_date.Format(TradesmenTable.workers_comp_expiry_date.DefaultValue);
            }
                    
            if (this.workers_comp_expiry_date.Text == null ||
                this.workers_comp_expiry_date.Text.Trim().Length == 0) {
                this.workers_comp_expiry_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.workers_comp_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_number);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.workers_comp_number.Text = formattedValue;
            } else {  
                this.workers_comp_number.Text = TradesmenTable.workers_comp_number.Format(TradesmenTable.workers_comp_number.DefaultValue);
            }
                    
            if (this.workers_comp_number.Text == null ||
                this.workers_comp_number.Text.Trim().Length == 0) {
                this.workers_comp_number.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in tradesmenRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in tradesmenRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in tradesmenRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in tradesmenRecordControl.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in tradesmenRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            TradesmenTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["Tradesmen"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "Tradesmen"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(TradesmenTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(TradesmenTable.id0).ToString());
            } else {
                
                wc.iAND(TradesmenTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in tradesmenRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            TradesmenTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasetradesmenRecordControl_Rec"];
            }
            set {
                this.ViewState["BasetradesmenRecordControl_Rec"] = value;
            }
        }
        
        private TradesmenRecord _DataSource;
        public TradesmenRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal address {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal dob {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dob");
            }
        }
        
        public System.Web.UI.WebControls.Literal dobLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dobLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal email {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email");
            }
        }
        
        public System.Web.UI.WebControls.Literal emailLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "emailLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal entity_type_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal entity_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal fax {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "fax");
            }
        }
        
        public System.Web.UI.WebControls.Literal faxLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "faxLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal mobile {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal name {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
        
        public System.Web.UI.WebControls.Literal nameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal phone {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal public_liability_expiry_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_expiry_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal public_liability_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_expiry_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal public_liability_number {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_number");
            }
        }
        
        public System.Web.UI.WebControls.Literal public_liability_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_numberLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal send_email_notifications {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "send_email_notifications");
            }
        }
        
        public System.Web.UI.WebControls.Literal send_email_notificationsLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "send_email_notificationsLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal status_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal status_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal trade_licence_expiry_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_expiry_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal trade_licence_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_expiry_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal trade_licence_number {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_number");
            }
        }
        
        public System.Web.UI.WebControls.Literal trade_licence_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_numberLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image tradesmenDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal tradesmenDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.Literal workers_comp_expiry_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_expiry_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal workers_comp_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_expiry_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal workers_comp_number {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_number");
            }
        }
        
        public System.Web.UI.WebControls.Literal workers_comp_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_numberLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            TradesmenRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public TradesmenRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return TradesmenTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  