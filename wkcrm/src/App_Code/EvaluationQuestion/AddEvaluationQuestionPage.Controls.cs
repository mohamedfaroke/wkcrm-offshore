﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// AddEvaluationQuestionPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.AddEvaluationQuestionPage
{
  

#region "Section 1: Place your customizations here."

    
public class EvaluationQuestionOptionsTableControlRow : BaseEvaluationQuestionOptionsTableControlRow
{
      
        // The BaseEvaluationQuestionOptionsTableControlRow implements code for a ROW within the
        // the EvaluationQuestionOptionsTableControl table.  The BaseEvaluationQuestionOptionsTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of EvaluationQuestionOptionsTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class EvaluationQuestionOptionsTableControl : BaseEvaluationQuestionOptionsTableControl
{
        // The BaseEvaluationQuestionOptionsTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The EvaluationQuestionOptionsTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  
public class EvaluationQuestionRecordControl : BaseEvaluationQuestionRecordControl
{
      
        // The BaseEvaluationQuestionRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the EvaluationQuestionOptionsTableControlRow control on the AddEvaluationQuestionPage page.
// Do not modify this class. Instead override any method in EvaluationQuestionOptionsTableControlRow.
public class BaseEvaluationQuestionOptionsTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseEvaluationQuestionOptionsTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EvaluationQuestionOptionsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseEvaluationQuestionOptionsTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new EvaluationQuestionOptionsRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.OptionIdSpecified) {
                this.PopulateOptionIdDropDownList(this.DataSource.OptionId.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateOptionIdDropDownList(EvaluationQuestionOptionsTable.OptionId.DefaultValue, 100);
                } else {
                this.PopulateOptionIdDropDownList(null, 100);
                }
            }
                
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // EvaluationQuestion in EvaluationQuestionRecordControl is One To Many to EvaluationQuestionOptionsTableControl.
                    
            // Setup the parent id in the record.
            EvaluationQuestionRecordControl recEvaluationQuestionRecordControl = (EvaluationQuestionRecordControl)this.Page.FindControlRecursively("EvaluationQuestionRecordControl");
            if (recEvaluationQuestionRecordControl != null && recEvaluationQuestionRecordControl.DataSource == null) {
                // Load the record if it is not loaded yet.
                recEvaluationQuestionRecordControl.LoadData();
            }
            if (recEvaluationQuestionRecordControl == null || recEvaluationQuestionRecordControl.DataSource == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:NoParentRecId", "WKCRM"));
            }
                    
            this.DataSource.QuestionId = recEvaluationQuestionRecordControl.DataSource.QuestionID;
            
            // 2. Validate the data.  Override in EvaluationQuestionOptionsTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in EvaluationQuestionOptionsTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((EvaluationQuestionOptionsTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionOptionsTableControl")).DataChanged = true;
                ((EvaluationQuestionOptionsTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionOptionsTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.OptionId), EvaluationQuestionOptionsTable.OptionId);
                  
        }

        //  To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in EvaluationQuestionOptionsTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EvaluationQuestionOptionsTable.DeleteRecord(pk);

          
            ((EvaluationQuestionOptionsTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionOptionsTableControl")).DataChanged = true;
            ((EvaluationQuestionOptionsTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionOptionsTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_OptionIdDropDownList() {
            return new WhereClause();
        }
                
        // Fill the OptionId list.
        protected virtual void PopulateOptionIdDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_OptionIdDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(QuestionOptionTable.OptionText, OrderByItem.OrderDir.Asc);

                      this.OptionId.Items.Clear();
            foreach (QuestionOptionRecord itemValue in QuestionOptionTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.OptionIDSpecified) {
                    cvalue = itemValue.OptionID.ToString();
                    fvalue = itemValue.Format(QuestionOptionTable.OptionText);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.OptionId.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.OptionId, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.OptionId, EvaluationQuestionOptionsTable.OptionId.Format(selectedValue))) {
                string fvalue = EvaluationQuestionOptionsTable.OptionId.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.OptionId.Items.Insert(0, item);
            }

                  
            this.OptionId.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseEvaluationQuestionOptionsTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseEvaluationQuestionOptionsTableControlRow_Rec"] = value;
            }
        }
        
        private EvaluationQuestionOptionsRecord _DataSource;
        public EvaluationQuestionOptionsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.CheckBox EvaluationQuestionOptionsRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList OptionId {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionId");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EvaluationQuestionOptionsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EvaluationQuestionOptionsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return EvaluationQuestionOptionsTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the EvaluationQuestionOptionsTableControl control on the AddEvaluationQuestionPage page.
// Do not modify this class. Instead override any method in EvaluationQuestionOptionsTableControl.
public class BaseEvaluationQuestionOptionsTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseEvaluationQuestionOptionsTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.EvaluationQuestionOptionsPagination.FirstPage.Click += new ImageClickEventHandler(EvaluationQuestionOptionsPagination_FirstPage_Click);
            this.EvaluationQuestionOptionsPagination.LastPage.Click += new ImageClickEventHandler(EvaluationQuestionOptionsPagination_LastPage_Click);
            this.EvaluationQuestionOptionsPagination.NextPage.Click += new ImageClickEventHandler(EvaluationQuestionOptionsPagination_NextPage_Click);
            this.EvaluationQuestionOptionsPagination.PageSizeButton.Button.Click += new EventHandler(EvaluationQuestionOptionsPagination_PageSizeButton_Click);
            this.EvaluationQuestionOptionsPagination.PreviousPage.Click += new ImageClickEventHandler(EvaluationQuestionOptionsPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.OptionIdLabel1.Click += new EventHandler(OptionIdLabel1_Click);

            // Setup the button events.
        
            this.EvaluationQuestionOptionsAddButton.Button.Click += new EventHandler(EvaluationQuestionOptionsAddButton_Click);
            this.EvaluationQuestionOptionsDeleteButton.Button.Click += new EventHandler(EvaluationQuestionOptionsDeleteButton_Click);
            this.EvaluationQuestionOptionsExportButton.Button.Click += new EventHandler(EvaluationQuestionOptionsExportButton_Click);

            // Setup the filter and search events.
        
            this.OptionIdFilter.SelectedIndexChanged += new EventHandler(OptionIdFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.OptionIdFilter)) {
                this.OptionIdFilter.Items.Add(new ListItem(this.GetFromSession(this.OptionIdFilter), this.GetFromSession(this.OptionIdFilter)));
                this.OptionIdFilter.SelectedValue = this.GetFromSession(this.OptionIdFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.EvaluationQuestionOptionsDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EvaluationQuestionOptionsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionOptionsRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = EvaluationQuestionOptionsTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EvaluationQuestionOptionsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionOptionsRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (EvaluationQuestionOptionsTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (EvaluationQuestionOptionsRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionOptionsRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = EvaluationQuestionOptionsTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.PopulateOptionIdFilter(MiscUtils.GetSelectedValue(this.OptionIdFilter, this.GetFromSession(this.OptionIdFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("EvaluationQuestionOptionsTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                EvaluationQuestionOptionsTableControlRow recControl = (EvaluationQuestionOptionsTableControlRow)(repItem.FindControl("EvaluationQuestionOptionsTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(EvaluationQuestionOptionsTable.OptionId, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for EvaluationQuestionOptionsTableControl pagination.
        
            this.EvaluationQuestionOptionsPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.EvaluationQuestionOptionsPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.EvaluationQuestionOptionsPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.EvaluationQuestionOptionsPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.EvaluationQuestionOptionsPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.EvaluationQuestionOptionsPagination.CurrentPage.Text = "0";
            }
            this.EvaluationQuestionOptionsPagination.PageSize.Text = this.PageSize.ToString();
            this.EvaluationQuestionOptionsTotalItems.Text = this.TotalRecords.ToString();
            this.EvaluationQuestionOptionsPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.EvaluationQuestionOptionsPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (EvaluationQuestionOptionsTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            EvaluationQuestionOptionsTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        EvaluationQuestionRecordControl parentRecordControl = (EvaluationQuestionRecordControl)(this.Page.FindControlRecursively("EvaluationQuestionRecordControl"));
            EvaluationQuestionRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
          
            if (parentRec.QuestionIDSpecified) {
                wc.iAND(EvaluationQuestionOptionsTable.QuestionId, BaseFilter.ComparisonOperator.EqualsTo, parentRec.QuestionID.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            if (MiscUtils.IsValueSelected(this.OptionIdFilter)) {
                wc.iAND(EvaluationQuestionOptionsTable.OptionId, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.OptionIdFilter, this.GetFromSession(this.OptionIdFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.EvaluationQuestionOptionsPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.EvaluationQuestionOptionsPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("EvaluationQuestionOptionsTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    EvaluationQuestionOptionsTableControlRow recControl = (EvaluationQuestionOptionsTableControlRow)(repItem.FindControl("EvaluationQuestionOptionsTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        EvaluationQuestionOptionsRecord rec = new EvaluationQuestionOptionsRecord();
        
                        if (MiscUtils.IsValueSelected(recControl.OptionId)) {
                            rec.Parse(recControl.OptionId.SelectedItem.Value, EvaluationQuestionOptionsTable.OptionId);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new EvaluationQuestionOptionsRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (EvaluationQuestionOptionsRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionOptionsRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(EvaluationQuestionOptionsTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(EvaluationQuestionOptionsTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for OptionIdFilter.
        protected virtual void PopulateOptionIdFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(QuestionOptionTable.OptionText, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.OptionIdFilter.Items.Clear();
            foreach (QuestionOptionRecord itemValue in QuestionOptionTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.OptionIDSpecified) {
                    cvalue = itemValue.OptionID.ToString();
                    fvalue = itemValue.Format(QuestionOptionTable.OptionText);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.OptionIdFilter.Items.IndexOf(item) < 0) {
                    this.OptionIdFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.OptionIdFilter, selectedValue);

            // Add the All item.
            this.OptionIdFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter OptionIdFilter.
        public virtual WhereClause CreateWhereClause_OptionIdFilter()
        {
              
            WhereClause wc = new WhereClause();
                  EvaluationQuestionRecordControl parentRecordControl = (EvaluationQuestionRecordControl)this.Page.FindControlRecursively("EvaluationQuestionRecordControl");
            EvaluationQuestionRecord parentRec = parentRecordControl.GetRecord();
            if (parentRec == null) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:ParentNotInitialized", "WKCRM"));
            }
                    
            if (parentRec.QuestionIDSpecified) {
                wc.iAND(EvaluationQuestionOptionsTable.QuestionId, BaseFilter.ComparisonOperator.EqualsTo, parentRec.QuestionID.ToString());
            } else {
                wc.RunQuery = false;
                return wc;
            }
            
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.OptionIdFilter, this.OptionIdFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.OptionIdFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["EvaluationQuestionOptionsTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["EvaluationQuestionOptionsTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void EvaluationQuestionOptionsPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void EvaluationQuestionOptionsPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void EvaluationQuestionOptionsPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionOptionsPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void EvaluationQuestionOptionsPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void OptionIdLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(EvaluationQuestionOptionsTable.OptionId);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(EvaluationQuestionOptionsTable.OptionId, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void EvaluationQuestionOptionsAddButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            this.AddNewRecord = 1;
            this.DataChanged = true;
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionOptionsDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(true);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionOptionsExportButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            WhereClause wc = this.CreateWhereClause();
            String exportedData = EvaluationQuestionOptionsTable.Export(wc);
            BaseClasses.Utils.NetUtils.WriteResponseTextAttachment(this.Page.Response, "EvaluationQuestionOptionsTable.csv", exportedData);
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void OptionIdFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private EvaluationQuestionOptionsRecord[] _DataSource = null;
        public  EvaluationQuestionOptionsRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public WKCRM.UI.IThemeButton EvaluationQuestionOptionsAddButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsAddButton");
            }
        }
        
        public WKCRM.UI.IThemeButton EvaluationQuestionOptionsDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton EvaluationQuestionOptionsExportButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsExportButton");
            }
        }
        
        public WKCRM.UI.IPagination EvaluationQuestionOptionsPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsPagination");
            }
        }
        
        public System.Web.UI.WebControls.Literal EvaluationQuestionOptionsTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox EvaluationQuestionOptionsToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label EvaluationQuestionOptionsTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList OptionIdFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionIdFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal OptionIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionIdLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton OptionIdLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionIdLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                EvaluationQuestionOptionsTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                EvaluationQuestionOptionsRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (EvaluationQuestionOptionsTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.EvaluationQuestionOptionsRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public EvaluationQuestionOptionsTableControlRow GetSelectedRecordControl()
        {
        EvaluationQuestionOptionsTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public EvaluationQuestionOptionsTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (EvaluationQuestionOptionsTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.EvaluationQuestionOptionsRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (EvaluationQuestionOptionsTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.AddEvaluationQuestionPage.EvaluationQuestionOptionsTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            EvaluationQuestionOptionsTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (EvaluationQuestionOptionsTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.EvaluationQuestionOptionsRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public EvaluationQuestionOptionsTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("EvaluationQuestionOptionsTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                EvaluationQuestionOptionsTableControlRow recControl = (EvaluationQuestionOptionsTableControlRow)repItem.FindControl("EvaluationQuestionOptionsTableControlRow");
                recList.Add(recControl);
            }

            return (EvaluationQuestionOptionsTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.AddEvaluationQuestionPage.EvaluationQuestionOptionsTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  
// Base class for the EvaluationQuestionRecordControl control on the AddEvaluationQuestionPage page.
// Do not modify this class. Instead override any method in EvaluationQuestionRecordControl.
public class BaseEvaluationQuestionRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseEvaluationQuestionRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in EvaluationQuestionRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in EvaluationQuestionRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in EvaluationQuestionRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EvaluationQuestionTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new EvaluationQuestionRecord();
                return;
            }

            // Retrieve the record from the database.
            EvaluationQuestionRecord[] recList = EvaluationQuestionTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = EvaluationQuestionTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in EvaluationQuestionRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.associated_taskSpecified) {
                this.Populateassociated_taskDropDownList(this.DataSource.associated_task.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateassociated_taskDropDownList(EvaluationQuestionTable.associated_task.DefaultValue, 100);
                } else {
                this.Populateassociated_taskDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.EvaluationIDSpecified) {
                this.PopulateEvaluationIDDropDownList(this.DataSource.EvaluationID.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateEvaluationIDDropDownList(EvaluationQuestionTable.EvaluationID.DefaultValue, 100);
                } else {
                this.PopulateEvaluationIDDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.OrderNumberSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionTable.OrderNumber);
                this.OrderNumber.Text = formattedValue;
            } else {  
                this.OrderNumber.Text = EvaluationQuestionTable.OrderNumber.Format(EvaluationQuestionTable.OrderNumber.DefaultValue);
            }
                    
            if (this.DataSource.QuestionSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionTable.Question);
                this.Question.Text = formattedValue;
            } else {  
                this.Question.Text = EvaluationQuestionTable.Question.Format(EvaluationQuestionTable.Question.DefaultValue);
            }
                    
            if (this.DataSource.TypeIDSpecified) {
                this.PopulateTypeIDDropDownList(this.DataSource.TypeID.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateTypeIDDropDownList(EvaluationQuestionTable.TypeID.DefaultValue, 100);
                } else {
                this.PopulateTypeIDDropDownList(null, 100);
                }
            }
                
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in EvaluationQuestionRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in EvaluationQuestionRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in EvaluationQuestionRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in EvaluationQuestionRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.associated_task), EvaluationQuestionTable.associated_task);
                  
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.EvaluationID), EvaluationQuestionTable.EvaluationID);
                  
            this.DataSource.Parse(this.OrderNumber.Text, EvaluationQuestionTable.OrderNumber);
                          
            this.DataSource.Parse(this.Question.Text, EvaluationQuestionTable.Question);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.TypeID), EvaluationQuestionTable.TypeID);
                  
        }

        //  To customize, override this method in EvaluationQuestionRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in EvaluationQuestionRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EvaluationQuestionTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_associated_taskDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_EvaluationIDDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_TypeIDDropDownList() {
            return new WhereClause();
        }
                
        // Fill the associated_task list.
        protected virtual void Populateassociated_taskDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_associated_taskDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TradeTaskTypesTable.name, OrderByItem.OrderDir.Asc);

                      this.associated_task.Items.Clear();
            foreach (TradeTaskTypesRecord itemValue in TradeTaskTypesTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(TradeTaskTypesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.associated_task.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.associated_task, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.associated_task, EvaluationQuestionTable.associated_task.Format(selectedValue))) {
                string fvalue = EvaluationQuestionTable.associated_task.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.associated_task.Items.Insert(0, item);
            }

                  
            this.associated_task.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the EvaluationID list.
        protected virtual void PopulateEvaluationIDDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_EvaluationIDDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EvaluationsTable.EvaluationName, OrderByItem.OrderDir.Asc);

                      this.EvaluationID.Items.Clear();
            foreach (EvaluationsRecord itemValue in EvaluationsTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.EvaluationIDSpecified) {
                    cvalue = itemValue.EvaluationID.ToString();
                    fvalue = itemValue.Format(EvaluationsTable.EvaluationName);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.EvaluationID.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.EvaluationID, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.EvaluationID, EvaluationQuestionTable.EvaluationID.Format(selectedValue))) {
                string fvalue = EvaluationQuestionTable.EvaluationID.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.EvaluationID.Items.Insert(0, item);
            }

                  
            this.EvaluationID.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the TypeID list.
        protected virtual void PopulateTypeIDDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_TypeIDDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(QuestionTypeTable.Type0, OrderByItem.OrderDir.Asc);

                      this.TypeID.Items.Clear();
            foreach (QuestionTypeRecord itemValue in QuestionTypeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.typeIdSpecified) {
                    cvalue = itemValue.typeId.ToString();
                    fvalue = itemValue.Format(QuestionTypeTable.Type0);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.TypeID.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.TypeID, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.TypeID, EvaluationQuestionTable.TypeID.Format(selectedValue))) {
                string fvalue = EvaluationQuestionTable.TypeID.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.TypeID.Items.Insert(0, item);
            }

                  
            this.TypeID.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseEvaluationQuestionRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseEvaluationQuestionRecordControl_Rec"] = value;
            }
        }
        
        private EvaluationQuestionRecord _DataSource;
        public EvaluationQuestionRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.DropDownList associated_task {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "associated_task");
            }
        }
        
        public System.Web.UI.WebControls.Literal associated_taskLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "associated_taskLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList EvaluationID {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationID");
            }
        }
        
        public System.Web.UI.WebControls.Literal EvaluationIDLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationIDLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image EvaluationQuestionDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal EvaluationQuestionDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.TextBox OrderNumber {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OrderNumber");
            }
        }
        
        public System.Web.UI.WebControls.Literal OrderNumberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OrderNumberLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox Question {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Question");
            }
        }
        
        public System.Web.UI.WebControls.Literal QuestionLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList TypeID {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TypeID");
            }
        }
        
        public System.Web.UI.WebControls.Literal TypeIDLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TypeIDLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EvaluationQuestionRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EvaluationQuestionRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new EvaluationQuestionRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  