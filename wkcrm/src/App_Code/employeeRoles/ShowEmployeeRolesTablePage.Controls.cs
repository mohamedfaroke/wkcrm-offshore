﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowEmployeeRolesTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowEmployeeRolesTablePage
{
  

#region "Section 1: Place your customizations here."

    
public class employeeRolesTableControlRow : BaseemployeeRolesTableControlRow
{
      
        // The BaseemployeeRolesTableControlRow implements code for a ROW within the
        // the employeeRolesTableControl table.  The BaseemployeeRolesTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of employeeRolesTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class employeeRolesTableControl : BaseemployeeRolesTableControl
{
        // The BaseemployeeRolesTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The employeeRolesTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the employeeRolesTableControlRow control on the ShowEmployeeRolesTablePage page.
// Do not modify this class. Instead override any method in employeeRolesTableControlRow.
public class BaseemployeeRolesTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseemployeeRolesTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in employeeRolesTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.employeeRolesRecordRowEditButton.Click += new ImageClickEventHandler(employeeRolesRecordRowEditButton_Click);
        }

        // To customize, override this method in employeeRolesTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in employeeRolesTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EmployeeRolesTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseemployeeRolesTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new EmployeeRolesRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in employeeRolesTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.employee_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeRolesTable.employee_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.employee_id.Text = formattedValue;
            } else {  
                this.employee_id.Text = EmployeeRolesTable.employee_id.Format(EmployeeRolesTable.employee_id.DefaultValue);
            }
                    
            if (this.employee_id.Text == null ||
                this.employee_id.Text.Trim().Length == 0) {
                this.employee_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.role_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeRolesTable.role_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.role_id.Text = formattedValue;
            } else {  
                this.role_id.Text = EmployeeRolesTable.role_id.Format(EmployeeRolesTable.role_id.DefaultValue);
            }
                    
            if (this.role_id.Text == null ||
                this.role_id.Text.Trim().Length == 0) {
                this.role_id.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in employeeRolesTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in employeeRolesTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).DataChanged = true;
                ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in employeeRolesTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EmployeeRolesTable.DeleteRecord(pk);

          
            ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).DataChanged = true;
            ((employeeRolesTableControl)MiscUtils.GetParentControlObject(this, "employeeRolesTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void employeeRolesRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../employeeRoles/EditEmployeeRolesPage.aspx?EmployeeRoles={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseemployeeRolesTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseemployeeRolesTableControlRow_Rec"] = value;
            }
        }
        
        private EmployeeRolesRecord _DataSource;
        public EmployeeRolesRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal employee_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_id");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton employeeRolesRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesRecordRowEditButton");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox employeeRolesRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Literal role_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "role_id");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EmployeeRolesRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EmployeeRolesRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return EmployeeRolesTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the employeeRolesTableControl control on the ShowEmployeeRolesTablePage page.
// Do not modify this class. Instead override any method in employeeRolesTableControl.
public class BaseemployeeRolesTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseemployeeRolesTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.employeeRolesPagination.FirstPage.Click += new ImageClickEventHandler(employeeRolesPagination_FirstPage_Click);
            this.employeeRolesPagination.LastPage.Click += new ImageClickEventHandler(employeeRolesPagination_LastPage_Click);
            this.employeeRolesPagination.NextPage.Click += new ImageClickEventHandler(employeeRolesPagination_NextPage_Click);
            this.employeeRolesPagination.PageSizeButton.Button.Click += new EventHandler(employeeRolesPagination_PageSizeButton_Click);
            this.employeeRolesPagination.PreviousPage.Click += new ImageClickEventHandler(employeeRolesPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.employee_idLabel1.Click += new EventHandler(employee_idLabel1_Click);
            this.role_idLabel1.Click += new EventHandler(role_idLabel1_Click);

            // Setup the button events.
        
            this.employeeRolesDeleteButton.Button.Click += new EventHandler(employeeRolesDeleteButton_Click);
            this.employeeRolesEditButton.Button.Click += new EventHandler(employeeRolesEditButton_Click);
            this.employeeRolesExportButton.Button.Click += new EventHandler(employeeRolesExportButton_Click);
            this.employeeRolesNewButton.Button.Click += new EventHandler(employeeRolesNewButton_Click);

            // Setup the filter and search events.
        
            this.employee_idFilter.SelectedIndexChanged += new EventHandler(employee_idFilter_SelectedIndexChanged);
            this.role_idFilter.SelectedIndexChanged += new EventHandler(role_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.employee_idFilter)) {
                this.employee_idFilter.Items.Add(new ListItem(this.GetFromSession(this.employee_idFilter), this.GetFromSession(this.employee_idFilter)));
                this.employee_idFilter.SelectedValue = this.GetFromSession(this.employee_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.role_idFilter)) {
                this.role_idFilter.Items.Add(new ListItem(this.GetFromSession(this.role_idFilter), this.GetFromSession(this.role_idFilter)));
                this.role_idFilter.SelectedValue = this.GetFromSession(this.role_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.employeeRolesDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EmployeeRolesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = EmployeeRolesTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EmployeeRolesRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (employeeRolesTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (EmployeeRolesRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = EmployeeRolesTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateemployee_idFilter(MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), 500);
            this.Populaterole_idFilter(MiscUtils.GetSelectedValue(this.role_idFilter, this.GetFromSession(this.role_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("employeeRolesTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                employeeRolesTableControlRow recControl = (employeeRolesTableControlRow)(repItem.FindControl("employeeRolesTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(EmployeeRolesTable.employee_id, this.DataSource);
            this.Page.PregetDfkaRecords(EmployeeRolesTable.role_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for employeeRolesTableControl pagination.
        
            this.employeeRolesPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.employeeRolesPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.employeeRolesPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.employeeRolesPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.employeeRolesPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.employeeRolesPagination.CurrentPage.Text = "0";
            }
            this.employeeRolesPagination.PageSize.Text = this.PageSize.ToString();
            this.employeeRolesTotalItems.Text = this.TotalRecords.ToString();
            this.employeeRolesPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.employeeRolesPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (employeeRolesTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            EmployeeRolesTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(EmployeeRolesTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.role_idFilter)) {
                wc.iAND(EmployeeRolesTable.role_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.role_idFilter, this.GetFromSession(this.role_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.employeeRolesPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.employeeRolesPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("employeeRolesTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    employeeRolesTableControlRow recControl = (employeeRolesTableControlRow)(repItem.FindControl("employeeRolesTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        EmployeeRolesRecord rec = new EmployeeRolesRecord();
        
                        if (recControl.employee_id.Text != "") {
                            rec.Parse(recControl.employee_id.Text, EmployeeRolesTable.employee_id);
                        }
                        if (recControl.role_id.Text != "") {
                            rec.Parse(recControl.role_id.Text, EmployeeRolesTable.role_id);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new EmployeeRolesRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (EmployeeRolesRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.EmployeeRolesRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(employeeRolesTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(employeeRolesTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for employee_idFilter.
        protected virtual void Populateemployee_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.employee_idFilter.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.employee_idFilter.Items.IndexOf(item) < 0) {
                    this.employee_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.employee_idFilter, selectedValue);

            // Add the All item.
            this.employee_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for role_idFilter.
        protected virtual void Populaterole_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(RoleTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.role_idFilter.Items.Clear();
            foreach (RoleRecord itemValue in RoleTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(RoleTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.role_idFilter.Items.IndexOf(item) < 0) {
                    this.role_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.role_idFilter, selectedValue);

            // Add the All item.
            this.role_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter employee_idFilter.
        public virtual WhereClause CreateWhereClause_employee_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.role_idFilter)) {
                wc.iAND(EmployeeRolesTable.role_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.role_idFilter, this.GetFromSession(this.role_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter role_idFilter.
        public virtual WhereClause CreateWhereClause_role_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(EmployeeRolesTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.employee_idFilter, this.employee_idFilter.SelectedValue);
            this.SaveToSession(this.role_idFilter, this.role_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.employee_idFilter);
            this.RemoveFromSession(this.role_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["employeeRolesTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["employeeRolesTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void employeeRolesPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void employeeRolesPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void employeeRolesPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void employeeRolesPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void employeeRolesPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void employee_idLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(EmployeeRolesTable.employee_id);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(EmployeeRolesTable.employee_id, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void role_idLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(EmployeeRolesTable.role_id);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(EmployeeRolesTable.role_id, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void employeeRolesDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void employeeRolesEditButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../employeeRoles/EditEmployeeRolesPage.aspx?EmployeeRoles={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void employeeRolesExportButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            WhereClause wc = this.CreateWhereClause();
            String exportedData = EmployeeRolesTable.Export(wc);
            BaseClasses.Utils.NetUtils.WriteResponseTextAttachment(this.Page.Response, "EmployeeRolesTable.csv", exportedData);
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void employeeRolesNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../employeeRoles/AddEmployeeRolesPage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void employee_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void role_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private EmployeeRolesRecord[] _DataSource = null;
        public  EmployeeRolesRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.DropDownList employee_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton employee_idLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel1");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeRolesDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeRolesEditButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesEditButton");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeRolesExportButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesExportButton");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeRolesNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesNewButton");
            }
        }
        
        public WKCRM.UI.IPagination employeeRolesPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesPagination");
            }
        }
        
        public System.Web.UI.WebControls.Literal employeeRolesTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox employeeRolesToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label employeeRolesTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRolesTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList role_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "role_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal role_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "role_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton role_idLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "role_idLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                employeeRolesTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                EmployeeRolesRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (employeeRolesTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.employeeRolesRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public employeeRolesTableControlRow GetSelectedRecordControl()
        {
        employeeRolesTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public employeeRolesTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (employeeRolesTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.employeeRolesRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (employeeRolesTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowEmployeeRolesTablePage.employeeRolesTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            employeeRolesTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (employeeRolesTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.employeeRolesRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public employeeRolesTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("employeeRolesTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                employeeRolesTableControlRow recControl = (employeeRolesTableControlRow)repItem.FindControl("employeeRolesTableControlRow");
                recList.Add(recControl);
            }

            return (employeeRolesTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowEmployeeRolesTablePage.employeeRolesTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  