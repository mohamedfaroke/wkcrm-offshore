﻿
// This file implements the code-behind class for EditProjectPage.aspx.
// App_Code\EditProjectPage.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI
{
  
partial class EditProjectPage
        : BaseApplicationPage
// Code-behind class for the EditProjectPage page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    

        public EditProjectPage()
        {
            this.Initialize();
            this.Load += new EventHandler(EditProjectPage_Load);
            this.Init += new EventHandler(EditProjectPage_Init);
        }

        void EditProjectPage_Init(object sender, EventArgs e)
        {
            RadToolTipManager1.AjaxUpdate += new Telerik.Web.UI.ToolTipUpdateEventHandler(RadToolTipManager1_AjaxUpdate);
            RadScheduler1.AppointmentCreated += new AppointmentCreatedEventHandler(RadScheduler1_AppointmentCreated);
            RadScheduler1.FormCreating += new SchedulerFormCreatingEventHandler(RadScheduler1_FormCreating);
            RadScheduler1.AppointmentUpdate += new Telerik.Web.UI.AppointmentUpdateEventHandler(RadScheduler1_AppointmentUpdate);
            RadScheduler1.DataBound +=new EventHandler(RadScheduler1_DataBound);
        }

    void EditProjectPage_Load(object sender, EventArgs e)
    {
        string projectID = Request["Project"];
        if (!IsPostBack)
        {
            Globals.SetSchedulerSettings(RadScheduler1, Globals.GetAllProjectTasks(projectID));
            RadScheduler1.SelectedView = SchedulerViewType.WeekView;
            RadScheduler1.ReadOnly = false;
            RadScheduler1.SelectedDate = Globals.GetEarliestTask(projectID).Date;
            RadScheduler1.AllowDelete = false;
        }
        //else
        //    RadScheduler1.DataSource = Globals.GetAllProjectTasks(projectID);

        RadAjaxManager1.AjaxRequest += new RadAjaxControl.AjaxRequestDelegate(RadAjaxManager1_AjaxRequest);

        RadToolTipManager1.AutoCloseDelay = 6000;
       
        RadScheduler1.OnClientAppointmentDoubleClick = "AppointmentDblClicked";
        RadScheduler1.OnClientAppointmentInserting = "AppointmentInserting";
        
    }

    void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "Refresh")
        {
            string projectID = Request["Project"];
            RadScheduler1.DataSource = Globals.GetAllProjectTasks(projectID);
            RadScheduler1.Rebind();
            try
            {
                DbUtils.StartTransaction();
                this.projectRecordControl.LoadData();
                this.projectRecordControl.DataBind();
                this.View_TradesmenTasksTableControl.LoadData();
                this.View_TradesmenTasksTableControl.DataBind();
            }
            finally
            {
                DbUtils.EndTransaction();
            }
           
        }
    }


    private bool IsAppointmentRegisteredForTooltip(Appointment apt)
    {
        foreach (ToolTipTargetControl targetControl in RadToolTipManager1.TargetControls)
        {
            if (apt.DomElements.Contains(targetControl.TargetControlID))
            {
                return true;
            }
        }
        return false;
    }

    void RadScheduler1_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
    {
        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
        {
            Shared_Appointment apt = e.Container.FindControl("aptDetails") as Shared_Appointment;
            if (apt != null)
            {
                apt.Appointment = e.Appointment;
                apt.AppType = AppointmentType.PROJECTTASK;
            }
            string aptId = e.Appointment.ID.ToString();
            foreach (string domElementId in e.Appointment.DomElements)
            {
                RadToolTipManager1.TargetControls.Add(domElementId, aptId, true);
            }

        }
    }

    void RadScheduler1_FormCreating(object sender, SchedulerFormCreatingEventArgs e)
    {
        e.Cancel = true;
    }
    void RadScheduler1_DataBound(object sender, EventArgs e)
    {
        RadToolTipManager1.TargetControls.Clear();
    }

    public void RadToolTipManager1_AjaxUpdate(object sender, ToolTipUpdateEventArgs e)
    {
        try
        {
            Appointment apt = RadScheduler1.Appointments.FindByID(e.Value);
            Shared_AppointmentToolTip tootip = LoadControl("../Shared/AppointmentToolTip.ascx") as Shared_AppointmentToolTip;
            TasksRecord rec = TasksTable.GetRecord(apt.ID.ToString(), false);
            if (rec == null)
                return;
            AppointmentBase myApt = new AppointmentTaskWrapper(rec);
            tootip.TargetAppointment = myApt;
            e.UpdatePanel.ContentTemplateContainer.Controls.Add(tootip);
        }
        catch
        {
        }
    }

    void RadScheduler1_AppointmentUpdate(object sender, Telerik.Web.UI.AppointmentUpdateEventArgs e)
    {
        string taskID = e.Appointment.ID.ToString();

        TasksRecord rec = TasksTable.GetRecord(taskID, false);

        AppointmentTaskWrapper task = new AppointmentTaskWrapper(rec);
        if (task != null)
        {
            task.Update(e.ModifiedAppointment);
        }
    }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links
    
        public void CancelButton_Click(object sender, EventArgs args)
        {
          
            // Click handler for CancelButton.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            CancelButton_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void SaveButton_Click(object sender, EventArgs args)
        {
          
            // Click handler for SaveButton.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
           // SaveButton_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.


            bool shouldRedirect = true;
            try
            {
                DbUtils.StartTransaction();

                if (!this.IsPageRefresh)
                {

                    this.projectRecordControl.SaveData();

                }

                if (!this.IsPageRefresh)
                {

                    this.View_TradesmenTasksTableControl.SaveData();

                }
                this.CommitTransaction(sender);
            }
            catch (Exception ex)
            {
                this.RollBackTransaction(sender);
                shouldRedirect = false;
                this.ErrorOnPage = true;

                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            }
            finally
            {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect)
            {
                this.ShouldSaveControlsToSession = true;
                RemoveCurrentRequestFromSessionNavigationHistory();
                string projectId = Request["project"];
                View_ProjectWithAccNameRecord rec = View_ProjectWithAccNameView.GetRecord(projectId, false);
                string url = String.Format("../account/EditAccountPage.aspx?Account={0}", rec.AccId);
                this.Response.Redirect(url);
            }


        }

#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.CancelButton.Button.Click += new EventHandler(CancelButton_Click);
            this.SaveButton.Button.Click += new EventHandler(SaveButton_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
            try {
                // Load data only when displaying the page for the first time
                if ((!this.IsPostBack)) {

                    // Must start a transaction before performing database operations
                    DbUtils.StartTransaction();

                    // Load data for each record and table UI control.
                    // Ordering is important because child controls get 
                    // their parent ids from their parent UI controls.
        
                    this.projectRecordControl.LoadData();
                    this.projectRecordControl.DataBind();           
          
                    this.View_TradesmenTasksTableControl.LoadData();
                    this.View_TradesmenTasksTableControl.DataBind();           
          
                }
            } catch (Exception ex) {
                // An error has occured so display an error message.
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "Page_Load_Error_Message", ex.Message);
            } finally {
                if (!this.IsPostBack) {
                    // End database transaction
                    DbUtils.EndTransaction();
                }
            }
        
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void CancelButton_Click_Base(object sender, EventArgs args)
        {
            
            bool shouldRedirect = true;
            try {
                
            } catch (Exception ex) {
                shouldRedirect = false;
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
            if (shouldRedirect) {
                this.ShouldSaveControlsToSession = true;
                this.RedirectBack();
            }
        }
          
        // event handler for Button with Layout
        public void SaveButton_Click_Base(object sender, EventArgs args)
        {
            
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                if (!this.IsPageRefresh) {
            
                    this.projectRecordControl.SaveData();
              
                }
        
                if (!this.IsPageRefresh) {
            
                    this.View_TradesmenTasksTableControl.SaveData();
              
                }
                  this.CommitTransaction(sender);
            } catch (Exception ex) {
                this.RollBackTransaction(sender);
                shouldRedirect = false;
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.ShouldSaveControlsToSession = true;
                this.RedirectBack();
            }
        }
          
#endregion

  
}
  
}
  