﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses.Utils;
using WKCRM.Business;

public partial class Utilities_SetAccountCompletion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetCompletion.Click += SetCompletion_Click;
    }

    void SetCompletion_Click(object sender, EventArgs e)
    {
        var threshold = Globals.CrmOptions.completionThreshold;
      
        Results.Text = "Completion threshold set at " + threshold + "<br/>";
        var psIds = View_PaymentScheduleTotalOutstandingView.GetRecords("TotalOutStanding <= " + threshold).Select(p => p.payment_schedule_id.ToString()).ToList();
        if (psIds.Count > 0)
        {
            var inClause = psIds.Aggregate((result, id) => id + "," + result);
            var contracts = PaymentScheduleTable.GetRecords(String.Format("id in ({0})", inClause)).Select(ps => ps.contract_id.ToString()).ToList();
            inClause = contracts.Aggregate((result, id) => id + "," + result);
            var accIds = ContractMainTable.GetRecords(String.Format("id in ({0})", inClause)).Select(c => c.account_id.ToString());
            inClause = accIds.Aggregate((result, id) => id + "," + result);

            var accRecs = AccountTable.GetRecords(String.Format("id in ({0})", inClause));
            Results.Text += accRecs.Length + " accounts will be have their completion status set <br/>";
            foreach (var acc in accRecs)
            {
                try
                {
                    DbUtils.StartTransaction();
                    var wAcc = acc.GetWritableRecord() as AccountRecord;
                    wAcc.account_status_id = Globals.ACCOUNT_COMPLETED;
                    wAcc.Save();
                    Results.Text += String.Format("{0} account status set to completed <br/>",acc.Name);
                    DbUtils.CommitTransaction();
                }
                catch(Exception ex)
                {
                    DbUtils.RollBackTransaction();
                    Results.Text += String.Format("{0} - error whilst attempting to udpate status ({1})<br/>", acc.Name, ex.Message);
                }
                finally
                {
                    DbUtils.EndTransaction();
                }
            }
        }
    }
}