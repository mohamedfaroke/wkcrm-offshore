﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WKCRM.UI;
using System.Data;
using EmpowerIT.Applications;

public partial class Utilities_ExportCRMOptions : BaseApplicationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ExportCRMOptions.Click += new EventHandler(ExportCRMOptions_Click);
    }

    void ExportCRMOptions_Click(object sender, EventArgs e)
    {
       
        DataTable dt = new DataTable();
        string sql = "SELECT * FROM CrmOptions";
        string filename = "CRM Options Export";
        
        if (!String.IsNullOrEmpty(sql))
        {
            DataAccess.PopulateDataTableByQuery(dt, sql);
            CSVExport export = new CSVExport(this.Page);
            export.DataSource = dt;
            export.FileName = filename;
            export.ExportCSV();
        }

    }
}