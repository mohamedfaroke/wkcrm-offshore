﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WKCRM.Business;
using BaseClasses.Data;
using WKCRM.UI;
using EmpowerIT.Applications;
using System.Data;

public partial class Utilities_ExportPriceList : BaseApplicationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            OrderBy ob = new OrderBy(false, true);
            ob.Add(ContractItemTypeTable.name, OrderByItem.OrderDir.Asc);
            ItemTypeCombo.DataTextField = "name";
            ItemTypeCombo.DataValueField = "id0";
            ItemTypeCombo.DataSource = ContractItemTypeTable.GetRecords("status_id=1", ob);
            ItemTypeCombo.DataBind();
        }
        ExportPriceList.Click += new EventHandler(ExportPriceList_Click);
    }

    void ExportPriceList_Click(object sender, EventArgs e)
    {
        int itemTypeId = Convert.ToInt32(ItemTypeCombo.SelectedValue);
        DataTable dt = new DataTable();
        string sql = "";
        string filename = String.Format("{0} Price List Export", ItemTypeCombo.Text);
        switch (itemTypeId)
        {
            case (Globals.CONTRACTITEM_DOORS):
                sql = String.Format("SELECT contractDoors.id, {0} AS contract_item_type, contractDoors.name, contractDoors.unit_price, contractDoorTypes.name as sub_category,'N/A' as supplier_code,contractDoors.status_id FROM contractDoors,contractDoorTypes WHERE contractDoors.status_id=1 AND contractDoors.door_type_id=contractDoorTypes.id", itemTypeId);
                break;
            case (Globals.CONTRACTITEM_BENCHTOPS):
                sql = String.Format("SELECT id, {0} AS contract_item_type, name, unit_price, 'N/A' as sub_category, 'N/A' as supplier_code,status_id FROM contractBenchtops WHERE status_id=1", itemTypeId);
                break;
            case (Globals.CONTRACTITEM_TRADES):
                sql = String.Format("SELECT contractTradeTasks.id, {0} as contract_item_type, contractTradeTasks.name, contractTradeTasks.price AS unit_price, contractTrades.name as sub_category,'N/A' as supplier_code, 'N/A' as status_id FROM contractTradeTasks,contractTrades WHERE contractTradeTasks.contract_trade_type=contractTrades.id", itemTypeId);
                break;
            default:
                sql = String.Format("SELECT id, contract_item_type, name, price AS unit_price, sub_category,supplier_code, status_id FROM contractOthers WHERE contract_item_type={0} AND status_id=1", itemTypeId);
                break;
        }
        if(!String.IsNullOrEmpty(sql))
        {
            DataAccess.PopulateDataTableByQuery(dt, sql);
            CSVExport export = new CSVExport(this.Page);
            export.DataSource = dt;
            export.FileName = filename;
            export.ExportCSV();
        }

    }
}