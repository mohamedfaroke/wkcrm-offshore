﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="DeactivateAccount.aspx.cs" Inherits="WKCRM.UI.DeactivateAccount" %>
<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.DeactivateAccount" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu7MenuItem">
		</WKCRM:Menu>
					</td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent"><asp:Label runat="server" id="Label" Text="&lt;h5>The accounts and contracts listed below will be deactivated if you proceed.&lt;br>They will no longer appear in the account listing, unless you specifically search for deactivated accounts&lt;/h5>">
		</asp:Label>
							<a name="StartOfPageContent"></a><div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div><div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div><asp:UpdateProgress runat="server" id="accountTableControlUpdateProgress" AssociatedUpdatePanelID="accountTableControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="accountTableControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:accountTableControl runat="server" id="accountTableControl">
														
<!-- Begin Table Panel.html -->
<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="accountTableTitle" Text="Accounts to Deactivate">
														</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="accountTotalItems">
														</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("accountTableControl$accountSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("accountTableControl$accountSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("accountTableControl$accountFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("accountTableControl$accountFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="date_createdLabel" Text="Date Created">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="NameLabel" Text="Name">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="designer_idLabel" Text="Designer">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="addressLabel" Text="Address">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="suburbLabel" Text="Suburb">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="postcodeLabel" Text="Post Code">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="account_type_idLabel" Text="Account Type">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="account_status_idLabel" Text="Account Status">
															</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="accountTableControlRepeater">
																<ITEMTEMPLATE>
																		<WKCRM:accountTableControlRow runat="server" id="accountTableControlRow">
																				
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				
				
			<td class="ttc" ><asp:Literal runat="server" id="date_created">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="Name">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="designer_id">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="address">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="suburb">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="postcode">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="account_type_id">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="account_status_id">
																				</asp:Literal></td>
			
			</tr>
			</div>
			
																		</WKCRM:accountTableControlRow>
																</ITEMTEMPLATE>
														</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table><!-- End Table Panel.html -->
											</WKCRM:accountTableControl>

										</ContentTemplate>
									</asp:UpdatePanel>
													<asp:UpdateProgress runat="server" id="contractMainTableControlUpdateProgress" AssociatedUpdatePanelID="contractMainTableControlUpdatePanel">
																<ProgressTemplate>
																	<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																	</div>
																	<div style=" position:absolute; padding:30px;">
																		<img src="../Images/updating.gif">
																	</div>
																</ProgressTemplate>
															</asp:UpdateProgress>
															<asp:UpdatePanel runat="server" id="contractMainTableControlUpdatePanel" UpdateMode="Conditional">

																<ContentTemplate>
																	<WKCRM:contractMainTableControl runat="server" id="contractMainTableControl">
																				
<!-- Begin Table Panel.html -->
<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="contractMainTableTitle" Text="Contracts to Deactivate">
																				</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="contractMainTotalItems">
																				</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractMainTableControl$contractMainSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractMainTableControl$contractMainSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractMainTableControl$contractMainFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractMainTableControl$contractMainFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="date_createdLabel1" Text="Date Created">
																					</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="created_byLabel" Text="Created By">
																					</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="account_idLabel" Text="Account">
																					</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="contract_type_idLabel" Text="Contract Type">
																					</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="contractMainTableControlRepeater">
																						<ITEMTEMPLATE>
																								<WKCRM:contractMainTableControlRow runat="server" id="contractMainTableControlRow">
																										
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				
				
			<td class="ttc" ><asp:Literal runat="server" id="date_created1">
																										</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="created_by">
																										</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="account_id">
																										</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="contract_type_id">
																										</asp:Literal></td>
			
			</tr>
			</div>
			
																								</WKCRM:contractMainTableControlRow>
																						</ITEMTEMPLATE>
																				</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table><!-- End Table Panel.html -->
																	</WKCRM:contractMainTableControl>

																</ContentTemplate>
															</asp:UpdatePanel>
														
</td>
						</tr><tr>
						<td class="pContent"><table><tr><td><WKCRM:ThemeButton runat="server" id="deactivateButton" Button-CausesValidation="True" Button-CommandName="Custom" Button-RedirectArgument="UpdateData" Button-RedirectURL="Back" Button-Text="Deactivate">
														</WKCRM:ThemeButton>
</td><td><WKCRM:ThemeButton runat="server" id="Button1" Button-CausesValidation="True" Button-CommandName="Custom" Button-RedirectURL="Back" Button-Text="Cancel">
														</WKCRM:ThemeButton>
</td></tr></table>&nbsp;</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
														</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



