﻿<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.EditAccountPage" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="EditAccountPage.aspx.cs" Inherits="WKCRM.UI.EditAccountPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu7MenuItem">
		</WKCRM:Menu>
					</td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
					<table>
						<tr>
						<td style="text-align:center;vertical-align:middle;">
                            <asp:ImageButton ID="contractButton" runat="server" ImageUrl="~/Images/contract.gif"/><br />Contracts</td>
						<td style="text-align:center;vertical-align:middle;">
                            <asp:ImageButton ID="paymentsButton" runat="server" ImageUrl="~/Images/payments.gif"/>
                            <asp:ImageButton ID="paymentsDisabledButton" runat="server" ImageUrl="~/Images/payments_disabled.gif" Visible="false" Enabled="false"/>
                            <br />Payments</td>
                            <td style="text-align:center;vertical-align:middle;">
                            <asp:ImageButton ID="projectButton" runat="server" ImageUrl="~/Images/project.gif"/>
                            <asp:ImageButton ID="projectDisabledButton" runat="server" ImageUrl="~/Images/project_disabled.gif" Visible="false" Enabled="false"/>
                            <br />Project Schedule</td>
						<td style="text-align:center;vertical-align:middle;">
                            <asp:ImageButton ID="surveyButton" runat="server" ImageUrl="~/Images/survey.gif"/>
                            <asp:ImageButton ID="surveyDisabledButton" runat="server" ImageUrl="~/Images/survey_disabled.gif" Visible="false" Enabled="false"/>
                            <br /></td>
						    
						</tr>
						</table>
						
						
						</td>
						</tr><tr>
						<td class="pContent"><a name="StartOfPageContent"></a><asp:UpdateProgress runat="server" id="accountRecordControlUpdateProgress" AssociatedUpdatePanelID="accountRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="accountRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:accountRecordControl runat="server" id="accountRecordControl">
														
<!-- Begin Record Panel.html -->
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %><table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="accountDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="accountDialogTitle" Text="Edit Account">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="NameLabel" Text="First Name">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="Name" MaxLength="50" Columns="50" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="NameRequiredFieldValidator" ControlToValidate="Name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;First Name&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="NameTextBoxMaxLengthValidator" ControlToValidate="Name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;First Name&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="Label" Text="Surname">
														</asp:Label>&nbsp;</td><td class="dfv"><asp:TextBox runat="server" id="LastName" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="LastNameRequiredFieldValidator" ControlToValidate="LastName" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Surname&quot;) %>" Enabled="True">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="LastNameTextBoxMaxLengthValidator" ControlToValidate="LastName" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Surname&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="business_phoneLabel" Text="Business Phone">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="business_phone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="business_phoneTextBoxMaxLengthValidator" ControlToValidate="business_phone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Business Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="home_phoneLabel" Text="Home Phone">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="home_phone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="home_phoneTextBoxMaxLengthValidator" ControlToValidate="home_phone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Home Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="business_phone2Label" Text="Business Phone (Mrs/Ms)">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="business_phone2" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="business_phone2TextBoxMaxLengthValidator" ControlToValidate="business_phone2" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Business Phone 2&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="mobileLabel" Text="Mobile">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="mobile" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="mobileTextBoxMaxLengthValidator" ControlToValidate="mobile" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Mobile&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="Label3" Text="Created Date">
														</asp:Label></td>
	<td class="dfv"><asp:Label runat="server" id="CreatedDate">
														</asp:Label></td><td class="fls"><asp:Literal runat="server" id="mobile2Label" Text="Mobile (Mrs/Ms)">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="mobile2" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="mobile2TextBoxMaxLengthValidator" ControlToValidate="mobile2" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Mobile 2&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label1" Text="Sale Date">
														</asp:Label>&nbsp;</td>
	<td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="sale_date" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("sale_date"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="sale_dateFvDsHyperLink" ControlToUpdate="sale_date" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="sale_dateTextBoxMaxLengthValidator" ControlToValidate="sale_date" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Sale Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td><td class="fls"><asp:Label runat="server" id="Label4" Text="Date Died">
														</asp:Label></td><td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="date_died" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("date_died"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="date_diedFvDsHyperLink" ControlToUpdate="date_died" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="date_diedTextBoxMaxLengthValidator" ControlToValidate="date_died" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Date Died&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="builders_nameLabel" Text="Builders Name">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="builders_name" MaxLength="50" Columns="30" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="builders_nameTextBoxMaxLengthValidator" ControlToValidate="builders_name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Builders Name&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="emailLabel" Text="Email">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="email" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="emailTextBoxMaxLengthValidator" ControlToValidate="email" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Email&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="builders_phoneLabel" Text="Builders Phone">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="builders_phone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="builders_phoneTextBoxMaxLengthValidator" ControlToValidate="builders_phone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Builders Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="Label7" Text="Secondary Email">
														</asp:Label></td><td class="dfv"><asp:TextBox runat="server" id="email2" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="email2TextBoxMaxLengthValidator" ControlToValidate="email2" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Email 2&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="addressLabel" Text="Site Address">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="address" MaxLength="50" Columns="50" CssClass="field_input" Rows="4">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="addressRequiredFieldValidator" ControlToValidate="address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Address&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="addressTextBoxMaxLengthValidator" ControlToValidate="address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Address&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="faxLabel" Text="Fax">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="fax" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="faxTextBoxMaxLengthValidator" ControlToValidate="fax" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Fax&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="postcodeLabel" Text="Post Code">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="postcode" Columns="4" MaxLength="4" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="postcodeRequiredFieldValidator" ControlToValidate="postcode" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Post Code&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="postcodeTextBoxMaxLengthValidator" ControlToValidate="postcode" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Post Code&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="SuburbLabel" Text="Suburb">
														</asp:Label></td><td class="dfv">&nbsp;<telerik:RadComboBox ID="SuburbCombo" runat="server" /></td>
	</tr>
	
	
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="postal_addressLabel" Text="Postal Address">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="postal_address" MaxLength="50" Columns="60" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="postal_addressTextBoxMaxLengthValidator" ControlToValidate="postal_address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Postal Address&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="next_review_dateLabel" Text="Next Review Date">
														</asp:Literal></td><td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="next_review_date" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("next_review_date"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="next_review_dateFvDsHyperLink" ControlToUpdate="next_review_date" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="next_review_dateTextBoxMaxLengthValidator" ControlToValidate="next_review_date" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Next Review Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="designer_idLabel" Text="Designer">
														</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="designer_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="designer_idFvLlsHyperLink" ControlToUpdate="designer_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="employee" Field="Employee_.id" DisplayField="Employee_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="designer_idRequiredFieldValidator" ControlToValidate="designer_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Designer&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
<asp:Label runat="server" id="scheduleLabel" Text="
          ">
														</asp:Label></td><td class="fls"><asp:Literal runat="server" id="account_status_idLabel" Text="Account Status">
														</asp:Literal></td><td class="dfv"><asp:DropDownList runat="server" id="account_status_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="account_status_idFvLlsHyperLink" ControlToUpdate="account_status_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="accountStatus" Field="AccountStatus_.id" DisplayField="AccountStatus_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="account_status_idRequiredFieldValidator" ControlToValidate="account_status_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Account Status&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="left_reasonLabel" Text="Left Reason">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="left_reason" MaxLength="50" Columns="60" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="left_reasonTextBoxMaxLengthValidator" ControlToValidate="left_reason" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Left Reason&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="has_been_revivedLabel" Text="Has Been Revived">
														</asp:Literal></td><td class="dfv"><asp:CheckBox runat="server" id="has_been_revived" UncheckedValue="No">
														</asp:CheckBox></td>
	</tr>
	
	
	
	
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="opportunity_source_idLabel" Text="Opportunity Source">
														</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="opportunity_source_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="opportunity_source_idFvLlsHyperLink" ControlToUpdate="opportunity_source_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="opportunitySource" Field="OpportunitySource_.id" DisplayField="OpportunitySource_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="opportunity_source_idRequiredFieldValidator" ControlToValidate="opportunity_source_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Opportunity Source&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator></td><td class="fls"><asp:Literal runat="server" id="revived_reasonLabel" Text="Revived Reason">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="revived_reason" MaxLength="50" Columns="60" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="revived_reasonTextBoxMaxLengthValidator" ControlToValidate="revived_reason" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Revived Reason&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label2" Text="Opportunity Detail">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="OpportunityDetails" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="OpportunityDetailsTextBoxMaxLengthValidator" ControlToValidate="OpportunityDetails" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Opportunity Details&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="contact_source_idLabel" Text="Contact Source">
														</asp:Literal></td><td class="dfv"><asp:DropDownList runat="server" id="contact_source_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="contact_source_idFvLlsHyperLink" ControlToUpdate="contact_source_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="contactSource" Field="ContactSource_.id" DisplayField="ContactSource_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="contact_source_idRequiredFieldValidator" ControlToValidate="contact_source_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Contact Source&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator></td>
	</tr>
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="benchtop_typeLabel" Text="Benchtop Type">
														</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="benchtop_type" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="benchtop_typeFvLlsHyperLink" ControlToUpdate="benchtop_type" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="AccountBenchtopType" Field="AccountBenchtopType_.AccountBenchtopTypeId" DisplayField="AccountBenchtopType_.AccountBenchtopType">														</Selectors:FvLlsHyperLink></td><td class="fls"><asp:Literal runat="server" id="door_typeLabel" Text="Door Type">
														</asp:Literal></td><td class="dfv"><asp:DropDownList runat="server" id="door_type" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="door_typeFvLlsHyperLink" ControlToUpdate="door_type" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="AccountDoorType" Field="AccountDoorType_.AccountDoorTypeId" DisplayField="AccountDoorType_.AccountDoorType">														</Selectors:FvLlsHyperLink></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="time_frameLabel" Text="Time Frame">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="time_frame" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="time_frameTextBoxMaxLengthValidator" ControlToValidate="time_frame" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Time Frame&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="price_rangeLabel" Text="Price Range">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="price_range" Columns="19" MaxLength="19" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="price_rangeTextBoxMaxLengthValidator" ControlToValidate="price_range" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Price Range&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label5" Text="Home Warranty Policy">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="homeWarrantyPolicy" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="homeWarrantyPolicyTextBoxMaxLengthValidator" ControlToValidate="homeWarrantyPolicy" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Home Warranty Policy&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="Label6" Text="Home Warranty Date">
														</asp:Label></td><td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="homeWarrantyDate" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("homeWarrantyDate"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="homeWarrantyDateFvDsHyperLink" ControlToUpdate="homeWarrantyDate" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="homeWarrantyDateTextBoxMaxLengthValidator" ControlToValidate="homeWarrantyDate" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Home Warranty Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr>
	
	
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %><!-- End Record Panel.html -->
											</WKCRM:accountRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									
<asp:UpdateProgress runat="server" id="communicationTableControlUpdateProgress" AssociatedUpdatePanelID="communicationTableControlUpdatePanel">
																	<ProgressTemplate>
																		<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																		</div>
																		<div style=" position:absolute; padding:30px;">
																			<img src="../Images/updating.gif">
																		</div>
																	</ProgressTemplate>
																</asp:UpdateProgress>
																<asp:UpdatePanel runat="server" id="communicationTableControlUpdatePanel" UpdateMode="Conditional">

																	<ContentTemplate>
																		<WKCRM:communicationTableControl runat="server" id="communicationTableControl">
																					
<!-- Begin Table Panel.html -->
<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="communicationTableTitle" Text="Communication">
																					</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="communicationTotalItems">
																					</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("communicationTableControl$communicationSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("communicationTableControl$communicationSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("communicationTableControl$communicationFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("communicationTableControl$communicationFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="communicationNewButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../communication/AddcommunicationPage.aspx?accid={accountRecordControl:FV:id}" Button-Text="&lt;%# GetResourceValue(&quot;Btn:New&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="employee_idLabel1" Text="Employee">
																						</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="datetimeLabel1" Text="Date Time" CausesValidation="False">
																						</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="notesLabel" Text="Notes">
																						</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="communicationTableControlRepeater">
																							<ITEMTEMPLATE>
																									<WKCRM:communicationTableControlRow runat="server" id="communicationTableControlRow">
																											
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:ImageButton runat="server" id="communicationRecordRowEditButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_edit.gif" RedirectURL="../communication/EditcommunicationPage.aspx?Communication={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:EditRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																											</asp:ImageButton></td>
				
				
				
			<td class="ttc" ><asp:Literal runat="server" id="employee_id">
																											</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="datetime2">
																											</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="notes">
																											</asp:Literal></td>
			
			</tr>
			</div>
			
																									</WKCRM:communicationTableControlRow>
																							</ITEMTEMPLATE>
																					</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table><!-- End Table Panel.html -->
																		</WKCRM:communicationTableControl>

																	</ContentTemplate>
																</asp:UpdatePanel>
															
<br/><br/>
<WKCRM:leadMeetingsTableControl runat="server" id="leadMeetingsTableControl">
																		
<!-- Begin Table Panel.html -->
<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="leadMeetingsTableTitle" Text="Meetings">
																		</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="leadMeetingsTotalItems">
																		</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("leadMeetingsTableControl$leadMeetingsSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("leadMeetingsTableControl$leadMeetingsSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("leadMeetingsTableControl$leadMeetingsFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("leadMeetingsTableControl$leadMeetingsFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="leadMeetingsNewButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../leadMeetings/AddLeadMeeting.aspx?accid={accountRecordControl:FV:id}" Button-Text="&lt;%# GetResourceValue(&quot;Btn:New&quot;, &quot;WKCRM&quot;) %>">
																			</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th><th class="thc" >&nbsp;</th><th class="thc" >&nbsp;</th>
			
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="datetimeLabel" Text="Date Time" CausesValidation="False">
																			</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="designer_idLabel1" Text="Designer">
																			</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="addressLabel1" Text="Address">
																			</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="onsiteLabel" Text="Onsite">
																			</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="leadMeetingsTableControlRepeater">
																				<ITEMTEMPLATE>
																						<WKCRM:leadMeetingsTableControlRow runat="server" id="leadMeetingsTableControlRow">
																								
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:ImageButton runat="server" id="leadMeetingsRecordRowEditButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_edit.gif" RedirectURL="../leadMeetings/EditleadMeetingsPage.aspx?LeadMeetings={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:EditRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																								</asp:ImageButton></td><td class="tic" scope="row"><asp:ImageButton runat="server" id="ImageButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/file_small_pdf.gif" RedirectURL="../Reports/ShowReport.aspx?reportID=11&amp;recordId={leadMeetingsTableControlRow:FV:id}" ToolTip="Display Reminder" AlternateText="">

																								</asp:ImageButton></td><td class="tic" scope="row"><asp:ImageButton runat="server" id="EmailButton" CausesValidation="True" CommandName="Custom" ImageURL="../Images/mail_forward.png" ToolTip="Send via Email" AlternateText="">

																								</asp:ImageButton></td>
				
				
				
			<td class="ttc" ><asp:Literal runat="server" id="datetime1">
																								</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="designer_id1">
																								</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="address1">
																								</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="onsite">
																								</asp:Literal></td>
			
			</tr>
			</div>
			
																						</WKCRM:leadMeetingsTableControlRow>
																				</ITEMTEMPLATE>
																		</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table><!-- End Table Panel.html -->
															</WKCRM:leadMeetingsTableControl>
														
<br/><br/>
<asp:UpdateProgress runat="server" id="attachmentTableControlUpdateProgress" AssociatedUpdatePanelID="attachmentTableControlUpdatePanel">
																						<ProgressTemplate>
																							<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																							</div>
																							<div style=" position:absolute; padding:30px;">
																								<img src="../Images/updating.gif">
																							</div>
																						</ProgressTemplate>
																					</asp:UpdateProgress>
																					<asp:UpdatePanel runat="server" id="attachmentTableControlUpdatePanel" UpdateMode="Conditional">

																						<ContentTemplate>
																							<WKCRM:attachmentTableControl runat="server" id="attachmentTableControl">
																										
<!-- Begin Table Panel.html -->
<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="attachmentTableTitle" Text="Attachment">
																										</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="attachmentTotalItems">
																										</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("attachmentTableControl$attachmentSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("attachmentTableControl$attachmentSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("attachmentTableControl$attachmentFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("attachmentTableControl$attachmentFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="attachmentNewButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../attachment/AddAttachmentPage.aspx?accid={accountRecordControl:FV:id}" Button-Text="&lt;%# GetResourceValue(&quot;Btn:New&quot;, &quot;WKCRM&quot;) %>">
																											</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="attachmentDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																											</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="attachmentToggleAll" onclick="toggleAllCheckboxes(this);">

																											</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="nameLabel1" Text="Name">
																											</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="attachmentTableControlRepeater">
																												<ITEMTEMPLATE>
																														<WKCRM:attachmentTableControlRow runat="server" id="attachmentTableControlRow">
																																
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				<td class="tic"><asp:Label runat="server" id="ViewAttachmentLabel" Text="
          ">
																																</asp:Label></td>
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="attachmentRecordRowSelection">

																																</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="name1">
																																</asp:Literal></td>
			
			</tr>
			</div>
			
																														</WKCRM:attachmentTableControlRow>
																												</ITEMTEMPLATE>
																										</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table><!-- End Table Panel.html -->
																							</WKCRM:attachmentTableControl>

																						</ContentTemplate>
																					</asp:UpdatePanel>
																				

<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-RedirectArgument="updatedata" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
																					</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
																					</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table><div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div><div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>



<br/><br/></td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
																				</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



