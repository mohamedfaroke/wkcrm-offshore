﻿<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.AddAccountPage" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="AddAccountPage.aspx.cs" Inherits="WKCRM.UI.AddAccountPage" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu2" Src="../Menu Panels/Menu2.ascx" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>

	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu7MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td>&nbsp;<WKCRM:Menu2 runat="server" id="Menu1" HiliteSettings="Menu1MenuItem">
		</WKCRM:Menu2></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent"><a name="StartOfPageContent"></a><asp:UpdateProgress runat="server" id="accountRecordControlUpdateProgress" AssociatedUpdatePanelID="accountRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="accountRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:accountRecordControl runat="server" id="accountRecordControl">
														
<!-- Begin Record Panel.html -->
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %><table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="accountDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="accountDialogTitle" Text="Add Lead">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="NameLabel" Text="First Name">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="Name" MaxLength="50" Columns="60" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="NameRequiredFieldValidator" ControlToValidate="Name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;First Name&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="NameTextBoxMaxLengthValidator" ControlToValidate="Name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;First Name&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="Label" Text="Surname">
														</asp:Label>&nbsp;</td><td class="dfv"><asp:TextBox runat="server" id="LastName" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="LastNameRequiredFieldValidator" ControlToValidate="LastName" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Surname&quot;) %>" Enabled="True">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="LastNameTextBoxMaxLengthValidator" ControlToValidate="LastName" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Surname&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="home_phoneLabel" Text="Home Phone">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="home_phone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="home_phoneTextBoxMaxLengthValidator" ControlToValidate="home_phone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Home Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="business_phoneLabel" Text="Business Phone">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="business_phone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="business_phoneTextBoxMaxLengthValidator" ControlToValidate="business_phone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Business Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="mobileLabel" Text="Mobile">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="mobile" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="mobileTextBoxMaxLengthValidator" ControlToValidate="mobile" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Mobile&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="Label4" Text="Business Phone (Mrs/Ms)">
														</asp:Label></td><td class="dfv"><asp:TextBox runat="server" id="Business2" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="Business2TextBoxMaxLengthValidator" ControlToValidate="Business2" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Business Phone 2&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label1" Text="Mobile (Mrs/Ms)">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="Mobile2" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="Mobile2TextBoxMaxLengthValidator" ControlToValidate="Mobile2" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Mobile 2&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="faxLabel" Text="Fax">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="fax" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="faxTextBoxMaxLengthValidator" ControlToValidate="fax" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Fax&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label2" Text="Builders Name">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="BuildersName" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="BuildersNameTextBoxMaxLengthValidator" ControlToValidate="BuildersName" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Builders Name&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="Label3" Text="Builders Phone">
														</asp:Label></td><td class="dfv"><asp:TextBox runat="server" id="BuildersPhone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="BuildersPhoneTextBoxMaxLengthValidator" ControlToValidate="BuildersPhone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Builders Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="addressLabel" Text="Site Address">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="address" MaxLength="50" Columns="60" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="addressRequiredFieldValidator" ControlToValidate="address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Address&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="addressTextBoxMaxLengthValidator" ControlToValidate="address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Address&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="SuburbLabel" Text="Suburb">
														</asp:Label></td><td class="dfv"><telerik:RadComboBox ID="SuburbCombo" runat="server" /></td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="postcodeLabel" Text="Post Code">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="postcode" Columns="4" MaxLength="4" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="postcodeRequiredFieldValidator" ControlToValidate="postcode" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Post Code&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="postcodeTextBoxMaxLengthValidator" ControlToValidate="postcode" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Post Code&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="emailLabel" Text="Email">
														</asp:Literal></td><td class="dfv"><asp:TextBox runat="server" id="email" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="emailTextBoxMaxLengthValidator" ControlToValidate="email" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Email&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label5" Text="Postal Address">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="PostalAddress" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="PostalAddressTextBoxMaxLengthValidator" ControlToValidate="PostalAddress" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Postal Address&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Label runat="server" id="Label8" Text="Secondary Email">
														</asp:Label></td><td class="dfv"><asp:TextBox runat="server" id="email2" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="email2TextBoxMaxLengthValidator" ControlToValidate="email2" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Email 2&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="designer_idLabel" Text="Designer">
														</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="designer_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="designer_idFvLlsHyperLink" ControlToUpdate="designer_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="employee" Field="Employee_.id" DisplayField="Employee_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="designer_idRequiredFieldValidator" ControlToValidate="designer_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Designer&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
<asp:Label runat="server" id="showScheduleLabel" Text="&lt;a href=&quot;javascript:&quot;onclick=&quot;OpenDesignerSchedule(); return false;&quot;>Show Designer Schedules...&lt;/a>">
														</asp:Label></td><td class="fls"><asp:Literal runat="server" id="date_createdLabel" Text="Date Created">
														</asp:Literal></td><td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="date_created" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("date_created"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="date_createdFvDsHyperLink" ControlToUpdate="date_created" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="date_createdRequiredFieldValidator" ControlToValidate="date_created" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Date Created&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="date_createdTextBoxMaxLengthValidator" ControlToValidate="date_created" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Date Created&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="benchtop_typeLabel" Text="Benchtop Type">
														</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="benchtop_type" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="benchtop_typeFvLlsHyperLink" ControlToUpdate="benchtop_type" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="AccountBenchtopType" Field="AccountBenchtopType_.AccountBenchtopTypeId" DisplayField="AccountBenchtopType_.AccountBenchtopType">														</Selectors:FvLlsHyperLink></td><td class="fls"><asp:Label runat="server" id="Label6" Text="Sale Date">
														</asp:Label></td><td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="sale_date" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("sale_date"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="sale_dateFvDsHyperLink" ControlToUpdate="sale_date" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="sale_dateTextBoxMaxLengthValidator" ControlToValidate="sale_date" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Sale Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="time_frameLabel" Text="Time Frame">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="time_frame" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="time_frameTextBoxMaxLengthValidator" ControlToValidate="time_frame" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Time Frame&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="door_typeLabel" Text="Door Type">
														</asp:Literal></td><td class="dfv"><asp:DropDownList runat="server" id="door_type" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="door_typeFvLlsHyperLink" ControlToUpdate="door_type" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="AccountDoorType" Field="AccountDoorType_.AccountDoorTypeId" DisplayField="AccountDoorType_.AccountDoorType">														</Selectors:FvLlsHyperLink></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="opportunity_source_idLabel" Text="Opportunity Source">
														</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="opportunity_source_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="opportunity_source_idFvLlsHyperLink" ControlToUpdate="opportunity_source_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="opportunitySource" Field="OpportunitySource_.id" DisplayField="OpportunitySource_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="opportunity_source_idRequiredFieldValidator" ControlToValidate="opportunity_source_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Opportunity Source&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator></td><td class="fls"><asp:Literal runat="server" id="price_rangeLabel" Text="Price Range">
														</asp:Literal></td><td class="dfv"><asp:DropDownList runat="server" id="price_range" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
																<asp:ListItem Value="10-15k">
																</asp:ListItem><asp:ListItem Value="15-20K">
																</asp:ListItem><asp:ListItem Value="20-25K">
																</asp:ListItem><asp:ListItem Value="25-30K">
																</asp:ListItem><asp:ListItem Value="30-35K">
																</asp:ListItem><asp:ListItem Value="35-40K">
																</asp:ListItem><asp:ListItem Value="45-50K">
																</asp:ListItem><asp:ListItem Value="50+K">
																</asp:ListItem>
														</asp:DropDownList></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label7" Text="Opportunity Detail">
														</asp:Label></td>
	<td class="dfv"><asp:TextBox runat="server" id="OpportuntiyDetails" Columns="50" MaxLength="50">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="OpportuntiyDetailsTextBoxMaxLengthValidator" ControlToValidate="OpportuntiyDetails" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Opportunity Details&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td><td class="fls"><asp:Literal runat="server" id="contact_source_idLabel" Text="Contact Source">
														</asp:Literal></td><td class="dfv"><asp:DropDownList runat="server" id="contact_source_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="contact_source_idFvLlsHyperLink" ControlToUpdate="contact_source_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="contactSource" Field="ContactSource_.id" DisplayField="ContactSource_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="contact_source_idRequiredFieldValidator" ControlToValidate="contact_source_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Contact Source&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator></td>
	</tr>
	
	
	
	
	
	
	
	
	
	
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %><!-- End Record Panel.html -->
											</WKCRM:accountRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									



<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-RedirectURL="../leadMeetings/AddLeadMeeting.aspx?accid={accountRecordControl:FV:id}" Button-Text="Save and Arrange Appointment">
										</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table><div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div><div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div></td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
									</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



