﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.PaymentSchedule" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="PaymentSchedule.aspx.cs" Inherits="WKCRM.UI.PaymentSchedule" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<script language="javascript" src="../RadWindow.js"></script>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
	<asp:Label ID="scriptHack" runtat="server"></asp:Label>
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu3MenuItem">
		</WKCRM:Menu>
					</td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent"><telerik:RadWindowManager ID="RadWindowManager1" runat="server" Modal="true" Behaviors="Move,Close" VisibleStatusbar="false" ReloadOnShow="true" Width="450px" Height="450px"><Windows><telerik:RadWindow ID="StandardWindow" runat="server"></telerik:RadWindow></Windows></telerik:RadWindowManager><a name="StartOfPageContent"></a>
							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div><div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div><asp:UpdateProgress runat="server" id="View_PaymentScheduleOverviewTableControlUpdateProgress" AssociatedUpdatePanelID="View_PaymentScheduleOverviewTableControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="View_PaymentScheduleOverviewTableControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:View_PaymentScheduleOverviewTableControl runat="server" id="View_PaymentScheduleOverviewTableControl">
														
<!-- Begin Table Panel.html -->
<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="View_PaymentScheduleOverviewTableTitle" Text="Payment Schedule Overview">
														</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="View_PaymentScheduleOverviewTotalItems">
														</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_PaymentScheduleOverviewTableControl$View_PaymentScheduleOverviewSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_PaymentScheduleOverviewTableControl$View_PaymentScheduleOverviewSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_PaymentScheduleOverviewTableControl$View_PaymentScheduleOverviewFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_PaymentScheduleOverviewTableControl$View_PaymentScheduleOverviewFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			
			
			<th class="thc" scope="col">&nbsp;</th><th class="thc" scope="col">&nbsp;</th><th class="thc" scope="col"><asp:Literal runat="server" id="payment_stage_idLabel" Text="Payment Stage">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="TotalAmountLabel" Text="Total Amount">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="ReceivedLabel" Text="Total Received">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="OutstandingLabel" Text="Outstanding">
															</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="View_PaymentScheduleOverviewTableControlRepeater">
																<ITEMTEMPLATE>
																		<WKCRM:View_PaymentScheduleOverviewTableControlRow runat="server" id="View_PaymentScheduleOverviewTableControlRow">
																				
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				
				
			<td class="ttc" ><asp:ImageButton runat="server" id="paymentButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/Payment.gif" ToolTip="Make a Payment" AlternateText="">

																				</asp:ImageButton></td><td class="ttc" ><asp:ImageButton runat="server" id="variationsButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/Variation.gif" ToolTip="Make a Variation to this Total" AlternateText="">

																				</asp:ImageButton></td><td class="ttc" ><asp:Literal runat="server" id="payment_stage_id">
																				</asp:Literal></td>
			
			<td class="ttc" style=";;;;text-align:right"><asp:Literal runat="server" id="TotalAmount">
																				</asp:Literal></td>
			
			<td class="ttc" style=";;;text-align:right"><asp:Literal runat="server" id="Received">
																				</asp:Literal></td>
			
			<td class="ttc" style=";;;;text-align:right"><asp:Literal runat="server" id="Outstanding">
																				</asp:Literal></td>
			
			</tr>
			</div>
			
																		</WKCRM:View_PaymentScheduleOverviewTableControlRow>
																</ITEMTEMPLATE>
														</asp:Repeater>
			<!-- Totals Area -->
			
			
			<tr>
				
				
			<td class="tpttcv">&nbsp;</td><td class="tpttcv"><asp:label id="Label" runat="server" text="Grand Totals"/></td><td class="tpttcv">&nbsp;</td>
			
			<td class="tpttcv"><asp:Label runat="server" id="TotalAmountGrandTotal" Field="TotalAmount" Table="View_PaymentScheduleOverview">

															</asp:Label></td>
			
			<td class="tpttcv"><asp:Label runat="server" id="ReceivedGrandTotal" Field="TotalReceived" Table="View_PaymentScheduleOverview">

															</asp:Label></td>
			
			<td class="tpttcv"><asp:Label runat="server" id="OutstandingGrandTotal" Field="Outstanding" Table="View_PaymentScheduleOverview">

															</asp:Label></td>
			
			</tr>
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table><!-- End Table Panel.html -->
											</WKCRM:View_PaymentScheduleOverviewTableControl>

										</ContentTemplate>
									</asp:UpdatePanel>
								
</td></tr><tr>
						<td class="pContent">&nbsp;<asp:UpdateProgress runat="server" id="paymentsTableControlUpdateProgress" AssociatedUpdatePanelID="paymentsTableControlUpdatePanel">
																<ProgressTemplate>
																	<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																	</div>
																	<div style=" position:absolute; padding:30px;">
																		<img src="../Images/updating.gif">
																	</div>
																</ProgressTemplate>
															</asp:UpdateProgress>
															<asp:UpdatePanel runat="server" id="paymentsTableControlUpdatePanel" UpdateMode="Conditional">

																<ContentTemplate>
																	<WKCRM:paymentsTableControl runat="server" id="paymentsTableControl">
																				
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="paymentsTableTitle" Text="Payments">
																				</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="paymentsTotalItems">
																				</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("paymentsTableControl$paymentsSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("paymentsTableControl$paymentsSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("paymentsTableControl$paymentsFilterButton")) %>	
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="payment_stages_idLabel" Text="Payment Stages">
																					</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="payment_stages_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
																					</asp:DropDownList></td>
	</tr>
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="payment_type_idLabel" Text="Payment Type">
																					</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="payment_type_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
																					</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("paymentsTableControl$paymentsFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			
			
			<th class="thc" scope="col">&nbsp;</th><th class="thc" scope="col"><asp:Literal runat="server" id="payment_stages_idLabel1" Text="Payment Stages">
																					</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="datetimeLabel1" Text="Date Time" CausesValidation="False">
																					</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="employee_idLabel2" Text="Employee">
																					</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="payment_type_idLabel1" Text="Payment Type">
																					</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="notesLabel1" Text="Notes">
																					</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="amountLabel1" Text="Amount">
																					</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="paymentsTableControlRepeater">
																						<ITEMTEMPLATE>
																								<WKCRM:paymentsTableControlRow runat="server" id="paymentsTableControlRow">
																										
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				
				
			<td class="ttc" ><asp:ImageButton runat="server" id="paymentReportButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/file_small_pdf.gif" RedirectURL="../Reports/ShowReport.aspx?recordId={paymentsTableControlRow:FV:id}&amp;reportID=4" AlternateText="">

																										</asp:ImageButton></td><td class="ttc" ><asp:Literal runat="server" id="payment_stages_id">
																										</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="datetime2">
																										</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="employee_id1">
																										</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="payment_type_id">
																										</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="notes1">
																										</asp:Literal></td>
			
			<td class="ttc"  style="text-align:right"><asp:Literal runat="server" id="amount1">
																										</asp:Literal></td>
			
			</tr>
			</div>
			
																								</WKCRM:paymentsTableControlRow>
																						</ITEMTEMPLATE>
																				</asp:Repeater>
			<!-- Totals Area -->
			
			
			<tr>
				
				
			<td class="tpttcv">&nbsp;</td><td class="tpttcv"><%# GetResourceValue("Txt:GrandTotal", "WKCRM") %></td>
			
			<td class="tpttcv">&nbsp;</td>
			
			<td class="tpttcv">&nbsp;</td>
			
			<td class="tpttcv">&nbsp;</td>
			
			<td class="tpttcv">&nbsp;</td>
			
			<td class="tpttcv"><asp:Label runat="server" id="amountGrandTotal" Field="amount" Table="payments">

																					</asp:Label></td>
			
			</tr>
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																	</WKCRM:paymentsTableControl>

																</ContentTemplate>
															</asp:UpdatePanel>
														
</td></tr><tr>
						<td class="pContent">&nbsp;<WKCRM:paymentVariationsTableControl runat="server" id="paymentVariationsTableControl">
																	
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="paymentVariationsTableTitle" Text="Variations">
																	</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="paymentVariationsTotalItems">
																	</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("paymentVariationsTableControl$paymentVariationsSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("paymentVariationsTableControl$paymentVariationsSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("paymentVariationsTableControl$paymentVariationsFilterButton")) %>	
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="payment_stage_idLabel1" Text="Payment Stage">
																		</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="payment_stage_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
																		</asp:DropDownList></td>
	</tr>
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="employee_idLabel" Text="Employee">
																		</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="employee_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
																		</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("paymentVariationsTableControl$paymentVariationsFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			
			
			<th class="thc" scope="col">&nbsp;</th><th class="thc" scope="col"><asp:Literal runat="server" id="payment_stage_idLabel2" Text="Payment Stage">
																		</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="amountLabel" Text="Amount">
																		</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="notesLabel" Text="Notes">
																		</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="employee_idLabel1" Text="Employee">
																		</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="datetimeLabel" Text="Date Added" CausesValidation="False">
																		</asp:LinkButton></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="paymentVariationsTableControlRepeater">
																			<ITEMTEMPLATE>
																					<WKCRM:paymentVariationsTableControlRow runat="server" id="paymentVariationsTableControlRow">
																							
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				
				
			<td class="ttc" ><asp:ImageButton runat="server" id="variationReportButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/file_small_pdf.gif" RedirectURL="../Reports/ShowReport.aspx?reportID=3&amp;recordId={paymentVariationsTableControlRow:FV:id}" ToolTip="View &amp; Print Variation Contract" AlternateText="">

																							</asp:ImageButton></td><td class="ttc" ><asp:Literal runat="server" id="payment_stage_id1">
																							</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="amount">
																							</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="notes">
																							</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="employee_id">
																							</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="datetime1">
																							</asp:Literal></td>
			
			</tr>
			</div>
			
																					</WKCRM:paymentVariationsTableControlRow>
																			</ITEMTEMPLATE>
																	</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

														</WKCRM:paymentVariationsTableControl>
													

</td></tr>
<tr>
<td>
<WKCRM:ThemeButton runat="server" id="Button" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectArgument="UpdateData" Button-RedirectURL="Back" Button-Text="Ok">
													</WKCRM:ThemeButton>
</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
													</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



