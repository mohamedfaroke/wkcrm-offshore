﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WKCRM.Business;
using WKCRM.UI;

public partial class PaymentSchedule_FinaliseTotal : BaseApplicationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SaveButton.Click += new EventHandler(SaveButton_Click);

        origTotal.Focus();
        priceTextbox.TextChanged += new EventHandler(priceTextbox_TextChanged);
        if (!this.IsPostBack)
        {

            string accID = Request["AccID"];
            if (!String.IsNullOrEmpty(Request["goahead"]))
            {
                string errorMsg = "";

                if (Session["discPrice"] != null)
                {
                    decimal newAmt = Convert.ToDecimal(Session["discPrice"]);

                    if (Globals.StartProjectPhase(accID, this.SystemUtils.GetUserID(), out errorMsg, newAmt))
                    {
                        //We should have created a payment schedule. Payment schedules are based on contracts
                        //which are based on accounts ... lets get em! (obscure shoes! reference)
                        string psID = Globals.GetPaymentScheduleID(accID);
                        if (!String.IsNullOrEmpty(psID))
                        {
                            string myUrl = "../PaymentSchedule/PaymentSchedule.aspx?PaymentSchedule=" + psID;
                            RadAjaxManager1.ResponseScripts.Add("RedirectParentPage('" + myUrl + "')");
                            //scriptHack.Text = Globals.InjectJavascript("RedirectParentPage('" + myUrl + "')");

                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(errorMsg))
                            errorMsg = "Error starting project phase";
                        errorMsg += ". This window will now be closed";
                        RadAjaxManager1.ResponseScripts.Add("radconfirm('" + errorMsg + "',CloseWindow,350,150,null,'Error')");
                        //scriptHack.Text = Globals.InjectJavascript("radconfirm('" + errorMsg + "',CloseWindow,350,150,null,'Error')");
                    }
                }
            }
            else
            {
                View_AccountContractProjectRecord acp = View_AccountContractProjectView.GetRecord("AccID=" + accID);
                string contractID = acp.ContractID.ToString();
                if (!String.IsNullOrEmpty(contractID))
                {
                    View_ContractItemTotalsRecord total = View_ContractItemTotalsView.GetRecord("contract_id=" + contractID);
                    if (total != null)
                    {
                        decimal totalEx = total.TotalAmount;
                        decimal totalInc = Math.Floor(Globals.AddGST(totalEx));
                        origTotal.Text = totalInc.ToString("$0.00");
                        priceTextbox.NumberFormat.DecimalDigits = 0;
                        priceTextbox.Value = Convert.ToDouble(totalInc);
                        Session["pDiff"] = 0;
                        Session["discPrice"] = totalInc;
                    }
                }
            }

        }
    }

    void SaveButton_Click(object sender, EventArgs e)
    {
        decimal percentage = Convert.ToDecimal(Session["pDiff"]);
        if (percentage != null)
        {

            string alertMsg = "There is a difference of " + percentage.ToString("0.00%") + " between the original and final totals. Are you sure?";
            string myFunc = "ShowRadConfirm('" + alertMsg + "',discountCallBack,'Are You Sure?')";
            if (percentage != 0)
            {
                RadWindowManagerDiscount.RadConfirm(alertMsg, "discountCallBack", null, null, null, "Are You Sure?");
            }
                //scriptHack.Text = Globals.InjectJavascript(myFunc);
            else
                RadAjaxManager1.ResponseScripts.Add("discountCallBack(true)");
            //scriptHack.Text = Globals.InjectJavascript("discountCallBack(true)");
        }
    }

    void priceTextbox_TextChanged(object sender, EventArgs e)
    {
        string accID = Request["AccID"];
        View_AccountContractProjectRecord acp = View_AccountContractProjectView.GetRecord("AccID=" + accID);
        string contractID = acp.ContractID.ToString();
        if (!String.IsNullOrEmpty(contractID))
        {
            View_ContractItemTotalsRecord total = View_ContractItemTotalsView.GetRecord("contract_id=" + contractID);
            if (total != null && priceTextbox.Value.HasValue)
            {
                decimal totalEx = total.TotalAmount;
                decimal totalInc = Math.Floor(Globals.AddGST(totalEx));

                decimal newTotalInc = (decimal)priceTextbox.Value;

                decimal percentage = (1 - (totalInc / newTotalInc));

                percentageDiff.Text = percentage.ToString("0.00%");
                //Greater than 10% difference, make it red
                if (Math.Abs(percentage) >= (decimal)0.1)
                    percentageDiff.ForeColor = System.Drawing.Color.Red;
                else
                    percentageDiff.ForeColor = System.Drawing.Color.Black;
                Session["pDiff"] = percentage;
                Session["discPrice"] = newTotalInc;
            }
        }
    }
}