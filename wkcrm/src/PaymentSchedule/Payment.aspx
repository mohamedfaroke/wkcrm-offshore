﻿<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.Payment" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Payment.aspx.cs" Inherits="WKCRM.UI.Payment" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
			<script language="javascript" src="../RadWindow.js">
			</script>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<asp:Label runat="server" ID="scriptHack"></asp:Label>
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	<br /><br />
<asp:UpdateProgress runat="server" id="paymentsRecordControlUpdateProgress" AssociatedUpdatePanelID="paymentsRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="paymentsRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:paymentsRecordControl runat="server" id="paymentsRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="paymentsDialogTitle" Text="Make a payment">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="Label" Text="Payment Stage">
														</asp:Label></td>
	<td class="dfv"><asp:Label runat="server" id="paymentStage" Text="
          ">
														</asp:Label></td>
	</tr><tr>
	<td class="fls">
		<asp:Literal runat="server" id="payment_type_idLabel" Text="Payment Type">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:DropDownList runat="server" id="payment_type_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="payment_type_idFvLlsHyperLink" ControlToUpdate="payment_type_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="paymentTypes" Field="PaymentTypes_.id" DisplayField="PaymentTypes_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="payment_type_idRequiredFieldValidator" ControlToValidate="payment_type_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Payment Type&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="amountLabel" Text="Amount">
														</asp:Literal>
	</td>
	<td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="amount" Columns="20" MaxLength="20" onkeyup="adjustCurrency(this, event.keyCode,'$', '.', 'False');" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("amount"),"CurrencyTextBox","$",".","False") %>
														&nbsp;
														<asp:RequiredFieldValidator runat="server" id="amountRequiredFieldValidator" ControlToValidate="amount" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Amount&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="amountTextBoxMaxLengthValidator" ControlToValidate="amount" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Amount&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="notesLabel" Text="Notes">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="notes" MaxLength="50" Columns="40" CssClass="field_input" Rows="3" TextMode="MultiLine">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="notesTextBoxMaxLengthValidator" ControlToValidate="notes" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Notes&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
</td><td style="width:15%"></td></tr></table>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:paymentsRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									


<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-HtmlAttributes-onClick="CloseWindow()" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table>
</td><td style="width:15%"></td></tr></table>

<br/>
<br/></form>
	</body>
</html>
