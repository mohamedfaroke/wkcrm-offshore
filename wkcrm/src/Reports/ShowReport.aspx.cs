﻿
// This file implements the code-behind class for ShowReport.aspx.
// App_Code\ShowReport.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Impowa.Dev.Utilities;
using System.IO;
using System.Collections.Specialized;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
        

#endregion

  
namespace WKCRM.UI
{
  
partial class ShowReport
        : BaseApplicationPage
// Code-behind class for the ShowReport page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    
    Boolean displayAsPDF = true;

    String viewName;
    String reportId;
    String reportFormat;
    String reportYear;

    String reportName;
    String exportPath;
    String reportsPath;
    String whereClause;
    String crystalServer;
    String crystalDatabase;
    String crystalUsername;
    String crystalPassword;

    NameValueCollection settings;
    ConnectionInfo connectionInfo;
    private ReportDocument currentReport;

        public ShowReport()
        {
            this.Initialize();
            this.PreRender += new EventHandler(ShowReport_PreRender);
            this.Load += new EventHandler(ShowReport_Load);
        }

    void ShowReport_Load(object sender, EventArgs e)
    {
        RemoveCurrentRequestFromSessionNavigationHistory();
    }

    void ShowReport_PreRender(object sender, EventArgs e)
    {


        reportId = Request.QueryString["reportID"];
        reportYear = Request.QueryString["year"];

        ReportsRecord rec = ReportsTable.GetRecord("id = " + reportId);
        reportName = rec.filename;
        viewName = rec.viewname;

        


        string reportTitle = rec.name;

        // Create the report object
        currentReport = new ReportDocument();

        reportsPath = Server.MapPath("..") + "\\Reports\\rpt\\";

        currentReport.Load(reportsPath + reportName);
        string reportParams = rec.params0;
        if (!String.IsNullOrEmpty(reportParams))
        {
            DateTime fdate = Convert.ToDateTime(Request.QueryString["DateFrom"]);
            DateTime tdate = Convert.ToDateTime(Request.QueryString["DateTo"]);
            string[] splitParams = reportParams.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            //Currently only handle DateFrom,DateTo
            foreach (string myparam in splitParams)
            {
                ParameterField reportField = currentReport.ParameterFields[myparam];
                reportField.CurrentValues.Clear();
                if(myparam == "DateFrom")
                    reportField.CurrentValues.AddValue(fdate);
                else if(myparam =="DateTo")
                    reportField.CurrentValues.AddValue(tdate);
            }
            //SUPER HACK
            if(reportId == "13")
            {
                //Add whereclause in addition to the parameters
                //+= it here because i know there is ar ecord selection formula already
                currentReport.DataDefinition.RecordSelectionFormula += " AND " + createWhereClause();
            }
        }
        else
        {
            currentReport.DataDefinition.RecordSelectionFormula = createWhereClause();
        }
        connectionInfo = new ConnectionInfo();
        connectionInfo.UserID = Web.GetFromWebConfig("ReportUserName");
        connectionInfo.Password = Web.GetFromWebConfig("ReportPassword");
        connectionInfo.ServerName = Web.GetFromWebConfig("ReportDatabaseFile");
        connectionInfo.DatabaseName = Web.GetFromWebConfig("ReportDatabase");
        SetDBLogonForReport(connectionInfo, currentReport);
        SetDBLogonForSubreports(connectionInfo, currentReport);

       // crystalReportViewer.ReportSource = currentReport;
        //crystalReportViewer.h
        // Display report as PDF in the Browser            
        MemoryStream oStream; //using System.IO
        oStream = (MemoryStream)currentReport.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + reportTitle.Replace(" ", "") + ".pdf");
        Response.BinaryWrite(oStream.ToArray());
        Response.Flush();
        Response.End();
    }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }
         private String createWhereClause()
         {
             // Going to do some magic ....
            // Lets be smart and know when to add the extra clauses
             //If they arent in the request, it means we dont want them!
            
             //This is a dodgy hack because crystal falls over if you specify a blank record selection formula, so 
             // we put a statement that will always be true
             whereClause = " 1 = 1 ";

             string fdate = Request.QueryString["DateFrom"];
             string tdate = Request.QueryString["DateTo"];

             if(!String.IsNullOrEmpty(fdate))
             {
                 string fromDateFormatted = Convert.ToDateTime(fdate).ToString("yyyy/MM/dd");
                 whereClause += getClause("Date", fromDateFormatted, " >= ", "#");
             }

             if(!String.IsNullOrEmpty(tdate))
             {
                 string toDateFormatted = Convert.ToDateTime(tdate).ToString("yyyy/MM/dd");
                 whereClause += getClause("Date", toDateFormatted, " <= ", "#");
             }


             if(!String.IsNullOrEmpty(Request.QueryString["year"]))
                 whereClause += getClause("Year", Request.QueryString["year"], " = ", "") + " ";
             if (!String.IsNullOrEmpty(Request.QueryString["recordId"]))
             {
                 // Quick hack to make sure that this works for all reports
                 // This is because report 4 and report 6 use the same baseview, however the 'id' field is 
                 // different each time ... fix this in phase2, maybe...
                 if (reportId == "4")
                     whereClause += getClause("PaymentsID", Request.QueryString["recordId"], " = ", "") + " ";
                 else
                     whereClause += getClause("id", Request.QueryString["recordId"], " = ", "") + " ";
             }
             return whereClause;
         }


         // Default sign is '=' and no quotes
         private String getClause(String col, String value)
         {
             return getClause(col, value, " = ", "");
         }

         private String getClause(String col, String value, String sign, String quotes)
         {
             String comp = "";
             if (value != null && !value.Equals("") && !value.Equals("-1"))
             {
                 comp = " and {" + viewName + "." + col + "} " + sign + " " + quotes + value + quotes;
             }
             return comp;
         }



         private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument reportDocument)
         {
             Tables tables = reportDocument.Database.Tables;
             foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
             {
                 TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                 tableLogonInfo.ConnectionInfo = connectionInfo;
                 table.ApplyLogOnInfo(tableLogonInfo);
                 table.Location = connectionInfo.DatabaseName + ".dbo." + table.Location.Substring(table.Location.LastIndexOf(".") + 1);
             }
         }

         private void SetDBLogonForSubreports(ConnectionInfo connectionInfo, ReportDocument reportDocument)
         {
             Sections sections = reportDocument.ReportDefinition.Sections;
             foreach (Section section in sections)
             {
                 ReportObjects reportObjects = section.ReportObjects;
                 foreach (ReportObject reportObject in reportObjects)
                 {
                     if (reportObject.Kind == ReportObjectKind.SubreportObject)
                     {
                         SubreportObject subreportObject = (SubreportObject)reportObject;
                         ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
                         SetDBLogonForReport(connectionInfo, subReportDocument);
                     }
                 }
             }
         }

         protected void crystalReportViewer_Init(object sender, EventArgs e)
         {

         }


#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links
    

#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
#endregion

  
}
  
}
  