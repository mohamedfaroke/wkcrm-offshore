﻿<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Overview.aspx.cs" Inherits="WKCRM.UI.Overview" %>
<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu5" Src="../Menu Panels/Menu5.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Reference Control="../Shared/AppointmentToolTip.ascx" %>
<%@ Register TagPrefix="Apt" TagName="AptSummary" Src="~/Shared/Appointment.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
    <link href="../Styles/RadSkins/Scheduler.Mac.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td><WKCRM:Menu5 runat="server" id="Menu1">
		</WKCRM:Menu5></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							
							<div>
                            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
                                <AjaxSettings>
                                    <telerik:AjaxSetting AjaxControlID="RadScheduler1">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="RadScheduler1" />
                                              <telerik:AjaxUpdatedControl ControlID="RadToolTipManager1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                </AjaxSettings>
                            </telerik:RadAjaxManager>
                                        
                            <telerik:RadScheduler ID="RadScheduler1" runat="server" DataKeyField="ID" DataSubjectField="Subject" DataStartField="Start" DataEndField="End" Width="100%">
<AppointmentTemplate>
       <Apt:AptSummary ID="aptDetails" runat="server" />
</AppointmentTemplate>
                            
                            </telerik:RadScheduler>
                            
                             <telerik:RadToolTipManager runat="server" ID="RadToolTipManager1"
                                            Width="300" Height="100" Skin="Vista"
                                            Animation="Resize" Position="BottomRight" HideEvent="LeaveToolTip" Text="Loading..." />
                            
                            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="75px" IsSticky="False" Width="75px">
                                <asp:Image ID="Image1" runat="server" AlternateText="Loading..." ImageUrl="../Images/Loading.gif" />
                            </telerik:RadAjaxLoadingPanel>
                        </div>
							
							
							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
		</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



