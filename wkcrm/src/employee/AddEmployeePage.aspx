﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="AddEmployeePage.aspx.cs" Inherits="WKCRM.UI.AddEmployeePage" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.AddEmployeePage" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu1" Src="../Menu Panels/Menu1.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="5035" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu11MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td>&nbsp;<WKCRM:Menu1 runat="server" id="Menu1" HiliteSettings="Menu2MenuItem">
		</WKCRM:Menu1></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent"><a name="StartOfPageContent"></a><asp:UpdateProgress runat="server" id="employeeRecordControlUpdateProgress" AssociatedUpdatePanelID="employeeRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="employeeRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:employeeRecordControl runat="server" id="employeeRecordControl">
														
<!-- Begin Record Panel.html -->
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %><table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="employeeDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="employeeDialogTitle" Text="Add Employee">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="nameLabel" Text="Name">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="name" MaxLength="50" Columns="50" CssClass="field_input" Rows="2">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="nameRequiredFieldValidator" ControlToValidate="name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Name&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="nameTextBoxMaxLengthValidator" ControlToValidate="name" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Name&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="location_idLabel" Text="Location">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:DropDownList runat="server" id="location_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="location_idFvLlsHyperLink" ControlToUpdate="location_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="location" Field="Location_.id" DisplayField="Location_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="location_idRequiredFieldValidator" ControlToValidate="location_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Location&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="dobLabel" Text="Date Of Birth">
														</asp:Literal>
	</td>
	<td class="dfv">
		<table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="dob" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("dob"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="dobFvDsHyperLink" ControlToUpdate="dob" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="dobTextBoxMaxLengthValidator" ControlToValidate="dob" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Date Of Birth&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="phoneLabel" Text="Phone">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="phone" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="phoneTextBoxMaxLengthValidator" ControlToValidate="phone" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Phone&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="mobileLabel" Text="Mobile">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="mobile" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="mobileTextBoxMaxLengthValidator" ControlToValidate="mobile" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Mobile&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="addressLabel" Text="Address">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="address" MaxLength="50" Columns="60" CssClass="field_input" Rows="2" TextMode="MultiLine">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="addressTextBoxMaxLengthValidator" ControlToValidate="address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Address&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="emailLabel" Text="Email">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="email" Columns="50" MaxLength="50" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="emailTextBoxMaxLengthValidator" ControlToValidate="email" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Email&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="usernameLabel" Text="Username">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="UserName1" Columns="20" MaxLength="20" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="UserName1RequiredFieldValidator" ControlToValidate="UserName1" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Username&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="UserName1TextBoxMaxLengthValidator" ControlToValidate="UserName1" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Username&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="passwordLabel" Text="Password">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="password" Columns="50" MaxLength="50" CssClass="field_input" TextMode="Password">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="passwordRequiredFieldValidator" ControlToValidate="password" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Password&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="passwordTextBoxMaxLengthValidator" ControlToValidate="password" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Password&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %><%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %><!-- End Record Panel.html -->
											</WKCRM:employeeRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									
<br/><br/><br/><br/>


<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-RedirectURL="../employeeRoles/AddEmployeeRolesPage.aspx?empid={employeeRecordControl:FV:id}" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table><br/><br/><div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div><div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div></td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
									</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



