﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ShowTasksTablePage.aspx.cs" Inherits="WKCRM.UI.ShowTasksTablePage" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu3" Src="../Menu Panels/Menu3.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Pagination" Src="../Shared/Pagination.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.ShowTasksTablePage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu3MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td><WKCRM:Menu3 runat="server" id="Menu1" HiliteSettings="Menu2MenuItem">
		</WKCRM:Menu3></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent"><a name="StartOfPageContent"></a><div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div><div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>&nbsp;<asp:UpdateProgress runat="server" id="View_TradesmenTasksTableControlUpdateProgress" AssociatedUpdatePanelID="View_TradesmenTasksTableControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="View_TradesmenTasksTableControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:View_TradesmenTasksTableControl runat="server" id="View_TradesmenTasksTableControl">
														
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="View_TradesmenTasksTableTitle" Text="Project Tasks">
														</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="View_TradesmenTasksTotalItems">
														</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_TradesmenTasksTableControl$View_TradesmenTasksSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_TradesmenTasksTableControl$View_TradesmenTasksSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_TradesmenTasksTableControl$View_TradesmenTasksFilterButton")) %>	
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="project_idLabel" Text="Project">
															</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="project_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
															</asp:DropDownList></td>
	</tr>
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="tradesman_idLabel" Text="Tradesman">
															</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="tradesman_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
															</asp:DropDownList></td>
	</tr>
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="TaskAcknowledgedLabel" Text="Task Acknowledged">
															</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="TaskAcknowledgedFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
															</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_TradesmenTasksTableControl$View_TradesmenTasksFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="View_TradesmenTasksEditButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="EditTasksPage.aspx?Tasks={View_TradesmenTasksTableControlRow:FV:id}" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Edit&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="View_TradesmenTasksDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			<td class="pra">
			<WKCRM:Pagination runat="server" id="View_TradesmenTasksPagination">
														</WKCRM:Pagination>
			</td>
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="View_TradesmenTasksToggleAll" onclick="toggleAllCheckboxes(this);">

															</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="datetimeLabel" Text="Date Time" CausesValidation="False">
															</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="project_idLabel1" Text="Project">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="tradesman_idLabel1" Text="Tradesman">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="task_type_idLabel" Text="Task Type">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="notesLabel" Text="Notes">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="TaskAcknowledgedLabel1" Text="Task Acknowledged">
															</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="View_TradesmenTasksTableControlRepeater">
																<ITEMTEMPLATE>
																		<WKCRM:View_TradesmenTasksTableControlRow runat="server" id="View_TradesmenTasksTableControlRow">
																				
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:ImageButton runat="server" id="View_TradesmenTasksRecordRowEditButton" CausesValidation="False" CommandName="Redirect" Consumers="page" CssClass="button_link" ImageURL="../Images/icon_edit.gif" RedirectURL="EditTasksPage.aspx?Tasks={View_TradesmenTasksTableControlRow:FV:id}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:EditRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																				</asp:ImageButton></td>
				
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="View_TradesmenTasksRecordRowSelection">

																				</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="datetime1">
																				</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="project_id">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="tradesman_id">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="task_type_id">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="notes">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="TaskAcknowledged">
																				</asp:Literal></td>
			
			</tr>
			</div>
			
																		</WKCRM:View_TradesmenTasksTableControlRow>
																</ITEMTEMPLATE>
														</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

											</WKCRM:View_TradesmenTasksTableControl>

										</ContentTemplate>
									</asp:UpdatePanel>
								
<br/>
<br/>

<br/>
<br/>
</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
								</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



