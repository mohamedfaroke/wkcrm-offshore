<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HandleSelection.aspx.cs" Inherits="Contract_HandleSelection" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  
    <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
</head>
<script language="javascript" src="../RadWindow.js"></script>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
    <asp:Label ID="scriptHack" runat="server"></asp:Label>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
         <AjaxSettings>
             <telerik:AjaxSetting AjaxControlID="RadComboBox1">
                 <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="designerNote" />
                     <telerik:AjaxUpdatedControl ControlID="styleTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="doorCombo" />
                     <telerik:AjaxUpdatedControl ControlID="drawCombo" />
                     <telerik:AjaxUpdatedControl ControlID="priceLabel" />
                     <telerik:AjaxUpdatedControl ControlID="priceDesc" />
                     <telerik:AjaxUpdatedControl ControlID="priceTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="qtyTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="supplierTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="notesTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                     <telerik:AjaxUpdatedControl ControlID="markupLabel" />
                     <telerik:AjaxUpdatedControl ControlID="markupCombo" />
                 </UpdatedControls>
             </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="priceTextbox">
                 <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                 </UpdatedControls>
             </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="qtyTextbox">
                 <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                 </UpdatedControls>
             </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="markupCombo">
                 <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                 </UpdatedControls>
             </telerik:AjaxSetting>
         </AjaxSettings>

    </telerik:RadAjaxManager>
    
    <div>
        <table style="width: 133px">
            <tr>
                <td style="width: 127px; height: 14px;" align="right">
                    <asp:Label ID="Label1" runat="server" Text="Handle"></asp:Label></td>
                <td style="width: 170px; height: 14px;">
                    <telerik:RadComboBox ID="RadComboBox1" runat="server" AutoPostBack="true">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 227px; height: 14px">
                </td>
                <td style="width: 71px; height: 14px;">
                </td>
            </tr>
            <tr>
                <td >
                </td>
                <td style="width: 170px">
                    <asp:Label ID="designerNote" runat="server" ForeColor="Red"></asp:Label></td>
                <td style="width: 227px">
                </td>
                <td style="width: 71px">
                </td>
            </tr>
            <tr>
                <td style="width: 127px" align="right">
                    <asp:Label ID="Label2" runat="server" Text="Style No"></asp:Label></td>
                <td style="width: 170px">
                    <telerik:RadTextBox ID="styleTextbox" runat="server">
                    </telerik:RadTextBox></td>
                <td style="width: 227px">
                </td>
                <td style="width: 71px">
                </td>
            </tr>
            <tr>
                <td style="width: 127px" align="right">
                    <asp:Label ID="Label3" runat="server" Text="Door Handle Position" Width="146px"></asp:Label></td>
                <td style="width: 170px">
                    <telerik:RadComboBox ID="doorCombo" runat="server" RadComboBoxImagePosition="Right">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="As Design" Value="As Design" />
                            <telerik:RadComboBoxItem runat="server" Text="Horizontal" Value="Horizontal" />
                            <telerik:RadComboBoxItem runat="server" Text="Vertical" Value="Vertical" />
                        </Items>
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                        <ExpandAnimation Type="OutQuart" />
                    </telerik:RadComboBox>
                </td>
                <td style="width: 227px">
                </td>
                <td style="width: 71px">
                </td>
            </tr>
            <tr>
                <td style="width: 127px" align="right">
                    <asp:Label ID="Label4" runat="server" Text="Draw Handle Position" Width="146px"></asp:Label></td>
                <td style="width: 170px">
                    <telerik:RadComboBox ID="drawCombo" runat="server" RadComboBoxImagePosition="Right">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="As Design" Value="As Design" />
                            <telerik:RadComboBoxItem runat="server" Text="Horizontal" Value="Horizontal" />
                            <telerik:RadComboBoxItem runat="server" Text="Vertical" Value="Vertical" />
                        </Items>
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                        <ExpandAnimation Type="OutQuart" />
                    </telerik:RadComboBox>
                </td>
                <td style="width: 227px">
                </td>
                <td style="width: 71px">
                </td>
            </tr>
            <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="priceLabel" runat="server" Text="Price"></asp:Label>&nbsp;<asp:Label ID="priceDesc" runat="server" ForeColor="Red"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="priceTextbox" runat="server" AutoPostBack="True">
                        <NumberFormat AllowRounding="True" />
                    </telerik:RadNumericTextBox></td>
                <td style="width: 227px; height: 18px">
                </td>
                <td style="width: 71px; height: 18px;">
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 127px; height: 18px">
                    <asp:Label ID="markupLabel" runat="server" Text="Markup"></asp:Label></td>
                <td style="width: 170px; height: 18px">
                    <telerik:RadComboBox ID="markupCombo" runat="server" RadComboBoxImagePosition="Right" AutoPostBack="True">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="0%" Value="0.00" />
                            <telerik:RadComboBoxItem runat="server" Text="5%" Value="0.05" />
                            <telerik:RadComboBoxItem runat="server" Text="10%" Value="0.10" />
                            <telerik:RadComboBoxItem runat="server" Text="15%" Value="0.15" />
                            <telerik:RadComboBoxItem runat="server" Text="20%" Value="0.20" />
                            <telerik:RadComboBoxItem runat="server" Text="25%" Value="0.25" />
                            <telerik:RadComboBoxItem runat="server" Text="30%" Value="0.30" />
                        </Items>
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                        <ExpandAnimation Type="OutQuart" />
                    </telerik:RadComboBox>
                </td>
                <td style="width: 227px; height: 18px">
                </td>
                <td style="width: 71px; height: 18px">
                </td>
            </tr>
            <tr>
                <td style="width: 127px; height: 18px" align="right">
                    <asp:Label ID="Label7" runat="server" Text="Quantity"></asp:Label></td>
                <td style="width: 170px; height: 18px">
                    <telerik:RadNumericTextBox ID="qtyTextbox" runat="server" AutoPostBack="True">
                        <NumberFormat AllowRounding="True" />
                    </telerik:RadNumericTextBox></td>
                <td style="width: 227px; height: 18px">
                </td>
                <td style="width: 71px; height: 18px">
                </td>
            </tr>
            <tr>
                <td style="width: 127px; height: 18px" align="right">
                    <asp:Label ID="Label5" runat="server" Text="Supplier"></asp:Label></td>
                <td style="width: 170px; height: 18px">
                    <telerik:RadTextBox ID="supplierTextbox" runat="server">
                    </telerik:RadTextBox></td>
                <td style="width: 227px; height: 18px">
                </td>
                <td style="width: 71px; height: 18px">
                </td>
            </tr>
            <tr>
                <td style="width: 127px" align="right">
                    <asp:Label ID="Label8" runat="server" Text="Notes"></asp:Label></td>
                <td style="width: 170px">
                    <telerik:RadTextBox ID="notesTextbox" runat="server"
                        TextMode="MultiLine" Width="167px">
                    </telerik:RadTextBox></td>
                <td style="width: 227px">
                </td>
                <td style="width: 71px">
                </td>
            </tr>
            <tr>
                <td style="height: 20px">
                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="Medium" Text="Total"></asp:Label></td>
                <td style="height: 20px; width: 170px;">
                    <asp:Label ID="totalAmt" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label></td>
                <td style="width: 227px; height: 20px">
                </td>
                <td style="height: 20px; width: 71px;">
                </td>
            </tr>
            <tr>
                <td style="height: 20px">
                    <asp:Button ID="saveButton" runat="server" OnClick="saveButton_Click" Text="Save"/></td>
                <td style="height: 20px; width: 170px;">
                    <asp:Button ID="cancelButton" runat="server" Text="Cancel" OnClientClick="CloseWindow()" /></td>
                <td style="width: 227px; height: 20px">
                </td>
                <td style="height: 20px; width: 71px;">
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
