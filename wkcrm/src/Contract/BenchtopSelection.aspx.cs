﻿
// This file implements the code-behind class for BenchtopSelection.aspx.
// App_Code\BenchtopSelection.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using WKCRM.UI.Controls.BenchtopSelection;
using System.Globalization;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI
{
  
partial class BenchtopSelection
        : BaseApplicationPage
// Code-behind class for the BenchtopSelection page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    

        public BenchtopSelection()
        {
            this.Initialize();
            this.Load += new EventHandler(BenchtopSelection_Load);
        }


    void BenchtopSelection_Load(object sender, EventArgs e)
    {
        RemoveCurrentRequestFromSessionNavigationHistory();

        string editBench = Request["Bench"];
        totalAmt.PreRender += new EventHandler(totalAmt_PreRender);
        benchtopCombo.AppendDataBoundItems = true;
        benchtopCombo.MarkFirstMatch = true;
        edgeProfileCombo.MarkFirstMatch = true;
        //A hack to ensure that the pricedTextbox doesnt go spastic when
        //it gets focus on page load and end up getting confused about
        // what values to display
        benchtopCombo.Focus();
        if (!this.IsPostBack)
        {
            supplierBox.Enabled = false;
            qtyTextbox.Enabled = false;
            priceTextbox.Enabled = false;
            colourTextbox.Enabled = false;
            finishTextbox.Enabled = false;
            edgeProfileCombo.Enabled = false;
            edgeThickness.Enabled = false;
            notesTextbox.Enabled = false;
            markupCombo.Enabled = false;


            benchtopCombo.Items.Clear();
            RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
            benchtopCombo.Items.Add(myItem);

             OrderBy order = new OrderBy(false, true);
             order.Add(ContractBenchtopsTable.order_by, OrderByItem.OrderDir.Asc);
             string where = "status_id=1";

            benchtopCombo.DataSource = ContractBenchtopsTable.GetRecords(where,order);
            benchtopCombo.DataTextField = "name";
            benchtopCombo.DataValueField = "id0";
            benchtopCombo.DataBind();

            if (benchtopCombo.Items.Count < 15)
            {
                int myHeight = benchtopCombo.Items.Count * 25;
                benchtopCombo.Height = Unit.Pixel(myHeight < 150 ? 150 : myHeight);
            }

            if (!String.IsNullOrEmpty(editBench))
            {
                //have to pre populate the controls
                ContractBenchtopItemRecord benchItem = ContractBenchtopItemTable.GetRecord(editBench, false);
                RadComboBoxItem itm = benchtopCombo.FindItemByValue(benchItem.item_id.ToString());
                if (itm != null)
                    itm.Selected = true;
                // Do this after selecting the combo item, but before setting
                // the values, as it will reset these values to 0
                SetBenchtopFields();
                qtyTextbox.Value = (double)benchItem.units;
                priceTextbox.Value = (double)benchItem.unit_price;
                supplierBox.Text = benchItem.supplier;
                colourTextbox.Text = benchItem.colour;
                edgeThickness.Text = benchItem.edge_thickness;
                finishTextbox.Text = benchItem.finish;
                notesTextbox.Text = benchItem.notes;
                RadComboBoxItem ep = edgeProfileCombo.FindItemByText(benchItem.edge_profile);
                if (ep != null)
                    ep.Selected = true;
                if (markupCombo.Enabled)
                {
                    itm = markupCombo.FindItemByValue(benchItem.markup.ToString());
                    if (itm != null)
                        itm.Selected = true;
                }
            }

            string rWidth = Request["rWidth"];
            if (!String.IsNullOrEmpty(rWidth))
                scriptHack.Text = "<script> ResizeWindow('" + rWidth + "')</script>";
        }
        benchtopCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(benchtopCombo_SelectedIndexChanged);
        RadAjaxManager1.AjaxSettings.AddAjaxSetting(contractBenchtopOptionItemTableControlRepeater, totalAmt, null);
        RadAjaxManager1.AjaxSettings.AddAjaxSetting(contractBenchtopOptionItemDeleteButton, totalAmt, null);
    }



    private void BuildEdgeProfileCombo()
    {
        edgeProfileCombo.Items.Clear();

        edgeProfileCombo.AppendDataBoundItems = true;
        RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
        edgeProfileCombo.Items.Add(myItem);

        OrderBy order = new OrderBy(false, true);
        order.Add(ContractBenchtopEdgeProfilesTable.name, OrderByItem.OrderDir.Asc);
        string where = "status_id=1 AND benchtop_id=" + benchtopCombo.SelectedValue.ToString();

        edgeProfileCombo.DataSource = ContractBenchtopEdgeProfilesTable.GetRecords(where, order);
        edgeProfileCombo.DataTextField = "name";
        edgeProfileCombo.DataValueField = "id0";
        edgeProfileCombo.DataBind();
        if (edgeProfileCombo.Items.Count < 15)
        {
            int myHeight = edgeProfileCombo.Items.Count * 25;
            edgeProfileCombo.Height = Unit.Pixel(myHeight < 150 ? 150 : myHeight);
        }

    }

    void totalAmt_PreRender(object sender, EventArgs e)
    {
        double markup = 0;
        if (markupCombo.Enabled)
            markup = Convert.ToDouble(markupCombo.SelectedValue);

        double? val = (qtyTextbox.Value * priceTextbox.Value) * (1 + markup);
        double optionTot = 0;
        foreach (contractBenchtopOptionItemTableControlRow row in contractBenchtopOptionItemTableControl.GetRecordControls())
        {
            // Only if we have specified a qty and an item

            if (row.Visible && row.contract_benchtop_option_item.SelectedIndex != 0 && !String.IsNullOrEmpty(row.units.Text))
            {
                double optionAmt;
                //This will return false if it did not succeed
                if (Double.TryParse(row.price.Text,NumberStyles.Currency,CultureInfo.CurrentCulture, out optionAmt))
                {
                    double optionUnits;
                    if (Double.TryParse(row.units.Text,NumberStyles.Currency, CultureInfo.CurrentCulture, out optionUnits))
                    {
                        optionAmt = optionAmt * optionUnits;
                        optionTot += optionAmt;
                    }
                }
            }
        }
        if (val.HasValue)
        {
            val += optionTot;
            totalAmt.Text = val.Value.ToString("$0.00");
        }
        else
            totalAmt.Text = "";
    }

    void benchtopCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        scriptHack.Text = "";
        if (benchtopCombo.SelectedValue == "-1")
        {
            priceTextbox.Enabled = false;
            qtyTextbox.Enabled = false;
            priceTextbox.Value = null;
            qtyTextbox.Value = null;
            supplierBox.Enabled = false;
            supplierBox.Text = "";
            edgeProfileCombo.Enabled = false;
            colourTextbox.Enabled = false;
            edgeThickness.Enabled = false;
            finishTextbox.Enabled = false;
            notesTextbox.Enabled = false;
            notesTextbox.Text = "";
            colourTextbox.Text = "";
            edgeThickness.Text = "";
            finishTextbox.Text = "";

            markupCombo.Enabled = false;
            priceLabel1.Text = "Price";

            return;
        }

        SetBenchtopFields();
        //After setting everything up, lets populate the client selection ONLY if it is empty
        if (supplierBox.Enabled && supplierBox.Text == "" &&
            (benchtopCombo.SelectedItem.Text == "Solid Timber" || benchtopCombo.SelectedItem.Text == "Stainless Steel"))
            supplierBox.Text = "By Client";
    }

    private void SetBenchtopFields()
    {
        ContractBenchtopsRecord benchtop = ContractBenchtopsTable.GetRecord(benchtopCombo.SelectedValue, false);
        if (benchtop != null)
        {
            markupCombo.Enabled = false;
            priceLabel1.Text = "Price";
            if (benchtop.contract_payment_type_id == Globals.PRICETYPE_QUOTE)
            {
                priceTextbox.Enabled = true;
                priceTextbox.ReadOnly = false;
                priceTextbox.Value = 0;

                qtyTextbox.Enabled = true;
                qtyTextbox.ReadOnly = true;
                qtyTextbox.Value = 1;

                supplierBox.Enabled = true;

                markupCombo.Enabled = true;
                priceLabel1.Text = "Cost";
            }
            else if (benchtop.contract_payment_type_id == Globals.PRICETYPE_UNITS)
            {
                priceTextbox.Enabled = true;
                priceTextbox.ReadOnly = true;
                priceTextbox.Value = (double)benchtop.unit_price;
                qtyTextbox.Enabled = true;
                qtyTextbox.ReadOnly = false;
                supplierBox.Enabled = false;
            }
            else if (benchtop.contract_payment_type_id == Globals.PRICETYPE_FIXED)
            {
                priceTextbox.Enabled = true;
                priceTextbox.ReadOnly = true;
                priceTextbox.Value = (double)benchtop.unit_price;

                qtyTextbox.Value = 1;
                qtyTextbox.Enabled = true;
                qtyTextbox.ReadOnly = true;

                supplierBox.Enabled = false;
            }


            BuildEdgeProfileCombo();
            edgeProfileCombo.Enabled = true;
            colourTextbox.Enabled = true;
            edgeThickness.Enabled = true;
            finishTextbox.Enabled = true;
            notesTextbox.Enabled = true;
        }
    }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links


    public void saveButton_Click(object sender, EventArgs args)
    {
        bool success = false;
        string contractID = this.Page.Request["Contract"];
        string editBench = Request["Bench"];
        string errorMsg = "";
        //This is the template item that was selected
        ContractBenchtopsRecord bench = ContractBenchtopsTable.GetRecord(benchtopCombo.SelectedValue, false);
        if (String.IsNullOrEmpty(contractID) || bench == null)
            errorMsg = "Invalid Page Paramaters";
        if (supplierBox.Enabled && String.IsNullOrEmpty(supplierBox.Text))
            errorMsg = "Please specify a supplier";
        if (String.IsNullOrEmpty(colourTextbox.Text) || String.IsNullOrEmpty(finishTextbox.Text) || edgeProfileCombo.SelectedIndex <= 0 || String.IsNullOrEmpty(edgeThickness.Text))
            errorMsg = "Edge profile, edge thickness, colour and finish fields are mandatory";

        if (!String.IsNullOrEmpty(errorMsg))
        {
          //  string jsText = "alert('" + errorMsg + "',250,150,'Error')";
           // scriptHack.Text = Globals.InjectJavascript(jsText);
            scriptHack.Text = "<script> alert('" + errorMsg + "');</script>";
            return;
        }

        try
        {
            DbUtils.StartTransaction();
            ContractBenchtopItemRecord benchTop;
            ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
           
            if (String.IsNullOrEmpty(editBench))
            {
                benchTop = new ContractBenchtopItemRecord();
                benchTop.Parse(contractID, ContractBenchtopItemTable.contract_id);
            }
            else
            {
                benchTop = ContractBenchtopItemTable.GetRecord(editBench, true);
            }
            //Populate the benchtop item with the particulars
            benchTop.item_id = bench.id0;
            benchTop.unit_price = (decimal)priceTextbox.Value;
            benchTop.units = (decimal)qtyTextbox.Value;
            benchTop.options_amount = 0;//Always reset this, it will be recalculated below
            benchTop.supplier = supplierBox.Text;
            benchTop.colour = colourTextbox.Text;
            benchTop.edge_thickness = edgeThickness.Text;
            benchTop.finish = finishTextbox.Text;
            benchTop.notes = notesTextbox.Text;
            benchTop.edge_profile = edgeProfileCombo.SelectedItem.Text;

            if (markupCombo.Enabled)
                benchTop.markup = Convert.ToDecimal(markupCombo.SelectedValue);

            benchTop.Save();

            double optionTot = 0;
            
            foreach (contractBenchtopOptionItemTableControlRow row in contractBenchtopOptionItemTableControl.GetRecordControls())
            {
                if (contractBenchtopOptionItemTableControl.MarkedForDeletion(row))
                {
                    row.Delete();
                }
                else if (row.Visible && row.contract_benchtop_option_item.SelectedIndex != 0 && !String.IsNullOrEmpty(row.units.Text))
                {
                    ContractBenchtopOptionsRecord opt = ContractBenchtopOptionsTable.GetRecord(row.contract_benchtop_option_item.SelectedValue, false);
                    ContractBenchtopOptionItemRecord optRec;
                    if (opt == null)
                        continue;
                    if (row.IsNewRecord)
                    {
                        optRec = new ContractBenchtopOptionItemRecord();
                    }
                    else
                    {
                        optRec = ContractBenchtopOptionItemTable.GetRecord(Globals.GetRecordIDFromXML(row.RecordUniqueId, "id"), true);
                    }
                    optRec.contract_benchtop_item = benchTop.id0;
                    optRec.contract_benchtop_option_item = opt.id0;
                    optRec.price = opt.price;
                    optRec.units = Convert.ToInt32(row.units.Text);
                    optRec.Save();
                    benchTop.options_amount += optRec.amount;
                }
            }
            benchTop.Save();

            Globals.UpdateContract(contract, this.SystemUtils.GetUserID());

            DbUtils.CommitTransaction();
            success = true;
        }
        catch
        {
            success = false;
            DbUtils.RollBackTransaction();
        }
        finally
        {
            DbUtils.EndTransaction();
        }


        if (success)
            scriptHack.Text = "<script> RefreshParentPage() </script>";
        // NOTE: If the Base function redirects to another page, any code here will not be executed.
    }

public void Button1_Click(object sender, EventArgs args)
        {
          
            // Click handler for Button1.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Button1_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.Button1.Button.Click += new EventHandler(Button1_Click);
            this.saveButton.Button.Click += new EventHandler(saveButton_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");
                // Show message on Click
                this.Button1.Button.Attributes.Add("onClick", "CloseWindow()");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
            try {
                // Load data only when displaying the page for the first time
                if ((!this.IsPostBack)) {

                    // Must start a transaction before performing database operations
                    DbUtils.StartTransaction();

                    // Load data for each record and table UI control.
                    // Ordering is important because child controls get 
                    // their parent ids from their parent UI controls.
        
                    this.contractBenchtopOptionItemTableControl.LoadData();
                    this.contractBenchtopOptionItemTableControl.DataBind();           
          
                }
            } catch (Exception ex) {
                // An error has occured so display an error message.
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "Page_Load_Error_Message", ex.Message);
            } finally {
                if (!this.IsPostBack) {
                    // End database transaction
                    DbUtils.EndTransaction();
                }
            }
        
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void Button1_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public void saveButton_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
#endregion

  
}
  
}
  