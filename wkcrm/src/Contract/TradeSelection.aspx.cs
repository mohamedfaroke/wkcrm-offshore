﻿
// This file implements the code-behind class for TradeSelection.aspx.
// App_Code\TradeSelection.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;

        

#endregion

  
namespace WKCRM.UI
{
  
partial class TradeSelection
        : BaseApplicationPage
// Code-behind class for the TradeSelection page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    

        public TradeSelection()
        {
            this.Initialize();
            this.Load += new EventHandler(TradeSelection_Load);
        }

    void TradeSelection_Load(object sender, EventArgs e)
    {
        RemoveCurrentRequestFromSessionNavigationHistory();
        tradeCombo.AppendDataBoundItems = true;
        tradeCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(tradeCombo_SelectedIndexChanged);
        taskCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(taskCombo_SelectedIndexChanged);
        totalAmt.PreRender += new EventHandler(totalAmt_PreRender);
        if (!this.IsPostBack)
        {
            ResetFields();
            tradeCombo.Items.Clear();
            RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
            tradeCombo.Items.Add(myItem);

            OrderBy order = new OrderBy(false, true);
            order.Add(ContractTradesTable.name, OrderByItem.OrderDir.Asc);

            tradeCombo.DataSource = ContractTradesTable.GetRecords("status_id=1",order);
            tradeCombo.DataTextField = "name";
            tradeCombo.DataValueField = "id0";
            tradeCombo.DataBind();
            if (tradeCombo.Items.Count < 15)
            {
                int myHeight = tradeCombo.Items.Count * 25;
                tradeCombo.Height = Unit.Pixel(myHeight < 150 ? 150 : myHeight);
            }
            string taskID = Request["task"];
            if (!String.IsNullOrEmpty(taskID))
            {
                ContractTradeItemRecord taskItem = ContractTradeItemTable.GetRecord(taskID, false);
                if (taskItem != null)
                {
                    ContractTradeTasksRecord task = ContractTradeTasksTable.GetRecord(taskItem.trade_task_id.ToString(), false);
                    if (task != null)
                    {
                        RadComboBoxItem tradeType = tradeCombo.FindItemByValue(task.contract_trade_type.ToString());
                        if (tradeType != null)
                        {
                            tradeType.Selected = true;
                            BuildTasksCombo();
                            RadComboBoxItem taskType = taskCombo.FindItemByValue(task.id0.ToString());
                            if (taskType != null)
                            {
                                taskType.Selected = true;
                                BuildOtherFields();
                                qtyTextbox.Value = (double)taskItem.units;
                                priceTextbox.Value = (double)taskItem.price;
                                notesTextbox.Text = taskItem.notes;
                            }
                        }
                    }
                }
            }
        }
    }

    void totalAmt_PreRender(object sender, EventArgs e)
    {
        if (qtyTextbox.Value != null && priceTextbox.Value != null)
        {
           
            double qty = (double)qtyTextbox.Value;
            double price = (double)priceTextbox.Value;
            double total = qty * price;
            totalAmt.Text = total.ToString("$0.00");
        }
        else
        {
            totalAmt.Text = "";
        }
    }

   

    private void ResetFields()
    {
        qtyTextbox.Enabled = false;
        priceTextbox.Enabled = false;
        qtyTextbox.ReadOnly = true;
        priceTextbox.ReadOnly = true;
        qtyTextbox.Value = 0;
        priceTextbox.Value = 0;
        designerNote.Text = "";
        unitDesc.Text = "";
    }

    void tradeCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (tradeCombo.SelectedValue == "-1")
        {
            taskCombo.SelectedItem.Selected = false;
            taskCombo.Enabled = false;
            ResetFields();
            return;
        }
        BuildTasksCombo();
    }

    void taskCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (taskCombo.SelectedValue == "-1")
        {
            ResetFields();
            return;
        }

        BuildOtherFields();
    }

    private void BuildOtherFields()
    {
        ContractTradeTasksRecord selectedTask = ContractTradeTasksTable.GetRecord(taskCombo.SelectedValue, false);
        switch (selectedTask.contract_payment_type_id)
        {
            case(Globals.PRICETYPE_FIXED):
                priceTextbox.ReadOnly = true;
                priceTextbox.Enabled = true;
                priceTextbox.Value = (double)selectedTask.price;
                qtyTextbox.ReadOnly = true;
                qtyTextbox.Enabled = true;
                qtyTextbox.Value = 1;
                break;
            case(Globals.PRICETYPE_QUOTE):
                priceTextbox.ReadOnly = false;
                priceTextbox.Enabled = true;
                priceTextbox.Value = 0;
                qtyTextbox.ReadOnly = true;
                qtyTextbox.Enabled = true;
                qtyTextbox.Value = 1;
                break;
            case(Globals.PRICETYPE_UNITS):
                priceTextbox.ReadOnly = true;
                priceTextbox.Enabled = true;
                priceTextbox.Value = (double)selectedTask.price;
                qtyTextbox.ReadOnly = false;
                qtyTextbox.Enabled = true;
                qtyTextbox.Value = 1;
                break;
            default:
                break;
        }
        if (selectedTask.IsDefaultItem)
        {
            ContractTradesRecord trade = ContractTradesTable.GetRecord(tradeCombo.SelectedValue, false);
            if (trade != null)
                priceTextbox.Value = (double)trade.default_amount;
        }
        unitDesc.Text = Globals.GetUnitDescription(selectedTask.contract_payment_type_id);
        designerNote.Text = selectedTask.notes;
    }

    private void BuildTasksCombo()
    {
        string whereStr = "contract_trade_type=" + tradeCombo.SelectedValue + " AND part_of_standard_price=0";
        OrderBy order = new OrderBy(false, true);
        order.Add(ContractTradeTasksTable.name, OrderByItem.OrderDir.Asc);

        taskCombo.AppendDataBoundItems = true;
        taskCombo.Items.Clear();
        RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
        taskCombo.Items.Add(myItem);
        taskCombo.DataSource = ContractTradeTasksTable.GetRecords(whereStr,order);
        taskCombo.DataTextField = "name";
        taskCombo.DataValueField = "id0";
        taskCombo.DataBind();
        taskCombo.Enabled = true;

        if (taskCombo.Items.Count < 15)
        {
            int myHeight = taskCombo.Items.Count * 25;
            taskCombo.Height = Unit.Pixel(myHeight < 150 ? 150 : myHeight);
        }

    }



        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links


    public void saveButton_Click(object sender, EventArgs args)
    {

        // Click handler for saveButton.
        // Customize by adding code before the call or replace the call to the Base function with your own code.
        // saveButton_Click_Base(sender, args);
        // NOTE: If the Base function redirects to another page, any code here will not be executed.
        bool success = false;
        string contractID = Request["Contract"];
        string taskID = Request["task"];
        bool qtyValid = qtyTextbox.ReadOnly || (qtyTextbox.Value.HasValue && qtyTextbox.Value > 0);
        bool priceValid = priceTextbox.ReadOnly || (priceTextbox.Value.HasValue && priceTextbox.Value >= 0);
        bool valid = (qtyValid && priceValid);
        if (String.IsNullOrEmpty(contractID) || !valid)
            return;
        try
        {
            DbUtils.StartTransaction();
            bool addStandardItems = true;
            ContractTradeItemRecord tradeRec;
            ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
            ContractTradeTasksRecord task = ContractTradeTasksTable.GetRecord(taskCombo.SelectedValue, false);
            if (String.IsNullOrEmpty(taskID))
                tradeRec = new ContractTradeItemRecord();
            else
            {
                tradeRec = ContractTradeItemTable.GetRecord(taskID, true);
                //If we selected the standard item, and we had already saved it
                // previously (i.e we didn't change it this time)
                // we dont re add the standard items
                if (task.IsDefaultItem && tradeRec.trade_task_id == task.id0)
                    addStandardItems = false;
            }

            tradeRec.contract_id = contract.id0;
            tradeRec.notes = notesTextbox.Text;
            tradeRec.price = (decimal)priceTextbox.Value;
            tradeRec.trade_task_id = task.id0;
            tradeRec.units = (decimal)qtyTextbox.Value;
            tradeRec.Save();

            if (task.IsDefaultItem && addStandardItems)
            {
                //Here we have to create the default items
                string whereStr = "contract_trade_type=" + task.contract_trade_type.ToString() + " AND part_of_standard_price=1";
                ContractTradeItemRecord standardItem;
                foreach (ContractTradeTasksRecord standardTask in ContractTradeTasksTable.GetRecords(whereStr))
                {
                    standardItem = new ContractTradeItemRecord();
                    standardItem.contract_id = contract.id0;
                    standardItem.price = standardTask.price;
                    standardItem.trade_task_id = standardTask.id0;
                    standardItem.units = 1;
                    standardItem.Save();
                }
            }

            Globals.UpdateContract(contract, this.SystemUtils.GetUserID());

            DbUtils.CommitTransaction();
            success = true;
        }
        catch
        {
            success = false;
            DbUtils.RollBackTransaction();
        }
        finally
        {
            DbUtils.EndTransaction();
        }

        if (success)
        {
            scriptHack.Text = "<script> RefreshParentPage() </script>";
        }
    }
public void cancelButton_Click(object sender, EventArgs args)
        {
          
            // Click handler for cancelButton.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            cancelButton_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.cancelButton.Button.Click += new EventHandler(cancelButton_Click);
            this.saveButton.Button.Click += new EventHandler(saveButton_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");
                // Show message on Click
                this.cancelButton.Button.Attributes.Add("onClick", "CloseWindow()");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void cancelButton_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public void saveButton_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
#endregion

  
}
  
}
  