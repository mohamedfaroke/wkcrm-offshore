<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HeightSelection.aspx.cs" Inherits="Contract_HeightSelection" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  
    <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
</head>
<script language="javascript" src="../RadWindow.js"></script>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
    <asp:Label ID="scriptHack" runat="server"></asp:Label>
    <div>
    <br />
        <table>
            <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="priceLabel" runat="server" Text="Ceiling Height"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="heightCeilingValue" runat="server" MinValue="0" AutoPostBack="false">
                        <NumberFormat AllowRounding="false" DecimalDigits="2" />
                    </telerik:RadNumericTextBox></td>
              
            </tr>
             <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="Label1" runat="server" Text="Total Cabinet Height"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="heightCabinetValue" runat="server" MinValue="0" AutoPostBack="false">
                        <NumberFormat AllowRounding="false" DecimalDigits="2" />
                    </telerik:RadNumericTextBox></td>
              
            </tr>
              <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="Label2" runat="server" Text="Bench Height"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="heightBenchValue" runat="server" MinValue="0" AutoPostBack="false">
                        <NumberFormat AllowRounding="false" DecimalDigits="2" />
                    </telerik:RadNumericTextBox></td>
              
            </tr>
              <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="Label3" runat="server" Text="Splashback Height"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="heightSplashbackValue" runat="server" MinValue="0" AutoPostBack="false">
                        <NumberFormat AllowRounding="false" DecimalDigits="2" />
                    </telerik:RadNumericTextBox></td>
               
            </tr>
              <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="Label4" runat="server" Text="Wall Cabinet Height"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="heightWallCabinetValue" runat="server" MinValue="0" AutoPostBack="false">
                        <NumberFormat AllowRounding="false" DecimalDigits="2" />
                    </telerik:RadNumericTextBox></td>
               
            </tr>
              <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="Label5" runat="server" Text="Microwave Height"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="heightMicrowaveValue" runat="server" MinValue="0" AutoPostBack="false">
                        <NumberFormat AllowRounding="false" DecimalDigits="2" />
                    </telerik:RadNumericTextBox></td>
              
            </tr>
              <tr>
                <td style="width: 127px; height: 18px;" align="right">
                    <asp:Label ID="Label6" runat="server" Text="Wall Oven Height"></asp:Label></td>
                <td style="width: 170px; height: 18px;">
                    <telerik:RadNumericTextBox ID="heightWallOvenValue" runat="server" MinValue="0" AutoPostBack="false">
                        <NumberFormat AllowRounding="false" DecimalDigits="2" />
                    </telerik:RadNumericTextBox></td>
              
            </tr>
           
            <tr>
                <td style="height: 20px">
                    <asp:Button ID="saveButton" runat="server" OnClick="saveButton_Click" Text="Save"/></td>
                <td style="height: 20px; width: 170px;">
                    <asp:Button ID="cancelButton" runat="server" Text="Cancel" OnClientClick="CloseWindow()" /></td>
               
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
