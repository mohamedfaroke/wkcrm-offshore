﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="MiscSelection.aspx.cs" Inherits="WKCRM.UI.MiscSelection" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
	<script language="javascript" src="../RadWindow.js"></script>
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
			<asp:Label ID="scriptHack" runat="server"></asp:Label>
		 	 <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="75px"
            Width="75px">
            <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading4.gif") %>'
                style="border: 0px;" />
        </telerik:RadAjaxLoadingPanel>
		 
		 <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" >
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="itemCombo">
                    <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="priceLabel" />
                     <telerik:AjaxUpdatedControl ControlID="priceDesc" />
                     <telerik:AjaxUpdatedControl ControlID="priceTextbox" LoadingPanelID="RadAjaxLoadingPanel1"/>
                     <telerik:AjaxUpdatedControl ControlID="qtyTextbox"  LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="supplierTextbox"  LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="notesTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="totalAmt"  LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="markupLabel" />
                     <telerik:AjaxUpdatedControl ControlID="markupCombo" />

                    </UpdatedControls>
                </telerik:AjaxSetting>
           
                <telerik:AjaxSetting AjaxControlID="categoryCombo">
                    <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SubCategoryCombo"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                     <telerik:AjaxUpdatedControl ControlID="itemCombo"  LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="priceLabel" />
                     <telerik:AjaxUpdatedControl ControlID="priceDesc" />
                     <telerik:AjaxUpdatedControl ControlID="priceTextbox"  LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="qtyTextbox"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                     <telerik:AjaxUpdatedControl ControlID="supplierTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="notesTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="totalAmt"  LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="markupLabel" />
                     <telerik:AjaxUpdatedControl ControlID="markupCombo"  LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                 <telerik:AjaxSetting AjaxControlID="SubCategoryCombo">
                    <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="categoryCombo"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                    <telerik:AjaxUpdatedControl ControlID="SubCategoryCombo"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                     <telerik:AjaxUpdatedControl ControlID="itemCombo"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                     <telerik:AjaxUpdatedControl ControlID="priceLabel" />
                     <telerik:AjaxUpdatedControl ControlID="priceDesc" />
                     <telerik:AjaxUpdatedControl ControlID="priceTextbox"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                     <telerik:AjaxUpdatedControl ControlID="qtyTextbox"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                     <telerik:AjaxUpdatedControl ControlID="supplierTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="notesTextbox" />
                     <telerik:AjaxUpdatedControl ControlID="totalAmt" LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="markupLabel" />
                     <telerik:AjaxUpdatedControl ControlID="markupCombo" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="markupCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="priceTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="qtyTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManagerMisc" runat="server">
		</telerik:RadWindowManager>
        <br />
		
		<table>
		<tr>
		<td><asp:Label ID="Label1" runat="server" Text="Category"></asp:Label></td>
		<td>
		<telerik:RadComboBox ID="categoryCombo" runat="server" AutoPostback="true">
        </telerik:RadComboBox>
		</td>
		<td></td>
		</tr>
         <tr>
		<td><asp:Label ID="subCategoryLabel" Text="Sub Category" runat="server"></asp:Label></td>
		<td>
		<telerik:RadComboBox ID="SubCategoryCombo" runat="server" AutoPostback="true">
        </telerik:RadComboBox>
		</td>
		<td></td>
		</tr>
		<tr>
		<td><asp:Label ID="itemLabel" runat="server"></asp:Label></td>
		<td>
		<telerik:RadComboBox ID="itemCombo" runat="server" AutoPostback="true">
        </telerik:RadComboBox>
		</td>
		<td></td>
		</tr>
		<tr>
                <td >
                </td>
                <td>
                    <asp:Label ID="designerNote" runat="server" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="priceLabel" runat="server" Text="Price"></asp:Label>&nbsp;<asp:Label ID="priceDesc" runat="server" ForeColor="Red"></asp:Label></td>
                <td>
                    <telerik:RadNumericTextBox ID="priceTextbox" runat="server" Culture="English (Australia)"
                        Type="Currency" AutoPostback="true">
                    </telerik:RadNumericTextBox></td>
                <td>
                
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="markupLabel" runat="server" Text="Markup"></asp:Label></td>
                <td>
                    <telerik:RadComboBox ID="markupCombo" runat="server" RadComboBoxImagePosition="Right" AutoPostBack="True">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="0%" Value="0.00" />
                            <telerik:RadComboBoxItem runat="server" Text="5%" Value="0.05" />
                            <telerik:RadComboBoxItem runat="server" Text="10%" Value="0.10" />
                            <telerik:RadComboBoxItem runat="server" Text="15%" Value="0.15" />
                            <telerik:RadComboBoxItem runat="server" Text="20%" Value="0.20" />
                            <telerik:RadComboBoxItem runat="server" Text="25%" Value="0.25" />
                            <telerik:RadComboBoxItem runat="server" Text="30%" Value="0.30" />
                        </Items>
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                        <ExpandAnimation Type="OutQuart" />
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Quantity"></asp:Label></td>
                <td>
                    <telerik:RadNumericTextBox ID="qtyTextbox" runat="server" MinValue="0" AutoPostback="true">
                    </telerik:RadNumericTextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Supplier"></asp:Label></td>
                <td>
                    <telerik:RadTextBox ID="supplierBox" runat="server">
                    </telerik:RadTextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Notes"></asp:Label></td>
                <td>
                    <telerik:RadTextBox ID="notesTextbox" runat="server" Columns="30" Rows="2" TextMode="MultiLine">
                    </telerik:RadTextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Medium" Text="Total"></asp:Label></td>
                <td>
                    <asp:Label ID="totalAmt" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label></td>
                <td>
                </td>
            </tr><tr>
                <td>
</td>
                <td>
</td>
                <td>&nbsp;</td>
            </tr>
             <tr>
                <td style="height: 20px">
                    <asp:Button ID="saveButton" runat="server" Text="Save"/></td>
                <td style="height: 20px; width: 170px;">
                    <asp:Button ID="cancelButton" runat="server" Text="Cancel" OnClientClick="CloseWindow()" /></td>
                <td style="width: 227px; height: 20px">
                </td>
                <td style="height: 20px; width: 71px;">
                </td>
            </tr>
		
		</table>
		
		
		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>
