﻿
// This file implements the code-behind class for Contract.aspx.
// App_Code\Contract.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI
{
  
partial class Contract
        : BaseApplicationPage
// Code-behind class for the Contract page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    

        public Contract()
        {
            this.Initialize();
            this.Load += new EventHandler(Contract_Load);
        }

    void Contract_Load(object sender, EventArgs e)
    {
        contractMainRecordControlUpdateProgress.Visible = false;
        if (!this.IsPostBack)
        {
            //Do some MAGIC here to build a tabstrip
            //I cant be bothered figuring out how to use the datasource/databind
            //method, so i'll so some good ol'iteration
            int i = 0;
            string contractRequest = "?Contract=" + Request["Contract"];
            string contractID = Request["Contract"];
            if(String.IsNullOrEmpty(contractID))
                Response.Redirect("~");
            if (Globals.IsQuotePhase(contractID))
            {
                OrderBy order = new OrderBy(false, true);
                order.Add(ContractItemTypeTable.order_by, OrderByItem.OrderDir.Asc);
                //Add Kitchen heights item
                string name = "Kitchen Heights";
                int type = Globals.CONTRACT_KITCHEN_HEIGHTS;
                RadTab tab = new RadTab(name);
                tab.Value = "HeightSelection.aspx" + contractRequest;
                RadTabStrip1.Tabs.Add(tab);
                //Set client event to set window name
                RadTabStrip1.OnClientTabSelected = "TabSelected";
                foreach (ContractItemTypeRecord rec in ContractItemTypeTable.GetRecords("status_id=1",order))
                {
                    name = rec.name;
                    type = rec.id0;
                    tab = new RadTab(name);
                    switch (type)
                    {
                        case (Globals.CONTRACTITEM_DOORS):
                            tab.Value = "DoorSelection.aspx" + contractRequest;
                            break;
                        case (Globals.CONTRACTITEM_BENCHTOPS):
                            tab.Value = "BenchtopSelection.aspx" + contractRequest + "&rWidth=600";
                            break;
                        case (Globals.CONTRACTITEM_TRADES):
                            tab.Value = "TradeSelection.aspx" + contractRequest;
                            break;
                        case (Globals.CONTRACTITEM_HANDLES):
                            tab.Value = "HandleSelection.aspx" + contractRequest + "&itemType=" + type.ToString();
                            break;
                        case (Globals.CONTRACTITEM_ACCESSORIES):
                        case (Globals.CONTRACTITEM_MISC):
                        case (Globals.CONTRACTITEM_DRAWERS):
                        case (Globals.CONTRACTITEM_FITTINGS):
                        case (Globals.CONTRACTITEM_BLUMPRODUCTS):
                        case (Globals.CONTRACTITEM_CABINETS):
                        case (Globals.CONTRACTITEM_BINS):
                        case (Globals.CONTRACTITEM_GLASS):
                        case (Globals.CONTRACTITEM_WARDROBES):
                            tab.Value = "MiscSelection.aspx" + contractRequest + "&itemType=" + type.ToString();
                            break;
                        default:
                            tab.Value = "OtherSelection.aspx" + contractRequest + "&itemType=" + type.ToString();
                            break;
                    }
                    RadTabStrip1.Tabs.Add(tab);
                }
            }
            else
            {
                //Its not a quote, we need to disable a bunch of buttons
                contractOtherItemDeleteButton.Visible = false;
                contractDoorItemDeleteButton.Visible = false;
                contractBenchtopItemDeleteButton.Visible = false;
                View_ContractTradeItemAndTradeTypeDeleteButton.Visible = false;
                //Also need to check if we need to display a warning about insurance
                // If full insurance, do nothing
                // If only partial insurance, give a warning
                // If NO insurance, give a warning


                //WKRCRM-187: Warning will no longer be displayed to end users
                //if (!Globals.ContractHasValidInsurance(contractID, true))
                //{
                //    //We dont have full insurance, which must mean we have
                //    //partial insurance. If we have NO insurance, we shouldnt even
                //    //be at this stage, so we probably have bigger problems.
                //    scriptHack.Text = "<script> window.alert('Please enter Home Insurance Policy details')</script>";
                //}
            } 
        }
    }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links
    

public void OKButton_Click(object sender, EventArgs args)
        {
          
            // Click handler for OKButton.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            OKButton_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.OKButton.Button.Click += new EventHandler(OKButton_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
            try {
                // Load data only when displaying the page for the first time
                if ((!this.IsPostBack)) {

                    // Must start a transaction before performing database operations
                    DbUtils.StartTransaction();

                    // Load data for each record and table UI control.
                    // Ordering is important because child controls get 
                    // their parent ids from their parent UI controls.
        
                    this.contractMainRecordControl.LoadData();
                    this.contractMainRecordControl.DataBind();           
          
                    this.contractBenchtopItemTableControl.LoadData();
                    this.contractBenchtopItemTableControl.DataBind();           
          
                    this.contractDoorItemTableControl.LoadData();
                    this.contractDoorItemTableControl.DataBind();           
          
                    this.contractOtherItemTableControl.LoadData();
                    this.contractOtherItemTableControl.DataBind();           
          
                    this.View_ContractOtherItemWithTypesTableControl.LoadData();
                    this.View_ContractOtherItemWithTypesTableControl.DataBind();           
          
                    this.View_ContractTradeItemAndTradeTypeTableControl.LoadData();
                    this.View_ContractTradeItemAndTradeTypeTableControl.DataBind();           
          
                }
            } catch (Exception ex) {
                // An error has occured so display an error message.
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "Page_Load_Error_Message", ex.Message);
            } finally {
                if (!this.IsPostBack) {
                    // End database transaction
                    DbUtils.EndTransaction();
                }
            }
        
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void OKButton_Click_Base(object sender, EventArgs args)
        {
            
            bool shouldRedirect = true;
            try {
                
            } catch (Exception ex) {
                shouldRedirect = false;
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
            if (shouldRedirect) {
                this.ShouldSaveControlsToSession = true;
                this.RedirectBack();
            }
        }
          
#endregion

  
}
  
}
  