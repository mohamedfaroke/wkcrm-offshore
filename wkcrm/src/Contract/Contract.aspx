﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Contract.aspx.cs" Inherits="WKCRM.UI.Contract" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.Contract" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title>Contract</title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<script language="javascript" src="../RadWindow.js"></script>
	<script>
	function GetWindowManager()
    {
        var oManager = $find("<%=RadWindowManager1.ClientID %>");
        return oManager;
    }
	</script>
	<body id="Body1" runat="server" class="pBack">
		<asp:Label runat="server" ID="scriptHack"></asp:Label>
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		
				<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
		        <AjaxSettings>
             
                <telerik:AjaxSetting AjaxControlID="View_ContractTradeItemAndTradeTypeDeleteButton">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="contractMainRecordControlUpdatePanel" />
                    </UpdatedControls>
                </telerik:AjaxSetting> 
                 <telerik:AjaxSetting AjaxControlID="contractOtherItemDeleteButton">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="contractMainRecordControlUpdatePanel" />
                    </UpdatedControls>
                </telerik:AjaxSetting> 
                 <telerik:AjaxSetting AjaxControlID="contractBenchtopItemDeleteButton">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="contractMainRecordControlUpdatePanel" />
                    </UpdatedControls>
                </telerik:AjaxSetting> 
                 <telerik:AjaxSetting AjaxControlID="contractDoorItemDeleteButton">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="contractMainRecordControlUpdatePanel" />
                    </UpdatedControls>
                </telerik:AjaxSetting> 
                
            </AjaxSettings>
        </telerik:RadAjaxManager>
		
		
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu">
		</WKCRM:Menu>
					
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							
							<telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Vista"></telerik:RadTabStrip>
							<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Modal="true" Behaviors="Move,Close" VisibleStatusbar="false" ReloadOnShow="true" Width="400px" Height="500px" >
                            <Windows> 
                            <telerik:RadWindow ID="EditWindow" runat="server"></telerik:RadWindow>
                            <telerik:RadWindow ID="StandardWindow" runat="server"></telerik:RadWindow>
                            </Windows>
                            </telerik:RadWindowManager>
							<br />
							
							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						<asp:UpdateProgress runat="server" id="contractMainRecordControlUpdateProgress" AssociatedUpdatePanelID="contractMainRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="contractMainRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:contractMainRecordControl runat="server" id="contractMainRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_view.gif" runat="server" id="contractMainDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="contractMainDialogTitle" Text="Contract Details">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls" style="vertical-align:middle"><br/><asp:Label runat="server" id="viewContractLabel" Text="Print Contract">
														</asp:Label></td>
	<td class="dfv" style="vertical-align:text-bottom"><asp:ImageButton runat="server" id="viewContractButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/file_small_pdf.gif" RedirectURL="../Reports/ShowReport.aspx?reportId=5&amp;recordId={contractMainRecordControl:FV:id}" AlternateText="">

														</asp:ImageButton></td><td class="fls"><asp:Label runat="server" id="Label" Text="&lt;h4>Calculated Total (Inc GST)&lt;/h4>">
														</asp:Label></td><td class="fls"><h3><b><asp:Label runat="server" id="contractTotal" Text="
          ">
														</asp:Label></b></h3></td>
	</tr><tr>
	<td class="fls" style="vertical-align:middle"><asp:Label runat="server" id="Label5" Text="Print Costings">
														</asp:Label></td>
	<td class="dfv" style="vertical-align:text-bottom"><asp:ImageButton runat="server" id="viewCostingsButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/file_small_pdf.gif" RedirectURL="../Reports/ShowReport.aspx?reportId=12&amp;recordId={contractMainRecordControl:FV:id}" AlternateText="">

														</asp:ImageButton></td><td class="fls"><asp:Label runat="server" id="original_totalLabel" Text="&lt;h4>Final Total (Inc GST)&lt;/h4>">
														</asp:Label></td><td class="fls"><h3><b><asp:Literal runat="server" id="original_total">
														</asp:Literal></b></h3></td>
	</tr><tr>
	<td class="fls">
		<asp:Literal runat="server" id="account_idLabel" Text="Account">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="account_id">
														</asp:Literal>
	</td><td class="fls"><asp:Literal runat="server" id="contract_type_idLabel" Text="Contract Type">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="contract_type_id">
														</asp:Literal></td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="date_createdLabel" Text="Date Created">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="date_created">
														</asp:Literal>
	</td><td class="fls"><asp:Literal runat="server" id="created_byLabel" Text="Created By">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="created_by">
														</asp:Literal></td>
	</tr>
	
	
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="date_modifiedLabel" Text="Date Modified">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="date_modified">
														</asp:Literal>
	</td><td class="fls"><asp:Literal runat="server" id="modified_byLabel" Text="Modified By">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="modified_by">
														</asp:Literal></td>
	</tr>
	
	
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="signed_dateLabel" Text="Signed Date">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="signed_date">
														</asp:Literal>
	</td><td class="fls">&nbsp;</td><td class="dfv">&nbsp;</td>
	</tr>
	
	
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="check_measure_feeLabel" Text="Check Measure Fee">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="check_measure_fee">
														</asp:Literal>
	</td><td class="fls"><asp:Label runat="server" id="Label3" Text="Second Check Measure Fee">
														</asp:Label></td><td class="dfv"><asp:Label runat="server" id="second_check_measure_fee">
														</asp:Label></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="difficult_delivery_feeLabel" Text="Difficult/Hard Delivery upto 2 Levels">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="difficult_delivery_fee">
														</asp:Literal></td><td class="fls"><asp:Label runat="server" id="Label2" Text="Dual Material Poly/Colour Board">
														</asp:Label></td><td class="dfv"><asp:Label runat="server" id="dual_finish">
														</asp:Label></td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="HighRiseLabel" Text="High Rise Fee">
														</asp:Label></td>
	<td class="dfv"><asp:Label runat="server" id="highrise_fee">
														</asp:Label></td><td class="fls"><asp:Label runat="server" id="Label4" Text="Dual Material Poly/Veneer Threshold Fee">
														</asp:Label></td><td class="dfv"><asp:Label runat="server" id="dual_finishes_lessthan_fee">
														</asp:Label></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label8" Text="Kickboards fitted after Timber Floor Fee">
														</asp:Label></td>
	<td class="dfv"><asp:Label runat="server" id="kickboardsAfterTimberFee">
														</asp:Label></td><td class="fls"><asp:Label runat="server" id="Label9" Text="Kickboards/integrated dishwasher fitted after Timber Floor Fee">
														</asp:Label></td><td class="dfv"><asp:Label runat="server" id="kickboardsWasherAfterTimber_fee">
														</asp:Label></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="non_metro_delivery_feeLabel" Text="Delivery Fee">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="non_metro_delivery_fee">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="door_threshold_feeLabel" Text="Door Threshold Fee">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="door_threshold_fee">
														</asp:Literal></td>
	</tr>
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="home_insurance_sentLabel" Text="Home Warranty Sent">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="home_insurance_sent">
														</asp:Literal></td><td class="fls"><asp:Label runat="server" id="Label1" Text="Benchtop Threshold Fee">
														</asp:Label></td><td class="dfv"><asp:Label runat="server" id="bench_threshold_fee">
														</asp:Label></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="home_insurance_policyLabel" Text="Home Warranty Policy">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="home_insurance_policy">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="home_insurance_feeLabel" Text="Home Insurance Fee">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="home_insurance_fee">
														</asp:Literal></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label6" Text="Designer Fee">
														</asp:Label></td>
	<td class="dfv"><asp:Label runat="server" id="designer_fee">
														</asp:Label></td><td class="fls"><asp:Label runat="server" id="Label7" Text="Consultation Fee">
														</asp:Label></td><td class="dfv"><asp:Label runat="server" id="consultation_fee">
														</asp:Label></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="estimated_completion_dateLabel" Text="Estimated Completion Date">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="estimated_completion_date">
														</asp:Literal></td><td class="fls"><WKCRM:ThemeButton runat="server" id="editContract" Button-CausesValidation="False" Button-CommandName="Redirect" Button-Text="Edit Contract Details">
														</WKCRM:ThemeButton></td><td class="dfv">&nbsp;</td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
 <%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:contractMainRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									

<table><tr><td>
<asp:UpdateProgress runat="server" id="contractDoorItemTableControlUpdateProgress" AssociatedUpdatePanelID="contractDoorItemTableControlUpdatePanel">
																	<ProgressTemplate>
																		<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																		</div>
																		<div style=" position:absolute; padding:30px;">
																			<img src="../Images/updating.gif">
																		</div>
																	</ProgressTemplate>
																</asp:UpdateProgress>
																<asp:UpdatePanel runat="server" id="contractDoorItemTableControlUpdatePanel" UpdateMode="Conditional">

																	<ContentTemplate>
																		<WKCRM:contractDoorItemTableControl runat="server" id="contractDoorItemTableControl">
																					
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="contractDoorItemTableTitle" Text="Doors">
																					</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="contractDoorItemTotalItems">
																					</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractDoorItemTableControl$contractDoorItemSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractDoorItemTableControl$contractDoorItemSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractDoorItemTableControl$contractDoorItemFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractDoorItemTableControl$contractDoorItemFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="contractDoorItemDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="contractDoorItemToggleAll" onclick="toggleAllCheckboxes(this);">

																						</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="item_idLabel" Text="Item">
																						</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unit_priceLabel" Text="Unit Price">
																						</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unitsLabel" Text="Units">
																						</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="amountLabel" Text="Amount">
																						</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="contractDoorItemTableControlRepeater">
																							<ITEMTEMPLATE>
																									<WKCRM:contractDoorItemTableControlRow runat="server" id="contractDoorItemTableControlRow">
																											
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:Label runat="server" id="doorRowEdit" Text="
          ">
																											</asp:Label></td>
				
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="contractDoorItemRecordRowSelection">

																											</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="item_id">
																											</asp:Literal></td>
			
			<td class="ttc" style=";;text-align:right"><asp:Literal runat="server" id="unit_price">
																											</asp:Literal></td>
			
			<td class="ttc" style=";;text-align:right"><asp:Literal runat="server" id="units">
																											</asp:Literal></td>
			
			<td class="ttc" style=";;text-align:right"><asp:Literal runat="server" id="amount">
																											</asp:Literal></td>
			
			</tr>
			</div>
			
																									</WKCRM:contractDoorItemTableControlRow>
																							</ITEMTEMPLATE>
																					</asp:Repeater>
			<!-- Totals Area -->
			
			
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																		</WKCRM:contractDoorItemTableControl>

																	</ContentTemplate>
																</asp:UpdatePanel>
															
</td>
<td><asp:UpdateProgress runat="server" id="contractBenchtopItemTableControlUpdateProgress" AssociatedUpdatePanelID="contractBenchtopItemTableControlUpdatePanel">
																							<ProgressTemplate>
																								<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																								</div>
																								<div style=" position:absolute; padding:30px;">
																									<img src="../Images/updating.gif">
																								</div>
																							</ProgressTemplate>
																						</asp:UpdateProgress>
																						<asp:UpdatePanel runat="server" id="contractBenchtopItemTableControlUpdatePanel" UpdateMode="Conditional">

																							<ContentTemplate>
																								<WKCRM:contractBenchtopItemTableControl runat="server" id="contractBenchtopItemTableControl">
																											
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="contractBenchtopItemTableTitle" Text="Benchtop Item">
																											</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="contractBenchtopItemTotalItems">
																											</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractBenchtopItemTableControl$contractBenchtopItemSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractBenchtopItemTableControl$contractBenchtopItemSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractBenchtopItemTableControl$contractBenchtopItemFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractBenchtopItemTableControl$contractBenchtopItemFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="contractBenchtopItemDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																												</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="contractBenchtopItemToggleAll" onclick="toggleAllCheckboxes(this);">

																												</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="item_idLabel1" Text="Item">
																												</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unit_priceLabel1" Text="Unit Price">
																												</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unitsLabel1" Text="Units">
																												</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="options_amountLabel" Text="Options Amount">
																												</asp:Literal></th><th class="thc" scope="col"><asp:Literal runat="server" id="amount1Label" Text="Amount">
																												</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="contractBenchtopItemTableControlRepeater">
																													<ITEMTEMPLATE>
																															<WKCRM:contractBenchtopItemTableControlRow runat="server" id="contractBenchtopItemTableControlRow">
																																	
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:Label runat="server" id="benchRowEdit" Text="
          ">
																																	</asp:Label></td>
				
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="contractBenchtopItemRecordRowSelection">

																																	</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="item_id1">
																																	</asp:Literal></td>
			
			<td class="ttc" style=";;;text-align:right"><asp:Literal runat="server" id="unit_price1">
																																	</asp:Literal></td>
			
			<td class="ttc" style=";;;text-align:right"><asp:Literal runat="server" id="units1">
																																	</asp:Literal></td>
			
			<td class="ttc" style=";;;text-align:right"><asp:Literal runat="server" id="options_amount">
																																	</asp:Literal></td><td class="ttc" style=";;;text-align:right"><asp:Literal runat="server" id="amount1">
																																	</asp:Literal></td>
			
			</tr>
			</div>
			
																															</WKCRM:contractBenchtopItemTableControlRow>
																													</ITEMTEMPLATE>
																											</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																								</WKCRM:contractBenchtopItemTableControl>

																							</ContentTemplate>
																						</asp:UpdatePanel>
																					

</td>

</tr>
</table>
<table>
<tr><td>&nbsp;<asp:UpdateProgress runat="server" id="View_ContractOtherItemWithTypesTableControlUpdateProgress" AssociatedUpdatePanelID="View_ContractOtherItemWithTypesTableControlUpdatePanel">
																													<ProgressTemplate>
																														<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																														</div>
																														<div style=" position:absolute; padding:30px;">
																															<img src="../Images/updating.gif">
																														</div>
																													</ProgressTemplate>
																												</asp:UpdateProgress>
																												<asp:UpdatePanel runat="server" id="View_ContractOtherItemWithTypesTableControlUpdatePanel" UpdateMode="Conditional">

																													<ContentTemplate>
																														<WKCRM:View_ContractOtherItemWithTypesTableControl runat="server" id="View_ContractOtherItemWithTypesTableControl">
																																	
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="View_ContractOtherItemWithTypesTableTitle" Text="Splashbacks">
																																	</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="View_ContractOtherItemWithTypesTotalItems">
																																	</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_ContractOtherItemWithTypesTableControl$View_ContractOtherItemWithTypesSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_ContractOtherItemWithTypesTableControl$View_ContractOtherItemWithTypesSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_ContractOtherItemWithTypesTableControl$View_ContractOtherItemWithTypesFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_ContractOtherItemWithTypesTableControl$View_ContractOtherItemWithTypesFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="View_ContractOtherItemWithTypesDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																																		</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			<th class="thc" style="padding:0px;vertical-align:middle;">&nbsp;</th><th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="View_ContractOtherItemWithTypesToggleAll" onclick="toggleAllCheckboxes(this);">

																																		</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="item_idLabel2" Text="Item">
																																		</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unit_priceLabel2" Text="Unit Price">
																																		</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unitsLabel3" Text="Units">
																																		</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="amountLabel2" Text="Amount">
																																		</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="View_ContractOtherItemWithTypesTableControlRepeater">
																																			<ITEMTEMPLATE>
																																					<WKCRM:View_ContractOtherItemWithTypesTableControlRow runat="server" id="View_ContractOtherItemWithTypesTableControlRow">
																																							
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				<td class="tic" scope="row"><asp:Label runat="server" id="splashbackRowEdit" Text="
          ">
																																							</asp:Label></td><td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="View_ContractOtherItemWithTypesRecordRowSelection">

																																							</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="item_id2">
																																							</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="unit_price2">
																																							</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="units4">
																																							</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="amount4">
																																							</asp:Literal></td>
			
			</tr>
			</div>
			
																																					</WKCRM:View_ContractOtherItemWithTypesTableControlRow>
																																			</ITEMTEMPLATE>
																																	</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																														</WKCRM:View_ContractOtherItemWithTypesTableControl>

																													</ContentTemplate>
																												</asp:UpdatePanel>
																											

</td>
<td>&nbsp;</td>

</tr><tr><td><asp:UpdateProgress runat="server" id="contractOtherItemTableControlUpdateProgress" AssociatedUpdatePanelID="contractOtherItemTableControlUpdatePanel">
																																			<ProgressTemplate>
																																				<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																																				</div>
																																				<div style=" position:absolute; padding:30px;">
																																					<img src="../Images/updating.gif">
																																				</div>
																																			</ProgressTemplate>
																																		</asp:UpdateProgress>
																																		<asp:UpdatePanel runat="server" id="contractOtherItemTableControlUpdatePanel" UpdateMode="Conditional">

																																			<ContentTemplate>
																																				<WKCRM:contractOtherItemTableControl runat="server" id="contractOtherItemTableControl">
																																							
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="contractOtherItemTableTitle" Text="Other Items">
																																							</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="contractOtherItemTotalItems">
																																							</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractOtherItemTableControl$contractOtherItemSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractOtherItemTableControl$contractOtherItemSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractOtherItemTableControl$contractOtherItemFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr><tr>
		<td class="fila"><asp:Literal runat="server" id="contract_item_type1Label" Text="Contract Item Type">
																																								</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="contract_item_typeFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
																																								</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractOtherItemTableControl$contractOtherItemFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="contractOtherItemDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																																								</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="contractOtherItemToggleAll" onclick="toggleAllCheckboxes(this);">

																																								</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="contract_item_typeLabel" Text="Contract Item Type">
																																								</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="item_id1Label" Text="Item">
																																								</asp:Literal></th>
			
			<th class="thc" scope="col">&nbsp;<asp:Literal runat="server" id="unit_price1Label" Text="Unit Price">
																																								</asp:Literal></th>
			
			<th class="thc" scope="col">&nbsp;<asp:Literal runat="server" id="units1Label" Text="Units">
																																								</asp:Literal></th><th class="thc" scope="col"><asp:Literal runat="server" id="amount2Label" Text="Amount">
																																								</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="contractOtherItemTableControlRepeater">
																																									<ITEMTEMPLATE>
																																											<WKCRM:contractOtherItemTableControlRow runat="server" id="contractOtherItemTableControlRow">
																																													
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:Label runat="server" id="otherRowEdit" Text="
          ">
																																													</asp:Label></td>
				
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="contractOtherItemRecordRowSelection">

																																													</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="contract_item_type">
																																													</asp:Literal></td>
			
			<td class="ttc" style="text-align: left;"><asp:Literal runat="server" id="item_id3">
																																													</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="unit_price3">
																																													</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;">&nbsp;<asp:Literal runat="server" id="units3">
																																													</asp:Literal></td><td class="ttc" ><asp:Literal runat="server" id="amount3">
																																													</asp:Literal></td>
			
			</tr>
			</div>
			
																																											</WKCRM:contractOtherItemTableControlRow>
																																									</ITEMTEMPLATE>
																																							</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																																				</WKCRM:contractOtherItemTableControl>

																																			</ContentTemplate>
																																		</asp:UpdatePanel>
																																	
</td>
<td>&nbsp;</td>

</tr><tr><td>&nbsp;<asp:UpdateProgress runat="server" id="View_ContractTradeItemAndTradeTypeTableControlUpdateProgress" AssociatedUpdatePanelID="View_ContractTradeItemAndTradeTypeTableControlUpdatePanel">
																																									<ProgressTemplate>
																																										<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																																										</div>
																																										<div style=" position:absolute; padding:30px;">
																																											<img src="../Images/updating.gif">
																																										</div>
																																									</ProgressTemplate>
																																								</asp:UpdateProgress>
																																								<asp:UpdatePanel runat="server" id="View_ContractTradeItemAndTradeTypeTableControlUpdatePanel" UpdateMode="Conditional">

																																									<ContentTemplate>
																																										<WKCRM:View_ContractTradeItemAndTradeTypeTableControl runat="server" id="View_ContractTradeItemAndTradeTypeTableControl">
																																													
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="View_ContractTradeItemAndTradeTypeTableTitle" Text="Trades">
																																													</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="View_ContractTradeItemAndTradeTypeTotalItems">
																																													</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_ContractTradeItemAndTradeTypeTableControl$View_ContractTradeItemAndTradeTypeSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_ContractTradeItemAndTradeTypeTableControl$View_ContractTradeItemAndTradeTypeSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_ContractTradeItemAndTradeTypeTableControl$View_ContractTradeItemAndTradeTypeFilterButton")) %>	
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="TradeLabel" Text="Trade">
																																														</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="TradeFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
																																														</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_ContractTradeItemAndTradeTypeTableControl$View_ContractTradeItemAndTradeTypeFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="View_ContractTradeItemAndTradeTypeDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																																														</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="View_ContractTradeItemAndTradeTypeToggleAll" onclick="toggleAllCheckboxes(this);">

																																														</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="TradeLabel1" Text="Trade">
																																														</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="trade_task_idLabel" Text="Trade Task">
																																														</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="priceLabel" Text="Price">
																																														</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unitsLabel2" Text="Units">
																																														</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="amountLabel1" Text="Amount">
																																														</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="View_ContractTradeItemAndTradeTypeTableControlRepeater">
																																															<ITEMTEMPLATE>
																																																	<WKCRM:View_ContractTradeItemAndTradeTypeTableControlRow runat="server" id="View_ContractTradeItemAndTradeTypeTableControlRow">
																																																			
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:Label runat="server" id="tradeRowEdit" Text="
          ">
																																																			</asp:Label></td>
				
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="View_ContractTradeItemAndTradeTypeRecordRowSelection">

																																																			</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="Trade">
																																																			</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="trade_task_id">
																																																			</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="price">
																																																			</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="units2">
																																																			</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="amount2">
																																																			</asp:Literal></td>
			
			</tr>
			</div>
			
																																																	</WKCRM:View_ContractTradeItemAndTradeTypeTableControlRow>
																																															</ITEMTEMPLATE>
																																													</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																																										</WKCRM:View_ContractTradeItemAndTradeTypeTableControl>

																																									</ContentTemplate>
																																								</asp:UpdatePanel>
																																							
<br/>
<br/>

<br/>
<br/>
</td>
<td>&nbsp;</td>

</tr></table>



<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td><WKCRM:ThemeButton runat="server" id="OKButton" Button-CausesValidation="False" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:OK&quot;, &quot;WKCRM&quot;) %>">
																																								</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	</tr>
	</table>
	</td>
	</tr>
</table>

</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
																																							</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



