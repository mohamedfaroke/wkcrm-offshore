$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2014 Q1',
            boys: 2666,
            girls: null,
            adults: 2647
        }, {
            period: '2014 Q2',
            boys: 2778,
            girls: 2294,
            adults: 2441
        }, {
            period: '2014 Q3',
            boys: 4912,
            girls: 1969,
            adults: 2501
        }, {
            period: '2014 Q4',
            boys: 3767,
            girls: 3597,
            adults: 5689
        }, {
            period: '2015 Q1',
            boys: 6810,
            girls: 1914,
            adults: 2293
        }, {
            period: '2015 Q2',
            boys: 5670,
            girls: 4293,
            adults: 1881
        }, {
            period: '2015 Q3',
            boys: 4820,
            girls: 3795,
            adults: 1588
        }, {
            period: '2015 Q4',
            boys: 15073,
            girls: 5967,
            adults: 5175
        }, {
            period: '2012 Q1',
            boys: 10687,
            girls: 4460,
            adults: 2028
        }, {
            period: '2012 Q2',
            boys: 8432,
            girls: 5713,
            adults: 1791
        }],
        xkey: 'period',
        ykeys: ['boys', 'girls', 'adults'],
        labels: ['Boys', 'Girls', 'Adults'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Evening Class",
            value: 12
        }, {
            label: "Morning Class",
            value: 30
        }, {
            label: "Weekend Class",
            value: 20
        }],
        resize: true
    });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: 'July',
            a: 100,
            b: 90
        }, {
            y: 'Aug',
            a: 75,
            b: 65
        }, {
            y: 'Sep',
            a: 50,
            b: 40
        }, {
            y: 'Oct',
            a: 75,
            b: 65
        }, {
            y: 'Nov',
            a: 50,
            b: 40
        }, {
            y: 'Dec',
            a: 75,
            b: 65
        }, {
            y: 'Jan',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Expected', 'Received'],
        hideHover: 'auto',
        resize: true
    });

});
