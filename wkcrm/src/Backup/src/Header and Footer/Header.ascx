﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Header.ascx.cs" Inherits="WKCRM.UI.Header" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>

<style>

body
{
    margin: 0px;
    text-align:center;
}

table
{
    border-collapse: collapse;
}

td.logout
{
    vertical-align: bottom;
    background-color: #E6D2BA;
    text-align: right;
    padding-right: 10px;
    padding-bottom: 10px;
}

td.footer
{
    font-family: Verdana;
    font-size: 10px;
    color: #6F6F6F;
}

table.box
{
    width: 400px;
    border: 2px solid #E2E2E2;
}

table.box td.title
{
    font: bold 12px/20px Verdana;
    text-transform: uppercase;
    background-color: #D3E3F3;
    padding-left: 10px;
    color: #5C5C5C;
    border: 1px solid #E2E2E2;
}

</style>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
 <tr>
	<td width="720">
	<asp:Image ImageUrl="../Images/Header.png" runat="server" id="_Logo" AlternateText="">

</asp:Image>
	</td>
	<td class="logout">
        <asp:ImageButton ID="logoutButton" runat="server" ImageUrl="../Images/logout.png" BorderWidth="0"/>
        </td>
	
 </tr>
 <tr> 
 <td> <asp:Label ID="welcomeMsg" runat="server" />
 </td>
 </tr>
</table>
<br />