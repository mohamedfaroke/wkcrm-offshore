﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ProjectPhase.aspx.cs" Inherits="WKCRM.UI.ProjectPhase" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.ProjectPhase" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Pagination" Src="../Shared/Pagination.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu3MenuItem">
		</WKCRM:Menu>
					</td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<WKCRM:View_AccountsProjectPhaseTableControl runat="server" id="View_AccountsProjectPhaseTableControl">
					
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="View_AccountsProjectPhaseTableTitle" Text="Project Phase Accounts">
					</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="View_AccountsProjectPhaseTotalItems">
					</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"><%# GetResourceValue("Txt:SearchFor", "WKCRM") %></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_AccountsProjectPhaseTableControl$View_AccountsProjectPhaseSearchButton")) %>
		<asp:TextBox runat="server" id="View_AccountsProjectPhaseSearchArea" CaseSensitive="False" CssClass="Search_Input" Fields="address,suburb,LastName,FirstName">
						</asp:TextBox>
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_AccountsProjectPhaseTableControl$View_AccountsProjectPhaseSearchButton")) %>
		</td>
		<td class="filbc"><WKCRM:ThemeButton runat="server" id="View_AccountsProjectPhaseSearchButton" Button-CausesValidation="False" Button-CommandName="Search" Button-Text="&lt;%# GetResourceValue(&quot;Btn:SearchGoButtonText&quot;, &quot;WKCRM&quot;) %>">
						</WKCRM:ThemeButton></td><td class="filbc"><WKCRM:ThemeButton runat="server" id="Button" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="LeadReviews.aspx" Button-Text="Lead Reviews">
						</WKCRM:ThemeButton>
</td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td><td rowspan="100" class="filbc">&nbsp;</td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("View_AccountsProjectPhaseTableControl$View_AccountsProjectPhaseFilterButton")) %>	
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="designer_idLabel" Text="Designer">
						</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="designer_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
						</asp:DropDownList></td>
	</tr><tr>
		<td class="fila"><asp:Literal runat="server" id="account_status_id1Label" Text="Account Status">
						</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="account_status_idFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
						</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("View_AccountsProjectPhaseTableControl$View_AccountsProjectPhaseFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			<td class="pra">
			<WKCRM:Pagination runat="server" id="View_AccountsProjectPhasePagination">
					</WKCRM:Pagination>
			</td>
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			
			
			<th class="thc" scope="col">&nbsp;</th><th class="thc" scope="col"><asp:LinkButton runat="server" id="LastNameLabel" Text="Surname" CausesValidation="False">
						</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="FirstNameLabel" Text="First Name" CausesValidation="False">
						</asp:LinkButton></th><th class="thc" scope="col"><asp:Label runat="server" id="Label" Text="Contract Number">
						</asp:Label></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="designer_idLabel1" Text="Designer">
						</asp:Literal>&nbsp;</th><th class="thc" scope="col"><asp:Label runat="server" id="Label2" Text="Suburb">
						</asp:Label></th><th class="thc" scope="col">&nbsp;<asp:LinkButton runat="server" id="Label1" Text="Sale Date" CausesValidation="False">
						</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="account_status_idLabel" Text="Account Status">
						</asp:Literal></th>
			
			
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="View_AccountsProjectPhaseTableControlRepeater">
							<ITEMTEMPLATE>
									<WKCRM:View_AccountsProjectPhaseTableControlRow runat="server" id="View_AccountsProjectPhaseTableControlRow">
											
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				
				
			<td class="ttc" ><asp:ImageButton runat="server" id="ImageButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageURL="../Images/file_small_pdf.gif" RedirectURL="../Reports/ShowReport.aspx?reportId=6&amp;recordId={View_AccountsProjectPhaseTableControlRow:FV:id}" ToolTip="Print Account Statement" AlternateText="">

											</asp:ImageButton></td><td class="ttc" ><asp:LinkButton runat="server" id="LastName" CausesValidation="False" CommandName="Redirect" RedirectURL="EditAccountPage.aspx?Account={View_AccountsProjectPhaseTableControlRow:FV:id}">
											</asp:LinkButton></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="FirstName">
											</asp:Literal></td><td class="ttc" ><asp:Label runat="server" id="ContractNumber">
											</asp:Label>&nbsp;</td>
			
			<td class="ttc" ><asp:Literal runat="server" id="designer_id">
											</asp:Literal>&nbsp;</td><td class="ttc" ><asp:Label runat="server" id="Suburb">
											</asp:Label>&nbsp;</td><td class="ttc" ><asp:Label runat="server" id="sale_date">
											</asp:Label>&nbsp;&nbsp;</td>
			
			<td class="ttc"  style="text-align:right"><asp:Literal runat="server" id="account_status_id">
											</asp:Literal>&nbsp;</td>
			
			
			
			</tr>
			</div>
			
									</WKCRM:View_AccountsProjectPhaseTableControlRow>
							</ITEMTEMPLATE>
					</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

		</WKCRM:View_AccountsProjectPhaseTableControl>
	
<br/>
<br/>

<br/>
<br/>


							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
	</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



