﻿<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.ShowAccountPage" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ShowAccountPage.aspx.cs" Inherits="WKCRM.UI.ShowAccountPage" %>
<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu2" Src="../Menu Panels/Menu2.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu7MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td>&nbsp;<WKCRM:Menu2 runat="server" id="Menu1" HiliteSettings="Menu1MenuItem">
		</WKCRM:Menu2></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<asp:UpdateProgress runat="server" id="accountRecordControlUpdateProgress" AssociatedUpdatePanelID="accountRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="accountRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:accountRecordControl runat="server" id="accountRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_view.gif" runat="server" id="accountDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="accountDialogTitle" Text="Show Account">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="date_createdLabel" Text="Date Created">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="date_created">
														</asp:Literal></td><td class="dfv">&nbsp;</td><td class="dfv">&nbsp;</td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="NameLabel" Text="Name">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="Name">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="home_phoneLabel" Text="Home Phone">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="home_phone">
														</asp:Literal></td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="business_phoneLabel" Text="Business Phone">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="business_phone">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="mobileLabel" Text="Mobile">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="mobile">
														</asp:Literal></td>
	</tr>
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="addressLabel" Text="Address">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="address">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="suburbLabel" Text="Suburb">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="suburb">
														</asp:Literal></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="postcodeLabel" Text="Post Code">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="postcode">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="emailLabel" Text="Email">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="email">
														</asp:Literal></td>
	</tr>
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="faxLabel" Text="Fax">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="fax">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="designer_idLabel" Text="Designer">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="designer_id">
														</asp:Literal></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="account_type_idLabel" Text="Account Type">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="account_type_id">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="account_status_idLabel" Text="Account Status">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="account_status_id">
														</asp:Literal></td>
	</tr>
	
	
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="next_review_dateLabel" Text="Next Review Date">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="next_review_date">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="has_been_revivedLabel" Text="Has Been Revived">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="has_been_revived">
														</asp:Literal></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="advertise_source_idLabel" Text="Contact Source">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="advertise_source_id">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="opportunity_source_idLabel" Text="Opportunity Source">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="opportunity_source_id">
														</asp:Literal></td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="revived_reasonLabel" Text="Revived Reason">
														</asp:Literal></td>
	<td class="dfv"><asp:Literal runat="server" id="revived_reason">
														</asp:Literal></td><td class="fls"><asp:Literal runat="server" id="left_reasonLabel" Text="Left Reason">
														</asp:Literal></td><td class="dfv"><asp:Literal runat="server" id="left_reason">
														</asp:Literal></td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
 <%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:accountRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									
<br/>
<br/>
<asp:UpdateProgress runat="server" id="communicationTableControlUpdateProgress" AssociatedUpdatePanelID="communicationTableControlUpdatePanel">
																	<ProgressTemplate>
																		<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																		</div>
																		<div style=" position:absolute; padding:30px;">
																			<img src="../Images/updating.gif">
																		</div>
																	</ProgressTemplate>
																</asp:UpdateProgress>
																<asp:UpdatePanel runat="server" id="communicationTableControlUpdatePanel" UpdateMode="Conditional">

																	<ContentTemplate>
																		<WKCRM:communicationTableControl runat="server" id="communicationTableControl">
																					
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="communicationTableTitle" Text="Communication">
																					</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="communicationTotalItems">
																					</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("communicationTableControl$communicationSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("communicationTableControl$communicationSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("communicationTableControl$communicationFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("communicationTableControl$communicationFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="communicationNewButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../communication/AddCommunicationPage.aspx" Button-Text="&lt;%# GetResourceValue(&quot;Btn:New&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" colspan="2"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="datetimeLabel1" Text="Date Time" CausesValidation="False">
																						</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="employee_idLabel1" Text="Employee">
																						</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="notesLabel" Text="Notes">
																						</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="communicationTableControlRepeater">
																							<ITEMTEMPLATE>
																									<WKCRM:communicationTableControlRow runat="server" id="communicationTableControlRow">
																											
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:ImageButton runat="server" id="communicationRecordRowEditButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_edit.gif" RedirectURL="../communication/EditCommunicationPage.aspx?Communication={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:EditRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																											</asp:ImageButton></td>
				<td class="tic"><asp:ImageButton runat="server" id="communicationRecordRowViewButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_view.gif" RedirectURL="../communication/ShowCommunicationPage.aspx?Communication={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:ViewRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																											</asp:ImageButton></td>
				
				
			<td class="ttc" ><asp:Literal runat="server" id="datetime1">
																											</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="employee_id">
																											</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="notes">
																											</asp:Literal></td>
			
			</tr>
			</div>
			
																									</WKCRM:communicationTableControlRow>
																							</ITEMTEMPLATE>
																					</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																		</WKCRM:communicationTableControl>

																	</ContentTemplate>
																</asp:UpdatePanel>
																				<asp:UpdateProgress runat="server" id="leadMeetingsTableControlUpdateProgress" AssociatedUpdatePanelID="leadMeetingsTableControlUpdatePanel">
																							<ProgressTemplate>
																								<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																								</div>
																								<div style=" position:absolute; padding:30px;">
																									<img src="../Images/updating.gif">
																								</div>
																							</ProgressTemplate>
																						</asp:UpdateProgress>
																						<asp:UpdatePanel runat="server" id="leadMeetingsTableControlUpdatePanel" UpdateMode="Conditional">

																							<ContentTemplate>
																								<WKCRM:leadMeetingsTableControl runat="server" id="leadMeetingsTableControl">
																											
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="leadMeetingsTableTitle" Text="Meetings">
																											</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="leadMeetingsTotalItems">
																											</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("leadMeetingsTableControl$leadMeetingsSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("leadMeetingsTableControl$leadMeetingsSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("leadMeetingsTableControl$leadMeetingsFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("leadMeetingsTableControl$leadMeetingsFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="leadMeetingsNewButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Shared/ConfigureAddRecord.aspx" Button-Text="&lt;%# GetResourceValue(&quot;Btn:New&quot;, &quot;WKCRM&quot;) %>">
																												</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" colspan="2"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="datetimeLabel" Text="Date Time" CausesValidation="False">
																												</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="designer_idLabel1" Text="Designer">
																												</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="addressLabel1" Text="Address">
																												</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="onsiteLabel" Text="Onsite">
																												</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="leadMeetingsTableControlRepeater">
																													<ITEMTEMPLATE>
																															<WKCRM:leadMeetingsTableControlRow runat="server" id="leadMeetingsTableControlRow">
																																	
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:ImageButton runat="server" id="leadMeetingsRecordRowEditButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_edit.gif" RedirectURL="../leadMeetings/EditleadMeetingsPage.aspx?LeadMeetings={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:EditRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																																	</asp:ImageButton></td>
				<td class="tic"><asp:ImageButton runat="server" id="leadMeetingsRecordRowViewButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_view.gif" RedirectURL="../leadMeetings/ShowleadMeetingsPage.aspx?LeadMeetings={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:ViewRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																																	</asp:ImageButton></td>
				
				
			<td class="ttc" ><asp:Literal runat="server" id="datetime2">
																																	</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="designer_id1">
																																	</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="address1">
																																	</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="onsite">
																																	</asp:Literal></td>
			
			</tr>
			</div>
			
																															</WKCRM:leadMeetingsTableControlRow>
																													</ITEMTEMPLATE>
																											</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																								</WKCRM:leadMeetingsTableControl>

																							</ContentTemplate>
																						</asp:UpdatePanel>
																										<asp:UpdateProgress runat="server" id="attachmentTableControlUpdateProgress" AssociatedUpdatePanelID="attachmentTableControlUpdatePanel">
																													<ProgressTemplate>
																														<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																														</div>
																														<div style=" position:absolute; padding:30px;">
																															<img src="../Images/updating.gif">
																														</div>
																													</ProgressTemplate>
																												</asp:UpdateProgress>
																												<asp:UpdatePanel runat="server" id="attachmentTableControlUpdatePanel" UpdateMode="Conditional">

																													<ContentTemplate>
																														<WKCRM:attachmentTableControl runat="server" id="attachmentTableControl">
																																	
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="attachmentTableTitle" Text="Attachment">
																																	</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="attachmentTotalItems">
																																	</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("attachmentTableControl$attachmentSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("attachmentTableControl$attachmentSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("attachmentTableControl$attachmentFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("attachmentTableControl$attachmentFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="attachmentNewButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../attachment/AddattachmentPage.aspx" Button-Text="&lt;%# GetResourceValue(&quot;Btn:New&quot;, &quot;WKCRM&quot;) %>">
																																		</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="attachmentDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																																		</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" COLSPAN="1"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="attachmentToggleAll" onclick="toggleAllCheckboxes(this);">

																																		</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="nameLabel1" Text="Name">
																																		</asp:Literal></th>
			
			
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="attachmentTableControlRepeater">
																																			<ITEMTEMPLATE>
																																					<WKCRM:attachmentTableControlRow runat="server" id="attachmentTableControlRow">
																																							
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:Label runat="server" id="ViewAttachmentLabel" Text="
          ">
																																							</asp:Label></td>
				
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="attachmentRecordRowSelection">

																																							</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="name1">
																																							</asp:Literal></td>
			
			
			
			</tr>
			</div>
			
																																					</WKCRM:attachmentTableControlRow>
																																			</ITEMTEMPLATE>
																																	</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																														</WKCRM:attachmentTableControl>

																													</ContentTemplate>
																												</asp:UpdatePanel>
																											






<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td><WKCRM:ThemeButton runat="server" id="OKButton" Button-CausesValidation="False" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:OK&quot;, &quot;WKCRM&quot;) %>">
																												</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	</tr>
	</table>
	</td>
	</tr>
</table>



							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						

</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
																											</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



