﻿<%@ Page Language="cs" AutoEventWireup="true" EnableEventValidation="false" CodeFile="LargeListSelector.aspx.cs" Inherits="WKCRM.UI.LargeListSelector" %>
<%@ Register Tagprefix="WKCRM" TagName="Pagination" Src="Pagination.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.LargeListSelector" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css" />
  </head>
  <body id="Body1" runat="server">
	<form id="Form1" method="post" runat="server">
          <BaseClasses:BasePageSettings id="PageSettings" runat="server" />
          
<WKCRM:ItemsTable runat="server" id="ItemsTable">
			
	<table border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td colspan="2" align="center">
<!-- BEGIN Search -->
<table>
 <tr>
  <td>
   <table>
    <%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("ItemsTable$SearchButton")) %>
    <tr>
     <td class="filter_area">
      Starts with
     </td>
     <td>
      <asp:TextBox runat="server" id="StartsWith" CssClass="Search_Input" />
     </td>
    </tr>
    <tr>
     <td class="filter_area">
      Contains
     </td>
     <td>
      <asp:TextBox runat="server" id="Contains" CssClass="Search_Input" />
     </td>
    </tr>
    <%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("ItemsTable$SearchButton")) %>
   </table>
  </td>
  <td style="vertical-align:middle;">
   <WKCRM:ThemeButton runat="server" id="SearchButton" Button-CausesValidation="False" Button-CommandName="Search" Button-Text="<%# GetResourceValue(&quot;Btn:SearchGoButtonText&quot;, &quot;WKCRM&quot;) %>">
			</WKCRM:ThemeButton>
  </td>
 </tr>
</table>
<!-- END Search -->
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
<!-- BEGIN Pagination -->
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="pagination_row">
<table cellpadding="0" cellspacing="0" border="0">
 <tr>
  <td class="paginationRowEdgeL"><img src="../Images/space.gif" width="20" height="40" alt="" /></td>
  <td class="pagination_area">
	<WKCRM:Pagination runat="server" id="Pagination"></WKCRM:Pagination>
  </td>
  <td class="paginationRowEdgeR"><img src="../Images/space.gif" width="20" height="40" alt="" /></td>
 </tr>
</table>
</td>
</tr>
</table>
<!-- END Pagination -->
		</td>
		</tr>
		<asp:Repeater runat="server" id="Row1">
					<ITEMTEMPLATE>
							<WKCRM:ItemsTableRecordControl runat="server" id="ItemsTableRecordControl">
									
			<tr>
				<td class="header_cell" style="text-align:center;">
					<div class="column_header"><asp:Label runat="server" ID="ValueText" Visible="false"></asp:Label><asp:LinkButton runat="server" id="SelectButton" CausesValidation="False" Text="Select"></asp:LinkButton></div>
				</td>
				<td class="table_cell">
					<asp:Label runat="server" id="ItemText"></asp:Label>
				</td>
			</tr>
		
							</WKCRM:ItemsTableRecordControl>
					</ITEMTEMPLATE>
			</asp:Repeater>
	</table>

</WKCRM:ItemsTable>
 </form>
  </body>
</html>