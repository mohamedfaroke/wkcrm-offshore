using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WKCRM.Business;
using Telerik.Web.UI;

public partial class Shared_Appointment : System.Web.UI.UserControl
{
    private Appointment _apt;
    public AppointmentType AppType;

    public Appointment Appointment
    {
        get { return _apt; }
        set { _apt = value; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (aptSubject != null && _apt != null)
        {
            aptSubject.Text = _apt.Subject;
        }
        if (aptIcon != null && _apt != null)
        {
            string appointmentID = _apt.ID.ToString();
            if (appointmentID.StartsWith(Globals.APPLEADMEETING_PREFIX) || AppType == AppointmentType.LEADMEETING)
            {
                aptIcon.ImageUrl = Globals.GetAppointmentImage("meeting", false);
            }
            else if (appointmentID.StartsWith(Globals.APPPROJECTTASK_PREFIX) || AppType == AppointmentType.PROJECTTASK)
            {
                string aptID = appointmentID.Replace(Globals.APPPROJECTTASK_PREFIX, "");
                TasksRecord rec = TasksTable.GetRecord(aptID, false);
                AppointmentTaskWrapper apt = new AppointmentTaskWrapper(rec);        
                aptIcon.ImageUrl = Globals.GetAppointmentImage(apt.TaskType, false);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
