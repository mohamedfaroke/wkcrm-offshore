﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ShowTradesmenPage.aspx.cs" Inherits="WKCRM.UI.ShowTradesmenPage" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.ShowTradesmenPage" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu1" Src="../Menu Panels/Menu1.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu11MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td>&nbsp;<WKCRM:Menu1 runat="server" id="Menu1" HiliteSettings="Menu6MenuItem">
		</WKCRM:Menu1></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<asp:UpdateProgress runat="server" id="tradesmenRecordControlUpdateProgress" AssociatedUpdatePanelID="tradesmenRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="tradesmenRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:tradesmenRecordControl runat="server" id="tradesmenRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_view.gif" runat="server" id="tradesmenDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="tradesmenDialogTitle" Text="Show Tradesmen">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="nameLabel" Text="Name">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="name">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="dobLabel" Text="Date Of Birth">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="dob">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="phoneLabel" Text="Phone">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="phone">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="mobileLabel" Text="Mobile">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="mobile">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="faxLabel" Text="Fax">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="fax">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="addressLabel" Text="Address">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="address">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="emailLabel" Text="Email">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="email">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="send_email_notificationsLabel" Text="Send Email Notifications">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="send_email_notifications">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="entity_type_idLabel" Text="Entity Type">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="entity_type_id">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="public_liability_numberLabel" Text="Public Liability Number">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="public_liability_number">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="public_liability_expiry_dateLabel" Text="Public Liability Expiration Date">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="public_liability_expiry_date">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="workers_comp_numberLabel" Text="Workers Company Number">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="workers_comp_number">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="workers_comp_expiry_dateLabel" Text="Workers Company Expiration Date">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="workers_comp_expiry_date">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="trade_licence_numberLabel" Text="Trade Licence Number">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="trade_licence_number">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="trade_licence_expiry_dateLabel" Text="Trade Licence Expiration Date">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="trade_licence_expiry_date">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="status_idLabel" Text="Active">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="status_id">
														</asp:Literal>
	</td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
 <%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:tradesmenRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									
<br/>
<br/>


<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td><WKCRM:ThemeButton runat="server" id="OKButton" Button-CausesValidation="False" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:OK&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	</tr>
	</table>
	</td>
	</tr>
</table>

<br/>
<br/>

							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
									</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



