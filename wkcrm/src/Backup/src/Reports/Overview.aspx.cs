﻿
// This file implements the code-behind class for Overview.aspx.
// App_Code\Overview.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI
{
  
partial class Overview
        : BaseApplicationPage
// Code-behind class for the Overview page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    

        public Overview()
        {
            this.Initialize();
            this.Load += new EventHandler(Overview_Load);
            this.Init += new EventHandler(Overview_Init);
        }

        void Overview_Init(object sender, EventArgs e)
        {
            RadScheduler1.AppointmentCreated += new AppointmentCreatedEventHandler(RadScheduler1_AppointmentCreated);
            RadScheduler1.DataBound += new EventHandler(RadScheduler1_DataBound);
            RadToolTipManager1.AjaxUpdate += new ToolTipUpdateEventHandler(RadToolTipManager1_AjaxUpdate);
        }

        void RadScheduler1_DataBound(object sender, EventArgs e)
        {
            RadToolTipManager1.TargetControls.Clear();
        }

        private bool IsAppointmentRegisteredForTooltip(Appointment apt)
        {
            foreach (ToolTipTargetControl targetControl in RadToolTipManager1.TargetControls)
            {
                if (apt.DomElements.Contains(targetControl.TargetControlID))
                {
                    return true;
                }
            }
            return false;
        }


        void Overview_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Globals.SetSchedulerSettings(RadScheduler1, GetSchedulerDataSource());
                RadScheduler1.Width = new Unit(800, UnitType.Pixel);
            }
            else
                RadScheduler1.DataSource = GetSchedulerDataSource();

            RadToolTipManager1.AutoCloseDelay = 6000;
            bool showTaskApts = !String.IsNullOrEmpty(Request["TaskApts"]);
            Menu1.HiliteSettings = !showTaskApts ? "Menu1MenuItem" : "Menu2MenuItem";
        }

    private ArrayList GetSchedulerDataSource()
    {
        bool showTaskApts = !String.IsNullOrEmpty(Request["TaskApts"]);
        return showTaskApts ? Globals.GetAllProjectTasks() : Globals.GetAllLeadMeetings();
    }

    void RadScheduler1_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
    {
        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
        {
            bool showTaskApts = !String.IsNullOrEmpty(Request["TaskApts"]);
            Shared_Appointment apt = e.Container.FindControl("aptDetails") as Shared_Appointment;
            if (apt != null)
            {
                apt.Appointment = e.Appointment;
                apt.AppType = showTaskApts ? AppointmentType.PROJECTTASK : AppointmentType.LEADMEETING;
            }
            string aptId = e.Appointment.ID.ToString();
            foreach (string domElementId in e.Appointment.DomElements)
            {
                RadToolTipManager1.TargetControls.Add(domElementId, aptId, true);
            }

        }
    }

    public void RadToolTipManager1_AjaxUpdate(object sender, ToolTipUpdateEventArgs e)
    {
        try
        {
            bool showTaskApts = !String.IsNullOrEmpty(Request["TaskApts"]);
            Appointment apt = RadScheduler1.Appointments.FindByID(e.Value);
            Shared_AppointmentToolTip tootip = LoadControl("../Shared/AppointmentToolTip.ascx") as Shared_AppointmentToolTip;
            AppointmentBase myApt = null;
            string appointmentID = apt.ID.ToString();
            if (!showTaskApts)
            {
               // string aptID = appointmentID.Replace(Globals.APPLEADMEETING_PREFIX, "");
                LeadMeetingsRecord rec = LeadMeetingsTable.GetRecord(appointmentID, false);
                myApt = new AppointmentLeadWrapper(rec);
            }
            else if (showTaskApts)
            {
              //  string aptID = appointmentID.Replace(Globals.APPPROJECTTASK_PREFIX, "");
                TasksRecord rec = TasksTable.GetRecord(appointmentID, false);
                myApt = new AppointmentTaskWrapper(rec);
            }
            tootip.TargetAppointment = myApt;
            e.UpdatePanel.ContentTemplateContainer.Controls.Add(tootip);
        }
        catch
        {
        }
    }


        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links
    

#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
#endregion

  
}
  
}
  