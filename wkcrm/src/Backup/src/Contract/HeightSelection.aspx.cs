using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Telerik.Web.UI;
using WKCRM.Business;
using System.Text.RegularExpressions;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Web.UI;

public partial class Contract_HeightSelection : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RemoveCurrentRequestFromSessionNavigationHistory();

        if (!this.IsPostBack)
        {
            string contractId = Request["contract"];
            if (!String.IsNullOrEmpty(contractId))
            {
                SetupFields();
            }
        }
    }



    private void SetupFields()
    {
        string contractID = Request["Contract"];
        ContractMainRecord contract = ContractMainTable.GetRecord(contractID, false);
        if(contract != null)
        {
            heightCeilingValue.Value = Convert.ToDouble(contract.height_ceiling);
            heightCabinetValue.Value = Convert.ToDouble(contract.height_cabinet);
            heightBenchValue.Value = Convert.ToDouble(contract.height_bench);
            heightSplashbackValue.Value = Convert.ToDouble(contract.height_splashback);
            heightWallCabinetValue.Value = Convert.ToDouble(contract.height_wallcabinet);
            heightMicrowaveValue.Value = Convert.ToDouble(contract.height_microwave);
            heightWallOvenValue.Value = Convert.ToDouble(contract.height_walloven);
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        string errorMsg = "";
        string contractID = Request["Contract"];
        bool valid = true;

        if (!String.IsNullOrEmpty(errorMsg))
        {
           // string jsText = "radalert('" + errorMsg + "',250,150,'Error')";
            scriptHack.Text = "<script> alert('" + errorMsg + "');</script>";
            return;
        }


        if (valid && !String.IsNullOrEmpty(contractID))
        {
            bool success = false;

            try
            {
                DbUtils.StartTransaction();
                ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);

                contract.height_ceiling = heightCeilingValue.Value.HasValue ? Convert.ToDecimal(heightCeilingValue.Value) : 0;
                contract.height_cabinet = heightCabinetValue.Value.HasValue ? Convert.ToDecimal(heightCabinetValue.Value) : 0;
                contract.height_bench = heightBenchValue.Value.HasValue ? Convert.ToDecimal(heightBenchValue.Value) : 0;
                contract.height_splashback = heightSplashbackValue.Value.HasValue ? Convert.ToDecimal(heightSplashbackValue.Value) : 0;
                contract.height_wallcabinet = heightWallCabinetValue.Value.HasValue ? Convert.ToDecimal(heightWallCabinetValue.Value) : 0;
                contract.height_microwave = heightMicrowaveValue.Value.HasValue ? Convert.ToDecimal(heightMicrowaveValue.Value) : 0;
                contract.height_walloven = heightWallOvenValue.Value.HasValue ? Convert.ToDecimal(heightWallOvenValue.Value) : 0;
                //Save is called in the below function
                Globals.UpdateContract(contract, this.SystemUtils.GetUserID());

                DbUtils.CommitTransaction();
                success = true;
            }
            catch
            {
                success = false;
                DbUtils.RollBackTransaction();
            }
            finally
            {
                DbUtils.EndTransaction();
            }
            if (success)
            {
                scriptHack.Text = "<script> RefreshParentPage() </script>";
            }
        }
    }
}
