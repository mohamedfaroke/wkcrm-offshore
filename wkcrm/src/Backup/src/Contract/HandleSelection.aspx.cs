using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Telerik.Web.UI;
using WKCRM.Business;
using System.Text.RegularExpressions;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Web.UI;

public partial class Contract_HandleSelection : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RemoveCurrentRequestFromSessionNavigationHistory();
        RadComboBox1.AppendDataBoundItems = true;
        RadComboBox1.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(RadComboBox1_SelectedIndexChanged);
        totalAmt.PreRender += new EventHandler(totalAmt_PreRender);
        RadComboBox1.MarkFirstMatch = true;
        RadComboBox1.Focus();

        if (!this.IsPostBack)
        {
            //We know we are loading handles, but just do it the same way 
            ResetFields();
            string itmType = Request["itemType"];
            string itmId = Request["itemId"];
            if (!String.IsNullOrEmpty(itmType))
            {
                ContractItemTypeRecord itemRec = ContractItemTypeTable.GetRecord(itmType, false);
                if (itemRec != null)
                {

                    RadComboBox1.Items.Clear();
                    RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
                    RadComboBox1.Items.Add(myItem);

                    OrderBy order = new OrderBy(false, true);
                    order.Add(ContractOthersTable.name, OrderByItem.OrderDir.Asc);
                    string where = "contract_item_type=" + itmType + " AND status_id=1";

                    RadComboBox1.DataSource = ContractOthersTable.GetRecords(where, order);
                    RadComboBox1.DataTextField = "name";
                    RadComboBox1.DataValueField = "id0";
                    RadComboBox1.DataBind();
                    if (RadComboBox1.Items.Count < 15)
                    {
                        int myHeight = RadComboBox1.Items.Count * 25;
                        RadComboBox1.Height = Unit.Pixel(myHeight < 150 ? 150 : myHeight);
                    }
                }
                //We are preloading a record from the database
                if (!String.IsNullOrEmpty(itmId))
                {

                    ContractOtherItemRecord otherItemRec = ContractOtherItemTable.GetRecord(itmId, false);
                    RadComboBoxItem boxItem = RadComboBox1.FindItemByValue(otherItemRec.item_id.ToString());
                    if (boxItem != null)
                    {
                        boxItem.Selected = true;
                    }
                    SetupFields();
                    priceTextbox.Value = (double)otherItemRec.unit_price;
                    qtyTextbox.Value = (double)otherItemRec.units;
                    supplierTextbox.Text = otherItemRec.supplier;
                    //Do the magic parsing here
                    ParseNoteField(otherItemRec.notes);
                    if (markupCombo.Enabled)
                    {
                        RadComboBoxItem itm = markupCombo.FindItemByValue(otherItemRec.markup.ToString());
                        if (itm != null)
                            itm.Selected = true;
                    }
                }
            }
        }
    }

    void totalAmt_PreRender(object sender, EventArgs e)
    {

        double markup = 0;
        if (markupCombo.Enabled)
            markup = Convert.ToDouble(markupCombo.SelectedValue);

        double? val = (qtyTextbox.Value * priceTextbox.Value) * (1 + markup);
        if (val.HasValue)
            totalAmt.Text = val.Value.ToString("$0.00");
        else
            totalAmt.Text = "";
    }

    void RadComboBox1_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (RadComboBox1.SelectedValue == "-1")
        {
            ResetFields();
            return;
        }

        SetupFields();
    }
    private void ResetFields()
    {
        priceTextbox.Enabled = false;
        qtyTextbox.Enabled = false;
        supplierTextbox.Enabled = false;
        priceTextbox.Value = null;
        qtyTextbox.Value = null;
        supplierTextbox.Text = "";
        designerNote.Text = "";
        doorCombo.Enabled = false;
        drawCombo.Enabled = false;

        markupCombo.Enabled = false;

        priceLabel.Text = "Price";
    }

    private void SetupFields()
    {
        ContractOthersRecord otherRec = ContractOthersTable.GetRecord(RadComboBox1.SelectedValue, false);
        markupCombo.Enabled = false;
        if (otherRec != null)
        {
            switch (otherRec.contract_payment_type)
            {
                case (Globals.PRICETYPE_FIXED):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = true;
                    priceTextbox.Value = (double)otherRec.price;

                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = true;
                    qtyTextbox.Value = 1;

                    supplierTextbox.Enabled = false;
                    break;
                case (Globals.PRICETYPE_QUOTE):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = false;
                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = false;
                    priceTextbox.Value = 0;
                    qtyTextbox.Value = 1;
                    supplierTextbox.Enabled = true;

                    markupCombo.Enabled = true;
                    priceLabel.Text = "Cost";

                    break;
                case (Globals.PRICETYPE_UNITS):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = true;
                    priceTextbox.Value = (double)otherRec.price;
                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = false;
                    qtyTextbox.Value = 1;
                    supplierTextbox.Enabled = false;
                    break;
                default:
                    priceTextbox.Enabled = false;
                    qtyTextbox.Enabled = false;
                    supplierTextbox.Enabled = false;
                    priceTextbox.Value = 0;
                    qtyTextbox.Value = 0;
                    break;
            }
            priceDesc.Text = Globals.GetUnitDescription(otherRec.contract_payment_type);
            designerNote.Text = otherRec.notes;
            doorCombo.Enabled = true;
            drawCombo.Enabled = true;


        }
    }

    private string BuildNoteField()
    {
        string longNote = "";

        longNote += "Style No: " + styleTextbox.Text + "\n";
        longNote += "Door Pos: " + doorCombo.SelectedValue + "\n";
        longNote += "Draw Pos: " + drawCombo.SelectedValue + "\n";
        longNote += notesTextbox.Text;
        return longNote;
    }

    private void ParseNoteField(string note)
    {
        string myNote = note;
        Regex myRegex = new Regex(@"Style No: ((.*)\n)?");
        Match myMatch = myRegex.Match(myNote);
        if (myMatch.Success)
        {
            styleTextbox.Text = myMatch.Groups[2].Value;
            myNote = myNote.Replace(myMatch.Value, "");
        }

        myRegex = new Regex(@"Door Pos: ((.*)\n)?");
        myMatch = myRegex.Match(myNote);
        if (myMatch.Success)
        {
            RadComboBoxItem itm = doorCombo.FindItemByValue(myMatch.Groups[2].Value);
            if (itm != null)
                itm.Selected = true;
            myNote = myNote.Replace(myMatch.Value, "");
        }

        myRegex = new Regex(@"Draw Pos: ((.*)\n)?");
        myMatch = myRegex.Match(myNote);
        if (myMatch.Success)
        {
            RadComboBoxItem itm = drawCombo.FindItemByValue(myMatch.Groups[2].Value);
            if (itm != null)
                itm.Selected = true;
            myNote = myNote.Replace(myMatch.Value, "");
        }
        notesTextbox.Text = myNote;
    }


    protected void saveButton_Click(object sender, EventArgs e)
    {
        bool qtyValid = qtyTextbox.ReadOnly || (qtyTextbox.Value.HasValue && qtyTextbox.Value > 0);
        bool priceValid = priceTextbox.ReadOnly || (priceTextbox.Value.HasValue && priceTextbox.Value >= 0);
        bool valid = (qtyValid && priceValid);
        string contractID = Request["Contract"];
        string itemID = Request["ItemId"];
        string errorMsg = "";

        if (supplierTextbox.Enabled && String.IsNullOrEmpty(supplierTextbox.Text))
            errorMsg = "Please specify a supplier";
        if (!valid)
            errorMsg = "Invalid Price or Quantity";
        if (String.IsNullOrEmpty(styleTextbox.Text))
            errorMsg = "Please specify a style";
        if (String.IsNullOrEmpty(contractID))
            errorMsg = "Invalid Page Paramaters";

        if (!String.IsNullOrEmpty(errorMsg))
        {
           // string jsText = "radalert('" + errorMsg + "',250,150,'Error')";
            scriptHack.Text = "<script> alert('" + errorMsg + "');</script>";
            return;
        }


        if (valid && !String.IsNullOrEmpty(contractID))
        {
            bool success = false;

            try
            {
                DbUtils.StartTransaction();
                ContractOtherItemRecord otherItemRec;
                ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
                ContractOthersRecord otherItem = ContractOthersTable.GetRecord(RadComboBox1.SelectedValue, false);
                if (String.IsNullOrEmpty(itemID))
                    otherItemRec = new ContractOtherItemRecord();
                else
                    otherItemRec = ContractOtherItemTable.GetRecord(itemID, true);

                otherItemRec.contract_id = contract.id0;
                otherItemRec.item_id = otherItem.id0;
                otherItemRec.unit_price = (decimal)priceTextbox.Value;
                otherItemRec.units = (decimal)qtyTextbox.Value;

                if (markupCombo.Enabled)
                    otherItemRec.markup = Convert.ToDecimal(markupCombo.SelectedValue);

                otherItemRec.notes = BuildNoteField();
                otherItemRec.supplier = supplierTextbox.Text;
                otherItemRec.Save();

                

                Globals.UpdateContract(contract, this.SystemUtils.GetUserID());

                DbUtils.CommitTransaction();
                success = true;
            }
            catch
            {
                success = false;
                DbUtils.RollBackTransaction();
            }
            finally
            {
                DbUtils.EndTransaction();
            }
            if (success)
            {
                scriptHack.Text = "<script> RefreshParentPage() </script>";
            }
        }
    }
}
