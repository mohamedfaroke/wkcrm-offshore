﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="TradeSelection.aspx.cs" Inherits="WKCRM.UI.TradeSelection" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<script language="javascript" src="../RadWindow.js"></script>
	<body id="Body1" runat="server" class="pBack">
	<asp:Label ID="scriptHack" runat="server"></asp:Label>
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="tradeCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="taskCombo" />
                        <telerik:AjaxUpdatedControl ControlID="qtyTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="priceTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                        <telerik:AjaxUpdatedControl ControlID="unitDesc" />
                        <telerik:AjaxUpdatedControl ControlID="designerNote" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="taskCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="qtyTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="priceTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                          <telerik:AjaxUpdatedControl ControlID="unitDesc" />
                         <telerik:AjaxUpdatedControl ControlID="designerNote" /> 
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="qtyTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="priceTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
		<br />
		<table>
		<tr>
		<td><asp:Label ID="Label1" runat="server" Text="Trade"></asp:Label></td>
		<td>
            <telerik:radcombobox id="tradeCombo" runat="server" autopostback="true" height="350px" width="200px">
            </telerik:RadComboBox>
        </td>
		</tr>
		<tr>
		<td><asp:Label ID="Label2" runat="server" Text="Task"></asp:Label></td>
		<td>
            <telerik:RadComboBox ID="taskCombo" runat="server" AutoPostback="true" height="350px" width="200px">
            </telerik:RadComboBox>
        </td>
		</tr>
		<tr>
		<td></td>
		<td><asp:Label ID="designerNote" runat="server" Text="" ForeColor="Red"></asp:Label></td>
		</tr>
		<tr>
		<td><asp:Label ID="Label3" runat="server" Text="Quantity"></asp:Label></td>
		<td>
            <telerik:RadNumericTextBox ID="qtyTextbox" runat="server" AutoPostback="true">
            </telerik:RadNumericTextBox>
        </td>
		</tr>
		<tr>
		<td><asp:Label ID="Label4" runat="server" Text="Price"></asp:Label>&nbsp;<asp:Label ID="unitDesc" runat="server" ForeColor="Red"></asp:Label></td>
		<td>
            <telerik:RadNumericTextBox ID="priceTextbox" runat="server" Type="Currency" AutoPostback="true">
            </telerik:RadNumericTextBox>
        </td>
		</tr>
		 <tr>
                <td>
                    <asp:Label ID="Label42" runat="server" Text="Notes"></asp:Label></td>
                <td>
                    <telerik:RadTextBox ID="notesTextbox" runat="server" Columns="30" Rows="2" TextMode="MultiLine">
                    </telerik:RadTextBox></td>
                <td>
                </td>
            </tr>
		
		<tr>
		<td><asp:Label ID="Label5" runat="server" Text="Total" Font-Bold="True" Font-Size="Medium"></asp:Label></td>
		<td>
          <asp:Label ID="totalAmt" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
        </td>
		</tr><tr>
		<td><WKCRM:ThemeButton runat="server" id="saveButton" Button-CausesValidation="True" Button-CommandName="Custom" Button-Text="Save">
		</WKCRM:ThemeButton>
</td>
		<td><WKCRM:ThemeButton runat="server" id="cancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-HtmlAttributes-onClick="CloseWindow()" Button-Text="Cancel">
		</WKCRM:ThemeButton>
</td>
		</tr>
		</table>
		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>
