﻿
// This file implements the code-behind class for MiscSelection.aspx.
// App_Code\MiscSelection.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI
{
  
partial class MiscSelection
        : BaseApplicationPage
// Code-behind class for the MiscSelection page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    

        public MiscSelection()
        {
            this.Initialize();
            this.Load += new EventHandler(MiscSelection_Load);
        }

    void MiscSelection_Load(object sender, EventArgs e)
    {
        this.saveButton.Click += new EventHandler(saveButton_Click);


        RemoveCurrentRequestFromSessionNavigationHistory();

        categoryCombo.AppendDataBoundItems = true;
        categoryCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(categoryCombo_SelectedIndexChanged);

        SubCategoryCombo.AppendDataBoundItems = true;
        SubCategoryCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(SubCategoryCombo_SelectedIndexChanged);

        itemCombo.AppendDataBoundItems = true;//So we can add our own Please Select item
        itemCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(itemCombo_SelectedIndexChanged);
        
        
        totalAmt.PreRender += new EventHandler(totalAmt_PreRender);
        categoryCombo.MarkFirstMatch = true;
        itemCombo.MarkFirstMatch = true;
        SubCategoryCombo.MarkFirstMatch = true;
        categoryCombo.Focus();

        if (!this.IsPostBack)
        {
            ResetFields(true);

            string itmType = Request["itemType"];
            string itmId = Request["itemId"];
            if (!String.IsNullOrEmpty(itmType))
            {
                ContractItemTypeRecord itemRec = ContractItemTypeTable.GetRecord(itmType, false);
                if (itemRec != null)
                {
                    object[] categories = null;
                    if (itemRec.id0 == Globals.CONTRACTITEM_MISC)
                    {
                        categories = View_ContractOthersCategory_MiscView.GetRecords("");
                    }
                    else if (itemRec.id0 == Globals.CONTRACTITEM_ACCESSORIES)
                    {
                        categories = View_ContractOthersCategory_AccessoriesView.GetRecords("");
                    }
                    else
                    {
                        //MASSIVE ASSUMPTION
                        //IF IN HERE, WE WILL ONLY HAVE ONE CATEGORY SPECIFED FOR THIS TYPE
                        categories = new object[] {ContractOthersTable.GetRecord("status_id=1 AND contract_item_type=" + itmType)};
                        categoryCombo.Enabled = false;
                    }
                   
                    string subCatItem = "";
                    if (categories != null)
                    {
                        RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
                        myItem.Selected = true;
                        categoryCombo.Items.Add(myItem);
                        categoryCombo.DataSource = categories;
                        categoryCombo.DataTextField = "category";
                        categoryCombo.DataValueField = "category";
                        categoryCombo.DataBind();
                        if (categoryCombo.Items.Count == 2)//If only 1 category (excluding default item), then select it by default
                            categoryCombo.Items[1].Selected = true;
                        if (!String.IsNullOrEmpty(itmId))
                        {
                            ContractOtherItemRecord otherItemRec = ContractOtherItemTable.GetRecord(itmId, false);
                            ContractOthersRecord othersRec = ContractOthersTable.GetRecord(otherItemRec.item_id.ToString(), false);
                            RadComboBoxItem catItem = categoryCombo.FindItemByValue(othersRec.category);
                            if (catItem != null)
                            {
                                catItem.Selected = true;
                            }
                            subCatItem = othersRec.sub_category;
                        }
                    }

                    itemLabel.Text = itemRec.name;
                    SetupItemCombo();
                    if (!String.IsNullOrEmpty(subCatItem))
                    {
                        RadComboBoxItem scItm = SubCategoryCombo.FindItemByValue(subCatItem);
                        if (scItm != null)
                        {
                            scItm.Selected = true;
                            SetupItemComboForReal();
                        }
                    }
                }
                //We are preloading a record from the database
                if (!String.IsNullOrEmpty(itmId))
                {

                    ContractOtherItemRecord otherItemRec = ContractOtherItemTable.GetRecord(itmId, false);
                    
                    RadComboBoxItem boxItem = itemCombo.FindItemByValue(otherItemRec.item_id.ToString());
                   
                    if (boxItem != null)
                    {
                        boxItem.Selected = true;
                    }
                    SetupFields();
                    priceTextbox.Value = (double)otherItemRec.unit_price;
                    qtyTextbox.Value = (double)otherItemRec.units;
                    supplierBox.Text = otherItemRec.supplier;
                    notesTextbox.Text = otherItemRec.notes;
                    if (markupCombo.Enabled)
                    {
                        RadComboBoxItem itm = markupCombo.FindItemByValue(otherItemRec.markup.ToString());
                        if (itm != null)
                            itm.Selected = true;
                    }
                }
            }
        }
    }

    void SubCategoryCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        ResetFields(false);
        SetupItemComboForReal();
    }

    public void saveButton_Click(object sender, EventArgs e)
    {
        bool qtyValid = qtyTextbox.ReadOnly || (qtyTextbox.Value.HasValue && qtyTextbox.Value > 0);
        bool priceValid = priceTextbox.ReadOnly || (priceTextbox.Value.HasValue && priceTextbox.Value >= 0);
        bool valid = (qtyValid && priceValid);
        string contractID = Request["Contract"];
        string itemID = Request["ItemId"];
        string errorMsg = "";

        if (supplierBox.Enabled && String.IsNullOrEmpty(supplierBox.Text))
            errorMsg = "Please specify a supplier";
        if (!valid)
            errorMsg = "Invalid Price or Quantity";
        if (String.IsNullOrEmpty(contractID))
            errorMsg = "Invalid Page Paramaters";

        if (!String.IsNullOrEmpty(errorMsg))
        {
            // string jsText = "radalert('" + errorMsg + "',250,150,'Error')";
            scriptHack.Text = "<script> alert('" + errorMsg + "');</script>";
            return;
        }


        if (valid && !String.IsNullOrEmpty(contractID))
        {
            bool success = false;

            try
            {
                DbUtils.StartTransaction();
                ContractOtherItemRecord otherItemRec;
                ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
                ContractOthersRecord otherItem = ContractOthersTable.GetRecord(itemCombo.SelectedValue, false);
                if (String.IsNullOrEmpty(itemID))
                    otherItemRec = new ContractOtherItemRecord();
                else
                    otherItemRec = ContractOtherItemTable.GetRecord(itemID, true);

                otherItemRec.contract_id = contract.id0;
                otherItemRec.item_id = otherItem.id0;
                otherItemRec.unit_price = (decimal)priceTextbox.Value;
                otherItemRec.units = (decimal)qtyTextbox.Value;

                if (markupCombo.Enabled)
                    otherItemRec.markup = Convert.ToDecimal(markupCombo.SelectedValue);

                otherItemRec.notes = notesTextbox.Text;
                otherItemRec.supplier = supplierBox.Text;
                otherItemRec.Save();

                Globals.UpdateContract(contract, this.SystemUtils.GetUserID());

                DbUtils.CommitTransaction();
                success = true;
            }
            catch
            {
                success = false;
                DbUtils.RollBackTransaction();
            }
            finally
            {
                DbUtils.EndTransaction();
            }
            if (success)
            {
                scriptHack.Text = "<script> RefreshParentPage() </script>";
            }
        }
    }

    private void SetupItemCombo()
    {
        SubCategoryCombo.ClearSelection();
        SubCategoryCombo.Items.Clear();
        itemCombo.ClearSelection();
        itemCombo.Items.Clear();

        if (categoryCombo.SelectedValue == "-1")
        {
            itemCombo.Text = "";
            itemCombo.Enabled = false;
            SubCategoryCombo.Enabled = false;
            return;
        }
        string itmType = Request["itemType"];
        SubCategoryCombo.Enabled = true;
        SubCategoryCombo.Items.Clear();
        string scWC = String.Format("contract_item_type={0} AND category='{1}'", itmType, categoryCombo.SelectedValue);

        RadComboBoxItem myNewItem = new RadComboBoxItem("Please Select", "-1");
        myNewItem.Selected = true;
        SubCategoryCombo.Items.Add(myNewItem);
        OrderBy scOB = new OrderBy(false, true);
        scOB.Add(View_contractOthers_SubCategoriesView.sub_category, OrderByItem.OrderDir.Asc);
        SubCategoryCombo.DataSource = View_contractOthers_SubCategoriesView.GetRecords(scWC, scOB);
        SubCategoryCombo.DataTextField = "sub_category";
        SubCategoryCombo.DataValueField = "sub_category";
       
        SubCategoryCombo.DataBind();
        
        SetupItemComboForReal();
    }

    private void SetupItemComboForReal()
    {
        itemCombo.ClearSelection();
        itemCombo.Items.Clear();

        if (SubCategoryCombo.SelectedValue == "-1")
        {
            itemCombo.Text = "";
            itemCombo.Enabled = false;
            return;
        }

        itemCombo.Enabled = true;
        string itmType = Request["itemType"];
        itemCombo.Items.Clear();
        RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
        myItem.Selected = true;
        itemCombo.Items.Add(myItem);

        OrderBy order = new OrderBy(false, true);
        order.Add(ContractOthersTable.name, OrderByItem.OrderDir.Asc);
        string where = "contract_item_type=" + itmType + " AND status_id=1";
        if (categoryCombo.SelectedValue != "-1")
        {
            where += " AND category='" + categoryCombo.SelectedValue + "'";
        }
        if (SubCategoryCombo.SelectedValue != "-1")
        {
            where += String.Format(" AND sub_category='{0}'", SubCategoryCombo.SelectedValue);
        }
        itemCombo.DataSource = ContractOthersTable.GetRecords(where, order);
        itemCombo.DataTextField = "name";
        itemCombo.DataValueField = "id0";

        itemCombo.DataBind();

    }


    void categoryCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        ResetFields(true);
        SetupItemCombo();
    }

    private void ResetFields(bool sub)
    {
        if(sub)
            SubCategoryCombo.Enabled = false;
        priceTextbox.Enabled = false;
        qtyTextbox.Enabled = false;
        supplierBox.Enabled = false;
        priceTextbox.Value = null;
        qtyTextbox.Value = null;
        supplierBox.Text = "";
        designerNote.Text = "";
        markupCombo.Enabled = false;
        priceLabel.Text = "Price";
      //  itemCombo.Items.Clear();
      //  itemCombo.Enabled = false;
    }

    void totalAmt_PreRender(object sender, EventArgs e)
    {
        double markup = 0;
        if (markupCombo.Enabled)
            markup = Convert.ToDouble(markupCombo.SelectedValue);

        double? val = (qtyTextbox.Value * priceTextbox.Value) * (1 + markup);
        if (val.HasValue)
            totalAmt.Text = val.Value.ToString("$0.00");
        else
            totalAmt.Text = "";
    }

    void itemCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (itemCombo.SelectedValue == "-1")
        {
            ResetFields(false);
            return;
        }

        SetupFields();
    }

    private void SetupFields()
    {
        ContractOthersRecord otherRec = ContractOthersTable.GetRecord(itemCombo.SelectedValue, false);
        if (otherRec != null)
        {
            markupCombo.Enabled = false;
            priceLabel.Text = "Price";
            switch (otherRec.contract_payment_type)
            {
                case (Globals.PRICETYPE_FIXED):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = true;
                    priceTextbox.Value = (double)otherRec.price;

                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = true;
                    qtyTextbox.Value = 1;

                    supplierBox.Enabled = false;
                    break;
                case (Globals.PRICETYPE_QUOTE):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = false;
                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = true;
                    priceTextbox.Value = 0;
                    qtyTextbox.Value = 1;
                    supplierBox.Enabled = true;

                    markupCombo.Enabled = true;
                    priceLabel.Text = "Cost";
                    break;
                case (Globals.PRICETYPE_UNITS):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = true;
                    priceTextbox.Value = (double)otherRec.price;
                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = false;
                    qtyTextbox.Value = 1;
                    supplierBox.Enabled = false;
                    break;
                default:
                    priceTextbox.Enabled = false;
                    qtyTextbox.Enabled = false;
                    supplierBox.Enabled = false;
                    priceTextbox.Value = 0;
                    qtyTextbox.Value = 0;
                    break;
            }
            priceDesc.Text = Globals.GetUnitDescription(otherRec.contract_payment_type);
            designerNote.Text = otherRec.notes;
        }
    }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links
    

#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
#endregion

  
}
  
}
  