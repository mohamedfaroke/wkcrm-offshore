﻿<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.BenchtopSelection" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="BenchtopSelection.aspx.cs" Inherits="WKCRM.UI.BenchtopSelection" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
		<script language="javascript" src="../RadWindow.js"></script>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
	<asp:Label runat="server" ID="scriptHack"></asp:Label>
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		
		<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
		<AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="benchtopCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="priceTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="qtyTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                        <telerik:AjaxUpdatedControl ControlID="supplierBox" />
                        <telerik:AjaxUpdatedControl ControlID="edgeProfileCombo" />
                        <telerik:AjaxUpdatedControl ControlID="notesTextBox" />
                        <telerik:AjaxUpdatedControl ControlID="colourTextBox" />
                        <telerik:AjaxUpdatedControl ControlID="edgeThickness" />
                        <telerik:AjaxUpdatedControl ControlID="finishTextBox" />
                        <telerik:AjaxUpdatedControl ControlID="scriptHack" />
                        <telerik:AjaxUpdatedControl ControlID="priceLabel1" />
                        <telerik:AjaxUpdatedControl ControlID="markupCombo" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="markupCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="priceTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="qtyTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="contractBenchtopOptionItemTableControlUpdatePanel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                </UpdatedControls>
            </telerik:AjaxSetting>            
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManagerBench" runat="server">
		</telerik:RadWindowManager>
        <br />
      <table>
        <tr>
        <td ><asp:Label ID="Label1" runat="server" Text="Benchtop"></asp:Label></td>
        <td><telerik:RadComboBox ID="benchtopCombo" runat="server" AutoPostBack="True" Height="350px" Width="200px"></telerik:RadComboBox></td>
        </tr>
          <tr>
              <td style="height: 18px">
                  <asp:Label ID="Label4" runat="server" Text="Edge Profile"></asp:Label></td>
              <td style="height: 18px">
                  <telerik:RadComboBox ID="edgeProfileCombo" runat="server" Height="350px" Width="200px">
                  </telerik:RadComboBox>
              </td>
          </tr>
          <tr>
              <td style="height: 18px">
                  <asp:Label ID="Label2" runat="server" Text="Edge Thickness (mm)"></asp:Label></td>
              <td style="height: 18px">
                  <telerik:RadTextBox ID="edgeThickness" runat="server" EmptyMessage="Please Enter Edge Thickness Details" MaxLength="100" TextMode="SingleLine" Width="125px">
                  </telerik:RadTextBox></td>
             
          </tr>
          <tr>
              <td style="height: 14px">
                  <asp:Label ID="Label6" runat="server" Text="Colour"></asp:Label></td>
              <td style="height: 14px">
                  <telerik:RadTextBox ID="colourTextbox" runat="server" EmptyMessage="Please Enter Colour Details" TextMode="SingleLine" Width="125px">
                  </telerik:RadTextBox></td>
          </tr>
          <tr>
              <td>
                  <asp:Label ID="Label7" runat="server" Text="Finish"></asp:Label></td>
              <td>
                  <telerik:RadTextBox ID="finishTextbox" runat="server" EmptyMessage="Please Enter Finish Details" TextMode="SingleLine" Width="125px">
                  </telerik:RadTextBox></td>
          </tr>
            <tr>
                <td >
                    <asp:Label ID="priceLabel1" runat="server" Text="Price"></asp:Label></td>
                <td >
                    <telerik:RadNumericTextBox ID="priceTextbox" runat="server" AutoPostBack="True" MinValue="0" Type="Currency">
                    </telerik:RadNumericTextBox></td>
            </tr>
             <tr>
                <td>
                 <asp:Label ID="markupLabel" runat="server" Text="Markup"></asp:Label></td>
                <td>
                    <telerik:RadComboBox ID="markupCombo" runat="server" RadComboBoxImagePosition="Right" AutoPostBack="True">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="0%" Value="0.00" />
                            <telerik:RadComboBoxItem runat="server" Text="5%" Value="0.05" />
                            <telerik:RadComboBoxItem runat="server" Text="10%" Value="0.10" />
                            <telerik:RadComboBoxItem runat="server" Text="15%" Value="0.15" />
                            <telerik:RadComboBoxItem runat="server" Text="20%" Value="0.20" />
                            <telerik:RadComboBoxItem runat="server" Text="25%" Value="0.25" />
                            <telerik:RadComboBoxItem runat="server" Text="30%" Value="0.30" />
                        </Items>
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                        <ExpandAnimation Type="OutQuart" />
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label3" runat="server" Text="Quantity"></asp:Label></td>
                <td >
                    <telerik:RadNumericTextBox ID="qtyTextbox" runat="server" AutoPostBack="True" MinValue="0">
                    </telerik:RadNumericTextBox></td>
            </tr>
           <tr>
                <td style="height: 22px">
                    <asp:Label ID="Label5" runat="server" Text="Supplier"></asp:Label></td>
                <td style="height: 22px">
                    <telerik:RadTextBox ID="supplierBox" runat="server">
                    </telerik:RadTextBox></td>
            </tr>
          <tr>
              <td style="height: 22px">
                  <asp:Label ID="Label8" runat="server" Text="Notes"></asp:Label></td>
              <td style="height: 22px">
                  <telerik:RadTextBox ID="notesTextbox" runat="server" MaxLength="100" TextMode="MultiLine">
                  </telerik:RadTextBox></td>
          </tr>
            
            </table>
            <table>
            <tr>
                <td>
<asp:UpdateProgress runat="server" id="contractBenchtopOptionItemTableControlUpdateProgress" AssociatedUpdatePanelID="contractBenchtopOptionItemTableControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="contractBenchtopOptionItemTableControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:contractBenchtopOptionItemTableControl runat="server" id="contractBenchtopOptionItemTableControl">
														
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="contractBenchtopOptionItemTableTitle" Text="Benchtop Option Item">
														</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="contractBenchtopOptionItemTotalItems">
														</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractBenchtopOptionItemTableControl$contractBenchtopOptionItemSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractBenchtopOptionItemTableControl$contractBenchtopOptionItemSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("contractBenchtopOptionItemTableControl$contractBenchtopOptionItemFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("contractBenchtopOptionItemTableControl$contractBenchtopOptionItemFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="contractBenchtopOptionItemAddButton" Button-CausesValidation="False" Button-CommandName="AddRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Add&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="contractBenchtopOptionItemDeleteButton" Button-CausesValidation="False" Button-CommandArgument="DeleteOnUpdate" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="contractBenchtopOptionItemToggleAll" onclick="toggleAllCheckboxes(this);">

															</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="contract_benchtop_option_itemLabel" Text="Contract Benchtop Option Item">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="priceLabel" Text="Price">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="unitsLabel" Text="Units">
															</asp:Literal></th><th class="thc" scope="col"><asp:Label runat="server" id="Label" Text="Total">
															</asp:Label></th>
			
			
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="contractBenchtopOptionItemTableControlRepeater">
																<ITEMTEMPLATE>
																		<WKCRM:contractBenchtopOptionItemTableControlRow runat="server" id="contractBenchtopOptionItemTableControlRow">
																				
			<div id="AJAXUpdateRecordRow">
			<tr>
			
			
			<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="contractBenchtopOptionItemRecordRowSelection">

																				</asp:CheckBox></td>
			
			<td class="ttc" ><asp:DropDownList runat="server" id="contract_benchtop_option_item" AutoPostback="true" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
																				</asp:DropDownList>
																				<Selectors:FvLlsHyperLink runat="server" id="contract_benchtop_option_itemFvLlsHyperLink" ControlToUpdate="contract_benchtop_option_item" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="contractBenchtopOptions" Field="ContractBenchtopOptions_.id" DisplayField="ContractBenchtopOptions_.name">																				</Selectors:FvLlsHyperLink>&nbsp;
																				<asp:RequiredFieldValidator runat="server" id="contract_benchtop_option_itemRequiredFieldValidator" ControlToValidate="contract_benchtop_option_item" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Contract Benchtop Option Item&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">																				</asp:RequiredFieldValidator></td>
			
			<td class="ttc" style=";;;;;text-align:right"><asp:Label runat="server" id="price">
																				</asp:Label></td>
			
			<td class="ttc" style=";;;;;text-align:right"><table border="0" cellpadding="0" cellspacing="0">
																				<tr>
																				<td style="padding-right: 5px">
																				<asp:TextBox runat="server" id="units" Columns="14" MaxLength="14" AutoPostback="true" CssClass="field_input" EnableIncrementDecrementButtons="false">
																				</asp:TextBox></td>
																				<td style="padding-right: 5px">
																				
																				&nbsp;
																				<asp:RequiredFieldValidator runat="server" id="unitsRequiredFieldValidator" ControlToValidate="units" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Units&quot;) %>" Enabled="True" Text="*">																				</asp:RequiredFieldValidator>&nbsp;
																				<BaseClasses:TextBoxMaxLengthValidator runat="server" id="unitsTextBoxMaxLengthValidator" ControlToValidate="units" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Units&quot;) %>">																				</BaseClasses:TextBoxMaxLengthValidator></td>
																				</tr>
																				</table>
																				</td><td class="ttc" style=";;;text-align:right">&nbsp;<asp:label id="optionAmt" runat="server" text=""/></td>
			
			
			
			</tr>
			</div>
			
																		</WKCRM:contractBenchtopOptionItemTableControlRow>
																</ITEMTEMPLATE>
														</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>	
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

											</WKCRM:contractBenchtopOptionItemTableControl>

										</ContentTemplate>
									</asp:UpdatePanel>
								

</td>
            </tr>
             
            
            <tr>
                <td><table>
                <tr>
                <td><asp:Label ID="TotalLabel" runat="server" Font-Bold="True" Font-Size="Medium" Text="Total:"></asp:Label></td>
                <td><asp:Label ID="totalAmt" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label></td>
            </tr>
                <tr><td><WKCRM:ThemeButton runat="server" id="saveButton" Button-CausesValidation="False" Button-CommandName="Custom" Button-Text="Save">
								</WKCRM:ThemeButton>
</td><td><WKCRM:ThemeButton runat="server" id="Button1" Button-CausesValidation="False" Button-CommandName="Custom" Button-HtmlAttributes-onClick="CloseWindow()" Button-Text="Cancel">
								</WKCRM:ThemeButton>
</td></tr></table>&nbsp;</td>
            </tr>
        </table>
		
		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>
