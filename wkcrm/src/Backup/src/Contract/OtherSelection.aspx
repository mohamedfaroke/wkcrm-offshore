﻿<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="OtherSelection.aspx.cs" Inherits="WKCRM.UI.OtherSelection" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
		<script language="javascript" src="../RadWindow.js"></script>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<asp:Label ID="scriptHack" runat="server"></asp:Label>
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadComboBox1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="priceTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="qtyTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                        <telerik:AjaxUpdatedControl ControlID="priceDesc" />
                        <telerik:AjaxUpdatedControl ControlID="designerNote" />
                        <telerik:AjaxUpdatedControl ControlID="supplierBox" />
                        <telerik:AjaxUpdatedControl ControlID="priceLabel" />
                        <telerik:AjaxUpdatedControl ControlID="markupCombo" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="markupCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="priceTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="qtyTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        
        
        <telerik:radwindowmanager id="RadWindowManagerOthers" runat="server">
		</telerik:radwindowmanager>
        
		<br />
		<table>
		<tr>
		<td><asp:Label ID="itemLabel" runat="server"></asp:Label></td>
		<td>
		<telerik:RadComboBox ID="RadComboBox1" runat="server" AutoPostback="true" Height="350px" Width="200px">
        </telerik:RadComboBox>
		</td>
		<td></td>
		</tr>
		<tr>
                <td >
                </td>
                <td>
                    <asp:Label ID="designerNote" runat="server" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="priceLabel" runat="server" Text="Price"></asp:Label>&nbsp;<asp:Label ID="priceDesc" runat="server" ForeColor="Red"></asp:Label></td>
                <td>
                    <telerik:RadNumericTextBox ID="priceTextbox" runat="server" Culture="English (Australia)"
                        Type="Currency" AutoPostback="true">
                    </telerik:RadNumericTextBox></td>
                <td>
                
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="markupLabel" runat="server" Text="Markup"></asp:Label></td>
                <td>
                    <telerik:RadComboBox ID="markupCombo" runat="server" RadComboBoxImagePosition="Right" AutoPostBack="True">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="0%" Value="0.00" />
                            <telerik:RadComboBoxItem runat="server" Text="5%" Value="0.05" />
                            <telerik:RadComboBoxItem runat="server" Text="10%" Value="0.10" />
                            <telerik:RadComboBoxItem runat="server" Text="15%" Value="0.15" />
                            <telerik:RadComboBoxItem runat="server" Text="20%" Value="0.20" />
                            <telerik:RadComboBoxItem runat="server" Text="25%" Value="0.25" />
                            <telerik:RadComboBoxItem runat="server" Text="30%" Value="0.30" />
                        </Items>
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                        <ExpandAnimation Type="OutQuart" />
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Quantity"></asp:Label></td>
                <td>
                    <telerik:RadNumericTextBox ID="qtyTextbox" runat="server" MinValue="0" AutoPostback="true">
                    </telerik:RadNumericTextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Supplier"></asp:Label></td>
                <td>
                    <telerik:RadTextBox ID="supplierBox" runat="server">
                    </telerik:RadTextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NotesLabel" runat="server" Text="Notes"></asp:Label></td>
                <td>
                    <telerik:RadTextBox ID="notesTextbox" runat="server" Columns="30" Rows="2" TextMode="MultiLine">
                    </telerik:RadTextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Medium" Text="Total"></asp:Label></td>
                <td>
                    <asp:Label ID="totalAmt" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label></td>
                <td>
                </td>
            </tr><tr>
                <td><WKCRM:ThemeButton runat="server" id="saveButton" Button-CausesValidation="False" Button-CommandName="Custom" Button-Text="Save">
		</WKCRM:ThemeButton>
</td>
                <td><WKCRM:ThemeButton runat="server" id="Button" Button-CausesValidation="False" Button-CommandName="Custom" Button-HtmlAttributes-onClick="CloseWindow()" Button-Text="Cancel">
		</WKCRM:ThemeButton>
</td>
                <td>&nbsp;</td>
            </tr>
		
		</table>
		
		
		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>
