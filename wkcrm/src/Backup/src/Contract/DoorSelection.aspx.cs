﻿
// This file implements the code-behind class for DoorSelection.aspx.
// App_Code\DoorSelection.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;     

#endregion

  
namespace WKCRM.UI
{
  
partial class DoorSelection
        : BaseApplicationPage
// Code-behind class for the DoorSelection page.
// Place your customizations in Section 1. Do not modify Section 2.
{

#region "Section 1: Place your customizations here."    


        public DoorSelection()
        {
            this.Initialize();
            this.Load += new EventHandler(DoorSelection_Load);
        }

    void DoorSelection_Load(object sender, EventArgs e)
    {
        RemoveCurrentRequestFromSessionNavigationHistory();

        totalAmt.PreRender += new EventHandler(totalAmt_PreRender);
        doorCombo.AppendDataBoundItems = true;
        doorCombo.MarkFirstMatch = true;
        doorTypeCombo.MarkFirstMatch = true;
        doorTypeCombo.AppendDataBoundItems = true;


        doorTypeCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(doorTypeCombo_SelectedIndexChanged);
        doorCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(doorCombo_SelectedIndexChanged);
        veneerCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(veneerCombo_SelectedIndexChanged);
        styleCombo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(styleCombo_SelectedIndexChanged);

        veneerCombo.AutoPostBack = true;
        styleCombo.AutoPostBack = true;
        qtyTextbox.AutoPostBack = true;
        if (!this.IsPostBack)
        {
            //First things first, we need to populate the door type drop downs
            doorCombo.Enabled = false;
            styleTextbox.Enabled = false;
            faceTextbox.Enabled = false;
            edgeTextbox.Enabled = false;
            colourTextbox.Enabled = false;
            finishTextbox.Enabled = false;
            qtyTextbox.Enabled = false;
            pricedTextbox.Enabled = true;
            pricedTextbox.ReadOnly = true;
            veneerCombo.Enabled = false;
            // Supplier field not enabled for doors
            // left in here just incase they change their minds
            supplierBox.Enabled = false;
            supplierBox.Visible = false;
            Label55.Visible = false;
            //Load the default
            BuildVeneerComboItems(DoorTypes.DoorType.NONE);
            BuildDoorTypeCombo();

            string editDoor = Request["Door"];
            if (!String.IsNullOrEmpty(editDoor))
            {
                LoadDoorData(editDoor);
            }
             //<Items>
             //        <telerik:RadComboBoxItem runat="server" Text="Single - 1 Good Side" Value="Single" />
             //        <telerik:RadComboBoxItem runat="server" Text="Double - 2 Good Side" Value="Double" />
             //    </Items>
        }
        styleValid.Enabled = styleTextbox.Enabled;
        edgeValid.Enabled = edgeTextbox.Enabled;
        faceValid.Enabled = faceTextbox.Enabled;
        finishValid.Enabled = finishTextbox.Enabled;
        colourValid.Enabled = colourTextbox.Enabled;
        RequiredFieldValidator6.Enabled = qtyTextbox.Enabled;
        qtyValidator.Enabled = qtyTextbox.Enabled;
    }

  
    private void BuildVeneerComboItems(DoorTypes.DoorType type)
    {
        veneerCombo.ClearSelection();
        veneerCombo.Items.Clear();
        if(type == DoorTypes.DoorType.TimberVeneerNatural)
        {
            veneerCombo.Items.Add(new RadComboBoxItem("A Grade Face B Grade Back", "AFaceBBack"));
        }
        else
        {
            veneerCombo.Items.Add(new RadComboBoxItem("Single - 1 Good Side", "Single"));
            veneerCombo.Items.Add(new RadComboBoxItem("Double - 2 Good Side", "Double"));
        }
    }


    private void BuildDoorTypeCombo()
    {
        doorTypeCombo.Items.Clear();
        RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
        doorTypeCombo.Items.Add(myItem);

        OrderBy order = new OrderBy(false, true);
        order.Add(ContractDoorTypesTable.name, OrderByItem.OrderDir.Asc);
        string where = "status_id=1";


        doorTypeCombo.DataSource = ContractDoorTypesTable.GetRecords(where,order);
        doorTypeCombo.DataTextField = "name";
        doorTypeCombo.DataValueField = "id0";
        doorTypeCombo.DataBind();

        if (doorTypeCombo.Items.Count < 15)
        {
            int myHeight = doorTypeCombo.Items.Count * 25;
            doorTypeCombo.Height = Unit.Pixel(myHeight < 150 ? 150 : myHeight);
        }


    }

    private void BuildDoorsCombo()
    {
        doorCombo.Items.Clear();
        RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
        myItem.Selected = true;
        doorCombo.Items.Add(myItem);

         OrderBy order = new OrderBy(false, true);
         order.Add(ContractDoorsTable.name, OrderByItem.OrderDir.Asc);
         string where = "door_type_id=" + doorTypeCombo.SelectedValue + " AND status_id=1";
        

        doorCombo.DataSource = ContractDoorsTable.GetRecords(where,order);
        doorCombo.DataTextField = "name";
        doorCombo.DataValueField = "id0";
        doorCombo.DataBind();
        doorCombo.Enabled = true;
        BuildVeneerComboItems((DoorTypes.DoorType) Convert.ToInt32(doorTypeCombo.SelectedValue));
        if (doorCombo.Items.Count < 15)
        {
            int myHeight = doorCombo.Items.Count * 25;
            doorCombo.Height = Unit.Pixel(myHeight < 150 ? 150 : myHeight);
        }
    }

    private void RecalculatePrice()
    {
        ContractDoorsRecord door = ContractDoorsTable.GetRecord(doorCombo.SelectedValue, false);
        if (door != null)
        {
            pricedTextbox.Value = (double)door.unit_price;
        }
    }

    private void BuildOtherDoorFields()
    {
        ContractDoorsRecord door = ContractDoorsTable.GetRecord(doorCombo.SelectedValue, false);
        if (door != null)
        {
            string doorType = "door_type_id=" + doorTypeCombo.SelectedValue;
            ContractDoorStylesRecord[] styleRecs = ContractDoorStylesTable.GetRecords(doorType);
            Globals.BuildContractDoorForm(door.style_type, styleTextbox, styleCombo, door.style0, styleRecs);
            styleValid.Enabled = styleTextbox.Enabled;
            //Edge profile
            ContractDoorEdgeProfilesRecord[] edgeRecs = ContractDoorEdgeProfilesTable.GetRecords(doorType);
            Globals.BuildContractDoorForm(door.edge_type, edgeTextbox, edgeCombo, door.edge_profile, edgeRecs);
            edgeValid.Enabled = edgeTextbox.Enabled;
            //Face
            ContractDoorFacesRecord[] faceRecs = ContractDoorFacesTable.GetRecords(doorType);
            Globals.BuildContractDoorForm(door.face_type, faceTextbox, faceCombo, door.face, faceRecs);
            faceValid.Enabled = faceTextbox.Enabled;
            //Finish
            ContractDoorFinishesRecord[] finishRecs = ContractDoorFinishesTable.GetRecords(doorType);
            Globals.BuildContractDoorForm(door.finish_type, finishTextbox, finishCombo, door.finish, finishRecs);
            finishValid.Enabled = finishTextbox.Enabled;
            //Colour
            ContractDoorColoursRecord[] colourRecs = ContractDoorColoursTable.GetRecords(doorType);
            Globals.BuildContractDoorForm(door.colour_type, colourTextbox, colourCombo, door.colour, colourRecs);
            colourValid.Enabled = colourTextbox.Enabled;

            qtyTextbox.Enabled = true;
            //Qty validators
            RequiredFieldValidator6.Enabled = true;
            qtyValidator.Enabled = true;

            pricedTextbox.Enabled = true;
            pricedTextbox.Value = (double)door.unit_price;
            doorNote.Text = door.notes;
            if (door.show_extra_fields)
            {
                pricedTextbox.ReadOnly = false;
                veneerCombo.Enabled = true;
            }
            else
            {
                
                pricedTextbox.ReadOnly = true;
                veneerCombo.Enabled = false;
            }
        }
    }

    private void LoadDoorData(string doorItemId)
    {
        ContractDoorItemRecord doorItem = ContractDoorItemTable.GetRecord(doorItemId, false);
        if (doorItem != null)
        {
            ContractDoorsRecord doorTemplate = ContractDoorsTable.GetRecord(doorItem.item_id.ToString(), false);
            if (doorTemplate != null)
            {
                BuildVeneerComboItems((DoorTypes.DoorType) doorTemplate.door_type_id);
                //We always have a DoorType combo, so we now select the item
                RadComboBoxItem boxItem = doorTypeCombo.FindItemByValue(doorTemplate.door_type_id.ToString());
                if (boxItem != null)
                {
                    // doorTypeCombo.SelectedItem.Selected = false;
                    boxItem.Selected = true;
                    BuildDoorsCombo();
                    RadComboBoxItem boxItem2 = doorCombo.FindItemByValue(doorItem.item_id.ToString());
                    if (boxItem2 != null)
                    {
                        boxItem2.Selected = true;
                        BuildOtherDoorFields();
                        //Now we prepopulate the textboxes/columns
                        Globals.SetDoorAttribute(colourCombo, colourTextbox, doorItem.colour);
                        Globals.SetDoorAttribute(styleCombo, styleTextbox, doorItem.style0);
                        Globals.SetDoorAttribute(edgeCombo, edgeTextbox, doorItem.edge_profile);
                        Globals.SetDoorAttribute(faceCombo, faceTextbox, doorItem.face);
                        Globals.SetDoorAttribute(finishCombo, finishTextbox, doorItem.finish);
                        Globals.SetDoorAttribute(veneerCombo, null, doorItem.veneer);
                        qtyTextbox.Value = (double)doorItem.units;
                        // We should always load the door data from the record that was saved
                        // If they make any changes the price will be updated, tough cheese
                        pricedTextbox.Value = (double)doorItem.unit_price;
                        notesTextbox.Text = doorItem.notes;
                        supplierBox.Text = doorItem.supplier;
                    }
                }
            }
        }
    }

    void totalAmt_PreRender(object sender, EventArgs e)
    {
        if (qtyTextbox.Value != null && pricedTextbox.Value != null)
        {
            double qty = (double)qtyTextbox.Value;
            double price = (double)pricedTextbox.Value;
            double total = qty * price;
           
            totalAmt.Text = total.ToString("$0.00");
        }
    }


    private void resetDoorFields()
    {
        //When we reset everything, combo's also become hidden
        

        styleTextbox.Enabled = false;
        styleTextbox.Text = ""; 
        styleTextbox.Visible = true;
        styleCombo.Enabled = false;
        styleCombo.Visible = false;
        faceTextbox.Enabled = false;
        faceTextbox.Text = "";
        faceTextbox.Visible = true;
        faceCombo.Enabled = false;
        faceCombo.Visible = false;
        edgeTextbox.Enabled = false;
        edgeTextbox.Text = "";
        edgeTextbox.Visible = true;
        edgeCombo.Enabled = false;
        edgeCombo.Visible = false;
        colourTextbox.Enabled = false;
        colourTextbox.Text = "";
        colourTextbox.Visible = true;
        colourCombo.Enabled = false;
        colourCombo.Visible = false;
        finishCombo.Enabled = false;
        finishCombo.Visible = false;
        finishTextbox.Enabled = false;
        finishTextbox.Text = "";
        finishTextbox.Visible = true;

        qtyTextbox.Enabled = false;
        qtyTextbox.Text = "";
        totalAmt.Text = "";

        pricedTextbox.Enabled = true;
        pricedTextbox.ReadOnly = true;
        pricedTextbox.Value = 0;
        veneerCombo.Enabled = false;

        supplierBox.Enabled = false;
        supplierBox.Visible = false;
        supplierBox.Text = "";

        doorNote.Text = "";
        designerNotes.Text = "";
        return;
    }

    private void ApplySpecialPricingForVeneerSelection(DoorTypes.DoorType doorType)
    {
        RecalculatePrice();//Get base price
        if (styleCombo.Text == "Horizontal")//All horizontal doors apply this logic
        {
            if (pricedTextbox.Value.HasValue)
                if (doorType == DoorTypes.DoorType.Navurban2)
                {
                    //13% cheaper
                    pricedTextbox.Value = Convert.ToDouble(Math.Round((decimal)(pricedTextbox.Value *0.87)));
                }
                else
                {
                    pricedTextbox.Value = Convert.ToDouble(Math.Round((decimal)(pricedTextbox.Value*1.2)));
                }
        }

        if(veneerCombo.SelectedValue == "Double" && IsTimberVeneerDoor(doorType))
        {
            //If it is a timber veneer door, and the selected veneer is DOUBLE,
            //add 30% to the price
            pricedTextbox.Value = Convert.ToDouble(Math.Round((decimal)(pricedTextbox.Value*1.3)));
        }
    }

    private bool IsTimberVeneerDoor(DoorTypes.DoorType doorType)
    {
        return (doorType == DoorTypes.DoorType.TimberveenerFinished
                || doorType == DoorTypes.DoorType.TimberveneerCertified
                || doorType == DoorTypes.DoorType.TimberVeneerEveneer
                || doorType == DoorTypes.DoorType.TimberVeneerLaminex
                || doorType == DoorTypes.DoorType.TimberVeneerNatural
                || doorType == DoorTypes.DoorType.TimberVeneerNewAge);
    }

    void styleCombo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        ApplySpecialPricingForVeneerSelection((DoorTypes.DoorType) Convert.ToInt32(doorTypeCombo.SelectedValue));
    }


    void veneerCombo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var doorType = (DoorTypes.DoorType) Convert.ToInt32(doorTypeCombo.SelectedValue);
        if(veneerCombo.SelectedValue == "Double" && !IsTimberVeneerDoor(doorType))
        {
            pricedTextbox.Value = null;
            pricedTextbox.Text  = "";
            pricedTextbox.ReadOnly = false;
        }
        else //Need to go through special pricing rules
        {
            pricedTextbox.ReadOnly = true;
            ApplySpecialPricingForVeneerSelection(doorType);
        }
    }


    void doorCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (doorCombo.SelectedValue == "-1")
        {
            resetDoorFields();
            return;
        }
        // The magic happens here ... we've selected a door
        // We now need to retrieve that doors record and work out what kind of 
        // input fields we are going to show

        BuildOtherDoorFields();
    }

    void doorTypeCombo_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (doorTypeCombo.SelectedValue == "-1")
        {
            doorCombo.SelectedItem.Selected = false;   
            doorCombo.Enabled = false;
            resetDoorFields();
            return;
        }
        // We've picked a type, now we populate the doors
        BuildDoorsCombo();
        
        resetDoorFields();
    }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links


    public void saveButton_Click(object sender, EventArgs args)
    {
        string contractID = this.Page.Request["Contract"];
        string editDoor = Request["Door"];
        ContractDoorsRecord door = ContractDoorsTable.GetRecord(doorCombo.SelectedValue, false);
        if (!Page.IsValid || String.IsNullOrEmpty(contractID) || door == null)
        {
            return;
        }
        bool success = false;
        // Click handler for saveButton.
        // Customize by adding code before the call or replace the call to the Base function with your own code.
        //saveButton_Click_Base(sender, args);
        try
        {
            DbUtils.StartTransaction();
            ContractDoorItemRecord doorRec;
            if (String.IsNullOrEmpty(editDoor))
            {
                doorRec = new ContractDoorItemRecord();
                doorRec.Parse(contractID, ContractDoorItemTable.contract_id);
            }
            else
                doorRec = ContractDoorItemTable.GetRecord(editDoor, true);
            // Fill in the guts of the door details here          
            doorRec.item_id = door.id0;
            doorRec.unit_price = (decimal)pricedTextbox.Value;
            doorRec.units = (decimal)qtyTextbox.Value;

            doorRec.colour = Globals.GetDoorAttribute(colourCombo, colourTextbox);
            doorRec.style0 = Globals.GetDoorAttribute(styleCombo, styleTextbox);
            doorRec.edge_profile = Globals.GetDoorAttribute(edgeCombo, edgeTextbox);
            doorRec.face = Globals.GetDoorAttribute(faceCombo, faceTextbox);
            doorRec.finish = Globals.GetDoorAttribute(finishCombo, finishTextbox);
            if (door.show_extra_fields)
                doorRec.veneer = Globals.GetDoorAttribute(veneerCombo, null);
            doorRec.notes = notesTextbox.Text;

            doorRec.Save();

            ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
            Globals.UpdateContract(contract, this.SystemUtils.GetUserID());

            DbUtils.CommitTransaction();
            success = true;
        }
        catch
        {
            success = false;
            DbUtils.RollBackTransaction();
        }
        finally
        {
            DbUtils.EndTransaction();
        }
        if (success)
        {
            scriptHack.Text = "<script> RefreshParentPage() </script>";
        }
        // NOTE: If the Base function redirects to another page, any code here will not be executed.
    }
    public void closeButton_Click(object sender, EventArgs args)
    {

        // Click handler for closeButton.
        // Customize by adding code before the call or replace the call to the Base function with your own code.
        closeButton_Click_Base(sender, args);
        // NOTE: If the Base function redirects to another page, any code here will not be executed.
    }
#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.closeButton.Button.Click += new EventHandler(closeButton_Click);
            this.saveButton.Button.Click += new EventHandler(saveButton_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");
                // Show message on Click
                this.closeButton.Button.Attributes.Add("onClick", "CloseWindow()");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void closeButton_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public void saveButton_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
#endregion

  
}
  
}
  