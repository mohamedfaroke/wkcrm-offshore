﻿<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="DoorSelection.aspx.cs" Inherits="WKCRM.UI.DoorSelection" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
		<script language="javascript" src="../RadWindow.js"></script>
    <body id="Body1" runat="server" class="pBack">
<asp:Label runat="server" ID="scriptHack"></asp:Label>
	
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />

<br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
      <div>
          <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="doorTypeCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="doorCombo" />
                        <telerik:AjaxUpdatedControl ControlID="styleTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="styleCombo" />
                        <telerik:AjaxUpdatedControl ControlID="styleValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="edgeTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="edgeCombo" />
                        <telerik:AjaxUpdatedControl ControlID="edgeValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="faceTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="faceCombo" />
                        <telerik:AjaxUpdatedControl ControlID="faceValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="finishTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="finishCombo" />
                        <telerik:AjaxUpdatedControl ControlID="finishValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="colourTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="colourCombo" />
                        <telerik:AjaxUpdatedControl ControlID="colourValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="qtyTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="veneerCombo" />
                        <telerik:AjaxUpdatedControl ControlID="pricedTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="doorNote" LoadingPanelID="RadAjaxLoadingPanel2" />
                       
                          <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="doorCombo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="styleTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="styleCombo" />
                        <telerik:AjaxUpdatedControl ControlID="styleValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="edgeTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="edgeCombo" />
                        <telerik:AjaxUpdatedControl ControlID="edgeValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="faceTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="faceCombo" />
                        <telerik:AjaxUpdatedControl ControlID="faceValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="finishTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="finishCombo" />
                        <telerik:AjaxUpdatedControl ControlID="finishValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="colourTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="colourCombo" />
                        <telerik:AjaxUpdatedControl ControlID="colourValid" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="qtyTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="veneerCombo" />
                        <telerik:AjaxUpdatedControl ControlID="pricedTextbox" />
                        <telerik:AjaxUpdatedControl ControlID="doorNote" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="veneerCombo">
                    <UpdatedControls>
                      <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                        <telerik:AjaxUpdatedControl ControlID="veneerCombo" />
                       <telerik:AjaxUpdatedControl ControlID="pricedTextbox" />
                    </UpdatedControls>
                    </telerik:AjaxSetting>
                 <telerik:AjaxSetting AjaxControlID="styleCombo">
                    <UpdatedControls>
                      <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                      <telerik:AjaxUpdatedControl ControlID="styleCombo" />
                       <telerik:AjaxUpdatedControl ControlID="pricedTextbox" />
                    </UpdatedControls>
                 </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="qtyTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="pricedTextbox">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="totalAmt" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="75px"
            Width="75px">
            <asp:Image ID="Image1" runat="server" AlternateText="Loading..." ImageUrl="../Images/Loading.gif" />
             </telerik:RadAjaxLoadingPanel>
             <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Height="75px"
                 Visible="False" Width="75px">
                 <asp:Image ID="Image2" runat="server" AlternateText="Loading..." ImageUrl="~/RadControls/Ajax/Skins/Default/Loading.gif" />
             </telerik:RadAjaxLoadingPanel>
      </div>

 <table width="100%">

            
      <tr>
                <td >
                    <asp:Label ID="Label1" runat="server" Text="Door Type"></asp:Label></td>
                <td >
                    <telerik:radcombobox id="doorTypeCombo" runat="server" autopostback="True" markfirstmatch="True" Height="350px" Width="100%"></telerik:radcombobox>
                </td>
                <td style="width: 40px">
                    <asp:CompareValidator ID="doorTypevalidator" runat="server" ControlToValidate="doorTypeCombo"
                        ErrorMessage="Please select a door type" Operator="NotEqual" ValueToCompare="Please Select">*</asp:CompareValidator></td>

            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label2" runat="server" Text="Door Style"></asp:Label></td>
                <td >
                    <telerik:radcombobox id="doorCombo" runat="server" autopostback="True" markfirstmatch="True" Height="350px" Width="100%"></telerik:radcombobox>
                </td>
                <td style="width: 40px">
                    <asp:CompareValidator ID="doorValidator" runat="server" ControlToValidate="doorCombo"
                        ErrorMessage="Please select a door style" Operator="NotEqual" ValueToCompare="Please Select" EnableClientScript="false">*</asp:CompareValidator>
                 
                        </td>

            </tr>
            <tr>
         <td>
         </td>
         <td>
                    <asp:Label ID="doorNote" runat="server" ForeColor="Red" Width="100%"></asp:Label></td>
         <td style="width: 40px">
         </td>
     </tr>
            <tr>
                <td >
                    <asp:Label ID="Label3" runat="server" Text="Style"></asp:Label></td>
                <td >
                    <telerik:RadTextBox ID="styleTextbox" runat="server" Width="100%">
                    </telerik:RadTextBox>
                    <telerik:RadComboBox ID="styleCombo" runat="server" Enabled="False" Visible="False" Width="100%">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 40px">
                    <asp:RequiredFieldValidator ID="styleValid" runat="server" ControlToValidate="styleTextbox"
                        ErrorMessage="Please enter a style" EnableClientScript="false" >*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Edge Profile" ></asp:Label></td>
                <td >
                    <telerik:RadTextBox ID="edgeTextbox" runat="server" Width="100%">
                    </telerik:RadTextBox>
                    <telerik:RadComboBox ID="edgeCombo" runat="server" Enabled="False" Visible="False" Width="100%">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 40px">
                    <asp:RequiredFieldValidator ID="edgeValid" runat="server" ControlToValidate="edgeTextbox"
                        ErrorMessage="Please enter an edge profile" EnableClientScript="false">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="height: 21px" >
                    <asp:Label ID="Label5" runat="server" Text="Face" ></asp:Label></td>
                <td style="height: 21px" >
                    <telerik:RadTextBox ID="faceTextbox" runat="server" Width="100%">
                    </telerik:RadTextBox>
                    <telerik:RadComboBox ID="faceCombo" runat="server" Enabled="False" Visible="False" Width="100%">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 40px; height: 21px">
                    <asp:RequiredFieldValidator ID="faceValid" runat="server" ControlToValidate="faceTextbox"
                        ErrorMessage="Please enter a Face" EnableClientScript="false">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label6" runat="server" Text="Finish" ></asp:Label></td>
                <td >
                    <telerik:RadTextBox ID="finishTextbox" runat="server" Width="100%">
                    </telerik:RadTextBox>
                    <telerik:RadComboBox ID="finishCombo" runat="server" Enabled="False" Visible="False" Width="100%">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 40px">
                    <asp:RequiredFieldValidator ID="finishValid" runat="server" ControlToValidate="finishTextbox"
                        ErrorMessage="Please enter a Finish"  EnableClientScript="false">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="height: 21px" >
                    <asp:Label ID="Label7" runat="server" Text="Colour" ></asp:Label></td>
                <td style="height: 21px" >
                    <telerik:RadTextBox ID="colourTextbox" runat="server" Width="100%">
                    </telerik:RadTextBox>
                    <telerik:RadComboBox ID="colourCombo" runat="server" Enabled="False" Visible="False" Width="100%">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 40px; height: 21px">
                    <asp:RequiredFieldValidator ID="colourValid" runat="server" ControlToValidate="colourTextbox"
                        ErrorMessage="Please enter a colour" EnableClientScript="false">*</asp:RequiredFieldValidator></td>
            </tr>
               <tr>
         <td style="height: 14px">
             <asp:Label ID="Label11" runat="server" Text="Veneer" Visible="True"></asp:Label></td>
         <td style="height: 14px">
             <telerik:RadComboBox ID="veneerCombo" runat="server" Width="100%">
                
             </telerik:RadComboBox>
         </td>
         <td style="width: 40px; height: 14px">
         </td>
     </tr>
 <tr>
         <td>
             <asp:label id="Label55" runat="server" text="Supplier"></asp:label>
         </td>
         <td>
             <telerik:radtextbox id="supplierBox" runat="server" Width="100%">
                    </telerik:radtextbox>
         </td>
         <td>
         </td>
     </tr>
      <tr>
         <td style="height: 14px">
             <asp:Label ID="designerNotes" runat="server" Text="Notes"></asp:Label></td>
         <td style="height: 14px">
             <telerik:RadTextBox ID="notesTextbox" runat="server" Columns="30" MaxLength="100"
                 Rows="2" TextMode="MultiLine" Width="100%">
             </telerik:RadTextBox></td>
         <td style="width: 40px; height: 14px">
         </td>
     </tr>
     
            <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Quantity"></asp:Label></td>
            <td>
                <telerik:RadNumericTextBox ID="qtyTextbox" runat="server" MinValue="0">
                </telerik:RadNumericTextBox>
                </td>
                <td style="width: 40px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="qtyTextbox"
                        ErrorMessage="Please enter a quantity" EnableClientScript="false">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="qtyValidator" runat="server" ControlToValidate="qtyTextbox"
                        ErrorMessage="Please enter a valid Quantity" Operator="NotEqual" Type="Double"
                        ValueToCompare="0" EnableClientScript="false">*</asp:CompareValidator>
                    </td>
            </tr>

   <tr>
         <td>
             <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Unit Price"></asp:Label></td>
         <td>
                    <telerik:RadNumericTextBox ID="pricedTextbox" runat="server" AutoPostBack="True" MinValue="0" Type="Currency">
                    </telerik:RadNumericTextBox>
</td>
         <td style="width: 40px">
         </td>
     </tr>
   <tr>
         <td>
             <asp:Label ID="Label9" runat="server" Text="Total" Font-Bold="True" Font-Size="Medium"></asp:Label></td>
         <td>
             <asp:Label ID="totalAmt" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label></td>
         <td style="width: 40px">
         </td>
     </tr>
     <tr>
     <td>
     		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="false" ShowSummary="false" runat="server"></asp:ValidationSummary>
     </td>
     </tr>
            
     <tr>
         <td style="height: 22px" ><script language="JavaScript" type="text/javascript">clearRTL()</script>
<WKCRM:ThemeButton runat="server" id="saveButton" Button-CausesValidation="True" Button-CommandName="Custom" Button-Text="Save">
</WKCRM:ThemeButton></td>
         <td style="height: 22px" ><WKCRM:ThemeButton runat="server" id="closeButton" Button-CausesValidation="False" Button-CommandName="Custom" Button-HtmlAttributes-onClick="CloseWindow()" Button-Text="Cancel">
</WKCRM:ThemeButton></td>
    </tr>
        </table>

       

		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
	</form>
	</body>
</html>
