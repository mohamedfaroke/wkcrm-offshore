﻿<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.EditContract" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="EditContract.aspx.cs" Inherits="WKCRM.UI.EditContract" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title>EditContract</title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<script language="javascript" src="../RadWindow.js"> 
	</script>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
	<asp:Label ID="scriptHack" runat="server"></asp:Label>
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	<br />
	<asp:UpdateProgress runat="server" id="contractMainRecordControlUpdateProgress" AssociatedUpdatePanelID="contractMainRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="contractMainRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:contractMainRecordControl runat="server" id="contractMainRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="contractMainDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="contractMainDialogTitle" Text="Edit Contract">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls"><asp:Label runat="server" id="Label" Text="Account">
														</asp:Label></td>
	<td class="dfv"><asp:Label runat="server" id="accountLabel" Text="
          ">
														</asp:Label></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="second_check_measure_feeLabel" Text="Second Check Measure Fee">
														</asp:Literal></td>
	<td class="dfv"><asp:CheckBox runat="server" id="second_check_measure_fee" CheckedValue="Yes" UncheckedValue="No">
														</asp:CheckBox></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="dual_finishes_feeLabel" Text="Dual Finishes Fee">
														</asp:Literal></td>
	<td class="dfv"><asp:CheckBox runat="server" id="dual_finishes_fee" CheckedValue="Yes" UncheckedValue="No">
														</asp:CheckBox></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="dual_finishes_lessthan_feeLabel" Text="Dual Finishes Lessthan Fee">
														</asp:Literal></td>
	<td class="dfv"><asp:CheckBox runat="server" id="dual_finishes_lessthan_fee" CheckedValue="Yes" UncheckedValue="No">
														</asp:CheckBox></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="difficult_delivery_feeLabel" Text="Difficult Delivery Fee">
														</asp:Literal></td>
	<td class="dfv"><asp:CheckBox runat="server" id="difficult_delivery_fee" CheckedValue="Yes" UncheckedValue="No">
														</asp:CheckBox></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="highrise_feeLabel" Text="Highrise Fee">
														</asp:Literal></td>
	<td class="dfv"><asp:CheckBox runat="server" id="highrise_fee" CheckedValue="Yes" UncheckedValue="No">
														</asp:CheckBox></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label2" Text="Kickboards fitted after Timber Floor Fee">
														</asp:Label></td>
	<td class="dfv"><asp:CheckBox runat="server" id="KickboardAfterTimberFee">
														</asp:CheckBox></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label3" Text="Kickboards/integrated washer fitted after Timber Floor Fee">
														</asp:Label></td>
	<td class="dfv"><asp:CheckBox runat="server" id="KickboardsWasherAfterTimberFee">
														</asp:CheckBox></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="designer_feeLabel" Text="Designer Fee">
														</asp:Literal></td>
	<td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="designer_fee" Columns="20" MaxLength="20" CssClass="field_input" EnableIncrementDecrementButtons="False">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														
														&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="designer_feeTextBoxMaxLengthValidator" ControlToValidate="designer_fee" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Designer Fee&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="consultation_feeLabel" Text="Consultation Fee">
														</asp:Literal></td>
	<td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="consultation_fee" Columns="20" MaxLength="20" CssClass="field_input" EnableIncrementDecrementButtons="False">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														
														&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="consultation_feeTextBoxMaxLengthValidator" ControlToValidate="consultation_fee" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Consultation Fee&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="home_insurance_sentLabel" Text="Home Warranty Sent">
														</asp:Literal></td>
	<td class="dfv"><asp:CheckBox runat="server" id="home_insurance_sent" CheckedValue="Yes" UncheckedValue="No">
														</asp:CheckBox></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="home_insurance_policyLabel" Text="Home Warranty Policy">
														</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="home_insurance_policy" MaxLength="50" Columns="30" CssClass="field_input">
														</asp:TextBox>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="home_insurance_policyTextBoxMaxLengthValidator" ControlToValidate="home_insurance_policy" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Home Warranty Policy&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr><tr>
	<td class="fls"><asp:Literal runat="server" id="estimated_completion_dateLabel" Text="Estimated Completion Date">
														</asp:Literal></td>
	<td class="dfv"><table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="estimated_completion_date" Columns="20" MaxLength="20" onkeyup="DateFormat(this, this.value, event.keyCode, 'dd/mm/yyyy')" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("estimated_completion_date"),"DateTextBox","dd/mm/yyyy","","") %>
														</td>
														<td style="padding-right: 5px">
														<Selectors:FvDsHyperLink runat="server" id="estimated_completion_dateFvDsHyperLink" ControlToUpdate="estimated_completion_date" Text="&lt;%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Format="d">														</Selectors:FvDsHyperLink>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="estimated_completion_dateTextBoxMaxLengthValidator" ControlToValidate="estimated_completion_date" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Estimated Completion Date&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														</td>
	</tr>
	
	
	
	
	
	
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:contractMainRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									


<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-HtmlAttributes-onClick="CloseWindow()" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table>

<br/>
<br/></form>
	</body>
</html>
