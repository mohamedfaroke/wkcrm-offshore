﻿
// This file implements the code-behind class for OtherSelection.aspx.
// App_Code\OtherSelection.Controls.vb contains the Table, Row and Record control classes
// for the page.  Best practices calls for overriding methods in the Row or Record control classes.

#region "Using statements"    

using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI
{
  
partial class OtherSelection
        : BaseApplicationPage
// Code-behind class for the OtherSelection page.
// Place your customizations in Section 1. Do not modify Section 2.
{
        

#region "Section 1: Place your customizations here."    

        public OtherSelection()
        {
            this.Initialize();
            this.Load += new EventHandler(OtherSelection_Load);
        }

    void OtherSelection_Load(object sender, EventArgs e)
    {
        RemoveCurrentRequestFromSessionNavigationHistory();
        RadComboBox1.AppendDataBoundItems = true;//So we can add our own Please Select item
        RadComboBox1.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(RadComboBox1_SelectedIndexChanged);
        totalAmt.PreRender += new EventHandler(totalAmt_PreRender);
        RadComboBox1.MarkFirstMatch = true;
        RadComboBox1.Focus();

        if (!this.IsPostBack)
        {
            ResetFields();

            string itmType = Request["itemType"];
            string itmId = Request["itemId"];
            if (!String.IsNullOrEmpty(itmType))
            {
                ContractItemTypeRecord itemRec = ContractItemTypeTable.GetRecord(itmType, false);

                if(itemRec.id0 == Globals.CONTRACTITEM_SPLASHBACK)
                {
                    NotesLabel.Text = "Colour";
                }
                else
                {
                    NotesLabel.Text = "Notes";
                }


                if (itemRec != null)
                {
                    itemLabel.Text = itemRec.name;
                    RadComboBox1.Items.Clear();
                    RadComboBoxItem myItem = new RadComboBoxItem("Please Select", "-1");
                    RadComboBox1.Items.Add(myItem);

                    OrderBy order = new OrderBy(false, true);
                    order.Add(ContractOthersTable.name, OrderByItem.OrderDir.Asc);
                    string where = "contract_item_type=" + itmType + " AND status_id=1";

                    RadComboBox1.DataSource = ContractOthersTable.GetRecords(where,order);
                    RadComboBox1.DataTextField = "name";
                    RadComboBox1.DataValueField = "id0";
                    RadComboBox1.DataBind();
                    if (RadComboBox1.Items.Count < 15)
                    {
                        int myHeight = RadComboBox1.Items.Count * 25;
                        RadComboBox1.Height = Unit.Pixel( myHeight < 150 ? 150 : myHeight );
                    }
                }
                //We are preloading a record from the database
                if (!String.IsNullOrEmpty(itmId))
                {
                    
                    ContractOtherItemRecord otherItemRec = ContractOtherItemTable.GetRecord(itmId, false);
                    RadComboBoxItem boxItem = RadComboBox1.FindItemByValue(otherItemRec.item_id.ToString());
                    if (boxItem != null)
                    {
                        boxItem.Selected = true;
                    }
                    SetupFields();
                    priceTextbox.Value = (double)otherItemRec.unit_price;
                    qtyTextbox.Value = (double)otherItemRec.units;
                    supplierBox.Text = otherItemRec.supplier;
                    notesTextbox.Text = otherItemRec.notes;
                    if (markupCombo.Enabled)
                    {
                        RadComboBoxItem itm = markupCombo.FindItemByValue(otherItemRec.markup.ToString());
                        if (itm != null)
                            itm.Selected = true;
                    }
                }
            }
        }
    }

    private void ResetFields()
    {
        priceTextbox.Enabled = false;
        qtyTextbox.Enabled = false;
        supplierBox.Enabled = false;
        priceTextbox.Value = null;
        qtyTextbox.Value = null;
        supplierBox.Text = "";
        designerNote.Text = "";
        markupCombo.Enabled = false;
        priceLabel.Text = "Price";
    }

    void totalAmt_PreRender(object sender, EventArgs e)
    {
        double markup = 0;
        if (markupCombo.Enabled)
            markup = Convert.ToDouble(markupCombo.SelectedValue);

        double? val = (qtyTextbox.Value * priceTextbox.Value) * (1 + markup);
        if (val.HasValue)
            totalAmt.Text = val.Value.ToString("$0.00");
        else
            totalAmt.Text = "";
    }

    void RadComboBox1_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (RadComboBox1.SelectedValue == "-1")
        {
            ResetFields();
            return;
        }

        SetupFields();
        var itmType = Convert.ToInt32(Request["itemType"]);
        if (itmType == Globals.CONTRACTITEM_SPLASHBACK && supplierBox.Enabled && supplierBox.Text == "" && RadComboBox1.SelectedItem.Text == "Stainless Steel")
        {
            supplierBox.Text = "By Client";
        }
    }

    private void SetupFields()
    {
        ContractOthersRecord otherRec = ContractOthersTable.GetRecord(RadComboBox1.SelectedValue, false);
        if (otherRec != null)
        {
            markupCombo.Enabled = false;
            priceLabel.Text = "Price";
            switch (otherRec.contract_payment_type)
            {
                case (Globals.PRICETYPE_FIXED):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = true;
                    priceTextbox.Value = (double)otherRec.price;

                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = true;
                    qtyTextbox.Value = 1;

                    supplierBox.Enabled = false;
                    break;
                case(Globals.PRICETYPE_QUOTE):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = false;
                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = true;
                    priceTextbox.Value = 0;
                    qtyTextbox.Value = 1;
                    supplierBox.Enabled = true;

                    markupCombo.Enabled = true;
                    priceLabel.Text = "Cost";
                    break;
                case(Globals.PRICETYPE_UNITS):
                    priceTextbox.Enabled = true;
                    priceTextbox.ReadOnly = true;
                    priceTextbox.Value = (double)otherRec.price;
                    qtyTextbox.Enabled = true;
                    qtyTextbox.ReadOnly = false;
                    qtyTextbox.Value = 1;
                    supplierBox.Enabled = false;
                    break;
                default:
                    priceTextbox.Enabled = false;
                    qtyTextbox.Enabled = false;
                    supplierBox.Enabled = false;
                    priceTextbox.Value = 0;
                    qtyTextbox.Value = 0;
                    break;
            }
            priceDesc.Text = Globals.GetUnitDescription(otherRec.contract_payment_type);
            designerNote.Text = otherRec.notes;
        }
    }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links


    public void saveButton_Click(object sender, EventArgs args)
    {

        // Click handler for saveButton.
        // Customize by adding code before the call or replace the call to the Base function with your own code.
        // saveButton_Click_Base(sender, args);
        // NOTE: If the Base function redirects to another page, any code here will not be executed.
        bool qtyValid = qtyTextbox.ReadOnly || (qtyTextbox.Value.HasValue && qtyTextbox.Value > 0);
        bool priceValid = priceTextbox.ReadOnly || (priceTextbox.Value.HasValue && priceTextbox.Value >= 0);
        bool valid = (qtyValid && priceValid);
        string contractID = Request["Contract"];
        string itemID = Request["ItemId"];
        string errorMsg = "";

        if (supplierBox.Enabled && String.IsNullOrEmpty(supplierBox.Text))
            errorMsg = "Please specify a supplier";
        if (!valid)
            errorMsg = "Invalid Price or Quantity";
        if (String.IsNullOrEmpty(contractID))
            errorMsg = "Invalid Page Paramaters";

        if (!String.IsNullOrEmpty(errorMsg))
        {
            //string jsText = "radalert('" + errorMsg + "',250,150,'Error')";
            //scriptHack.Text = Globals.InjectJavascript(jsText);
            scriptHack.Text = "<script> alert('" + errorMsg + "');</script>";
            return;
        }


        if (valid && !String.IsNullOrEmpty(contractID))
        {
            bool success = false;

            try
            {
                DbUtils.StartTransaction();
                ContractOtherItemRecord otherItemRec;
                ContractMainRecord contract = ContractMainTable.GetRecord(contractID, true);
                ContractOthersRecord otherItem = ContractOthersTable.GetRecord(RadComboBox1.SelectedValue, false);
                if (String.IsNullOrEmpty(itemID))
                    otherItemRec = new ContractOtherItemRecord();
                else
                    otherItemRec = ContractOtherItemTable.GetRecord(itemID, true);

                otherItemRec.contract_id = contract.id0;
                otherItemRec.item_id = otherItem.id0;
                otherItemRec.unit_price = (decimal)priceTextbox.Value;
                otherItemRec.units = (decimal)qtyTextbox.Value;
                otherItemRec.notes = notesTextbox.Text;
                otherItemRec.supplier = supplierBox.Text;
                if (markupCombo.Enabled)
                    otherItemRec.markup = Convert.ToDecimal(markupCombo.SelectedValue);

                otherItemRec.Save();

                Globals.UpdateContract(contract, this.SystemUtils.GetUserID());

                DbUtils.CommitTransaction();
                success = true;
            }
            catch
            {
                success = false;
                DbUtils.RollBackTransaction();
            }
            finally
            {
                DbUtils.EndTransaction();
            }
            if (success)
            {
                scriptHack.Text = "<script> RefreshParentPage() </script>";
            }
        }


    }
public void Button_Click(object sender, EventArgs args)
        {
          
            // Click handler for Button.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Button_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.Button.Button.Click += new EventHandler(Button_Click);
            this.saveButton.Button.Click += new EventHandler(saveButton_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
            // Check if user has access to this page.  Redirects to either sign-in page
            // or 'no access' page if not. Does not do anything if role-based security
            // is not turned on, but you can override to add your own security.
            this.Authorize(this.GetAuthorizedRoles());

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        
                // Setup the header text for the validation summary control.
                this.ValidationSummary1.HeaderText = GetResourceValue("ValidationSummaryHeaderText", "WKCRM");
                // Show message on Click
                this.Button.Button.Attributes.Add("onClick", "CloseWindow()");

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void Button_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public void saveButton_Click_Base(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
#endregion

  
}
  
}
  