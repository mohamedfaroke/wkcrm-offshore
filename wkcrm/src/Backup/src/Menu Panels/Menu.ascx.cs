﻿
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Web.UI;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
        
namespace WKCRM.UI
{

  // Code-behind class for the Menu user control.
       
partial class Menu : BaseApplicationMenuControl , IMenu
{
		

#region "Section 1: Place your customizations here."    

        public Menu()
        {
            this.Initialize();
        }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
         }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links
    

public void Menu7MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu7MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu7MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu7MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu7MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu7MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }

public void Menu11MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu11MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.


    bool isHostess = Globals.IsHostess(SystemUtils.GetUserRole());
    if (!isHostess)
        Menu11MenuItemHilited_Click_Base(sender, args);
    // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu11MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu11MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            bool isHostess = Globals.IsHostess(SystemUtils.GetUserRole());
    if (!isHostess)
        Menu11MenuItem_Click_Base(sender, args);
    // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu1MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu1MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu1MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu1MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu1MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu1MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }


//public void Menu5MenuItemHilited_Click(object sender, EventArgs args)
//        {
//          
//            // Click handler for Menu5MenuItemHilited.
//            // Customize by adding code before the call or replace the call to the Base function with your own code.
//            Menu5MenuItemHilited_Click_Base(sender, args);
//            // NOTE: If the Base function redirects to another page, any code here will not be executed.
//        }
//public void Menu5MenuItem_Click(object sender, EventArgs args)
//        {
//          
//            // Click handler for Menu5MenuItem.
//            // Customize by adding code before the call or replace the call to the Base function with your own code.
//            Menu5MenuItem_Click_Base(sender, args);
//            // NOTE: If the Base function redirects to another page, any code here will not be executed.
//        }

public void Menu3MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu3MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu3MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu3MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu3MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu3MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.Menu11MenuItem.Button.Click += new EventHandler(Menu11MenuItem_Click);
            this.Menu11MenuItemHilited.Button.Click += new EventHandler(Menu11MenuItemHilited_Click);
            this.Menu1MenuItem.Button.Click += new EventHandler(Menu1MenuItem_Click);
            this.Menu1MenuItemHilited.Button.Click += new EventHandler(Menu1MenuItemHilited_Click);
            this.Menu3MenuItem.Button.Click += new EventHandler(Menu3MenuItem_Click);
            this.Menu3MenuItemHilited.Button.Click += new EventHandler(Menu3MenuItemHilited_Click);
            this.Menu7MenuItem.Button.Click += new EventHandler(Menu7MenuItem_Click);
            this.Menu7MenuItemHilited.Button.Click += new EventHandler(Menu7MenuItemHilited_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        
                  ((BaseApplicationPage)this.Page).Authorize((Control)this.Menu11MenuItem, "5035");
            
                  ((BaseApplicationPage)this.Page).Authorize((Control)this.Menu3MenuItem, "NOT_ANONYMOUS");
            
                  ((BaseApplicationPage)this.Page).Authorize((Control)this.Menu7MenuItem, "NOT_ANONYMOUS");
            

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void Menu11MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../contactSource/ShowContactSourceTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu11MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../contactSource/ShowContactSourceTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu1MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Portal/Home.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu1MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Portal/Home.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu3MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../account/ProjectPhase.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu3MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../account/ProjectPhase.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu7MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../account/ShowAccountTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu7MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../account/ShowAccountTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
#region Interface Properties
          
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu11MenuItem {
            get {
                return this._Menu11MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu11MenuItemHilited {
            get {
                return this._Menu11MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu1MenuItem {
            get {
                return this._Menu1MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu1MenuItemHilited {
            get {
                return this._Menu1MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu3MenuItem {
            get {
                return this._Menu3MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu3MenuItemHilited {
            get {
                return this._Menu3MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu7MenuItem {
            get {
                return this._Menu7MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu7MenuItemHilited {
            get {
                return this._Menu7MenuItemHilited;
            }
        }
                
#endregion
        
#endregion

  

}
  
}

  