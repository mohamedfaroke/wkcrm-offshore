﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="HostessMenu.ascx.cs" Inherits="WKCRM.UI.HostessMenu" %>
<%@ Register Tagprefix="WKCRM" TagName="Menu_Item_Highlighted" Src="../Shared/Menu_Item_Highlighted.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu_Item" Src="../Shared/Menu_Item.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>

<table cellspacing="0" cellpadding="0" border="0">
 <tr>
	<td class="menus">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="mel"><img src="../Images/space.gif" height="1" width="39" alt=""/></td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu1MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Portal/HostessPortal.aspx" Button-Text="Oppurtunities">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu1MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Portal/HostessPortal.aspx" Button-Text="Oppurtunities" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu2MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../account/AddAccountPage.aspx" Button-Text="Accounts">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu2MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../account/AddAccountPage.aspx" Button-Text="Accounts" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu3MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Security/SignOut.aspx" Button-Text="Sign Out">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu3MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Security/SignOut.aspx" Button-Text="Sign Out" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td class="mer"><img src="../Images/space.gif" height="1" width="39" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="mbbg"><img src="../Images/space.gif" height="1" width="1" alt=""/></td>
 </tr>
</table>
