﻿<%@ Register Tagprefix="WKCRM" TagName="Menu_Item" Src="../Shared/Menu_Item.ascx" %>

<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Menu.ascx.cs" Inherits="WKCRM.UI.Menu" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu_Item_Highlighted" Src="../Shared/Menu_Item_Highlighted.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>

<table cellspacing="0" cellpadding="0" border="0">
 <tr>
	<td class="menus">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="mel"><img src="../Images/space.gif" height="1" width="39" alt=""/></td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu1MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Portal/Home.aspx" Button-Text="Home">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu1MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../Portal/Home.aspx" Button-Text="Home" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu7MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../account/ShowAccountTablePage.aspx" Button-Text="Leads">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu7MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../account/ShowAccountTablePage.aspx" Button-Text="Leads" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu3MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../account/ProjectPhase.aspx" Button-Text="Sales">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu3MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../account/ProjectPhase.aspx" Button-Text="Sales" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	
	
	<td>
	<WKCRM:Menu_Item runat="server" id="_Menu11MenuItem" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../contactSource/ShowContactSourceTablePage.aspx" Button-Text="Admin">
</WKCRM:Menu_Item>
	<WKCRM:Menu_Item_Highlighted runat="server" id="_Menu11MenuItemHilited" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../contactSource/ShowContactSourceTablePage.aspx" Button-Text="Admin" Visible="False">
</WKCRM:Menu_Item_Highlighted>
	</td>
	
	<td class="mer"><img src="../Images/space.gif" height="1" width="39" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="mbbg"><img src="../Images/space.gif" height="1" width="1" alt=""/></td>
 </tr>
</table>
