﻿
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using BaseClasses;
using BaseClasses.Utils;
using BaseClasses.Web.UI;
using BaseClasses.Web.UI.WebControls;
        
using WKCRM.Business;
using WKCRM.Data;
        
namespace WKCRM.UI
{

  // Code-behind class for the Menu1 user control.
       
partial class Menu1 : BaseApplicationMenuControl , IMenu1
{
		

#region "Section 1: Place your customizations here."    

        public Menu1()
        {
            this.Initialize();
        }

        public void LoadData()
        {
            // LoadData reads database data and assigns it to UI controls.
            // Customize by adding code before or after the call to LoadData_Base()
            // or replace the call to LoadData_Base().
            LoadData_Base();
            bool isHostess = Globals.IsHostess(SystemUtils.GetUserRole());
            if (isHostess)
            {
                this.Visible = false;
            }
        }

#region "Ajax Functions"

        
    [System.Web.Services.WebMethod()] 
    public static Object[] GetRecordFieldValue(String tableName , 
                                                String recordID , 
                                                String columnName, 
                                                String title, 
                                                bool persist, 
                                                int popupWindowHeight, 
                                                int popupWindowWidth, 
                                                bool popupWindowScrollBar)
    {
        // GetRecordFieldValue gets the pop up window content from the column specified by
        // columnName in the record specified by the recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetRecordFieldValue_Base()
        // or replace the call to  GetRecordFieldValue_Base().

        return GetRecordFieldValue_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

    
        [System.Web.Services.WebMethod()]
      
    public static object[] GetImage(String tableName, 
                                    String recordID, 
                                    String columnName, 
                                    String title, 
                                    bool persist, 
                                    int popupWindowHeight, 
                                    int popupWindowWidth, 
                                    bool popupWindowScrollBar)
    {
        // GetImage gets the Image url for the image in the column "columnName" and
        // in the record specified by recordID in data base table specified by tableName.
        // Customize by adding code before or after the call to  GetImage_Base()
        // or replace the call to  GetImage_Base().
        return GetImage_Base(tableName, recordID, columnName, title, persist, popupWindowHeight, popupWindowWidth, popupWindowScrollBar);
    }

#endregion

    // Page Event Handlers - buttons, sort, links
    
        public void Menu1MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu1MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu1MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu1MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu1MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu1MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu2MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu2MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu2MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu2MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu2MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu2MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu3MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu3MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu3MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu3MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu3MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu3MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu4MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu4MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu4MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu4MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu4MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu4MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu5MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu5MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu5MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
        public void Menu5MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu5MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu5MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }

public void Menu7MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu7MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu7MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu7MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu7MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu7MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu6MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu6MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu6MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu6MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu6MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu6MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu8MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu8MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu8MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu8MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu8MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu8MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu9MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu9MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu9MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu9MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu9MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu9MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu10MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu10MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu10MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu10MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu10MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu10MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu12MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu12MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu12MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu12MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu12MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu12MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu11MenuItemHilited_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu11MenuItemHilited.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu11MenuItemHilited_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
public void Menu11MenuItem_Click(object sender, EventArgs args)
        {
          
            // Click handler for Menu11MenuItem.
            // Customize by adding code before the call or replace the call to the Base function with your own code.
            Menu11MenuItem_Click_Base(sender, args);
            // NOTE: If the Base function redirects to another page, any code here will not be executed.
        }
#endregion

#region "Section 2: Do not modify this section."
        

        private void Initialize()
        {
            // Called by the class constructor to initialize event handlers for Init and Load
            // You can customize by modifying the constructor in Section 1.
            this.Init += new EventHandler(Page_InitializeEventHandlers);
            this.Load += new EventHandler(Page_Load);

            
        }

        // Handles base.Init. Registers event handler for any button, sort or links.
        // You can add additional Init handlers in Section 1.
        protected virtual void Page_InitializeEventHandlers(object sender, System.EventArgs e)
        {
            // Register the Event handler for any Events.
        
            this.Menu10MenuItem.Button.Click += new EventHandler(Menu10MenuItem_Click);
            this.Menu10MenuItemHilited.Button.Click += new EventHandler(Menu10MenuItemHilited_Click);
            this.Menu11MenuItem.Button.Click += new EventHandler(Menu11MenuItem_Click);
            this.Menu11MenuItemHilited.Button.Click += new EventHandler(Menu11MenuItemHilited_Click);
            this.Menu12MenuItem.Button.Click += new EventHandler(Menu12MenuItem_Click);
            this.Menu12MenuItemHilited.Button.Click += new EventHandler(Menu12MenuItemHilited_Click);
            this.Menu1MenuItem.Button.Click += new EventHandler(Menu1MenuItem_Click);
            this.Menu1MenuItemHilited.Button.Click += new EventHandler(Menu1MenuItemHilited_Click);
            this.Menu2MenuItem.Button.Click += new EventHandler(Menu2MenuItem_Click);
            this.Menu2MenuItemHilited.Button.Click += new EventHandler(Menu2MenuItemHilited_Click);
            this.Menu3MenuItem.Button.Click += new EventHandler(Menu3MenuItem_Click);
            this.Menu3MenuItemHilited.Button.Click += new EventHandler(Menu3MenuItemHilited_Click);
            this.Menu4MenuItem.Button.Click += new EventHandler(Menu4MenuItem_Click);
            this.Menu4MenuItemHilited.Button.Click += new EventHandler(Menu4MenuItemHilited_Click);
            this.Menu5MenuItem.Button.Click += new EventHandler(Menu5MenuItem_Click);
            this.Menu5MenuItemHilited.Button.Click += new EventHandler(Menu5MenuItemHilited_Click);
            this.Menu6MenuItem.Button.Click += new EventHandler(Menu6MenuItem_Click);
            this.Menu6MenuItemHilited.Button.Click += new EventHandler(Menu6MenuItemHilited_Click);
            this.Menu7MenuItem.Button.Click += new EventHandler(Menu7MenuItem_Click);
            this.Menu7MenuItemHilited.Button.Click += new EventHandler(Menu7MenuItemHilited_Click);
            this.Menu8MenuItem.Button.Click += new EventHandler(Menu8MenuItem_Click);
            this.Menu8MenuItemHilited.Button.Click += new EventHandler(Menu8MenuItemHilited_Click);
            this.Menu9MenuItem.Button.Click += new EventHandler(Menu9MenuItem_Click);
            this.Menu9MenuItemHilited.Button.Click += new EventHandler(Menu9MenuItemHilited_Click);
        }

        // Handles base.Load.  Read database data and put into the UI controls.
        // You can add additional Load handlers in Section 1.
        protected virtual void Page_Load(object sender, EventArgs e)
        {
        

            // Load data only when displaying the page for the first time
            if ((!this.IsPostBack)) {   
        

        // Read the data for all controls on the page.
        // To change the behavior, override the DataBind method for the individual
        // record or table UI controls.
        this.LoadData();
    }
    }

    public static object[] GetRecordFieldValue_Base(String tableName , 
                                                    String recordID , 
                                                    String columnName, 
                                                    String title, 
                                                    bool persist, 
                                                    int popupWindowHeight, 
                                                    int popupWindowWidth, 
                                                    bool popupWindowScrollBar)
    {
        string content =  NetUtils.EncodeStringForHtmlDisplay(BaseClasses.Utils.MiscUtils.GetFieldData(tableName, recordID, columnName)) ;
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // ' returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3) and (4) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    public static object[] GetImage_Base(String tableName, 
                                          String recordID, 
                                          String columnName, 
                                          String title, 
                                          bool persist, 
                                          int popupWindowHeight, 
                                          int popupWindowWidth, 
                                          bool popupWindowScrollBar)
    {
        string  content= "<IMG src =" + "\"../Shared/ExportFieldValue.aspx?Table=" + tableName + "&Field=" + columnName + "&Record=" + recordID + "\"/>";
        // returnValue is an array of string values.
        // returnValue(0) represents title of the pop up window.
        // returnValue(1) represents content ie, image url.
        // retrunValue(2) represents whether pop up window should be made persistant
        // or it should closes as soon as mouse moved out.
        // returnValue(3), (4) represents pop up window height and width respectivly
        // returnValue(5) represents whether pop up window should contain scroll bar.
        // (0),(2),(3), (4) and (5) is initially set as pass through attribute.
        // They can be modified by going to Attribute tab of the properties window of the control in aspx page.
        object[] returnValue = new object[6];
        returnValue[0] = title;
        returnValue[1] = content;
        returnValue[2] = persist;
        returnValue[3] = popupWindowWidth;
        returnValue[4] = popupWindowHeight;
        returnValue[5] = popupWindowScrollBar;
        return returnValue;
    }

    // Load data from database into UI controls.
    // Modify LoadData in Section 1 above to customize.  Or override DataBind() in
    // the individual table and record controls to customize.
    public void LoadData_Base()
    {
    
        }

        // Write out event methods for the page events
        
        // event handler for Button with Layout
        public void Menu10MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ImportPriceList.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu10MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ImportPriceList.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu11MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ExportCRMOptions.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu11MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ExportCRMOptions.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu12MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ImportCRMOptions.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu12MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ImportCRMOptions.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu1MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../contactSource/ShowContactSourceTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu1MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../contactSource/ShowContactSourceTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu2MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../employee/ShowEmployeeTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu2MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../employee/ShowEmployeeTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu3MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../employeeRoles/ShowEmployeeRolesTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu3MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../employeeRoles/ShowEmployeeRolesTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu4MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../opportunitySource/ShowOpportunitySourceTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu4MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../opportunitySource/ShowOpportunitySourceTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu5MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../opportunityCategory/ShowOpportunityCategoryTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu5MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../opportunityCategory/ShowOpportunityCategoryTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu6MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../tradesmen/ShowTradesmenTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu6MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../tradesmen/ShowTradesmenTablePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu7MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Reports/Overview.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu7MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Reports/Overview.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu8MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Reports/OutstandingTasks.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu8MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Reports/OutstandingTasks.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu9MenuItem_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ExportPriceList.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public void Menu9MenuItemHilited_Click_Base(object sender, EventArgs args)
        {
            
            string url = @"../Utilities/ExportPriceList.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = ((BaseApplicationPage)this.Page).ModifyRedirectUrl(url, "");
                ((BaseApplicationPage)this.Page).CommitTransaction(sender);
            } catch (Exception ex) {
                ((BaseApplicationPage)this.Page).RollBackTransaction(sender);
                shouldRedirect = false;
                ((BaseApplicationPage)this.Page).ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                ((BaseApplicationPage)this.Page).Response.Redirect(url);
            }
        }
          
#region Interface Properties
          
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu10MenuItem {
            get {
                return this._Menu10MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu10MenuItemHilited {
            get {
                return this._Menu10MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu11MenuItem {
            get {
                return this._Menu11MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu11MenuItemHilited {
            get {
                return this._Menu11MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu12MenuItem {
            get {
                return this._Menu12MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu12MenuItemHilited {
            get {
                return this._Menu12MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu1MenuItem {
            get {
                return this._Menu1MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu1MenuItemHilited {
            get {
                return this._Menu1MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu2MenuItem {
            get {
                return this._Menu2MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu2MenuItemHilited {
            get {
                return this._Menu2MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu3MenuItem {
            get {
                return this._Menu3MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu3MenuItemHilited {
            get {
                return this._Menu3MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu4MenuItem {
            get {
                return this._Menu4MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu4MenuItemHilited {
            get {
                return this._Menu4MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu5MenuItem {
            get {
                return this._Menu5MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu5MenuItemHilited {
            get {
                return this._Menu5MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu6MenuItem {
            get {
                return this._Menu6MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu6MenuItemHilited {
            get {
                return this._Menu6MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu7MenuItem {
            get {
                return this._Menu7MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu7MenuItemHilited {
            get {
                return this._Menu7MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu8MenuItem {
            get {
                return this._Menu8MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu8MenuItemHilited {
            get {
                return this._Menu8MenuItemHilited;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item Menu9MenuItem {
            get {
                return this._Menu9MenuItem;
            }
        }
                
        [Bindable(true),
        Category("Behavior"),
        DefaultValue(""),
        NotifyParentProperty(true),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IMenu_Item_Highlighted Menu9MenuItemHilited {
            get {
                return this._Menu9MenuItemHilited;
            }
        }
                
#endregion
        
#endregion

  

}
  
}

  