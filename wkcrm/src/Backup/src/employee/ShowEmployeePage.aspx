﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ShowEmployeePage.aspx.cs" Inherits="WKCRM.UI.ShowEmployeePage" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.ShowEmployeePage" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="Menu1" Src="../Menu Panels/Menu1.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="5035" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu11MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td>&nbsp;<WKCRM:Menu1 runat="server" id="Menu1" HiliteSettings="Menu2MenuItem">
		</WKCRM:Menu1></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<asp:UpdateProgress runat="server" id="employeeRecordControlUpdateProgress" AssociatedUpdatePanelID="employeeRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="employeeRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:employeeRecordControl runat="server" id="employeeRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_view.gif" runat="server" id="employeeDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="employeeDialogTitle" Text="Show Employee">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="nameLabel" Text="Name">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="name">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="location_idLabel" Text="Location">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="location_id">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="dobLabel" Text="Date Of Birth">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="dob">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="phoneLabel" Text="Phone">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="phone">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="mobileLabel" Text="Mobile">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="mobile">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="addressLabel" Text="Address">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="address">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="emailLabel" Text="Email">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="email">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="usernameLabel" Text="Username">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="UserName1">
														</asp:Literal>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="passwordLabel" Text="Password">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:Literal runat="server" id="password">
														</asp:Literal>
	</td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
 <%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:employeeRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									
<br/>
<br/>
<asp:UpdateProgress runat="server" id="employeeRolesTableControlUpdateProgress" AssociatedUpdatePanelID="employeeRolesTableControlUpdatePanel">
																	<ProgressTemplate>
																		<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																		</div>
																		<div style=" position:absolute; padding:30px;">
																			<img src="../Images/updating.gif">
																		</div>
																	</ProgressTemplate>
																</asp:UpdateProgress>
																<asp:UpdatePanel runat="server" id="employeeRolesTableControlUpdatePanel" UpdateMode="Conditional">

																	<ContentTemplate>
																		<WKCRM:employeeRolesTableControl runat="server" id="employeeRolesTableControl">
																					
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dht" valign="middle"><asp:Literal runat="server" id="employeeRolesTableTitle" Text="Roles">
																					</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="employeeRolesTotalItems">
																					</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("employeeRolesTableControl$employeeRolesSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("employeeRolesTableControl$employeeRolesSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("employeeRolesTableControl$employeeRolesFilterButton")) %>	
	
	<tr>
		<td class="fila"></td>
		<td></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("employeeRolesTableControl$employeeRolesFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			<td class="prbbc">
			
			</td>
			
			
			
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			
			
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="role_idLabel1" Text="Role">
																						</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="employeeRolesTableControlRepeater">
																							<ITEMTEMPLATE>
																									<WKCRM:employeeRolesTableControlRow runat="server" id="employeeRolesTableControlRow">
																											
			<div id="AJAXUpdateRecordRow">
			<tr>
				
				
				
				
			<td class="ttc" ><asp:Literal runat="server" id="role_id">
																											</asp:Literal></td>
			
			</tr>
			</div>
			
																									</WKCRM:employeeRolesTableControlRow>
																							</ITEMTEMPLATE>
																					</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																		</WKCRM:employeeRolesTableControl>

																	</ContentTemplate>
																</asp:UpdatePanel>
															
<br/>
<br/>



<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td><WKCRM:ThemeButton runat="server" id="OKButton" Button-CausesValidation="False" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:OK&quot;, &quot;WKCRM&quot;) %>">
																</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	</tr>
	</table>
	</td>
	</tr>
</table>

<br/>
<br/>

							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
															</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



