﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="SetLanguagePage.aspx.cs" Inherits="WKCRM.UI.SetLanguagePage" %>
<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title></title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pageBackground">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server"></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		<table cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="pageAlignment">
		<table cellspacing="0" cellpadding="0" border="0" class="borderTable">
			<tr>
			<td class="pageBorderTL"><img src="../Images/space.gif" height="10" width="14" alt=""/></td>
			<td class="pageBorderT"><img src="../Images/space.gif" height="10" width="14" alt=""/></td>
			<td class="pageBorderTR"><img src="../Images/space.gif" height="10" width="14" alt=""/></td>
			</tr>
			<tr>
			<td class="pageBorderL"><img src="../Images/space.gif" height="1" width="1" alt=""/></td>
			<td class="pageBorderC">
		<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
			<td>
				<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
			</td>
			</tr>
			</table>
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
			<td>
				<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
			</td>
			</tr>
			</table>
			<table cellspacing="0" cellpadding="0" border="0">
			<tr>
			<td>
			</td>
			<td>
				<WKCRM:Menu runat="server" id="Menu">
		</WKCRM:Menu>
			</td>
			</tr>
			<tr>
			<td>
				
			</td>
			<td>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td class="page_yellow">
					<a name="StartOfPageContent"></a>
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
					    <tr>
					        <td class="page_yellow"><a name="StartOfPageContent"></a>
					            <table cellspacing="0" cellpadding="0" border="0">
					                <tr>
					                    <td style="text-align:center">
					                        <asp:listbox id="LanguageBox" runat="server" selectionmode="single" style="height:200px;color:#666666;font-family:Verdana, Geneva, ms sans serif;font-size:10px;"></asp:listbox>
					                        <br/><br/>
					                    </td>
					                </tr>
					                <tr>
					                    <td style="text-align:center" width="100%">
					                        <WKCRM:ThemeButton runat="server" id="Button" Button-CausesValidation="False" Button-CommandName="SetLanguage" Button-Text="<%# GetResourceValue(&quot;DS:Text&quot;, &quot;WKCRM&quot;) %>" Button-ToolTip="Sets the culture of your web pages to language selected">
</WKCRM:ThemeButton>
					                    </td>
					                </tr>
					             </table>
					        </td>
					    </tr>
					</table>
					
					<br/>
					<br/>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			<tr>
			<td>
			</td>
				<td class="divider">
				<img src="../Images/space.gif" height="1" width="1" alt=""/>
			</td>
			</tr>
			<tr>
			<td>
			</td>
			<td>
			<WKCRM:Footer runat="server" id="PageFooter">
		</WKCRM:Footer>
			</td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
			</td>
			<td class="pageBorderR"><img src="../Images/space.gif" height="1" width="1" alt=""/></td>
		</tr>
		<tr>
			<td class="pageBorderBL"><img src="../Images/space.gif" height="16" width="14" alt=""/></td>
			<td class="pageBorderB"><img src="../Images/space.gif" height="16" width="14" alt=""/></td>
			<td class="pageBorderBR"><img src="../Images/space.gif" height="16" width="14" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>
