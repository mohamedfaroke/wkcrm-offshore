using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Impowa.Dev.Security;
using Impowa.Dev.Utilities;


public partial class License : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        this.test.PreRender += new EventHandler(test_PreRender);
    }

    void test_PreRender(object sender, EventArgs e)
    {
        SecLib license = null;
        test2.Font.Size = 12;
        test2.Text = "";
        if (Session["SecLib"] != null)
            license = Session["SecLib"] as SecLib;
        try
        {
            string client = Impowa.Dev.Utilities.Web.GetFromWebConfig("ClientName");
            string appname = Impowa.Dev.Utilities.Web.GetFromWebConfig("ApplicationName");
            if (license == null)
            {

                license = new SecLib(appname, client, Server.MapPath(@"~"));
                Session["SecLib"] = license;
            }

            if (!String.IsNullOrEmpty(license.ErrorMessage))// If we have an error message, display that
                test2.Text += "<p>" + license.ErrorMessage + "<p>";
            else // Otherwise, we'll settle for just the reminder message (if any)
                test2.Text += "<p>" + license.ReminderMessage + "<p>";

            test2.Text += "<style> TD {	text-align: left;vertical-align: top;font-size: 12px;} </style>" + license.LicenseInfoHTML();
        }
        catch (Exception ez)// T
        {
            test2.Text += "<p>ERROR WITH LICENSE FILE<p>";
            test2.Text += ez.Message + " " + ez.Source + " " + ez.StackTrace + "<p>";
            test2.Text += "Please contact us on 1300 797 838 or visit our <a href='http://www.impowa.com.au'>website</a>";
        }
    }
}
