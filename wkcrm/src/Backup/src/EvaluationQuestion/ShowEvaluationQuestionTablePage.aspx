﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ShowEvaluationQuestionTablePage.aspx.cs" Inherits="WKCRM.UI.ShowEvaluationQuestionTablePage" %>
<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.ShowEvaluationQuestionTablePage" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Pagination" Src="../Shared/Pagination.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu6MenuItem">
		</WKCRM:Menu>
					</td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<asp:UpdateProgress runat="server" id="EvaluationQuestionTableControlUpdateProgress" AssociatedUpdatePanelID="EvaluationQuestionTableControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="EvaluationQuestionTableControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:EvaluationQuestionTableControl runat="server" id="EvaluationQuestionTableControl">
														
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	<td class="dheci" valign="middle"><a onclick="toggleRegions(this);"><img id="ToggleRegionIcon" src="../Images/ToggleHideFilters.gif" border="0" alt="Hide Filters"/></a></td>
	<td class="dht" valign="middle"><asp:Literal runat="server" id="EvaluationQuestionTableTitle" Text="Evaluation Question">
														</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="EvaluationQuestionTotalItems">
														</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"><%# GetResourceValue("Txt:SearchFor", "WKCRM") %></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("EvaluationQuestionTableControl$EvaluationQuestionSearchButton")) %>
		<asp:TextBox runat="server" id="EvaluationQuestionSearchArea" CaseSensitive="False" CssClass="Search_Input" Fields="Question">
															</asp:TextBox>
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("EvaluationQuestionTableControl$EvaluationQuestionSearchButton")) %>
		</td>
		<td class="filbc"><WKCRM:ThemeButton runat="server" id="EvaluationQuestionSearchButton" Button-CausesValidation="False" Button-CommandName="Search" Button-Text="&lt;%# GetResourceValue(&quot;Btn:SearchGoButtonText&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("EvaluationQuestionTableControl$EvaluationQuestionFilterButton")) %>	
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="EvaluationIDLabel" Text="Evaluation">
															</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="EvaluationIDFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
															</asp:DropDownList></td>
	</tr>
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="TypeIDLabel" Text="Type">
															</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="TypeIDFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
															</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("EvaluationQuestionTableControl$EvaluationQuestionFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionNewButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../EvaluationQuestion/AddEvaluationQuestionPage.aspx" Button-Text="&lt;%# GetResourceValue(&quot;Btn:New&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionEditButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../EvaluationQuestion/EditEvaluationQuestionPage.aspx?EvaluationQuestion={PK}" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Edit&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionDeleteButton" Button-CausesValidation="False" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionExportButton" Button-CausesValidation="False" Button-CommandName="ExportData" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Export&quot;, &quot;WKCRM&quot;) %>">
															</WKCRM:ThemeButton>
			</td>
			
			
			<td class="pra">
			<WKCRM:Pagination runat="server" id="EvaluationQuestionPagination">
														</WKCRM:Pagination>
			</td>
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" colspan="2"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="EvaluationQuestionToggleAll" onclick="toggleAllCheckboxes(this);">

															</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="EvaluationIDLabel1" Text="Evaluation" CausesValidation="False">
															</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="QuestionLabel" Text="Question" CausesValidation="False">
															</asp:LinkButton></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="TypeIDLabel1" Text="Type">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="OrderNumberLabel" Text="Order Number">
															</asp:Literal></th>
			
			<th class="thc" scope="col"><asp:Literal runat="server" id="associated_taskLabel" Text="Associated Task">
															</asp:Literal></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="EvaluationQuestionTableControlRepeater">
																<ITEMTEMPLATE>
																		<WKCRM:EvaluationQuestionTableControlRow runat="server" id="EvaluationQuestionTableControlRow">
																				
			<div id="AJAXUpdateRecordRow">
			<tr>
				<td class="tic" scope="row"><asp:ImageButton runat="server" id="EvaluationQuestionRecordRowEditButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_edit.gif" RedirectURL="../EvaluationQuestion/EditEvaluationQuestionPage.aspx?EvaluationQuestion={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:EditRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																				</asp:ImageButton></td>
				<td class="tic"><asp:ImageButton runat="server" id="EvaluationQuestionRecordRowViewButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_view.gif" RedirectURL="../EvaluationQuestion/ShowEvaluationQuestionPage.aspx?EvaluationQuestion={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:ViewRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																				</asp:ImageButton></td>
				<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="EvaluationQuestionRecordRowSelection">

																				</asp:CheckBox></td>
				
			<td class="ttc" ><asp:Literal runat="server" id="EvaluationID">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="Question">
																				</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="TypeID">
																				</asp:Literal></td>
			
			<td class="ttc" style="text-align: right;"><asp:Literal runat="server" id="OrderNumber">
																				</asp:Literal></td>
			
			<td class="ttc" ><asp:Literal runat="server" id="associated_task">
																				</asp:Literal></td>
			
			</tr>
			</div>
			
																		</WKCRM:EvaluationQuestionTableControlRow>
																</ITEMTEMPLATE>
														</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

											</WKCRM:EvaluationQuestionTableControl>

										</ContentTemplate>
										<Triggers>
											<asp:PostBackTrigger ControlID="EvaluationQuestionExportButton"/>
										</Triggers>
									</asp:UpdatePanel>
								
<br/>
<br/>


							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
								</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



