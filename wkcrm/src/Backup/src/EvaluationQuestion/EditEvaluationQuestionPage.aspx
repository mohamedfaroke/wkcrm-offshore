﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Pagination" Src="../Shared/Pagination.ascx" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="EditEvaluationQuestionPage.aspx.cs" Inherits="WKCRM.UI.EditEvaluationQuestionPage" %>
<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.EditEvaluationQuestionPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu">
		</WKCRM:Menu>
					</td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<asp:UpdateProgress runat="server" id="EvaluationQuestionRecordControlUpdateProgress" AssociatedUpdatePanelID="EvaluationQuestionRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="EvaluationQuestionRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:EvaluationQuestionRecordControl runat="server" id="EvaluationQuestionRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	<td class="dheci" valign="middle"><a onclick="toggleExpandCollapse(this);"><img id="ExpandCollapseIcon" src="../Images/DialogHeaderIconCollapse.gif" border="0" alt="Collapse panel"/></a></td>
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="EvaluationQuestionDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="EvaluationQuestionDialogTitle" Text="Edit Evaluation Question">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="EvaluationIDLabel" Text="Evaluation">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:DropDownList runat="server" id="EvaluationID" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="EvaluationIDFvLlsHyperLink" ControlToUpdate="EvaluationID" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="Evaluations" Field="Evaluations_.EvaluationID" DisplayField="Evaluations_.EvaluationName">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="EvaluationIDRequiredFieldValidator" ControlToValidate="EvaluationID" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Evaluation&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="QuestionLabel" Text="Question">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:TextBox runat="server" id="Question" MaxLength="50" Columns="60" CssClass="field_input" Rows="4" TextMode="MultiLine">
														</asp:TextBox>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="QuestionRequiredFieldValidator" ControlToValidate="Question" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Question&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="QuestionTextBoxMaxLengthValidator" ControlToValidate="Question" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Question&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="TypeIDLabel" Text="Type">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:DropDownList runat="server" id="TypeID" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="TypeIDFvLlsHyperLink" ControlToUpdate="TypeID" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="QuestionType" Field="QuestionType_.typeId" DisplayField="QuestionType_.Type">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="TypeIDRequiredFieldValidator" ControlToValidate="TypeID" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Type&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="OrderNumberLabel" Text="Order Number">
														</asp:Literal>
	</td>
	<td class="dfv">
		<table border="0" cellpadding="0" cellspacing="0">
														<tr>
														<td style="padding-right: 5px">
														<asp:TextBox runat="server" id="OrderNumber" Columns="14" MaxLength="14" onkeyup="adjustInteger(this, event.keyCode)" CssClass="field_input">
														</asp:TextBox></td>
														<td style="padding-right: 5px">
														<%# SystemUtils.GenerateIncrementDecrementButtons(true, Container.FindControl("OrderNumber"),"NumberTextBox","","","") %>
														&nbsp;
														<asp:RequiredFieldValidator runat="server" id="OrderNumberRequiredFieldValidator" ControlToValidate="OrderNumber" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Order Number&quot;) %>" Enabled="True" Text="*">														</asp:RequiredFieldValidator>&nbsp;
														<BaseClasses:TextBoxMaxLengthValidator runat="server" id="OrderNumberTextBoxMaxLengthValidator" ControlToValidate="OrderNumber" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Order Number&quot;) %>">														</BaseClasses:TextBoxMaxLengthValidator></td>
														</tr>
														</table>
														
	</td>
	</tr>
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="associated_taskLabel" Text="Associated Task">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:DropDownList runat="server" id="associated_task" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="associated_taskFvLlsHyperLink" ControlToUpdate="associated_task" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="tradeTaskTypes" Field="TradeTaskTypes_.id" DisplayField="TradeTaskTypes_.name">														</Selectors:FvLlsHyperLink>
	</td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:EvaluationQuestionRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									
<br/>
<br/>
<asp:UpdateProgress runat="server" id="EvaluationQuestionOptionsTableControlUpdateProgress" AssociatedUpdatePanelID="EvaluationQuestionOptionsTableControlUpdatePanel">
																	<ProgressTemplate>
																		<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
																		</div>
																		<div style=" position:absolute; padding:30px;">
																			<img src="../Images/updating.gif">
																		</div>
																	</ProgressTemplate>
																</asp:UpdateProgress>
																<asp:UpdatePanel runat="server" id="EvaluationQuestionOptionsTableControlUpdatePanel" UpdateMode="Conditional">

																	<ContentTemplate>
																		<WKCRM:EvaluationQuestionOptionsTableControl runat="server" id="EvaluationQuestionOptionsTableControl">
																					
<!-- Begin Table Panel.html -->

<table class="dv" cellpadding="0" cellspacing="0" border="0">
 <tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	<td class="dheci" valign="middle"><a onclick="toggleRegions(this);"><img id="ToggleRegionIcon" src="../Images/ToggleHideFilters.gif" border="0" alt="Hide Filters"/></a></td>
	<td class="dht" valign="middle"><asp:Literal runat="server" id="EvaluationQuestionOptionsTableTitle" Text="Evaluation Question Options">
																					</asp:Literal></td>
	<td class="dhtrc">
		<table id="CollapsibleRegionTotalRecords" style="display:none;" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" id="EvaluationQuestionOptionsTotalItems">
																					</asp:Label></td>
		</tr>
	</table>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
 </tr>
 <tr>
	<td class="dBody">
	<table id="CollapsibleRegion" style="display:block;" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td>
	<table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">
	
	<!-- Search & Filter Area -->
	<tr>
		<td class="fila"></td>
		<td>
		<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("EvaluationQuestionOptionsTableControl$EvaluationQuestionOptionsSearchButton")) %>
		
		<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("EvaluationQuestionOptionsTableControl$EvaluationQuestionOptionsSearchButton")) %>
		</td>
		<td class="filbc"></td>
	</tr>
	
	
	<tr>
	<td></td>
	<td></td>
	<td rowspan="100" class="filbc"></td> 
	</tr>
	<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("EvaluationQuestionOptionsTableControl$EvaluationQuestionOptionsFilterButton")) %>	
	
	<tr>
		<td class="fila"><asp:Literal runat="server" id="OptionIdLabel" Text="Option">
																						</asp:Literal></td>
		<td><asp:DropDownList runat="server" id="OptionIdFilter" CssClass="Filter_Input" onkeypress="dropDownListTypeAhead(this,false)" AutoPostBack="True">
																						</asp:DropDownList></td>
	</tr>
	
	<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("EvaluationQuestionOptionsTableControl$EvaluationQuestionOptionsFilterButton")) %>
	
	</table>
	<div class="spacer"></div>
	<!-- Category Area -->
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<!-- Pagination Area -->
		<td class="pr" colspan="100">
		<table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td class="prel"><img src="../Images/space.gif" alt=""/></td>
			
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionOptionsAddButton" Button-CausesValidation="False" Button-CommandName="AddRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Add&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionOptionsEditButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="../EvaluationQuestionOptions/EditEvaluationQuestionOptionsPage.aspx?EvaluationQuestionOptions={PK}" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Edit&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionOptionsDeleteButton" Button-CausesValidation="False" Button-CommandArgument="DeleteOnUpdate" Button-CommandName="DeleteRecord" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Delete&quot;, &quot;WKCRM&quot;) %>" ConfirmMessage="&lt;%# GetResourceValue(&quot;DeleteConfirm&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionOptionsSaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			<td class="prbbc">
			<WKCRM:ThemeButton runat="server" id="EvaluationQuestionOptionsExportButton" Button-CausesValidation="False" Button-CommandName="ExportData" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Export&quot;, &quot;WKCRM&quot;) %>">
																						</WKCRM:ThemeButton>
			</td>
			
			
			<td class="pra">
			<WKCRM:Pagination runat="server" id="EvaluationQuestionOptionsPagination">
																					</WKCRM:Pagination>
			</td>
		<td class="prer"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		<!--Table View Area -->
		<tr>
		<td class="tre">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
			<!-- This is the table's header row -->
			
			<div id="AJAXUpdateHeaderRow">
			<tr class="tch">
			<th class="thc" colspan="2"><img src="../Images/space.gif" height="1" width="1" alt=""/></th>
			<th class="thc" style="padding:0px;vertical-align:middle;"><asp:CheckBox runat="server" id="EvaluationQuestionOptionsToggleAll" onclick="toggleAllCheckboxes(this);">

																						</asp:CheckBox></th>
			
			<th class="thc" scope="col"><asp:LinkButton runat="server" id="OptionIdLabel1" Text="Option" CausesValidation="False">
																						</asp:LinkButton></th>
			
			</tr>
			</div>
			
			<!-- Table Rows -->
			<asp:Repeater runat="server" id="EvaluationQuestionOptionsTableControlRepeater">
																							<ITEMTEMPLATE>
																									<WKCRM:EvaluationQuestionOptionsTableControlRow runat="server" id="EvaluationQuestionOptionsTableControlRow">
																											
			<div id="AJAXUpdateRecordRow">
			<tr>
			<td class="tic" scope="row"><asp:ImageButton runat="server" id="EvaluationQuestionOptionsRecordRowEditButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_edit.gif" RedirectURL="../EvaluationQuestionOptions/EditEvaluationQuestionOptionsPage.aspx?EvaluationQuestionOptions={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:EditRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																											</asp:ImageButton></td>
			<td class="tic"><asp:ImageButton runat="server" id="EvaluationQuestionOptionsRecordRowViewButton" CausesValidation="False" CommandName="Redirect" CssClass="button_link" ImageURL="../Images/icon_view.gif" RedirectURL="../EvaluationQuestionOptions/ShowEvaluationQuestionOptionsPage.aspx?EvaluationQuestionOptions={PK}" ToolTip="&lt;%# GetResourceValue(&quot;Txt:ViewRecord&quot;, &quot;WKCRM&quot;) %>" AlternateText="">

																											</asp:ImageButton></td>
			<td class="tic" onclick="moveToThisTableRow(this);"><asp:CheckBox runat="server" id="EvaluationQuestionOptionsRecordRowSelection">

																											</asp:CheckBox></td>
			
			<td class="ttc" style="text-align: right;"><asp:DropDownList runat="server" id="OptionId" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
																											</asp:DropDownList>
																											<Selectors:FvLlsHyperLink runat="server" id="OptionIdFvLlsHyperLink" ControlToUpdate="OptionId" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="QuestionOption" Field="QuestionOption_.OptionID" DisplayField="QuestionOption_.OptionText">																											</Selectors:FvLlsHyperLink>&nbsp;
																											<asp:RequiredFieldValidator runat="server" id="OptionIdRequiredFieldValidator" ControlToValidate="OptionId" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Option&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">																											</asp:RequiredFieldValidator></td>
			
			</tr>
			</div>
			
																									</WKCRM:EvaluationQuestionOptionsTableControlRow>
																							</ITEMTEMPLATE>
																					</asp:Repeater>
			<!-- Totals Area -->
			
			
			</table>
		</td>
		</tr>	
		</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
 </tr>
</table>
<!-- End Table Panel.html -->

																		</WKCRM:EvaluationQuestionOptionsTableControl>

																	</ContentTemplate>
																	<Triggers>
																		<asp:PostBackTrigger ControlID="EvaluationQuestionOptionsExportButton"/>
																	</Triggers>
																</asp:UpdatePanel>
															
<br/>
<br/>



<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-RedirectArgument="updatedata" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
																</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
																</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table>

<br/>
<br/>

							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
															</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



