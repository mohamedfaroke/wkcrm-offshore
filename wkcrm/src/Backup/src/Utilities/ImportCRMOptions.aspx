﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImportCRMOptions.aspx.cs" Inherits="Utilities_ImportCRMOptions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register TagPrefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>
<%@ Register TagPrefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>
<%@ Register Tagprefix="WKCRM" TagName="Menu1" Src="../Menu Panels/Menu1.ascx" %>
<%@ Register TagPrefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Import CRM Options</title>
    <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css" />
</head>
<script language="javascript" src="../RadWindow.js"></script>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True"
        EnablePageMethods="True" />
    <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
        <tr>
            <td class="pAlign">
                <table cellspacing="0" cellpadding="0" border="0" class="pbTable">
                    <tr>
                        <td class="pbTL">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                        <td class="pbT">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                        <td class="pbTR">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pbL">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                        <td class="pbC">
                            <table cellspacing="0" cellpadding="0" border="0" class="pcTable">
                                <tr>
                                    <td class="pcTL">
                                    </td>
                                    <td class="pcT">
                                       
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td>
                                                    <WKCRM:Header runat="server" ID="PageHeader"></WKCRM:Header>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="pcTR">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pcL">
                                    </td>
                                    <td class="pcC">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                            <tr>
                                                <td>
                                                  <WKCRM:Menu runat="server" ID="Menu" HiliteSettings="Menu11MenuItem"></WKCRM:Menu>
                                                    <WKCRM:Menu1 runat="server" id="Menu1" HiliteSettings="Menu12MenuItem">
		</WKCRM:Menu1>
                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="pContent">
                                                                <a name="StartOfPageContent"></a>
                                                              
                                                                <h3> This is a utility which will import the CRM Option values<p></p>
                                                                The steps below should be followed:
                                                                <ol>
                                                                    <li>Export the List and save to your desktop</li>
                                                                    <li>Open in Excel</li>
                                                                    <li>Change the values for the options</li>
                                                                    <li>Save the Excel Spread Sheet in CSV format</li>
                                                                    <li>Import the Excel Spread Sheet</li>
                                                                </ol>
                                                                </h3>
                                                                <br />
                                                                <div>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="Label2" runat="server" Text="CSV File:"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadUpload ID="CsvFileUpload" runat="server" MaxFileInputsCount="1">
                                                                                </telerik:RadUpload>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:Button ID="UploadButton" runat="server" Text="Upload CSV File"></asp:Button>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:Label ID="resultMessage" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="pcR">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pcBL">
                                    </td>
                                    <td class="pcB">
                                        <WKCRM:Footer runat="server" ID="PageFooter"></WKCRM:Footer>
                                    </td>
                                    <td class="pcBR">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="pbR">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pbBL">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                        <td class="pbB">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                        <td class="pbBR">
                            <img src="../Images/space.gif" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

