﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WKCRM.UI;
using Telerik.Web.UI;

public partial class Utilities_ImportCRMOptions : BaseApplicationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CsvFileUpload.ControlObjectsVisibility = ControlObjectsVisibility.None;
        UploadButton.Click += new EventHandler(UploadButton_Click);
    }

    void UploadButton_Click(object sender, EventArgs e)
    {
        RadUpload fileUpload = CsvFileUpload;

        try
        {
            string results;
            resultMessage.Text = "";
            List<string> fileNames = CSVImport.RadUploadSaveFile(fileUpload);
            CSVImport crmOptionImporter = new CRMOptionImporter(fileNames[0]);
            bool success = crmOptionImporter.ProcessCSV();
            if (crmOptionImporter.PartiallySuccessful)
                CSVImport.SaveCSVToDB(fileNames[0], this.SystemUtils.GetUserID());
            if (!success)
            {
                resultMessage.Text += crmOptionImporter.ErrorMessage + "<br/>";
            }
            resultMessage.Text += crmOptionImporter.ResultMessage;

            //Replace the new lines with breaks
            resultMessage.Text = resultMessage.Text.Replace("\n", "<br/>");
        }
        catch (Exception ex)
        {

            resultMessage.Text = "Could not save file. Reasons: " + ex.Message;
        }
    }
}