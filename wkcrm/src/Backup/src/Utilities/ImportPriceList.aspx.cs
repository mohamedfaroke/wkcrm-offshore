﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.IO;
using WKCRM.UI;

public partial class Utilities_ImportPriceList : BaseApplicationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CsvFileUpload.ControlObjectsVisibility = ControlObjectsVisibility.None;
        UploadButton.Click += new EventHandler(UploadButton_Click);
    }

    void UploadButton_Click(object sender, EventArgs e)
    {
        RadUpload fileUpload = CsvFileUpload;

        try
        {
            string results;
            resultMessage.Text = "";
            List<string> fileNames = CSVImport.RadUploadSaveFile(fileUpload);
            CSVImport priceImporter = new PriceListImporter(fileNames[0]);
            bool success = priceImporter.ProcessCSV();

            if (priceImporter.PartiallySuccessful)
                CSVImport.SaveCSVToDB(fileNames[0], this.SystemUtils.GetUserID());
            if (!success)
            {
                resultMessage.Text += priceImporter.ErrorMessage + "<br/>";
            }
            resultMessage.Text += priceImporter.ResultMessage;

            //Replace the new lines with breaks
            resultMessage.Text = resultMessage.Text.Replace("\n", "<br/>");
        }
        catch (Exception ex)
        {

            resultMessage.Text = "Could not save file. Reasons: " + ex.Message;
        }
    }
}