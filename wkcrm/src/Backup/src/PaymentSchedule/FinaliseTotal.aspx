﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FinaliseTotal.aspx.cs" Inherits="PaymentSchedule_FinaliseTotal" %>

<%@ Register TagPrefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Finalise Total</title>
</head>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
<script language="javascript" src="../RadWindow.js">
</script>
<script language="javascript">
    function discountCallBack(arg) {
        if (arg)
            window.location = window.location + '&goahead=1';
    }
	
</script>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager1" runat="server" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="priceTextbox">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="percentageDiff" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="SaveButton">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManagerDiscount" runat="server">
    </telerik:RadWindowManager>
    <table>
        <tr>
            <td style="text-align: right;">
                Original Total (Inc GST):
            </td>
            <td>
                <asp:Label ID="origTotal" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                New Total (Inc GST):
            </td>
            <td>
                <telerik:RadNumericTextBox ID="priceTextbox" runat="server" Culture="English (Australia)"
                    Type="Currency" AutoPostBack="true">
                </telerik:RadNumericTextBox>
            </td>
            <td>
                <asp:Label ID="percentageDiff" runat="server" Text="" />&nbsp;
            </td>
        </tr>
        <tr>
            <td>
              <asp:Button ID="SaveButton" runat="server" Text="Save & Continue" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
