﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Home.aspx.cs" Inherits="WKCRM.UI.Home" %>

<%@ Register TagPrefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register TagPrefix="WKCRM" Namespace="WKCRM.UI.Controls.Home" %>

<%@ Register TagPrefix="Selectors" Namespace="WKCRM" %>

<%@ Register TagPrefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register TagPrefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register TagPrefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Wonderful Kitchens CRM v1.0 </title>
    <link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css" />
</head>
<body id="Body1" runat="server" class="pBack">
    <form id="Form1" method="post" runat="server">
        <BaseClasses:ScrollCoordinates ID="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
        <BaseClasses:BasePageSettings ID="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS"></BaseClasses:BasePageSettings>
        <script language="JavaScript" type="text/javascript">clearRTL()</script>
        <asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />

        <!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
        <table width="100%">
            <tr>
                <td style="width: 15%"></td>
                <td style="width: 70%">
                    <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
                        <tr>
                            <td class="pAlign">
                                <table cellspacing="0" cellpadding="0" border="0" class="pbTable">
                                    <tr>
                                        <td class="pbTL">
                                            <img src="../Images/space.gif" alt="" /></td>
                                        <td class="pbT">
                                            <img src="../Images/space.gif" alt="" /></td>
                                        <td class="pbTR">
                                            <img src="../Images/space.gif" alt="" /></td>
                                    </tr>
                                    <tr>
                                        <td class="pbL">
                                            <img src="../Images/space.gif" alt="" /></td>
                                        <td class="pbC">
                                            <table cellspacing="0" cellpadding="0" border="0" class="pcTable">
                                                <tr>
                                                    <td class="pcTL"></td>
                                                    <td class="pcT">
                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                            <tr>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <WKCRM:Header runat="server" ID="PageHeader"></WKCRM:Header>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="pcTR"></td>
                                                </tr>
                                                <tr>
                                                    <td class="pcL"></td>
                                                    <td class="pcC">
                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">

                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td class="pContent" style="text-align: center; vertical-align: middle">

                                                                                <table border="10" cellpadding="20" cellspacing="10" width="85%" align="center"
                                                                                    style="border-left: 1px solid #80C5D1; border-top: 1px solid #80C5D1; border-right: 1px solid #80C5D1; border-bottom: 1px solid #80C5D1;">

                                                                                    <tr>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;"
                                                                                            width="33%">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton2" Button-HtmlAttributes-onClick="window.open('../opportunity/Opportunity.aspx','', 'width=400, height=250, resizable=yes, scrollbars=yes, modal=no')" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/Home/main_screen_opportunity.gif" ToolTip="Add Opportunities" AlternateText=""></asp:ImageButton><br>
                                                                                            Record Opportunities</td>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;"
                                                                                            width="33%">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/Home/main_screen_accounts.gif" RedirectURL="../account/ShowAccountTablePage.aspx" ToolTip="Manage Leads" AlternateText=""></asp:ImageButton><br>
                                                                                            Manage Leads</td>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;"
                                                                                            width="33%">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton1" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/Home/main_screen_project.gif" RedirectURL="../account/ProjectPhase.aspx" ToolTip="Manage Projects" AlternateText=""></asp:ImageButton><br>
                                                                                            Manage Projects</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton5" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/home/main_screen_survey.gif" RedirectURL="../Survey/SurveyTablePage.aspx" AlternateText=""></asp:ImageButton><br>
                                                                                            Surveys</td>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton6" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/home/main_screen_leadreview.gif" RedirectURL="../account/LeadReviews.aspx" AlternateText=""></asp:ImageButton><br>
                                                                                            Lead Reviews</td>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton4" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/Home/main_screen_overview.gif" RedirectURL="../Reports/Overview.aspx" ToolTip="Overview" AlternateText=""></asp:ImageButton><br>
                                                                                            Overview</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton8" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/Home/main_screen_reports.gif" RedirectURL="../Reports/ShowReportsTablePage.aspx" ToolTip="View Reports" AlternateText=""></asp:ImageButton><br>
                                                                                            Reports&nbsp;&nbsp;</td>
                                                                                        <td id="AdminCell" runat="server" style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton3" CausesValidation="False" CommandName="Redirect" Consumers="page" ImageUrl="../Images/Home/main_screen_administration.gif" RedirectURL="../contactSource/ShowContactSourceTablePage.aspx" ToolTip="Administration" AlternateText=""></asp:ImageButton><br>
                                                                                            Administration&nbsp;&nbsp;</td>
                                                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid #cfcbc4;">
                                                                                            <asp:ImageButton runat="server" ID="ImageButton7" CausesValidation="False" CommandName="LogOut" Consumers="page" ImageUrl="../Images/home/main_screen_signout.gif" AlternateText=""></asp:ImageButton><br>
                                                                                            Sign Out</td>
                                                                                    </tr>



                                                                                </table>
                                                                                <br>
                                                                                <a name="StartOfPageContent"></a>

                                                                                <div id="detailPopup" style="z-index: 2; visibility: visible; position: absolute;"></div>
                                                                                <div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index: 1; visibility: visible; position: absolute;"></div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="pContent" style="text-align: center; vertical-align: middle">&nbsp;<asp:UpdateProgress runat="server" ID="taskNotificationTableControlUpdateProgress" AssociatedUpdatePanelID="taskNotificationTableControlUpdatePanel">
                                                                                <ProgressTemplate>
                                                                                    <div style="position: absolute; width: 100%; height: 1000px; background-color: #000000; filter: alpha(opacity=10); opacity: 0.20; -moz-opacity: 0.20; padding: 20px;">
                                                                                    </div>
                                                                                    <div style="position: absolute; padding: 30px;">
                                                                                        <img src="../Images/updating.gif">
                                                                                    </div>
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress>
                                                                                <asp:UpdatePanel runat="server" ID="taskNotificationTableControlUpdatePanel" UpdateMode="Conditional">

                                                                                    <ContentTemplate>
                                                                                        <WKCRM:taskNotificationTableControl runat="server" ID="taskNotificationTableControl">

                                                                                            <!-- Begin Table Panel.html -->

                                                                                            <table class="dv" cellpadding="0" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td class="dh">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td class="dhel">
                                                                                                                    <img src="../Images/space.gif" alt="" /></td>

                                                                                                                <td class="dht" valign="middle">
                                                                                                                    <asp:Literal runat="server" ID="taskNotificationTableTitle" Text="Notification">
                                                                                                                    </asp:Literal></td>
                                                                                                                <td class="dhtrc">
                                                                                                                    <table id="CollapsibleRegionTotalRecords" style="display: none;" cellspacing="0" cellpadding="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="dhtrct"><%# GetResourceValue("Txt:TotalItems", "WKCRM") %>&nbsp;<asp:Label runat="server" ID="taskNotificationTotalItems">
                                                                                                                            </asp:Label></td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td class="dher">
                                                                                                                    <img src="../Images/space.gif" alt="" /></td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="dBody">
                                                                                                        <table id="CollapsibleRegion" style="display: block;" cellspacing="0" cellpadding="0" border="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <table id="FilterRegion" cellpadding="0" cellspacing="3" border="0">

                                                                                                                        <!-- Search & Filter Area -->
                                                                                                                        <tr>
                                                                                                                            <td class="fila"></td>
                                                                                                                            <td>
                                                                                                                                <%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("taskNotificationTableControl$taskNotificationSearchButton")) %>

                                                                                                                                <%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("taskNotificationTableControl$taskNotificationSearchButton")) %>
                                                                                                                            </td>
                                                                                                                            <td class="filbc"></td>
                                                                                                                        </tr>


                                                                                                                        <tr>
                                                                                                                            <td></td>
                                                                                                                            <td></td>
                                                                                                                            <td rowspan="100" class="filbc"></td>
                                                                                                                        </tr>
                                                                                                                        <%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("taskNotificationTableControl$taskNotificationFilterButton")) %>

                                                                                                                        <tr>
                                                                                                                            <td class="fila"></td>
                                                                                                                            <td></td>
                                                                                                                        </tr>

                                                                                                                        <%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("taskNotificationTableControl$taskNotificationFilterButton")) %>
                                                                                                                    </table>
                                                                                                                    <div class="spacer"></div>
                                                                                                                    <!-- Category Area -->
                                                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <table id="CategoryRegion" class="tv" cellpadding="0" cellspacing="0" border="0">

                                                                                                                                                <tr>
                                                                                                                                                    <!-- Pagination Area -->
                                                                                                                                                    <td class="pr" colspan="100">
                                                                                                                                                        <table id="PaginationRegion" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                                            <tr>
                                                                                                                                                                <td class="prel">
                                                                                                                                                                    <img src="../Images/space.gif" alt="" /></td>


                                                                                                                                                                <td class="prbbc"></td>

                                                                                                                                                                <td class="prbbc">
                                                                                                                                                                    <WKCRM:ThemeButton runat="server" ID="taskNotificationEditButton" Button-CausesValidation="False" Button-CommandName="UpdateData" Button-Text="Update"></WKCRM:ThemeButton>
                                                                                                                                                                </td>

                                                                                                                                                                <td class="prbbc"></td>

                                                                                                                                                                <td class="prbbc"></td>



                                                                                                                                                                <td class="prer">
                                                                                                                                                                    <img src="../Images/space.gif" alt="" /></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </table>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                <!--Table View Area -->
                                                                                                                                                <tr>
                                                                                                                                                    <td class="tre">
                                                                                                                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" onkeydown="captureUpDownKey(this, event)">
                                                                                                                                                            <!-- This is the table's header row -->

                                                                                                                                                            <div id="AJAXUpdateHeaderRow">
                                                                                                                                                                <tr class="tch">



                                                                                                                                                                    <th class="thc" scope="col">
                                                                                                                                                                        <asp:Literal runat="server" ID="date_createdLabel" Text="Date Created">
                                                                                                                                                                        </asp:Literal></th>

                                                                                                                                                                    <th class="thc" scope="col">
                                                                                                                                                                        <asp:Literal runat="server" ID="taskNotificationTypeLabel1" Text="Notification Type">
                                                                                                                                                                        </asp:Literal></th>

                                                                                                                                                                    <th class="thc" scope="col">
                                                                                                                                                                        <asp:Literal runat="server" ID="TaskDescriptionLabel" Text="Task Description">
                                                                                                                                                                        </asp:Literal></th>

                                                                                                                                                                    <th class="thc" scope="col">
                                                                                                                                                                        <asp:Literal runat="server" ID="completedLabel" Text="Completed">
                                                                                                                                                                        </asp:Literal></th>



                                                                                                                                                                </tr>
                                                                                                                                                            </div>

                                                                                                                                                            <!-- Table Rows -->
                                                                                                                                                            <asp:Repeater runat="server" ID="taskNotificationTableControlRepeater">
                                                                                                                                                                <ItemTemplate>
                                                                                                                                                                    <WKCRM:taskNotificationTableControlRow runat="server" ID="taskNotificationTableControlRow">

                                                                                                                                                                        <div id="AJAXUpdateRecordRow">
                                                                                                                                                                            <tr>




                                                                                                                                                                                <td class="ttc">
                                                                                                                                                                                    <asp:Literal runat="server" ID="date_created">
                                                                                                                                                                                    </asp:Literal></td>

                                                                                                                                                                                <td class="ttc">
                                                                                                                                                                                    <asp:Literal runat="server" ID="taskNotificationType">
                                                                                                                                                                                    </asp:Literal></td>

                                                                                                                                                                                <td class="ttc">
                                                                                                                                                                                    <asp:Literal runat="server" ID="TaskDescription">
                                                                                                                                                                                    </asp:Literal></td>

                                                                                                                                                                                <td class="ttc">
                                                                                                                                                                                    <asp:CheckBox runat="server" ID="completed" CheckedValue="Yes" UncheckedValue="No"></asp:CheckBox></td>



                                                                                                                                                                            </tr>
                                                                                                                                                                        </div>

                                                                                                                                                                    </WKCRM:taskNotificationTableControlRow>
                                                                                                                                                                </ItemTemplate>
                                                                                                                                                            </asp:Repeater>
                                                                                                                                                            <!-- Totals Area -->


                                                                                                                                                        </table>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </table>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <!-- End Table Panel.html -->

                                                                                        </WKCRM:taskNotificationTableControl>

                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>

                                                                                <br />
                                                                                <br />

                                                                                <br />
                                                                                <br />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="pcR"></td>
                                                </tr>
                                                <tr>
                                                    <td class="pcBL"></td>
                                                    <td class="pcB">
                                                        <WKCRM:Footer runat="server" ID="PageFooter"></WKCRM:Footer>
                                                    </td>
                                                    <td class="pcBR"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="pbR">
                                            <img src="../Images/space.gif" alt="" /></td>
                                    </tr>
                                    <tr>
                                        <td class="pbBL">
                                            <img src="../Images/space.gif" alt="" /></td>
                                        <td class="pbB">
                                            <img src="../Images/space.gif" alt="" /></td>
                                        <td class="pbBR">
                                            <img src="../Images/space.gif" alt="" /></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 15%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
    </form>
</body>
</html>



