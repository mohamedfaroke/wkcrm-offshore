﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Opportunity.aspx.cs" Inherits="WKCRM.UI.Opportunity" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.Opportunity" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
	<br />
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" />
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
		<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
		<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	<asp:UpdateProgress runat="server" id="opportunityRecordControlUpdateProgress" AssociatedUpdatePanelID="opportunityRecordControlUpdatePanel">
										<ProgressTemplate>
											<div style="position:absolute;   width:100%;height:1000px;background-color:#000000;filter:alpha(opacity=10);opacity:0.20;-moz-opacity:0.20;padding:20px;">
											</div>
											<div style=" position:absolute; padding:30px;">
												<img src="../Images/updating.gif">
											</div>
										</ProgressTemplate>
									</asp:UpdateProgress>
									<asp:UpdatePanel runat="server" id="opportunityRecordControlUpdatePanel" UpdateMode="Conditional">

										<ContentTemplate>
											<WKCRM:opportunityRecordControl runat="server" id="opportunityRecordControl">
														
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="opportunityDialogIcon" AlternateText="">

														</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="opportunityDialogTitle" Text="Add Opportunity">
														</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="location_idLabel" Text="Location">
														</asp:Literal>
	</td>
	<td class="dfv">
		<asp:DropDownList runat="server" id="location_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="location_idFvLlsHyperLink" ControlToUpdate="location_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="location" Field="Location_.id" DisplayField="Location_.name">														</Selectors:FvLlsHyperLink>&nbsp;
														<asp:RequiredFieldValidator runat="server" id="location_idRequiredFieldValidator" ControlToValidate="location_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Location&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">														</asp:RequiredFieldValidator>
	</td>
	</tr>
	
	
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="opportunity_source_idLabel" Text="Opportunity Source">
														</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="opportunity_source_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
														</asp:DropDownList>
														<Selectors:FvLlsHyperLink runat="server" id="opportunity_source_idFvLlsHyperLink" ControlToUpdate="opportunity_source_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="opportunitySource" Field="OpportunitySource_.id" DisplayField="OpportunitySource_.name">														</Selectors:FvLlsHyperLink></td>
	</tr>
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
</td><td style="width:15%"></td></tr></table>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<!-- End Record Panel.html -->

											</WKCRM:opportunityRecordControl>
											</ContentTemplate>
										</asp:UpdatePanel>
									


<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
										</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td></td>
	</tr>
	</table>
	</td>
	</tr>
</table>
<td style="width:15%"></td>

<br/>
<br/></form>
	</body>
</html>
