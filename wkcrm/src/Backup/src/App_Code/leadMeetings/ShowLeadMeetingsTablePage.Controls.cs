﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowLeadMeetingsTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowLeadMeetingsTablePage
{
  

#region "Section 1: Place your customizations here."

    
public class leadMeetingsTableControlRow : BaseleadMeetingsTableControlRow
{
      
        // The BaseleadMeetingsTableControlRow implements code for a ROW within the
        // the leadMeetingsTableControl table.  The BaseleadMeetingsTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of leadMeetingsTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class leadMeetingsTableControl : BaseleadMeetingsTableControl
{
        // The BaseleadMeetingsTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The leadMeetingsTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

    public override WhereClause CreateWhereClause()
    {
        WhereClause wc = base.CreateWhereClause();
        string roleIds = this.Page.SystemUtils.GetUserRole();
        string userID = this.Page.SystemUtils.GetUserID();
        if (MyRoles.IsHostess(roleIds))
        {
            EmployeeRecord empRec = EmployeeTable.GetRecord(userID, false);
            if (empRec != null)
            {
                string hostessWC = String.Format("designer_id IN (SELECT id FROM employee WHERE location_id={0})", empRec.location_id);
                wc.iAND(hostessWC);
            }
        }
        else if (MyRoles.IsDesigner(roleIds))
        {
            string designerWC = String.Format("designer_id=" + userID);
            wc.iAND(designerWC);
        }
        return wc;
    }
}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the leadMeetingsTableControlRow control on the ShowLeadMeetingsTablePage page.
// Do not modify this class. Instead override any method in leadMeetingsTableControlRow.
public class BaseleadMeetingsTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseleadMeetingsTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in leadMeetingsTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.leadMeetingsRecordRowEditButton.Click += new ImageClickEventHandler(leadMeetingsRecordRowEditButton_Click);
        }

        // To customize, override this method in leadMeetingsTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in leadMeetingsTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = LeadMeetingsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseleadMeetingsTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new LeadMeetingsRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in leadMeetingsTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.account_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.account_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.account_id.Text = formattedValue;
            } else {  
                this.account_id.Text = LeadMeetingsTable.account_id.Format(LeadMeetingsTable.account_id.DefaultValue);
            }
                    
            if (this.account_id.Text == null ||
                this.account_id.Text.Trim().Length == 0) {
                this.account_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.address);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.LeadMeetingsTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"address\\\", \\\"Address\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = LeadMeetingsTable.address.Format(LeadMeetingsTable.address.DefaultValue);
            }
                    
            if (this.address.Text == null ||
                this.address.Text.Trim().Length == 0) {
                this.address.Text = "&nbsp;";
            }
                  
            if (this.DataSource.notesSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.notes);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.LeadMeetingsTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"notes\\\", \\\"Notes\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.bench_and_benchtop_selection.Text = formattedValue;
            } else {  
                this.bench_and_benchtop_selection.Text = LeadMeetingsTable.notes.Format(LeadMeetingsTable.notes.DefaultValue);
            }
                    
            if (this.bench_and_benchtop_selection.Text == null ||
                this.bench_and_benchtop_selection.Text.Trim().Length == 0) {
                this.bench_and_benchtop_selection.Text = "&nbsp;";
            }
                  
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime1.Text = formattedValue;
            } else {  
                this.datetime1.Text = LeadMeetingsTable.datetime0.Format(LeadMeetingsTable.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime1.Text == null ||
                this.datetime1.Text.Trim().Length == 0) {
                this.datetime1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.designer_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.designer_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.designer_id.Text = formattedValue;
            } else {  
                this.designer_id.Text = LeadMeetingsTable.designer_id.Format(LeadMeetingsTable.designer_id.DefaultValue);
            }
                    
            if (this.designer_id.Text == null ||
                this.designer_id.Text.Trim().Length == 0) {
                this.designer_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.hostess_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.hostess_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.hostess_id.Text = formattedValue;
            } else {  
                this.hostess_id.Text = LeadMeetingsTable.hostess_id.Format(LeadMeetingsTable.hostess_id.DefaultValue);
            }
                    
            if (this.hostess_id.Text == null ||
                this.hostess_id.Text.Trim().Length == 0) {
                this.hostess_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.location_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.location_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.location.Text = formattedValue;
            } else {  
                this.location.Text = LeadMeetingsTable.location_id.Format(LeadMeetingsTable.location_id.DefaultValue);
            }
                    
            if (this.DataSource.onsiteSpecified) {
                      
                string formattedValue = this.DataSource.Format(LeadMeetingsTable.onsite, @"Showroom,Onsite");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.onsite.Text = formattedValue;
            } else {  
                this.onsite.Text = LeadMeetingsTable.onsite.Format(LeadMeetingsTable.onsite.DefaultValue, @"Showroom,Onsite");
            }
                    
            if (this.onsite.Text == null ||
                this.onsite.Text.Trim().Length == 0) {
                this.onsite.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in leadMeetingsTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in leadMeetingsTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).DataChanged = true;
                ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in leadMeetingsTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            LeadMeetingsTable.DeleteRecord(pk);

          
            ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).DataChanged = true;
            ((leadMeetingsTableControl)MiscUtils.GetParentControlObject(this, "leadMeetingsTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void leadMeetingsRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../leadMeetings/EditLeadMeetingsPage.aspx?LeadMeetings={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseleadMeetingsTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseleadMeetingsTableControlRow_Rec"] = value;
            }
        }
        
        private LeadMeetingsRecord _DataSource;
        public LeadMeetingsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal account_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal address {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
           
        public System.Web.UI.WebControls.Literal bench_and_benchtop_selection {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "bench_and_benchtop_selection");
            }
        }
           
        public System.Web.UI.WebControls.Literal datetime1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime1");
            }
        }
           
        public System.Web.UI.WebControls.Literal designer_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal hostess_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "hostess_id");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton leadMeetingsRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsRecordRowEditButton");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox leadMeetingsRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Label location {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location");
            }
        }
           
        public System.Web.UI.WebControls.Literal onsite {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "onsite");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            LeadMeetingsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public LeadMeetingsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return LeadMeetingsTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the leadMeetingsTableControl control on the ShowLeadMeetingsTablePage page.
// Do not modify this class. Instead override any method in leadMeetingsTableControl.
public class BaseleadMeetingsTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseleadMeetingsTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.leadMeetingsPagination.FirstPage.Click += new ImageClickEventHandler(leadMeetingsPagination_FirstPage_Click);
            this.leadMeetingsPagination.LastPage.Click += new ImageClickEventHandler(leadMeetingsPagination_LastPage_Click);
            this.leadMeetingsPagination.NextPage.Click += new ImageClickEventHandler(leadMeetingsPagination_NextPage_Click);
            this.leadMeetingsPagination.PageSizeButton.Button.Click += new EventHandler(leadMeetingsPagination_PageSizeButton_Click);
            this.leadMeetingsPagination.PreviousPage.Click += new ImageClickEventHandler(leadMeetingsPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.datetimeLabel.Click += new EventHandler(datetimeLabel_Click);

            // Setup the button events.
        
            this.leadMeetingsDeleteButton.Button.Click += new EventHandler(leadMeetingsDeleteButton_Click);
            this.leadMeetingsEditButton.Button.Click += new EventHandler(leadMeetingsEditButton_Click);
            this.leadMeetingsSearchButton.Button.Click += new EventHandler(leadMeetingsSearchButton_Click);

            // Setup the filter and search events.
        
            this.account_idFilter.SelectedIndexChanged += new EventHandler(account_idFilter_SelectedIndexChanged);
            this.designer_idFilter.SelectedIndexChanged += new EventHandler(designer_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.account_idFilter)) {
                this.account_idFilter.Items.Add(new ListItem(this.GetFromSession(this.account_idFilter), this.GetFromSession(this.account_idFilter)));
                this.account_idFilter.SelectedValue = this.GetFromSession(this.account_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.designer_idFilter)) {
                this.designer_idFilter.Items.Add(new ListItem(this.GetFromSession(this.designer_idFilter), this.GetFromSession(this.designer_idFilter)));
                this.designer_idFilter.SelectedValue = this.GetFromSession(this.designer_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.leadMeetingsDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (LeadMeetingsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = LeadMeetingsTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (LeadMeetingsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (leadMeetingsTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (LeadMeetingsRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = LeadMeetingsTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateaccount_idFilter(MiscUtils.GetSelectedValue(this.account_idFilter, this.GetFromSession(this.account_idFilter)), 500);
            this.Populatedesigner_idFilter(MiscUtils.GetSelectedValue(this.designer_idFilter, this.GetFromSession(this.designer_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("leadMeetingsTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                leadMeetingsTableControlRow recControl = (leadMeetingsTableControlRow)(repItem.FindControl("leadMeetingsTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(LeadMeetingsTable.account_id, this.DataSource);
            this.Page.PregetDfkaRecords(LeadMeetingsTable.designer_id, this.DataSource);
            this.Page.PregetDfkaRecords(LeadMeetingsTable.hostess_id, this.DataSource);
            this.Page.PregetDfkaRecords(LeadMeetingsTable.location_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for leadMeetingsTableControl pagination.
        
            this.leadMeetingsPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.leadMeetingsPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.leadMeetingsPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.leadMeetingsPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.leadMeetingsPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.leadMeetingsPagination.CurrentPage.Text = "0";
            }
            this.leadMeetingsPagination.PageSize.Text = this.PageSize.ToString();
            this.leadMeetingsTotalItems.Text = this.TotalRecords.ToString();
            this.leadMeetingsPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.leadMeetingsPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (leadMeetingsTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            LeadMeetingsTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.account_idFilter)) {
                wc.iAND(LeadMeetingsTable.account_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.account_idFilter, this.GetFromSession(this.account_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.designer_idFilter)) {
                wc.iAND(LeadMeetingsTable.designer_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.designer_idFilter, this.GetFromSession(this.designer_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.leadMeetingsPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.leadMeetingsPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("leadMeetingsTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    leadMeetingsTableControlRow recControl = (leadMeetingsTableControlRow)(repItem.FindControl("leadMeetingsTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        LeadMeetingsRecord rec = new LeadMeetingsRecord();
        
                        if (recControl.account_id.Text != "") {
                            rec.Parse(recControl.account_id.Text, LeadMeetingsTable.account_id);
                        }
                        if (recControl.address.Text != "") {
                            rec.Parse(recControl.address.Text, LeadMeetingsTable.address);
                        }
                        if (recControl.bench_and_benchtop_selection.Text != "") {
                            rec.Parse(recControl.bench_and_benchtop_selection.Text, LeadMeetingsTable.notes);
                        }
                        if (recControl.datetime1.Text != "") {
                            rec.Parse(recControl.datetime1.Text, LeadMeetingsTable.datetime0);
                        }
                        if (recControl.designer_id.Text != "") {
                            rec.Parse(recControl.designer_id.Text, LeadMeetingsTable.designer_id);
                        }
                        if (recControl.hostess_id.Text != "") {
                            rec.Parse(recControl.hostess_id.Text, LeadMeetingsTable.hostess_id);
                        }
                        if (recControl.location.Text != "") {
                            rec.Parse(recControl.location.Text, LeadMeetingsTable.location_id);
                        }
                        if (recControl.onsite.Text != "") {
                            rec.Parse(recControl.onsite.Text, LeadMeetingsTable.onsite);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new LeadMeetingsRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (LeadMeetingsRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.LeadMeetingsRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(leadMeetingsTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(leadMeetingsTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for account_idFilter.
        protected virtual void Populateaccount_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(AccountTable.Name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.account_idFilter.Items.Clear();
            foreach (AccountRecord itemValue in AccountTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(AccountTable.Name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.account_idFilter.Items.IndexOf(item) < 0) {
                    this.account_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.account_idFilter, selectedValue);

            // Add the All item.
            this.account_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for designer_idFilter.
        protected virtual void Populatedesigner_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.designer_idFilter.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.designer_idFilter.Items.IndexOf(item) < 0) {
                    this.designer_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.designer_idFilter, selectedValue);

            // Add the All item.
            this.designer_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter account_idFilter.
        public virtual WhereClause CreateWhereClause_account_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.designer_idFilter)) {
                wc.iAND(LeadMeetingsTable.designer_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.designer_idFilter, this.GetFromSession(this.designer_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter designer_idFilter.
        public virtual WhereClause CreateWhereClause_designer_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.account_idFilter)) {
                wc.iAND(LeadMeetingsTable.account_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.account_idFilter, this.GetFromSession(this.account_idFilter)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.account_idFilter, this.account_idFilter.SelectedValue);
            this.SaveToSession(this.designer_idFilter, this.designer_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.account_idFilter);
            this.RemoveFromSession(this.designer_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["leadMeetingsTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["leadMeetingsTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void leadMeetingsPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void leadMeetingsPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void leadMeetingsPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void leadMeetingsPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void leadMeetingsPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void datetimeLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(LeadMeetingsTable.datetime0);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(LeadMeetingsTable.datetime0, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void leadMeetingsDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void leadMeetingsEditButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../leadMeetings/EditLeadMeetingsPage.aspx?LeadMeetings={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void leadMeetingsSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void account_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void designer_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private LeadMeetingsRecord[] _DataSource = null;
        public  LeadMeetingsRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.DropDownList account_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal account_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal account_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal bench_and_benchtop_selectionLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "bench_and_benchtop_selectionLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton datetimeLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList designer_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal hostess_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "hostess_idLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton leadMeetingsDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton leadMeetingsEditButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsEditButton");
            }
        }
        
        public WKCRM.UI.IPagination leadMeetingsPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsPagination");
            }
        }
        
        public System.Web.UI.WebControls.TextBox leadMeetingsSearchArea {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsSearchArea");
            }
        }
        
        public WKCRM.UI.IThemeButton leadMeetingsSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal leadMeetingsTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox leadMeetingsToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label leadMeetingsTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "leadMeetingsTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Label locationLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "locationLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal onsiteLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "onsiteLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                leadMeetingsTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                LeadMeetingsRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (leadMeetingsTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.leadMeetingsRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public leadMeetingsTableControlRow GetSelectedRecordControl()
        {
        leadMeetingsTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public leadMeetingsTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (leadMeetingsTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.leadMeetingsRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (leadMeetingsTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowLeadMeetingsTablePage.leadMeetingsTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            leadMeetingsTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (leadMeetingsTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.leadMeetingsRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public leadMeetingsTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("leadMeetingsTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                leadMeetingsTableControlRow recControl = (leadMeetingsTableControlRow)repItem.FindControl("leadMeetingsTableControlRow");
                recList.Add(recControl);
            }

            return (leadMeetingsTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowLeadMeetingsTablePage.leadMeetingsTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  