﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// EditTradesmenPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.EditTradesmenPage
{
  

#region "Section 1: Place your customizations here."

    
public class tradesmenRecordControl : BasetradesmenRecordControl
{
      
        // The BasetradesmenRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the tradesmenRecordControl control on the EditTradesmenPage page.
// Do not modify this class. Instead override any method in tradesmenRecordControl.
public class BasetradesmenRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BasetradesmenRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in tradesmenRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in tradesmenRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in tradesmenRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = TradesmenTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new TradesmenRecord();
                return;
            }

            // Retrieve the record from the database.
            TradesmenRecord[] recList = TradesmenTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = TradesmenTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in tradesmenRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.ABNSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.ABN);
                this.ABN.Text = formattedValue;
            } else {  
                this.ABN.Text = TradesmenTable.ABN.Format(TradesmenTable.ABN.DefaultValue);
            }
                    
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.address);
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = TradesmenTable.address.Format(TradesmenTable.address.DefaultValue);
            }
                    
            if (this.DataSource.emailSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.email);
                this.email.Text = formattedValue;
            } else {  
                this.email.Text = TradesmenTable.email.Format(TradesmenTable.email.DefaultValue);
            }
                    
            if (this.DataSource.entity_type_idSpecified) {
                this.Populateentity_type_idDropDownList(this.DataSource.entity_type_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateentity_type_idDropDownList(TradesmenTable.entity_type_id.DefaultValue, 100);
                } else {
                this.Populateentity_type_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.faxSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.fax);
                this.fax.Text = formattedValue;
            } else {  
                this.fax.Text = TradesmenTable.fax.Format(TradesmenTable.fax.DefaultValue);
            }
                    
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.mobile);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = TradesmenTable.mobile.Format(TradesmenTable.mobile.DefaultValue);
            }
                    
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.name);
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = TradesmenTable.name.Format(TradesmenTable.name.DefaultValue);
            }
                    
            if (this.DataSource.phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.phone);
                this.phone.Text = formattedValue;
            } else {  
                this.phone.Text = TradesmenTable.phone.Format(TradesmenTable.phone.DefaultValue);
            }
                    
            this.public_liability_expiry_date.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.public_liability_expiry_date.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.public_liability_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_expiry_date);
                this.public_liability_expiry_date.Text = formattedValue;
            } else {  
                this.public_liability_expiry_date.Text = TradesmenTable.public_liability_expiry_date.Format(TradesmenTable.public_liability_expiry_date.DefaultValue);
            }
                    
            if (this.DataSource.public_liability_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_number);
                this.public_liability_number.Text = formattedValue;
            } else {  
                this.public_liability_number.Text = TradesmenTable.public_liability_number.Format(TradesmenTable.public_liability_number.DefaultValue);
            }
                    
            if (this.DataSource.public_liability_companySpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.public_liability_company);
                this.PublicLiabilityCompany.Text = formattedValue;
            } else {  
                this.PublicLiabilityCompany.Text = TradesmenTable.public_liability_company.Format(TradesmenTable.public_liability_company.DefaultValue);
            }
                    
            if (this.DataSource.send_email_notificationsSpecified) {
                this.send_email_notifications.Checked = this.DataSource.send_email_notifications;
            } else {
                if (!this.DataSource.IsCreated) {
                    this.send_email_notifications.Checked = TradesmenTable.send_email_notifications.ParseValue(TradesmenTable.send_email_notifications.DefaultValue).ToBoolean();
                }
            }
                    
            if (this.DataSource.status_idSpecified) {
                this.status_id.Checked = this.DataSource.status_id;
            } else {
                if (!this.DataSource.IsCreated) {
                    this.status_id.Checked = TradesmenTable.status_id.ParseValue(TradesmenTable.status_id.DefaultValue).ToBoolean();
                }
            }
                    
            this.trade_licence_expiry_date.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.trade_licence_expiry_date.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.trade_licence_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.trade_licence_expiry_date);
                this.trade_licence_expiry_date.Text = formattedValue;
            } else {  
                this.trade_licence_expiry_date.Text = TradesmenTable.trade_licence_expiry_date.Format(TradesmenTable.trade_licence_expiry_date.DefaultValue);
            }
                    
            if (this.DataSource.trade_licence_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.trade_licence_number);
                this.trade_licence_number.Text = formattedValue;
            } else {  
                this.trade_licence_number.Text = TradesmenTable.trade_licence_number.Format(TradesmenTable.trade_licence_number.DefaultValue);
            }
                    
            if (this.DataSource.TradesmanTypeIdSpecified) {
                this.PopulateTradesmanTypeDropDownList(this.DataSource.TradesmanTypeId.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateTradesmanTypeDropDownList(TradesmenTable.TradesmanTypeId.DefaultValue, 100);
                } else {
                this.PopulateTradesmanTypeDropDownList(null, 100);
                }
            }
                
            this.workers_comp_expiry_date.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.workers_comp_expiry_date.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.workers_comp_expiry_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_expiry_date);
                this.workers_comp_expiry_date.Text = formattedValue;
            } else {  
                this.workers_comp_expiry_date.Text = TradesmenTable.workers_comp_expiry_date.Format(TradesmenTable.workers_comp_expiry_date.DefaultValue);
            }
                    
            if (this.DataSource.workers_comp_numberSpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_number);
                this.workers_comp_number.Text = formattedValue;
            } else {  
                this.workers_comp_number.Text = TradesmenTable.workers_comp_number.Format(TradesmenTable.workers_comp_number.DefaultValue);
            }
                    
            if (this.DataSource.workers_comp_companySpecified) {
                      
                string formattedValue = this.DataSource.Format(TradesmenTable.workers_comp_company);
                this.WorkersCompCompany.Text = formattedValue;
            } else {  
                this.WorkersCompCompany.Text = TradesmenTable.workers_comp_company.Format(TradesmenTable.workers_comp_company.DefaultValue);
            }
                    
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in tradesmenRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in tradesmenRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in tradesmenRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in tradesmenRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.ABN.Text, TradesmenTable.ABN);
                          
            this.DataSource.Parse(this.address.Text, TradesmenTable.address);
                          
            this.DataSource.Parse(this.email.Text, TradesmenTable.email);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.entity_type_id), TradesmenTable.entity_type_id);
                  
            this.DataSource.Parse(this.fax.Text, TradesmenTable.fax);
                          
            this.DataSource.Parse(this.mobile.Text, TradesmenTable.mobile);
                          
            this.DataSource.Parse(this.name.Text, TradesmenTable.name);
                          
            this.DataSource.Parse(this.phone.Text, TradesmenTable.phone);
                          
            this.DataSource.Parse(this.public_liability_expiry_date.Text, TradesmenTable.public_liability_expiry_date);
                          
            this.DataSource.Parse(this.public_liability_number.Text, TradesmenTable.public_liability_number);
                          
            this.DataSource.Parse(this.PublicLiabilityCompany.Text, TradesmenTable.public_liability_company);
                          
            this.DataSource.send_email_notifications = this.send_email_notifications.Checked;
                    
            this.DataSource.status_id = this.status_id.Checked;
                    
            this.DataSource.Parse(this.trade_licence_expiry_date.Text, TradesmenTable.trade_licence_expiry_date);
                          
            this.DataSource.Parse(this.trade_licence_number.Text, TradesmenTable.trade_licence_number);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.TradesmanType), TradesmenTable.TradesmanTypeId);
                  
            this.DataSource.Parse(this.workers_comp_expiry_date.Text, TradesmenTable.workers_comp_expiry_date);
                          
            this.DataSource.Parse(this.workers_comp_number.Text, TradesmenTable.workers_comp_number);
                          
            this.DataSource.Parse(this.WorkersCompCompany.Text, TradesmenTable.workers_comp_company);
                          
        }

        //  To customize, override this method in tradesmenRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            TradesmenTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["Tradesmen"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "Tradesmen"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(TradesmenTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(TradesmenTable.id0).ToString());
            } else {
                
                wc.iAND(TradesmenTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in tradesmenRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            TradesmenTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_entity_type_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_TradesmanTypeDropDownList() {
            return new WhereClause();
        }
                
        // Fill the entity_type_id list.
        protected virtual void Populateentity_type_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_entity_type_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TradeEntityTypesTable.name, OrderByItem.OrderDir.Asc);

                      this.entity_type_id.Items.Clear();
            foreach (TradeEntityTypesRecord itemValue in TradeEntityTypesTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(TradeEntityTypesTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.entity_type_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.entity_type_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.entity_type_id, TradesmenTable.entity_type_id.Format(selectedValue))) {
                string fvalue = TradesmenTable.entity_type_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.entity_type_id.Items.Insert(0, item);
            }

                  
            this.entity_type_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the TradesmanType list.
        protected virtual void PopulateTradesmanTypeDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_TradesmanTypeDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(TradesmanTypeTable.TradesmanType, OrderByItem.OrderDir.Asc);

                      this.TradesmanType.Items.Clear();
            foreach (TradesmanTypeRecord itemValue in TradesmanTypeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.TradesmanTypeIdSpecified) {
                    cvalue = itemValue.TradesmanTypeId.ToString();
                    fvalue = itemValue.Format(TradesmanTypeTable.TradesmanType);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.TradesmanType.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.TradesmanType, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.TradesmanType, TradesmenTable.TradesmanTypeId.Format(selectedValue))) {
                string fvalue = TradesmenTable.TradesmanTypeId.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.TradesmanType.Items.Insert(0, item);
            }

                  
            this.TradesmanType.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasetradesmenRecordControl_Rec"];
            }
            set {
                this.ViewState["BasetradesmenRecordControl_Rec"] = value;
            }
        }
        
        private TradesmenRecord _DataSource;
        public TradesmenRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.TextBox ABN {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "ABN");
            }
        }
           
        public System.Web.UI.WebControls.TextBox address {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox email {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email");
            }
        }
        
        public System.Web.UI.WebControls.Literal emailLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "emailLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList entity_type_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal entity_type_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "entity_type_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox fax {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "fax");
            }
        }
        
        public System.Web.UI.WebControls.Literal faxLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "faxLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.Label Label1 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label1");
            }
        }
        
        public System.Web.UI.WebControls.Label Label2 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label2");
            }
        }
        
        public System.Web.UI.WebControls.Label Label3 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label3");
            }
        }
           
        public System.Web.UI.WebControls.TextBox mobile {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox name {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
        
        public System.Web.UI.WebControls.Literal nameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox phone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox public_liability_expiry_date {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_expiry_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal public_liability_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_expiry_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox public_liability_number {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_number");
            }
        }
        
        public System.Web.UI.WebControls.Literal public_liability_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "public_liability_numberLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox PublicLiabilityCompany {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "PublicLiabilityCompany");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox send_email_notifications {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "send_email_notifications");
            }
        }
        
        public System.Web.UI.WebControls.Literal send_email_notificationsLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "send_email_notificationsLabel");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox status_id {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal status_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox trade_licence_expiry_date {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_expiry_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal trade_licence_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_expiry_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox trade_licence_number {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_number");
            }
        }
        
        public System.Web.UI.WebControls.Literal trade_licence_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "trade_licence_numberLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList TradesmanType {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TradesmanType");
            }
        }
        
        public System.Web.UI.WebControls.Image tradesmenDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal tradesmenDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmenDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.TextBox workers_comp_expiry_date {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_expiry_date");
            }
        }
        
        public System.Web.UI.WebControls.Literal workers_comp_expiry_dateLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_expiry_dateLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox workers_comp_number {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_number");
            }
        }
        
        public System.Web.UI.WebControls.Literal workers_comp_numberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "workers_comp_numberLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox WorkersCompCompany {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "WorkersCompCompany");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            TradesmenRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public TradesmenRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return TradesmenTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  