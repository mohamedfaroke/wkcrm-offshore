﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowEvaluationQuestionOptionsPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowEvaluationQuestionOptionsPage
{
  

#region "Section 1: Place your customizations here."

    
public class EvaluationQuestionOptionsRecordControl : BaseEvaluationQuestionOptionsRecordControl
{
      
        // The BaseEvaluationQuestionOptionsRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.
        

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the EvaluationQuestionOptionsRecordControl control on the ShowEvaluationQuestionOptionsPage page.
// Do not modify this class. Instead override any method in EvaluationQuestionOptionsRecordControl.
public class BaseEvaluationQuestionOptionsRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseEvaluationQuestionOptionsRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in EvaluationQuestionOptionsRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in EvaluationQuestionOptionsRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EvaluationQuestionOptionsTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new EvaluationQuestionOptionsRecord();
                return;
            }

            // Retrieve the record from the database.
            EvaluationQuestionOptionsRecord[] recList = EvaluationQuestionOptionsTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = EvaluationQuestionOptionsTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.OptionIdSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionOptionsTable.OptionId);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.OptionId.Text = formattedValue;
            } else {  
                this.OptionId.Text = EvaluationQuestionOptionsTable.OptionId.Format(EvaluationQuestionOptionsTable.OptionId.DefaultValue);
            }
                    
            if (this.OptionId.Text == null ||
                this.OptionId.Text.Trim().Length == 0) {
                this.OptionId.Text = "&nbsp;";
            }
                  
            if (this.DataSource.QuestionIdSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionOptionsTable.QuestionId);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.QuestionId.Text = formattedValue;
            } else {  
                this.QuestionId.Text = EvaluationQuestionOptionsTable.QuestionId.Format(EvaluationQuestionOptionsTable.QuestionId.DefaultValue);
            }
                    
            if (this.QuestionId.Text == null ||
                this.QuestionId.Text.Trim().Length == 0) {
                this.QuestionId.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in EvaluationQuestionOptionsRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in EvaluationQuestionOptionsRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            EvaluationQuestionOptionsTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["EvaluationQuestionOptions"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "EvaluationQuestionOptions"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(EvaluationQuestionOptionsTable.QuestionId, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(EvaluationQuestionOptionsTable.QuestionId).ToString());
                wc.iAND(EvaluationQuestionOptionsTable.OptionId, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(EvaluationQuestionOptionsTable.OptionId).ToString());
            } else {
                
                wc.iAND(EvaluationQuestionOptionsTable.QuestionId, BaseFilter.ComparisonOperator.EqualsTo, recId);
                wc.iAND(EvaluationQuestionOptionsTable.OptionId, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in EvaluationQuestionOptionsRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EvaluationQuestionOptionsTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseEvaluationQuestionOptionsRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseEvaluationQuestionOptionsRecordControl_Rec"] = value;
            }
        }
        
        private EvaluationQuestionOptionsRecord _DataSource;
        public EvaluationQuestionOptionsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Image EvaluationQuestionOptionsDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal EvaluationQuestionOptionsDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionOptionsDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.Literal OptionId {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionId");
            }
        }
        
        public System.Web.UI.WebControls.Literal OptionIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionIdLabel");
            }
        }
           
        public System.Web.UI.WebControls.Literal QuestionId {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionId");
            }
        }
        
        public System.Web.UI.WebControls.Literal QuestionIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionIdLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EvaluationQuestionOptionsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EvaluationQuestionOptionsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return EvaluationQuestionOptionsTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  