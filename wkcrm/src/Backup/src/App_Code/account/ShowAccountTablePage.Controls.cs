﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowAccountTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
using Impowa.Dev.Utilities;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowAccountTablePage
{
  

#region "Section 1: Place your customizations here."
  

public class View_AccountLeadPhaseTableControl : BaseView_AccountLeadPhaseTableControl
{
        // The BaseView_AccountLeadPhaseTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The View_AccountLeadPhaseTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

    public override void LoadData()
    {
        if (!this.Page.IsPostBack)
            Populateaccount_status_idFilter1(Globals.ACCOUNT_OPEN.ToString(), 500);
        base.LoadData();
    }

    public override WhereClause CreateWhereClause()
    {
        WhereClause wc = base.CreateWhereClause();
        string roleIds = this.Page.SystemUtils.GetUserRole();
        string userID = this.Page.SystemUtils.GetUserID();
        if(MyRoles.IsHostess(roleIds))
        {
            EmployeeRecord empRec = EmployeeTable.GetRecord(userID, false);
            if(empRec != null)
            {
                string hostessWC = String.Format("designer_id IN (SELECT id FROM employee WHERE location_id={0})", empRec.location_id);
                wc.iAND(hostessWC);
            }
        }
        else if(MyRoles.IsDesigner(roleIds))
        {
            string designerWC = String.Format("designer_id=" + userID);
            wc.iAND(designerWC);
        }
        return wc;
    }

    public override void View_AccountLeadPhaseDeleteButton_Click(object sender, EventArgs args)
    {
        ArrayList selectedRecords = new ArrayList();
        foreach (View_AccountLeadPhaseTableControlRow row in this.GetSelectedRecordControls())
        {
            selectedRecords.Add(Globals.GetRecordIDFromXML(row.RecordUniqueId, "id"));
        }
        if (selectedRecords.Count > 0)
        {
            this.Page.Session["DeactiveAccounts"] = selectedRecords;
            this.Page.Response.Redirect("./DeactivateAccount.aspx");
        }
        else
            MiscUtils.RegisterJScriptAlert(this, "SelectKey", "Please select accounts to deactivate");
    }


    protected override void Populatedesigner_idFilter1(string selectedValue, int maxItems)
    {
       // base.Populatedesigner_idFilter1(selectedValue, maxItems);
        designer_idFilter1.Items.Clear();
        foreach (EmployeeRecord emp in Globals.Designers)
        {
            ListItem itm = new ListItem(emp.name, emp.id0.ToString());
            designer_idFilter1.Items.Add(itm);
        }

        // Add the All item.
        this.designer_idFilter1.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));

        if (!String.IsNullOrEmpty(selectedValue))
        {
            ListItem selectedItem = designer_idFilter1.Items.FindByValue(selectedValue);
            if (selectedItem != null)
            {
                selectedItem.Selected = true;
            }
        }
    }

    protected override void Populateaccount_status_idFilter1(string selectedValue, int maxItems)
    {
        Globals.PopulateAccountStatusDropdowList(selectedValue, account_status_idFilter1);
    }
}
public class View_AccountLeadPhaseTableControlRow : BaseView_AccountLeadPhaseTableControlRow
{
      
        // The BaseView_AccountLeadPhaseTableControlRow implements code for a ROW within the
        // the View_AccountLeadPhaseTableControl table.  The BaseView_AccountLeadPhaseTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of View_AccountLeadPhaseTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public View_AccountLeadPhaseTableControlRow()
    {
        this.Load += new EventHandler(View_AccountLeadPhaseTableControlRow_Load);
        this.PreRender += new EventHandler(View_AccountLeadPhaseTableControlRow_PreRender);
    }

    void View_AccountLeadPhaseTableControlRow_Load(object sender, EventArgs e)
    {
    }

    void View_AccountLeadPhaseTableControlRow_PreRender(object sender, EventArgs e)
    {
        RadAjaxManager manager = this.Page.FindControl("RadAjaxManager1") as RadAjaxManager;
        Label injectScript = this.Page.FindControl("injectScript") as Label;
        if (manager != null && injectScript != null)
        {
           // manager.AjaxSettings.AddAjaxSetting(projectButton, injectScript);
        }
    }

    public override void projectButton_Click(object sender, ImageClickEventArgs args)
    {
        string errorMsg;
        string accID = Globals.GetRecordIDFromXML(this.RecordUniqueId, "id");
        if (Globals.CanStartProjectPhase(accID, out errorMsg))
        {
            Label injectScript = this.Page.FindControl("injectScript") as Label;

            if (injectScript != null)
            {
                string url = "../PaymentSchedule/FinaliseTotal.aspx?AccID=" + accID;
                injectScript.Text = "<script> Sys.Application.add_load(function(){StandardWindow('" + url + "')})</script>";
            }          
        }
        else
        {
            if (String.IsNullOrEmpty(errorMsg))
                errorMsg = "Error starting project phase";
            MiscUtils.RegisterJScriptAlert(this, "CONTRACT_ERROR", errorMsg);
        }
    }
}
#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the View_AccountLeadPhaseTableControlRow control on the ShowAccountTablePage page.
// Do not modify this class. Instead override any method in View_AccountLeadPhaseTableControlRow.
public class BaseView_AccountLeadPhaseTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseView_AccountLeadPhaseTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in View_AccountLeadPhaseTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.projectButton.Click += new ImageClickEventHandler(projectButton_Click);
            this.LastName.Click += new EventHandler(LastName_Click);
        }

        // To customize, override this method in View_AccountLeadPhaseTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
                  this.Page.Authorize((Control)projectButton, "5030;5015;5020;5025;5035");
            
        }

        // Read data from database. To customize, override this method in View_AccountLeadPhaseTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_AccountLeadPhaseView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseView_AccountLeadPhaseTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_AccountLeadPhaseRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in View_AccountLeadPhaseTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.account_status_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.account_status_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.account_status_id2.Text = formattedValue;
            } else {  
                this.account_status_id2.Text = View_AccountLeadPhaseView.account_status_id.Format(View_AccountLeadPhaseView.account_status_id.DefaultValue);
            }
                    
            if (this.account_status_id2.Text == null ||
                this.account_status_id2.Text.Trim().Length == 0) {
                this.account_status_id2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.date_createdSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.date_created);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.date_created.Text = formattedValue;
            } else {  
                this.date_created.Text = View_AccountLeadPhaseView.date_created.Format(View_AccountLeadPhaseView.date_created.DefaultValue);
            }
                    
            if (this.DataSource.designer_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.designer_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.designer_id2.Text = formattedValue;
            } else {  
                this.designer_id2.Text = View_AccountLeadPhaseView.designer_id.Format(View_AccountLeadPhaseView.designer_id.DefaultValue);
            }
                    
            if (this.designer_id2.Text == null ||
                this.designer_id2.Text.Trim().Length == 0) {
                this.designer_id2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.FirstNameSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.FirstName);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.View_AccountLeadPhaseView, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"FirstName\\\", \\\"First Name\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.FirstName.Text = formattedValue;
            } else {  
                this.FirstName.Text = View_AccountLeadPhaseView.FirstName.Format(View_AccountLeadPhaseView.FirstName.DefaultValue);
            }
                    
            if (this.FirstName.Text == null ||
                this.FirstName.Text.Trim().Length == 0) {
                this.FirstName.Text = "&nbsp;";
            }
                  
            if (this.DataSource.LastNameSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.LastName);
                this.LastName.Text = formattedValue;
            } else {  
                this.LastName.Text = View_AccountLeadPhaseView.LastName.Format(View_AccountLeadPhaseView.LastName.DefaultValue);
            }
                    
            if (this.DataSource.next_review_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.next_review_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.next_review_date2.Text = formattedValue;
            } else {  
                this.next_review_date2.Text = View_AccountLeadPhaseView.next_review_date.Format(View_AccountLeadPhaseView.next_review_date.DefaultValue);
            }
                    
            if (this.next_review_date2.Text == null ||
                this.next_review_date2.Text.Trim().Length == 0) {
                this.next_review_date2.Text = "&nbsp;";
            }
                  
            if (this.DataSource.sale_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.sale_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.sale_date.Text = formattedValue;
            } else {  
                this.sale_date.Text = View_AccountLeadPhaseView.sale_date.Format(View_AccountLeadPhaseView.sale_date.DefaultValue);
            }
                    
            if (this.DataSource.weeks_openSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountLeadPhaseView.weeks_open);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.weeks_open2.Text = formattedValue;
            } else {  
                this.weeks_open2.Text = View_AccountLeadPhaseView.weeks_open.Format(View_AccountLeadPhaseView.weeks_open.DefaultValue);
            }
                    
            if (this.weeks_open2.Text == null ||
                this.weeks_open2.Text.Trim().Length == 0) {
                this.weeks_open2.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in View_AccountLeadPhaseTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in View_AccountLeadPhaseTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in View_AccountLeadPhaseTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((View_AccountLeadPhaseTableControl)MiscUtils.GetParentControlObject(this, "View_AccountLeadPhaseTableControl")).DataChanged = true;
                ((View_AccountLeadPhaseTableControl)MiscUtils.GetParentControlObject(this, "View_AccountLeadPhaseTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in View_AccountLeadPhaseTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in View_AccountLeadPhaseTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in View_AccountLeadPhaseTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_AccountLeadPhaseView.DeleteRecord(pk);

          
            ((View_AccountLeadPhaseTableControl)MiscUtils.GetParentControlObject(this, "View_AccountLeadPhaseTableControl")).DataChanged = true;
            ((View_AccountLeadPhaseTableControl)MiscUtils.GetParentControlObject(this, "View_AccountLeadPhaseTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void projectButton_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for LinkButton
        public virtual void LastName_Click(object sender, EventArgs args)
        {
            
            string url = @"EditAccountPage.aspx?Account={View_AccountLeadPhaseTableControlRow:PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseView_AccountLeadPhaseTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseView_AccountLeadPhaseTableControlRow_Rec"] = value;
            }
        }
        
        private View_AccountLeadPhaseRecord _DataSource;
        public View_AccountLeadPhaseRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal account_status_id2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_status_id2");
            }
        }
           
        public System.Web.UI.WebControls.Label date_created {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_created");
            }
        }
           
        public System.Web.UI.WebControls.Literal designer_id2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_id2");
            }
        }
           
        public System.Web.UI.WebControls.Literal FirstName {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "FirstName");
            }
        }
           
        public System.Web.UI.WebControls.LinkButton LastName {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "LastName");
            }
        }
           
        public System.Web.UI.WebControls.Literal next_review_date2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "next_review_date2");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton projectButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "projectButton");
            }
        }
           
        public System.Web.UI.WebControls.Label sale_date {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "sale_date");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox View_AccountLeadPhaseRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhaseRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Literal weeks_open2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "weeks_open2");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_AccountLeadPhaseRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_AccountLeadPhaseRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_AccountLeadPhaseView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the View_AccountLeadPhaseTableControl control on the ShowAccountTablePage page.
// Do not modify this class. Instead override any method in View_AccountLeadPhaseTableControl.
public class BaseView_AccountLeadPhaseTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseView_AccountLeadPhaseTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.View_AccountLeadPhasePagination.FirstPage.Click += new ImageClickEventHandler(View_AccountLeadPhasePagination_FirstPage_Click);
            this.View_AccountLeadPhasePagination.LastPage.Click += new ImageClickEventHandler(View_AccountLeadPhasePagination_LastPage_Click);
            this.View_AccountLeadPhasePagination.NextPage.Click += new ImageClickEventHandler(View_AccountLeadPhasePagination_NextPage_Click);
            this.View_AccountLeadPhasePagination.PageSizeButton.Button.Click += new EventHandler(View_AccountLeadPhasePagination_PageSizeButton_Click);
            this.View_AccountLeadPhasePagination.PreviousPage.Click += new ImageClickEventHandler(View_AccountLeadPhasePagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.dateCreatedHeading.Click += new EventHandler(dateCreatedHeading_Click);
            this.FirstNameLabel.Click += new EventHandler(FirstNameLabel_Click);
            this.Label.Click += new EventHandler(Label_Click);
            this.LastNameLabel.Click += new EventHandler(LastNameLabel_Click);
            this.weeks_openLabel1.Click += new EventHandler(weeks_openLabel1_Click);

            // Setup the button events.
        
            this.Button.Button.Click += new EventHandler(Button_Click);
            this.View_AccountLeadPhaseDeleteButton.Button.Click += new EventHandler(View_AccountLeadPhaseDeleteButton_Click);
            this.View_AccountLeadPhaseNewButton.Button.Click += new EventHandler(View_AccountLeadPhaseNewButton_Click);
            this.View_AccountLeadPhaseSearchButton.Button.Click += new EventHandler(View_AccountLeadPhaseSearchButton_Click);

            // Setup the filter and search events.
        
            this.account_status_idFilter1.SelectedIndexChanged += new EventHandler(account_status_idFilter1_SelectedIndexChanged);
            this.designer_idFilter1.SelectedIndexChanged += new EventHandler(designer_idFilter1_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.account_status_idFilter1)) {
                this.account_status_idFilter1.Items.Add(new ListItem(this.GetFromSession(this.account_status_idFilter1), this.GetFromSession(this.account_status_idFilter1)));
                this.account_status_idFilter1.SelectedValue = this.GetFromSession(this.account_status_idFilter1);
            }
            if (!this.Page.IsPostBack && this.InSession(this.designer_idFilter1)) {
                this.designer_idFilter1.Items.Add(new ListItem(this.GetFromSession(this.designer_idFilter1), this.GetFromSession(this.designer_idFilter1)));
                this.designer_idFilter1.SelectedValue = this.GetFromSession(this.designer_idFilter1);
            }
            if (!this.Page.IsPostBack && this.InSession(this.View_AccountLeadPhaseSearchArea1)) {
                
                this.View_AccountLeadPhaseSearchArea1.Text = this.GetFromSession(this.View_AccountLeadPhaseSearchArea1);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_AccountLeadPhaseView.date_created, OrderByItem.OrderDir.Desc);
        
                this.CurrentSortOrder.Add(View_AccountLeadPhaseView.LastName, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.View_AccountLeadPhaseDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_AccountLeadPhaseRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_AccountLeadPhaseRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_AccountLeadPhaseView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_AccountLeadPhaseRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_AccountLeadPhaseRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (View_AccountLeadPhaseTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_AccountLeadPhaseRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_AccountLeadPhaseRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_AccountLeadPhaseView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateaccount_status_idFilter1(MiscUtils.GetSelectedValue(this.account_status_idFilter1, this.GetFromSession(this.account_status_idFilter1)), 500);
            this.Populatedesigner_idFilter1(MiscUtils.GetSelectedValue(this.designer_idFilter1, this.GetFromSession(this.designer_idFilter1)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_AccountLeadPhaseTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                View_AccountLeadPhaseTableControlRow recControl = (View_AccountLeadPhaseTableControlRow)(repItem.FindControl("View_AccountLeadPhaseTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_AccountLeadPhaseView.account_status_id, this.DataSource);
            this.Page.PregetDfkaRecords(View_AccountLeadPhaseView.designer_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for View_AccountLeadPhaseTableControl pagination.
        
            this.View_AccountLeadPhasePagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.View_AccountLeadPhasePagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.View_AccountLeadPhasePagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.View_AccountLeadPhasePagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.View_AccountLeadPhasePagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.View_AccountLeadPhasePagination.CurrentPage.Text = "0";
            }
            this.View_AccountLeadPhasePagination.PageSize.Text = this.PageSize.ToString();
            this.View_AccountLeadPhaseTotalItems.Text = this.TotalRecords.ToString();
            this.View_AccountLeadPhasePagination.TotalItems.Text = this.TotalRecords.ToString();
            this.View_AccountLeadPhasePagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (View_AccountLeadPhaseTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_AccountLeadPhaseView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.account_status_idFilter1)) {
                wc.iAND(View_AccountLeadPhaseView.account_status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.account_status_idFilter1, this.GetFromSession(this.account_status_idFilter1)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.designer_idFilter1)) {
                wc.iAND(View_AccountLeadPhaseView.designer_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.designer_idFilter1, this.GetFromSession(this.designer_idFilter1)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.View_AccountLeadPhaseSearchArea1)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(View_AccountLeadPhaseView.address, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.suburb, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.FirstName, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.LastName, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                wc.iAND(search);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.View_AccountLeadPhasePagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.View_AccountLeadPhasePagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_AccountLeadPhaseTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    View_AccountLeadPhaseTableControlRow recControl = (View_AccountLeadPhaseTableControlRow)(repItem.FindControl("View_AccountLeadPhaseTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_AccountLeadPhaseRecord rec = new View_AccountLeadPhaseRecord();
        
                        if (recControl.account_status_id2.Text != "") {
                            rec.Parse(recControl.account_status_id2.Text, View_AccountLeadPhaseView.account_status_id);
                        }
                        if (recControl.date_created.Text != "") {
                            rec.Parse(recControl.date_created.Text, View_AccountLeadPhaseView.date_created);
                        }
                        if (recControl.designer_id2.Text != "") {
                            rec.Parse(recControl.designer_id2.Text, View_AccountLeadPhaseView.designer_id);
                        }
                        if (recControl.FirstName.Text != "") {
                            rec.Parse(recControl.FirstName.Text, View_AccountLeadPhaseView.FirstName);
                        }
                        if (recControl.LastName.Text != "") {
                            rec.Parse(recControl.LastName.Text, View_AccountLeadPhaseView.LastName);
                        }
                        if (recControl.next_review_date2.Text != "") {
                            rec.Parse(recControl.next_review_date2.Text, View_AccountLeadPhaseView.next_review_date);
                        }
                        if (recControl.sale_date.Text != "") {
                            rec.Parse(recControl.sale_date.Text, View_AccountLeadPhaseView.sale_date);
                        }
                        if (recControl.weeks_open2.Text != "") {
                            rec.Parse(recControl.weeks_open2.Text, View_AccountLeadPhaseView.weeks_open);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_AccountLeadPhaseRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_AccountLeadPhaseRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_AccountLeadPhaseRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(View_AccountLeadPhaseTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(View_AccountLeadPhaseTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for account_status_idFilter1.
        protected virtual void Populateaccount_status_idFilter1(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(AccountStatusTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.account_status_idFilter1.Items.Clear();
            foreach (AccountStatusRecord itemValue in AccountStatusTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(AccountStatusTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.account_status_idFilter1.Items.IndexOf(item) < 0) {
                    this.account_status_idFilter1.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.account_status_idFilter1, selectedValue);

            // Add the All item.
            this.account_status_idFilter1.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for designer_idFilter1.
        protected virtual void Populatedesigner_idFilter1(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.designer_idFilter1.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.designer_idFilter1.Items.IndexOf(item) < 0) {
                    this.designer_idFilter1.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.designer_idFilter1, selectedValue);

            // Add the All item.
            this.designer_idFilter1.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter account_status_idFilter1.
        public virtual WhereClause CreateWhereClause_account_status_idFilter1()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.designer_idFilter1)) {
                wc.iAND(View_AccountLeadPhaseView.designer_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.designer_idFilter1, this.GetFromSession(this.designer_idFilter1)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.View_AccountLeadPhaseSearchArea1)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(View_AccountLeadPhaseView.address, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.suburb, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.FirstName, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.LastName, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter designer_idFilter1.
        public virtual WhereClause CreateWhereClause_designer_idFilter1()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.account_status_idFilter1)) {
                wc.iAND(View_AccountLeadPhaseView.account_status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.account_status_idFilter1, this.GetFromSession(this.account_status_idFilter1)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.View_AccountLeadPhaseSearchArea1)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(View_AccountLeadPhaseView.address, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.suburb, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.FirstName, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                search.iOR(View_AccountLeadPhaseView.LastName, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountLeadPhaseSearchArea1, this.GetFromSession(this.View_AccountLeadPhaseSearchArea1)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter View_AccountLeadPhaseSearchArea1.
        public virtual WhereClause CreateWhereClause_View_AccountLeadPhaseSearchArea1()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.account_status_idFilter1)) {
                wc.iAND(View_AccountLeadPhaseView.account_status_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.account_status_idFilter1, this.GetFromSession(this.account_status_idFilter1)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.designer_idFilter1)) {
                wc.iAND(View_AccountLeadPhaseView.designer_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.designer_idFilter1, this.GetFromSession(this.designer_idFilter1)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.account_status_idFilter1, this.account_status_idFilter1.SelectedValue);
            this.SaveToSession(this.designer_idFilter1, this.designer_idFilter1.SelectedValue);
            this.SaveToSession(this.View_AccountLeadPhaseSearchArea1, this.View_AccountLeadPhaseSearchArea1.Text);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.account_status_idFilter1);
            this.RemoveFromSession(this.designer_idFilter1);
            this.RemoveFromSession(this.View_AccountLeadPhaseSearchArea1);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["View_AccountLeadPhaseTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["View_AccountLeadPhaseTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void View_AccountLeadPhasePagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_AccountLeadPhasePagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_AccountLeadPhasePagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void View_AccountLeadPhasePagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_AccountLeadPhasePagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void dateCreatedHeading_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountLeadPhaseView.date_created);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountLeadPhaseView.date_created, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void FirstNameLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountLeadPhaseView.FirstName);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountLeadPhaseView.FirstName, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void Label_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountLeadPhaseView.sale_date);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountLeadPhaseView.sale_date, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void LastNameLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountLeadPhaseView.LastName);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountLeadPhaseView.LastName, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void weeks_openLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountLeadPhaseView.weeks_open);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountLeadPhaseView.weeks_open, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void Button_Click(object sender, EventArgs args)
        {
            
            string url = @"LeadReviews.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void View_AccountLeadPhaseDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void View_AccountLeadPhaseNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"AddAccountPage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void View_AccountLeadPhaseSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void account_status_idFilter1_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void designer_idFilter1_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_AccountLeadPhaseRecord[] _DataSource = null;
        public  View_AccountLeadPhaseRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.DropDownList account_status_idFilter1 {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_status_idFilter1");
            }
        }
        
        public System.Web.UI.WebControls.Literal account_status_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_status_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal account_status_idLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "account_status_idLabel2");
            }
        }
        
        public WKCRM.UI.IThemeButton Button {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Button");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton dateCreatedHeading {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "dateCreatedHeading");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList designer_idFilter1 {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idFilter1");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel2 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel2");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel3 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel3");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton FirstNameLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "FirstNameLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton Label {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton LastNameLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "LastNameLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal next_review_dateLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "next_review_dateLabel1");
            }
        }
        
        public WKCRM.UI.IThemeButton View_AccountLeadPhaseDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhaseDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton View_AccountLeadPhaseNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhaseNewButton");
            }
        }
        
        public WKCRM.UI.IPagination View_AccountLeadPhasePagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhasePagination");
            }
        }
        
        public System.Web.UI.WebControls.TextBox View_AccountLeadPhaseSearchArea1 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhaseSearchArea1");
            }
        }
        
        public WKCRM.UI.IThemeButton View_AccountLeadPhaseSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhaseSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal View_AccountLeadPhaseTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhaseTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label View_AccountLeadPhaseTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountLeadPhaseTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton weeks_openLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "weeks_openLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                View_AccountLeadPhaseTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_AccountLeadPhaseRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (View_AccountLeadPhaseTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_AccountLeadPhaseRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public View_AccountLeadPhaseTableControlRow GetSelectedRecordControl()
        {
        View_AccountLeadPhaseTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public View_AccountLeadPhaseTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (View_AccountLeadPhaseTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.View_AccountLeadPhaseRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (View_AccountLeadPhaseTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowAccountTablePage.View_AccountLeadPhaseTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            View_AccountLeadPhaseTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (View_AccountLeadPhaseTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.View_AccountLeadPhaseRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public View_AccountLeadPhaseTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("View_AccountLeadPhaseTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                View_AccountLeadPhaseTableControlRow recControl = (View_AccountLeadPhaseTableControlRow)repItem.FindControl("View_AccountLeadPhaseTableControlRow");
                recList.Add(recControl);
            }

            return (View_AccountLeadPhaseTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowAccountTablePage.View_AccountLeadPhaseTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  