﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// LeadReviews.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.LeadReviews
{
  

#region "Section 1: Place your customizations here."

    
public class View_AccountReviewThisWeekTableControlRow : BaseView_AccountReviewThisWeekTableControlRow
{
      
        // The BaseView_AccountReviewThisWeekTableControlRow implements code for a ROW within the
        // the View_AccountReviewThisWeekTableControl table.  The BaseView_AccountReviewThisWeekTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of View_AccountReviewThisWeekTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            if (this.DataSource.DaysTillReview < 0)
            {
                this.DaysTillReview.Text = "<b>Review Overdue</b>";
            }
        }
    }

}

  

public class View_AccountReviewThisWeekTableControl : BaseView_AccountReviewThisWeekTableControl
{
        // The BaseView_AccountReviewThisWeekTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The View_AccountReviewThisWeekTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the View_AccountReviewThisWeekTableControlRow control on the LeadReviews page.
// Do not modify this class. Instead override any method in View_AccountReviewThisWeekTableControlRow.
public class BaseView_AccountReviewThisWeekTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseView_AccountReviewThisWeekTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.View_AccountReviewThisWeekRecordRowEditButton.Click += new ImageClickEventHandler(View_AccountReviewThisWeekRecordRowEditButton_Click);
        }

        // To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_AccountReviewThisWeekView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseView_AccountReviewThisWeekTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_AccountReviewThisWeekRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.DaysTillReviewSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountReviewThisWeekView.DaysTillReview);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.DaysTillReview.Text = formattedValue;
            } else {  
                this.DaysTillReview.Text = View_AccountReviewThisWeekView.DaysTillReview.Format(View_AccountReviewThisWeekView.DaysTillReview.DefaultValue);
            }
                    
            if (this.DaysTillReview.Text == null ||
                this.DaysTillReview.Text.Trim().Length == 0) {
                this.DaysTillReview.Text = "&nbsp;";
            }
                  
            if (this.DataSource.designer_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountReviewThisWeekView.designer_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.designer_id.Text = formattedValue;
            } else {  
                this.designer_id.Text = View_AccountReviewThisWeekView.designer_id.Format(View_AccountReviewThisWeekView.designer_id.DefaultValue);
            }
                    
            if (this.designer_id.Text == null ||
                this.designer_id.Text.Trim().Length == 0) {
                this.designer_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.NameSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountReviewThisWeekView.Name);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.View_AccountReviewThisWeekView, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"Name\\\", \\\"Name\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.Name.Text = formattedValue;
            } else {  
                this.Name.Text = View_AccountReviewThisWeekView.Name.Format(View_AccountReviewThisWeekView.Name.DefaultValue);
            }
                    
            if (this.Name.Text == null ||
                this.Name.Text.Trim().Length == 0) {
                this.Name.Text = "&nbsp;";
            }
                  
            if (this.DataSource.next_review_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountReviewThisWeekView.next_review_date);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.next_review_date.Text = formattedValue;
            } else {  
                this.next_review_date.Text = View_AccountReviewThisWeekView.next_review_date.Format(View_AccountReviewThisWeekView.next_review_date.DefaultValue);
            }
                    
            if (this.next_review_date.Text == null ||
                this.next_review_date.Text.Trim().Length == 0) {
                this.next_review_date.Text = "&nbsp;";
            }
                  
            if (this.DataSource.weeks_openSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_AccountReviewThisWeekView.weeks_open);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.weeks_open.Text = formattedValue;
            } else {  
                this.weeks_open.Text = View_AccountReviewThisWeekView.weeks_open.Format(View_AccountReviewThisWeekView.weeks_open.DefaultValue);
            }
                    
            if (this.weeks_open.Text == null ||
                this.weeks_open.Text.Trim().Length == 0) {
                this.weeks_open.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in View_AccountReviewThisWeekTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in View_AccountReviewThisWeekTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((View_AccountReviewThisWeekTableControl)MiscUtils.GetParentControlObject(this, "View_AccountReviewThisWeekTableControl")).DataChanged = true;
                ((View_AccountReviewThisWeekTableControl)MiscUtils.GetParentControlObject(this, "View_AccountReviewThisWeekTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in View_AccountReviewThisWeekTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_AccountReviewThisWeekView.DeleteRecord(pk);

          
            ((View_AccountReviewThisWeekTableControl)MiscUtils.GetParentControlObject(this, "View_AccountReviewThisWeekTableControl")).DataChanged = true;
            ((View_AccountReviewThisWeekTableControl)MiscUtils.GetParentControlObject(this, "View_AccountReviewThisWeekTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void View_AccountReviewThisWeekRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"EditAccountPage.aspx?account={View_AccountReviewThisWeekTableControlRow:FV:id}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseView_AccountReviewThisWeekTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseView_AccountReviewThisWeekTableControlRow_Rec"] = value;
            }
        }
        
        private View_AccountReviewThisWeekRecord _DataSource;
        public View_AccountReviewThisWeekRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal DaysTillReview {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "DaysTillReview");
            }
        }
           
        public System.Web.UI.WebControls.Literal designer_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal Name {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Name");
            }
        }
           
        public System.Web.UI.WebControls.Literal next_review_date {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "next_review_date");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton View_AccountReviewThisWeekRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountReviewThisWeekRecordRowEditButton");
            }
        }
           
        public System.Web.UI.WebControls.Literal weeks_open {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "weeks_open");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_AccountReviewThisWeekRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_AccountReviewThisWeekRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_AccountReviewThisWeekView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the View_AccountReviewThisWeekTableControl control on the LeadReviews page.
// Do not modify this class. Instead override any method in View_AccountReviewThisWeekTableControl.
public class BaseView_AccountReviewThisWeekTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseView_AccountReviewThisWeekTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.View_AccountReviewThisWeekPagination.FirstPage.Click += new ImageClickEventHandler(View_AccountReviewThisWeekPagination_FirstPage_Click);
            this.View_AccountReviewThisWeekPagination.LastPage.Click += new ImageClickEventHandler(View_AccountReviewThisWeekPagination_LastPage_Click);
            this.View_AccountReviewThisWeekPagination.NextPage.Click += new ImageClickEventHandler(View_AccountReviewThisWeekPagination_NextPage_Click);
            this.View_AccountReviewThisWeekPagination.PageSizeButton.Button.Click += new EventHandler(View_AccountReviewThisWeekPagination_PageSizeButton_Click);
            this.View_AccountReviewThisWeekPagination.PreviousPage.Click += new ImageClickEventHandler(View_AccountReviewThisWeekPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.NameLabel.Click += new EventHandler(NameLabel_Click);
            this.next_review_dateLabel.Click += new EventHandler(next_review_dateLabel_Click);
            this.weeks_openLabel.Click += new EventHandler(weeks_openLabel_Click);

            // Setup the button events.
        
            this.View_AccountReviewThisWeekSearchButton.Button.Click += new EventHandler(View_AccountReviewThisWeekSearchButton_Click);

            // Setup the filter and search events.
        
            this.designer_idFilter.SelectedIndexChanged += new EventHandler(designer_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.designer_idFilter)) {
                this.designer_idFilter.Items.Add(new ListItem(this.GetFromSession(this.designer_idFilter), this.GetFromSession(this.designer_idFilter)));
                this.designer_idFilter.SelectedValue = this.GetFromSession(this.designer_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.View_AccountReviewThisWeekSearchArea)) {
                
                this.View_AccountReviewThisWeekSearchArea.Text = this.GetFromSession(this.View_AccountReviewThisWeekSearchArea);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_AccountReviewThisWeekView.next_review_date, OrderByItem.OrderDir.Asc);
        
                this.CurrentSortOrder.Add(View_AccountReviewThisWeekView.Name, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_AccountReviewThisWeekRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_AccountReviewThisWeekRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_AccountReviewThisWeekView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_AccountReviewThisWeekRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_AccountReviewThisWeekRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (View_AccountReviewThisWeekTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_AccountReviewThisWeekRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_AccountReviewThisWeekRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_AccountReviewThisWeekView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populatedesigner_idFilter(MiscUtils.GetSelectedValue(this.designer_idFilter, this.GetFromSession(this.designer_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_AccountReviewThisWeekTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                View_AccountReviewThisWeekTableControlRow recControl = (View_AccountReviewThisWeekTableControlRow)(repItem.FindControl("View_AccountReviewThisWeekTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_AccountReviewThisWeekView.designer_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for View_AccountReviewThisWeekTableControl pagination.
        
            this.View_AccountReviewThisWeekPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.View_AccountReviewThisWeekPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.View_AccountReviewThisWeekPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.View_AccountReviewThisWeekPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.View_AccountReviewThisWeekPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.View_AccountReviewThisWeekPagination.CurrentPage.Text = "0";
            }
            this.View_AccountReviewThisWeekPagination.PageSize.Text = this.PageSize.ToString();
            this.View_AccountReviewThisWeekTotalItems.Text = this.TotalRecords.ToString();
            this.View_AccountReviewThisWeekPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.View_AccountReviewThisWeekPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (View_AccountReviewThisWeekTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_AccountReviewThisWeekView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.designer_idFilter)) {
                wc.iAND(View_AccountReviewThisWeekView.designer_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.designer_idFilter, this.GetFromSession(this.designer_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.View_AccountReviewThisWeekSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(View_AccountReviewThisWeekView.Name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountReviewThisWeekSearchArea, this.GetFromSession(this.View_AccountReviewThisWeekSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.View_AccountReviewThisWeekPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.View_AccountReviewThisWeekPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_AccountReviewThisWeekTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    View_AccountReviewThisWeekTableControlRow recControl = (View_AccountReviewThisWeekTableControlRow)(repItem.FindControl("View_AccountReviewThisWeekTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_AccountReviewThisWeekRecord rec = new View_AccountReviewThisWeekRecord();
        
                        if (recControl.DaysTillReview.Text != "") {
                            rec.Parse(recControl.DaysTillReview.Text, View_AccountReviewThisWeekView.DaysTillReview);
                        }
                        if (recControl.designer_id.Text != "") {
                            rec.Parse(recControl.designer_id.Text, View_AccountReviewThisWeekView.designer_id);
                        }
                        if (recControl.Name.Text != "") {
                            rec.Parse(recControl.Name.Text, View_AccountReviewThisWeekView.Name);
                        }
                        if (recControl.next_review_date.Text != "") {
                            rec.Parse(recControl.next_review_date.Text, View_AccountReviewThisWeekView.next_review_date);
                        }
                        if (recControl.weeks_open.Text != "") {
                            rec.Parse(recControl.weeks_open.Text, View_AccountReviewThisWeekView.weeks_open);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_AccountReviewThisWeekRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_AccountReviewThisWeekRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_AccountReviewThisWeekRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(View_AccountReviewThisWeekTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(View_AccountReviewThisWeekTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for designer_idFilter.
        protected virtual void Populatedesigner_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.designer_idFilter.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.designer_idFilter.Items.IndexOf(item) < 0) {
                    this.designer_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.designer_idFilter, selectedValue);

            // Add the All item.
            this.designer_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter designer_idFilter.
        public virtual WhereClause CreateWhereClause_designer_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.View_AccountReviewThisWeekSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(View_AccountReviewThisWeekView.Name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.View_AccountReviewThisWeekSearchArea, this.GetFromSession(this.View_AccountReviewThisWeekSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter View_AccountReviewThisWeekSearchArea.
        public virtual WhereClause CreateWhereClause_View_AccountReviewThisWeekSearchArea()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.designer_idFilter)) {
                wc.iAND(View_AccountReviewThisWeekView.designer_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.designer_idFilter, this.GetFromSession(this.designer_idFilter)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.designer_idFilter, this.designer_idFilter.SelectedValue);
            this.SaveToSession(this.View_AccountReviewThisWeekSearchArea, this.View_AccountReviewThisWeekSearchArea.Text);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.designer_idFilter);
            this.RemoveFromSession(this.View_AccountReviewThisWeekSearchArea);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["View_AccountReviewThisWeekTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["View_AccountReviewThisWeekTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void View_AccountReviewThisWeekPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_AccountReviewThisWeekPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_AccountReviewThisWeekPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void View_AccountReviewThisWeekPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void View_AccountReviewThisWeekPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void NameLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountReviewThisWeekView.Name);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountReviewThisWeekView.Name, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void next_review_dateLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountReviewThisWeekView.next_review_date);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountReviewThisWeekView.next_review_date, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void weeks_openLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(View_AccountReviewThisWeekView.weeks_open);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(View_AccountReviewThisWeekView.weeks_open, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void View_AccountReviewThisWeekSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void designer_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_AccountReviewThisWeekRecord[] _DataSource = null;
        public  View_AccountReviewThisWeekRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal DaysTillReviewLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "DaysTillReviewLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList designer_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton NameLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "NameLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton next_review_dateLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "next_review_dateLabel");
            }
        }
        
        public WKCRM.UI.IPagination View_AccountReviewThisWeekPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountReviewThisWeekPagination");
            }
        }
        
        public System.Web.UI.WebControls.TextBox View_AccountReviewThisWeekSearchArea {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountReviewThisWeekSearchArea");
            }
        }
        
        public WKCRM.UI.IThemeButton View_AccountReviewThisWeekSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountReviewThisWeekSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal View_AccountReviewThisWeekTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountReviewThisWeekTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label View_AccountReviewThisWeekTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_AccountReviewThisWeekTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton weeks_openLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "weeks_openLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                View_AccountReviewThisWeekTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_AccountReviewThisWeekRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public View_AccountReviewThisWeekTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public View_AccountReviewThisWeekTableControlRow[] GetSelectedRecordControls()
        {
        
            return (View_AccountReviewThisWeekTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.LeadReviews.View_AccountReviewThisWeekTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            View_AccountReviewThisWeekTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (View_AccountReviewThisWeekTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public View_AccountReviewThisWeekTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("View_AccountReviewThisWeekTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                View_AccountReviewThisWeekTableControlRow recControl = (View_AccountReviewThisWeekTableControlRow)repItem.FindControl("View_AccountReviewThisWeekTableControlRow");
                recList.Add(recControl);
            }

            return (View_AccountReviewThisWeekTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.LeadReviews.View_AccountReviewThisWeekTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  