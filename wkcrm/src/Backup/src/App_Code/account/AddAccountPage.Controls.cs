﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// AddAccountPage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
using Telerik.Web.UI;
        

#endregion

  
namespace WKCRM.UI.Controls.AddAccountPage
{
  

#region "Section 1: Place your customizations here."

    
public class accountRecordControl : BaseaccountRecordControl
{
      
        // The BaseaccountRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

    public accountRecordControl()
    {
        this.Load += new EventHandler(accountRecordControl_Load);
    }

    void accountRecordControl_Load(object sender, EventArgs e)
    {
        this.designer_id.AutoPostBack = true;
        this.designer_id.SelectedIndexChanged += new EventHandler(designer_id_SelectedIndexChanged);
        
    }

   


    void designer_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        Globals.SetDesignerScheduleLabel(showScheduleLabel, designer_id);
    }

    public override WhereClause CreateWhereClause_designer_idDropDownList()
    {
        return Globals.BuildDesignerWC();
    }

    public override WhereClause CreateWhereClause_contact_source_idDropDownList()
    {
        return Globals.BuildContactSourceWC();
    }

    public override WhereClause CreateWhereClause_opportunity_source_idDropDownList()
    {
        return Globals.BuildOpSourceWC();
    }

    public override void GetUIData()
    {
        base.GetUIData();
        this.DataSource.Parse(Globals.ACCOUNT_OPEN, AccountTable.account_status_id);
        this.DataSource.Parse(Globals.ACCOUNTPHASE_LEAD, AccountTable.account_type_id);
        RadComboBox suburbCombo = this.Page.FindControlRecursively("SuburbCombo") as RadComboBox;
        if(suburbCombo != null)
        {
            this.DataSource.Parse(suburbCombo.Text, AccountTable.suburb);
        }
        this.DataSource.Parse(this.Page.SystemUtils.GetUserID(), AccountTable.created_by_id);
    }

    public override void Validate()
    {
        base.Validate();
        if (String.IsNullOrEmpty(home_phone.Text) && 
            String.IsNullOrEmpty(business_phone.Text) &&
            String.IsNullOrEmpty(Business2.Text) &&
            String.IsNullOrEmpty(mobile.Text) &&
            String.IsNullOrEmpty(Mobile2.Text) &&
            String.IsNullOrEmpty(BuildersPhone.Text))
        {
            throw new Exception("Must supply at least one contact number");
        }
        RadComboBox suburbCombo = this.Page.FindControlRecursively("SuburbCombo") as RadComboBox;
        if (suburbCombo != null && String.IsNullOrEmpty(suburbCombo.Text))
        {
            throw new Exception("Please select a suburb");
        }
        
        if(this.opportunity_source_id.SelectedItem.Text == "Referral" &&
            String.IsNullOrEmpty(OpportuntiyDetails.Text))
        {
            throw new Exception("Please enter opportunity details for referral");
        }
        

    }

    public override void SaveData()
    {
        base.SaveData();
        Globals.GenerateAccountNotifications(this.DataSource);
        
    }
}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the accountRecordControl control on the AddAccountPage page.
// Do not modify this class. Instead override any method in accountRecordControl.
public class BaseaccountRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseaccountRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in accountRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in accountRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in accountRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = AccountTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new AccountRecord();
                return;
            }

            // Retrieve the record from the database.
            AccountRecord[] recList = AccountTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = AccountTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in accountRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.address);
                this.address.Text = formattedValue;
            } else {  
                this.address.Text = AccountTable.address.Format(AccountTable.address.DefaultValue);
            }
                    
            if (this.DataSource.benchtop_typeSpecified) {
                this.Populatebenchtop_typeDropDownList(this.DataSource.benchtop_type.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatebenchtop_typeDropDownList(AccountTable.benchtop_type.DefaultValue, 100);
                } else {
                this.Populatebenchtop_typeDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.builders_nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.builders_name);
                this.BuildersName.Text = formattedValue;
            } else {  
                this.BuildersName.Text = AccountTable.builders_name.Format(AccountTable.builders_name.DefaultValue);
            }
                    
            if (this.DataSource.builders_phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.builders_phone);
                this.BuildersPhone.Text = formattedValue;
            } else {  
                this.BuildersPhone.Text = AccountTable.builders_phone.Format(AccountTable.builders_phone.DefaultValue);
            }
                    
            if (this.DataSource.business_phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.business_phone);
                this.business_phone.Text = formattedValue;
            } else {  
                this.business_phone.Text = AccountTable.business_phone.Format(AccountTable.business_phone.DefaultValue);
            }
                    
            if (this.DataSource.business_phone2Specified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.business_phone2);
                this.Business2.Text = formattedValue;
            } else {  
                this.Business2.Text = AccountTable.business_phone2.Format(AccountTable.business_phone2.DefaultValue);
            }
                    
            if (this.DataSource.contact_source_idSpecified) {
                this.Populatecontact_source_idDropDownList(this.DataSource.contact_source_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatecontact_source_idDropDownList(AccountTable.contact_source_id.DefaultValue, 100);
                } else {
                this.Populatecontact_source_idDropDownList(null, 100);
                }
            }
                
            this.date_created.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.date_created.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            this.date_created.Text = AccountTable.date_created.Format(DateTime.Now.ToShortDateString());
            if (this.DataSource.designer_idSpecified) {
                this.Populatedesigner_idDropDownList(this.DataSource.designer_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatedesigner_idDropDownList(AccountTable.designer_id.DefaultValue, 100);
                } else {
                this.Populatedesigner_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.door_typeSpecified) {
                this.Populatedoor_typeDropDownList(this.DataSource.door_type.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatedoor_typeDropDownList(AccountTable.door_type.DefaultValue, 100);
                } else {
                this.Populatedoor_typeDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.emailSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.email);
                this.email.Text = formattedValue;
            } else {  
                this.email.Text = AccountTable.email.Format(AccountTable.email.DefaultValue);
            }
                    
            if (this.DataSource.email2Specified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.email2);
                this.email2.Text = formattedValue;
            } else {  
                this.email2.Text = AccountTable.email2.Format(AccountTable.email2.DefaultValue);
            }
                    
            if (this.DataSource.faxSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.fax);
                this.fax.Text = formattedValue;
            } else {  
                this.fax.Text = AccountTable.fax.Format(AccountTable.fax.DefaultValue);
            }
                    
            if (this.DataSource.home_phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.home_phone);
                this.home_phone.Text = formattedValue;
            } else {  
                this.home_phone.Text = AccountTable.home_phone.Format(AccountTable.home_phone.DefaultValue);
            }
                    
            if (this.DataSource.LastNameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.LastName);
                this.LastName.Text = formattedValue;
            } else {  
                this.LastName.Text = AccountTable.LastName.Format(AccountTable.LastName.DefaultValue);
            }
                    
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.mobile);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = AccountTable.mobile.Format(AccountTable.mobile.DefaultValue);
            }
                    
            if (this.DataSource.mobile2Specified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.mobile2);
                this.Mobile2.Text = formattedValue;
            } else {  
                this.Mobile2.Text = AccountTable.mobile2.Format(AccountTable.mobile2.DefaultValue);
            }
                    
            if (this.DataSource.FirstNameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.FirstName);
                this.Name.Text = formattedValue;
            } else {  
                this.Name.Text = AccountTable.FirstName.Format(AccountTable.FirstName.DefaultValue);
            }
                    
            if (this.DataSource.opportunity_source_idSpecified) {
                this.Populateopportunity_source_idDropDownList(this.DataSource.opportunity_source_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateopportunity_source_idDropDownList(AccountTable.opportunity_source_id.DefaultValue, 100);
                } else {
                this.Populateopportunity_source_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.opportunity_detailsSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.opportunity_details);
                this.OpportuntiyDetails.Text = formattedValue;
            } else {  
                this.OpportuntiyDetails.Text = AccountTable.opportunity_details.Format(AccountTable.opportunity_details.DefaultValue);
            }
                    
            if (this.DataSource.postal_addressSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.postal_address);
                this.PostalAddress.Text = formattedValue;
            } else {  
                this.PostalAddress.Text = AccountTable.postal_address.Format(AccountTable.postal_address.DefaultValue);
            }
                    
            if (this.DataSource.postcodeSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.postcode);
                this.postcode.Text = formattedValue;
            } else {  
                this.postcode.Text = AccountTable.postcode.Format(AccountTable.postcode.DefaultValue);
            }
                    
            if (this.DataSource.price_rangeSpecified) {
                this.Populateprice_rangeDropDownList(this.DataSource.price_range, 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateprice_rangeDropDownList(AccountTable.price_range.DefaultValue, 100);
                } else {
                this.Populateprice_rangeDropDownList(null, 100);
                }
            }
                
            this.sale_date.Attributes.Add("onfocus", "toggleEnableDisableDateFormatter(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
            this.sale_date.Attributes.Add("onblur", "presubmitDateValidation(this, '" + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("'", "").ToLower() + "');");
                    
            if (this.DataSource.sale_dateSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.sale_date);
                this.sale_date.Text = formattedValue;
            } else {  
                this.sale_date.Text = AccountTable.sale_date.Format(AccountTable.sale_date.DefaultValue);
            }
                    
            if (this.DataSource.time_frameSpecified) {
                      
                string formattedValue = this.DataSource.Format(AccountTable.time_frame);
                this.time_frame.Text = formattedValue;
            } else {  
                this.time_frame.Text = AccountTable.time_frame.Format(AccountTable.time_frame.DefaultValue);
            }
                    
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in accountRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in accountRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in accountRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in accountRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.address.Text, AccountTable.address);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.benchtop_type), AccountTable.benchtop_type);
                  
            this.DataSource.Parse(this.BuildersName.Text, AccountTable.builders_name);
                          
            this.DataSource.Parse(this.BuildersPhone.Text, AccountTable.builders_phone);
                          
            this.DataSource.Parse(this.business_phone.Text, AccountTable.business_phone);
                          
            this.DataSource.Parse(this.Business2.Text, AccountTable.business_phone2);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.contact_source_id), AccountTable.contact_source_id);
                  
            this.DataSource.Parse(this.date_created.Text, AccountTable.date_created);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.designer_id), AccountTable.designer_id);
                  
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.door_type), AccountTable.door_type);
                  
            this.DataSource.Parse(this.email.Text, AccountTable.email);
                          
            this.DataSource.Parse(this.email2.Text, AccountTable.email2);
                          
            this.DataSource.Parse(this.fax.Text, AccountTable.fax);
                          
            this.DataSource.Parse(this.home_phone.Text, AccountTable.home_phone);
                          
            this.DataSource.Parse(this.LastName.Text, AccountTable.LastName);
                          
            this.DataSource.Parse(this.mobile.Text, AccountTable.mobile);
                          
            this.DataSource.Parse(this.Mobile2.Text, AccountTable.mobile2);
                          
            this.DataSource.Parse(this.Name.Text, AccountTable.FirstName);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.opportunity_source_id), AccountTable.opportunity_source_id);
                  
            this.DataSource.Parse(this.OpportuntiyDetails.Text, AccountTable.opportunity_details);
                          
            this.DataSource.Parse(this.PostalAddress.Text, AccountTable.postal_address);
                          
            this.DataSource.Parse(this.postcode.Text, AccountTable.postcode);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.price_range), AccountTable.price_range);
                  
            this.DataSource.Parse(this.sale_date.Text, AccountTable.sale_date);
                          
            this.DataSource.Parse(this.time_frame.Text, AccountTable.time_frame);
                          
        }

        //  To customize, override this method in accountRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in accountRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            AccountTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_benchtop_typeDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_contact_source_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_designer_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_door_typeDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_opportunity_source_idDropDownList() {
            return new WhereClause();
        }
                
        public virtual WhereClause CreateWhereClause_price_rangeDropDownList() {
            return new WhereClause();
        }
                
        // Fill the benchtop_type list.
        protected virtual void Populatebenchtop_typeDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_benchtop_typeDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(AccountBenchtopTypeTable.AccountBenchtopType, OrderByItem.OrderDir.Asc);

                      this.benchtop_type.Items.Clear();
            foreach (AccountBenchtopTypeRecord itemValue in AccountBenchtopTypeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.AccountBenchtopTypeIdSpecified) {
                    cvalue = itemValue.AccountBenchtopTypeId.ToString();
                    fvalue = itemValue.Format(AccountBenchtopTypeTable.AccountBenchtopType);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.benchtop_type.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.benchtop_type, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.benchtop_type, AccountTable.benchtop_type.Format(selectedValue))) {
                string fvalue = AccountTable.benchtop_type.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.benchtop_type.Items.Insert(0, item);
            }

                  
            this.benchtop_type.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the contact_source_id list.
        protected virtual void Populatecontact_source_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_contact_source_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(ContactSourceTable.name, OrderByItem.OrderDir.Asc);

                      this.contact_source_id.Items.Clear();
            foreach (ContactSourceRecord itemValue in ContactSourceTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(ContactSourceTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.contact_source_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.contact_source_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.contact_source_id, AccountTable.contact_source_id.Format(selectedValue))) {
                string fvalue = AccountTable.contact_source_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.contact_source_id.Items.Insert(0, item);
            }

                  
            this.contact_source_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the designer_id list.
        protected virtual void Populatedesigner_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_designer_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

                      this.designer_id.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.designer_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.designer_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.designer_id, AccountTable.designer_id.Format(selectedValue))) {
                string fvalue = AccountTable.designer_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.designer_id.Items.Insert(0, item);
            }

                  
            this.designer_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the door_type list.
        protected virtual void Populatedoor_typeDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_door_typeDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(AccountDoorTypeTable.AccountDoorType, OrderByItem.OrderDir.Asc);

                      this.door_type.Items.Clear();
            foreach (AccountDoorTypeRecord itemValue in AccountDoorTypeTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.AccountDoorTypeIdSpecified) {
                    cvalue = itemValue.AccountDoorTypeId.ToString();
                    fvalue = itemValue.Format(AccountDoorTypeTable.AccountDoorType);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.door_type.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.door_type, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.door_type, AccountTable.door_type.Format(selectedValue))) {
                string fvalue = AccountTable.door_type.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.door_type.Items.Insert(0, item);
            }

                  
            this.door_type.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the opportunity_source_id list.
        protected virtual void Populateopportunity_source_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_opportunity_source_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(OpportunitySourceTable.name, OrderByItem.OrderDir.Asc);

                      this.opportunity_source_id.Items.Clear();
            foreach (OpportunitySourceRecord itemValue in OpportunitySourceTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(OpportunitySourceTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.opportunity_source_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.opportunity_source_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.opportunity_source_id, AccountTable.opportunity_source_id.Format(selectedValue))) {
                string fvalue = AccountTable.opportunity_source_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.opportunity_source_id.Items.Insert(0, item);
            }

                  
            this.opportunity_source_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        // Fill the price_range list.
        protected virtual void Populateprice_rangeDropDownList
                (string selectedValue, int maxItems) {
                  
            this.price_range.Items.Clear();
                      
            this.price_range.Items.Add(new ListItem("10-15k", "10-15k"));
            this.price_range.Items.Add(new ListItem("15-20K", "15-20K"));
            this.price_range.Items.Add(new ListItem("20-25K", "20-25K"));
            this.price_range.Items.Add(new ListItem("25-30K", "25-30K"));
            this.price_range.Items.Add(new ListItem("30-35K", "30-35K"));
            this.price_range.Items.Add(new ListItem("35-40K", "35-40K"));
            this.price_range.Items.Add(new ListItem("45-50K", "45-50K"));
            this.price_range.Items.Add(new ListItem("50+K", "50+K"));
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.price_range, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.price_range, AccountTable.price_range.Format(selectedValue))) {
                string fvalue = AccountTable.price_range.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.price_range.Items.Insert(0, item);
            }

                  
            this.price_range.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseaccountRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseaccountRecordControl_Rec"] = value;
            }
        }
        
        private AccountRecord _DataSource;
        public AccountRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Image accountDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "accountDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal accountDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "accountDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.TextBox address {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "address");
            }
        }
        
        public System.Web.UI.WebControls.Literal addressLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "addressLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList benchtop_type {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "benchtop_type");
            }
        }
        
        public System.Web.UI.WebControls.Literal benchtop_typeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "benchtop_typeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox BuildersName {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "BuildersName");
            }
        }
           
        public System.Web.UI.WebControls.TextBox BuildersPhone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "BuildersPhone");
            }
        }
           
        public System.Web.UI.WebControls.TextBox business_phone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "business_phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal business_phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "business_phoneLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox Business2 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Business2");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList contact_source_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contact_source_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal contact_source_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contact_source_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox date_created {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_created");
            }
        }
        
        public System.Web.UI.WebControls.Literal date_createdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "date_createdLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList designer_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal designer_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designer_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList door_type {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "door_type");
            }
        }
        
        public System.Web.UI.WebControls.Literal door_typeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "door_typeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox email {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email");
            }
        }
           
        public System.Web.UI.WebControls.TextBox email2 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email2");
            }
        }
        
        public System.Web.UI.WebControls.Literal emailLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "emailLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox fax {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "fax");
            }
        }
        
        public System.Web.UI.WebControls.Literal faxLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "faxLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox home_phone {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_phone");
            }
        }
        
        public System.Web.UI.WebControls.Literal home_phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "home_phoneLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.Label Label1 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label1");
            }
        }
        
        public System.Web.UI.WebControls.Label Label2 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label2");
            }
        }
        
        public System.Web.UI.WebControls.Label Label3 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label3");
            }
        }
        
        public System.Web.UI.WebControls.Label Label4 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label4");
            }
        }
        
        public System.Web.UI.WebControls.Label Label5 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label5");
            }
        }
        
        public System.Web.UI.WebControls.Label Label6 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label6");
            }
        }
        
        public System.Web.UI.WebControls.Label Label7 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label7");
            }
        }
        
        public System.Web.UI.WebControls.Label Label8 {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label8");
            }
        }
           
        public System.Web.UI.WebControls.TextBox LastName {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "LastName");
            }
        }
           
        public System.Web.UI.WebControls.TextBox mobile {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
           
        public System.Web.UI.WebControls.TextBox Mobile2 {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Mobile2");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox Name {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Name");
            }
        }
        
        public System.Web.UI.WebControls.Literal NameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "NameLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList opportunity_source_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunity_source_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_idLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox OpportuntiyDetails {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OpportuntiyDetails");
            }
        }
           
        public System.Web.UI.WebControls.TextBox PostalAddress {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "PostalAddress");
            }
        }
           
        public System.Web.UI.WebControls.TextBox postcode {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "postcode");
            }
        }
        
        public System.Web.UI.WebControls.Literal postcodeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "postcodeLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList price_range {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "price_range");
            }
        }
        
        public System.Web.UI.WebControls.Literal price_rangeLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "price_rangeLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox sale_date {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "sale_date");
            }
        }
        
        public System.Web.UI.WebControls.Label showScheduleLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "showScheduleLabel");
            }
        }
        
        public System.Web.UI.WebControls.Label SuburbLabel {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "SuburbLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox time_frame {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "time_frame");
            }
        }
        
        public System.Web.UI.WebControls.Literal time_frameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "time_frameLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            AccountRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public AccountRecord GetRecord()
        {
        
            if (this.DataSource != null) {
              return this.DataSource;
            }
            
            return new AccountRecord();
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  