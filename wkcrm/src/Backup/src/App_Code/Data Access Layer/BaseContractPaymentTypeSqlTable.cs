﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractPaymentTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractPaymentTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractPaymentTypeTable"></see> class.
/// </remarks>
/// <seealso cref="ContractPaymentTypeTable"></seealso>
/// <seealso cref="ContractPaymentTypeSqlTable"></seealso>
public class BaseContractPaymentTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractPaymentTypeSqlTable()
	{
	}

	public BaseContractPaymentTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
