﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradesmenSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="TradesmenSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TradesmenTable"></see> class.
/// </remarks>
/// <seealso cref="TradesmenTable"></seealso>
/// <seealso cref="TradesmenSqlTable"></seealso>
public class BaseTradesmenSqlTable : DynamicSQLServerAdapter
{
	
	public BaseTradesmenSqlTable()
	{
	}

	public BaseTradesmenSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
