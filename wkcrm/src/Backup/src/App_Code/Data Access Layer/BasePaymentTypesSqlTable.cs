﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentTypesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="PaymentTypesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentTypesTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentTypesTable"></seealso>
/// <seealso cref="PaymentTypesSqlTable"></seealso>
public class BasePaymentTypesSqlTable : DynamicSQLServerAdapter
{
	
	public BasePaymentTypesSqlTable()
	{
	}

	public BasePaymentTypesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
