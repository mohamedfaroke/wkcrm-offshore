﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EmployeeRolesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="EmployeeRolesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EmployeeRolesTable"></see> class.
/// </remarks>
/// <seealso cref="EmployeeRolesTable"></seealso>
/// <seealso cref="EmployeeRolesSqlTable"></seealso>
public class BaseEmployeeRolesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseEmployeeRolesSqlTable()
	{
	}

	public BaseEmployeeRolesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
