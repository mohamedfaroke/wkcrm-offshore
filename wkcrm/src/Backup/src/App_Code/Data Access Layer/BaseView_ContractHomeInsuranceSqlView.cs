﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractHomeInsuranceSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractHomeInsuranceSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractHomeInsuranceView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractHomeInsuranceView"></seealso>
/// <seealso cref="View_ContractHomeInsuranceSqlView"></seealso>
public class BaseView_ContractHomeInsuranceSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ContractHomeInsuranceSqlView()
	{
	}

	public BaseView_ContractHomeInsuranceSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
