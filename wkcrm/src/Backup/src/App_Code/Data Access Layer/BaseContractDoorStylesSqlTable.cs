﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorStylesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorStylesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorStylesTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorStylesTable"></seealso>
/// <seealso cref="ContractDoorStylesSqlTable"></seealso>
public class BaseContractDoorStylesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractDoorStylesSqlTable()
	{
	}

	public BaseContractDoorStylesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
