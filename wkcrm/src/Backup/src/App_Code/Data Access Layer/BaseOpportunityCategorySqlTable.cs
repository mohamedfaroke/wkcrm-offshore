﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in OpportunityCategorySqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="OpportunityCategorySqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="OpportunityCategoryTable"></see> class.
/// </remarks>
/// <seealso cref="OpportunityCategoryTable"></seealso>
/// <seealso cref="OpportunityCategorySqlTable"></seealso>
public class BaseOpportunityCategorySqlTable : DynamicSQLServerAdapter
{
	
	public BaseOpportunityCategorySqlTable()
	{
	}

	public BaseOpportunityCategorySqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
