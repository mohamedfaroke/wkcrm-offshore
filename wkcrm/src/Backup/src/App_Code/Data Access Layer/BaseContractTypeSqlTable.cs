﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractTypeTable"></see> class.
/// </remarks>
/// <seealso cref="ContractTypeTable"></seealso>
/// <seealso cref="ContractTypeSqlTable"></seealso>
public class BaseContractTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractTypeSqlTable()
	{
	}

	public BaseContractTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
