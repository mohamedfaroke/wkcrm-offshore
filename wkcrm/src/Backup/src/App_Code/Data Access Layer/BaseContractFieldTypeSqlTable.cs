﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractFieldTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractFieldTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractFieldTypeTable"></see> class.
/// </remarks>
/// <seealso cref="ContractFieldTypeTable"></seealso>
/// <seealso cref="ContractFieldTypeSqlTable"></seealso>
public class BaseContractFieldTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractFieldTypeSqlTable()
	{
	}

	public BaseContractFieldTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
