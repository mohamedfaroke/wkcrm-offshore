﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in QuestionOptionSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="QuestionOptionSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="QuestionOptionTable"></see> class.
/// </remarks>
/// <seealso cref="QuestionOptionTable"></seealso>
/// <seealso cref="QuestionOptionSqlTable"></seealso>
public class BaseQuestionOptionSqlTable : DynamicSQLServerAdapter
{
	
	public BaseQuestionOptionSqlTable()
	{
	}

	public BaseQuestionOptionSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
