﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradeEntityTypesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="TradeEntityTypesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TradeEntityTypesTable"></see> class.
/// </remarks>
/// <seealso cref="TradeEntityTypesTable"></seealso>
/// <seealso cref="TradeEntityTypesSqlTable"></seealso>
public class BaseTradeEntityTypesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseTradeEntityTypesSqlTable()
	{
	}

	public BaseTradeEntityTypesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
