﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EvaluationQuestionSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="EvaluationQuestionSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EvaluationQuestionTable"></see> class.
/// </remarks>
/// <seealso cref="EvaluationQuestionTable"></seealso>
/// <seealso cref="EvaluationQuestionSqlTable"></seealso>
public class BaseEvaluationQuestionSqlTable : DynamicSQLServerAdapter
{
	
	public BaseEvaluationQuestionSqlTable()
	{
	}

	public BaseEvaluationQuestionSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
