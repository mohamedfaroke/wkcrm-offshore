﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradesmanTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="TradesmanTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TradesmanTypeTable"></see> class.
/// </remarks>
/// <seealso cref="TradesmanTypeTable"></seealso>
/// <seealso cref="TradesmanTypeSqlTable"></seealso>
public class BaseTradesmanTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseTradesmanTypeSqlTable()
	{
	}

	public BaseTradesmanTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
