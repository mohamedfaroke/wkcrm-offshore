﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorColoursSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorColoursSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorColoursTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorColoursTable"></seealso>
/// <seealso cref="ContractDoorColoursSqlTable"></seealso>
public class BaseContractDoorColoursSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractDoorColoursSqlTable()
	{
	}

	public BaseContractDoorColoursSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
