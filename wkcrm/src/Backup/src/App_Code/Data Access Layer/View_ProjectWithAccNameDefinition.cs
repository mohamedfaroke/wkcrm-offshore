﻿using System;

namespace WKCRM.Business
{

/// <summary>
/// Contains embedded schema and configuration data that is used by the 
/// <see cref="View_ProjectWithAccNameView">WKCRM.View_ProjectWithAccNameView</see> class
/// to initialize the class's TableDefinition.
/// </summary>
/// <seealso cref="View_ProjectWithAccNameView"></seealso>
public class View_ProjectWithAccNameDefinition
{
#region "Definition (XML) for View_ProjectWithAccNameDefinition table"
	//Next 281 lines contain Table Definition (XML) for table "View_ProjectWithAccNameDefinition"
	private static string _DefinitionString = 
@"<XMLDefinition Generator=""Iron Speed Designer"" Version=""4.3"" Type=""VIEW"">" +
  @"<ColumnDefinition>" +
    @"<Column InternalName=""0"" Priority=""1"" ColumnNum=""0"">" +
      @"<columnName>id</columnName>" +
      @"<columnUIName>ID</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>Y</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<columnVirtualPK Source=""User"">Y</columnVirtualPK>" +
    "</Column>" +
    @"<Column InternalName=""1"" Priority=""2"" ColumnNum=""1"">" +
      @"<columnName>contract_id</columnName>" +
      @"<columnUIName>Contract</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ProjectWithAccName_contract_id_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.ContractMainTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay></columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
    @"<Column InternalName=""2"" Priority=""3"" ColumnNum=""2"">" +
      @"<columnName>AccId</columnName>" +
      @"<columnUIName Source=""User"">Acc</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault Source=""User""></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed Source=""User"">N</columnComputed>" +
      @"<columnIdentity Source=""User"">N</columnIdentity>" +
      @"<columnReadOnly Source=""User"">N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<columnCodeName></columnCodeName>" +
      @"<InternalName>2</InternalName>" +
      @"<columnTableClassName></columnTableClassName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ProjectWithAccName_AccId_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.AccountTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay>Name</columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
    @"<Column InternalName=""3"" Priority=""4"" ColumnNum=""3"">" +
      @"<columnName>date_created</columnName>" +
      @"<columnUIName>Date Created</columnUIName>" +
      @"<columnType>Date</columnType>" +
      @"<columnDBType>datetime</columnDBType>" +
      @"<columnLengthSet></columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<columnFormat>G</columnFormat>" +
    "</Column>" +
    @"<Column InternalName=""4"" Priority=""5"" ColumnNum=""4"">" +
      @"<columnName>created_by</columnName>" +
      @"<columnUIName>Created By</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ProjectWithAccName_created_by_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.EmployeeTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay>name</columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
    @"<Column InternalName=""5"" Priority=""6"" ColumnNum=""5"">" +
      @"<columnName>date_modified</columnName>" +
      @"<columnUIName>Date Modified</columnUIName>" +
      @"<columnType>Date</columnType>" +
      @"<columnDBType>datetime</columnDBType>" +
      @"<columnLengthSet></columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<columnFormat>G</columnFormat>" +
    "</Column>" +
    @"<Column InternalName=""6"" Priority=""7"" ColumnNum=""6"">" +
      @"<columnName>modified_by</columnName>" +
      @"<columnUIName>Modified By</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ProjectWithAccName_modified_by_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.EmployeeTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay>name</columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
    @"<Column InternalName=""7"" Priority=""8"" ColumnNum=""7"">" +
      @"<columnName>project_manager</columnName>" +
      @"<columnUIName>Project Manager</columnUIName>" +
      @"<columnType>Number</columnType>" +
      @"<columnDBType>numeric</columnDBType>" +
      @"<columnLengthSet>18.0</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
      @"<foreignKey>" +
        @"<columnFKName>FKVIRTUAL_View_ProjectWithAccName_project_manager_1</columnFKName>" +
        @"<columnFKTable>WKCRM.Business.EmployeeTable, App_Code</columnFKTable>" +
        @"<columnFKOwner>dbo</columnFKOwner>" +
        @"<columnFKColumn>id</columnFKColumn>" +
        @"<columnFKColumnDisplay>name</columnFKColumnDisplay>" +
        @"<foreignKeyType>Implicit</foreignKeyType>" +
      "</foreignKey>" +
    "</Column>" +
    @"<Column InternalName=""8"" Priority=""9"" ColumnNum=""8"">" +
      @"<columnName>Name</columnName>" +
      @"<columnUIName>Name</columnUIName>" +
      @"<columnType>String</columnType>" +
      @"<columnDBType>nvarchar</columnDBType>" +
      @"<columnLengthSet>100</columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
    "</Column>" +
    @"<Column InternalName=""9"" Priority=""10"" ColumnNum=""9"">" +
      @"<columnName>status_id</columnName>" +
      @"<columnUIName>Active</columnUIName>" +
      @"<columnType>Boolean</columnType>" +
      @"<columnDBType>bit</columnDBType>" +
      @"<columnLengthSet></columnLengthSet>" +
      @"<columnDefault></columnDefault>" +
      @"<columnDBDefault></columnDBDefault>" +
      @"<columnIndex>N</columnIndex>" +
      @"<columnUnique>N</columnUnique>" +
      @"<columnFunction></columnFunction>" +
      @"<columnDBFormat></columnDBFormat>" +
      @"<columnPK>N</columnPK>" +
      @"<columnPermanent>N</columnPermanent>" +
      @"<columnComputed>N</columnComputed>" +
      @"<columnIdentity>N</columnIdentity>" +
      @"<columnReadOnly>N</columnReadOnly>" +
      @"<columnRequired>Y</columnRequired>" +
      @"<columnNotNull>Y</columnNotNull>" +
      @"<columnVisibleWidth>%ISD_DEFAULT%</columnVisibleWidth>" +
      @"<columnTableAliasName></columnTableAliasName>" +
    "</Column>" +
  "</ColumnDefinition>" +
  @"<TableName>View_ProjectWithAccName</TableName>" +
  @"<Version>2</Version>" +
  @"<Owner>dbo</Owner>" +
  @"<TableCodeName>View_ProjectWithAccName</TableCodeName>" +
  @"<TableAliasName>View_ProjectWithAccName_</TableAliasName>" +
  @"<ConnectionName>Databasewk1</ConnectionName>" +
  @"<canCreateRecords Source=""User"">Y</canCreateRecords>" +
  @"<canEditRecords Source=""User"">Y</canEditRecords>" +
  @"<canDeleteRecords Source=""User"">Y</canDeleteRecords>" +
  @"<canViewRecords Source=""Database"">Y</canViewRecords>" +
  @"<AppShortName>WKCRM</AppShortName>" +
"</XMLDefinition>";
#endregion

	/// <summary>
	/// Gets the embedded schema and configuration data for the  
	/// <see cref="View_ProjectWithAccNameView"></see>
	/// class's TableDefinition.
	/// </summary>
	/// <remarks>This function is only called once at runtime.</remarks>
	/// <returns>An XML string.</returns>
	public static string GetXMLString()
	{
		return _DefinitionString;
	}
}

}
