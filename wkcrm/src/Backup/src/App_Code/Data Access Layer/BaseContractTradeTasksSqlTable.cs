﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractTradeTasksSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractTradeTasksSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractTradeTasksTable"></see> class.
/// </remarks>
/// <seealso cref="ContractTradeTasksTable"></seealso>
/// <seealso cref="ContractTradeTasksSqlTable"></seealso>
public class BaseContractTradeTasksSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractTradeTasksSqlTable()
	{
	}

	public BaseContractTradeTasksSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
