﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopsTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopsTable"></seealso>
/// <seealso cref="ContractBenchtopsSqlTable"></seealso>
public class BaseContractBenchtopsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractBenchtopsSqlTable()
	{
	}

	public BaseContractBenchtopsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
