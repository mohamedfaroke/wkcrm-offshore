﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in DeliveryFeesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="DeliveryFeesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="DeliveryFeesTable"></see> class.
/// </remarks>
/// <seealso cref="DeliveryFeesTable"></seealso>
/// <seealso cref="DeliveryFeesSqlTable"></seealso>
public class BaseDeliveryFeesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseDeliveryFeesSqlTable()
	{
	}

	public BaseDeliveryFeesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
