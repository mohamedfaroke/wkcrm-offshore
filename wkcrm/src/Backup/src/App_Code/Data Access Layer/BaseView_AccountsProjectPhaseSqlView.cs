﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_AccountsProjectPhaseSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_AccountsProjectPhaseSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_AccountsProjectPhaseView"></see> class.
/// </remarks>
/// <seealso cref="View_AccountsProjectPhaseView"></seealso>
/// <seealso cref="View_AccountsProjectPhaseSqlView"></seealso>
public class BaseView_AccountsProjectPhaseSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_AccountsProjectPhaseSqlView()
	{
	}

	public BaseView_AccountsProjectPhaseSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
