﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="AccountTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AccountTypeTable"></see> class.
/// </remarks>
/// <seealso cref="AccountTypeTable"></seealso>
/// <seealso cref="AccountTypeSqlTable"></seealso>
public class BaseAccountTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseAccountTypeSqlTable()
	{
	}

	public BaseAccountTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
