﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AttachmentSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="AttachmentSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AttachmentTable"></see> class.
/// </remarks>
/// <seealso cref="AttachmentTable"></seealso>
/// <seealso cref="AttachmentSqlTable"></seealso>
public class BaseAttachmentSqlTable : DynamicSQLServerAdapter
{
	
	public BaseAttachmentSqlTable()
	{
	}

	public BaseAttachmentSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
