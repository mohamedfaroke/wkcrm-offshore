﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractOthersSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractOthersSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractOthersTable"></see> class.
/// </remarks>
/// <seealso cref="ContractOthersTable"></seealso>
/// <seealso cref="ContractOthersSqlTable"></seealso>
public class BaseContractOthersSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractOthersSqlTable()
	{
	}

	public BaseContractOthersSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
