﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ProjectEvaluationsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ProjectEvaluationsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ProjectEvaluationsTable"></see> class.
/// </remarks>
/// <seealso cref="ProjectEvaluationsTable"></seealso>
/// <seealso cref="ProjectEvaluationsSqlTable"></seealso>
public class BaseProjectEvaluationsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseProjectEvaluationsSqlTable()
	{
	}

	public BaseProjectEvaluationsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
