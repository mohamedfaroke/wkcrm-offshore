﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ProjectWithAccNameSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ProjectWithAccNameSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ProjectWithAccNameView"></see> class.
/// </remarks>
/// <seealso cref="View_ProjectWithAccNameView"></seealso>
/// <seealso cref="View_ProjectWithAccNameSqlView"></seealso>
public class BaseView_ProjectWithAccNameSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ProjectWithAccNameSqlView()
	{
	}

	public BaseView_ProjectWithAccNameSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
