﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentScheduleSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="PaymentScheduleSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentScheduleTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentScheduleTable"></seealso>
/// <seealso cref="PaymentScheduleSqlTable"></seealso>
public class BasePaymentScheduleSqlTable : DynamicSQLServerAdapter
{
	
	public BasePaymentScheduleSqlTable()
	{
	}

	public BasePaymentScheduleSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
