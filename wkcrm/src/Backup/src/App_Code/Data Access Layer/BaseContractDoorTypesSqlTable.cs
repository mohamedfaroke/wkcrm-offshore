﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractDoorTypesSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractDoorTypesSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractDoorTypesTable"></see> class.
/// </remarks>
/// <seealso cref="ContractDoorTypesTable"></seealso>
/// <seealso cref="ContractDoorTypesSqlTable"></seealso>
public class BaseContractDoorTypesSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractDoorTypesSqlTable()
	{
	}

	public BaseContractDoorTypesSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
