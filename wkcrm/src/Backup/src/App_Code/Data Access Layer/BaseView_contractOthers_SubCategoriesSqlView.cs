﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_contractOthers_SubCategoriesSqlView.cs 

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_contractOthers_SubCategoriesSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_contractOthers_SubCategoriesView"></see> class.
/// </remarks>
/// <seealso cref="View_contractOthers_SubCategoriesView"></seealso>
/// <seealso cref="View_contractOthers_SubCategoriesSqlView"></seealso>
public class BaseView_contractOthers_SubCategoriesSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_contractOthers_SubCategoriesSqlView()
	{
	}

	public BaseView_contractOthers_SubCategoriesSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
