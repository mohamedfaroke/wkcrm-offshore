﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractItemTotalsSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractItemTotalsSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractItemTotalsView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractItemTotalsView"></seealso>
/// <seealso cref="View_ContractItemTotalsSqlView"></seealso>
public class BaseView_ContractItemTotalsSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_ContractItemTotalsSqlView()
	{
	}

	public BaseView_ContractItemTotalsSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
