﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EvaluationsSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="EvaluationsSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EvaluationsTable"></see> class.
/// </remarks>
/// <seealso cref="EvaluationsTable"></seealso>
/// <seealso cref="EvaluationsSqlTable"></seealso>
public class BaseEvaluationsSqlTable : DynamicSQLServerAdapter
{
	
	public BaseEvaluationsSqlTable()
	{
	}

	public BaseEvaluationsSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
