﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TasksSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="TasksSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TasksTable"></see> class.
/// </remarks>
/// <seealso cref="TasksTable"></seealso>
/// <seealso cref="TasksSqlTable"></seealso>
public class BaseTasksSqlTable : DynamicSQLServerAdapter
{
	
	public BaseTasksSqlTable()
	{
	}

	public BaseTasksSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
