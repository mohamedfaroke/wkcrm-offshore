﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_AccountLeadPhaseSqlView.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="View_AccountLeadPhaseSqlView"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_AccountLeadPhaseView"></see> class.
/// </remarks>
/// <seealso cref="View_AccountLeadPhaseView"></seealso>
/// <seealso cref="View_AccountLeadPhaseSqlView"></seealso>
public class BaseView_AccountLeadPhaseSqlView : DynamicSQLServerAdapter
{
	
	public BaseView_AccountLeadPhaseSqlView()
	{
	}

	public BaseView_AccountLeadPhaseSqlView(string connectionName) : base(connectionName)
	{
	}

}

}
