﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractBenchtopItemSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="ContractBenchtopItemSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractBenchtopItemTable"></see> class.
/// </remarks>
/// <seealso cref="ContractBenchtopItemTable"></seealso>
/// <seealso cref="ContractBenchtopItemSqlTable"></seealso>
public class BaseContractBenchtopItemSqlTable : DynamicSQLServerAdapter
{
	
	public BaseContractBenchtopItemSqlTable()
	{
	}

	public BaseContractBenchtopItemSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
