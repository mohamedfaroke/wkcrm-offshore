﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountBenchtopTypeSqlTable.cs

using BaseClasses.Data.SqlProvider;

namespace WKCRM.Data
{

/// <summary>
/// The generated superclass for the <see cref="AccountBenchtopTypeSqlTable"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AccountBenchtopTypeTable"></see> class.
/// </remarks>
/// <seealso cref="AccountBenchtopTypeTable"></seealso>
/// <seealso cref="AccountBenchtopTypeSqlTable"></seealso>
public class BaseAccountBenchtopTypeSqlTable : DynamicSQLServerAdapter
{
	
	public BaseAccountBenchtopTypeSqlTable()
	{
	}

	public BaseAccountBenchtopTypeSqlTable(string connectionName) : base(connectionName)
	{
	}

}

}
