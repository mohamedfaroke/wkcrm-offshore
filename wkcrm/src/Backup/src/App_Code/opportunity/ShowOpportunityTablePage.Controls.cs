﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowOpportunityTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowOpportunityTablePage
{
  

#region "Section 1: Place your customizations here."

    
public class opportunityTableControlRow : BaseopportunityTableControlRow
{
      
        // The BaseopportunityTableControlRow implements code for a ROW within the
        // the opportunityTableControl table.  The BaseopportunityTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of opportunityTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class opportunityTableControl : BaseopportunityTableControl
{
        // The BaseopportunityTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The opportunityTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the opportunityTableControlRow control on the ShowOpportunityTablePage page.
// Do not modify this class. Instead override any method in opportunityTableControlRow.
public class BaseopportunityTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseopportunityTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in opportunityTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in opportunityTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in opportunityTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = OpportunityTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseopportunityTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new OpportunityRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in opportunityTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.contact_source_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunityTable.contact_source_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.contact_source_id.Text = formattedValue;
            } else {  
                this.contact_source_id.Text = OpportunityTable.contact_source_id.Format(OpportunityTable.contact_source_id.DefaultValue);
            }
                    
            if (this.contact_source_id.Text == null ||
                this.contact_source_id.Text.Trim().Length == 0) {
                this.contact_source_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.datetime0Specified) {
                      
                string formattedValue = this.DataSource.Format(OpportunityTable.datetime0, @"g");
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.datetime1.Text = formattedValue;
            } else {  
                this.datetime1.Text = OpportunityTable.datetime0.Format(OpportunityTable.datetime0.DefaultValue, @"g");
            }
                    
            if (this.datetime1.Text == null ||
                this.datetime1.Text.Trim().Length == 0) {
                this.datetime1.Text = "&nbsp;";
            }
                  
            if (this.DataSource.employee_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunityTable.employee_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.employee_id.Text = formattedValue;
            } else {  
                this.employee_id.Text = OpportunityTable.employee_id.Format(OpportunityTable.employee_id.DefaultValue);
            }
                    
            if (this.employee_id.Text == null ||
                this.employee_id.Text.Trim().Length == 0) {
                this.employee_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.location_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunityTable.location_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.location_id.Text = formattedValue;
            } else {  
                this.location_id.Text = OpportunityTable.location_id.Format(OpportunityTable.location_id.DefaultValue);
            }
                    
            if (this.location_id.Text == null ||
                this.location_id.Text.Trim().Length == 0) {
                this.location_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.opportunity_source_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunityTable.opportunity_source_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.opportunity_source_id.Text = formattedValue;
            } else {  
                this.opportunity_source_id.Text = OpportunityTable.opportunity_source_id.Format(OpportunityTable.opportunity_source_id.DefaultValue);
            }
                    
            if (this.opportunity_source_id.Text == null ||
                this.opportunity_source_id.Text.Trim().Length == 0) {
                this.opportunity_source_id.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in opportunityTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in opportunityTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in opportunityTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((opportunityTableControl)MiscUtils.GetParentControlObject(this, "opportunityTableControl")).DataChanged = true;
                ((opportunityTableControl)MiscUtils.GetParentControlObject(this, "opportunityTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in opportunityTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in opportunityTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in opportunityTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            OpportunityTable.DeleteRecord(pk);

          
            ((opportunityTableControl)MiscUtils.GetParentControlObject(this, "opportunityTableControl")).DataChanged = true;
            ((opportunityTableControl)MiscUtils.GetParentControlObject(this, "opportunityTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseopportunityTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseopportunityTableControlRow_Rec"] = value;
            }
        }
        
        private OpportunityRecord _DataSource;
        public OpportunityRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal contact_source_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contact_source_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal datetime1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetime1");
            }
        }
           
        public System.Web.UI.WebControls.Literal employee_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal location_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal opportunity_source_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_id");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            OpportunityRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public OpportunityRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return OpportunityTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the opportunityTableControl control on the ShowOpportunityTablePage page.
// Do not modify this class. Instead override any method in opportunityTableControl.
public class BaseopportunityTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseopportunityTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.opportunityPagination.FirstPage.Click += new ImageClickEventHandler(opportunityPagination_FirstPage_Click);
            this.opportunityPagination.LastPage.Click += new ImageClickEventHandler(opportunityPagination_LastPage_Click);
            this.opportunityPagination.NextPage.Click += new ImageClickEventHandler(opportunityPagination_NextPage_Click);
            this.opportunityPagination.PageSizeButton.Button.Click += new EventHandler(opportunityPagination_PageSizeButton_Click);
            this.opportunityPagination.PreviousPage.Click += new ImageClickEventHandler(opportunityPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.datetimeLabel.Click += new EventHandler(datetimeLabel_Click);
            this.employee_idLabel1.Click += new EventHandler(employee_idLabel1_Click);

            // Setup the button events.
        
            this.opportunityExportButton.Button.Click += new EventHandler(opportunityExportButton_Click);
            this.opportunityNewButton.Button.Click += new EventHandler(opportunityNewButton_Click);

            // Setup the filter and search events.
        
            this.employee_idFilter.SelectedIndexChanged += new EventHandler(employee_idFilter_SelectedIndexChanged);
            this.location_idFilter.SelectedIndexChanged += new EventHandler(location_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.employee_idFilter)) {
                this.employee_idFilter.Items.Add(new ListItem(this.GetFromSession(this.employee_idFilter), this.GetFromSession(this.employee_idFilter)));
                this.employee_idFilter.SelectedValue = this.GetFromSession(this.employee_idFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.location_idFilter)) {
                this.location_idFilter.Items.Add(new ListItem(this.GetFromSession(this.location_idFilter), this.GetFromSession(this.location_idFilter)));
                this.location_idFilter.SelectedValue = this.GetFromSession(this.location_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show message on Click
                this.opportunityNewButton.Button.Attributes.Add("onClick", "window.open('./Opportunity.aspx','', 'width=400, height=250, resizable=yes, scrollbars=yes, modal=no')");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (OpportunityRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.OpportunityRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = OpportunityTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (OpportunityRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.OpportunityRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (opportunityTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (OpportunityRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.OpportunityRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = OpportunityTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populateemployee_idFilter(MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), 500);
            this.Populatelocation_idFilter(MiscUtils.GetSelectedValue(this.location_idFilter, this.GetFromSession(this.location_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("opportunityTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                opportunityTableControlRow recControl = (opportunityTableControlRow)(repItem.FindControl("opportunityTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(OpportunityTable.contact_source_id, this.DataSource);
            this.Page.PregetDfkaRecords(OpportunityTable.employee_id, this.DataSource);
            this.Page.PregetDfkaRecords(OpportunityTable.location_id, this.DataSource);
            this.Page.PregetDfkaRecords(OpportunityTable.opportunity_source_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for opportunityTableControl pagination.
        
            this.opportunityPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.opportunityPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.opportunityPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.opportunityPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.opportunityPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.opportunityPagination.CurrentPage.Text = "0";
            }
            this.opportunityPagination.PageSize.Text = this.PageSize.ToString();
            this.opportunityTotalItems.Text = this.TotalRecords.ToString();
            this.opportunityPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.opportunityPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (opportunityTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            OpportunityTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(OpportunityTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.location_idFilter)) {
                wc.iAND(OpportunityTable.location_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.location_idFilter, this.GetFromSession(this.location_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.opportunityPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.opportunityPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("opportunityTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    opportunityTableControlRow recControl = (opportunityTableControlRow)(repItem.FindControl("opportunityTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        OpportunityRecord rec = new OpportunityRecord();
        
                        if (recControl.contact_source_id.Text != "") {
                            rec.Parse(recControl.contact_source_id.Text, OpportunityTable.contact_source_id);
                        }
                        if (recControl.datetime1.Text != "") {
                            rec.Parse(recControl.datetime1.Text, OpportunityTable.datetime0);
                        }
                        if (recControl.employee_id.Text != "") {
                            rec.Parse(recControl.employee_id.Text, OpportunityTable.employee_id);
                        }
                        if (recControl.location_id.Text != "") {
                            rec.Parse(recControl.location_id.Text, OpportunityTable.location_id);
                        }
                        if (recControl.opportunity_source_id.Text != "") {
                            rec.Parse(recControl.opportunity_source_id.Text, OpportunityTable.opportunity_source_id);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new OpportunityRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (OpportunityRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.OpportunityRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(opportunityTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(opportunityTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for employee_idFilter.
        protected virtual void Populateemployee_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.employee_idFilter.Items.Clear();
            foreach (EmployeeRecord itemValue in EmployeeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(EmployeeTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.employee_idFilter.Items.IndexOf(item) < 0) {
                    this.employee_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.employee_idFilter, selectedValue);

            // Add the All item.
            this.employee_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for location_idFilter.
        protected virtual void Populatelocation_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(LocationTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.location_idFilter.Items.Clear();
            foreach (LocationRecord itemValue in LocationTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(LocationTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.location_idFilter.Items.IndexOf(item) < 0) {
                    this.location_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.location_idFilter, selectedValue);

            // Add the All item.
            this.location_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter employee_idFilter.
        public virtual WhereClause CreateWhereClause_employee_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.location_idFilter)) {
                wc.iAND(OpportunityTable.location_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.location_idFilter, this.GetFromSession(this.location_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter location_idFilter.
        public virtual WhereClause CreateWhereClause_location_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.employee_idFilter)) {
                wc.iAND(OpportunityTable.employee_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.employee_idFilter, this.GetFromSession(this.employee_idFilter)), false, false);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.employee_idFilter, this.employee_idFilter.SelectedValue);
            this.SaveToSession(this.location_idFilter, this.location_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.employee_idFilter);
            this.RemoveFromSession(this.location_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["opportunityTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["opportunityTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void opportunityPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void opportunityPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void opportunityPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void opportunityPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void opportunityPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void datetimeLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(OpportunityTable.datetime0);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(OpportunityTable.datetime0, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void employee_idLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(OpportunityTable.employee_id);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(OpportunityTable.employee_id, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void opportunityExportButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            WhereClause wc = this.CreateWhereClause();
            String exportedData = OpportunityTable.Export(wc);
            BaseClasses.Utils.NetUtils.WriteResponseTextAttachment(this.Page.Response, "OpportunityTable.csv", exportedData);
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void opportunityNewButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void employee_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void location_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private OpportunityRecord[] _DataSource = null;
        public  OpportunityRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal contact_source_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contact_source_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton datetimeLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "datetimeLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList employee_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal employee_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton employee_idLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employee_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList location_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal location_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal location_idLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunity_source_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_source_idLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton opportunityExportButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunityExportButton");
            }
        }
        
        public WKCRM.UI.IThemeButton opportunityNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunityNewButton");
            }
        }
        
        public WKCRM.UI.IPagination opportunityPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunityPagination");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunityTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunityTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label opportunityTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunityTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                opportunityTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                OpportunityRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public opportunityTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public opportunityTableControlRow[] GetSelectedRecordControls()
        {
        
            return (opportunityTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.ShowOpportunityTablePage.opportunityTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            opportunityTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (opportunityTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public opportunityTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("opportunityTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                opportunityTableControlRow recControl = (opportunityTableControlRow)repItem.FindControl("opportunityTableControlRow");
                recList.Add(recControl);
            }

            return (opportunityTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowOpportunityTablePage.opportunityTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  