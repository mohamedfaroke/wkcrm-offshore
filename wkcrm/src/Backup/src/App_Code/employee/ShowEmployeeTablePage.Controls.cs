﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowEmployeeTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowEmployeeTablePage
{
  

#region "Section 1: Place your customizations here."

    
public class employeeTableControlRow : BaseemployeeTableControlRow
{
      
        // The BaseemployeeTableControlRow implements code for a ROW within the
        // the employeeTableControl table.  The BaseemployeeTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of employeeTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class employeeTableControl : BaseemployeeTableControl
{
        // The BaseemployeeTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The employeeTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the employeeTableControlRow control on the ShowEmployeeTablePage page.
// Do not modify this class. Instead override any method in employeeTableControlRow.
public class BaseemployeeTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseemployeeTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in employeeTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.employeeRecordRowEditButton.Click += new ImageClickEventHandler(employeeRecordRowEditButton_Click);
            this.employeeRecordRowViewButton.Click += new ImageClickEventHandler(employeeRecordRowViewButton_Click);
        }

        // To customize, override this method in employeeTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in employeeTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EmployeeTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseemployeeTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new EmployeeRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in employeeTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.emailSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.email);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.EmployeeTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"email\\\", \\\"Email\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.email.Text = formattedValue;
            } else {  
                this.email.Text = EmployeeTable.email.Format(EmployeeTable.email.DefaultValue);
            }
                    
            if (this.email.Text == null ||
                this.email.Text.Trim().Length == 0) {
                this.email.Text = "&nbsp;";
            }
                  
            if (this.DataSource.location_idSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.location_id);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.location_id.Text = formattedValue;
            } else {  
                this.location_id.Text = EmployeeTable.location_id.Format(EmployeeTable.location_id.DefaultValue);
            }
                    
            if (this.location_id.Text == null ||
                this.location_id.Text.Trim().Length == 0) {
                this.location_id.Text = "&nbsp;";
            }
                  
            if (this.DataSource.mobileSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.mobile);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.mobile.Text = formattedValue;
            } else {  
                this.mobile.Text = EmployeeTable.mobile.Format(EmployeeTable.mobile.DefaultValue);
            }
                    
            if (this.mobile.Text == null ||
                this.mobile.Text.Trim().Length == 0) {
                this.mobile.Text = "&nbsp;";
            }
                  
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.name);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.EmployeeTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"name\\\", \\\"Name\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = EmployeeTable.name.Format(EmployeeTable.name.DefaultValue);
            }
                    
            if (this.name.Text == null ||
                this.name.Text.Trim().Length == 0) {
                this.name.Text = "&nbsp;";
            }
                  
            if (this.DataSource.phoneSpecified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.phone);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.phone.Text = formattedValue;
            } else {  
                this.phone.Text = EmployeeTable.phone.Format(EmployeeTable.phone.DefaultValue);
            }
                    
            if (this.phone.Text == null ||
                this.phone.Text.Trim().Length == 0) {
                this.phone.Text = "&nbsp;";
            }
                  
            if (this.DataSource.UserName0Specified) {
                      
                string formattedValue = this.DataSource.Format(EmployeeTable.UserName0);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.UserName1.Text = formattedValue;
            } else {  
                this.UserName1.Text = EmployeeTable.UserName0.Format(EmployeeTable.UserName0.DefaultValue);
            }
                    
            if (this.UserName1.Text == null ||
                this.UserName1.Text.Trim().Length == 0) {
                this.UserName1.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in employeeTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in employeeTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in employeeTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((employeeTableControl)MiscUtils.GetParentControlObject(this, "employeeTableControl")).DataChanged = true;
                ((employeeTableControl)MiscUtils.GetParentControlObject(this, "employeeTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in employeeTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in employeeTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in employeeTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EmployeeTable.DeleteRecord(pk);

          
            ((employeeTableControl)MiscUtils.GetParentControlObject(this, "employeeTableControl")).DataChanged = true;
            ((employeeTableControl)MiscUtils.GetParentControlObject(this, "employeeTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void employeeRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../employee/EditEmployeePage.aspx?Employee={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for ImageButton
        public virtual void employeeRecordRowViewButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../employee/ShowEmployeePage.aspx?Employee={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseemployeeTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseemployeeTableControlRow_Rec"] = value;
            }
        }
        
        private EmployeeRecord _DataSource;
        public EmployeeRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal email {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "email");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton employeeRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRecordRowEditButton");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox employeeRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRecordRowSelection");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton employeeRecordRowViewButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeRecordRowViewButton");
            }
        }
           
        public System.Web.UI.WebControls.Literal location_id {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_id");
            }
        }
           
        public System.Web.UI.WebControls.Literal mobile {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobile");
            }
        }
           
        public System.Web.UI.WebControls.Literal name {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
           
        public System.Web.UI.WebControls.Literal phone {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phone");
            }
        }
           
        public System.Web.UI.WebControls.Literal UserName1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "UserName1");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EmployeeRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EmployeeRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return EmployeeTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the employeeTableControl control on the ShowEmployeeTablePage page.
// Do not modify this class. Instead override any method in employeeTableControl.
public class BaseemployeeTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseemployeeTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.employeePagination.FirstPage.Click += new ImageClickEventHandler(employeePagination_FirstPage_Click);
            this.employeePagination.LastPage.Click += new ImageClickEventHandler(employeePagination_LastPage_Click);
            this.employeePagination.NextPage.Click += new ImageClickEventHandler(employeePagination_NextPage_Click);
            this.employeePagination.PageSizeButton.Button.Click += new EventHandler(employeePagination_PageSizeButton_Click);
            this.employeePagination.PreviousPage.Click += new ImageClickEventHandler(employeePagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.location_idLabel1.Click += new EventHandler(location_idLabel1_Click);
            this.nameLabel1.Click += new EventHandler(nameLabel1_Click);

            // Setup the button events.
        
            this.employeeDeleteButton.Button.Click += new EventHandler(employeeDeleteButton_Click);
            this.employeeEditButton.Button.Click += new EventHandler(employeeEditButton_Click);
            this.employeeExportButton.Button.Click += new EventHandler(employeeExportButton_Click);
            this.employeeNewButton.Button.Click += new EventHandler(employeeNewButton_Click);
            this.employeeSearchButton.Button.Click += new EventHandler(employeeSearchButton_Click);

            // Setup the filter and search events.
        
            this.location_idFilter.SelectedIndexChanged += new EventHandler(location_idFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.employeeSearchArea)) {
                
                this.employeeSearchArea.Text = this.GetFromSession(this.employeeSearchArea);
            }
            if (!this.Page.IsPostBack && this.InSession(this.location_idFilter)) {
                this.location_idFilter.Items.Add(new ListItem(this.GetFromSession(this.location_idFilter), this.GetFromSession(this.location_idFilter)));
                this.location_idFilter.SelectedValue = this.GetFromSession(this.location_idFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.employeeDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EmployeeRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EmployeeRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = EmployeeTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EmployeeRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EmployeeRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (employeeTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (EmployeeRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.EmployeeRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = EmployeeTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.Populatelocation_idFilter(MiscUtils.GetSelectedValue(this.location_idFilter, this.GetFromSession(this.location_idFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("employeeTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                employeeTableControlRow recControl = (employeeTableControlRow)(repItem.FindControl("employeeTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(EmployeeTable.location_id, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for employeeTableControl pagination.
        
            this.employeePagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.employeePagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.employeePagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.employeePagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.employeePagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.employeePagination.CurrentPage.Text = "0";
            }
            this.employeePagination.PageSize.Text = this.PageSize.ToString();
            this.employeeTotalItems.Text = this.TotalRecords.ToString();
            this.employeePagination.TotalItems.Text = this.TotalRecords.ToString();
            this.employeePagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (employeeTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            EmployeeTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.employeeSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(EmployeeTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.phone, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.address, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.UserName0, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.email, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.mobile, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.location_idFilter)) {
                wc.iAND(EmployeeTable.location_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.location_idFilter, this.GetFromSession(this.location_idFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.employeePagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.employeePagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("employeeTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    employeeTableControlRow recControl = (employeeTableControlRow)(repItem.FindControl("employeeTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        EmployeeRecord rec = new EmployeeRecord();
        
                        if (recControl.email.Text != "") {
                            rec.Parse(recControl.email.Text, EmployeeTable.email);
                        }
                        if (recControl.location_id.Text != "") {
                            rec.Parse(recControl.location_id.Text, EmployeeTable.location_id);
                        }
                        if (recControl.mobile.Text != "") {
                            rec.Parse(recControl.mobile.Text, EmployeeTable.mobile);
                        }
                        if (recControl.name.Text != "") {
                            rec.Parse(recControl.name.Text, EmployeeTable.name);
                        }
                        if (recControl.phone.Text != "") {
                            rec.Parse(recControl.phone.Text, EmployeeTable.phone);
                        }
                        if (recControl.UserName1.Text != "") {
                            rec.Parse(recControl.UserName1.Text, EmployeeTable.UserName0);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new EmployeeRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (EmployeeRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.EmployeeRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(employeeTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(employeeTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for location_idFilter.
        protected virtual void Populatelocation_idFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(LocationTable.name, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.location_idFilter.Items.Clear();
            foreach (LocationRecord itemValue in LocationTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(LocationTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.location_idFilter.Items.IndexOf(item) < 0) {
                    this.location_idFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.location_idFilter, selectedValue);

            // Add the All item.
            this.location_idFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter employeeSearchArea.
        public virtual WhereClause CreateWhereClause_employeeSearchArea()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.location_idFilter)) {
                wc.iAND(EmployeeTable.location_id, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.location_idFilter, this.GetFromSession(this.location_idFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter location_idFilter.
        public virtual WhereClause CreateWhereClause_location_idFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.employeeSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(EmployeeTable.name, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.phone, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.address, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.UserName0, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.email, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                search.iOR(EmployeeTable.mobile, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.employeeSearchArea, this.GetFromSession(this.employeeSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.employeeSearchArea, this.employeeSearchArea.Text);
            this.SaveToSession(this.location_idFilter, this.location_idFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.employeeSearchArea);
            this.RemoveFromSession(this.location_idFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["employeeTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["employeeTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void employeePagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void employeePagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void employeePagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void employeePagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void employeePagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void location_idLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(EmployeeTable.location_id);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(EmployeeTable.location_id, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void nameLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(EmployeeTable.name);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(EmployeeTable.name, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void employeeDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void employeeEditButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../employee/EditEmployeePage.aspx?Employee={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void employeeExportButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            WhereClause wc = this.CreateWhereClause();
            String exportedData = EmployeeTable.Export(wc);
            BaseClasses.Utils.NetUtils.WriteResponseTextAttachment(this.Page.Response, "EmployeeTable.csv", exportedData);
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void employeeNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../employee/AddEmployeePage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void employeeSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void location_idFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private EmployeeRecord[] _DataSource = null;
        public  EmployeeRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal emailLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "emailLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeEditButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeEditButton");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeExportButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeExportButton");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeNewButton");
            }
        }
        
        public WKCRM.UI.IPagination employeePagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeePagination");
            }
        }
        
        public System.Web.UI.WebControls.TextBox employeeSearchArea {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeSearchArea");
            }
        }
        
        public WKCRM.UI.IThemeButton employeeSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal employeeTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox employeeToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label employeeTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "employeeTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList location_idFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal location_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton location_idLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "location_idLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal mobileLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "mobileLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton nameLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel1");
            }
        }
        
        public System.Web.UI.WebControls.Literal phoneLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "phoneLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal usernameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "usernameLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                employeeTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                EmployeeRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (employeeTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.employeeRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public employeeTableControlRow GetSelectedRecordControl()
        {
        employeeTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public employeeTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (employeeTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.employeeRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (employeeTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowEmployeeTablePage.employeeTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            employeeTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (employeeTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.employeeRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public employeeTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("employeeTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                employeeTableControlRow recControl = (employeeTableControlRow)repItem.FindControl("employeeTableControlRow");
                recList.Add(recControl);
            }

            return (employeeTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowEmployeeTablePage.employeeTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  