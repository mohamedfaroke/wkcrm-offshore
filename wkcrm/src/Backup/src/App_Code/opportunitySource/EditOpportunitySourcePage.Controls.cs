﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// EditOpportunitySourcePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.EditOpportunitySourcePage
{
  

#region "Section 1: Place your customizations here."

    

  
public class opportunitySourceRecordControl : BaseopportunitySourceRecordControl
{
      
        // The BaseopportunitySourceRecordControl implements the LoadData, DataBind and other
        // methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. For example, you can override the LoadData, 
        // CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

    public override WhereClause CreateWhereClause_opportunity_category_idDropDownList()
    {
        return Globals.BuildOpCategoryWC();
    }
}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the opportunitySourceRecordControl control on the EditOpportunitySourcePage page.
// Do not modify this class. Instead override any method in opportunitySourceRecordControl.
public class BaseopportunitySourceRecordControl : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseopportunitySourceRecordControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in opportunitySourceRecordControl.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in opportunitySourceRecordControl.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in opportunitySourceRecordControl.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = OpportunitySourceTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            WhereClause wc = this.CreateWhereClause();
            if (wc == null) {
                this.DataSource = new OpportunitySourceRecord();
                return;
            }

            // Retrieve the record from the database.
            OpportunitySourceRecord[] recList = OpportunitySourceTable.GetRecords(wc, null, 0, 2);
            if (recList.Length == 0) {
                throw new Exception(Page.GetResourceValue("Err:NoRecRetrieved", "WKCRM"));
            }

            
            this.DataSource = OpportunitySourceTable.GetRecord(recList[0].GetID().ToXmlString(), true);
              
        }

        // Populate the UI controls using the DataSource. To customize, override this method in opportunitySourceRecordControl.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.display_orderSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunitySourceTable.display_order);
                this.display_order.Text = formattedValue;
            } else {  
                this.display_order.Text = OpportunitySourceTable.display_order.Format(OpportunitySourceTable.display_order.DefaultValue);
            }
                    
            if (this.DataSource.nameSpecified) {
                      
                string formattedValue = this.DataSource.Format(OpportunitySourceTable.name);
                this.name.Text = formattedValue;
            } else {  
                this.name.Text = OpportunitySourceTable.name.Format(OpportunitySourceTable.name.DefaultValue);
            }
                    
            if (this.DataSource.opportunity_category_idSpecified) {
                this.Populateopportunity_category_idDropDownList(this.DataSource.opportunity_category_id.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populateopportunity_category_idDropDownList(OpportunitySourceTable.opportunity_category_id.DefaultValue, 100);
                } else {
                this.Populateopportunity_category_idDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.status_idSpecified) {
                this.status_id.Checked = this.DataSource.status_id;
            } else {
                if (!this.DataSource.IsCreated) {
                    this.status_id.Checked = OpportunitySourceTable.status_id.ParseValue(OpportunitySourceTable.status_id.DefaultValue).ToBoolean();
                }
            }
                    
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in opportunitySourceRecordControl.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in opportunitySourceRecordControl to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in opportunitySourceRecordControl to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in opportunitySourceRecordControl.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.display_order.Text, OpportunitySourceTable.display_order);
                          
            this.DataSource.Parse(this.name.Text, OpportunitySourceTable.name);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.opportunity_category_id), OpportunitySourceTable.opportunity_category_id);
                  
            this.DataSource.status_id = this.status_id.Checked;
                    
        }

        //  To customize, override this method in opportunitySourceRecordControl.
        public virtual WhereClause CreateWhereClause()
        {
        
            WhereClause wc;
            OpportunitySourceTable.Instance.InnerFilter = null;
            wc = new WhereClause();
            // Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
            
            // Retrieve the record id from the URL parameter.
            string recId = this.Page.Request.QueryString["OpportunitySource"];
            if (recId == null || recId.Length == 0) {
                // Get the error message from the application resource file.
                throw new Exception(Page.GetResourceValue("Err:UrlParamMissing", "WKCRM").Replace("{URL}", "OpportunitySource"));
            }

              
            if (KeyValue.IsXmlKey(recId)) {
                KeyValue pkValue = KeyValue.XmlToKey(recId);
                
                wc.iAND(OpportunitySourceTable.id0, BaseFilter.ComparisonOperator.EqualsTo, pkValue.GetColumnValue(OpportunitySourceTable.id0).ToString());
            } else {
                
                wc.iAND(OpportunitySourceTable.id0, BaseFilter.ComparisonOperator.EqualsTo, recId);
            }
              
            return wc;
          
        }
        

        //  To customize, override this method in opportunitySourceRecordControl.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            OpportunitySourceTable.DeleteRecord(pk);

          
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_opportunity_category_idDropDownList() {
            return new WhereClause();
        }
                
        // Fill the opportunity_category_id list.
        protected virtual void Populateopportunity_category_idDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_opportunity_category_idDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(OpportunityCategoryTable.name, OrderByItem.OrderDir.Asc);

                      this.opportunity_category_id.Items.Clear();
            foreach (OpportunityCategoryRecord itemValue in OpportunityCategoryTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(OpportunityCategoryTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.opportunity_category_id.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.opportunity_category_id, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.opportunity_category_id, OpportunitySourceTable.opportunity_category_id.Format(selectedValue))) {
                string fvalue = OpportunitySourceTable.opportunity_category_id.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.opportunity_category_id.Items.Insert(0, item);
            }

                  
            this.opportunity_category_id.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseopportunitySourceRecordControl_Rec"];
            }
            set {
                this.ViewState["BaseopportunitySourceRecordControl_Rec"] = value;
            }
        }
        
        private OpportunitySourceRecord _DataSource;
        public OpportunitySourceRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.TextBox display_order {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "display_order");
            }
        }
        
        public System.Web.UI.WebControls.Literal display_orderLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "display_orderLabel");
            }
        }
           
        public System.Web.UI.WebControls.TextBox name {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "name");
            }
        }
        
        public System.Web.UI.WebControls.Literal nameLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "nameLabel");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList opportunity_category_id {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_category_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunity_category_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunity_category_idLabel");
            }
        }
        
        public System.Web.UI.WebControls.Image opportunitySourceDialogIcon {
            get {
                return (System.Web.UI.WebControls.Image)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceDialogIcon");
            }
        }
        
        public System.Web.UI.WebControls.Literal opportunitySourceDialogTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "opportunitySourceDialogTitle");
            }
        }
           
        public System.Web.UI.WebControls.CheckBox status_id {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_id");
            }
        }
        
        public System.Web.UI.WebControls.Literal status_idLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "status_idLabel");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            OpportunitySourceRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public OpportunitySourceRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return OpportunitySourceTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  

#endregion
    
  
}

  