﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// ShowEvaluationQuestionTablePage.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.ShowEvaluationQuestionTablePage
{
  

#region "Section 1: Place your customizations here."

    
public class EvaluationQuestionTableControlRow : BaseEvaluationQuestionTableControlRow
{
      
        // The BaseEvaluationQuestionTableControlRow implements code for a ROW within the
        // the EvaluationQuestionTableControl table.  The BaseEvaluationQuestionTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of EvaluationQuestionTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.
        

}

  

public class EvaluationQuestionTableControl : BaseEvaluationQuestionTableControl
{
        // The BaseEvaluationQuestionTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The EvaluationQuestionTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the EvaluationQuestionTableControlRow control on the ShowEvaluationQuestionTablePage page.
// Do not modify this class. Instead override any method in EvaluationQuestionTableControlRow.
public class BaseEvaluationQuestionTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseEvaluationQuestionTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in EvaluationQuestionTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
            this.EvaluationQuestionRecordRowEditButton.Click += new ImageClickEventHandler(EvaluationQuestionRecordRowEditButton_Click);
            this.EvaluationQuestionRecordRowViewButton.Click += new ImageClickEventHandler(EvaluationQuestionRecordRowViewButton_Click);
        }

        // To customize, override this method in EvaluationQuestionTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in EvaluationQuestionTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = EvaluationQuestionTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseEvaluationQuestionTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new EvaluationQuestionRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in EvaluationQuestionTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.associated_taskSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionTable.associated_task);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.associated_task.Text = formattedValue;
            } else {  
                this.associated_task.Text = EvaluationQuestionTable.associated_task.Format(EvaluationQuestionTable.associated_task.DefaultValue);
            }
                    
            if (this.associated_task.Text == null ||
                this.associated_task.Text.Trim().Length == 0) {
                this.associated_task.Text = "&nbsp;";
            }
                  
            if (this.DataSource.EvaluationIDSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionTable.EvaluationID);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.EvaluationID.Text = formattedValue;
            } else {  
                this.EvaluationID.Text = EvaluationQuestionTable.EvaluationID.Format(EvaluationQuestionTable.EvaluationID.DefaultValue);
            }
                    
            if (this.EvaluationID.Text == null ||
                this.EvaluationID.Text.Trim().Length == 0) {
                this.EvaluationID.Text = "&nbsp;";
            }
                  
            if (this.DataSource.OrderNumberSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionTable.OrderNumber);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.OrderNumber.Text = formattedValue;
            } else {  
                this.OrderNumber.Text = EvaluationQuestionTable.OrderNumber.Format(EvaluationQuestionTable.OrderNumber.DefaultValue);
            }
                    
            if (this.OrderNumber.Text == null ||
                this.OrderNumber.Text.Trim().Length == 0) {
                this.OrderNumber.Text = "&nbsp;";
            }
                  
            if (this.DataSource.QuestionSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionTable.Question);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                formattedValue = NetUtils.EncodeStringForHtmlDisplay(formattedValue);
                if(formattedValue != null){
                    int popupThreshold = (int)(100);
                              
                    int maxLength = formattedValue.Length;
                    if (maxLength > (int)(100)){
                        maxLength = (int)(100);
                    }
                                
                    if (formattedValue.Length >= popupThreshold) {
                              
                         formattedValue = "<a OnClick=\'gPersist=true;\' OnMouseOut=\'detailRolloverPopupClose();\'" +
                              "OnMouseOver=\'SaveMousePosition(event); delayRolloverPopup(\"PageMethods.GetRecordFieldValue(\\\"WKCRM.Business.EvaluationQuestionTable, App_Code\\\",\\\"" +
                              this.DataSource.GetID().ToString() + "\\\", \\\"Question\\\", \\\"Question\\\"," +
                              " false, 200," +
                              " 300, true, PopupDisplayWindowCallBackWith20);\", 500);'>" + formattedValue.Substring(0, maxLength) + "..." + "</a>";
                    }
                    else{
                        if (formattedValue.Length > maxLength) {
                            formattedValue = formattedValue.Substring(0,maxLength);
                        }
                    }
                }
                            
                this.Question.Text = formattedValue;
            } else {  
                this.Question.Text = EvaluationQuestionTable.Question.Format(EvaluationQuestionTable.Question.DefaultValue);
            }
                    
            if (this.Question.Text == null ||
                this.Question.Text.Trim().Length == 0) {
                this.Question.Text = "&nbsp;";
            }
                  
            if (this.DataSource.TypeIDSpecified) {
                      
                string formattedValue = this.DataSource.Format(EvaluationQuestionTable.TypeID);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.TypeID.Text = formattedValue;
            } else {  
                this.TypeID.Text = EvaluationQuestionTable.TypeID.Format(EvaluationQuestionTable.TypeID.DefaultValue);
            }
                    
            if (this.TypeID.Text == null ||
                this.TypeID.Text.Trim().Length == 0) {
                this.TypeID.Text = "&nbsp;";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in EvaluationQuestionTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in EvaluationQuestionTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in EvaluationQuestionTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((EvaluationQuestionTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionTableControl")).DataChanged = true;
                ((EvaluationQuestionTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in EvaluationQuestionTableControlRow.
        public virtual void GetUIData()
        {
        
        }

        //  To customize, override this method in EvaluationQuestionTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in EvaluationQuestionTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            EvaluationQuestionTable.DeleteRecord(pk);

          
            ((EvaluationQuestionTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionTableControl")).DataChanged = true;
            ((EvaluationQuestionTableControl)MiscUtils.GetParentControlObject(this, "EvaluationQuestionTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        // event handler for ImageButton
        public virtual void EvaluationQuestionRecordRowEditButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../EvaluationQuestion/EditEvaluationQuestionPage.aspx?EvaluationQuestion={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for ImageButton
        public virtual void EvaluationQuestionRecordRowViewButton_Click(object sender, ImageClickEventArgs args)
        {
            
            string url = @"../EvaluationQuestion/ShowEvaluationQuestionPage.aspx?EvaluationQuestion={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseEvaluationQuestionTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseEvaluationQuestionTableControlRow_Rec"] = value;
            }
        }
        
        private EvaluationQuestionRecord _DataSource;
        public EvaluationQuestionRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.Literal associated_task {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "associated_task");
            }
        }
           
        public System.Web.UI.WebControls.Literal EvaluationID {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationID");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton EvaluationQuestionRecordRowEditButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionRecordRowEditButton");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox EvaluationQuestionRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionRecordRowSelection");
            }
        }
        
        public System.Web.UI.WebControls.ImageButton EvaluationQuestionRecordRowViewButton {
            get {
                return (System.Web.UI.WebControls.ImageButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionRecordRowViewButton");
            }
        }
           
        public System.Web.UI.WebControls.Literal OrderNumber {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OrderNumber");
            }
        }
           
        public System.Web.UI.WebControls.Literal Question {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Question");
            }
        }
           
        public System.Web.UI.WebControls.Literal TypeID {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TypeID");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            EvaluationQuestionRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public EvaluationQuestionRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return EvaluationQuestionTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the EvaluationQuestionTableControl control on the ShowEvaluationQuestionTablePage page.
// Do not modify this class. Instead override any method in EvaluationQuestionTableControl.
public class BaseEvaluationQuestionTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseEvaluationQuestionTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        
            this.EvaluationQuestionPagination.FirstPage.Click += new ImageClickEventHandler(EvaluationQuestionPagination_FirstPage_Click);
            this.EvaluationQuestionPagination.LastPage.Click += new ImageClickEventHandler(EvaluationQuestionPagination_LastPage_Click);
            this.EvaluationQuestionPagination.NextPage.Click += new ImageClickEventHandler(EvaluationQuestionPagination_NextPage_Click);
            this.EvaluationQuestionPagination.PageSizeButton.Button.Click += new EventHandler(EvaluationQuestionPagination_PageSizeButton_Click);
            this.EvaluationQuestionPagination.PreviousPage.Click += new ImageClickEventHandler(EvaluationQuestionPagination_PreviousPage_Click);

            // Setup the sorting events.
        
            this.EvaluationIDLabel1.Click += new EventHandler(EvaluationIDLabel1_Click);
            this.QuestionLabel.Click += new EventHandler(QuestionLabel_Click);

            // Setup the button events.
        
            this.EvaluationQuestionDeleteButton.Button.Click += new EventHandler(EvaluationQuestionDeleteButton_Click);
            this.EvaluationQuestionEditButton.Button.Click += new EventHandler(EvaluationQuestionEditButton_Click);
            this.EvaluationQuestionExportButton.Button.Click += new EventHandler(EvaluationQuestionExportButton_Click);
            this.EvaluationQuestionNewButton.Button.Click += new EventHandler(EvaluationQuestionNewButton_Click);
            this.EvaluationQuestionSearchButton.Button.Click += new EventHandler(EvaluationQuestionSearchButton_Click);

            // Setup the filter and search events.
        
            this.EvaluationIDFilter.SelectedIndexChanged += new EventHandler(EvaluationIDFilter_SelectedIndexChanged);
            this.TypeIDFilter.SelectedIndexChanged += new EventHandler(TypeIDFilter_SelectedIndexChanged);
            if (!this.Page.IsPostBack && this.InSession(this.EvaluationIDFilter)) {
                this.EvaluationIDFilter.Items.Add(new ListItem(this.GetFromSession(this.EvaluationIDFilter), this.GetFromSession(this.EvaluationIDFilter)));
                this.EvaluationIDFilter.SelectedValue = this.GetFromSession(this.EvaluationIDFilter);
            }
            if (!this.Page.IsPostBack && this.InSession(this.EvaluationQuestionSearchArea)) {
                
                this.EvaluationQuestionSearchArea.Text = this.GetFromSession(this.EvaluationQuestionSearchArea);
            }
            if (!this.Page.IsPostBack && this.InSession(this.TypeIDFilter)) {
                this.TypeIDFilter.Items.Add(new ListItem(this.GetFromSession(this.TypeIDFilter), this.GetFromSession(this.TypeIDFilter)));
                this.TypeIDFilter.SelectedValue = this.GetFromSession(this.TypeIDFilter);
            }

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "10"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.EvaluationQuestionDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EvaluationQuestionRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = EvaluationQuestionTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (EvaluationQuestionRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (EvaluationQuestionTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (EvaluationQuestionRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = EvaluationQuestionTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        
            this.PopulateEvaluationIDFilter(MiscUtils.GetSelectedValue(this.EvaluationIDFilter, this.GetFromSession(this.EvaluationIDFilter)), 500);
            this.PopulateTypeIDFilter(MiscUtils.GetSelectedValue(this.TypeIDFilter, this.GetFromSession(this.TypeIDFilter)), 500);

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("EvaluationQuestionTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                EvaluationQuestionTableControlRow recControl = (EvaluationQuestionTableControlRow)(repItem.FindControl("EvaluationQuestionTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(EvaluationQuestionTable.associated_task, this.DataSource);
            this.Page.PregetDfkaRecords(EvaluationQuestionTable.EvaluationID, this.DataSource);
            this.Page.PregetDfkaRecords(EvaluationQuestionTable.TypeID, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for EvaluationQuestionTableControl pagination.
        
            this.EvaluationQuestionPagination.FirstPage.Enabled = !(this.PageIndex == 0);
            this.EvaluationQuestionPagination.LastPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.EvaluationQuestionPagination.NextPage.Enabled = !(this.PageIndex == this.TotalPages - 1);
            this.EvaluationQuestionPagination.PreviousPage.Enabled = !(this.PageIndex == 0);

            // Bind the pagination labels.
        
            if (this.TotalPages > 0) {
                this.EvaluationQuestionPagination.CurrentPage.Text = (this.PageIndex + 1).ToString();
            } else {
                this.EvaluationQuestionPagination.CurrentPage.Text = "0";
            }
            this.EvaluationQuestionPagination.PageSize.Text = this.PageSize.ToString();
            this.EvaluationQuestionTotalItems.Text = this.TotalRecords.ToString();
            this.EvaluationQuestionPagination.TotalItems.Text = this.TotalRecords.ToString();
            this.EvaluationQuestionPagination.TotalPages.Text = this.TotalPages.ToString();
        }

        public virtual void SaveData()
        {
            foreach (EvaluationQuestionTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            EvaluationQuestionTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            if (MiscUtils.IsValueSelected(this.EvaluationIDFilter)) {
                wc.iAND(EvaluationQuestionTable.EvaluationID, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.EvaluationIDFilter, this.GetFromSession(this.EvaluationIDFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.EvaluationQuestionSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(EvaluationQuestionTable.Question, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.EvaluationQuestionSearchArea, this.GetFromSession(this.EvaluationQuestionSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.TypeIDFilter)) {
                wc.iAND(EvaluationQuestionTable.TypeID, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.TypeIDFilter, this.GetFromSession(this.TypeIDFilter)), false, false);
            }
                  
            return (wc);
        }
        
        protected virtual void GetPageSize()
        {
        
            if (this.EvaluationQuestionPagination.PageSize.Text.Length > 0) {
                try {
                    this.PageSize = Convert.ToInt32(this.EvaluationQuestionPagination.PageSize.Text);
                } catch (Exception ex) {
                }
            }
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("EvaluationQuestionTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    EvaluationQuestionTableControlRow recControl = (EvaluationQuestionTableControlRow)(repItem.FindControl("EvaluationQuestionTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        EvaluationQuestionRecord rec = new EvaluationQuestionRecord();
        
                        if (recControl.associated_task.Text != "") {
                            rec.Parse(recControl.associated_task.Text, EvaluationQuestionTable.associated_task);
                        }
                        if (recControl.EvaluationID.Text != "") {
                            rec.Parse(recControl.EvaluationID.Text, EvaluationQuestionTable.EvaluationID);
                        }
                        if (recControl.OrderNumber.Text != "") {
                            rec.Parse(recControl.OrderNumber.Text, EvaluationQuestionTable.OrderNumber);
                        }
                        if (recControl.Question.Text != "") {
                            rec.Parse(recControl.Question.Text, EvaluationQuestionTable.Question);
                        }
                        if (recControl.TypeID.Text != "") {
                            rec.Parse(recControl.TypeID.Text, EvaluationQuestionTable.TypeID);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new EvaluationQuestionRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (EvaluationQuestionRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.EvaluationQuestionRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(EvaluationQuestionTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(EvaluationQuestionTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        // Get the filters' data for EvaluationIDFilter.
        protected virtual void PopulateEvaluationIDFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(EvaluationsTable.EvaluationName, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.EvaluationIDFilter.Items.Clear();
            foreach (EvaluationsRecord itemValue in EvaluationsTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.EvaluationIDSpecified) {
                    cvalue = itemValue.EvaluationID.ToString();
                    fvalue = itemValue.Format(EvaluationsTable.EvaluationName);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.EvaluationIDFilter.Items.IndexOf(item) < 0) {
                    this.EvaluationIDFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.EvaluationIDFilter, selectedValue);

            // Add the All item.
            this.EvaluationIDFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
            
        // Get the filters' data for TypeIDFilter.
        protected virtual void PopulateTypeIDFilter(string selectedValue, int maxItems)
        {
              
            //Setup the WHERE clause.
            WhereClause wc = new WhereClause();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(QuestionTypeTable.Type0, OrderByItem.OrderDir.Asc);

            string noValueFormat = Page.GetResourceValue("Txt:Other", "WKCRM");

            this.TypeIDFilter.Items.Clear();
            foreach (QuestionTypeRecord itemValue in QuestionTypeTable.GetRecords(wc, orderBy, 0, maxItems))
            {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = noValueFormat;
                if (itemValue.typeIdSpecified) {
                    cvalue = itemValue.typeId.ToString();
                    fvalue = itemValue.Format(QuestionTypeTable.Type0);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                if (this.TypeIDFilter.Items.IndexOf(item) < 0) {
                    this.TypeIDFilter.Items.Add(item);
                }
            }
                
            // Set the selected value.
            MiscUtils.SetSelectedValue(this.TypeIDFilter, selectedValue);

            // Add the All item.
            this.TypeIDFilter.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:All", "WKCRM"), "--ANY--"));
        }
                          
        // Create a where clause for the filter EvaluationIDFilter.
        public virtual WhereClause CreateWhereClause_EvaluationIDFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.EvaluationQuestionSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(EvaluationQuestionTable.Question, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.EvaluationQuestionSearchArea, this.GetFromSession(this.EvaluationQuestionSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            if (MiscUtils.IsValueSelected(this.TypeIDFilter)) {
                wc.iAND(EvaluationQuestionTable.TypeID, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.TypeIDFilter, this.GetFromSession(this.TypeIDFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter EvaluationQuestionSearchArea.
        public virtual WhereClause CreateWhereClause_EvaluationQuestionSearchArea()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.EvaluationIDFilter)) {
                wc.iAND(EvaluationQuestionTable.EvaluationID, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.EvaluationIDFilter, this.GetFromSession(this.EvaluationIDFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.TypeIDFilter)) {
                wc.iAND(EvaluationQuestionTable.TypeID, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.TypeIDFilter, this.GetFromSession(this.TypeIDFilter)), false, false);
            }
                  
            return wc;
        }
                          
        // Create a where clause for the filter TypeIDFilter.
        public virtual WhereClause CreateWhereClause_TypeIDFilter()
        {
              
            WhereClause wc = new WhereClause();
                  
            if (MiscUtils.IsValueSelected(this.EvaluationIDFilter)) {
                wc.iAND(EvaluationQuestionTable.EvaluationID, BaseFilter.ComparisonOperator.EqualsTo, MiscUtils.GetSelectedValue(this.EvaluationIDFilter, this.GetFromSession(this.EvaluationIDFilter)), false, false);
            }
                  
            if (MiscUtils.IsValueSelected(this.EvaluationQuestionSearchArea)) {
                WhereClause search = new WhereClause();
                    
                search.iOR(EvaluationQuestionTable.Question, BaseFilter.ComparisonOperator.Contains, MiscUtils.GetSelectedValue(this.EvaluationQuestionSearchArea, this.GetFromSession(this.EvaluationQuestionSearchArea)), true, false);
      
                wc.iAND(search);
            }
                  
            return wc;
        }
            
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            this.SaveToSession(this.EvaluationIDFilter, this.EvaluationIDFilter.SelectedValue);
            this.SaveToSession(this.EvaluationQuestionSearchArea, this.EvaluationQuestionSearchArea.Text);
            this.SaveToSession(this.TypeIDFilter, this.TypeIDFilter.SelectedValue);
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            this.RemoveFromSession(this.EvaluationIDFilter);
            this.RemoveFromSession(this.EvaluationQuestionSearchArea);
            this.RemoveFromSession(this.TypeIDFilter);
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["EvaluationQuestionTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["EvaluationQuestionTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        
        // event handler for ImageButton
        public virtual void EvaluationQuestionPagination_FirstPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex = 0;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void EvaluationQuestionPagination_LastPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.DisplayLastPage = true;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void EvaluationQuestionPagination_NextPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            this.PageIndex += 1;
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionPagination_PageSizeButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          
        // event handler for ImageButton
        public virtual void EvaluationQuestionPagination_PreviousPage_Click(object sender, ImageClickEventArgs args)
        {
            
            try {
                
            if (this.PageIndex > 0) {
                this.PageIndex -= 1;
                this.DataChanged = true;
            }
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for sorting events.
        
        // event handler for FieldSort
        public virtual void EvaluationIDLabel1_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(EvaluationQuestionTable.EvaluationID);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(EvaluationQuestionTable.EvaluationID, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          
        // event handler for FieldSort
        public virtual void QuestionLabel_Click(object sender, EventArgs args)
        {
            
            OrderByItem sd = this.CurrentSortOrder.Find(EvaluationQuestionTable.Question);
            if (sd != null) {
                sd.Reverse();
            } else {
                this.CurrentSortOrder.Reset();
                this.CurrentSortOrder.Add(EvaluationQuestionTable.Question, OrderByItem.OrderDir.Asc);
            }

            this.DataChanged = true;
              
        }
          

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void EvaluationQuestionDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(false);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionEditButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../EvaluationQuestion/EditEvaluationQuestionPage.aspx?EvaluationQuestion={PK}";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionExportButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            WhereClause wc = this.CreateWhereClause();
            String exportedData = EvaluationQuestionTable.Export(wc);
            BaseClasses.Utils.NetUtils.WriteResponseTextAttachment(this.Page.Response, "EvaluationQuestionTable.csv", exportedData);
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionNewButton_Click(object sender, EventArgs args)
        {
            
            string url = @"../EvaluationQuestion/AddEvaluationQuestionPage.aspx";
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                url = this.ModifyRedirectUrl(url, "");
                url = this.Page.ModifyRedirectUrl(url, "");
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.Response.Redirect(url);
            }
        }
          
        // event handler for Button with Layout
        public virtual void EvaluationQuestionSearchButton_Click(object sender, EventArgs args)
        {
            
            try {
                
            this.DataChanged = true;
      
            } catch (Exception ex) {
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
    
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        
        // event handler for FieldFilter
        protected virtual void EvaluationIDFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            
        // event handler for FieldFilter
        protected virtual void TypeIDFilter_SelectedIndexChanged(object sender, EventArgs args)
        {
            this.DataChanged = true;
        }
            

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private EvaluationQuestionRecord[] _DataSource = null;
        public  EvaluationQuestionRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal associated_taskLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "associated_taskLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList EvaluationIDFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationIDFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal EvaluationIDLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationIDLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton EvaluationIDLabel1 {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationIDLabel1");
            }
        }
        
        public WKCRM.UI.IThemeButton EvaluationQuestionDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionDeleteButton");
            }
        }
        
        public WKCRM.UI.IThemeButton EvaluationQuestionEditButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionEditButton");
            }
        }
        
        public WKCRM.UI.IThemeButton EvaluationQuestionExportButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionExportButton");
            }
        }
        
        public WKCRM.UI.IThemeButton EvaluationQuestionNewButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionNewButton");
            }
        }
        
        public WKCRM.UI.IPagination EvaluationQuestionPagination {
            get {
                return (WKCRM.UI.IPagination)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionPagination");
            }
        }
        
        public System.Web.UI.WebControls.TextBox EvaluationQuestionSearchArea {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionSearchArea");
            }
        }
        
        public WKCRM.UI.IThemeButton EvaluationQuestionSearchButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionSearchButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal EvaluationQuestionTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox EvaluationQuestionToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label EvaluationQuestionTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "EvaluationQuestionTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Literal OrderNumberLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OrderNumberLabel");
            }
        }
        
        public System.Web.UI.WebControls.LinkButton QuestionLabel {
            get {
                return (System.Web.UI.WebControls.LinkButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionLabel");
            }
        }
        
        public System.Web.UI.WebControls.DropDownList TypeIDFilter {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TypeIDFilter");
            }
        }
        
        public System.Web.UI.WebControls.Literal TypeIDLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TypeIDLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal TypeIDLabel1 {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "TypeIDLabel1");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                EvaluationQuestionTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                EvaluationQuestionRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (EvaluationQuestionTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.EvaluationQuestionRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public EvaluationQuestionTableControlRow GetSelectedRecordControl()
        {
        EvaluationQuestionTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public EvaluationQuestionTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (EvaluationQuestionTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.EvaluationQuestionRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (EvaluationQuestionTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowEvaluationQuestionTablePage.EvaluationQuestionTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            EvaluationQuestionTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (EvaluationQuestionTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.EvaluationQuestionRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public EvaluationQuestionTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("EvaluationQuestionTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                EvaluationQuestionTableControlRow recControl = (EvaluationQuestionTableControlRow)repItem.FindControl("EvaluationQuestionTableControlRow");
                recList.Add(recControl);
            }

            return (EvaluationQuestionTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.ShowEvaluationQuestionTablePage.EvaluationQuestionTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  