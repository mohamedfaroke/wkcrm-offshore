﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ReportsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ReportsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ReportsTable"></see> class.
/// </remarks>
/// <seealso cref="ReportsTable"></seealso>
/// <seealso cref="ReportsRecord"></seealso>
public class BaseReportsRecord : PrimaryKeyRecord
{

	public readonly static ReportsTable TableUtils = ReportsTable.Instance;

	// Constructors
 
	protected BaseReportsRecord() : base(TableUtils)
	{
	}

	protected BaseReportsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.id field.
	/// </summary>
	public void Setid0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.id field.
	/// </summary>
	public void Setid0FieldValue(string val)
	{
		this.SetString(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.id field.
	/// </summary>
	public void Setid0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.id field.
	/// </summary>
	public void Setid0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.id field.
	/// </summary>
	public void Setid0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.filename field.
	/// </summary>
	public ColumnValue GetfilenameValue()
	{
		return this.GetValue(TableUtils.filenameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.filename field.
	/// </summary>
	public string GetfilenameFieldValue()
	{
		return this.GetValue(TableUtils.filenameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.filename field.
	/// </summary>
	public void SetfilenameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.filenameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.filename field.
	/// </summary>
	public void SetfilenameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.filenameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.viewname field.
	/// </summary>
	public ColumnValue GetviewnameValue()
	{
		return this.GetValue(TableUtils.viewnameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.viewname field.
	/// </summary>
	public string GetviewnameFieldValue()
	{
		return this.GetValue(TableUtils.viewnameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.viewname field.
	/// </summary>
	public void SetviewnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.viewnameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.viewname field.
	/// </summary>
	public void SetviewnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.viewnameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.params field.
	/// </summary>
	public ColumnValue Getparams0Value()
	{
		return this.GetValue(TableUtils.params0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.params field.
	/// </summary>
	public string Getparams0FieldValue()
	{
		return this.GetValue(TableUtils.params0Column).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.params field.
	/// </summary>
	public void Setparams0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.params0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.params field.
	/// </summary>
	public void Setparams0FieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.params0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.description field.
	/// </summary>
	public ColumnValue GetdescriptionValue()
	{
		return this.GetValue(TableUtils.descriptionColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.description field.
	/// </summary>
	public string GetdescriptionFieldValue()
	{
		return this.GetValue(TableUtils.descriptionColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.description field.
	/// </summary>
	public void SetdescriptionFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.descriptionColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.description field.
	/// </summary>
	public void SetdescriptionFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.descriptionColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.displayOrder field.
	/// </summary>
	public ColumnValue GetdisplayOrderValue()
	{
		return this.GetValue(TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.displayOrder field.
	/// </summary>
	public Int32 GetdisplayOrderFieldValue()
	{
		return this.GetValue(TableUtils.displayOrderColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(string val)
	{
		this.SetString(val, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.status field.
	/// </summary>
	public ColumnValue GetstatusValue()
	{
		return this.GetValue(TableUtils.statusColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.status field.
	/// </summary>
	public bool GetstatusFieldValue()
	{
		return this.GetValue(TableUtils.statusColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.status field.
	/// </summary>
	public void SetstatusFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.statusColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.status field.
	/// </summary>
	public void SetstatusFieldValue(string val)
	{
		this.SetString(val, TableUtils.statusColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.status field.
	/// </summary>
	public void SetstatusFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.statusColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.requiredRole field.
	/// </summary>
	public ColumnValue GetrequiredRoleValue()
	{
		return this.GetValue(TableUtils.requiredRoleColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Reports_.requiredRole field.
	/// </summary>
	public Int32 GetrequiredRoleFieldValue()
	{
		return this.GetValue(TableUtils.requiredRoleColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.requiredRole field.
	/// </summary>
	public void SetrequiredRoleFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.requiredRoleColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.requiredRole field.
	/// </summary>
	public void SetrequiredRoleFieldValue(string val)
	{
		this.SetString(val, TableUtils.requiredRoleColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.requiredRole field.
	/// </summary>
	public void SetrequiredRoleFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.requiredRoleColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.requiredRole field.
	/// </summary>
	public void SetrequiredRoleFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.requiredRoleColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.requiredRole field.
	/// </summary>
	public void SetrequiredRoleFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.requiredRoleColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.filename field.
	/// </summary>
	public string filename
	{
		get
		{
			return this.GetValue(TableUtils.filenameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.filenameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool filenameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.filenameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.filename field.
	/// </summary>
	public string filenameDefault
	{
		get
		{
			return TableUtils.filenameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.viewname field.
	/// </summary>
	public string viewname
	{
		get
		{
			return this.GetValue(TableUtils.viewnameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.viewnameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool viewnameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.viewnameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.viewname field.
	/// </summary>
	public string viewnameDefault
	{
		get
		{
			return TableUtils.viewnameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.params field.
	/// </summary>
	public string params0
	{
		get
		{
			return this.GetValue(TableUtils.params0Column).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.params0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool params0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.params0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.params field.
	/// </summary>
	public string params0Default
	{
		get
		{
			return TableUtils.params0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.description field.
	/// </summary>
	public string description
	{
		get
		{
			return this.GetValue(TableUtils.descriptionColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.descriptionColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool descriptionSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.descriptionColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.description field.
	/// </summary>
	public string descriptionDefault
	{
		get
		{
			return TableUtils.descriptionColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.displayOrder field.
	/// </summary>
	public Int32 displayOrder
	{
		get
		{
			return this.GetValue(TableUtils.displayOrderColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.displayOrderColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool displayOrderSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.displayOrderColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.displayOrder field.
	/// </summary>
	public string displayOrderDefault
	{
		get
		{
			return TableUtils.displayOrderColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.status field.
	/// </summary>
	public bool status
	{
		get
		{
			return this.GetValue(TableUtils.statusColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.statusColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool statusSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.statusColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.status field.
	/// </summary>
	public string statusDefault
	{
		get
		{
			return TableUtils.statusColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Reports_.requiredRole field.
	/// </summary>
	public Int32 requiredRole
	{
		get
		{
			return this.GetValue(TableUtils.requiredRoleColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.requiredRoleColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool requiredRoleSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.requiredRoleColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Reports_.requiredRole field.
	/// </summary>
	public string requiredRoleDefault
	{
		get
		{
			return TableUtils.requiredRoleColumn.DefaultValue;
		}
	}


#endregion
}

}
