﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_AccountContractProjectRecord.vb

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_AccountContractProjectRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_AccountContractProjectView"></see> class.
/// </remarks>
/// <seealso cref="View_AccountContractProjectView"></seealso>
/// <seealso cref="View_AccountContractProjectRecord"></seealso>
public class BaseView_AccountContractProjectRecord : KeylessRecord
{

	public readonly static View_AccountContractProjectView TableUtils = View_AccountContractProjectView.Instance;

	// Constructors
 
	protected BaseView_AccountContractProjectRecord() : base(TableUtils)
	{
	}

	protected BaseView_AccountContractProjectRecord(KeylessRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public ColumnValue GetAccIDValue()
	{
		return this.GetValue(TableUtils.AccIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public Decimal GetAccIDFieldValue()
	{
		return this.GetValue(TableUtils.AccIDColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public void SetAccIDFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.AccIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public void SetAccIDFieldValue(string val)
	{
		this.SetString(val, TableUtils.AccIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public void SetAccIDFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public void SetAccIDFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public void SetAccIDFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccIDColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public ColumnValue GetContractIDValue()
	{
		return this.GetValue(TableUtils.ContractIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public Decimal GetContractIDFieldValue()
	{
		return this.GetValue(TableUtils.ContractIDColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public void SetContractIDFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.ContractIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public void SetContractIDFieldValue(string val)
	{
		this.SetString(val, TableUtils.ContractIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public void SetContractIDFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ContractIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public void SetContractIDFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ContractIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public void SetContractIDFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ContractIDColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public ColumnValue GetProjIDValue()
	{
		return this.GetValue(TableUtils.ProjIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public Decimal GetProjIDFieldValue()
	{
		return this.GetValue(TableUtils.ProjIDColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public void SetProjIDFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.ProjIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public void SetProjIDFieldValue(string val)
	{
		this.SetString(val, TableUtils.ProjIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public void SetProjIDFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ProjIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public void SetProjIDFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ProjIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public void SetProjIDFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ProjIDColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public ColumnValue GetPaymentScheduleIDValue()
	{
		return this.GetValue(TableUtils.PaymentScheduleIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public Decimal GetPaymentScheduleIDFieldValue()
	{
		return this.GetValue(TableUtils.PaymentScheduleIDColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public void SetPaymentScheduleIDFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.PaymentScheduleIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public void SetPaymentScheduleIDFieldValue(string val)
	{
		this.SetString(val, TableUtils.PaymentScheduleIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public void SetPaymentScheduleIDFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.PaymentScheduleIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public void SetPaymentScheduleIDFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.PaymentScheduleIDColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public void SetPaymentScheduleIDFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.PaymentScheduleIDColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public Decimal AccID
	{
		get
		{
			return this.GetValue(TableUtils.AccIDColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.AccIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool AccIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.AccIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.AccID field.
	/// </summary>
	public string AccIDDefault
	{
		get
		{
			return TableUtils.AccIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public Decimal ContractID
	{
		get
		{
			return this.GetValue(TableUtils.ContractIDColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.ContractIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool ContractIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.ContractIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ContractID field.
	/// </summary>
	public string ContractIDDefault
	{
		get
		{
			return TableUtils.ContractIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public Decimal ProjID
	{
		get
		{
			return this.GetValue(TableUtils.ProjIDColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.ProjIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool ProjIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.ProjIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.ProjID field.
	/// </summary>
	public string ProjIDDefault
	{
		get
		{
			return TableUtils.ProjIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public Decimal PaymentScheduleID
	{
		get
		{
			return this.GetValue(TableUtils.PaymentScheduleIDColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.PaymentScheduleIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool PaymentScheduleIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.PaymentScheduleIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountContractProject_.PaymentScheduleID field.
	/// </summary>
	public string PaymentScheduleIDDefault
	{
		get
		{
			return TableUtils.PaymentScheduleIDColumn.DefaultValue;
		}
	}


#endregion

}

}
