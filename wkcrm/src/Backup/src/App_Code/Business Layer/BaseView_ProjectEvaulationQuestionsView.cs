﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ProjectEvaulationQuestionsView.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ProjectEvaulationQuestionsView"></see> class.
/// Provides access to the schema information and record data of a database table or view named View_ProjectEvaulationQuestions.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="View_ProjectEvaulationQuestionsView.Instance">View_ProjectEvaulationQuestionsView.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="View_ProjectEvaulationQuestionsView"></seealso>
[SerializableAttribute()]
public class BaseView_ProjectEvaulationQuestionsView : PrimaryKeyTable
{

    private readonly string TableDefinitionString = View_ProjectEvaulationQuestionsDefinition.GetXMLString();







    protected BaseView_ProjectEvaulationQuestionsView()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.View_ProjectEvaulationQuestionsView");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.View_ProjectEvaulationQuestionsRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new View_ProjectEvaulationQuestionsSqlView();
        ((View_ProjectEvaulationQuestionsSqlView)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.ProjectEvaluationId column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn ProjectEvaluationIdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.ProjectEvaluationId column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn ProjectEvaluationId
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.ProjectEvaluationIdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.QuestionId column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn QuestionIdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.QuestionId column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn QuestionId
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.QuestionIdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.OptionId column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn OptionIdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.OptionId column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn OptionId
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.OptionIdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.Answer column object.
    /// </summary>
    public BaseClasses.Data.StringColumn AnswerColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.Answer column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn Answer
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.AnswerColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.designerId column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn designerIdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.designerId column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn designerId
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.designerIdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.tradesmanId column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn tradesmanIdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.tradesmanId column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn tradesmanId
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.tradesmanIdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.TypeID column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn TypeIDColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.TypeID column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn TypeID
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.TypeIDColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.OrderNumber column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn OrderNumberColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[7];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ProjectEvaulationQuestions_.OrderNumber column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn OrderNumber
    {
        get
        {
            return View_ProjectEvaulationQuestionsView.Instance.OrderNumberColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of View_ProjectEvaulationQuestionsRecord records using a where clause.
    /// </summary>
    public static View_ProjectEvaulationQuestionsRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of View_ProjectEvaulationQuestionsRecord records using a where and order by clause.
    /// </summary>
    public static View_ProjectEvaulationQuestionsRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of View_ProjectEvaulationQuestionsRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static View_ProjectEvaulationQuestionsRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = View_ProjectEvaulationQuestionsView.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (View_ProjectEvaulationQuestionsRecord[])recList.ToArray(Type.GetType("WKCRM.Business.View_ProjectEvaulationQuestionsRecord"));
    }   
    
    public static View_ProjectEvaulationQuestionsRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = View_ProjectEvaulationQuestionsView.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (View_ProjectEvaulationQuestionsRecord[])recList.ToArray(Type.GetType("WKCRM.Business.View_ProjectEvaulationQuestionsRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)View_ProjectEvaulationQuestionsView.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)View_ProjectEvaulationQuestionsView.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a View_ProjectEvaulationQuestionsRecord record using a where clause.
    /// </summary>
    public static View_ProjectEvaulationQuestionsRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a View_ProjectEvaulationQuestionsRecord record using a where and order by clause.
    /// </summary>
    public static View_ProjectEvaulationQuestionsRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = View_ProjectEvaulationQuestionsView.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        View_ProjectEvaulationQuestionsRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (View_ProjectEvaulationQuestionsRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return View_ProjectEvaulationQuestionsView.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        View_ProjectEvaulationQuestionsRecord[] recs = GetRecords(where);
        return  View_ProjectEvaulationQuestionsView.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        View_ProjectEvaulationQuestionsRecord[] recs = GetRecords(where, orderBy);
        return  View_ProjectEvaulationQuestionsView.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        View_ProjectEvaulationQuestionsRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  View_ProjectEvaulationQuestionsView.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        View_ProjectEvaulationQuestionsView.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  View_ProjectEvaulationQuestionsView.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return View_ProjectEvaulationQuestionsView.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return View_ProjectEvaulationQuestionsView.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return View_ProjectEvaulationQuestionsView.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return View_ProjectEvaulationQuestionsView.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return View_ProjectEvaulationQuestionsView.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return View_ProjectEvaulationQuestionsView.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return View_ProjectEvaulationQuestionsView.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = View_ProjectEvaulationQuestionsView.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static View_ProjectEvaulationQuestionsRecord GetRecord(string id, bool bMutable)
        {
            return (View_ProjectEvaulationQuestionsRecord)View_ProjectEvaulationQuestionsView.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static View_ProjectEvaulationQuestionsRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (View_ProjectEvaulationQuestionsRecord)View_ProjectEvaulationQuestionsView.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string ProjectEvaluationIdValue, 
        string QuestionIdValue, 
        string OptionIdValue, 
        string AnswerValue, 
        string designerIdValue, 
        string tradesmanIdValue, 
        string TypeIDValue, 
        string OrderNumberValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(ProjectEvaluationIdValue, ProjectEvaluationIdColumn);
        rec.SetString(QuestionIdValue, QuestionIdColumn);
        rec.SetString(OptionIdValue, OptionIdColumn);
        rec.SetString(AnswerValue, AnswerColumn);
        rec.SetString(designerIdValue, designerIdColumn);
        rec.SetString(tradesmanIdValue, tradesmanIdColumn);
        rec.SetString(TypeIDValue, TypeIDColumn);
        rec.SetString(OrderNumberValue, OrderNumberColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			View_ProjectEvaulationQuestionsView.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				View_ProjectEvaulationQuestionsView.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(View_ProjectEvaulationQuestionsView.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return View_ProjectEvaulationQuestionsView.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(View_ProjectEvaulationQuestionsView.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = View_ProjectEvaulationQuestionsView.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = View_ProjectEvaulationQuestionsView.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (View_ProjectEvaulationQuestionsView.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = View_ProjectEvaulationQuestionsView.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
