﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractPaymentTypeRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractPaymentTypeRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractPaymentTypeTable"></see> class.
/// </remarks>
/// <seealso cref="ContractPaymentTypeTable"></seealso>
/// <seealso cref="ContractPaymentTypeRecord"></seealso>
public class BaseContractPaymentTypeRecord : PrimaryKeyRecord
{

	public readonly static ContractPaymentTypeTable TableUtils = ContractPaymentTypeTable.Instance;

	// Constructors
 
	protected BaseContractPaymentTypeRecord() : base(TableUtils)
	{
	}

	protected BaseContractPaymentTypeRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public ColumnValue Getint0Value()
	{
		return this.GetValue(TableUtils.int0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public Int32 Getint0FieldValue()
	{
		return this.GetValue(TableUtils.int0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public void Setint0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.int0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public void Setint0FieldValue(string val)
	{
		this.SetString(val, TableUtils.int0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public void Setint0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.int0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public void Setint0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.int0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public void Setint0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.int0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractPaymentType_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractPaymentType_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractPaymentType_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractPaymentType_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public Int32 int0
	{
		get
		{
			return this.GetValue(TableUtils.int0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.int0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool int0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.int0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.int field.
	/// </summary>
	public string int0Default
	{
		get
		{
			return TableUtils.int0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractPaymentType_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractPaymentType_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractPaymentType_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
