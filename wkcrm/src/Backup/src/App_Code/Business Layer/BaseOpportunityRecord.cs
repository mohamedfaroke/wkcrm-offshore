﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in OpportunityRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="OpportunityRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="OpportunityTable"></see> class.
/// </remarks>
/// <seealso cref="OpportunityTable"></seealso>
/// <seealso cref="OpportunityRecord"></seealso>
public class BaseOpportunityRecord : PrimaryKeyRecord
{

	public readonly static OpportunityTable TableUtils = OpportunityTable.Instance;

	// Constructors
 
	protected BaseOpportunityRecord() : base(TableUtils)
	{
	}

	protected BaseOpportunityRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public ColumnValue Getemployee_idValue()
	{
		return this.GetValue(TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public Decimal Getemployee_idFieldValue()
	{
		return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.datetime field.
	/// </summary>
	public ColumnValue Getdatetime0Value()
	{
		return this.GetValue(TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.datetime field.
	/// </summary>
	public DateTime Getdatetime0FieldValue()
	{
		return this.GetValue(TableUtils.datetime0Column).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(string val)
	{
		this.SetString(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.datetime0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.location_id field.
	/// </summary>
	public ColumnValue Getlocation_idValue()
	{
		return this.GetValue(TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.location_id field.
	/// </summary>
	public Int32 Getlocation_idFieldValue()
	{
		return this.GetValue(TableUtils.location_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public ColumnValue Getcontact_source_idValue()
	{
		return this.GetValue(TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public Int32 Getcontact_source_idFieldValue()
	{
		return this.GetValue(TableUtils.contact_source_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contact_source_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public ColumnValue Getopportunity_source_idValue()
	{
		return this.GetValue(TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public Int32 Getopportunity_source_idFieldValue()
	{
		return this.GetValue(TableUtils.opportunity_source_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_source_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Opportunity_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public Decimal employee_id
	{
		get
		{
			return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.employee_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool employee_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.employee_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.employee_id field.
	/// </summary>
	public string employee_idDefault
	{
		get
		{
			return TableUtils.employee_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Opportunity_.datetime field.
	/// </summary>
	public DateTime datetime0
	{
		get
		{
			return this.GetValue(TableUtils.datetime0Column).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.datetime0Column);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool datetime0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.datetime0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.datetime field.
	/// </summary>
	public string datetime0Default
	{
		get
		{
			return TableUtils.datetime0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Opportunity_.location_id field.
	/// </summary>
	public Int32 location_id
	{
		get
		{
			return this.GetValue(TableUtils.location_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.location_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool location_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.location_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.location_id field.
	/// </summary>
	public string location_idDefault
	{
		get
		{
			return TableUtils.location_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public Int32 contact_source_id
	{
		get
		{
			return this.GetValue(TableUtils.contact_source_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contact_source_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contact_source_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contact_source_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.contact_source_id field.
	/// </summary>
	public string contact_source_idDefault
	{
		get
		{
			return TableUtils.contact_source_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public Int32 opportunity_source_id
	{
		get
		{
			return this.GetValue(TableUtils.opportunity_source_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.opportunity_source_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool opportunity_source_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.opportunity_source_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Opportunity_.opportunity_source_id field.
	/// </summary>
	public string opportunity_source_idDefault
	{
		get
		{
			return TableUtils.opportunity_source_idColumn.DefaultValue;
		}
	}


#endregion
}

}
