﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in OpportunitySourceRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="OpportunitySourceRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="OpportunitySourceTable"></see> class.
/// </remarks>
/// <seealso cref="OpportunitySourceTable"></seealso>
/// <seealso cref="OpportunitySourceRecord"></seealso>
public class BaseOpportunitySourceRecord : PrimaryKeyRecord
{

	public readonly static OpportunitySourceTable TableUtils = OpportunitySourceTable.Instance;

	// Constructors
 
	protected BaseOpportunitySourceRecord() : base(TableUtils)
	{
	}

	protected BaseOpportunitySourceRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public ColumnValue Getopportunity_category_idValue()
	{
		return this.GetValue(TableUtils.opportunity_category_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public Int32 Getopportunity_category_idFieldValue()
	{
		return this.GetValue(TableUtils.opportunity_category_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public void Setopportunity_category_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.opportunity_category_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public void Setopportunity_category_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.opportunity_category_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public void Setopportunity_category_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_category_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public void Setopportunity_category_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_category_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public void Setopportunity_category_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_category_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public ColumnValue Getdisplay_orderValue()
	{
		return this.GetValue(TableUtils.display_orderColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public Int32 Getdisplay_orderFieldValue()
	{
		return this.GetValue(TableUtils.display_orderColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public void Setdisplay_orderFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.display_orderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public void Setdisplay_orderFieldValue(string val)
	{
		this.SetString(val, TableUtils.display_orderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public void Setdisplay_orderFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.display_orderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public void Setdisplay_orderFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.display_orderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public void Setdisplay_orderFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.display_orderColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's OpportunitySource_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's OpportunitySource_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's OpportunitySource_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public Int32 opportunity_category_id
	{
		get
		{
			return this.GetValue(TableUtils.opportunity_category_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.opportunity_category_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool opportunity_category_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.opportunity_category_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.opportunity_category_id field.
	/// </summary>
	public string opportunity_category_idDefault
	{
		get
		{
			return TableUtils.opportunity_category_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public Int32 display_order
	{
		get
		{
			return this.GetValue(TableUtils.display_orderColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.display_orderColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool display_orderSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.display_orderColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.display_order field.
	/// </summary>
	public string display_orderDefault
	{
		get
		{
			return TableUtils.display_orderColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's OpportunitySource_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's OpportunitySource_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
