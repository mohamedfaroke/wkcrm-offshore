﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentStagesRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="PaymentStagesRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentStagesTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentStagesTable"></seealso>
/// <seealso cref="PaymentStagesRecord"></seealso>
public class BasePaymentStagesRecord : PrimaryKeyRecord
{

	public readonly static PaymentStagesTable TableUtils = PaymentStagesTable.Instance;

	// Constructors
 
	protected BasePaymentStagesRecord() : base(TableUtils)
	{
	}

	protected BasePaymentStagesRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public ColumnValue GetpercentageValue()
	{
		return this.GetValue(TableUtils.percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public Decimal GetpercentageFieldValue()
	{
		return this.GetValue(TableUtils.percentageColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public void SetpercentageFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public void SetpercentageFieldValue(string val)
	{
		this.SetString(val, TableUtils.percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public void SetpercentageFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public void SetpercentageFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public void SetpercentageFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.percentageColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public ColumnValue GetthresholdValue()
	{
		return this.GetValue(TableUtils.thresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public Decimal GetthresholdFieldValue()
	{
		return this.GetValue(TableUtils.thresholdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public void SetthresholdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.thresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public void SetthresholdFieldValue(string val)
	{
		this.SetString(val, TableUtils.thresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public void SetthresholdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.thresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public void SetthresholdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.thresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public void SetthresholdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.thresholdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public ColumnValue Getalt_percentageValue()
	{
		return this.GetValue(TableUtils.alt_percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public Decimal Getalt_percentageFieldValue()
	{
		return this.GetValue(TableUtils.alt_percentageColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public void Setalt_percentageFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.alt_percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public void Setalt_percentageFieldValue(string val)
	{
		this.SetString(val, TableUtils.alt_percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public void Setalt_percentageFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.alt_percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public void Setalt_percentageFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.alt_percentageColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public void Setalt_percentageFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.alt_percentageColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public ColumnValue GetdisplayOrderValue()
	{
		return this.GetValue(TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public Int32 GetdisplayOrderFieldValue()
	{
		return this.GetValue(TableUtils.displayOrderColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(string val)
	{
		this.SetString(val, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public void SetdisplayOrderFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.displayOrderColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentStages_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentStages_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public Decimal percentage
	{
		get
		{
			return this.GetValue(TableUtils.percentageColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.percentageColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool percentageSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.percentageColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.percentage field.
	/// </summary>
	public string percentageDefault
	{
		get
		{
			return TableUtils.percentageColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public Decimal threshold
	{
		get
		{
			return this.GetValue(TableUtils.thresholdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.thresholdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool thresholdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.thresholdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.threshold field.
	/// </summary>
	public string thresholdDefault
	{
		get
		{
			return TableUtils.thresholdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public Decimal alt_percentage
	{
		get
		{
			return this.GetValue(TableUtils.alt_percentageColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.alt_percentageColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool alt_percentageSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.alt_percentageColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.alt_percentage field.
	/// </summary>
	public string alt_percentageDefault
	{
		get
		{
			return TableUtils.alt_percentageColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentStages_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public Int32 displayOrder
	{
		get
		{
			return this.GetValue(TableUtils.displayOrderColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.displayOrderColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool displayOrderSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.displayOrderColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentStages_.displayOrder field.
	/// </summary>
	public string displayOrderDefault
	{
		get
		{
			return TableUtils.displayOrderColumn.DefaultValue;
		}
	}


#endregion
}

}
