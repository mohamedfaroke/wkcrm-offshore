﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentVariationsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="PaymentVariationsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentVariationsTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentVariationsTable"></seealso>
/// <seealso cref="PaymentVariationsRecord"></seealso>
public class BasePaymentVariationsRecord : PrimaryKeyRecord
{

	public readonly static PaymentVariationsTable TableUtils = PaymentVariationsTable.Instance;

	// Constructors
 
	protected BasePaymentVariationsRecord() : base(TableUtils)
	{
	}

	protected BasePaymentVariationsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public ColumnValue Getemployee_idValue()
	{
		return this.GetValue(TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public Decimal Getemployee_idFieldValue()
	{
		return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.datetime field.
	/// </summary>
	public ColumnValue Getdatetime0Value()
	{
		return this.GetValue(TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.datetime field.
	/// </summary>
	public DateTime Getdatetime0FieldValue()
	{
		return this.GetValue(TableUtils.datetime0Column).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(string val)
	{
		this.SetString(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.datetime0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public ColumnValue GetamountValue()
	{
		return this.GetValue(TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public Decimal GetamountFieldValue()
	{
		return this.GetValue(TableUtils.amountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public void SetamountFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public void SetamountFieldValue(string val)
	{
		this.SetString(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public void SetamountFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public void SetamountFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public void SetamountFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public ColumnValue Getpayment_stage_idValue()
	{
		return this.GetValue(TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public Int32 Getpayment_stage_idFieldValue()
	{
		return this.GetValue(TableUtils.payment_stage_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public ColumnValue Getpayment_schedule_idValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public Decimal Getpayment_schedule_idFieldValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentVariations_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentVariations_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public Decimal employee_id
	{
		get
		{
			return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.employee_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool employee_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.employee_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.employee_id field.
	/// </summary>
	public string employee_idDefault
	{
		get
		{
			return TableUtils.employee_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentVariations_.datetime field.
	/// </summary>
	public DateTime datetime0
	{
		get
		{
			return this.GetValue(TableUtils.datetime0Column).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.datetime0Column);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool datetime0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.datetime0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.datetime field.
	/// </summary>
	public string datetime0Default
	{
		get
		{
			return TableUtils.datetime0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public Decimal amount
	{
		get
		{
			return this.GetValue(TableUtils.amountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.amountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool amountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.amountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.amount field.
	/// </summary>
	public string amountDefault
	{
		get
		{
			return TableUtils.amountColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public Int32 payment_stage_id
	{
		get
		{
			return this.GetValue(TableUtils.payment_stage_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.payment_stage_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool payment_stage_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.payment_stage_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_stage_id field.
	/// </summary>
	public string payment_stage_idDefault
	{
		get
		{
			return TableUtils.payment_stage_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public Decimal payment_schedule_id
	{
		get
		{
			return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.payment_schedule_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool payment_schedule_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.payment_schedule_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.payment_schedule_id field.
	/// </summary>
	public string payment_schedule_idDefault
	{
		get
		{
			return TableUtils.payment_schedule_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentVariations_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentVariations_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}


#endregion
}

}
