﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in PaymentScheduleStagesRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="PaymentScheduleStagesRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="PaymentScheduleStagesTable"></see> class.
/// </remarks>
/// <seealso cref="PaymentScheduleStagesTable"></seealso>
/// <seealso cref="PaymentScheduleStagesRecord"></seealso>
public class BasePaymentScheduleStagesRecord : PrimaryKeyRecord
{

	public readonly static PaymentScheduleStagesTable TableUtils = PaymentScheduleStagesTable.Instance;

	// Constructors
 
	protected BasePaymentScheduleStagesRecord() : base(TableUtils)
	{
	}

	protected BasePaymentScheduleStagesRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public ColumnValue Getpayment_schedule_idValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public Decimal Getpayment_schedule_idFieldValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public ColumnValue Getpayment_stage_idValue()
	{
		return this.GetValue(TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public Int32 Getpayment_stage_idFieldValue()
	{
		return this.GetValue(TableUtils.payment_stage_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public void Setpayment_stage_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_stage_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public ColumnValue GetamountValue()
	{
		return this.GetValue(TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public Decimal GetamountFieldValue()
	{
		return this.GetValue(TableUtils.amountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public void SetamountFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public void SetamountFieldValue(string val)
	{
		this.SetString(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public void SetamountFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public void SetamountFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public void SetamountFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public Decimal payment_schedule_id
	{
		get
		{
			return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.payment_schedule_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool payment_schedule_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.payment_schedule_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_schedule_id field.
	/// </summary>
	public string payment_schedule_idDefault
	{
		get
		{
			return TableUtils.payment_schedule_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public Int32 payment_stage_id
	{
		get
		{
			return this.GetValue(TableUtils.payment_stage_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.payment_stage_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool payment_stage_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.payment_stage_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.payment_stage_id field.
	/// </summary>
	public string payment_stage_idDefault
	{
		get
		{
			return TableUtils.payment_stage_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public Decimal amount
	{
		get
		{
			return this.GetValue(TableUtils.amountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.amountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool amountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.amountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's PaymentScheduleStages_.amount field.
	/// </summary>
	public string amountDefault
	{
		get
		{
			return TableUtils.amountColumn.DefaultValue;
		}
	}


#endregion
}

}
