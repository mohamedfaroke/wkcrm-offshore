﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in CrmOptionsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="CrmOptionsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="CrmOptionsTable"></see> class.
/// </remarks>
/// <seealso cref="CrmOptionsTable"></seealso>
/// <seealso cref="CrmOptionsRecord"></seealso>
public class BaseCrmOptionsRecord : PrimaryKeyRecord
{

	public readonly static CrmOptionsTable TableUtils = CrmOptionsTable.Instance;

	// Constructors
 
	protected BaseCrmOptionsRecord() : base(TableUtils)
	{
	}

	protected BaseCrmOptionsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public ColumnValue GetdoorQtyThresholdValue()
	{
		return this.GetValue(TableUtils.doorQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public Decimal GetdoorQtyThresholdFieldValue()
	{
		return this.GetValue(TableUtils.doorQtyThresholdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public void SetdoorQtyThresholdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.doorQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public void SetdoorQtyThresholdFieldValue(string val)
	{
		this.SetString(val, TableUtils.doorQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public void SetdoorQtyThresholdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.doorQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public void SetdoorQtyThresholdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.doorQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public void SetdoorQtyThresholdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.doorQtyThresholdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public ColumnValue GetdoorQtyExtraCostValue()
	{
		return this.GetValue(TableUtils.doorQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public Decimal GetdoorQtyExtraCostFieldValue()
	{
		return this.GetValue(TableUtils.doorQtyExtraCostColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public void SetdoorQtyExtraCostFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.doorQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public void SetdoorQtyExtraCostFieldValue(string val)
	{
		this.SetString(val, TableUtils.doorQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public void SetdoorQtyExtraCostFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.doorQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public void SetdoorQtyExtraCostFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.doorQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public void SetdoorQtyExtraCostFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.doorQtyExtraCostColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public ColumnValue GetdifficultDeliveryFeeValue()
	{
		return this.GetValue(TableUtils.difficultDeliveryFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public Decimal GetdifficultDeliveryFeeFieldValue()
	{
		return this.GetValue(TableUtils.difficultDeliveryFeeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public void SetdifficultDeliveryFeeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.difficultDeliveryFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public void SetdifficultDeliveryFeeFieldValue(string val)
	{
		this.SetString(val, TableUtils.difficultDeliveryFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public void SetdifficultDeliveryFeeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.difficultDeliveryFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public void SetdifficultDeliveryFeeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.difficultDeliveryFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public void SetdifficultDeliveryFeeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.difficultDeliveryFeeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public ColumnValue GetsecondCheckMeasureFeeValue()
	{
		return this.GetValue(TableUtils.secondCheckMeasureFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public Decimal GetsecondCheckMeasureFeeFieldValue()
	{
		return this.GetValue(TableUtils.secondCheckMeasureFeeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public void SetsecondCheckMeasureFeeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.secondCheckMeasureFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public void SetsecondCheckMeasureFeeFieldValue(string val)
	{
		this.SetString(val, TableUtils.secondCheckMeasureFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public void SetsecondCheckMeasureFeeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.secondCheckMeasureFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public void SetsecondCheckMeasureFeeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.secondCheckMeasureFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public void SetsecondCheckMeasureFeeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.secondCheckMeasureFeeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.GST field.
	/// </summary>
	public ColumnValue GetGSTValue()
	{
		return this.GetValue(TableUtils.GSTColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.GST field.
	/// </summary>
	public Decimal GetGSTFieldValue()
	{
		return this.GetValue(TableUtils.GSTColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.GST field.
	/// </summary>
	public void SetGSTFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.GSTColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.GST field.
	/// </summary>
	public void SetGSTFieldValue(string val)
	{
		this.SetString(val, TableUtils.GSTColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.GST field.
	/// </summary>
	public void SetGSTFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.GSTColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.GST field.
	/// </summary>
	public void SetGSTFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.GSTColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.GST field.
	/// </summary>
	public void SetGSTFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.GSTColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public ColumnValue GetinsuranceThresholdValue()
	{
		return this.GetValue(TableUtils.insuranceThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public Decimal GetinsuranceThresholdFieldValue()
	{
		return this.GetValue(TableUtils.insuranceThresholdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public void SetinsuranceThresholdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.insuranceThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public void SetinsuranceThresholdFieldValue(string val)
	{
		this.SetString(val, TableUtils.insuranceThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public void SetinsuranceThresholdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.insuranceThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public void SetinsuranceThresholdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.insuranceThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public void SetinsuranceThresholdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.insuranceThresholdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public ColumnValue GetinsuranceFeeValue()
	{
		return this.GetValue(TableUtils.insuranceFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public Decimal GetinsuranceFeeFieldValue()
	{
		return this.GetValue(TableUtils.insuranceFeeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public void SetinsuranceFeeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.insuranceFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public void SetinsuranceFeeFieldValue(string val)
	{
		this.SetString(val, TableUtils.insuranceFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public void SetinsuranceFeeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.insuranceFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public void SetinsuranceFeeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.insuranceFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public void SetinsuranceFeeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.insuranceFeeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public ColumnValue GetbenchtopQtyThresholdValue()
	{
		return this.GetValue(TableUtils.benchtopQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public Decimal GetbenchtopQtyThresholdFieldValue()
	{
		return this.GetValue(TableUtils.benchtopQtyThresholdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public void SetbenchtopQtyThresholdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.benchtopQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public void SetbenchtopQtyThresholdFieldValue(string val)
	{
		this.SetString(val, TableUtils.benchtopQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public void SetbenchtopQtyThresholdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtopQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public void SetbenchtopQtyThresholdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtopQtyThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public void SetbenchtopQtyThresholdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtopQtyThresholdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public ColumnValue GetbenchtopQtyExtraCostValue()
	{
		return this.GetValue(TableUtils.benchtopQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public Decimal GetbenchtopQtyExtraCostFieldValue()
	{
		return this.GetValue(TableUtils.benchtopQtyExtraCostColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public void SetbenchtopQtyExtraCostFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.benchtopQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public void SetbenchtopQtyExtraCostFieldValue(string val)
	{
		this.SetString(val, TableUtils.benchtopQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public void SetbenchtopQtyExtraCostFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtopQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public void SetbenchtopQtyExtraCostFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtopQtyExtraCostColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public void SetbenchtopQtyExtraCostFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtopQtyExtraCostColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public ColumnValue GetdualFinishesValue()
	{
		return this.GetValue(TableUtils.dualFinishesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public Decimal GetdualFinishesFieldValue()
	{
		return this.GetValue(TableUtils.dualFinishesColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public void SetdualFinishesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.dualFinishesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public void SetdualFinishesFieldValue(string val)
	{
		this.SetString(val, TableUtils.dualFinishesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public void SetdualFinishesFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public void SetdualFinishesFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public void SetdualFinishesFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishesColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public ColumnValue GetdualFinishLessFeeValue()
	{
		return this.GetValue(TableUtils.dualFinishLessFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public Decimal GetdualFinishLessFeeFieldValue()
	{
		return this.GetValue(TableUtils.dualFinishLessFeeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public void SetdualFinishLessFeeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.dualFinishLessFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public void SetdualFinishLessFeeFieldValue(string val)
	{
		this.SetString(val, TableUtils.dualFinishLessFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public void SetdualFinishLessFeeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishLessFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public void SetdualFinishLessFeeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishLessFeeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public void SetdualFinishLessFeeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishLessFeeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public ColumnValue GetdualFinishLessFeeThresholdValue()
	{
		return this.GetValue(TableUtils.dualFinishLessFeeThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public Decimal GetdualFinishLessFeeThresholdFieldValue()
	{
		return this.GetValue(TableUtils.dualFinishLessFeeThresholdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public void SetdualFinishLessFeeThresholdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.dualFinishLessFeeThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public void SetdualFinishLessFeeThresholdFieldValue(string val)
	{
		this.SetString(val, TableUtils.dualFinishLessFeeThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public void SetdualFinishLessFeeThresholdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishLessFeeThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public void SetdualFinishLessFeeThresholdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishLessFeeThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public void SetdualFinishLessFeeThresholdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dualFinishLessFeeThresholdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public ColumnValue GethighRiseValue()
	{
		return this.GetValue(TableUtils.highRiseColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public Decimal GethighRiseFieldValue()
	{
		return this.GetValue(TableUtils.highRiseColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public void SethighRiseFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.highRiseColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public void SethighRiseFieldValue(string val)
	{
		this.SetString(val, TableUtils.highRiseColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public void SethighRiseFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.highRiseColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public void SethighRiseFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.highRiseColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public void SethighRiseFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.highRiseColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public ColumnValue GetkickboardsAfterTimberValue()
	{
		return this.GetValue(TableUtils.kickboardsAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public Decimal GetkickboardsAfterTimberFieldValue()
	{
		return this.GetValue(TableUtils.kickboardsAfterTimberColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public void SetkickboardsAfterTimberFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.kickboardsAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public void SetkickboardsAfterTimberFieldValue(string val)
	{
		this.SetString(val, TableUtils.kickboardsAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public void SetkickboardsAfterTimberFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public void SetkickboardsAfterTimberFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public void SetkickboardsAfterTimberFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsAfterTimberColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public ColumnValue GetkickboardsWasherAfterTimberValue()
	{
		return this.GetValue(TableUtils.kickboardsWasherAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public Decimal GetkickboardsWasherAfterTimberFieldValue()
	{
		return this.GetValue(TableUtils.kickboardsWasherAfterTimberColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public void SetkickboardsWasherAfterTimberFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.kickboardsWasherAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public void SetkickboardsWasherAfterTimberFieldValue(string val)
	{
		this.SetString(val, TableUtils.kickboardsWasherAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public void SetkickboardsWasherAfterTimberFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsWasherAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public void SetkickboardsWasherAfterTimberFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsWasherAfterTimberColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public void SetkickboardsWasherAfterTimberFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsWasherAfterTimberColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public ColumnValue GetcompletionThresholdValue()
	{
		return this.GetValue(TableUtils.completionThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public Decimal GetcompletionThresholdFieldValue()
	{
		return this.GetValue(TableUtils.completionThresholdColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public void SetcompletionThresholdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.completionThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public void SetcompletionThresholdFieldValue(string val)
	{
		this.SetString(val, TableUtils.completionThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public void SetcompletionThresholdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.completionThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public void SetcompletionThresholdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.completionThresholdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public void SetcompletionThresholdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.completionThresholdColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public Decimal doorQtyThreshold
	{
		get
		{
			return this.GetValue(TableUtils.doorQtyThresholdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.doorQtyThresholdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool doorQtyThresholdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.doorQtyThresholdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyThreshold field.
	/// </summary>
	public string doorQtyThresholdDefault
	{
		get
		{
			return TableUtils.doorQtyThresholdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public Decimal doorQtyExtraCost
	{
		get
		{
			return this.GetValue(TableUtils.doorQtyExtraCostColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.doorQtyExtraCostColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool doorQtyExtraCostSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.doorQtyExtraCostColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.doorQtyExtraCost field.
	/// </summary>
	public string doorQtyExtraCostDefault
	{
		get
		{
			return TableUtils.doorQtyExtraCostColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public Decimal difficultDeliveryFee
	{
		get
		{
			return this.GetValue(TableUtils.difficultDeliveryFeeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.difficultDeliveryFeeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool difficultDeliveryFeeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.difficultDeliveryFeeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.difficultDeliveryFee field.
	/// </summary>
	public string difficultDeliveryFeeDefault
	{
		get
		{
			return TableUtils.difficultDeliveryFeeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public Decimal secondCheckMeasureFee
	{
		get
		{
			return this.GetValue(TableUtils.secondCheckMeasureFeeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.secondCheckMeasureFeeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool secondCheckMeasureFeeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.secondCheckMeasureFeeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.secondCheckMeasureFee field.
	/// </summary>
	public string secondCheckMeasureFeeDefault
	{
		get
		{
			return TableUtils.secondCheckMeasureFeeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.GST field.
	/// </summary>
	public Decimal GST
	{
		get
		{
			return this.GetValue(TableUtils.GSTColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.GSTColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool GSTSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.GSTColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.GST field.
	/// </summary>
	public string GSTDefault
	{
		get
		{
			return TableUtils.GSTColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public Decimal insuranceThreshold
	{
		get
		{
			return this.GetValue(TableUtils.insuranceThresholdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.insuranceThresholdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool insuranceThresholdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.insuranceThresholdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceThreshold field.
	/// </summary>
	public string insuranceThresholdDefault
	{
		get
		{
			return TableUtils.insuranceThresholdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public Decimal insuranceFee
	{
		get
		{
			return this.GetValue(TableUtils.insuranceFeeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.insuranceFeeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool insuranceFeeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.insuranceFeeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.insuranceFee field.
	/// </summary>
	public string insuranceFeeDefault
	{
		get
		{
			return TableUtils.insuranceFeeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public Decimal benchtopQtyThreshold
	{
		get
		{
			return this.GetValue(TableUtils.benchtopQtyThresholdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.benchtopQtyThresholdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool benchtopQtyThresholdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.benchtopQtyThresholdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyThreshold field.
	/// </summary>
	public string benchtopQtyThresholdDefault
	{
		get
		{
			return TableUtils.benchtopQtyThresholdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public Decimal benchtopQtyExtraCost
	{
		get
		{
			return this.GetValue(TableUtils.benchtopQtyExtraCostColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.benchtopQtyExtraCostColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool benchtopQtyExtraCostSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.benchtopQtyExtraCostColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.benchtopQtyExtraCost field.
	/// </summary>
	public string benchtopQtyExtraCostDefault
	{
		get
		{
			return TableUtils.benchtopQtyExtraCostColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public Decimal dualFinishes
	{
		get
		{
			return this.GetValue(TableUtils.dualFinishesColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.dualFinishesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool dualFinishesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.dualFinishesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishes field.
	/// </summary>
	public string dualFinishesDefault
	{
		get
		{
			return TableUtils.dualFinishesColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public Decimal dualFinishLessFee
	{
		get
		{
			return this.GetValue(TableUtils.dualFinishLessFeeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.dualFinishLessFeeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool dualFinishLessFeeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.dualFinishLessFeeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFee field.
	/// </summary>
	public string dualFinishLessFeeDefault
	{
		get
		{
			return TableUtils.dualFinishLessFeeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public Decimal dualFinishLessFeeThreshold
	{
		get
		{
			return this.GetValue(TableUtils.dualFinishLessFeeThresholdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.dualFinishLessFeeThresholdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool dualFinishLessFeeThresholdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.dualFinishLessFeeThresholdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.dualFinishLessFeeThreshold field.
	/// </summary>
	public string dualFinishLessFeeThresholdDefault
	{
		get
		{
			return TableUtils.dualFinishLessFeeThresholdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public Decimal highRise
	{
		get
		{
			return this.GetValue(TableUtils.highRiseColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.highRiseColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool highRiseSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.highRiseColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.highRise field.
	/// </summary>
	public string highRiseDefault
	{
		get
		{
			return TableUtils.highRiseColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public Decimal kickboardsAfterTimber
	{
		get
		{
			return this.GetValue(TableUtils.kickboardsAfterTimberColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.kickboardsAfterTimberColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool kickboardsAfterTimberSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.kickboardsAfterTimberColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsAfterTimber field.
	/// </summary>
	public string kickboardsAfterTimberDefault
	{
		get
		{
			return TableUtils.kickboardsAfterTimberColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public Decimal kickboardsWasherAfterTimber
	{
		get
		{
			return this.GetValue(TableUtils.kickboardsWasherAfterTimberColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.kickboardsWasherAfterTimberColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool kickboardsWasherAfterTimberSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.kickboardsWasherAfterTimberColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.kickboardsWasherAfterTimber field.
	/// </summary>
	public string kickboardsWasherAfterTimberDefault
	{
		get
		{
			return TableUtils.kickboardsWasherAfterTimberColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public Decimal completionThreshold
	{
		get
		{
			return this.GetValue(TableUtils.completionThresholdColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.completionThresholdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool completionThresholdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.completionThresholdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's CrmOptions_.completionThreshold field.
	/// </summary>
	public string completionThresholdDefault
	{
		get
		{
			return TableUtils.completionThresholdColumn.DefaultValue;
		}
	}


#endregion
}

}
