﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in LeadMeetingsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="LeadMeetingsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="LeadMeetingsTable"></see> class.
/// </remarks>
/// <seealso cref="LeadMeetingsTable"></seealso>
/// <seealso cref="LeadMeetingsRecord"></seealso>
public class BaseLeadMeetingsRecord : PrimaryKeyRecord
{

	public readonly static LeadMeetingsTable TableUtils = LeadMeetingsTable.Instance;

	// Constructors
 
	protected BaseLeadMeetingsRecord() : base(TableUtils)
	{
	}

	protected BaseLeadMeetingsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public ColumnValue Getaccount_idValue()
	{
		return this.GetValue(TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public Decimal Getaccount_idFieldValue()
	{
		return this.GetValue(TableUtils.account_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public ColumnValue Getdesigner_idValue()
	{
		return this.GetValue(TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public Decimal Getdesigner_idFieldValue()
	{
		return this.GetValue(TableUtils.designer_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.datetime field.
	/// </summary>
	public ColumnValue Getdatetime0Value()
	{
		return this.GetValue(TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.datetime field.
	/// </summary>
	public DateTime Getdatetime0FieldValue()
	{
		return this.GetValue(TableUtils.datetime0Column).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(string val)
	{
		this.SetString(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.datetime0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.end_datetime field.
	/// </summary>
	public ColumnValue Getend_datetimeValue()
	{
		return this.GetValue(TableUtils.end_datetimeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.end_datetime field.
	/// </summary>
	public DateTime Getend_datetimeFieldValue()
	{
		return this.GetValue(TableUtils.end_datetimeColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.end_datetime field.
	/// </summary>
	public void Setend_datetimeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.end_datetimeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.end_datetime field.
	/// </summary>
	public void Setend_datetimeFieldValue(string val)
	{
		this.SetString(val, TableUtils.end_datetimeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.end_datetime field.
	/// </summary>
	public void Setend_datetimeFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.end_datetimeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.address field.
	/// </summary>
	public ColumnValue GetaddressValue()
	{
		return this.GetValue(TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.address field.
	/// </summary>
	public string GetaddressFieldValue()
	{
		return this.GetValue(TableUtils.addressColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.address field.
	/// </summary>
	public void SetaddressFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.address field.
	/// </summary>
	public void SetaddressFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.addressColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.onsite field.
	/// </summary>
	public ColumnValue GetonsiteValue()
	{
		return this.GetValue(TableUtils.onsiteColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.onsite field.
	/// </summary>
	public bool GetonsiteFieldValue()
	{
		return this.GetValue(TableUtils.onsiteColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.onsite field.
	/// </summary>
	public void SetonsiteFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.onsiteColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.onsite field.
	/// </summary>
	public void SetonsiteFieldValue(string val)
	{
		this.SetString(val, TableUtils.onsiteColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.onsite field.
	/// </summary>
	public void SetonsiteFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.onsiteColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public ColumnValue Getlocation_idValue()
	{
		return this.GetValue(TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public Int32 Getlocation_idFieldValue()
	{
		return this.GetValue(TableUtils.location_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public ColumnValue Gethostess_idValue()
	{
		return this.GetValue(TableUtils.hostess_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public Decimal Gethostess_idFieldValue()
	{
		return this.GetValue(TableUtils.hostess_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public void Sethostess_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.hostess_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public void Sethostess_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.hostess_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public void Sethostess_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.hostess_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public void Sethostess_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.hostess_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public void Sethostess_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.hostess_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's LeadMeetings_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public Decimal account_id
	{
		get
		{
			return this.GetValue(TableUtils.account_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.account_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool account_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.account_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.account_id field.
	/// </summary>
	public string account_idDefault
	{
		get
		{
			return TableUtils.account_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public Decimal designer_id
	{
		get
		{
			return this.GetValue(TableUtils.designer_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.designer_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool designer_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.designer_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.designer_id field.
	/// </summary>
	public string designer_idDefault
	{
		get
		{
			return TableUtils.designer_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.datetime field.
	/// </summary>
	public DateTime datetime0
	{
		get
		{
			return this.GetValue(TableUtils.datetime0Column).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.datetime0Column);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool datetime0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.datetime0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.datetime field.
	/// </summary>
	public string datetime0Default
	{
		get
		{
			return TableUtils.datetime0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.end_datetime field.
	/// </summary>
	public DateTime end_datetime
	{
		get
		{
			return this.GetValue(TableUtils.end_datetimeColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.end_datetimeColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool end_datetimeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.end_datetimeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.end_datetime field.
	/// </summary>
	public string end_datetimeDefault
	{
		get
		{
			return TableUtils.end_datetimeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.address field.
	/// </summary>
	public string address
	{
		get
		{
			return this.GetValue(TableUtils.addressColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.addressColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool addressSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.addressColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.address field.
	/// </summary>
	public string addressDefault
	{
		get
		{
			return TableUtils.addressColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.onsite field.
	/// </summary>
	public bool onsite
	{
		get
		{
			return this.GetValue(TableUtils.onsiteColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.onsiteColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool onsiteSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.onsiteColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.onsite field.
	/// </summary>
	public string onsiteDefault
	{
		get
		{
			return TableUtils.onsiteColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public Int32 location_id
	{
		get
		{
			return this.GetValue(TableUtils.location_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.location_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool location_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.location_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.location_id field.
	/// </summary>
	public string location_idDefault
	{
		get
		{
			return TableUtils.location_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public Decimal hostess_id
	{
		get
		{
			return this.GetValue(TableUtils.hostess_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.hostess_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool hostess_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.hostess_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.hostess_id field.
	/// </summary>
	public string hostess_idDefault
	{
		get
		{
			return TableUtils.hostess_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's LeadMeetings_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's LeadMeetings_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}


#endregion
}

}
