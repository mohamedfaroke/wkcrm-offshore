﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_EmployeeTotalOutstandingTasksRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_EmployeeTotalOutstandingTasksRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_EmployeeTotalOutstandingTasksView"></see> class.
/// </remarks>
/// <seealso cref="View_EmployeeTotalOutstandingTasksView"></seealso>
/// <seealso cref="View_EmployeeTotalOutstandingTasksRecord"></seealso>
public class BaseView_EmployeeTotalOutstandingTasksRecord : PrimaryKeyRecord
{

	public readonly static View_EmployeeTotalOutstandingTasksView TableUtils = View_EmployeeTotalOutstandingTasksView.Instance;

	// Constructors
 
	protected BaseView_EmployeeTotalOutstandingTasksRecord() : base(TableUtils)
	{
	}

	protected BaseView_EmployeeTotalOutstandingTasksRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public void Setid0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public void Setid0FieldValue(string val)
	{
		this.SetString(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public void Setid0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public void Setid0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public void Setid0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public ColumnValue Getlocation_idValue()
	{
		return this.GetValue(TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public Int32 Getlocation_idFieldValue()
	{
		return this.GetValue(TableUtils.location_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public ColumnValue Getrole_idValue()
	{
		return this.GetValue(TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public Int32 Getrole_idFieldValue()
	{
		return this.GetValue(TableUtils.role_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.role_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public ColumnValue GetOutstandingTasksValue()
	{
		return this.GetValue(TableUtils.OutstandingTasksColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public Int32 GetOutstandingTasksFieldValue()
	{
		return this.GetValue(TableUtils.OutstandingTasksColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public void SetOutstandingTasksFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OutstandingTasksColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public void SetOutstandingTasksFieldValue(string val)
	{
		this.SetString(val, TableUtils.OutstandingTasksColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public void SetOutstandingTasksFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OutstandingTasksColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public void SetOutstandingTasksFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OutstandingTasksColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public void SetOutstandingTasksFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OutstandingTasksColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public Int32 location_id
	{
		get
		{
			return this.GetValue(TableUtils.location_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.location_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool location_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.location_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.location_id field.
	/// </summary>
	public string location_idDefault
	{
		get
		{
			return TableUtils.location_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public Int32 role_id
	{
		get
		{
			return this.GetValue(TableUtils.role_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.role_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool role_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.role_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.role_id field.
	/// </summary>
	public string role_idDefault
	{
		get
		{
			return TableUtils.role_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public Int32 OutstandingTasks
	{
		get
		{
			return this.GetValue(TableUtils.OutstandingTasksColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OutstandingTasksColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OutstandingTasksSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OutstandingTasksColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_EmployeeTotalOutstandingTasks_.OutstandingTasks field.
	/// </summary>
	public string OutstandingTasksDefault
	{
		get
		{
			return TableUtils.OutstandingTasksColumn.DefaultValue;
		}
	}


#endregion
}

}
