﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractMainRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractMainRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractMainTable"></see> class.
/// </remarks>
/// <seealso cref="ContractMainTable"></seealso>
/// <seealso cref="ContractMainRecord"></seealso>
public class BaseContractMainRecord : PrimaryKeyRecord
{

	public readonly static ContractMainTable TableUtils = ContractMainTable.Instance;

	// Constructors
 
	protected BaseContractMainRecord() : base(TableUtils)
	{
	}

	protected BaseContractMainRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.account_id field.
	/// </summary>
	public ColumnValue Getaccount_idValue()
	{
		return this.GetValue(TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.account_id field.
	/// </summary>
	public Decimal Getaccount_idFieldValue()
	{
		return this.GetValue(TableUtils.account_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.date_created field.
	/// </summary>
	public ColumnValue Getdate_createdValue()
	{
		return this.GetValue(TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.date_created field.
	/// </summary>
	public DateTime Getdate_createdFieldValue()
	{
		return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_createdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.created_by field.
	/// </summary>
	public ColumnValue Getcreated_byValue()
	{
		return this.GetValue(TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.created_by field.
	/// </summary>
	public Decimal Getcreated_byFieldValue()
	{
		return this.GetValue(TableUtils.created_byColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(string val)
	{
		this.SetString(val, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.created_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.created_by field.
	/// </summary>
	public void Setcreated_byFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.created_byColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.date_modified field.
	/// </summary>
	public ColumnValue Getdate_modifiedValue()
	{
		return this.GetValue(TableUtils.date_modifiedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.date_modified field.
	/// </summary>
	public DateTime Getdate_modifiedFieldValue()
	{
		return this.GetValue(TableUtils.date_modifiedColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_modified field.
	/// </summary>
	public void Setdate_modifiedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_modifiedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_modified field.
	/// </summary>
	public void Setdate_modifiedFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_modifiedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_modified field.
	/// </summary>
	public void Setdate_modifiedFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_modifiedColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public ColumnValue Getmodified_byValue()
	{
		return this.GetValue(TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public Decimal Getmodified_byFieldValue()
	{
		return this.GetValue(TableUtils.modified_byColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(string val)
	{
		this.SetString(val, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.modified_byColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public void Setmodified_byFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.modified_byColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.signed_date field.
	/// </summary>
	public ColumnValue Getsigned_dateValue()
	{
		return this.GetValue(TableUtils.signed_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.signed_date field.
	/// </summary>
	public DateTime Getsigned_dateFieldValue()
	{
		return this.GetValue(TableUtils.signed_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.signed_date field.
	/// </summary>
	public void Setsigned_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.signed_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.signed_date field.
	/// </summary>
	public void Setsigned_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.signed_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.signed_date field.
	/// </summary>
	public void Setsigned_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.signed_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.original_total field.
	/// </summary>
	public ColumnValue Getoriginal_totalValue()
	{
		return this.GetValue(TableUtils.original_totalColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.original_total field.
	/// </summary>
	public Decimal Getoriginal_totalFieldValue()
	{
		return this.GetValue(TableUtils.original_totalColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.original_total field.
	/// </summary>
	public void Setoriginal_totalFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.original_totalColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.original_total field.
	/// </summary>
	public void Setoriginal_totalFieldValue(string val)
	{
		this.SetString(val, TableUtils.original_totalColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.original_total field.
	/// </summary>
	public void Setoriginal_totalFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.original_totalColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.original_total field.
	/// </summary>
	public void Setoriginal_totalFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.original_totalColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.original_total field.
	/// </summary>
	public void Setoriginal_totalFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.original_totalColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public ColumnValue Getcheck_measure_feeValue()
	{
		return this.GetValue(TableUtils.check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public Decimal Getcheck_measure_feeFieldValue()
	{
		return this.GetValue(TableUtils.check_measure_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public void Setcheck_measure_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public void Setcheck_measure_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public void Setcheck_measure_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public void Setcheck_measure_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public void Setcheck_measure_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.check_measure_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public ColumnValue Getsecond_check_measure_feeValue()
	{
		return this.GetValue(TableUtils.second_check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public Decimal Getsecond_check_measure_feeFieldValue()
	{
		return this.GetValue(TableUtils.second_check_measure_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public void Setsecond_check_measure_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.second_check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public void Setsecond_check_measure_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.second_check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public void Setsecond_check_measure_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.second_check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public void Setsecond_check_measure_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.second_check_measure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public void Setsecond_check_measure_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.second_check_measure_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public ColumnValue Getbench_threshold_feeValue()
	{
		return this.GetValue(TableUtils.bench_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public Decimal Getbench_threshold_feeFieldValue()
	{
		return this.GetValue(TableUtils.bench_threshold_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public void Setbench_threshold_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.bench_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public void Setbench_threshold_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.bench_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public void Setbench_threshold_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.bench_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public void Setbench_threshold_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.bench_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public void Setbench_threshold_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.bench_threshold_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public ColumnValue Getdoor_threshold_feeValue()
	{
		return this.GetValue(TableUtils.door_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public Decimal Getdoor_threshold_feeFieldValue()
	{
		return this.GetValue(TableUtils.door_threshold_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public void Setdoor_threshold_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.door_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public void Setdoor_threshold_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.door_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public void Setdoor_threshold_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public void Setdoor_threshold_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_threshold_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public void Setdoor_threshold_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_threshold_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public ColumnValue Getdifficult_delivery_feeValue()
	{
		return this.GetValue(TableUtils.difficult_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public Decimal Getdifficult_delivery_feeFieldValue()
	{
		return this.GetValue(TableUtils.difficult_delivery_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public void Setdifficult_delivery_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.difficult_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public void Setdifficult_delivery_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.difficult_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public void Setdifficult_delivery_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.difficult_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public void Setdifficult_delivery_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.difficult_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public void Setdifficult_delivery_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.difficult_delivery_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public ColumnValue Getnon_metro_delivery_feeValue()
	{
		return this.GetValue(TableUtils.non_metro_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public Decimal Getnon_metro_delivery_feeFieldValue()
	{
		return this.GetValue(TableUtils.non_metro_delivery_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public void Setnon_metro_delivery_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.non_metro_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public void Setnon_metro_delivery_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.non_metro_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public void Setnon_metro_delivery_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.non_metro_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public void Setnon_metro_delivery_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.non_metro_delivery_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public void Setnon_metro_delivery_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.non_metro_delivery_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public ColumnValue Gethome_insurance_feeValue()
	{
		return this.GetValue(TableUtils.home_insurance_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public Decimal Gethome_insurance_feeFieldValue()
	{
		return this.GetValue(TableUtils.home_insurance_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public void Sethome_insurance_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.home_insurance_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public void Sethome_insurance_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.home_insurance_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public void Sethome_insurance_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_insurance_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public void Sethome_insurance_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_insurance_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public void Sethome_insurance_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_insurance_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public ColumnValue Getdual_finishes_feeValue()
	{
		return this.GetValue(TableUtils.dual_finishes_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public Decimal Getdual_finishes_feeFieldValue()
	{
		return this.GetValue(TableUtils.dual_finishes_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public void Setdual_finishes_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.dual_finishes_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public void Setdual_finishes_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.dual_finishes_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public void Setdual_finishes_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dual_finishes_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public void Setdual_finishes_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dual_finishes_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public void Setdual_finishes_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dual_finishes_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public ColumnValue Getdual_finishes_lessthan_feeValue()
	{
		return this.GetValue(TableUtils.dual_finishes_lessthan_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public Decimal Getdual_finishes_lessthan_feeFieldValue()
	{
		return this.GetValue(TableUtils.dual_finishes_lessthan_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public void Setdual_finishes_lessthan_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.dual_finishes_lessthan_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public void Setdual_finishes_lessthan_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.dual_finishes_lessthan_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public void Setdual_finishes_lessthan_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dual_finishes_lessthan_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public void Setdual_finishes_lessthan_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dual_finishes_lessthan_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public void Setdual_finishes_lessthan_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.dual_finishes_lessthan_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public ColumnValue Gethighrise_feeValue()
	{
		return this.GetValue(TableUtils.highrise_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public Decimal Gethighrise_feeFieldValue()
	{
		return this.GetValue(TableUtils.highrise_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public void Sethighrise_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.highrise_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public void Sethighrise_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.highrise_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public void Sethighrise_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.highrise_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public void Sethighrise_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.highrise_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public void Sethighrise_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.highrise_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public ColumnValue GetkickboardsAfterTimber_feeValue()
	{
		return this.GetValue(TableUtils.kickboardsAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public Decimal GetkickboardsAfterTimber_feeFieldValue()
	{
		return this.GetValue(TableUtils.kickboardsAfterTimber_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsAfterTimber_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.kickboardsAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsAfterTimber_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.kickboardsAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsAfterTimber_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsAfterTimber_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsAfterTimber_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsAfterTimber_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public ColumnValue GetkickboardsWasherAfterTimber_feeValue()
	{
		return this.GetValue(TableUtils.kickboardsWasherAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public Decimal GetkickboardsWasherAfterTimber_feeFieldValue()
	{
		return this.GetValue(TableUtils.kickboardsWasherAfterTimber_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsWasherAfterTimber_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.kickboardsWasherAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsWasherAfterTimber_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.kickboardsWasherAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsWasherAfterTimber_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsWasherAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsWasherAfterTimber_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsWasherAfterTimber_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public void SetkickboardsWasherAfterTimber_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.kickboardsWasherAfterTimber_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public ColumnValue Getcontract_type_idValue()
	{
		return this.GetValue(TableUtils.contract_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public Int32 Getcontract_type_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_type_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public void Setcontract_type_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public void Setcontract_type_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public void Setcontract_type_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public void Setcontract_type_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public void Setcontract_type_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_type_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.estimated_completion_date field.
	/// </summary>
	public ColumnValue Getestimated_completion_dateValue()
	{
		return this.GetValue(TableUtils.estimated_completion_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.estimated_completion_date field.
	/// </summary>
	public DateTime Getestimated_completion_dateFieldValue()
	{
		return this.GetValue(TableUtils.estimated_completion_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.estimated_completion_date field.
	/// </summary>
	public void Setestimated_completion_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.estimated_completion_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.estimated_completion_date field.
	/// </summary>
	public void Setestimated_completion_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.estimated_completion_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.estimated_completion_date field.
	/// </summary>
	public void Setestimated_completion_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.estimated_completion_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.home_insurance_sent field.
	/// </summary>
	public ColumnValue Gethome_insurance_sentValue()
	{
		return this.GetValue(TableUtils.home_insurance_sentColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.home_insurance_sent field.
	/// </summary>
	public bool Gethome_insurance_sentFieldValue()
	{
		return this.GetValue(TableUtils.home_insurance_sentColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_sent field.
	/// </summary>
	public void Sethome_insurance_sentFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.home_insurance_sentColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_sent field.
	/// </summary>
	public void Sethome_insurance_sentFieldValue(string val)
	{
		this.SetString(val, TableUtils.home_insurance_sentColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_sent field.
	/// </summary>
	public void Sethome_insurance_sentFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_insurance_sentColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.home_insurance_policy field.
	/// </summary>
	public ColumnValue Gethome_insurance_policyValue()
	{
		return this.GetValue(TableUtils.home_insurance_policyColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.home_insurance_policy field.
	/// </summary>
	public string Gethome_insurance_policyFieldValue()
	{
		return this.GetValue(TableUtils.home_insurance_policyColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_policy field.
	/// </summary>
	public void Sethome_insurance_policyFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.home_insurance_policyColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_policy field.
	/// </summary>
	public void Sethome_insurance_policyFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_insurance_policyColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public ColumnValue Getdesigner_feeValue()
	{
		return this.GetValue(TableUtils.designer_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public Decimal Getdesigner_feeFieldValue()
	{
		return this.GetValue(TableUtils.designer_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public void Setdesigner_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.designer_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public void Setdesigner_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.designer_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public void Setdesigner_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public void Setdesigner_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public void Setdesigner_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public ColumnValue Getconsultation_feeValue()
	{
		return this.GetValue(TableUtils.consultation_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public Decimal Getconsultation_feeFieldValue()
	{
		return this.GetValue(TableUtils.consultation_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public void Setconsultation_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.consultation_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public void Setconsultation_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.consultation_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public void Setconsultation_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.consultation_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public void Setconsultation_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.consultation_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public void Setconsultation_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.consultation_feeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public ColumnValue Getheight_ceilingValue()
	{
		return this.GetValue(TableUtils.height_ceilingColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public Decimal Getheight_ceilingFieldValue()
	{
		return this.GetValue(TableUtils.height_ceilingColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public void Setheight_ceilingFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.height_ceilingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public void Setheight_ceilingFieldValue(string val)
	{
		this.SetString(val, TableUtils.height_ceilingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public void Setheight_ceilingFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_ceilingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public void Setheight_ceilingFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_ceilingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public void Setheight_ceilingFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_ceilingColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public ColumnValue Getheight_cabinetValue()
	{
		return this.GetValue(TableUtils.height_cabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public Decimal Getheight_cabinetFieldValue()
	{
		return this.GetValue(TableUtils.height_cabinetColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public void Setheight_cabinetFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.height_cabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public void Setheight_cabinetFieldValue(string val)
	{
		this.SetString(val, TableUtils.height_cabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public void Setheight_cabinetFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_cabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public void Setheight_cabinetFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_cabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public void Setheight_cabinetFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_cabinetColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public ColumnValue Getheight_benchValue()
	{
		return this.GetValue(TableUtils.height_benchColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public Decimal Getheight_benchFieldValue()
	{
		return this.GetValue(TableUtils.height_benchColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public void Setheight_benchFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.height_benchColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public void Setheight_benchFieldValue(string val)
	{
		this.SetString(val, TableUtils.height_benchColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public void Setheight_benchFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_benchColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public void Setheight_benchFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_benchColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public void Setheight_benchFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_benchColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public ColumnValue Getheight_splashbackValue()
	{
		return this.GetValue(TableUtils.height_splashbackColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public Decimal Getheight_splashbackFieldValue()
	{
		return this.GetValue(TableUtils.height_splashbackColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public void Setheight_splashbackFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.height_splashbackColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public void Setheight_splashbackFieldValue(string val)
	{
		this.SetString(val, TableUtils.height_splashbackColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public void Setheight_splashbackFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_splashbackColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public void Setheight_splashbackFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_splashbackColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public void Setheight_splashbackFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_splashbackColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public ColumnValue Getheight_wallcabinetValue()
	{
		return this.GetValue(TableUtils.height_wallcabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public Decimal Getheight_wallcabinetFieldValue()
	{
		return this.GetValue(TableUtils.height_wallcabinetColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public void Setheight_wallcabinetFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.height_wallcabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public void Setheight_wallcabinetFieldValue(string val)
	{
		this.SetString(val, TableUtils.height_wallcabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public void Setheight_wallcabinetFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_wallcabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public void Setheight_wallcabinetFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_wallcabinetColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public void Setheight_wallcabinetFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_wallcabinetColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public ColumnValue Getheight_microwaveValue()
	{
		return this.GetValue(TableUtils.height_microwaveColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public Decimal Getheight_microwaveFieldValue()
	{
		return this.GetValue(TableUtils.height_microwaveColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public void Setheight_microwaveFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.height_microwaveColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public void Setheight_microwaveFieldValue(string val)
	{
		this.SetString(val, TableUtils.height_microwaveColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public void Setheight_microwaveFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_microwaveColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public void Setheight_microwaveFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_microwaveColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public void Setheight_microwaveFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_microwaveColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public ColumnValue Getheight_wallovenValue()
	{
		return this.GetValue(TableUtils.height_wallovenColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public Decimal Getheight_wallovenFieldValue()
	{
		return this.GetValue(TableUtils.height_wallovenColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public void Setheight_wallovenFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.height_wallovenColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public void Setheight_wallovenFieldValue(string val)
	{
		this.SetString(val, TableUtils.height_wallovenColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public void Setheight_wallovenFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_wallovenColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public void Setheight_wallovenFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_wallovenColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public void Setheight_wallovenFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.height_wallovenColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractMain_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.account_id field.
	/// </summary>
	public Decimal account_id
	{
		get
		{
			return this.GetValue(TableUtils.account_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.account_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool account_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.account_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.account_id field.
	/// </summary>
	public string account_idDefault
	{
		get
		{
			return TableUtils.account_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.date_created field.
	/// </summary>
	public DateTime date_created
	{
		get
		{
			return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_createdColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_createdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_createdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_created field.
	/// </summary>
	public string date_createdDefault
	{
		get
		{
			return TableUtils.date_createdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.created_by field.
	/// </summary>
	public Decimal created_by
	{
		get
		{
			return this.GetValue(TableUtils.created_byColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.created_byColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool created_bySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.created_byColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.created_by field.
	/// </summary>
	public string created_byDefault
	{
		get
		{
			return TableUtils.created_byColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.date_modified field.
	/// </summary>
	public DateTime date_modified
	{
		get
		{
			return this.GetValue(TableUtils.date_modifiedColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_modifiedColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_modifiedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_modifiedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.date_modified field.
	/// </summary>
	public string date_modifiedDefault
	{
		get
		{
			return TableUtils.date_modifiedColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public Decimal modified_by
	{
		get
		{
			return this.GetValue(TableUtils.modified_byColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.modified_byColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool modified_bySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.modified_byColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.modified_by field.
	/// </summary>
	public string modified_byDefault
	{
		get
		{
			return TableUtils.modified_byColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.signed_date field.
	/// </summary>
	public DateTime signed_date
	{
		get
		{
			return this.GetValue(TableUtils.signed_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.signed_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool signed_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.signed_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.signed_date field.
	/// </summary>
	public string signed_dateDefault
	{
		get
		{
			return TableUtils.signed_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.original_total field.
	/// </summary>
	public Decimal original_total
	{
		get
		{
			return this.GetValue(TableUtils.original_totalColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.original_totalColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool original_totalSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.original_totalColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.original_total field.
	/// </summary>
	public string original_totalDefault
	{
		get
		{
			return TableUtils.original_totalColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public Decimal check_measure_fee
	{
		get
		{
			return this.GetValue(TableUtils.check_measure_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.check_measure_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool check_measure_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.check_measure_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.check_measure_fee field.
	/// </summary>
	public string check_measure_feeDefault
	{
		get
		{
			return TableUtils.check_measure_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public Decimal second_check_measure_fee
	{
		get
		{
			return this.GetValue(TableUtils.second_check_measure_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.second_check_measure_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool second_check_measure_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.second_check_measure_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.second_check_measure_fee field.
	/// </summary>
	public string second_check_measure_feeDefault
	{
		get
		{
			return TableUtils.second_check_measure_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public Decimal bench_threshold_fee
	{
		get
		{
			return this.GetValue(TableUtils.bench_threshold_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.bench_threshold_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool bench_threshold_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.bench_threshold_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.bench_threshold_fee field.
	/// </summary>
	public string bench_threshold_feeDefault
	{
		get
		{
			return TableUtils.bench_threshold_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public Decimal door_threshold_fee
	{
		get
		{
			return this.GetValue(TableUtils.door_threshold_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.door_threshold_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool door_threshold_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.door_threshold_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.door_threshold_fee field.
	/// </summary>
	public string door_threshold_feeDefault
	{
		get
		{
			return TableUtils.door_threshold_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public Decimal difficult_delivery_fee
	{
		get
		{
			return this.GetValue(TableUtils.difficult_delivery_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.difficult_delivery_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool difficult_delivery_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.difficult_delivery_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.difficult_delivery_fee field.
	/// </summary>
	public string difficult_delivery_feeDefault
	{
		get
		{
			return TableUtils.difficult_delivery_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public Decimal non_metro_delivery_fee
	{
		get
		{
			return this.GetValue(TableUtils.non_metro_delivery_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.non_metro_delivery_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool non_metro_delivery_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.non_metro_delivery_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.non_metro_delivery_fee field.
	/// </summary>
	public string non_metro_delivery_feeDefault
	{
		get
		{
			return TableUtils.non_metro_delivery_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public Decimal home_insurance_fee
	{
		get
		{
			return this.GetValue(TableUtils.home_insurance_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.home_insurance_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool home_insurance_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.home_insurance_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_fee field.
	/// </summary>
	public string home_insurance_feeDefault
	{
		get
		{
			return TableUtils.home_insurance_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public Decimal dual_finishes_fee
	{
		get
		{
			return this.GetValue(TableUtils.dual_finishes_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.dual_finishes_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool dual_finishes_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.dual_finishes_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_fee field.
	/// </summary>
	public string dual_finishes_feeDefault
	{
		get
		{
			return TableUtils.dual_finishes_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public Decimal dual_finishes_lessthan_fee
	{
		get
		{
			return this.GetValue(TableUtils.dual_finishes_lessthan_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.dual_finishes_lessthan_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool dual_finishes_lessthan_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.dual_finishes_lessthan_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.dual_finishes_lessthan_fee field.
	/// </summary>
	public string dual_finishes_lessthan_feeDefault
	{
		get
		{
			return TableUtils.dual_finishes_lessthan_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public Decimal highrise_fee
	{
		get
		{
			return this.GetValue(TableUtils.highrise_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.highrise_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool highrise_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.highrise_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.highrise_fee field.
	/// </summary>
	public string highrise_feeDefault
	{
		get
		{
			return TableUtils.highrise_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public Decimal kickboardsAfterTimber_fee
	{
		get
		{
			return this.GetValue(TableUtils.kickboardsAfterTimber_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.kickboardsAfterTimber_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool kickboardsAfterTimber_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.kickboardsAfterTimber_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsAfterTimber_fee field.
	/// </summary>
	public string kickboardsAfterTimber_feeDefault
	{
		get
		{
			return TableUtils.kickboardsAfterTimber_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public Decimal kickboardsWasherAfterTimber_fee
	{
		get
		{
			return this.GetValue(TableUtils.kickboardsWasherAfterTimber_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.kickboardsWasherAfterTimber_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool kickboardsWasherAfterTimber_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.kickboardsWasherAfterTimber_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.kickboardsWasherAfterTimber_fee field.
	/// </summary>
	public string kickboardsWasherAfterTimber_feeDefault
	{
		get
		{
			return TableUtils.kickboardsWasherAfterTimber_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public Int32 contract_type_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_type_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_type_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_type_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_type_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.contract_type_id field.
	/// </summary>
	public string contract_type_idDefault
	{
		get
		{
			return TableUtils.contract_type_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.estimated_completion_date field.
	/// </summary>
	public DateTime estimated_completion_date
	{
		get
		{
			return this.GetValue(TableUtils.estimated_completion_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.estimated_completion_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool estimated_completion_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.estimated_completion_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.estimated_completion_date field.
	/// </summary>
	public string estimated_completion_dateDefault
	{
		get
		{
			return TableUtils.estimated_completion_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.home_insurance_sent field.
	/// </summary>
	public bool home_insurance_sent
	{
		get
		{
			return this.GetValue(TableUtils.home_insurance_sentColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.home_insurance_sentColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool home_insurance_sentSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.home_insurance_sentColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_sent field.
	/// </summary>
	public string home_insurance_sentDefault
	{
		get
		{
			return TableUtils.home_insurance_sentColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.home_insurance_policy field.
	/// </summary>
	public string home_insurance_policy
	{
		get
		{
			return this.GetValue(TableUtils.home_insurance_policyColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.home_insurance_policyColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool home_insurance_policySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.home_insurance_policyColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.home_insurance_policy field.
	/// </summary>
	public string home_insurance_policyDefault
	{
		get
		{
			return TableUtils.home_insurance_policyColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public Decimal designer_fee
	{
		get
		{
			return this.GetValue(TableUtils.designer_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.designer_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool designer_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.designer_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.designer_fee field.
	/// </summary>
	public string designer_feeDefault
	{
		get
		{
			return TableUtils.designer_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public Decimal consultation_fee
	{
		get
		{
			return this.GetValue(TableUtils.consultation_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.consultation_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool consultation_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.consultation_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.consultation_fee field.
	/// </summary>
	public string consultation_feeDefault
	{
		get
		{
			return TableUtils.consultation_feeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public Decimal height_ceiling
	{
		get
		{
			return this.GetValue(TableUtils.height_ceilingColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.height_ceilingColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool height_ceilingSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.height_ceilingColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_ceiling field.
	/// </summary>
	public string height_ceilingDefault
	{
		get
		{
			return TableUtils.height_ceilingColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public Decimal height_cabinet
	{
		get
		{
			return this.GetValue(TableUtils.height_cabinetColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.height_cabinetColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool height_cabinetSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.height_cabinetColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_cabinet field.
	/// </summary>
	public string height_cabinetDefault
	{
		get
		{
			return TableUtils.height_cabinetColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public Decimal height_bench
	{
		get
		{
			return this.GetValue(TableUtils.height_benchColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.height_benchColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool height_benchSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.height_benchColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_bench field.
	/// </summary>
	public string height_benchDefault
	{
		get
		{
			return TableUtils.height_benchColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public Decimal height_splashback
	{
		get
		{
			return this.GetValue(TableUtils.height_splashbackColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.height_splashbackColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool height_splashbackSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.height_splashbackColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_splashback field.
	/// </summary>
	public string height_splashbackDefault
	{
		get
		{
			return TableUtils.height_splashbackColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public Decimal height_wallcabinet
	{
		get
		{
			return this.GetValue(TableUtils.height_wallcabinetColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.height_wallcabinetColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool height_wallcabinetSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.height_wallcabinetColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_wallcabinet field.
	/// </summary>
	public string height_wallcabinetDefault
	{
		get
		{
			return TableUtils.height_wallcabinetColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public Decimal height_microwave
	{
		get
		{
			return this.GetValue(TableUtils.height_microwaveColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.height_microwaveColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool height_microwaveSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.height_microwaveColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_microwave field.
	/// </summary>
	public string height_microwaveDefault
	{
		get
		{
			return TableUtils.height_microwaveColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public Decimal height_walloven
	{
		get
		{
			return this.GetValue(TableUtils.height_wallovenColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.height_wallovenColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool height_wallovenSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.height_wallovenColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.height_walloven field.
	/// </summary>
	public string height_wallovenDefault
	{
		get
		{
			return TableUtils.height_wallovenColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractMain_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractMain_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
