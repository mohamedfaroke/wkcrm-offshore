﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EmployeeRolesRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="EmployeeRolesRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EmployeeRolesTable"></see> class.
/// </remarks>
/// <seealso cref="EmployeeRolesTable"></seealso>
/// <seealso cref="EmployeeRolesRecord"></seealso>
public class BaseEmployeeRolesRecord : PrimaryKeyRecord, IUserRoleRecord
{

	public readonly static EmployeeRolesTable TableUtils = EmployeeRolesTable.Instance;

	// Constructors
 
	protected BaseEmployeeRolesRecord() : base(TableUtils)
	{
	}

	protected BaseEmployeeRolesRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}

#region "IUserRecord Members"

	//Get the user's unique identifier
	public string GetUserId()
	{
		return this.GetString(((BaseClasses.IUserTable)this.TableAccess).UserIdColumn);
	}

#endregion




#region "IUserRoleRecord Members"

	//Get the role to which this user belongs
	public string GetUserRole()
	{
		return this.GetString(((BaseClasses.IUserRoleTable)this.TableAccess).UserRoleColumn);
	}

#endregion


#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EmployeeRoles_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EmployeeRoles_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public ColumnValue Getemployee_idValue()
	{
		return this.GetValue(TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public Decimal Getemployee_idFieldValue()
	{
		return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public ColumnValue Getrole_idValue()
	{
		return this.GetValue(TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public Int32 Getrole_idFieldValue()
	{
		return this.GetValue(TableUtils.role_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.role_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public void Setrole_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.role_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's EmployeeRoles_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public Decimal employee_id
	{
		get
		{
			return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.employee_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool employee_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.employee_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.employee_id field.
	/// </summary>
	public string employee_idDefault
	{
		get
		{
			return TableUtils.employee_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public Int32 role_id
	{
		get
		{
			return this.GetValue(TableUtils.role_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.role_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool role_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.role_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EmployeeRoles_.role_id field.
	/// </summary>
	public string role_idDefault
	{
		get
		{
			return TableUtils.role_idColumn.DefaultValue;
		}
	}


#endregion
}

}
