﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_PaymentScheduleTotalOutstandingRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_PaymentScheduleTotalOutstandingRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_PaymentScheduleTotalOutstandingView"></see> class.
/// </remarks>
/// <seealso cref="View_PaymentScheduleTotalOutstandingView"></seealso>
/// <seealso cref="View_PaymentScheduleTotalOutstandingRecord"></seealso>
public class BaseView_PaymentScheduleTotalOutstandingRecord : PrimaryKeyRecord
{

	public readonly static View_PaymentScheduleTotalOutstandingView TableUtils = View_PaymentScheduleTotalOutstandingView.Instance;

	// Constructors
 
	protected BaseView_PaymentScheduleTotalOutstandingRecord() : base(TableUtils)
	{
	}

	protected BaseView_PaymentScheduleTotalOutstandingRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public ColumnValue Getpayment_schedule_idValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public Decimal Getpayment_schedule_idFieldValue()
	{
		return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public void Setpayment_schedule_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.payment_schedule_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public ColumnValue GetTotalOutStandingValue()
	{
		return this.GetValue(TableUtils.TotalOutStandingColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public Decimal GetTotalOutStandingFieldValue()
	{
		return this.GetValue(TableUtils.TotalOutStandingColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public void SetTotalOutStandingFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TotalOutStandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public void SetTotalOutStandingFieldValue(string val)
	{
		this.SetString(val, TableUtils.TotalOutStandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public void SetTotalOutStandingFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalOutStandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public void SetTotalOutStandingFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalOutStandingColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public void SetTotalOutStandingFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalOutStandingColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public Decimal payment_schedule_id
	{
		get
		{
			return this.GetValue(TableUtils.payment_schedule_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.payment_schedule_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool payment_schedule_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.payment_schedule_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.payment_schedule_id field.
	/// </summary>
	public string payment_schedule_idDefault
	{
		get
		{
			return TableUtils.payment_schedule_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public Decimal TotalOutStanding
	{
		get
		{
			return this.GetValue(TableUtils.TotalOutStandingColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TotalOutStandingColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TotalOutStandingSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TotalOutStandingColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_PaymentScheduleTotalOutstanding_.TotalOutStanding field.
	/// </summary>
	public string TotalOutStandingDefault
	{
		get
		{
			return TableUtils.TotalOutStandingColumn.DefaultValue;
		}
	}


#endregion
}

}
