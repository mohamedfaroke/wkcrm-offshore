﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractOthersRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractOthersRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractOthersTable"></see> class.
/// </remarks>
/// <seealso cref="ContractOthersTable"></seealso>
/// <seealso cref="ContractOthersRecord"></seealso>
public class BaseContractOthersRecord : PrimaryKeyRecord
{

	public readonly static ContractOthersTable TableUtils = ContractOthersTable.Instance;

	// Constructors
 
	protected BaseContractOthersRecord() : base(TableUtils)
	{
	}

	protected BaseContractOthersRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public ColumnValue Getcontract_item_typeValue()
	{
		return this.GetValue(TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public Int32 Getcontract_item_typeFieldValue()
	{
		return this.GetValue(TableUtils.contract_item_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.price field.
	/// </summary>
	public ColumnValue GetpriceValue()
	{
		return this.GetValue(TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.price field.
	/// </summary>
	public Decimal GetpriceFieldValue()
	{
		return this.GetValue(TableUtils.priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.price field.
	/// </summary>
	public void SetpriceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.price field.
	/// </summary>
	public void SetpriceFieldValue(string val)
	{
		this.SetString(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.price field.
	/// </summary>
	public void SetpriceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.price field.
	/// </summary>
	public void SetpriceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.price field.
	/// </summary>
	public void SetpriceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public ColumnValue Getcontract_payment_typeValue()
	{
		return this.GetValue(TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public Int32 Getcontract_payment_typeFieldValue()
	{
		return this.GetValue(TableUtils.contract_payment_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public void Setcontract_payment_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.category field.
	/// </summary>
	public ColumnValue GetcategoryValue()
	{
		return this.GetValue(TableUtils.categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.category field.
	/// </summary>
	public string GetcategoryFieldValue()
	{
		return this.GetValue(TableUtils.categoryColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.category field.
	/// </summary>
	public void SetcategoryFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.category field.
	/// </summary>
	public void SetcategoryFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.categoryColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.supplier_code field.
	/// </summary>
	public ColumnValue Getsupplier_codeValue()
	{
		return this.GetValue(TableUtils.supplier_codeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.supplier_code field.
	/// </summary>
	public string Getsupplier_codeFieldValue()
	{
		return this.GetValue(TableUtils.supplier_codeColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.supplier_code field.
	/// </summary>
	public void Setsupplier_codeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.supplier_codeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.supplier_code field.
	/// </summary>
	public void Setsupplier_codeFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.supplier_codeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.sub_category field.
	/// </summary>
	public ColumnValue Getsub_categoryValue()
	{
		return this.GetValue(TableUtils.sub_categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractOthers_.sub_category field.
	/// </summary>
	public string Getsub_categoryFieldValue()
	{
		return this.GetValue(TableUtils.sub_categoryColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.sub_category field.
	/// </summary>
	public void Setsub_categoryFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.sub_categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.sub_category field.
	/// </summary>
	public void Setsub_categoryFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.sub_categoryColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public Int32 contract_item_type
	{
		get
		{
			return this.GetValue(TableUtils.contract_item_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_item_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_item_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_item_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_item_type field.
	/// </summary>
	public string contract_item_typeDefault
	{
		get
		{
			return TableUtils.contract_item_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.price field.
	/// </summary>
	public Decimal price
	{
		get
		{
			return this.GetValue(TableUtils.priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.price field.
	/// </summary>
	public string priceDefault
	{
		get
		{
			return TableUtils.priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public Int32 contract_payment_type
	{
		get
		{
			return this.GetValue(TableUtils.contract_payment_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_payment_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_payment_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_payment_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.contract_payment_type field.
	/// </summary>
	public string contract_payment_typeDefault
	{
		get
		{
			return TableUtils.contract_payment_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.category field.
	/// </summary>
	public string category
	{
		get
		{
			return this.GetValue(TableUtils.categoryColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.categoryColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool categorySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.categoryColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.category field.
	/// </summary>
	public string categoryDefault
	{
		get
		{
			return TableUtils.categoryColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.supplier_code field.
	/// </summary>
	public string supplier_code
	{
		get
		{
			return this.GetValue(TableUtils.supplier_codeColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.supplier_codeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool supplier_codeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.supplier_codeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.supplier_code field.
	/// </summary>
	public string supplier_codeDefault
	{
		get
		{
			return TableUtils.supplier_codeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractOthers_.sub_category field.
	/// </summary>
	public string sub_category
	{
		get
		{
			return this.GetValue(TableUtils.sub_categoryColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.sub_categoryColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool sub_categorySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.sub_categoryColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractOthers_.sub_category field.
	/// </summary>
	public string sub_categoryDefault
	{
		get
		{
			return TableUtils.sub_categoryColumn.DefaultValue;
		}
	}


#endregion
}

}
