﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_OppConversionReportByLocRecord.vb

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_OppConversionReportByLocRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_OppConversionReportByLocView"></see> class.
/// </remarks>
/// <seealso cref="View_OppConversionReportByLocView"></seealso>
/// <seealso cref="View_OppConversionReportByLocRecord"></seealso>
public class BaseView_OppConversionReportByLocRecord : KeylessRecord
{

	public readonly static View_OppConversionReportByLocView TableUtils = View_OppConversionReportByLocView.Instance;

	// Constructors
 
	protected BaseView_OppConversionReportByLocRecord() : base(TableUtils)
	{
	}

	protected BaseView_OppConversionReportByLocRecord(KeylessRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public ColumnValue Getlocation_idValue()
	{
		return this.GetValue(TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public Int32 Getlocation_idFieldValue()
	{
		return this.GetValue(TableUtils.location_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public void Setlocation_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.location_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public ColumnValue GetOppsValue()
	{
		return this.GetValue(TableUtils.OppsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public Int32 GetOppsFieldValue()
	{
		return this.GetValue(TableUtils.OppsColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public void SetOppsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OppsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public void SetOppsFieldValue(string val)
	{
		this.SetString(val, TableUtils.OppsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public void SetOppsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OppsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public void SetOppsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OppsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public void SetOppsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OppsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public ColumnValue GetLeadsValue()
	{
		return this.GetValue(TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public Int32 GetLeadsFieldValue()
	{
		return this.GetValue(TableUtils.LeadsColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(string val)
	{
		this.SetString(val, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.LeadsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public ColumnValue GetMonthValue()
	{
		return this.GetValue(TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public Int32 GetMonthFieldValue()
	{
		return this.GetValue(TableUtils.MonthColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public void SetMonthFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public void SetMonthFieldValue(string val)
	{
		this.SetString(val, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public void SetMonthFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public void SetMonthFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public void SetMonthFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.MonthColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public ColumnValue GetYearValue()
	{
		return this.GetValue(TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public Int32 GetYearFieldValue()
	{
		return this.GetValue(TableUtils.YearColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public void SetYearFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public void SetYearFieldValue(string val)
	{
		this.SetString(val, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public void SetYearFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public void SetYearFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public void SetYearFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.YearColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public Int32 location_id
	{
		get
		{
			return this.GetValue(TableUtils.location_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.location_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool location_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.location_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.location_id field.
	/// </summary>
	public string location_idDefault
	{
		get
		{
			return TableUtils.location_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByLoc_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public Int32 Opps
	{
		get
		{
			return this.GetValue(TableUtils.OppsColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OppsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OppsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OppsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Opps field.
	/// </summary>
	public string OppsDefault
	{
		get
		{
			return TableUtils.OppsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public Int32 Leads
	{
		get
		{
			return this.GetValue(TableUtils.LeadsColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.LeadsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool LeadsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.LeadsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Leads field.
	/// </summary>
	public string LeadsDefault
	{
		get
		{
			return TableUtils.LeadsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public Int32 Month
	{
		get
		{
			return this.GetValue(TableUtils.MonthColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.MonthColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool MonthSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.MonthColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Month field.
	/// </summary>
	public string MonthDefault
	{
		get
		{
			return TableUtils.MonthColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public Int32 Year
	{
		get
		{
			return this.GetValue(TableUtils.YearColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.YearColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool YearSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.YearColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByLoc_.Year field.
	/// </summary>
	public string YearDefault
	{
		get
		{
			return TableUtils.YearColumn.DefaultValue;
		}
	}


#endregion

}

}
