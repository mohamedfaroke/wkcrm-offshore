﻿// This is a "safe" class, meaning that it is created once 
// and never overwritten. Any custom code you add to this class 
// will be preserved when you regenerate your application.
//
// Typical customizations that may be done in this class include
//  - adding custom event handlers
//  - overriding base class methods

using System;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// Provides access to the schema information and record data of a database table (or view).
/// See <see cref="BaseContractPaymentTypeTable"></see> for additional information.
/// </summary>
/// <remarks>
/// See <see cref="BaseContractPaymentTypeTable"></see> for additional information.
/// <para>
/// This class is implemented using the Singleton design pattern.
/// </para>
/// </remarks>
/// <seealso cref="BaseContractPaymentTypeTable"></seealso>
/// <seealso cref="BaseContractPaymentTypeSqlTable"></seealso>
/// <seealso cref="ContractPaymentTypeSqlTable"></seealso>
/// <seealso cref="ContractPaymentTypeDefinition"></seealso>
/// <seealso cref="ContractPaymentTypeRecord"></seealso>
/// <seealso cref="BaseContractPaymentTypeRecord"></seealso>
[SerializableAttribute()]
public class ContractPaymentTypeTable : BaseContractPaymentTypeTable, System.Runtime.Serialization.ISerializable, ISingleton
{

#region "ISerializable Members"

    /// <summary>
    /// Overridden to use the <see cref="ContractPaymentTypeTable_SerializationHelper"></see> class 
    /// for deserialization of <see cref="ContractPaymentTypeTable"></see> data.
    /// </summary>
    /// <remarks>
    /// Since the <see cref="ContractPaymentTypeTable"></see> class is implemented using the Singleton design pattern, 
    /// this method must be overridden to prevent additional instances from being created during deserialization.
    /// </remarks>
    void System.Runtime.Serialization.ISerializable.GetObjectData(
        System.Runtime.Serialization.SerializationInfo info, 
        System.Runtime.Serialization.StreamingContext context)
    {
        info.SetType(typeof(ContractPaymentTypeTable_SerializationHelper)); //No other values need to be added
    }

#region "Class ContractPaymentTypeTable_SerializationHelper"

    [SerializableAttribute()]
    private class ContractPaymentTypeTable_SerializationHelper: System.Runtime.Serialization.IObjectReference
    {
        //Method called after this object is deserialized
        public virtual object GetRealObject(System.Runtime.Serialization.StreamingContext context)
        {
            return ContractPaymentTypeTable.Instance;
        }
    }

#endregion

#endregion

    /// <summary>
    /// References the only instance of the <see cref="ContractPaymentTypeTable"></see> class.
    /// </summary>
    /// <remarks>
    /// Since the <see cref="ContractPaymentTypeTable"></see> class is implemented using the Singleton design pattern, 
    /// this field is the only way to access an instance of the class.
    /// </remarks>
    public readonly static ContractPaymentTypeTable Instance = new ContractPaymentTypeTable();

    private ContractPaymentTypeTable()
    {
    }


} // End class ContractPaymentTypeTable

}
