﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractItemTotalsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractItemTotalsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractItemTotalsView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractItemTotalsView"></seealso>
/// <seealso cref="View_ContractItemTotalsRecord"></seealso>
public class BaseView_ContractItemTotalsRecord : PrimaryKeyRecord
{

	public readonly static View_ContractItemTotalsView TableUtils = View_ContractItemTotalsView.Instance;

	// Constructors
 
	protected BaseView_ContractItemTotalsRecord() : base(TableUtils)
	{
	}

	protected BaseView_ContractItemTotalsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public ColumnValue Getcontract_idValue()
	{
		return this.GetValue(TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public Decimal Getcontract_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public ColumnValue GetTotalAmountValue()
	{
		return this.GetValue(TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public Decimal GetTotalAmountFieldValue()
	{
		return this.GetValue(TableUtils.TotalAmountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(string val)
	{
		this.SetString(val, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public void SetTotalAmountFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TotalAmountColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public Decimal contract_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.contract_id field.
	/// </summary>
	public string contract_idDefault
	{
		get
		{
			return TableUtils.contract_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public Decimal TotalAmount
	{
		get
		{
			return this.GetValue(TableUtils.TotalAmountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TotalAmountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TotalAmountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TotalAmountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractItemTotals_.TotalAmount field.
	/// </summary>
	public string TotalAmountDefault
	{
		get
		{
			return TableUtils.TotalAmountColumn.DefaultValue;
		}
	}


#endregion
}

}
