﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_OppConversionReportByHostessRecord.vb

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_OppConversionReportByHostessRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_OppConversionReportByHostessView"></see> class.
/// </remarks>
/// <seealso cref="View_OppConversionReportByHostessView"></seealso>
/// <seealso cref="View_OppConversionReportByHostessRecord"></seealso>
public class BaseView_OppConversionReportByHostessRecord : KeylessRecord
{

	public readonly static View_OppConversionReportByHostessView TableUtils = View_OppConversionReportByHostessView.Instance;

	// Constructors
 
	protected BaseView_OppConversionReportByHostessRecord() : base(TableUtils)
	{
	}

	protected BaseView_OppConversionReportByHostessRecord(KeylessRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public void Setid0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public void Setid0FieldValue(string val)
	{
		this.SetString(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public void Setid0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public void Setid0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public void Setid0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public ColumnValue GetLeadsValue()
	{
		return this.GetValue(TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public Int32 GetLeadsFieldValue()
	{
		return this.GetValue(TableUtils.LeadsColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(string val)
	{
		this.SetString(val, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.LeadsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public void SetLeadsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.LeadsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public ColumnValue GetOpsValue()
	{
		return this.GetValue(TableUtils.OpsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public Int32 GetOpsFieldValue()
	{
		return this.GetValue(TableUtils.OpsColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public void SetOpsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OpsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public void SetOpsFieldValue(string val)
	{
		this.SetString(val, TableUtils.OpsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public void SetOpsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OpsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public void SetOpsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OpsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public void SetOpsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OpsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public ColumnValue GetMonthValue()
	{
		return this.GetValue(TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public Int32 GetMonthFieldValue()
	{
		return this.GetValue(TableUtils.MonthColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public void SetMonthFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public void SetMonthFieldValue(string val)
	{
		this.SetString(val, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public void SetMonthFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public void SetMonthFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.MonthColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public void SetMonthFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.MonthColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public ColumnValue GetYearValue()
	{
		return this.GetValue(TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public Int32 GetYearFieldValue()
	{
		return this.GetValue(TableUtils.YearColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public void SetYearFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public void SetYearFieldValue(string val)
	{
		this.SetString(val, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public void SetYearFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public void SetYearFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.YearColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public void SetYearFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.YearColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByHostess_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public Int32 Leads
	{
		get
		{
			return this.GetValue(TableUtils.LeadsColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.LeadsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool LeadsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.LeadsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Leads field.
	/// </summary>
	public string LeadsDefault
	{
		get
		{
			return TableUtils.LeadsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public Int32 Ops
	{
		get
		{
			return this.GetValue(TableUtils.OpsColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OpsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OpsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OpsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Ops field.
	/// </summary>
	public string OpsDefault
	{
		get
		{
			return TableUtils.OpsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public Int32 Month
	{
		get
		{
			return this.GetValue(TableUtils.MonthColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.MonthColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool MonthSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.MonthColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Month field.
	/// </summary>
	public string MonthDefault
	{
		get
		{
			return TableUtils.MonthColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public Int32 Year
	{
		get
		{
			return this.GetValue(TableUtils.YearColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.YearColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool YearSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.YearColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_OppConversionReportByHostess_.Year field.
	/// </summary>
	public string YearDefault
	{
		get
		{
			return TableUtils.YearColumn.DefaultValue;
		}
	}


#endregion

}

}
