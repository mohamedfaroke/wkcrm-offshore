﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractTradeItemAndTradeTypeView.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractTradeItemAndTradeTypeView"></see> class.
/// Provides access to the schema information and record data of a database table or view named View_ContractTradeItemAndTradeType.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="View_ContractTradeItemAndTradeTypeView.Instance">View_ContractTradeItemAndTradeTypeView.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="View_ContractTradeItemAndTradeTypeView"></seealso>
[SerializableAttribute()]
public class BaseView_ContractTradeItemAndTradeTypeView : PrimaryKeyTable
{

    private readonly string TableDefinitionString = View_ContractTradeItemAndTradeTypeDefinition.GetXMLString();







    protected BaseView_ContractTradeItemAndTradeTypeView()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.View_ContractTradeItemAndTradeTypeView");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.View_ContractTradeItemAndTradeTypeRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new View_ContractTradeItemAndTradeTypeSqlView();
        ((View_ContractTradeItemAndTradeTypeSqlView)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn id0Column
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn id0
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.id0Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.contract_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn contract_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.contract_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn contract_id
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.contract_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.trade_task_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn trade_task_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.trade_task_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn trade_task_id
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.trade_task_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.units column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn unitsColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.units column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn units
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.unitsColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.price column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn priceColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.price column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn price
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.priceColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.amount column object.
    /// </summary>
    public BaseClasses.Data.CurrencyColumn amountColumn
    {
        get
        {
            return (BaseClasses.Data.CurrencyColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.amount column object.
    /// </summary>
    public static BaseClasses.Data.CurrencyColumn amount
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.amountColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.notes column object.
    /// </summary>
    public BaseClasses.Data.StringColumn notesColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.notes column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn notes
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.notesColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.Trade column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn TradeColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[7];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's View_ContractTradeItemAndTradeType_.Trade column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn Trade
    {
        get
        {
            return View_ContractTradeItemAndTradeTypeView.Instance.TradeColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of View_ContractTradeItemAndTradeTypeRecord records using a where clause.
    /// </summary>
    public static View_ContractTradeItemAndTradeTypeRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of View_ContractTradeItemAndTradeTypeRecord records using a where and order by clause.
    /// </summary>
    public static View_ContractTradeItemAndTradeTypeRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of View_ContractTradeItemAndTradeTypeRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static View_ContractTradeItemAndTradeTypeRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = View_ContractTradeItemAndTradeTypeView.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (View_ContractTradeItemAndTradeTypeRecord[])recList.ToArray(Type.GetType("WKCRM.Business.View_ContractTradeItemAndTradeTypeRecord"));
    }   
    
    public static View_ContractTradeItemAndTradeTypeRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = View_ContractTradeItemAndTradeTypeView.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (View_ContractTradeItemAndTradeTypeRecord[])recList.ToArray(Type.GetType("WKCRM.Business.View_ContractTradeItemAndTradeTypeRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)View_ContractTradeItemAndTradeTypeView.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)View_ContractTradeItemAndTradeTypeView.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a View_ContractTradeItemAndTradeTypeRecord record using a where clause.
    /// </summary>
    public static View_ContractTradeItemAndTradeTypeRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a View_ContractTradeItemAndTradeTypeRecord record using a where and order by clause.
    /// </summary>
    public static View_ContractTradeItemAndTradeTypeRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = View_ContractTradeItemAndTradeTypeView.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        View_ContractTradeItemAndTradeTypeRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (View_ContractTradeItemAndTradeTypeRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return View_ContractTradeItemAndTradeTypeView.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        View_ContractTradeItemAndTradeTypeRecord[] recs = GetRecords(where);
        return  View_ContractTradeItemAndTradeTypeView.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        View_ContractTradeItemAndTradeTypeRecord[] recs = GetRecords(where, orderBy);
        return  View_ContractTradeItemAndTradeTypeView.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        View_ContractTradeItemAndTradeTypeRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  View_ContractTradeItemAndTradeTypeView.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        View_ContractTradeItemAndTradeTypeView.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  View_ContractTradeItemAndTradeTypeView.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return View_ContractTradeItemAndTradeTypeView.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return View_ContractTradeItemAndTradeTypeView.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return View_ContractTradeItemAndTradeTypeView.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return View_ContractTradeItemAndTradeTypeView.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return View_ContractTradeItemAndTradeTypeView.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static View_ContractTradeItemAndTradeTypeRecord GetRecord(string id, bool bMutable)
        {
            return (View_ContractTradeItemAndTradeTypeRecord)View_ContractTradeItemAndTradeTypeView.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static View_ContractTradeItemAndTradeTypeRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (View_ContractTradeItemAndTradeTypeRecord)View_ContractTradeItemAndTradeTypeView.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string id0Value, 
        string contract_idValue, 
        string trade_task_idValue, 
        string unitsValue, 
        string priceValue, 
        string amountValue, 
        string notesValue, 
        string TradeValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(id0Value, id0Column);
        rec.SetString(contract_idValue, contract_idColumn);
        rec.SetString(trade_task_idValue, trade_task_idColumn);
        rec.SetString(unitsValue, unitsColumn);
        rec.SetString(priceValue, priceColumn);
        rec.SetString(amountValue, amountColumn);
        rec.SetString(notesValue, notesColumn);
        rec.SetString(TradeValue, TradeColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			View_ContractTradeItemAndTradeTypeView.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				View_ContractTradeItemAndTradeTypeView.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = View_ContractTradeItemAndTradeTypeView.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
