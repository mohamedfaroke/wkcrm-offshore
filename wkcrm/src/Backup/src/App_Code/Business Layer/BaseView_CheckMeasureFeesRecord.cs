﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_CheckMeasureFeesRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_CheckMeasureFeesRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_CheckMeasureFeesView"></see> class.
/// </remarks>
/// <seealso cref="View_CheckMeasureFeesView"></seealso>
/// <seealso cref="View_CheckMeasureFeesRecord"></seealso>
public class BaseView_CheckMeasureFeesRecord : PrimaryKeyRecord
{

	public readonly static View_CheckMeasureFeesView TableUtils = View_CheckMeasureFeesView.Instance;

	// Constructors
 
	protected BaseView_CheckMeasureFeesRecord() : base(TableUtils)
	{
	}

	protected BaseView_CheckMeasureFeesRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public ColumnValue Getlower_limitValue()
	{
		return this.GetValue(TableUtils.lower_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public Decimal Getlower_limitFieldValue()
	{
		return this.GetValue(TableUtils.lower_limitColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public void Setlower_limitFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.lower_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public void Setlower_limitFieldValue(string val)
	{
		this.SetString(val, TableUtils.lower_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public void Setlower_limitFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.lower_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public void Setlower_limitFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.lower_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public void Setlower_limitFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.lower_limitColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public ColumnValue Getupper_limitValue()
	{
		return this.GetValue(TableUtils.upper_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public Decimal Getupper_limitFieldValue()
	{
		return this.GetValue(TableUtils.upper_limitColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public void Setupper_limitFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.upper_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public void Setupper_limitFieldValue(string val)
	{
		this.SetString(val, TableUtils.upper_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public void Setupper_limitFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.upper_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public void Setupper_limitFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.upper_limitColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public void Setupper_limitFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.upper_limitColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public ColumnValue Getcheckmeasure_feeValue()
	{
		return this.GetValue(TableUtils.checkmeasure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public Decimal Getcheckmeasure_feeFieldValue()
	{
		return this.GetValue(TableUtils.checkmeasure_feeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public void Setcheckmeasure_feeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.checkmeasure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public void Setcheckmeasure_feeFieldValue(string val)
	{
		this.SetString(val, TableUtils.checkmeasure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public void Setcheckmeasure_feeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.checkmeasure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public void Setcheckmeasure_feeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.checkmeasure_feeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public void Setcheckmeasure_feeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.checkmeasure_feeColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_CheckMeasureFees_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public Decimal lower_limit
	{
		get
		{
			return this.GetValue(TableUtils.lower_limitColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.lower_limitColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool lower_limitSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.lower_limitColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.lower_limit field.
	/// </summary>
	public string lower_limitDefault
	{
		get
		{
			return TableUtils.lower_limitColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public Decimal upper_limit
	{
		get
		{
			return this.GetValue(TableUtils.upper_limitColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.upper_limitColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool upper_limitSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.upper_limitColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.upper_limit field.
	/// </summary>
	public string upper_limitDefault
	{
		get
		{
			return TableUtils.upper_limitColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public Decimal checkmeasure_fee
	{
		get
		{
			return this.GetValue(TableUtils.checkmeasure_feeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.checkmeasure_feeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool checkmeasure_feeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.checkmeasure_feeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_CheckMeasureFees_.checkmeasure_fee field.
	/// </summary>
	public string checkmeasure_feeDefault
	{
		get
		{
			return TableUtils.checkmeasure_feeColumn.DefaultValue;
		}
	}


#endregion
}

}
