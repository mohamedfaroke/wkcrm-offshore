﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in QuestionOptionRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="QuestionOptionRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="QuestionOptionTable"></see> class.
/// </remarks>
/// <seealso cref="QuestionOptionTable"></seealso>
/// <seealso cref="QuestionOptionRecord"></seealso>
public class BaseQuestionOptionRecord : PrimaryKeyRecord
{

	public readonly static QuestionOptionTable TableUtils = QuestionOptionTable.Instance;

	// Constructors
 
	protected BaseQuestionOptionRecord() : base(TableUtils)
	{
	}

	protected BaseQuestionOptionRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.OptionID field.
	/// </summary>
	public ColumnValue GetOptionIDValue()
	{
		return this.GetValue(TableUtils.OptionIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.OptionID field.
	/// </summary>
	public Int32 GetOptionIDFieldValue()
	{
		return this.GetValue(TableUtils.OptionIDColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.OptionText field.
	/// </summary>
	public ColumnValue GetOptionTextValue()
	{
		return this.GetValue(TableUtils.OptionTextColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.OptionText field.
	/// </summary>
	public string GetOptionTextFieldValue()
	{
		return this.GetValue(TableUtils.OptionTextColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.OptionText field.
	/// </summary>
	public void SetOptionTextFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OptionTextColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.OptionText field.
	/// </summary>
	public void SetOptionTextFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionTextColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.OptionValue field.
	/// </summary>
	public ColumnValue GetOptionValueValue()
	{
		return this.GetValue(TableUtils.OptionValueColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.OptionValue field.
	/// </summary>
	public string GetOptionValueFieldValue()
	{
		return this.GetValue(TableUtils.OptionValueColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.OptionValue field.
	/// </summary>
	public void SetOptionValueFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OptionValueColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.OptionValue field.
	/// </summary>
	public void SetOptionValueFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionValueColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public ColumnValue GetDisplayOrderValue()
	{
		return this.GetValue(TableUtils.DisplayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public Int32 GetDisplayOrderFieldValue()
	{
		return this.GetValue(TableUtils.DisplayOrderColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public void SetDisplayOrderFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.DisplayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public void SetDisplayOrderFieldValue(string val)
	{
		this.SetString(val, TableUtils.DisplayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public void SetDisplayOrderFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.DisplayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public void SetDisplayOrderFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.DisplayOrderColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public void SetDisplayOrderFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.DisplayOrderColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's QuestionOption_.OptionID field.
	/// </summary>
	public Int32 OptionID
	{
		get
		{
			return this.GetValue(TableUtils.OptionIDColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OptionIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OptionIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OptionIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.OptionID field.
	/// </summary>
	public string OptionIDDefault
	{
		get
		{
			return TableUtils.OptionIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's QuestionOption_.OptionText field.
	/// </summary>
	public string OptionText
	{
		get
		{
			return this.GetValue(TableUtils.OptionTextColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OptionTextColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OptionTextSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OptionTextColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.OptionText field.
	/// </summary>
	public string OptionTextDefault
	{
		get
		{
			return TableUtils.OptionTextColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's QuestionOption_.OptionValue field.
	/// </summary>
	public string OptionValue
	{
		get
		{
			return this.GetValue(TableUtils.OptionValueColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OptionValueColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OptionValueSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OptionValueColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.OptionValue field.
	/// </summary>
	public string OptionValueDefault
	{
		get
		{
			return TableUtils.OptionValueColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public Int32 DisplayOrder
	{
		get
		{
			return this.GetValue(TableUtils.DisplayOrderColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.DisplayOrderColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool DisplayOrderSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.DisplayOrderColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionOption_.DisplayOrder field.
	/// </summary>
	public string DisplayOrderDefault
	{
		get
		{
			return TableUtils.DisplayOrderColumn.DefaultValue;
		}
	}


#endregion
}

}
