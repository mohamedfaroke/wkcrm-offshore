﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_AccountLeadPhaseRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_AccountLeadPhaseRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_AccountLeadPhaseView"></see> class.
/// </remarks>
/// <seealso cref="View_AccountLeadPhaseView"></seealso>
/// <seealso cref="View_AccountLeadPhaseRecord"></seealso>
public class BaseView_AccountLeadPhaseRecord : PrimaryKeyRecord
{

	public readonly static View_AccountLeadPhaseView TableUtils = View_AccountLeadPhaseView.Instance;

	// Constructors
 
	protected BaseView_AccountLeadPhaseRecord() : base(TableUtils)
	{
	}

	protected BaseView_AccountLeadPhaseRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public void Setid0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public void Setid0FieldValue(string val)
	{
		this.SetString(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public void Setid0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public void Setid0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public void Setid0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.Name field.
	/// </summary>
	public ColumnValue GetNameValue()
	{
		return this.GetValue(TableUtils.NameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.Name field.
	/// </summary>
	public string GetNameFieldValue()
	{
		return this.GetValue(TableUtils.NameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.Name field.
	/// </summary>
	public void SetNameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.NameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.Name field.
	/// </summary>
	public void SetNameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.NameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.home_phone field.
	/// </summary>
	public ColumnValue Gethome_phoneValue()
	{
		return this.GetValue(TableUtils.home_phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.home_phone field.
	/// </summary>
	public string Gethome_phoneFieldValue()
	{
		return this.GetValue(TableUtils.home_phoneColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.home_phone field.
	/// </summary>
	public void Sethome_phoneFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.home_phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.home_phone field.
	/// </summary>
	public void Sethome_phoneFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.home_phoneColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.business_phone field.
	/// </summary>
	public ColumnValue Getbusiness_phoneValue()
	{
		return this.GetValue(TableUtils.business_phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.business_phone field.
	/// </summary>
	public string Getbusiness_phoneFieldValue()
	{
		return this.GetValue(TableUtils.business_phoneColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.business_phone field.
	/// </summary>
	public void Setbusiness_phoneFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.business_phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.business_phone field.
	/// </summary>
	public void Setbusiness_phoneFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.business_phoneColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.mobile field.
	/// </summary>
	public ColumnValue GetmobileValue()
	{
		return this.GetValue(TableUtils.mobileColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.mobile field.
	/// </summary>
	public string GetmobileFieldValue()
	{
		return this.GetValue(TableUtils.mobileColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.mobile field.
	/// </summary>
	public void SetmobileFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.mobileColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.mobile field.
	/// </summary>
	public void SetmobileFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.mobileColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.mobile2 field.
	/// </summary>
	public ColumnValue Getmobile2Value()
	{
		return this.GetValue(TableUtils.mobile2Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.mobile2 field.
	/// </summary>
	public string Getmobile2FieldValue()
	{
		return this.GetValue(TableUtils.mobile2Column).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.mobile2 field.
	/// </summary>
	public void Setmobile2FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.mobile2Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.mobile2 field.
	/// </summary>
	public void Setmobile2FieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.mobile2Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.business_phone2 field.
	/// </summary>
	public ColumnValue Getbusiness_phone2Value()
	{
		return this.GetValue(TableUtils.business_phone2Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.business_phone2 field.
	/// </summary>
	public string Getbusiness_phone2FieldValue()
	{
		return this.GetValue(TableUtils.business_phone2Column).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.business_phone2 field.
	/// </summary>
	public void Setbusiness_phone2FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.business_phone2Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.business_phone2 field.
	/// </summary>
	public void Setbusiness_phone2FieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.business_phone2Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.builders_phone field.
	/// </summary>
	public ColumnValue Getbuilders_phoneValue()
	{
		return this.GetValue(TableUtils.builders_phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.builders_phone field.
	/// </summary>
	public string Getbuilders_phoneFieldValue()
	{
		return this.GetValue(TableUtils.builders_phoneColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.builders_phone field.
	/// </summary>
	public void Setbuilders_phoneFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.builders_phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.builders_phone field.
	/// </summary>
	public void Setbuilders_phoneFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.builders_phoneColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.postal_address field.
	/// </summary>
	public ColumnValue Getpostal_addressValue()
	{
		return this.GetValue(TableUtils.postal_addressColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.postal_address field.
	/// </summary>
	public string Getpostal_addressFieldValue()
	{
		return this.GetValue(TableUtils.postal_addressColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.postal_address field.
	/// </summary>
	public void Setpostal_addressFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.postal_addressColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.postal_address field.
	/// </summary>
	public void Setpostal_addressFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.postal_addressColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.builders_name field.
	/// </summary>
	public ColumnValue Getbuilders_nameValue()
	{
		return this.GetValue(TableUtils.builders_nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.builders_name field.
	/// </summary>
	public string Getbuilders_nameFieldValue()
	{
		return this.GetValue(TableUtils.builders_nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.builders_name field.
	/// </summary>
	public void Setbuilders_nameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.builders_nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.builders_name field.
	/// </summary>
	public void Setbuilders_nameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.builders_nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public ColumnValue Getdesigner_idValue()
	{
		return this.GetValue(TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public Decimal Getdesigner_idFieldValue()
	{
		return this.GetValue(TableUtils.designer_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public void Setdesigner_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.designer_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.address field.
	/// </summary>
	public ColumnValue GetaddressValue()
	{
		return this.GetValue(TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.address field.
	/// </summary>
	public string GetaddressFieldValue()
	{
		return this.GetValue(TableUtils.addressColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.address field.
	/// </summary>
	public void SetaddressFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.address field.
	/// </summary>
	public void SetaddressFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.addressColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.suburb field.
	/// </summary>
	public ColumnValue GetsuburbValue()
	{
		return this.GetValue(TableUtils.suburbColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.suburb field.
	/// </summary>
	public string GetsuburbFieldValue()
	{
		return this.GetValue(TableUtils.suburbColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.suburb field.
	/// </summary>
	public void SetsuburbFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.suburbColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.suburb field.
	/// </summary>
	public void SetsuburbFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.suburbColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.postcode field.
	/// </summary>
	public ColumnValue GetpostcodeValue()
	{
		return this.GetValue(TableUtils.postcodeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.postcode field.
	/// </summary>
	public string GetpostcodeFieldValue()
	{
		return this.GetValue(TableUtils.postcodeColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.postcode field.
	/// </summary>
	public void SetpostcodeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.postcodeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.postcode field.
	/// </summary>
	public void SetpostcodeFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.postcodeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.email field.
	/// </summary>
	public ColumnValue GetemailValue()
	{
		return this.GetValue(TableUtils.emailColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.email field.
	/// </summary>
	public string GetemailFieldValue()
	{
		return this.GetValue(TableUtils.emailColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.email field.
	/// </summary>
	public void SetemailFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.emailColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.email field.
	/// </summary>
	public void SetemailFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.emailColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.fax field.
	/// </summary>
	public ColumnValue GetfaxValue()
	{
		return this.GetValue(TableUtils.faxColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.fax field.
	/// </summary>
	public string GetfaxFieldValue()
	{
		return this.GetValue(TableUtils.faxColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.fax field.
	/// </summary>
	public void SetfaxFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.faxColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.fax field.
	/// </summary>
	public void SetfaxFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.faxColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public ColumnValue Getaccount_type_idValue()
	{
		return this.GetValue(TableUtils.account_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public Int32 Getaccount_type_idFieldValue()
	{
		return this.GetValue(TableUtils.account_type_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public void Setaccount_type_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.account_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public void Setaccount_type_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.account_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public void Setaccount_type_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public void Setaccount_type_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public void Setaccount_type_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_type_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public ColumnValue Getaccount_status_idValue()
	{
		return this.GetValue(TableUtils.account_status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public Int32 Getaccount_status_idFieldValue()
	{
		return this.GetValue(TableUtils.account_status_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public void Setaccount_status_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.account_status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public void Setaccount_status_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.account_status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public void Setaccount_status_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public void Setaccount_status_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public void Setaccount_status_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_status_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.has_been_revived field.
	/// </summary>
	public ColumnValue Gethas_been_revivedValue()
	{
		return this.GetValue(TableUtils.has_been_revivedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.has_been_revived field.
	/// </summary>
	public bool Gethas_been_revivedFieldValue()
	{
		return this.GetValue(TableUtils.has_been_revivedColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.has_been_revived field.
	/// </summary>
	public void Sethas_been_revivedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.has_been_revivedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.has_been_revived field.
	/// </summary>
	public void Sethas_been_revivedFieldValue(string val)
	{
		this.SetString(val, TableUtils.has_been_revivedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.has_been_revived field.
	/// </summary>
	public void Sethas_been_revivedFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.has_been_revivedColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.next_review_date field.
	/// </summary>
	public ColumnValue Getnext_review_dateValue()
	{
		return this.GetValue(TableUtils.next_review_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.next_review_date field.
	/// </summary>
	public DateTime Getnext_review_dateFieldValue()
	{
		return this.GetValue(TableUtils.next_review_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.next_review_date field.
	/// </summary>
	public void Setnext_review_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.next_review_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.next_review_date field.
	/// </summary>
	public void Setnext_review_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.next_review_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.next_review_date field.
	/// </summary>
	public void Setnext_review_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.next_review_dateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public ColumnValue Getcontact_source_idValue()
	{
		return this.GetValue(TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public Int32 Getcontact_source_idFieldValue()
	{
		return this.GetValue(TableUtils.contact_source_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contact_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public void Setcontact_source_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contact_source_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public ColumnValue Getopportunity_source_idValue()
	{
		return this.GetValue(TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public Int32 Getopportunity_source_idFieldValue()
	{
		return this.GetValue(TableUtils.opportunity_source_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_source_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public void Setopportunity_source_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.opportunity_source_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.revived_reason field.
	/// </summary>
	public ColumnValue Getrevived_reasonValue()
	{
		return this.GetValue(TableUtils.revived_reasonColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.revived_reason field.
	/// </summary>
	public string Getrevived_reasonFieldValue()
	{
		return this.GetValue(TableUtils.revived_reasonColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.revived_reason field.
	/// </summary>
	public void Setrevived_reasonFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.revived_reasonColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.revived_reason field.
	/// </summary>
	public void Setrevived_reasonFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.revived_reasonColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.left_reason field.
	/// </summary>
	public ColumnValue Getleft_reasonValue()
	{
		return this.GetValue(TableUtils.left_reasonColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.left_reason field.
	/// </summary>
	public string Getleft_reasonFieldValue()
	{
		return this.GetValue(TableUtils.left_reasonColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.left_reason field.
	/// </summary>
	public void Setleft_reasonFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.left_reasonColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.left_reason field.
	/// </summary>
	public void Setleft_reasonFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.left_reasonColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.date_created field.
	/// </summary>
	public ColumnValue Getdate_createdValue()
	{
		return this.GetValue(TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.date_created field.
	/// </summary>
	public DateTime Getdate_createdFieldValue()
	{
		return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_createdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public ColumnValue Getweeks_openValue()
	{
		return this.GetValue(TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public Int32 Getweeks_openFieldValue()
	{
		return this.GetValue(TableUtils.weeks_openColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(string val)
	{
		this.SetString(val, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.weeks_openColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public void Setweeks_openFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.weeks_openColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public ColumnValue Getdoor_typeValue()
	{
		return this.GetValue(TableUtils.door_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public Int32 Getdoor_typeFieldValue()
	{
		return this.GetValue(TableUtils.door_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public void Setdoor_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.door_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public void Setdoor_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.door_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public void Setdoor_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public void Setdoor_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public void Setdoor_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.door_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public ColumnValue Getbenchtop_typeValue()
	{
		return this.GetValue(TableUtils.benchtop_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public Int32 Getbenchtop_typeFieldValue()
	{
		return this.GetValue(TableUtils.benchtop_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public void Setbenchtop_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.benchtop_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public void Setbenchtop_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.benchtop_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public void Setbenchtop_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtop_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public void Setbenchtop_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtop_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public void Setbenchtop_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.benchtop_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public ColumnValue Getprice_rangeValue()
	{
		return this.GetValue(TableUtils.price_rangeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public Decimal Getprice_rangeFieldValue()
	{
		return this.GetValue(TableUtils.price_rangeColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public void Setprice_rangeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.price_rangeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public void Setprice_rangeFieldValue(string val)
	{
		this.SetString(val, TableUtils.price_rangeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public void Setprice_rangeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.price_rangeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public void Setprice_rangeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.price_rangeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public void Setprice_rangeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.price_rangeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.time_frame field.
	/// </summary>
	public ColumnValue Gettime_frameValue()
	{
		return this.GetValue(TableUtils.time_frameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.time_frame field.
	/// </summary>
	public string Gettime_frameFieldValue()
	{
		return this.GetValue(TableUtils.time_frameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.time_frame field.
	/// </summary>
	public void Settime_frameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.time_frameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.time_frame field.
	/// </summary>
	public void Settime_frameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.time_frameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.FirstName field.
	/// </summary>
	public ColumnValue GetFirstNameValue()
	{
		return this.GetValue(TableUtils.FirstNameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.FirstName field.
	/// </summary>
	public string GetFirstNameFieldValue()
	{
		return this.GetValue(TableUtils.FirstNameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.FirstName field.
	/// </summary>
	public void SetFirstNameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.FirstNameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.FirstName field.
	/// </summary>
	public void SetFirstNameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.FirstNameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.LastName field.
	/// </summary>
	public ColumnValue GetLastNameValue()
	{
		return this.GetValue(TableUtils.LastNameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.LastName field.
	/// </summary>
	public string GetLastNameFieldValue()
	{
		return this.GetValue(TableUtils.LastNameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.LastName field.
	/// </summary>
	public void SetLastNameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.LastNameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.LastName field.
	/// </summary>
	public void SetLastNameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.LastNameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.sale_date field.
	/// </summary>
	public ColumnValue Getsale_dateValue()
	{
		return this.GetValue(TableUtils.sale_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_AccountLeadPhase_.sale_date field.
	/// </summary>
	public DateTime Getsale_dateFieldValue()
	{
		return this.GetValue(TableUtils.sale_dateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.sale_date field.
	/// </summary>
	public void Setsale_dateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.sale_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.sale_date field.
	/// </summary>
	public void Setsale_dateFieldValue(string val)
	{
		this.SetString(val, TableUtils.sale_dateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.sale_date field.
	/// </summary>
	public void Setsale_dateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.sale_dateColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.Name field.
	/// </summary>
	public string Name
	{
		get
		{
			return this.GetValue(TableUtils.NameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.NameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool NameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.NameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.Name field.
	/// </summary>
	public string NameDefault
	{
		get
		{
			return TableUtils.NameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.home_phone field.
	/// </summary>
	public string home_phone
	{
		get
		{
			return this.GetValue(TableUtils.home_phoneColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.home_phoneColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool home_phoneSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.home_phoneColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.home_phone field.
	/// </summary>
	public string home_phoneDefault
	{
		get
		{
			return TableUtils.home_phoneColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.business_phone field.
	/// </summary>
	public string business_phone
	{
		get
		{
			return this.GetValue(TableUtils.business_phoneColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.business_phoneColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool business_phoneSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.business_phoneColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.business_phone field.
	/// </summary>
	public string business_phoneDefault
	{
		get
		{
			return TableUtils.business_phoneColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.mobile field.
	/// </summary>
	public string mobile
	{
		get
		{
			return this.GetValue(TableUtils.mobileColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.mobileColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool mobileSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.mobileColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.mobile field.
	/// </summary>
	public string mobileDefault
	{
		get
		{
			return TableUtils.mobileColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.mobile2 field.
	/// </summary>
	public string mobile2
	{
		get
		{
			return this.GetValue(TableUtils.mobile2Column).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.mobile2Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool mobile2Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.mobile2Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.mobile2 field.
	/// </summary>
	public string mobile2Default
	{
		get
		{
			return TableUtils.mobile2Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.business_phone2 field.
	/// </summary>
	public string business_phone2
	{
		get
		{
			return this.GetValue(TableUtils.business_phone2Column).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.business_phone2Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool business_phone2Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.business_phone2Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.business_phone2 field.
	/// </summary>
	public string business_phone2Default
	{
		get
		{
			return TableUtils.business_phone2Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.builders_phone field.
	/// </summary>
	public string builders_phone
	{
		get
		{
			return this.GetValue(TableUtils.builders_phoneColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.builders_phoneColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool builders_phoneSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.builders_phoneColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.builders_phone field.
	/// </summary>
	public string builders_phoneDefault
	{
		get
		{
			return TableUtils.builders_phoneColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.postal_address field.
	/// </summary>
	public string postal_address
	{
		get
		{
			return this.GetValue(TableUtils.postal_addressColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.postal_addressColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool postal_addressSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.postal_addressColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.postal_address field.
	/// </summary>
	public string postal_addressDefault
	{
		get
		{
			return TableUtils.postal_addressColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.builders_name field.
	/// </summary>
	public string builders_name
	{
		get
		{
			return this.GetValue(TableUtils.builders_nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.builders_nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool builders_nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.builders_nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.builders_name field.
	/// </summary>
	public string builders_nameDefault
	{
		get
		{
			return TableUtils.builders_nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public Decimal designer_id
	{
		get
		{
			return this.GetValue(TableUtils.designer_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.designer_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool designer_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.designer_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.designer_id field.
	/// </summary>
	public string designer_idDefault
	{
		get
		{
			return TableUtils.designer_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.address field.
	/// </summary>
	public string address
	{
		get
		{
			return this.GetValue(TableUtils.addressColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.addressColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool addressSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.addressColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.address field.
	/// </summary>
	public string addressDefault
	{
		get
		{
			return TableUtils.addressColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.suburb field.
	/// </summary>
	public string suburb
	{
		get
		{
			return this.GetValue(TableUtils.suburbColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.suburbColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool suburbSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.suburbColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.suburb field.
	/// </summary>
	public string suburbDefault
	{
		get
		{
			return TableUtils.suburbColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.postcode field.
	/// </summary>
	public string postcode
	{
		get
		{
			return this.GetValue(TableUtils.postcodeColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.postcodeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool postcodeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.postcodeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.postcode field.
	/// </summary>
	public string postcodeDefault
	{
		get
		{
			return TableUtils.postcodeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.email field.
	/// </summary>
	public string email
	{
		get
		{
			return this.GetValue(TableUtils.emailColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.emailColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool emailSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.emailColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.email field.
	/// </summary>
	public string emailDefault
	{
		get
		{
			return TableUtils.emailColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.fax field.
	/// </summary>
	public string fax
	{
		get
		{
			return this.GetValue(TableUtils.faxColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.faxColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool faxSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.faxColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.fax field.
	/// </summary>
	public string faxDefault
	{
		get
		{
			return TableUtils.faxColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public Int32 account_type_id
	{
		get
		{
			return this.GetValue(TableUtils.account_type_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.account_type_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool account_type_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.account_type_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_type_id field.
	/// </summary>
	public string account_type_idDefault
	{
		get
		{
			return TableUtils.account_type_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public Int32 account_status_id
	{
		get
		{
			return this.GetValue(TableUtils.account_status_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.account_status_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool account_status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.account_status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.account_status_id field.
	/// </summary>
	public string account_status_idDefault
	{
		get
		{
			return TableUtils.account_status_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.has_been_revived field.
	/// </summary>
	public bool has_been_revived
	{
		get
		{
			return this.GetValue(TableUtils.has_been_revivedColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.has_been_revivedColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool has_been_revivedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.has_been_revivedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.has_been_revived field.
	/// </summary>
	public string has_been_revivedDefault
	{
		get
		{
			return TableUtils.has_been_revivedColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.next_review_date field.
	/// </summary>
	public DateTime next_review_date
	{
		get
		{
			return this.GetValue(TableUtils.next_review_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.next_review_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool next_review_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.next_review_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.next_review_date field.
	/// </summary>
	public string next_review_dateDefault
	{
		get
		{
			return TableUtils.next_review_dateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public Int32 contact_source_id
	{
		get
		{
			return this.GetValue(TableUtils.contact_source_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contact_source_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contact_source_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contact_source_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.contact_source_id field.
	/// </summary>
	public string contact_source_idDefault
	{
		get
		{
			return TableUtils.contact_source_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public Int32 opportunity_source_id
	{
		get
		{
			return this.GetValue(TableUtils.opportunity_source_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.opportunity_source_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool opportunity_source_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.opportunity_source_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.opportunity_source_id field.
	/// </summary>
	public string opportunity_source_idDefault
	{
		get
		{
			return TableUtils.opportunity_source_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.revived_reason field.
	/// </summary>
	public string revived_reason
	{
		get
		{
			return this.GetValue(TableUtils.revived_reasonColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.revived_reasonColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool revived_reasonSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.revived_reasonColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.revived_reason field.
	/// </summary>
	public string revived_reasonDefault
	{
		get
		{
			return TableUtils.revived_reasonColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.left_reason field.
	/// </summary>
	public string left_reason
	{
		get
		{
			return this.GetValue(TableUtils.left_reasonColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.left_reasonColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool left_reasonSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.left_reasonColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.left_reason field.
	/// </summary>
	public string left_reasonDefault
	{
		get
		{
			return TableUtils.left_reasonColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.date_created field.
	/// </summary>
	public DateTime date_created
	{
		get
		{
			return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_createdColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_createdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_createdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.date_created field.
	/// </summary>
	public string date_createdDefault
	{
		get
		{
			return TableUtils.date_createdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public Int32 weeks_open
	{
		get
		{
			return this.GetValue(TableUtils.weeks_openColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.weeks_openColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool weeks_openSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.weeks_openColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.weeks_open field.
	/// </summary>
	public string weeks_openDefault
	{
		get
		{
			return TableUtils.weeks_openColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public Int32 door_type
	{
		get
		{
			return this.GetValue(TableUtils.door_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.door_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool door_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.door_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.door_type field.
	/// </summary>
	public string door_typeDefault
	{
		get
		{
			return TableUtils.door_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public Int32 benchtop_type
	{
		get
		{
			return this.GetValue(TableUtils.benchtop_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.benchtop_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool benchtop_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.benchtop_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.benchtop_type field.
	/// </summary>
	public string benchtop_typeDefault
	{
		get
		{
			return TableUtils.benchtop_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public Decimal price_range
	{
		get
		{
			return this.GetValue(TableUtils.price_rangeColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.price_rangeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool price_rangeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.price_rangeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.price_range field.
	/// </summary>
	public string price_rangeDefault
	{
		get
		{
			return TableUtils.price_rangeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.time_frame field.
	/// </summary>
	public string time_frame
	{
		get
		{
			return this.GetValue(TableUtils.time_frameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.time_frameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool time_frameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.time_frameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.time_frame field.
	/// </summary>
	public string time_frameDefault
	{
		get
		{
			return TableUtils.time_frameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.FirstName field.
	/// </summary>
	public string FirstName
	{
		get
		{
			return this.GetValue(TableUtils.FirstNameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.FirstNameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool FirstNameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.FirstNameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.FirstName field.
	/// </summary>
	public string FirstNameDefault
	{
		get
		{
			return TableUtils.FirstNameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.LastName field.
	/// </summary>
	public string LastName
	{
		get
		{
			return this.GetValue(TableUtils.LastNameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.LastNameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool LastNameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.LastNameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.LastName field.
	/// </summary>
	public string LastNameDefault
	{
		get
		{
			return TableUtils.LastNameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_AccountLeadPhase_.sale_date field.
	/// </summary>
	public DateTime sale_date
	{
		get
		{
			return this.GetValue(TableUtils.sale_dateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.sale_dateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool sale_dateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.sale_dateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_AccountLeadPhase_.sale_date field.
	/// </summary>
	public string sale_dateDefault
	{
		get
		{
			return TableUtils.sale_dateColumn.DefaultValue;
		}
	}


#endregion
}

}
