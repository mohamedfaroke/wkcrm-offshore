﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradesmenTable.cs


using System;
using System.Data;
using System.Collections;
using System.Runtime;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;
using WKCRM.Data;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="TradesmenTable"></see> class.
/// Provides access to the schema information and record data of a database table or view named tradesmen.
/// </summary>
/// <remarks>
/// The connection details (name, location, etc.) of the database and table (or view) accessed by this class 
/// are resolved at runtime based on the connection string in the application's Web.Config file.
/// <para>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, use 
/// <see cref="TradesmenTable.Instance">TradesmenTable.Instance</see>.
/// </para>
/// </remarks>
/// <seealso cref="TradesmenTable"></seealso>
[SerializableAttribute()]
public class BaseTradesmenTable : PrimaryKeyTable
{

    private readonly string TableDefinitionString = TradesmenDefinition.GetXMLString();







    protected BaseTradesmenTable()
    {
        this.Initialize();
    }

    protected virtual void Initialize()
    {
        XmlTableDefinition def = new XmlTableDefinition(TableDefinitionString);
        this.TableDefinition = new TableDefinition();
        this.TableDefinition.TableClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.TradesmenTable");
        def.InitializeTableDefinition(this.TableDefinition);
        this.ConnectionName = def.GetConnectionName();
        this.RecordClassName = System.Reflection.Assembly.CreateQualifiedName("App_Code", "WKCRM.Business.TradesmenRecord");
        this.ApplicationName = "App_Code";
        this.DataAdapter = new TradesmenSqlTable();
        ((TradesmenSqlTable)this.DataAdapter).ConnectionName = this.ConnectionName;

        this.TableDefinition.AdapterMetaData = this.DataAdapter.AdapterMetaData;
    }

#region "Properties for columns"

    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn id0Column
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[0];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn id0
    {
        get
        {
            return TradesmenTable.Instance.id0Column;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.TradesmanTypeId column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn TradesmanTypeIdColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[1];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.TradesmanTypeId column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn TradesmanTypeId
    {
        get
        {
            return TradesmenTable.Instance.TradesmanTypeIdColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.name column object.
    /// </summary>
    public BaseClasses.Data.StringColumn nameColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[2];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.name column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn name
    {
        get
        {
            return TradesmenTable.Instance.nameColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.ABN column object.
    /// </summary>
    public BaseClasses.Data.StringColumn ABNColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[3];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.ABN column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn ABN
    {
        get
        {
            return TradesmenTable.Instance.ABNColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.dob column object.
    /// </summary>
    public BaseClasses.Data.DateColumn dobColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[4];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.dob column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn dob
    {
        get
        {
            return TradesmenTable.Instance.dobColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.phone column object.
    /// </summary>
    public BaseClasses.Data.StringColumn phoneColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[5];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.phone column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn phone
    {
        get
        {
            return TradesmenTable.Instance.phoneColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.mobile column object.
    /// </summary>
    public BaseClasses.Data.StringColumn mobileColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[6];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.mobile column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn mobile
    {
        get
        {
            return TradesmenTable.Instance.mobileColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.fax column object.
    /// </summary>
    public BaseClasses.Data.StringColumn faxColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[7];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.fax column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn fax
    {
        get
        {
            return TradesmenTable.Instance.faxColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.address column object.
    /// </summary>
    public BaseClasses.Data.StringColumn addressColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[8];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.address column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn address
    {
        get
        {
            return TradesmenTable.Instance.addressColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.email column object.
    /// </summary>
    public BaseClasses.Data.EmailColumn emailColumn
    {
        get
        {
            return (BaseClasses.Data.EmailColumn)this.TableDefinition.ColumnList[9];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.email column object.
    /// </summary>
    public static BaseClasses.Data.EmailColumn email
    {
        get
        {
            return TradesmenTable.Instance.emailColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.send_email_notifications column object.
    /// </summary>
    public BaseClasses.Data.BooleanColumn send_email_notificationsColumn
    {
        get
        {
            return (BaseClasses.Data.BooleanColumn)this.TableDefinition.ColumnList[10];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.send_email_notifications column object.
    /// </summary>
    public static BaseClasses.Data.BooleanColumn send_email_notifications
    {
        get
        {
            return TradesmenTable.Instance.send_email_notificationsColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.entity_type_id column object.
    /// </summary>
    public BaseClasses.Data.NumberColumn entity_type_idColumn
    {
        get
        {
            return (BaseClasses.Data.NumberColumn)this.TableDefinition.ColumnList[11];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.entity_type_id column object.
    /// </summary>
    public static BaseClasses.Data.NumberColumn entity_type_id
    {
        get
        {
            return TradesmenTable.Instance.entity_type_idColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.public_liability_company column object.
    /// </summary>
    public BaseClasses.Data.StringColumn public_liability_companyColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[12];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.public_liability_company column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn public_liability_company
    {
        get
        {
            return TradesmenTable.Instance.public_liability_companyColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.public_liability_number column object.
    /// </summary>
    public BaseClasses.Data.StringColumn public_liability_numberColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[13];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.public_liability_number column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn public_liability_number
    {
        get
        {
            return TradesmenTable.Instance.public_liability_numberColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.public_liability_expiry_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn public_liability_expiry_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[14];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.public_liability_expiry_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn public_liability_expiry_date
    {
        get
        {
            return TradesmenTable.Instance.public_liability_expiry_dateColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.workers_comp_company column object.
    /// </summary>
    public BaseClasses.Data.StringColumn workers_comp_companyColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[15];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.workers_comp_company column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn workers_comp_company
    {
        get
        {
            return TradesmenTable.Instance.workers_comp_companyColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.workers_comp_number column object.
    /// </summary>
    public BaseClasses.Data.StringColumn workers_comp_numberColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[16];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.workers_comp_number column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn workers_comp_number
    {
        get
        {
            return TradesmenTable.Instance.workers_comp_numberColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.workers_comp_expiry_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn workers_comp_expiry_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[17];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.workers_comp_expiry_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn workers_comp_expiry_date
    {
        get
        {
            return TradesmenTable.Instance.workers_comp_expiry_dateColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.trade_licence_number column object.
    /// </summary>
    public BaseClasses.Data.StringColumn trade_licence_numberColumn
    {
        get
        {
            return (BaseClasses.Data.StringColumn)this.TableDefinition.ColumnList[18];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.trade_licence_number column object.
    /// </summary>
    public static BaseClasses.Data.StringColumn trade_licence_number
    {
        get
        {
            return TradesmenTable.Instance.trade_licence_numberColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.trade_licence_expiry_date column object.
    /// </summary>
    public BaseClasses.Data.DateColumn trade_licence_expiry_dateColumn
    {
        get
        {
            return (BaseClasses.Data.DateColumn)this.TableDefinition.ColumnList[19];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.trade_licence_expiry_date column object.
    /// </summary>
    public static BaseClasses.Data.DateColumn trade_licence_expiry_date
    {
        get
        {
            return TradesmenTable.Instance.trade_licence_expiry_dateColumn;
        }
    }
    
    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.status_id column object.
    /// </summary>
    public BaseClasses.Data.BooleanColumn status_idColumn
    {
        get
        {
            return (BaseClasses.Data.BooleanColumn)this.TableDefinition.ColumnList[20];
        }
    }
    

    
    /// <summary>
    /// This is a convenience property that provides direct access to the table's Tradesmen_.status_id column object.
    /// </summary>
    public static BaseClasses.Data.BooleanColumn status_id
    {
        get
        {
            return TradesmenTable.Instance.status_idColumn;
        }
    }
    
    


#endregion

    
#region "Shared helper methods"

    /// <summary>
    /// This is a shared function that can be used to get an array of TradesmenRecord records using a where clause.
    /// </summary>
    public static TradesmenRecord[] GetRecords(string where)
    {
        return GetRecords(where, null, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }

    /// <summary>
    /// This is a shared function that can be used to get an array of TradesmenRecord records using a where and order by clause.
    /// </summary>
    public static TradesmenRecord[] GetRecords(string where, OrderBy orderBy)
    {
        return GetRecords(where, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MAX_BATCH_SIZE);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get an array of TradesmenRecord records using a where and order by clause clause with pagination.
    /// </summary>
    public static TradesmenRecord[] GetRecords(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        ArrayList recList = TradesmenTable.Instance.GetRecordList(whereFilter, orderBy, pageIndex, pageSize);

        return (TradesmenRecord[])recList.ToArray(Type.GetType("WKCRM.Business.TradesmenRecord"));
    }   
    
    public static TradesmenRecord[] GetRecords(
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{

        ArrayList recList = TradesmenTable.Instance.GetRecordList(where.GetFilter(), orderBy, pageIndex, pageSize);

        return (TradesmenRecord[])recList.ToArray(Type.GetType("WKCRM.Business.TradesmenRecord"));
    }

    /// <summary>
    /// This is a shared function that can be used to get total number of records that will be returned using the where clause.
    /// </summary>
    public static int GetRecordCount(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }

        return (int)TradesmenTable.Instance.GetRecordListCount(whereFilter, null);
    }
    
    public static int GetRecordCount(WhereClause where)
    {
        return (int)TradesmenTable.Instance.GetRecordListCount(where.GetFilter(), null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a TradesmenRecord record using a where clause.
    /// </summary>
    public static TradesmenRecord GetRecord(string where)
    {
        OrderBy orderBy = null;
        return GetRecord(where, orderBy);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a TradesmenRecord record using a where and order by clause.
    /// </summary>
    public static TradesmenRecord GetRecord(string where, OrderBy orderBy)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        ArrayList recList = TradesmenTable.Instance.GetRecordList(whereFilter, orderBy, BaseTable.MIN_PAGE_NUMBER, BaseTable.MIN_BATCH_SIZE);

        TradesmenRecord rec = null;
        if (recList.Count > 0)
        {
            rec = (TradesmenRecord)recList[0];
        }

        return rec;
    }
    
    public static String[] GetValues(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int maxItems)
	{

        // Create the filter list.
        SqlBuilderColumnSelection retCol = new SqlBuilderColumnSelection(false, true);
        retCol.AddColumn(col);

        return TradesmenTable.Instance.GetColumnValues(retCol, where.GetFilter(), orderBy, BaseTable.MIN_PAGE_NUMBER, maxItems);

    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where)
    {
        TradesmenRecord[] recs = GetRecords(where);
        return  TradesmenTable.Instance.CreateDataTable(recs, null);
    }

    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy)
    {
        TradesmenRecord[] recs = GetRecords(where, orderBy);
        return  TradesmenTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to get a DataTable to bound with a data bound control using a where and order by clause with pagination.
    /// </summary>
    public static System.Data.DataTable GetDataTable(string where, OrderBy orderBy, int pageIndex, int pageSize)
    {
        TradesmenRecord[] recs = GetRecords(where, orderBy, pageIndex, pageSize);
        return  TradesmenTable.Instance.CreateDataTable(recs, null);
    }
    
    /// <summary>
    /// This is a shared function that can be used to delete records using a where clause.
    /// </summary>
    public static void DeleteRecords(string where)
    {
        if (where == null || where.Trim() == "")
        {
           return;
        }
        
        SqlFilter whereFilter = new SqlFilter(where);
        TradesmenTable.Instance.DeleteRecordList(whereFilter);
    }
    
    /// <summary>
    /// This is a shared function that can be used to export records using a where clause.
    /// </summary>
    public static string Export(string where)
    {
        SqlFilter whereFilter = null;
        if (where != null && where.Trim() != "")
        {
           whereFilter = new SqlFilter(where);
        }
        
        return  TradesmenTable.Instance.ExportRecordData(whereFilter);
    }
   
    public static string Export(WhereClause where)
    {
        BaseFilter whereFilter = null;
        if (where != null)
        {
            whereFilter = where.GetFilter();
        }

        return TradesmenTable.Instance.ExportRecordData(whereFilter);
    }
    
	public static string GetSum(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Sum);

        return TradesmenTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }
    
    public static string GetCount(
		BaseColumn col,
		WhereClause where,
		OrderBy orderBy,
		int pageIndex,
		int pageSize)
	{
        SqlBuilderColumnSelection colSel = new SqlBuilderColumnSelection(false, false);
        colSel.AddColumn(col, SqlBuilderColumnOperation.OperationType.Count);

        return TradesmenTable.Instance.GetColumnStatistics(colSel, where.GetFilter(), orderBy, pageIndex, pageSize);
    }

    /// <summary>
    ///  This method returns the columns in the table.
    /// </summary>
    public static BaseColumn[] GetColumns() 
    {
        return TradesmenTable.Instance.TableDefinition.Columns;
    }

    /// <summary>
    ///  This method returns the columnlist in the table.
    /// </summary>   
    public static ColumnList GetColumnList() 
    {
        return TradesmenTable.Instance.TableDefinition.ColumnList;
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    public static IRecord CreateNewRecord() 
    {
        return TradesmenTable.Instance.CreateRecord();
    }

    /// <summary>
    /// This method creates a new record and returns it to be edited.
    /// </summary>
    /// <param name="tempId">ID of the new record.</param>   
    public static IRecord CreateNewRecord(string tempId) 
    {
        return TradesmenTable.Instance.CreateRecord(tempId);
    }

    /// <summary>
    /// This method checks if column is editable.
    /// </summary>
    /// <param name="columnName">Name of the column to check.</param>
    public static bool isReadOnlyColumn(string columnName) 
    {
        BaseColumn column = GetColumn(columnName);
        if (!(column == null)) 
        {
            return column.IsValuesReadOnly;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// This method gets the specified column.
    /// </summary>
    /// <param name="uniqueColumnName">Unique name of the column to fetch.</param>
    public static BaseColumn GetColumn(string uniqueColumnName) 
    {
        BaseColumn column = TradesmenTable.Instance.TableDefinition.ColumnList.GetByUniqueName(uniqueColumnName);
        return column;
    }

        //Convenience method for getting a record using a string-based record identifier
        public static TradesmenRecord GetRecord(string id, bool bMutable)
        {
            return (TradesmenRecord)TradesmenTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for getting a record using a KeyValue record identifier
        public static TradesmenRecord GetRecord(KeyValue id, bool bMutable)
        {
            return (TradesmenRecord)TradesmenTable.Instance.GetRecordData(id, bMutable);
        }

        //Convenience method for creating a record
        public KeyValue NewRecord(
        string TradesmanTypeIdValue, 
        string nameValue, 
        string ABNValue, 
        string dobValue, 
        string phoneValue, 
        string mobileValue, 
        string faxValue, 
        string addressValue, 
        string emailValue, 
        string send_email_notificationsValue, 
        string entity_type_idValue, 
        string public_liability_companyValue, 
        string public_liability_numberValue, 
        string public_liability_expiry_dateValue, 
        string workers_comp_companyValue, 
        string workers_comp_numberValue, 
        string workers_comp_expiry_dateValue, 
        string trade_licence_numberValue, 
        string trade_licence_expiry_dateValue, 
        string status_idValue
    )
        {
            IPrimaryKeyRecord rec = (IPrimaryKeyRecord)this.CreateRecord();
                    rec.SetString(TradesmanTypeIdValue, TradesmanTypeIdColumn);
        rec.SetString(nameValue, nameColumn);
        rec.SetString(ABNValue, ABNColumn);
        rec.SetString(dobValue, dobColumn);
        rec.SetString(phoneValue, phoneColumn);
        rec.SetString(mobileValue, mobileColumn);
        rec.SetString(faxValue, faxColumn);
        rec.SetString(addressValue, addressColumn);
        rec.SetString(emailValue, emailColumn);
        rec.SetString(send_email_notificationsValue, send_email_notificationsColumn);
        rec.SetString(entity_type_idValue, entity_type_idColumn);
        rec.SetString(public_liability_companyValue, public_liability_companyColumn);
        rec.SetString(public_liability_numberValue, public_liability_numberColumn);
        rec.SetString(public_liability_expiry_dateValue, public_liability_expiry_dateColumn);
        rec.SetString(workers_comp_companyValue, workers_comp_companyColumn);
        rec.SetString(workers_comp_numberValue, workers_comp_numberColumn);
        rec.SetString(workers_comp_expiry_dateValue, workers_comp_expiry_dateColumn);
        rec.SetString(trade_licence_numberValue, trade_licence_numberColumn);
        rec.SetString(trade_licence_expiry_dateValue, trade_licence_expiry_dateColumn);
        rec.SetString(status_idValue, status_idColumn);


            rec.Create(); //update the DB so any DB-initialized fields (like autoincrement IDs) can be initialized

            return rec.GetID();
        }
        
        /// <summary>
		///  This method deletes a specified record
		/// </summary>
		/// <param name="kv">Keyvalue of the record to be deleted.</param>
		public static void DeleteRecord(KeyValue kv)
		{
			TradesmenTable.Instance.DeleteOneRecord(kv);
		}

		/// <summary>
		/// This method checks if record exist in the database using the keyvalue provided.
		/// </summary>
		/// <param name="kv">Key value of the record.</param>
		public static bool DoesRecordExist(KeyValue kv)
		{
			bool recordExist = true;
			try
			{
				TradesmenTable.GetRecord(kv, false);
			}
			catch (Exception ex)
			{
				recordExist = false;
			}
			return recordExist;
		}

        /// <summary>
        ///  This method returns all the primary columns in the table.
        /// </summary>
        public static ColumnList GetPrimaryKeyColumns() 
        {
            if (!(TradesmenTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                return TradesmenTable.Instance.TableDefinition.PrimaryKey.Columns;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// This method takes a key and returns a keyvalue.
        /// </summary>
        /// <param name="key">key could be array of primary key values in case of composite primary key or a string containing single primary key value in case of non-composite primary key.</param>
        public static KeyValue GetKeyValue(object key) 
        {
            KeyValue kv = null;
            if (!(TradesmenTable.Instance.TableDefinition.PrimaryKey == null)) 
            {
                bool isCompositePrimaryKey = false;
                isCompositePrimaryKey = TradesmenTable.Instance.TableDefinition.PrimaryKey.IsCompositeKey;
                if ((isCompositePrimaryKey && key.GetType().IsArray)) 
                {
                    //  If the key is composite, then construct a key value.
                    kv = new KeyValue();
                    Array keyArray = ((Array)(key));
                    if (!(keyArray == null)) 
                    {
                        int length = keyArray.Length;
                        ColumnList pkColumns = TradesmenTable.Instance.TableDefinition.PrimaryKey.Columns;
                        int index = 0;
                        foreach (BaseColumn pkColumn in pkColumns) 
                        {
                            string keyString = ((keyArray.GetValue(index)).ToString());
                            if (TradesmenTable.Instance.TableDefinition.TableType == BaseClasses.Data.TableDefinition.TableTypes.Virtual)
                            {
                                kv.AddElement(pkColumn.UniqueName, keyString);
                            }
                            else 
                            {
                                kv.AddElement(pkColumn.InternalName, keyString);
                            }

                            index = (index + 1);
                        }
                    }
                }
                else 
                {
                    //  If the key is not composite, then get the key value.
                    kv = TradesmenTable.Instance.TableDefinition.PrimaryKey.ParseValue(((key).ToString()));
                }
            }
            return kv;
        }

#endregion
}

}
