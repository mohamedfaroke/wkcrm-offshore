﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in QuestionTypeRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="QuestionTypeRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="QuestionTypeTable"></see> class.
/// </remarks>
/// <seealso cref="QuestionTypeTable"></seealso>
/// <seealso cref="QuestionTypeRecord"></seealso>
public class BaseQuestionTypeRecord : PrimaryKeyRecord
{

	public readonly static QuestionTypeTable TableUtils = QuestionTypeTable.Instance;

	// Constructors
 
	protected BaseQuestionTypeRecord() : base(TableUtils)
	{
	}

	protected BaseQuestionTypeRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionType_.typeId field.
	/// </summary>
	public ColumnValue GettypeIdValue()
	{
		return this.GetValue(TableUtils.typeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionType_.typeId field.
	/// </summary>
	public Int32 GettypeIdFieldValue()
	{
		return this.GetValue(TableUtils.typeIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionType_.Type field.
	/// </summary>
	public ColumnValue GetType0Value()
	{
		return this.GetValue(TableUtils.Type0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's QuestionType_.Type field.
	/// </summary>
	public string GetType0FieldValue()
	{
		return this.GetValue(TableUtils.Type0Column).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionType_.Type field.
	/// </summary>
	public void SetType0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.Type0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionType_.Type field.
	/// </summary>
	public void SetType0FieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.Type0Column);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's QuestionType_.typeId field.
	/// </summary>
	public Int32 typeId
	{
		get
		{
			return this.GetValue(TableUtils.typeIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.typeIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool typeIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.typeIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionType_.typeId field.
	/// </summary>
	public string typeIdDefault
	{
		get
		{
			return TableUtils.typeIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's QuestionType_.Type field.
	/// </summary>
	public string Type0
	{
		get
		{
			return this.GetValue(TableUtils.Type0Column).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.Type0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool Type0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.Type0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's QuestionType_.Type field.
	/// </summary>
	public string Type0Default
	{
		get
		{
			return TableUtils.Type0Column.DefaultValue;
		}
	}


#endregion
}

}
