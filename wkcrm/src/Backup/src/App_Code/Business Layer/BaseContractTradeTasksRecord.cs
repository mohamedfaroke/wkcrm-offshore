﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ContractTradeTasksRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ContractTradeTasksRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ContractTradeTasksTable"></see> class.
/// </remarks>
/// <seealso cref="ContractTradeTasksTable"></seealso>
/// <seealso cref="ContractTradeTasksRecord"></seealso>
public class BaseContractTradeTasksRecord : PrimaryKeyRecord
{

	public readonly static ContractTradeTasksTable TableUtils = ContractTradeTasksTable.Instance;

	// Constructors
 
	protected BaseContractTradeTasksRecord() : base(TableUtils)
	{
	}

	protected BaseContractTradeTasksRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public ColumnValue Getcontract_trade_typeValue()
	{
		return this.GetValue(TableUtils.contract_trade_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public Int32 Getcontract_trade_typeFieldValue()
	{
		return this.GetValue(TableUtils.contract_trade_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public void Setcontract_trade_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_trade_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public void Setcontract_trade_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_trade_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public void Setcontract_trade_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_trade_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public void Setcontract_trade_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_trade_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public void Setcontract_trade_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_trade_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public ColumnValue GetpriceValue()
	{
		return this.GetValue(TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public Decimal GetpriceFieldValue()
	{
		return this.GetValue(TableUtils.priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public void SetpriceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public void SetpriceFieldValue(string val)
	{
		this.SetString(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public void SetpriceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public void SetpriceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public void SetpriceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.part_of_standard_price field.
	/// </summary>
	public ColumnValue Getpart_of_standard_priceValue()
	{
		return this.GetValue(TableUtils.part_of_standard_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.part_of_standard_price field.
	/// </summary>
	public bool Getpart_of_standard_priceFieldValue()
	{
		return this.GetValue(TableUtils.part_of_standard_priceColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.part_of_standard_price field.
	/// </summary>
	public void Setpart_of_standard_priceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.part_of_standard_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.part_of_standard_price field.
	/// </summary>
	public void Setpart_of_standard_priceFieldValue(string val)
	{
		this.SetString(val, TableUtils.part_of_standard_priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.part_of_standard_price field.
	/// </summary>
	public void Setpart_of_standard_priceFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.part_of_standard_priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public ColumnValue Getcontract_payment_type_idValue()
	{
		return this.GetValue(TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public Int32 Getcontract_payment_type_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_payment_type_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public void Setcontract_payment_type_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.IsDefaultItem field.
	/// </summary>
	public ColumnValue GetIsDefaultItemValue()
	{
		return this.GetValue(TableUtils.IsDefaultItemColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ContractTradeTasks_.IsDefaultItem field.
	/// </summary>
	public bool GetIsDefaultItemFieldValue()
	{
		return this.GetValue(TableUtils.IsDefaultItemColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.IsDefaultItem field.
	/// </summary>
	public void SetIsDefaultItemFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.IsDefaultItemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.IsDefaultItem field.
	/// </summary>
	public void SetIsDefaultItemFieldValue(string val)
	{
		this.SetString(val, TableUtils.IsDefaultItemColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.IsDefaultItem field.
	/// </summary>
	public void SetIsDefaultItemFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.IsDefaultItemColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public Int32 contract_trade_type
	{
		get
		{
			return this.GetValue(TableUtils.contract_trade_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_trade_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_trade_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_trade_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_trade_type field.
	/// </summary>
	public string contract_trade_typeDefault
	{
		get
		{
			return TableUtils.contract_trade_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public Decimal price
	{
		get
		{
			return this.GetValue(TableUtils.priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.price field.
	/// </summary>
	public string priceDefault
	{
		get
		{
			return TableUtils.priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.part_of_standard_price field.
	/// </summary>
	public bool part_of_standard_price
	{
		get
		{
			return this.GetValue(TableUtils.part_of_standard_priceColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.part_of_standard_priceColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool part_of_standard_priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.part_of_standard_priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.part_of_standard_price field.
	/// </summary>
	public string part_of_standard_priceDefault
	{
		get
		{
			return TableUtils.part_of_standard_priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public Int32 contract_payment_type_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_payment_type_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_payment_type_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_payment_type_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_payment_type_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.contract_payment_type_id field.
	/// </summary>
	public string contract_payment_type_idDefault
	{
		get
		{
			return TableUtils.contract_payment_type_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ContractTradeTasks_.IsDefaultItem field.
	/// </summary>
	public bool IsDefaultItem
	{
		get
		{
			return this.GetValue(TableUtils.IsDefaultItemColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.IsDefaultItemColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool IsDefaultItemSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.IsDefaultItemColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ContractTradeTasks_.IsDefaultItem field.
	/// </summary>
	public string IsDefaultItemDefault
	{
		get
		{
			return TableUtils.IsDefaultItemColumn.DefaultValue;
		}
	}


#endregion
}

}
