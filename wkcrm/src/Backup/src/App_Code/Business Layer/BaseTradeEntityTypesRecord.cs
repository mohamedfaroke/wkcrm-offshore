﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradeEntityTypesRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="TradeEntityTypesRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TradeEntityTypesTable"></see> class.
/// </remarks>
/// <seealso cref="TradeEntityTypesTable"></seealso>
/// <seealso cref="TradeEntityTypesRecord"></seealso>
public class BaseTradeEntityTypesRecord : PrimaryKeyRecord
{

	public readonly static TradeEntityTypesTable TableUtils = TradeEntityTypesTable.Instance;

	// Constructors
 
	protected BaseTradeEntityTypesRecord() : base(TableUtils)
	{
	}

	protected BaseTradeEntityTypesRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.enforce_legal_fields field.
	/// </summary>
	public ColumnValue Getenforce_legal_fieldsValue()
	{
		return this.GetValue(TableUtils.enforce_legal_fieldsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.enforce_legal_fields field.
	/// </summary>
	public bool Getenforce_legal_fieldsFieldValue()
	{
		return this.GetValue(TableUtils.enforce_legal_fieldsColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.enforce_legal_fields field.
	/// </summary>
	public void Setenforce_legal_fieldsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.enforce_legal_fieldsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.enforce_legal_fields field.
	/// </summary>
	public void Setenforce_legal_fieldsFieldValue(string val)
	{
		this.SetString(val, TableUtils.enforce_legal_fieldsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.enforce_legal_fields field.
	/// </summary>
	public void Setenforce_legal_fieldsFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.enforce_legal_fieldsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradeEntityTypes_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's TradeEntityTypes_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TradeEntityTypes_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TradeEntityTypes_.enforce_legal_fields field.
	/// </summary>
	public bool enforce_legal_fields
	{
		get
		{
			return this.GetValue(TableUtils.enforce_legal_fieldsColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.enforce_legal_fieldsColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool enforce_legal_fieldsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.enforce_legal_fieldsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.enforce_legal_fields field.
	/// </summary>
	public string enforce_legal_fieldsDefault
	{
		get
		{
			return TableUtils.enforce_legal_fieldsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TradeEntityTypes_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradeEntityTypes_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
