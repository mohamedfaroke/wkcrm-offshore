﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in ProjectEvaluationsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="ProjectEvaluationsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="ProjectEvaluationsTable"></see> class.
/// </remarks>
/// <seealso cref="ProjectEvaluationsTable"></seealso>
/// <seealso cref="ProjectEvaluationsRecord"></seealso>
public class BaseProjectEvaluationsRecord : PrimaryKeyRecord
{

	public readonly static ProjectEvaluationsTable TableUtils = ProjectEvaluationsTable.Instance;

	// Constructors
 
	protected BaseProjectEvaluationsRecord() : base(TableUtils)
	{
	}

	protected BaseProjectEvaluationsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.ProjectEvaluationId field.
	/// </summary>
	public ColumnValue GetProjectEvaluationIdValue()
	{
		return this.GetValue(TableUtils.ProjectEvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.ProjectEvaluationId field.
	/// </summary>
	public Int32 GetProjectEvaluationIdFieldValue()
	{
		return this.GetValue(TableUtils.ProjectEvaluationIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public ColumnValue Getproject_idValue()
	{
		return this.GetValue(TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public Decimal Getproject_idFieldValue()
	{
		return this.GetValue(TableUtils.project_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public void Setproject_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.project_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public ColumnValue GetEvaluationIdValue()
	{
		return this.GetValue(TableUtils.EvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public Int32 GetEvaluationIdFieldValue()
	{
		return this.GetValue(TableUtils.EvaluationIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public void SetEvaluationIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.EvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public void SetEvaluationIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.EvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public void SetEvaluationIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public void SetEvaluationIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public void SetEvaluationIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.EvaluationDate field.
	/// </summary>
	public ColumnValue GetEvaluationDateValue()
	{
		return this.GetValue(TableUtils.EvaluationDateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.EvaluationDate field.
	/// </summary>
	public DateTime GetEvaluationDateFieldValue()
	{
		return this.GetValue(TableUtils.EvaluationDateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationDate field.
	/// </summary>
	public void SetEvaluationDateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.EvaluationDateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationDate field.
	/// </summary>
	public void SetEvaluationDateFieldValue(string val)
	{
		this.SetString(val, TableUtils.EvaluationDateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationDate field.
	/// </summary>
	public void SetEvaluationDateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationDateColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.CreationDate field.
	/// </summary>
	public ColumnValue GetCreationDateValue()
	{
		return this.GetValue(TableUtils.CreationDateColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's ProjectEvaluations_.CreationDate field.
	/// </summary>
	public DateTime GetCreationDateFieldValue()
	{
		return this.GetValue(TableUtils.CreationDateColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.CreationDate field.
	/// </summary>
	public void SetCreationDateFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.CreationDateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.CreationDate field.
	/// </summary>
	public void SetCreationDateFieldValue(string val)
	{
		this.SetString(val, TableUtils.CreationDateColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.CreationDate field.
	/// </summary>
	public void SetCreationDateFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.CreationDateColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's ProjectEvaluations_.ProjectEvaluationId field.
	/// </summary>
	public Int32 ProjectEvaluationId
	{
		get
		{
			return this.GetValue(TableUtils.ProjectEvaluationIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.ProjectEvaluationIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool ProjectEvaluationIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.ProjectEvaluationIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.ProjectEvaluationId field.
	/// </summary>
	public string ProjectEvaluationIdDefault
	{
		get
		{
			return TableUtils.ProjectEvaluationIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public Decimal project_id
	{
		get
		{
			return this.GetValue(TableUtils.project_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.project_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool project_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.project_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.project_id field.
	/// </summary>
	public string project_idDefault
	{
		get
		{
			return TableUtils.project_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public Int32 EvaluationId
	{
		get
		{
			return this.GetValue(TableUtils.EvaluationIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.EvaluationIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool EvaluationIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.EvaluationIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationId field.
	/// </summary>
	public string EvaluationIdDefault
	{
		get
		{
			return TableUtils.EvaluationIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ProjectEvaluations_.EvaluationDate field.
	/// </summary>
	public DateTime EvaluationDate
	{
		get
		{
			return this.GetValue(TableUtils.EvaluationDateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.EvaluationDateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool EvaluationDateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.EvaluationDateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.EvaluationDate field.
	/// </summary>
	public string EvaluationDateDefault
	{
		get
		{
			return TableUtils.EvaluationDateColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's ProjectEvaluations_.CreationDate field.
	/// </summary>
	public DateTime CreationDate
	{
		get
		{
			return this.GetValue(TableUtils.CreationDateColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.CreationDateColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool CreationDateSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.CreationDateColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's ProjectEvaluations_.CreationDate field.
	/// </summary>
	public string CreationDateDefault
	{
		get
		{
			return TableUtils.CreationDateColumn.DefaultValue;
		}
	}


#endregion
}

}
