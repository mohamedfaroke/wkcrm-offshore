﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EvaluationQuestionOptionsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="EvaluationQuestionOptionsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EvaluationQuestionOptionsTable"></see> class.
/// </remarks>
/// <seealso cref="EvaluationQuestionOptionsTable"></seealso>
/// <seealso cref="EvaluationQuestionOptionsRecord"></seealso>
public class BaseEvaluationQuestionOptionsRecord : PrimaryKeyRecord
{

	public readonly static EvaluationQuestionOptionsTable TableUtils = EvaluationQuestionOptionsTable.Instance;

	// Constructors
 
	protected BaseEvaluationQuestionOptionsRecord() : base(TableUtils)
	{
	}

	protected BaseEvaluationQuestionOptionsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public ColumnValue GetQuestionIdValue()
	{
		return this.GetValue(TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public Int32 GetQuestionIdFieldValue()
	{
		return this.GetValue(TableUtils.QuestionIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.QuestionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public void SetQuestionIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.QuestionIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public ColumnValue GetOptionIdValue()
	{
		return this.GetValue(TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public Int32 GetOptionIdFieldValue()
	{
		return this.GetValue(TableUtils.OptionIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public void SetOptionIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.OptionIdColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public Int32 QuestionId
	{
		get
		{
			return this.GetValue(TableUtils.QuestionIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.QuestionIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool QuestionIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.QuestionIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.QuestionId field.
	/// </summary>
	public string QuestionIdDefault
	{
		get
		{
			return TableUtils.QuestionIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public Int32 OptionId
	{
		get
		{
			return this.GetValue(TableUtils.OptionIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.OptionIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool OptionIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.OptionIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's EvaluationQuestionOptions_.OptionId field.
	/// </summary>
	public string OptionIdDefault
	{
		get
		{
			return TableUtils.OptionIdColumn.DefaultValue;
		}
	}


#endregion
}

}
