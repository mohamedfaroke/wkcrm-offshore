﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TradesmanTypeRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="TradesmanTypeRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TradesmanTypeTable"></see> class.
/// </remarks>
/// <seealso cref="TradesmanTypeTable"></seealso>
/// <seealso cref="TradesmanTypeRecord"></seealso>
public class BaseTradesmanTypeRecord : PrimaryKeyRecord
{

	public readonly static TradesmanTypeTable TableUtils = TradesmanTypeTable.Instance;

	// Constructors
 
	protected BaseTradesmanTypeRecord() : base(TableUtils)
	{
	}

	protected BaseTradesmanTypeRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public ColumnValue GetTradesmanTypeIdValue()
	{
		return this.GetValue(TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public Int32 GetTradesmanTypeIdFieldValue()
	{
		return this.GetValue(TableUtils.TradesmanTypeIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public void SetTradesmanTypeIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradesmanType_.TradesmanType field.
	/// </summary>
	public ColumnValue GetTradesmanTypeValue()
	{
		return this.GetValue(TableUtils.TradesmanTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradesmanType_.TradesmanType field.
	/// </summary>
	public string GetTradesmanTypeFieldValue()
	{
		return this.GetValue(TableUtils.TradesmanTypeColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanType field.
	/// </summary>
	public void SetTradesmanTypeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TradesmanTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanType field.
	/// </summary>
	public void SetTradesmanTypeFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradesmanTypeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradesmanType_.Active field.
	/// </summary>
	public ColumnValue GetActiveValue()
	{
		return this.GetValue(TableUtils.ActiveColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TradesmanType_.Active field.
	/// </summary>
	public bool GetActiveFieldValue()
	{
		return this.GetValue(TableUtils.ActiveColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.Active field.
	/// </summary>
	public void SetActiveFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.ActiveColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.Active field.
	/// </summary>
	public void SetActiveFieldValue(string val)
	{
		this.SetString(val, TableUtils.ActiveColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.Active field.
	/// </summary>
	public void SetActiveFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.ActiveColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public Int32 TradesmanTypeId
	{
		get
		{
			return this.GetValue(TableUtils.TradesmanTypeIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TradesmanTypeIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TradesmanTypeIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TradesmanTypeIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanTypeId field.
	/// </summary>
	public string TradesmanTypeIdDefault
	{
		get
		{
			return TableUtils.TradesmanTypeIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TradesmanType_.TradesmanType field.
	/// </summary>
	public string TradesmanType
	{
		get
		{
			return this.GetValue(TableUtils.TradesmanTypeColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TradesmanTypeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TradesmanTypeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TradesmanTypeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.TradesmanType field.
	/// </summary>
	public string TradesmanTypeDefault
	{
		get
		{
			return TableUtils.TradesmanTypeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TradesmanType_.Active field.
	/// </summary>
	public bool Active
	{
		get
		{
			return this.GetValue(TableUtils.ActiveColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.ActiveColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool ActiveSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.ActiveColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TradesmanType_.Active field.
	/// </summary>
	public string ActiveDefault
	{
		get
		{
			return TableUtils.ActiveColumn.DefaultValue;
		}
	}


#endregion
}

}
