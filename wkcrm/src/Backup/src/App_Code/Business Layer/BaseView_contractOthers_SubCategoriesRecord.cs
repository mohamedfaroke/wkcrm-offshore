﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_contractOthers_SubCategoriesRecord.vb

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_contractOthers_SubCategoriesRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_contractOthers_SubCategoriesView"></see> class.
/// </remarks>
/// <seealso cref="View_contractOthers_SubCategoriesView"></seealso>
/// <seealso cref="View_contractOthers_SubCategoriesRecord"></seealso>
public class BaseView_contractOthers_SubCategoriesRecord : KeylessRecord
{

	public readonly static View_contractOthers_SubCategoriesView TableUtils = View_contractOthers_SubCategoriesView.Instance;

	// Constructors
 
	protected BaseView_contractOthers_SubCategoriesRecord() : base(TableUtils)
	{
	}

	protected BaseView_contractOthers_SubCategoriesRecord(KeylessRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public ColumnValue Getcontract_item_typeValue()
	{
		return this.GetValue(TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public Int32 Getcontract_item_typeFieldValue()
	{
		return this.GetValue(TableUtils.contract_item_typeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public void Setcontract_item_typeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_item_typeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_contractOthers_SubCategories_.category field.
	/// </summary>
	public ColumnValue GetcategoryValue()
	{
		return this.GetValue(TableUtils.categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_contractOthers_SubCategories_.category field.
	/// </summary>
	public string GetcategoryFieldValue()
	{
		return this.GetValue(TableUtils.categoryColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.category field.
	/// </summary>
	public void SetcategoryFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.category field.
	/// </summary>
	public void SetcategoryFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.categoryColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_contractOthers_SubCategories_.sub_category field.
	/// </summary>
	public ColumnValue Getsub_categoryValue()
	{
		return this.GetValue(TableUtils.sub_categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_contractOthers_SubCategories_.sub_category field.
	/// </summary>
	public string Getsub_categoryFieldValue()
	{
		return this.GetValue(TableUtils.sub_categoryColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.sub_category field.
	/// </summary>
	public void Setsub_categoryFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.sub_categoryColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.sub_category field.
	/// </summary>
	public void Setsub_categoryFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.sub_categoryColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public Int32 contract_item_type
	{
		get
		{
			return this.GetValue(TableUtils.contract_item_typeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_item_typeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_item_typeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_item_typeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.contract_item_type field.
	/// </summary>
	public string contract_item_typeDefault
	{
		get
		{
			return TableUtils.contract_item_typeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_contractOthers_SubCategories_.category field.
	/// </summary>
	public string category
	{
		get
		{
			return this.GetValue(TableUtils.categoryColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.categoryColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool categorySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.categoryColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.category field.
	/// </summary>
	public string categoryDefault
	{
		get
		{
			return TableUtils.categoryColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_contractOthers_SubCategories_.sub_category field.
	/// </summary>
	public string sub_category
	{
		get
		{
			return this.GetValue(TableUtils.sub_categoryColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.sub_categoryColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool sub_categorySpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.sub_categoryColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_contractOthers_SubCategories_.sub_category field.
	/// </summary>
	public string sub_categoryDefault
	{
		get
		{
			return TableUtils.sub_categoryColumn.DefaultValue;
		}
	}


#endregion

}

}
