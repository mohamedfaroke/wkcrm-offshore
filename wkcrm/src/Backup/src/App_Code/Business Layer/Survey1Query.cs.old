﻿// This is a "safe" class, meaning that it is created once 
// and never overwritten. Any custom code you add to this class 
// will be preserved when you regenerate your application.
//
// Typical customizations that may be done in this class include
//  - adding custom event handlers
//  - overriding base class methods

using System;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// Provides access to the schema information and record data of the Survey1 custom query.
/// See <see cref="BaseSurvey1Query"></see> for additional information.
/// </summary>
/// <remarks>
/// See <see cref="BaseSurvey1Query"></see> for additional information.
/// <para>
/// This class is implemented using the Singleton design pattern.
/// </para>
/// </remarks>
/// <seealso cref="BaseSurvey1Query"></seealso>
/// <seealso cref="BaseSurvey1SqlQuery"></seealso>
/// <seealso cref="Survey1SqlQuery"></seealso>
/// <seealso cref="Survey1Definition"></seealso>
/// <seealso cref="Survey1Record"></seealso>
/// <seealso cref="BaseSurvey1Record"></seealso>
[SerializableAttribute()]
public class Survey1Query : BaseSurvey1Query, System.Runtime.Serialization.ISerializable, ISingleton
{

#region "ISerializable Members"

	/// <summary>
	/// Overridden to use the <see cref="Survey1Query_SerializationHelper"></see> class 
	/// for deserialization of <see cref="Survey1Query"></see> data.
	/// </summary>
	/// <remarks>
	/// Since the <see cref="Survey1Query"></see> class is implemented using the Singleton design pattern, 
	/// this method must be overridden to prevent additional instances from being created during deserialization.
	/// </remarks>
	void System.Runtime.Serialization.ISerializable.GetObjectData(
		System.Runtime.Serialization.SerializationInfo info, 
		System.Runtime.Serialization.StreamingContext context)
	{
		info.SetType(typeof(Survey1Query_SerializationHelper)); //No other values need to be added
	}

#region "Class Survey1Query_SerializationHelper"

	[SerializableAttribute()]
	private class Survey1Query_SerializationHelper: System.Runtime.Serialization.IObjectReference
	{
		//Method called after this object is deserialized
		public virtual object GetRealObject(System.Runtime.Serialization.StreamingContext context)
		{
			return Survey1Query.Instance;
		}
	}

#endregion

#endregion

	/// <summary>
	/// References the only instance of the <see cref="Survey1Query"></see> class.
	/// </summary>
	/// <remarks>
	/// Since the <see cref="Survey1Query"></see> class is implemented using the Singleton design pattern, 
	/// this field is the only way to access an instance of the class.
	/// </remarks>
	public readonly static Survey1Query Instance = new Survey1Query();

	private Survey1Query()
	{
	}


}

}
