﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in TaskNotificationRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="TaskNotificationRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="TaskNotificationTable"></see> class.
/// </remarks>
/// <seealso cref="TaskNotificationTable"></seealso>
/// <seealso cref="TaskNotificationRecord"></seealso>
public class BaseTaskNotificationRecord : PrimaryKeyRecord
{

	public readonly static TaskNotificationTable TableUtils = TaskNotificationTable.Instance;

	// Constructors
 
	protected BaseTaskNotificationRecord() : base(TableUtils)
	{
	}

	protected BaseTaskNotificationRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public ColumnValue Getreference_idValue()
	{
		return this.GetValue(TableUtils.reference_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public Decimal Getreference_idFieldValue()
	{
		return this.GetValue(TableUtils.reference_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public void Setreference_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.reference_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public void Setreference_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.reference_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public void Setreference_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.reference_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public void Setreference_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.reference_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public void Setreference_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.reference_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public ColumnValue GettaskNotificationTypeValue()
	{
		return this.GetValue(TableUtils.taskNotificationTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public Int32 GettaskNotificationTypeFieldValue()
	{
		return this.GetValue(TableUtils.taskNotificationTypeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public void SettaskNotificationTypeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.taskNotificationTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public void SettaskNotificationTypeFieldValue(string val)
	{
		this.SetString(val, TableUtils.taskNotificationTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public void SettaskNotificationTypeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.taskNotificationTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public void SettaskNotificationTypeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.taskNotificationTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public void SettaskNotificationTypeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.taskNotificationTypeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.date_created field.
	/// </summary>
	public ColumnValue Getdate_createdValue()
	{
		return this.GetValue(TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.date_created field.
	/// </summary>
	public DateTime Getdate_createdFieldValue()
	{
		return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_createdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_created field.
	/// </summary>
	public void Setdate_createdFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_createdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.TaskDescription field.
	/// </summary>
	public ColumnValue GetTaskDescriptionValue()
	{
		return this.GetValue(TableUtils.TaskDescriptionColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.TaskDescription field.
	/// </summary>
	public string GetTaskDescriptionFieldValue()
	{
		return this.GetValue(TableUtils.TaskDescriptionColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.TaskDescription field.
	/// </summary>
	public void SetTaskDescriptionFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TaskDescriptionColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.TaskDescription field.
	/// </summary>
	public void SetTaskDescriptionFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TaskDescriptionColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public ColumnValue Getemployee_idValue()
	{
		return this.GetValue(TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public Decimal Getemployee_idFieldValue()
	{
		return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.TaskLink field.
	/// </summary>
	public ColumnValue GetTaskLinkValue()
	{
		return this.GetValue(TableUtils.TaskLinkColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.TaskLink field.
	/// </summary>
	public string GetTaskLinkFieldValue()
	{
		return this.GetValue(TableUtils.TaskLinkColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.TaskLink field.
	/// </summary>
	public void SetTaskLinkFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TaskLinkColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.TaskLink field.
	/// </summary>
	public void SetTaskLinkFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TaskLinkColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.date_viewed field.
	/// </summary>
	public ColumnValue Getdate_viewedValue()
	{
		return this.GetValue(TableUtils.date_viewedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.date_viewed field.
	/// </summary>
	public DateTime Getdate_viewedFieldValue()
	{
		return this.GetValue(TableUtils.date_viewedColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_viewed field.
	/// </summary>
	public void Setdate_viewedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_viewedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_viewed field.
	/// </summary>
	public void Setdate_viewedFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_viewedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_viewed field.
	/// </summary>
	public void Setdate_viewedFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_viewedColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.date_completed field.
	/// </summary>
	public ColumnValue Getdate_completedValue()
	{
		return this.GetValue(TableUtils.date_completedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.date_completed field.
	/// </summary>
	public DateTime Getdate_completedFieldValue()
	{
		return this.GetValue(TableUtils.date_completedColumn).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_completed field.
	/// </summary>
	public void Setdate_completedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.date_completedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_completed field.
	/// </summary>
	public void Setdate_completedFieldValue(string val)
	{
		this.SetString(val, TableUtils.date_completedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_completed field.
	/// </summary>
	public void Setdate_completedFieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.date_completedColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.completed field.
	/// </summary>
	public ColumnValue GetcompletedValue()
	{
		return this.GetValue(TableUtils.completedColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's TaskNotification_.completed field.
	/// </summary>
	public bool GetcompletedFieldValue()
	{
		return this.GetValue(TableUtils.completedColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.completed field.
	/// </summary>
	public void SetcompletedFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.completedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.completed field.
	/// </summary>
	public void SetcompletedFieldValue(string val)
	{
		this.SetString(val, TableUtils.completedColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.completed field.
	/// </summary>
	public void SetcompletedFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.completedColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public Decimal reference_id
	{
		get
		{
			return this.GetValue(TableUtils.reference_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.reference_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool reference_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.reference_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.reference_id field.
	/// </summary>
	public string reference_idDefault
	{
		get
		{
			return TableUtils.reference_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public Int32 taskNotificationType
	{
		get
		{
			return this.GetValue(TableUtils.taskNotificationTypeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.taskNotificationTypeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool taskNotificationTypeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.taskNotificationTypeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.taskNotificationType field.
	/// </summary>
	public string taskNotificationTypeDefault
	{
		get
		{
			return TableUtils.taskNotificationTypeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.date_created field.
	/// </summary>
	public DateTime date_created
	{
		get
		{
			return this.GetValue(TableUtils.date_createdColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_createdColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_createdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_createdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_created field.
	/// </summary>
	public string date_createdDefault
	{
		get
		{
			return TableUtils.date_createdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.TaskDescription field.
	/// </summary>
	public string TaskDescription
	{
		get
		{
			return this.GetValue(TableUtils.TaskDescriptionColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TaskDescriptionColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TaskDescriptionSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TaskDescriptionColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.TaskDescription field.
	/// </summary>
	public string TaskDescriptionDefault
	{
		get
		{
			return TableUtils.TaskDescriptionColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public Decimal employee_id
	{
		get
		{
			return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.employee_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool employee_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.employee_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.employee_id field.
	/// </summary>
	public string employee_idDefault
	{
		get
		{
			return TableUtils.employee_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.TaskLink field.
	/// </summary>
	public string TaskLink
	{
		get
		{
			return this.GetValue(TableUtils.TaskLinkColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TaskLinkColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TaskLinkSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TaskLinkColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.TaskLink field.
	/// </summary>
	public string TaskLinkDefault
	{
		get
		{
			return TableUtils.TaskLinkColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.date_viewed field.
	/// </summary>
	public DateTime date_viewed
	{
		get
		{
			return this.GetValue(TableUtils.date_viewedColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_viewedColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_viewedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_viewedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_viewed field.
	/// </summary>
	public string date_viewedDefault
	{
		get
		{
			return TableUtils.date_viewedColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.date_completed field.
	/// </summary>
	public DateTime date_completed
	{
		get
		{
			return this.GetValue(TableUtils.date_completedColumn).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.date_completedColumn);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool date_completedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.date_completedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.date_completed field.
	/// </summary>
	public string date_completedDefault
	{
		get
		{
			return TableUtils.date_completedColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's TaskNotification_.completed field.
	/// </summary>
	public bool completed
	{
		get
		{
			return this.GetValue(TableUtils.completedColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.completedColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool completedSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.completedColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's TaskNotification_.completed field.
	/// </summary>
	public string completedDefault
	{
		get
		{
			return TableUtils.completedColumn.DefaultValue;
		}
	}


#endregion
}

}
