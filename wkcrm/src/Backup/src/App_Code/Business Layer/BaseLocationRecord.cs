﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in LocationRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="LocationRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="LocationTable"></see> class.
/// </remarks>
/// <seealso cref="LocationTable"></seealso>
/// <seealso cref="LocationRecord"></seealso>
public class BaseLocationRecord : PrimaryKeyRecord
{

	public readonly static LocationTable TableUtils = LocationTable.Instance;

	// Constructors
 
	protected BaseLocationRecord() : base(TableUtils)
	{
	}

	protected BaseLocationRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.id field.
	/// </summary>
	public Int32 Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.name field.
	/// </summary>
	public ColumnValue GetnameValue()
	{
		return this.GetValue(TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.name field.
	/// </summary>
	public string GetnameFieldValue()
	{
		return this.GetValue(TableUtils.nameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.name field.
	/// </summary>
	public void SetnameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.nameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.name field.
	/// </summary>
	public void SetnameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.nameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.address field.
	/// </summary>
	public ColumnValue GetaddressValue()
	{
		return this.GetValue(TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.address field.
	/// </summary>
	public string GetaddressFieldValue()
	{
		return this.GetValue(TableUtils.addressColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.address field.
	/// </summary>
	public void SetaddressFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.addressColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.address field.
	/// </summary>
	public void SetaddressFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.addressColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.phone field.
	/// </summary>
	public ColumnValue GetphoneValue()
	{
		return this.GetValue(TableUtils.phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.phone field.
	/// </summary>
	public string GetphoneFieldValue()
	{
		return this.GetValue(TableUtils.phoneColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.phone field.
	/// </summary>
	public void SetphoneFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.phoneColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.phone field.
	/// </summary>
	public void SetphoneFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.phoneColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.status_id field.
	/// </summary>
	public ColumnValue Getstatus_idValue()
	{
		return this.GetValue(TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Location_.status_id field.
	/// </summary>
	public bool Getstatus_idFieldValue()
	{
		return this.GetValue(TableUtils.status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.status_id field.
	/// </summary>
	public void Setstatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Location_.id field.
	/// </summary>
	public Int32 id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Location_.name field.
	/// </summary>
	public string name
	{
		get
		{
			return this.GetValue(TableUtils.nameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.nameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool nameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.nameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.name field.
	/// </summary>
	public string nameDefault
	{
		get
		{
			return TableUtils.nameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Location_.address field.
	/// </summary>
	public string address
	{
		get
		{
			return this.GetValue(TableUtils.addressColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.addressColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool addressSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.addressColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.address field.
	/// </summary>
	public string addressDefault
	{
		get
		{
			return TableUtils.addressColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Location_.phone field.
	/// </summary>
	public string phone
	{
		get
		{
			return this.GetValue(TableUtils.phoneColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.phoneColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool phoneSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.phoneColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.phone field.
	/// </summary>
	public string phoneDefault
	{
		get
		{
			return TableUtils.phoneColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Location_.status_id field.
	/// </summary>
	public bool status_id
	{
		get
		{
			return this.GetValue(TableUtils.status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Location_.status_id field.
	/// </summary>
	public string status_idDefault
	{
		get
		{
			return TableUtils.status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
