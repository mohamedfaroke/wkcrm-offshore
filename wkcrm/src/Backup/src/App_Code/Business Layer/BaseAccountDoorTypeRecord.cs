﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in AccountDoorTypeRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="AccountDoorTypeRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="AccountDoorTypeTable"></see> class.
/// </remarks>
/// <seealso cref="AccountDoorTypeTable"></seealso>
/// <seealso cref="AccountDoorTypeRecord"></seealso>
public class BaseAccountDoorTypeRecord : PrimaryKeyRecord
{

	public readonly static AccountDoorTypeTable TableUtils = AccountDoorTypeTable.Instance;

	// Constructors
 
	protected BaseAccountDoorTypeRecord() : base(TableUtils)
	{
	}

	protected BaseAccountDoorTypeRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public ColumnValue GetAccountDoorTypeIdValue()
	{
		return this.GetValue(TableUtils.AccountDoorTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public Int32 GetAccountDoorTypeIdFieldValue()
	{
		return this.GetValue(TableUtils.AccountDoorTypeIdColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public void SetAccountDoorTypeIdFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.AccountDoorTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public void SetAccountDoorTypeIdFieldValue(string val)
	{
		this.SetString(val, TableUtils.AccountDoorTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public void SetAccountDoorTypeIdFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountDoorTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public void SetAccountDoorTypeIdFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountDoorTypeIdColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public void SetAccountDoorTypeIdFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountDoorTypeIdColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountDoorType_.AccountDoorType field.
	/// </summary>
	public ColumnValue GetAccountDoorTypeValue()
	{
		return this.GetValue(TableUtils.AccountDoorTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountDoorType_.AccountDoorType field.
	/// </summary>
	public string GetAccountDoorTypeFieldValue()
	{
		return this.GetValue(TableUtils.AccountDoorTypeColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorType field.
	/// </summary>
	public void SetAccountDoorTypeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.AccountDoorTypeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorType field.
	/// </summary>
	public void SetAccountDoorTypeFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.AccountDoorTypeColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountDoorType_.Status_id field.
	/// </summary>
	public ColumnValue GetStatus_idValue()
	{
		return this.GetValue(TableUtils.Status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's AccountDoorType_.Status_id field.
	/// </summary>
	public bool GetStatus_idFieldValue()
	{
		return this.GetValue(TableUtils.Status_idColumn).ToBoolean();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.Status_id field.
	/// </summary>
	public void SetStatus_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.Status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.Status_id field.
	/// </summary>
	public void SetStatus_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.Status_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.Status_id field.
	/// </summary>
	public void SetStatus_idFieldValue(bool val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.Status_idColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public Int32 AccountDoorTypeId
	{
		get
		{
			return this.GetValue(TableUtils.AccountDoorTypeIdColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.AccountDoorTypeIdColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool AccountDoorTypeIdSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.AccountDoorTypeIdColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorTypeId field.
	/// </summary>
	public string AccountDoorTypeIdDefault
	{
		get
		{
			return TableUtils.AccountDoorTypeIdColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's AccountDoorType_.AccountDoorType field.
	/// </summary>
	public string AccountDoorType
	{
		get
		{
			return this.GetValue(TableUtils.AccountDoorTypeColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.AccountDoorTypeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool AccountDoorTypeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.AccountDoorTypeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.AccountDoorType field.
	/// </summary>
	public string AccountDoorTypeDefault
	{
		get
		{
			return TableUtils.AccountDoorTypeColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's AccountDoorType_.Status_id field.
	/// </summary>
	public bool Status_id
	{
		get
		{
			return this.GetValue(TableUtils.Status_idColumn).ToBoolean();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
	   		this.SetValue(cv, TableUtils.Status_idColumn);
		}
	}
	
	

	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool Status_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.Status_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's AccountDoorType_.Status_id field.
	/// </summary>
	public string Status_idDefault
	{
		get
		{
			return TableUtils.Status_idColumn.DefaultValue;
		}
	}


#endregion
}

}
