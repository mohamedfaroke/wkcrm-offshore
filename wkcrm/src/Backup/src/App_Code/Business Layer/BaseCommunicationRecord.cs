﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in CommunicationRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="CommunicationRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="CommunicationTable"></see> class.
/// </remarks>
/// <seealso cref="CommunicationTable"></seealso>
/// <seealso cref="CommunicationRecord"></seealso>
public class BaseCommunicationRecord : PrimaryKeyRecord
{

	public readonly static CommunicationTable TableUtils = CommunicationTable.Instance;

	// Constructors
 
	protected BaseCommunicationRecord() : base(TableUtils)
	{
	}

	protected BaseCommunicationRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.account_id field.
	/// </summary>
	public ColumnValue Getaccount_idValue()
	{
		return this.GetValue(TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.account_id field.
	/// </summary>
	public Decimal Getaccount_idFieldValue()
	{
		return this.GetValue(TableUtils.account_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.account_id field.
	/// </summary>
	public void Setaccount_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.account_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.datetime field.
	/// </summary>
	public ColumnValue Getdatetime0Value()
	{
		return this.GetValue(TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.datetime field.
	/// </summary>
	public DateTime Getdatetime0FieldValue()
	{
		return this.GetValue(TableUtils.datetime0Column).ToDateTime();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(string val)
	{
		this.SetString(val, TableUtils.datetime0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.datetime field.
	/// </summary>
	public void Setdatetime0FieldValue(DateTime val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.datetime0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.employee_id field.
	/// </summary>
	public ColumnValue Getemployee_idValue()
	{
		return this.GetValue(TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.employee_id field.
	/// </summary>
	public Decimal Getemployee_idFieldValue()
	{
		return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.employee_id field.
	/// </summary>
	public void Setemployee_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.employee_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Communication_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Communication_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Communication_.account_id field.
	/// </summary>
	public Decimal account_id
	{
		get
		{
			return this.GetValue(TableUtils.account_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.account_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool account_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.account_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.account_id field.
	/// </summary>
	public string account_idDefault
	{
		get
		{
			return TableUtils.account_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Communication_.datetime field.
	/// </summary>
	public DateTime datetime0
	{
		get
		{
			return this.GetValue(TableUtils.datetime0Column).ToDateTime();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.datetime0Column);
			
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool datetime0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.datetime0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.datetime field.
	/// </summary>
	public string datetime0Default
	{
		get
		{
			return TableUtils.datetime0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Communication_.employee_id field.
	/// </summary>
	public Decimal employee_id
	{
		get
		{
			return this.GetValue(TableUtils.employee_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.employee_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool employee_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.employee_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.employee_id field.
	/// </summary>
	public string employee_idDefault
	{
		get
		{
			return TableUtils.employee_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Communication_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Communication_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}


#endregion
}

}
