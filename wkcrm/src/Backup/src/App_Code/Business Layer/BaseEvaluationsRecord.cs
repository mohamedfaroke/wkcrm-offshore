﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in EvaluationsRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="EvaluationsRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="EvaluationsTable"></see> class.
/// </remarks>
/// <seealso cref="EvaluationsTable"></seealso>
/// <seealso cref="EvaluationsRecord"></seealso>
public class BaseEvaluationsRecord : PrimaryKeyRecord
{

	public readonly static EvaluationsTable TableUtils = EvaluationsTable.Instance;

	// Constructors
 
	protected BaseEvaluationsRecord() : base(TableUtils)
	{
	}

	protected BaseEvaluationsRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Evaluations_.EvaluationID field.
	/// </summary>
	public ColumnValue GetEvaluationIDValue()
	{
		return this.GetValue(TableUtils.EvaluationIDColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Evaluations_.EvaluationID field.
	/// </summary>
	public Int32 GetEvaluationIDFieldValue()
	{
		return this.GetValue(TableUtils.EvaluationIDColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Evaluations_.EvaluationName field.
	/// </summary>
	public ColumnValue GetEvaluationNameValue()
	{
		return this.GetValue(TableUtils.EvaluationNameColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Evaluations_.EvaluationName field.
	/// </summary>
	public string GetEvaluationNameFieldValue()
	{
		return this.GetValue(TableUtils.EvaluationNameColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Evaluations_.EvaluationName field.
	/// </summary>
	public void SetEvaluationNameFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.EvaluationNameColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Evaluations_.EvaluationName field.
	/// </summary>
	public void SetEvaluationNameFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.EvaluationNameColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Evaluations_.Description field.
	/// </summary>
	public ColumnValue GetDescriptionValue()
	{
		return this.GetValue(TableUtils.DescriptionColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's Evaluations_.Description field.
	/// </summary>
	public string GetDescriptionFieldValue()
	{
		return this.GetValue(TableUtils.DescriptionColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Evaluations_.Description field.
	/// </summary>
	public void SetDescriptionFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.DescriptionColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Evaluations_.Description field.
	/// </summary>
	public void SetDescriptionFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.DescriptionColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's Evaluations_.EvaluationID field.
	/// </summary>
	public Int32 EvaluationID
	{
		get
		{
			return this.GetValue(TableUtils.EvaluationIDColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.EvaluationIDColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool EvaluationIDSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.EvaluationIDColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Evaluations_.EvaluationID field.
	/// </summary>
	public string EvaluationIDDefault
	{
		get
		{
			return TableUtils.EvaluationIDColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Evaluations_.EvaluationName field.
	/// </summary>
	public string EvaluationName
	{
		get
		{
			return this.GetValue(TableUtils.EvaluationNameColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.EvaluationNameColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool EvaluationNameSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.EvaluationNameColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Evaluations_.EvaluationName field.
	/// </summary>
	public string EvaluationNameDefault
	{
		get
		{
			return TableUtils.EvaluationNameColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's Evaluations_.Description field.
	/// </summary>
	public string Description
	{
		get
		{
			return this.GetValue(TableUtils.DescriptionColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.DescriptionColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool DescriptionSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.DescriptionColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's Evaluations_.Description field.
	/// </summary>
	public string DescriptionDefault
	{
		get
		{
			return TableUtils.DescriptionColumn.DefaultValue;
		}
	}


#endregion
}

}
