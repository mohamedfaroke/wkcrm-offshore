﻿// This class is "generated" and will be overwritten.
// Your customizations should be made in View_ContractTradeItemAndTradeTypeRecord.cs

using System;
using System.Collections;
using System.Data.SqlTypes;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Data.SqlProvider;

namespace WKCRM.Business
{

/// <summary>
/// The generated superclass for the <see cref="View_ContractTradeItemAndTradeTypeRecord"></see> class.
/// </summary>
/// <remarks>
/// This class is not intended to be instantiated directly.  To obtain an instance of this class, 
/// use the methods of the <see cref="View_ContractTradeItemAndTradeTypeView"></see> class.
/// </remarks>
/// <seealso cref="View_ContractTradeItemAndTradeTypeView"></seealso>
/// <seealso cref="View_ContractTradeItemAndTradeTypeRecord"></seealso>
public class BaseView_ContractTradeItemAndTradeTypeRecord : PrimaryKeyRecord
{

	public readonly static View_ContractTradeItemAndTradeTypeView TableUtils = View_ContractTradeItemAndTradeTypeView.Instance;

	// Constructors
 
	protected BaseView_ContractTradeItemAndTradeTypeRecord() : base(TableUtils)
	{
	}

	protected BaseView_ContractTradeItemAndTradeTypeRecord(PrimaryKeyRecord record) : base(record, TableUtils)
	{
	}







#region "Convenience methods to get/set values of fields"

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public ColumnValue Getid0Value()
	{
		return this.GetValue(TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public Decimal Getid0FieldValue()
	{
		return this.GetValue(TableUtils.id0Column).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public void Setid0FieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public void Setid0FieldValue(string val)
	{
		this.SetString(val, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public void Setid0FieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public void Setid0FieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public void Setid0FieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.id0Column);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public ColumnValue Getcontract_idValue()
	{
		return this.GetValue(TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public Decimal Getcontract_idFieldValue()
	{
		return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public void Setcontract_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.contract_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public ColumnValue Gettrade_task_idValue()
	{
		return this.GetValue(TableUtils.trade_task_idColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public Int32 Gettrade_task_idFieldValue()
	{
		return this.GetValue(TableUtils.trade_task_idColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public void Settrade_task_idFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.trade_task_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public void Settrade_task_idFieldValue(string val)
	{
		this.SetString(val, TableUtils.trade_task_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public void Settrade_task_idFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.trade_task_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public void Settrade_task_idFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.trade_task_idColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public void Settrade_task_idFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.trade_task_idColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public ColumnValue GetunitsValue()
	{
		return this.GetValue(TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public Decimal GetunitsFieldValue()
	{
		return this.GetValue(TableUtils.unitsColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public void SetunitsFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public void SetunitsFieldValue(string val)
	{
		this.SetString(val, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public void SetunitsFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public void SetunitsFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public void SetunitsFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.unitsColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public ColumnValue GetpriceValue()
	{
		return this.GetValue(TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public Decimal GetpriceFieldValue()
	{
		return this.GetValue(TableUtils.priceColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public void SetpriceFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public void SetpriceFieldValue(string val)
	{
		this.SetString(val, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public void SetpriceFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public void SetpriceFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public void SetpriceFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.priceColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public ColumnValue GetamountValue()
	{
		return this.GetValue(TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public Decimal GetamountFieldValue()
	{
		return this.GetValue(TableUtils.amountColumn).ToDecimal();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public void SetamountFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public void SetamountFieldValue(string val)
	{
		this.SetString(val, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public void SetamountFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public void SetamountFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public void SetamountFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.amountColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.notes field.
	/// </summary>
	public ColumnValue GetnotesValue()
	{
		return this.GetValue(TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.notes field.
	/// </summary>
	public string GetnotesFieldValue()
	{
		return this.GetValue(TableUtils.notesColumn).ToString();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.notes field.
	/// </summary>
	public void SetnotesFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.notesColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.notes field.
	/// </summary>
	public void SetnotesFieldValue(string val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.notesColumn);
	}
	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public ColumnValue GetTradeValue()
	{
		return this.GetValue(TableUtils.TradeColumn);
	}

	/// <summary>
	/// This is a convenience method that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public Int32 GetTradeFieldValue()
	{
		return this.GetValue(TableUtils.TradeColumn).ToInt32();
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public void SetTradeFieldValue(ColumnValue val)
	{
		this.SetValue(val, TableUtils.TradeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public void SetTradeFieldValue(string val)
	{
		this.SetString(val, TableUtils.TradeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public void SetTradeFieldValue(double val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public void SetTradeFieldValue(decimal val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradeColumn);
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public void SetTradeFieldValue(long val)
	{
		ColumnValue cv = new ColumnValue(val);
		this.SetValue(cv, TableUtils.TradeColumn);
	}


#endregion

#region "Convenience methods to get field names"

	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public Decimal id0
	{
		get
		{
			return this.GetValue(TableUtils.id0Column).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.id0Column);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool id0Specified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.id0Column);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.id field.
	/// </summary>
	public string id0Default
	{
		get
		{
			return TableUtils.id0Column.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public Decimal contract_id
	{
		get
		{
			return this.GetValue(TableUtils.contract_idColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.contract_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool contract_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.contract_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.contract_id field.
	/// </summary>
	public string contract_idDefault
	{
		get
		{
			return TableUtils.contract_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public Int32 trade_task_id
	{
		get
		{
			return this.GetValue(TableUtils.trade_task_idColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.trade_task_idColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool trade_task_idSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.trade_task_idColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.trade_task_id field.
	/// </summary>
	public string trade_task_idDefault
	{
		get
		{
			return TableUtils.trade_task_idColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public Decimal units
	{
		get
		{
			return this.GetValue(TableUtils.unitsColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.unitsColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool unitsSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.unitsColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.units field.
	/// </summary>
	public string unitsDefault
	{
		get
		{
			return TableUtils.unitsColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public Decimal price
	{
		get
		{
			return this.GetValue(TableUtils.priceColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.priceColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool priceSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.priceColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.price field.
	/// </summary>
	public string priceDefault
	{
		get
		{
			return TableUtils.priceColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public Decimal amount
	{
		get
		{
			return this.GetValue(TableUtils.amountColumn).ToDecimal();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.amountColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool amountSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.amountColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.amount field.
	/// </summary>
	public string amountDefault
	{
		get
		{
			return TableUtils.amountColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.notes field.
	/// </summary>
	public string notes
	{
		get
		{
			return this.GetValue(TableUtils.notesColumn).ToString();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.notesColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool notesSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.notesColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.notes field.
	/// </summary>
	public string notesDefault
	{
		get
		{
			return TableUtils.notesColumn.DefaultValue;
		}
	}
	/// <summary>
	/// This is a property that provides direct access to the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public Int32 Trade
	{
		get
		{
			return this.GetValue(TableUtils.TradeColumn).ToInt32();
		}
		set
		{
			ColumnValue cv = new ColumnValue(value);
			this.SetValue(cv, TableUtils.TradeColumn);
		}
	}


	/// <summary>
	/// This is a convenience method that can be used to determine that the column is set.
	/// </summary>
	public bool TradeSpecified
	{
		get
		{
			ColumnValue val = this.GetValue(TableUtils.TradeColumn);
            if (val == null || val.IsNull)
            {
                return false;
            }
            return true;
		}
	}

	/// <summary>
	/// This is a convenience method that allows direct modification of the value of the record's View_ContractTradeItemAndTradeType_.Trade field.
	/// </summary>
	public string TradeDefault
	{
		get
		{
			return TableUtils.TradeColumn.DefaultValue;
		}
	}


#endregion
}

}
