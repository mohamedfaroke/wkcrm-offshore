﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// Survey.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.Survey
{
  

#region "Section 1: Place your customizations here."

    
//public class ProjectEvaluationAnswersTableControlRow : BaseProjectEvaluationAnswersTableControlRow
//{
//      
//        // The BaseProjectEvaluationAnswersTableControlRow implements code for a ROW within the
//        // the ProjectEvaluationAnswersTableControl table.  The BaseProjectEvaluationAnswersTableControlRow implements the DataBind and SaveData methods.
//        // The loading of data is actually performed by the LoadData method in the base class of ProjectEvaluationAnswersTableControl.
//
//        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
//        // SaveData, GetUIData, and Validate methods.
//
//    public override void DataBind()
//    {
//        base.DataBind();
//        if (this.DataSource != null)
//        {
//            EvaluationQuestionRecord question = EvaluationQuestionTable.GetRecord(this.DataSource.QuestionID_EvaluationQuestion.ToString(), false);
//            if (question.TypeID == 1)
//            {
//                this.Answer.Enabled = false;
//                this.Answer.Visible = false;
//            }
//            else
//            {
//                this.OptionId.Enabled = false;
//                this.OptionId.Visible = false;
//            }
//        }
//    }
//
//    public override WhereClause CreateWhereClause_OptionIdDropDownList()
//    {
//        return Globals.BuildQuestionOptionsWC(this.DataSource.QuestionID_EvaluationQuestion.ToString());
//    }
//
//    protected override void PopulateOptionIdDropDownList(string selectedValue, int maxItems)
//    {
//      //  base.PopulateOptionIdDropDownList(selectedValue, maxItems);
//        //Setup the WHERE clause.
//        WhereClause wc = CreateWhereClause_OptionIdDropDownList();
//        OrderBy orderBy = new OrderBy(false, true);
//        orderBy.Add(QuestionOptionTable.DisplayOrder, OrderByItem.OrderDir.Asc);
//
//        this.OptionId.Items.Clear();
//        foreach (QuestionOptionRecord itemValue in QuestionOptionTable.GetRecords(wc, orderBy, 0, maxItems))
//        {
//            // Create the item and add to the list.
//            string cvalue = null;
//            string fvalue = null;
//            if (itemValue.OptionIDSpecified)
//            {
//                cvalue = itemValue.OptionID.ToString();
//                fvalue = itemValue.Format(QuestionOptionTable.OptionText);
//            }
//
//            ListItem item = new ListItem(fvalue, cvalue);
//            this.OptionId.Items.Add(item);
//        }
//
//        // Setup the selected item.
//        if (selectedValue != null &&
//            selectedValue.Length > 0 &&
//            !MiscUtils.SetSelectedValue(this.OptionId, selectedValue) &&
//            !MiscUtils.SetSelectedValue(this.OptionId, ProjectEvaluationAnswersTable.OptionId.Format(selectedValue)))
//        {
//            string fvalue = ProjectEvaluationAnswersTable.OptionId.Format(selectedValue);
//            ListItem item = new ListItem(fvalue, selectedValue);
//            item.Selected = true;
//            this.OptionId.Items.Insert(0, item);
//        }
//        this.OptionId.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
//    }
//}
//

  

//public class ProjectEvaluationAnswersTableControl : BaseProjectEvaluationAnswersTableControl
//{
//        // The BaseProjectEvaluationAnswersTableControl class implements the LoadData, DataBind, CreateWhereClause
//        // and other methods to load and display the data in a table control.
//
//        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
//        // The ProjectEvaluationAnswersTableControlRow class offers another place where you can customize
//        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.
//
//}
//

  

public class View_ProjectEvaulationQuestionsTableControl : BaseView_ProjectEvaulationQuestionsTableControl
{
        // The BaseView_ProjectEvaulationQuestionsTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The View_ProjectEvaulationQuestionsTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.

}
public class View_ProjectEvaulationQuestionsTableControlRow : BaseView_ProjectEvaulationQuestionsTableControlRow
{
      
        // The BaseView_ProjectEvaulationQuestionsTableControlRow implements code for a ROW within the
        // the View_ProjectEvaulationQuestionsTableControl table.  The BaseView_ProjectEvaulationQuestionsTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of View_ProjectEvaulationQuestionsTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public override void DataBind()
    {
        base.DataBind();
        if (this.DataSource != null)
        {
            if (this.DataSource.TypeID == 1)
            {
                Answer.Enabled = false;
                Answer.Visible = false;
            }
            else
            {
                OptionId.Enabled = false;
                OptionId.Visible = false;
            }
        }
    }
    public override WhereClause CreateWhereClause_OptionIdDropDownList()
    {
        return Globals.BuildQuestionOptionsWC(this.DataSource.QuestionId.ToString());
    }

    protected override void PopulateOptionIdDropDownList(string selectedValue, int maxItems)
    {
        //Setup the WHERE clause.
        WhereClause wc = CreateWhereClause_OptionIdDropDownList();
        OrderBy orderBy = new OrderBy(false, true);
        orderBy.Add(QuestionOptionTable.DisplayOrder, OrderByItem.OrderDir.Asc);

        this.OptionId.Items.Clear();
        foreach (QuestionOptionRecord itemValue in QuestionOptionTable.GetRecords(wc, orderBy, 0, maxItems))
        {
            // Create the item and add to the list.
            string cvalue = null;
            string fvalue = null;
            if (itemValue.OptionIDSpecified)
            {
                cvalue = itemValue.OptionID.ToString();
                fvalue = itemValue.Format(QuestionOptionTable.OptionText);
            }

            ListItem item = new ListItem(fvalue, cvalue);
            this.OptionId.Items.Add(item);
        }

        // Setup the selected item.
        if (selectedValue != null &&
            selectedValue.Length > 0 &&
            !MiscUtils.SetSelectedValue(this.OptionId, selectedValue) &&
            !MiscUtils.SetSelectedValue(this.OptionId, View_ProjectEvaulationQuestionsView.OptionId.Format(selectedValue)))
        {
            string fvalue = View_ProjectEvaulationQuestionsView.OptionId.Format(selectedValue);
            ListItem item = new ListItem(fvalue, selectedValue);
            item.Selected = true;
            this.OptionId.Items.Insert(0, item);
        }


        this.OptionId.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));


    }

}
#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the View_ProjectEvaulationQuestionsTableControlRow control on the Survey page.
// Do not modify this class. Instead override any method in View_ProjectEvaulationQuestionsTableControlRow.
public class BaseView_ProjectEvaulationQuestionsTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BaseView_ProjectEvaulationQuestionsTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = View_ProjectEvaulationQuestionsView.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BaseView_ProjectEvaulationQuestionsTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new View_ProjectEvaulationQuestionsRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.AnswerSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ProjectEvaulationQuestionsView.Answer);
                this.Answer.Text = formattedValue;
            } else {  
                this.Answer.Text = View_ProjectEvaulationQuestionsView.Answer.Format(View_ProjectEvaulationQuestionsView.Answer.DefaultValue);
            }
                    
            if (this.DataSource.designerIdSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ProjectEvaulationQuestionsView.designerId);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.designerId.Text = formattedValue;
            } else {  
                this.designerId.Text = View_ProjectEvaulationQuestionsView.designerId.Format(View_ProjectEvaulationQuestionsView.designerId.DefaultValue);
            }
                    
            if (this.designerId.Text == null ||
                this.designerId.Text.Trim().Length == 0) {
                this.designerId.Text = "N/A";
            }
                  
            if (this.DataSource.OptionIdSpecified) {
                this.PopulateOptionIdDropDownList(this.DataSource.OptionId.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.PopulateOptionIdDropDownList(View_ProjectEvaulationQuestionsView.OptionId.DefaultValue, 100);
                } else {
                this.PopulateOptionIdDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.QuestionIdSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ProjectEvaulationQuestionsView.QuestionId);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.QuestionId.Text = formattedValue;
            } else {  
                this.QuestionId.Text = View_ProjectEvaulationQuestionsView.QuestionId.Format(View_ProjectEvaulationQuestionsView.QuestionId.DefaultValue);
            }
                    
            if (this.DataSource.tradesmanIdSpecified) {
                      
                string formattedValue = this.DataSource.Format(View_ProjectEvaulationQuestionsView.tradesmanId);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.tradesmanId.Text = formattedValue;
            } else {  
                this.tradesmanId.Text = View_ProjectEvaulationQuestionsView.tradesmanId.Format(View_ProjectEvaulationQuestionsView.tradesmanId.DefaultValue);
            }
                    
            if (this.tradesmanId.Text == null ||
                this.tradesmanId.Text.Trim().Length == 0) {
                this.tradesmanId.Text = "N/A";
            }
                  
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in View_ProjectEvaulationQuestionsTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in View_ProjectEvaulationQuestionsTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((View_ProjectEvaulationQuestionsTableControl)MiscUtils.GetParentControlObject(this, "View_ProjectEvaulationQuestionsTableControl")).DataChanged = true;
                ((View_ProjectEvaulationQuestionsTableControl)MiscUtils.GetParentControlObject(this, "View_ProjectEvaulationQuestionsTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(this.Answer.Text, View_ProjectEvaulationQuestionsView.Answer);
                          
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.OptionId), View_ProjectEvaulationQuestionsView.OptionId);
                  
        }

        //  To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in View_ProjectEvaulationQuestionsTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            View_ProjectEvaulationQuestionsView.DeleteRecord(pk);

          
            ((View_ProjectEvaulationQuestionsTableControl)MiscUtils.GetParentControlObject(this, "View_ProjectEvaulationQuestionsTableControl")).DataChanged = true;
            ((View_ProjectEvaulationQuestionsTableControl)MiscUtils.GetParentControlObject(this, "View_ProjectEvaulationQuestionsTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_OptionIdDropDownList() {
            return new WhereClause();
        }
                
        // Fill the OptionId list.
        protected virtual void PopulateOptionIdDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_OptionIdDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(QuestionOptionTable.OptionText, OrderByItem.OrderDir.Asc);

                      this.OptionId.Items.Clear();
            foreach (QuestionOptionRecord itemValue in QuestionOptionTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.OptionIDSpecified) {
                    cvalue = itemValue.OptionID.ToString();
                    fvalue = itemValue.Format(QuestionOptionTable.OptionText);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.OptionId.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.OptionId, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.OptionId, View_ProjectEvaulationQuestionsView.OptionId.Format(selectedValue))) {
                string fvalue = View_ProjectEvaulationQuestionsView.OptionId.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.OptionId.Items.Insert(0, item);
            }

                  
            this.OptionId.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BaseView_ProjectEvaulationQuestionsTableControlRow_Rec"];
            }
            set {
                this.ViewState["BaseView_ProjectEvaulationQuestionsTableControlRow_Rec"] = value;
            }
        }
        
        private View_ProjectEvaulationQuestionsRecord _DataSource;
        public View_ProjectEvaulationQuestionsRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.TextBox Answer {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Answer");
            }
        }
           
        public System.Web.UI.WebControls.Label designerId {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designerId");
            }
        }
           
        public System.Web.UI.WebControls.DropDownList OptionId {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "OptionId");
            }
        }
           
        public System.Web.UI.WebControls.Label QuestionId {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionId");
            }
        }
           
        public System.Web.UI.WebControls.Label tradesmanId {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmanId");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            View_ProjectEvaulationQuestionsRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public View_ProjectEvaulationQuestionsRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return View_ProjectEvaulationQuestionsView.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the View_ProjectEvaulationQuestionsTableControl control on the Survey page.
// Do not modify this class. Instead override any method in View_ProjectEvaulationQuestionsTableControl.
public class BaseView_ProjectEvaulationQuestionsTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BaseView_ProjectEvaulationQuestionsTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.View_ProjectEvaulationQuestionsSaveButton.Button.Click += new EventHandler(View_ProjectEvaulationQuestionsSaveButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
                this.CurrentSortOrder.Add(View_ProjectEvaulationQuestionsView.OrderNumber, OrderByItem.OrderDir.Asc);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ProjectEvaulationQuestionsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ProjectEvaulationQuestionsRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = View_ProjectEvaulationQuestionsView.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (View_ProjectEvaulationQuestionsRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.View_ProjectEvaulationQuestionsRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (View_ProjectEvaulationQuestionsTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (View_ProjectEvaulationQuestionsRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.View_ProjectEvaulationQuestionsRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = View_ProjectEvaulationQuestionsView.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_ProjectEvaulationQuestionsTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                View_ProjectEvaulationQuestionsTableControlRow recControl = (View_ProjectEvaulationQuestionsTableControlRow)(repItem.FindControl("View_ProjectEvaulationQuestionsTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(View_ProjectEvaulationQuestionsView.designerId, this.DataSource);
            this.Page.PregetDfkaRecords(View_ProjectEvaulationQuestionsView.OptionId, this.DataSource);
            this.Page.PregetDfkaRecords(View_ProjectEvaulationQuestionsView.QuestionId, this.DataSource);
            this.Page.PregetDfkaRecords(View_ProjectEvaulationQuestionsView.tradesmanId, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for View_ProjectEvaulationQuestionsTableControl pagination.
        

            // Bind the pagination labels.
        
            this.View_ProjectEvaulationQuestionsTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (View_ProjectEvaulationQuestionsTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            View_ProjectEvaulationQuestionsView.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            // Get the static clause defined at design time on the Table Panel Wizard
            WhereClause qc = this.CreateQueryClause();
            if (qc != null) {
                wc.iAND(qc);
            }
        
            return (wc);
        }
        
        // Create a where clause for the Static clause defined at design time.
        protected virtual WhereClause CreateQueryClause()
        {
            CompoundFilter filter = new CompoundFilter(CompoundFilter.CompoundingOperators.And_Operator, null);

            filter.AddFilter(new BaseClasses.Data.ParameterValueFilter(BaseClasses.Data.BaseTable.CreateInstance(@"WKCRM.Business.View_ProjectEvaulationQuestionsView, App_Code").TableDefinition.ColumnList.GetByUniqueName(@"View_ProjectEvaulationQuestions_.ProjectEvaluationId"), @"projeval", BaseClasses.Data.BaseFilter.ComparisonOperator.EqualsTo, false, new BaseClasses.Data.IdentifierAliasInfo(@"", null)));

            WhereClause whereClause = new WhereClause();
            whereClause.AddFilter(filter, CompoundFilter.CompoundingOperators.And_Operator);

            return whereClause;

        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("View_ProjectEvaulationQuestionsTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    View_ProjectEvaulationQuestionsTableControlRow recControl = (View_ProjectEvaulationQuestionsTableControlRow)(repItem.FindControl("View_ProjectEvaulationQuestionsTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        View_ProjectEvaulationQuestionsRecord rec = new View_ProjectEvaulationQuestionsRecord();
        
                        if (recControl.Answer.Text != "") {
                            rec.Parse(recControl.Answer.Text, View_ProjectEvaulationQuestionsView.Answer);
                        }
                        if (recControl.designerId.Text != "") {
                            rec.Parse(recControl.designerId.Text, View_ProjectEvaulationQuestionsView.designerId);
                        }
                        if (MiscUtils.IsValueSelected(recControl.OptionId)) {
                            rec.Parse(recControl.OptionId.SelectedItem.Value, View_ProjectEvaulationQuestionsView.OptionId);
                        }
                        if (recControl.QuestionId.Text != "") {
                            rec.Parse(recControl.QuestionId.Text, View_ProjectEvaulationQuestionsView.QuestionId);
                        }
                        if (recControl.tradesmanId.Text != "") {
                            rec.Parse(recControl.tradesmanId.Text, View_ProjectEvaulationQuestionsView.tradesmanId);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new View_ProjectEvaulationQuestionsRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (View_ProjectEvaulationQuestionsRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.View_ProjectEvaulationQuestionsRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(View_ProjectEvaulationQuestionsTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(View_ProjectEvaulationQuestionsTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["View_ProjectEvaulationQuestionsTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["View_ProjectEvaulationQuestionsTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void View_ProjectEvaulationQuestionsSaveButton_Click(object sender, EventArgs args)
        {
            
            bool shouldRedirect = true;
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.IsPageRefresh) {
            
                    this.SaveData();
              
                }
                  this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                shouldRedirect = false;
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
            if (shouldRedirect) {
                this.Page.ShouldSaveControlsToSession = true;
                this.Page.RedirectBack();
            }
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private View_ProjectEvaulationQuestionsRecord[] _DataSource = null;
        public  View_ProjectEvaulationQuestionsRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal AnswerLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "AnswerLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal designerIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "designerIdLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal QuestionIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "QuestionIdLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal tradesmanIdLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "tradesmanIdLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton View_ProjectEvaulationQuestionsSaveButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ProjectEvaulationQuestionsSaveButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal View_ProjectEvaulationQuestionsTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ProjectEvaulationQuestionsTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.Label View_ProjectEvaulationQuestionsTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "View_ProjectEvaulationQuestionsTotalItems");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                View_ProjectEvaulationQuestionsTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                View_ProjectEvaulationQuestionsRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public View_ProjectEvaulationQuestionsTableControlRow GetSelectedRecordControl()
        {
        
            return null;
          
        }

        public View_ProjectEvaulationQuestionsTableControlRow[] GetSelectedRecordControls()
        {
        
            return (View_ProjectEvaulationQuestionsTableControlRow[])((new ArrayList()).ToArray(Type.GetType("WKCRM.UI.Controls.Survey.View_ProjectEvaulationQuestionsTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            View_ProjectEvaulationQuestionsTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (View_ProjectEvaulationQuestionsTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public View_ProjectEvaulationQuestionsTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("View_ProjectEvaulationQuestionsTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                View_ProjectEvaulationQuestionsTableControlRow recControl = (View_ProjectEvaulationQuestionsTableControlRow)repItem.FindControl("View_ProjectEvaulationQuestionsTableControlRow");
                recList.Add(recControl);
            }

            return (View_ProjectEvaulationQuestionsTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.Survey.View_ProjectEvaulationQuestionsTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  