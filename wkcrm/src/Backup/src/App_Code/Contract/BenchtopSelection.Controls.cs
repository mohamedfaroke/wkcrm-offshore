﻿
// This file implements the TableControl, TableControlRow, and RecordControl classes for the 
// BenchtopSelection.aspx page.  The Row or RecordControl classes are the 
// ideal place to add code customizations. For example, you can override the LoadData, 
// CreateWhereClause, DataBind, SaveData, GetUIData, and Validate methods.

#region "Using statements"    

using Microsoft.VisualBasic;
using BaseClasses.Web.UI.WebControls;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseClasses;
using BaseClasses.Data;
using BaseClasses.Utils;
        
using WKCRM.Business;
using WKCRM.Data;
        

#endregion

  
namespace WKCRM.UI.Controls.BenchtopSelection
{
  

#region "Section 1: Place your customizations here."

    
public class contractBenchtopOptionItemTableControlRow : BasecontractBenchtopOptionItemTableControlRow
{
      
        // The BasecontractBenchtopOptionItemTableControlRow implements code for a ROW within the
        // the contractBenchtopOptionItemTableControl table.  The BasecontractBenchtopOptionItemTableControlRow implements the DataBind and SaveData methods.
        // The loading of data is actually performed by the LoadData method in the base class of contractBenchtopOptionItemTableControl.

        // This is the ideal place to add your code customizations. For example, you can override the DataBind, 
        // SaveData, GetUIData, and Validate methods.

    public contractBenchtopOptionItemTableControlRow()
    {
        this.PreRender += new EventHandler(contractBenchtopOptionItemTableControlRow_PreRender);
        this.Load += new EventHandler(contractBenchtopOptionItemTableControlRow_Load);
    }

    void contractBenchtopOptionItemTableControlRow_Load(object sender, EventArgs e)
    {
        this.units.AutoPostBack = true;
    }

    void contractBenchtopOptionItemTableControlRow_PreRender(object sender, EventArgs e)
    {
        if (this.contract_benchtop_option_item.SelectedIndex != 0)
        {
            string optionID = contract_benchtop_option_item.SelectedValue;
            ContractBenchtopOptionsRecord optRec = ContractBenchtopOptionsTable.GetRecord(optionID, false);
            price.Text = optRec.price.ToString("$0.00");
            decimal myUnits = 0;
            if(!String.IsNullOrEmpty(this.units.Text))
                myUnits = Convert.ToDecimal(this.units.Text);
            decimal totalAmt = optRec.price * myUnits;
            Label optionAmt = this.FindControl("optionAmt") as Label;
            if(optionAmt != null)
                optionAmt.Text = totalAmt.ToString("$0.00");
        }
    }
}

  

public class contractBenchtopOptionItemTableControl : BasecontractBenchtopOptionItemTableControl
{
        // The BasecontractBenchtopOptionItemTableControl class implements the LoadData, DataBind, CreateWhereClause
        // and other methods to load and display the data in a table control.

        // This is the ideal place to add your code customizations. You can override the LoadData and CreateWhereClause,
        // The contractBenchtopOptionItemTableControlRow class offers another place where you can customize
        // the DataBind, GetUIData, SaveData and Validate methods specific to each row displayed on the table.
    
    public bool MarkedForDeletion(contractBenchtopOptionItemTableControlRow rec)
    {
        if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0)
        {
            return (false);
        }
        return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
    }

    protected override WhereClause CreateQueryClause()
    {
      //  return base.CreateQueryClause();
        return new WhereClause();
    }
    public override WhereClause CreateWhereClause()
    {
        WhereClause wc = base.CreateWhereClause();
        string bench = this.Page.Request["Bench"];
        if (String.IsNullOrEmpty(bench))//Hack so it does not show any records
            bench = "-1";

        wc.iAND(ContractBenchtopOptionItemTable.contract_benchtop_item, BaseFilter.ComparisonOperator.EqualsTo, bench);

        return wc;
    }
}

  

#endregion

  

#region "Section 2: Do not modify this section."
    
    
// Base class for the contractBenchtopOptionItemTableControlRow control on the BenchtopSelection page.
// Do not modify this class. Instead override any method in contractBenchtopOptionItemTableControlRow.
public class BasecontractBenchtopOptionItemTableControlRow : WKCRM.UI.BaseApplicationRecordControl
{
        public BasecontractBenchtopOptionItemTableControlRow()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        // To customize, override this method in contractBenchtopOptionItemTableControlRow.
        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Register the event handlers.
        
        }

        // To customize, override this method in contractBenchtopOptionItemTableControlRow.
        protected virtual void Control_Load(object sender, System.EventArgs e)
        {
        
        }

        // Read data from database. To customize, override this method in contractBenchtopOptionItemTableControlRow.
        public virtual void LoadData()  
        {
        
            if (this.RecordUniqueId != null && this.RecordUniqueId.Length > 0) {
                this.DataSource = ContractBenchtopOptionItemTable.GetRecord(this.RecordUniqueId, true);
                return;
            }
        
            // Since this is a row in the table, the data for this row is loaded by the 
            // LoadData method of the BasecontractBenchtopOptionItemTableControl when the data for the entire
            // table is loaded.
            this.DataSource = new ContractBenchtopOptionItemRecord();
          
        }

        // Populate the UI controls using the DataSource. To customize, override this method in contractBenchtopOptionItemTableControlRow.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        

            // For each field, check to see if a value is specified.  If a value is specified,
            // then format the value for display.  If no value is specified, use the default value (formatted).

        
            if (this.DataSource.contract_benchtop_option_itemSpecified) {
                this.Populatecontract_benchtop_option_itemDropDownList(this.DataSource.contract_benchtop_option_item.ToString(), 100);
            } else {
                if (!this.DataSource.IsCreated) {
                    this.Populatecontract_benchtop_option_itemDropDownList(ContractBenchtopOptionItemTable.contract_benchtop_option_item.DefaultValue, 100);
                } else {
                this.Populatecontract_benchtop_option_itemDropDownList(null, 100);
                }
            }
                
            if (this.DataSource.priceSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractBenchtopOptionItemTable.price);
                formattedValue = HttpUtility.HtmlEncode(formattedValue);
                this.price.Text = formattedValue;
            } else {  
                this.price.Text = ContractBenchtopOptionItemTable.price.Format(ContractBenchtopOptionItemTable.price.DefaultValue);
            }
                    
            if (this.DataSource.unitsSpecified) {
                      
                string formattedValue = this.DataSource.Format(ContractBenchtopOptionItemTable.units);
                this.units.Text = formattedValue;
            } else {  
                this.units.Text = ContractBenchtopOptionItemTable.units.Format(ContractBenchtopOptionItemTable.units.DefaultValue);
            }
                    
            this.IsNewRecord = true;
            if (this.DataSource.IsCreated) {
                this.IsNewRecord = false;
        
                this.RecordUniqueId = this.DataSource.GetID().ToXmlString();
            }

            

            // Load data for each record and table UI control.
            // Ordering is important because child controls get 
            // their parent ids from their parent UI controls.
            
        }

        //  To customize, override this method in contractBenchtopOptionItemTableControlRow.
        public virtual void SaveData()
        {
            // 1. Load the existing record from the database. Since we save the entire reocrd, this ensures 
            // that fields that are not displayed also properly initialized.
            this.LoadData();
        
            // 2. Validate the data.  Override in contractBenchtopOptionItemTableControlRow to add custom validation.
            this.Validate();

            // 3. Set the values in the record with data from UI controls.  Override in contractBenchtopOptionItemTableControlRow to set additional fields.
            this.GetUIData();

            // 4. Save in the database.
            // We should not save the record if the data did not change. This
            // will save a database hit and avoid triggering any database triggers.
            if (this.DataSource.IsAnyValueChanged) {
                // Save record to database but do not commit.
                // Auto generated ids are available after saving for use by child (dependent) records.
                this.DataSource.Save();
              
                ((contractBenchtopOptionItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopOptionItemTableControl")).DataChanged = true;
                ((contractBenchtopOptionItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopOptionItemTableControl")).ResetData = true;
            }
            this.IsNewRecord = false;
            this.DataChanged = true;
            this.ResetData = true;
            
        }

        //  To customize, override this method in contractBenchtopOptionItemTableControlRow.
        public virtual void GetUIData()
        {
        
            this.DataSource.Parse(MiscUtils.GetValueSelectedPageRequest(this.contract_benchtop_option_item), ContractBenchtopOptionItemTable.contract_benchtop_option_item);
                  
            this.DataSource.Parse(this.units.Text, ContractBenchtopOptionItemTable.units);
                          
        }

        //  To customize, override this method in contractBenchtopOptionItemTableControlRow.
        public virtual WhereClause CreateWhereClause()
        {
        
            return null;
          
        }
        

        //  To customize, override this method in contractBenchtopOptionItemTableControlRow.
        public virtual void Validate()
        {
            // Initially empty.  Override to add custom validation.
        }

        public virtual void Delete()
        {
        
            if (this.IsNewRecord) {
                return;
            }

            KeyValue pk = KeyValue.XmlToKey(this.RecordUniqueId);
            ContractBenchtopOptionItemTable.DeleteRecord(pk);

          
            ((contractBenchtopOptionItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopOptionItemTableControl")).DataChanged = true;
            ((contractBenchtopOptionItemTableControl)MiscUtils.GetParentControlObject(this, "contractBenchtopOptionItemTableControl")).ResetData = true;
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();

                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }

            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            string isNewRecord = (string)ViewState["IsNewRecord"];
            if (isNewRecord != null && isNewRecord.Length > 0) {
                this.IsNewRecord = Boolean.Parse(isNewRecord);
            }
            string myCheckSum = (string)ViewState["CheckSum"];
            if (myCheckSum != null && myCheckSum.Length > 0) {
                this.CheckSum = myCheckSum;
            }
        }

        protected override object SaveViewState()
        {
            ViewState["IsNewRecord"] = this.IsNewRecord.ToString();
            ViewState["CheckSum"] = this.CheckSum;
            return base.SaveViewState();
        }
        
        public virtual WhereClause CreateWhereClause_contract_benchtop_option_itemDropDownList() {
            return new WhereClause();
        }
                
        // Fill the contract_benchtop_option_item list.
        protected virtual void Populatecontract_benchtop_option_itemDropDownList
                (string selectedValue, int maxItems) {
                  
            //Setup the WHERE clause.
            WhereClause wc = CreateWhereClause_contract_benchtop_option_itemDropDownList();
            OrderBy orderBy = new OrderBy(false, true);
            orderBy.Add(ContractBenchtopOptionsTable.name, OrderByItem.OrderDir.Asc);

                      this.contract_benchtop_option_item.Items.Clear();
            foreach (ContractBenchtopOptionsRecord itemValue in ContractBenchtopOptionsTable.GetRecords(wc, orderBy, 0, maxItems)) {
                // Create the item and add to the list.
                string cvalue = null;
                string fvalue = null;
                if (itemValue.id0Specified) {
                    cvalue = itemValue.id0.ToString();
                    fvalue = itemValue.Format(ContractBenchtopOptionsTable.name);
                }

                ListItem item = new ListItem(fvalue, cvalue);
                this.contract_benchtop_option_item.Items.Add(item);
            }
                    
            // Setup the selected item.
            if (selectedValue != null &&
                selectedValue.Length > 0 &&
                !MiscUtils.SetSelectedValue(this.contract_benchtop_option_item, selectedValue) &&
                !MiscUtils.SetSelectedValue(this.contract_benchtop_option_item, ContractBenchtopOptionItemTable.contract_benchtop_option_item.Format(selectedValue))) {
                string fvalue = ContractBenchtopOptionItemTable.contract_benchtop_option_item.Format(selectedValue);
                ListItem item = new ListItem(fvalue, selectedValue);
                item.Selected = true;
                this.contract_benchtop_option_item.Items.Insert(0, item);
            }

                  
            this.contract_benchtop_option_item.Items.Insert(0, new ListItem(Page.GetResourceValue("Txt:PleaseSelect", "WKCRM"), "--PLEASE_SELECT--"));
                  
        }
                
        private bool _IsNewRecord = true;
        public virtual bool IsNewRecord {
            get {
                return this._IsNewRecord;
            }
            set {
                this._IsNewRecord = value;
            }
        }

        private bool _DataChanged = false;
        public virtual bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public virtual bool ResetData {
            get {
                return (this._ResetData);
            }
            set {
                this._ResetData = value;
            }
        }
        
        public String RecordUniqueId {
            get {
                return (string)this.ViewState["BasecontractBenchtopOptionItemTableControlRow_Rec"];
            }
            set {
                this.ViewState["BasecontractBenchtopOptionItemTableControlRow_Rec"] = value;
            }
        }
        
        private ContractBenchtopOptionItemRecord _DataSource;
        public ContractBenchtopOptionItemRecord DataSource {
            get {
                return (this._DataSource);
            }
            set {
                this._DataSource = value;
            }
        }

        private string _checkSum;
        public virtual string CheckSum {
            get {
                return (this._checkSum);
            }
            set {
                this._checkSum = value;
            }
        }

#region "Helper Properties"
           
        public System.Web.UI.WebControls.DropDownList contract_benchtop_option_item {
            get {
                return (System.Web.UI.WebControls.DropDownList)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_benchtop_option_item");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractBenchtopOptionItemRecordRowSelection {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopOptionItemRecordRowSelection");
            }
        }
           
        public System.Web.UI.WebControls.Label price {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "price");
            }
        }
           
        public System.Web.UI.WebControls.TextBox units {
            get {
                return (System.Web.UI.WebControls.TextBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "units");
            }
        }
        
#endregion

#region "Helper Functions"

        public override string ModifyRedirectUrl(string url, string arg)
        {
            ContractBenchtopOptionItemRecord rec = this.GetRecord();

            if (rec == null && url.IndexOf("{") >= 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:RecDataSrcNotInitialized", "WKCRM"));
            }

            return ModifyRedirectUrl(url, arg, rec);
        }

        public ContractBenchtopOptionItemRecord GetRecord()
        {
        
            if (this.DataSource != null) {
                return this.DataSource;
            }
            
            if (this.RecordUniqueId != null) {
                return ContractBenchtopOptionItemTable.GetRecord(this.RecordUniqueId, true);
            }
            
            // Localization.
            throw new Exception(Page.GetResourceValue("Err:RetrieveRec", "WKCRM"));
          
        }

        public BaseApplicationPage Page
        {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

#endregion

}

  
// Base class for the contractBenchtopOptionItemTableControl control on the BenchtopSelection page.
// Do not modify this class. Instead override any method in contractBenchtopOptionItemTableControl.
public class BasecontractBenchtopOptionItemTableControl : WKCRM.UI.BaseApplicationTableControl
{
        public BasecontractBenchtopOptionItemTableControl()
        {
            this.Init += new EventHandler(Control_Init);
            this.Load += new EventHandler(Control_Load);
            this.PreRender += new EventHandler(Control_PreRender);
        }

        protected virtual void Control_Init(object sender, System.EventArgs e)
        {
            // Setup the pagination events.
        

            // Setup the sorting events.
        

            // Setup the button events.
        
            this.contractBenchtopOptionItemAddButton.Button.Click += new EventHandler(contractBenchtopOptionItemAddButton_Click);
            this.contractBenchtopOptionItemDeleteButton.Button.Click += new EventHandler(contractBenchtopOptionItemDeleteButton_Click);

            // Setup the filter and search events.
        

            // Control Initializations.
            // Initialize the table's current sort order.
            if (this.InSession(this, "Order_By")) {
                this.CurrentSortOrder = OrderBy.FromXmlString(this.GetFromSession(this, "Order_By", null));
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
        
            }

    // Setup default pagination settings.
    
            this.PageSize = Convert.ToInt32(this.GetFromSession(this, "Page_Size", "100"));
            this.PageIndex = Convert.ToInt32(this.GetFromSession(this, "Page_Index", "0"));
            this.ClearControlsFromSession();
        }

        protected virtual void Control_Load(object sender, EventArgs e)
        {
    
                // Show confirmation message on Click
                this.contractBenchtopOptionItemDeleteButton.Button.Attributes.Add("onClick", "return (confirm('" + ((BaseApplicationPage)this.Page).GetResourceValue("DeleteConfirm", "WKCRM") + "'));");
        }

        // Read data from database. Returns an array of records that can be assigned
        // to the DataSource table control property.
        public virtual void LoadData()
        {
            try {
            
                // The WHERE clause will be empty when displaying all records in table.
                WhereClause wc = CreateWhereClause();
                if (wc != null && !wc.RunQuery) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (ContractBenchtopOptionItemRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopOptionItemRecord")));
                    return;
                }

                OrderBy orderBy = CreateOrderBy();

                // Get the pagesize from the pagesize control.
                this.GetPageSize();

                // Get the total number of records to be displayed.
                this.TotalRecords = ContractBenchtopOptionItemTable.GetRecordCount(wc);

                // Go to the last page.
                if (this.TotalPages <= 0) {
                    this.PageIndex = 0;
                } else if (this.DisplayLastPage || this.PageIndex >= this.TotalPages) {
                    this.PageIndex = this.TotalPages - 1;
                }

                // Retrieve the records and set the table DataSource.
                // Only PageSize records are fetched starting at PageIndex (zero based).
                if (this.TotalRecords <= 0) {
                    // Initialize an empty array of records
                    ArrayList alist = new ArrayList(0);
                    this.DataSource = (ContractBenchtopOptionItemRecord[])(alist.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopOptionItemRecord")));
                } else if (this.AddNewRecord > 0) {
                    // Get the records from the posted data
                    ArrayList postdata = new ArrayList(0);
                    foreach (contractBenchtopOptionItemTableControlRow rc in this.GetRecordControls()) {
                        if (!rc.IsNewRecord) {
                            rc.DataSource = rc.GetRecord();
                            rc.GetUIData();
                            postdata.Add(rc.DataSource);
                        }
                    }
                    this.DataSource = (ContractBenchtopOptionItemRecord[])(postdata.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopOptionItemRecord")));
                } else {
                    // Get the records from the database
                    this.DataSource = ContractBenchtopOptionItemTable.GetRecords(wc, orderBy, this.PageIndex, this.PageSize);
                }

                // Initialize the page and grand totals. now
            
            } catch (Exception ex) {
                throw ex;
            } finally {
                // Add records to the list.
                this.AddNewRecords();
            }
        }

        // Populate the UI controls.
        public override void DataBind()
        {
            base.DataBind();

            // Make sure that the DataSource is initialized.
            if (this.DataSource == null) {
                return;
            }
        
            // Improve performance by prefetching display as records.
            this.PreFetchForeignKeyValues();

            // Setup the pagination controls.
            BindPaginationControls();

            // Populate all filters data.
        

            // Bind the repeater with the list of records to expand the UI.
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractBenchtopOptionItemTableControlRepeater"));
            rep.DataSource = this.DataSource;
            rep.DataBind();

            int index = 0;
            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                // Loop through all rows in the table, set its DataSource and call DataBind().
                contractBenchtopOptionItemTableControlRow recControl = (contractBenchtopOptionItemTableControlRow)(repItem.FindControl("contractBenchtopOptionItemTableControlRow"));
                recControl.DataSource = this.DataSource[index];
                recControl.DataBind();
                recControl.Visible = !this.InDeletedRecordIds(recControl);
                index += 1;
            }
        }

        
        public void PreFetchForeignKeyValues() {
            if (this.DataSource == null) {
                return;
            }
          
            this.Page.PregetDfkaRecords(ContractBenchtopOptionItemTable.contract_benchtop_option_item, this.DataSource);
        }
         

        protected virtual void BindPaginationControls()
        {
            // Setup the pagination controls.

            // Bind the buttons for contractBenchtopOptionItemTableControl pagination.
        

            // Bind the pagination labels.
        
            this.contractBenchtopOptionItemTotalItems.Text = this.TotalRecords.ToString();
        }

        public virtual void SaveData()
        {
            foreach (contractBenchtopOptionItemTableControlRow recCtl in this.GetRecordControls())
            {
        
                if (this.InDeletedRecordIds(recCtl)) {
                    recCtl.Delete();
                } else {
                    if (recCtl.Visible) {
                        recCtl.SaveData();
                    }
                }
          
            }
            
            this.DataChanged = true;
            this.ResetData = true;
        }

        protected virtual OrderBy CreateOrderBy()
        {
            return this.CurrentSortOrder;
        }

        public virtual WhereClause CreateWhereClause()
        {
            ContractBenchtopOptionItemTable.Instance.InnerFilter = null;
            WhereClause wc = new WhereClause();
            // CreateWhereClause() Compose the WHERE clause consiting of:
            // 1. Static clause defined at design time.
            // 2. User selected filter criteria.
            // 3. User selected search criteria.
        
            // Get the static clause defined at design time on the Table Panel Wizard
            WhereClause qc = this.CreateQueryClause();
            if (qc != null) {
                wc.iAND(qc);
            }
        
            return (wc);
        }
        
        // Create a where clause for the Static clause defined at design time.
        protected virtual WhereClause CreateQueryClause()
        {
            CompoundFilter filter = new CompoundFilter(CompoundFilter.CompoundingOperators.And_Operator, null);

            filter.AddFilter(new BaseClasses.Data.ParameterValueFilter(BaseClasses.Data.BaseTable.CreateInstance(@"WKCRM.Business.ContractBenchtopOptionItemTable, App_Code").TableDefinition.ColumnList.GetByUniqueName(@"ContractBenchtopOptionItem_.contract_benchtop_item"), @"Bench", BaseClasses.Data.BaseFilter.ComparisonOperator.EqualsTo, false, new BaseClasses.Data.IdentifierAliasInfo(@"", null)));

            WhereClause whereClause = new WhereClause();
            whereClause.AddFilter(filter, CompoundFilter.CompoundingOperators.And_Operator);

            return whereClause;

        }
        
        protected virtual void GetPageSize()
        {
        
        }

        protected virtual void AddNewRecords()
        {
            ArrayList newRecordList = new ArrayList();

            // Loop though all the record controls and if the record control
            // does not have a unique record id set, then create a record
            // and add to the list.
            if (!this.ResetData)
            {
                System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)(this.FindControl("contractBenchtopOptionItemTableControlRepeater"));
                int index = 0;

                foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
                {
                    // Loop through all rows in the table, set its DataSource and call DataBind().
                    contractBenchtopOptionItemTableControlRow recControl = (contractBenchtopOptionItemTableControlRow)(repItem.FindControl("contractBenchtopOptionItemTableControlRow"));

                    if (recControl.Visible && recControl.IsNewRecord) {
                        ContractBenchtopOptionItemRecord rec = new ContractBenchtopOptionItemRecord();
        
                        if (MiscUtils.IsValueSelected(recControl.contract_benchtop_option_item)) {
                            rec.Parse(recControl.contract_benchtop_option_item.SelectedItem.Value, ContractBenchtopOptionItemTable.contract_benchtop_option_item);
                        }
                        if (recControl.price.Text != "") {
                            rec.Parse(recControl.price.Text, ContractBenchtopOptionItemTable.price);
                        }
                        if (recControl.units.Text != "") {
                            rec.Parse(recControl.units.Text, ContractBenchtopOptionItemTable.units);
                        }
                        newRecordList.Add(rec);
                    }
                }
            }

            // Add any new record to the list.
            for (int count = 1; count <= this.AddNewRecord; count++) {
                newRecordList.Insert(0, new ContractBenchtopOptionItemRecord());
            }
            this.AddNewRecord = 0;

            // Finally , add any new records to the DataSource.
            if (newRecordList.Count > 0) {
                ArrayList finalList = new ArrayList(this.DataSource);
                finalList.InsertRange(0, newRecordList);

                this.DataSource = (ContractBenchtopOptionItemRecord[])(finalList.ToArray(Type.GetType("WKCRM.Business.ContractBenchtopOptionItemRecord")));
            }
        }

        
        private void AddToDeletedRecordIds(contractBenchtopOptionItemTableControlRow rec)
        {
            if (rec.IsNewRecord) {
                return;
            }

            if (this.DeletedRecordIds != null && this.DeletedRecordIds.Length > 0) {
                this.DeletedRecordIds += ",";
            }

            this.DeletedRecordIds += "[" + rec.RecordUniqueId + "]";
        }

        private bool InDeletedRecordIds(contractBenchtopOptionItemTableControlRow rec)            
        {
            if (this.DeletedRecordIds == null || this.DeletedRecordIds.Length == 0) {
                return (false);
            }

            return (this.DeletedRecordIds.IndexOf("[" + rec.RecordUniqueId + "]") >= 0);
        }

        private String _DeletedRecordIds;
        public String DeletedRecordIds {
            get {
                return (this._DeletedRecordIds);
            }
            set {
                this._DeletedRecordIds = value;
            }
        }
        
        private void Control_PreRender(object sender, System.EventArgs e)
        {
            try {
                DbUtils.StartTransaction();
                
                if (!this.Page.ErrorOnPage && (this.Page.IsPageRefresh || this.DataChanged || this.ResetData)) {
                    this.LoadData();
                    this.DataBind();
                }
                
            } catch (Exception ex) {
                BaseClasses.Utils.MiscUtils.RegisterJScriptAlert(this, "BUTTON_CLICK_MESSAGE", ex.Message);
            } finally {
                DbUtils.EndTransaction();
            }
        }
        
        protected override void SaveControlsToSession()
        {
            base.SaveControlsToSession();

            // Save filter controls to values to session.
        
            
            // Save table control properties to the session.
            if (this.CurrentSortOrder != null) {
                this.SaveToSession(this, "Order_By", this.CurrentSortOrder.ToXmlString());
            }
            this.SaveToSession(this, "Page_Index", this.PageIndex.ToString());
            this.SaveToSession(this, "Page_Size", this.PageSize.ToString());
            
            this.SaveToSession(this, "DeletedRecordIds", this.DeletedRecordIds);
            
        }

        protected override void ClearControlsFromSession()
        {
            base.ClearControlsFromSession();

            // Clear filter controls values from the session.
        
            
            // Clear table properties from the session.
            this.RemoveFromSession(this, "Order_By");
            this.RemoveFromSession(this, "Page_Index");
            this.RemoveFromSession(this, "Page_Size");
            
            this.RemoveFromSession(this, "DeletedRecordIds");
            
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            string orderByStr = (string)ViewState["contractBenchtopOptionItemTableControl_OrderBy"];
            if (orderByStr != null && orderByStr.Length > 0) {
                this.CurrentSortOrder = BaseClasses.Data.OrderBy.FromXmlString(orderByStr);
            } else {
                this.CurrentSortOrder = new OrderBy(true, true);
            }

            if (ViewState["Page_Index"] != null) {
                this.PageIndex = (int)ViewState["Page_Index"];
            }

            if (ViewState["Page_Size"] != null) {
                this.PageSize = (int)ViewState["Page_Size"];
            }
        
            this.DeletedRecordIds = (string)this.ViewState["DeletedRecordIds"];
        
        }

        protected override object SaveViewState()
        {            
            if (this.CurrentSortOrder != null) {
                this.ViewState["contractBenchtopOptionItemTableControl_OrderBy"] = this.CurrentSortOrder.ToXmlString();
            }
            
            this.ViewState["Page_Index"] = this.PageIndex;
            this.ViewState["Page_Size"] = this.PageSize;
        
            this.ViewState["DeletedRecordIds"] = this.DeletedRecordIds;
        
            return (base.SaveViewState());
        }

        // Generate the event handling functions for pagination events.
        

        // Generate the event handling functions for sorting events.
        

        // Generate the event handling functions for button events.
        
        // event handler for Button with Layout
        public virtual void contractBenchtopOptionItemAddButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            this.AddNewRecord = 1;
            this.DataChanged = true;
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          
        // event handler for Button with Layout
        public virtual void contractBenchtopOptionItemDeleteButton_Click(object sender, EventArgs args)
        {
            
            try {
                DbUtils.StartTransaction();
                
            if (!this.Page.IsPageRefresh) {
        
                this.DeleteSelectedRecords(true);
          
            }
                this.Page.CommitTransaction(sender);
            } catch (Exception ex) {
                this.Page.RollBackTransaction(sender);
                this.Page.ErrorOnPage = true;
    
                throw ex;  
            } finally {
                DbUtils.EndTransaction();
            }
    
        }
          

        // Generate the event handling functions for filter and search events.
        

        // verify the processing details for these properties
        private int _PageSize;
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                this._PageSize = value;
            }
        }

        private int _PageIndex;
        public int PageIndex {
            get {
                // _PageSize return (the PageIndex);
                return this._PageIndex;
            }
            set {
                this._PageIndex = value;
            }
        }

        private int _TotalRecords;
        public int TotalRecords {
            get {
                return (this._TotalRecords);
            }
            set {
                if (this.PageSize > 0) {
                    this.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / Convert.ToDouble(this.PageSize)));
                }
                this._TotalRecords = value;
            }
        }

        private int _TotalPages;
        public int TotalPages {
            get {
                return this._TotalPages;
            }
            set {
                this._TotalPages = value;
            }
        }

        private bool _DisplayLastPage;
        public bool DisplayLastPage {
            get {
                return this._DisplayLastPage;
            }
            set {
                this._DisplayLastPage = value;
            }
        }

        private bool _DataChanged = false;
        public bool DataChanged {
            get {
                return this._DataChanged;
            }
            set {
                this._DataChanged = value;
            }
        }

        private bool _ResetData = false;
        public bool ResetData {
            get {
                return this._ResetData;
            }
            set {
                this._ResetData = value;
            }
        }

        private int _AddNewRecord = 0;
        public int AddNewRecord {
            get {
                return this._AddNewRecord;
            }
            set {
                this._AddNewRecord = value;
            }
        }

        private OrderBy _CurrentSortOrder = null;
        public OrderBy CurrentSortOrder {
            get {
                return this._CurrentSortOrder;
            }
            set {
                this._CurrentSortOrder = value;
            }
        }

        private ContractBenchtopOptionItemRecord[] _DataSource = null;
        public  ContractBenchtopOptionItemRecord[] DataSource {
            get {
                return this._DataSource;
            }
            set {
                this._DataSource = value;
            }
        }

#region "Helper Properties"
        
        public System.Web.UI.WebControls.Literal contract_benchtop_option_itemLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contract_benchtop_option_itemLabel");
            }
        }
        
        public WKCRM.UI.IThemeButton contractBenchtopOptionItemAddButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopOptionItemAddButton");
            }
        }
        
        public WKCRM.UI.IThemeButton contractBenchtopOptionItemDeleteButton {
            get {
                return (WKCRM.UI.IThemeButton)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopOptionItemDeleteButton");
            }
        }
        
        public System.Web.UI.WebControls.Literal contractBenchtopOptionItemTableTitle {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopOptionItemTableTitle");
            }
        }
        
        public System.Web.UI.WebControls.CheckBox contractBenchtopOptionItemToggleAll {
            get {
                return (System.Web.UI.WebControls.CheckBox)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopOptionItemToggleAll");
            }
        }
        
        public System.Web.UI.WebControls.Label contractBenchtopOptionItemTotalItems {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "contractBenchtopOptionItemTotalItems");
            }
        }
        
        public System.Web.UI.WebControls.Label Label {
            get {
                return (System.Web.UI.WebControls.Label)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "Label");
            }
        }
        
        public System.Web.UI.WebControls.Literal priceLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "priceLabel");
            }
        }
        
        public System.Web.UI.WebControls.Literal unitsLabel {
            get {
                return (System.Web.UI.WebControls.Literal)BaseClasses.Utils.MiscUtils.FindControlRecursively(this, "unitsLabel");
            }
        }
        
#endregion

#region "Helper Functions"
        
        public override string ModifyRedirectUrl(string url, string arg)
        {
            bool needToProcess = AreAnyUrlParametersForMe(url, arg);
            if (needToProcess) {
                contractBenchtopOptionItemTableControlRow recCtl = this.GetSelectedRecordControl();
                if (recCtl == null && url.IndexOf("{") >= 0) {
                    // Localization.
                    throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
                }

                ContractBenchtopOptionItemRecord rec = null;
                if (recCtl != null) {
                    rec = recCtl.GetRecord();
                }

                return ModifyRedirectUrl(url, arg, rec);
            }
            return url;
        }
          
        public int GetSelectedRecordIndex()
        {
            int counter = 0;
            foreach (contractBenchtopOptionItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractBenchtopOptionItemRecordRowSelection.Checked) {
                    return counter;
                }
                counter += 1;
            }
            return -1;
        }
        
        public contractBenchtopOptionItemTableControlRow GetSelectedRecordControl()
        {
        contractBenchtopOptionItemTableControlRow[] selectedList = this.GetSelectedRecordControls();
            if (selectedList.Length == 0) {
            return null;
            }
            return selectedList[0];
          
        }

        public contractBenchtopOptionItemTableControlRow[] GetSelectedRecordControls()
        {
        
            ArrayList selectedList = new ArrayList(25);
            foreach (contractBenchtopOptionItemTableControlRow recControl in this.GetRecordControls())
            {
                if (recControl.contractBenchtopOptionItemRecordRowSelection.Checked) {
                    selectedList.Add(recControl);
                }
            }
            return (contractBenchtopOptionItemTableControlRow[])(selectedList.ToArray(Type.GetType("WKCRM.UI.Controls.BenchtopSelection.contractBenchtopOptionItemTableControlRow")));
          
        }

        public virtual void DeleteSelectedRecords(bool deferDeletion)
        {
            contractBenchtopOptionItemTableControlRow[] recList = this.GetSelectedRecordControls();
            if (recList.Length == 0) {
                // Localization.
                throw new Exception(Page.GetResourceValue("Err:NoRecSelected", "WKCRM"));
            }
            
            foreach (contractBenchtopOptionItemTableControlRow recCtl in recList)
            {
                if (deferDeletion) {
                    if (!recCtl.IsNewRecord) {
                
                        this.AddToDeletedRecordIds(recCtl);
                  
                    }
                    recCtl.Visible = false;
                
                    recCtl.contractBenchtopOptionItemRecordRowSelection.Checked = false;
                
                } else {
                
                    recCtl.Delete();
                    this.DataChanged = true;
                    this.ResetData = true;
                  
                }
            }
        }

        public contractBenchtopOptionItemTableControlRow[] GetRecordControls()
        {
            ArrayList recList = new ArrayList();
            System.Web.UI.WebControls.Repeater rep = (System.Web.UI.WebControls.Repeater)this.FindControl("contractBenchtopOptionItemTableControlRepeater");

            foreach (System.Web.UI.WebControls.RepeaterItem repItem in rep.Items)
            {
                contractBenchtopOptionItemTableControlRow recControl = (contractBenchtopOptionItemTableControlRow)repItem.FindControl("contractBenchtopOptionItemTableControlRow");
                recList.Add(recControl);
            }

            return (contractBenchtopOptionItemTableControlRow[])recList.ToArray(Type.GetType("WKCRM.UI.Controls.BenchtopSelection.contractBenchtopOptionItemTableControlRow"));
        }

        public BaseApplicationPage Page {
            get {
                return ((BaseApplicationPage)base.Page);
            }
        }

    #endregion

    

    }
  

#endregion
    
  
}

  