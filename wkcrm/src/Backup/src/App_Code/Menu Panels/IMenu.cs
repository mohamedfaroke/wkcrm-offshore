﻿
using Microsoft.VisualBasic;
  
namespace WKCRM.UI
{

  

    public interface IMenu {

#region Interface Properties
        
        IMenu_Item Menu11MenuItem {get;}
                
        IMenu_Item_Highlighted Menu11MenuItemHilited {get;}
                
        IMenu_Item Menu1MenuItem {get;}
                
        IMenu_Item_Highlighted Menu1MenuItemHilited {get;}
                
        IMenu_Item Menu3MenuItem {get;}
                
        IMenu_Item_Highlighted Menu3MenuItemHilited {get;}
                
        IMenu_Item Menu7MenuItem {get;}
                
        IMenu_Item_Highlighted Menu7MenuItemHilited {get;}
                

#endregion

    }

  
}
  