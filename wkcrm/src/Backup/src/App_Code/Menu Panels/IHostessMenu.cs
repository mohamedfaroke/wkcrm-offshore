﻿
using Microsoft.VisualBasic;
  
namespace WKCRM.UI
{

  

    public interface IHostessMenu {

#region Interface Properties
        
        IMenu_Item Menu1MenuItem {get;}
                
        IMenu_Item_Highlighted Menu1MenuItemHilited {get;}
                
        IMenu_Item Menu2MenuItem {get;}
                
        IMenu_Item_Highlighted Menu2MenuItemHilited {get;}
                
        IMenu_Item Menu3MenuItem {get;}
                
        IMenu_Item_Highlighted Menu3MenuItemHilited {get;}
                

#endregion

    }

  
}
  