using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AppointmentBase
/// </summary>
/// 

public enum AppointmentType
{
    LEADMEETING =1,
    PROJECTTASK = 2
};

public class AppointmentBase
{

    protected string _id;
    protected DateTime _start;
    protected string _subject;
    protected AppointmentType _type;

    public AppointmentType AppointmentType
    {
        get { return _type; }
    }

    public string ID
    {
        get
        {
            return _id;
        }

    }

    public DateTime Start
    {
        get { return _start; }
        set { _start = value; }
    }

    public DateTime End { get; set; }

    public string Subject
    {
        get
        {
            return _subject;
        }
        set { _subject = value;}
    }

	public AppointmentBase()
	{
	}

    public void SetupDodgyID(string newid)
    {
        _id = newid;
    }
}
