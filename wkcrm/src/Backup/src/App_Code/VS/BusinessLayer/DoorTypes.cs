﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for DoorTypes
/// </summary>
public class DoorTypes
{

    public enum DoorType
    {
        NONE = 0,
        WhiteMelamine = 1000,
        RawMDF = 1005,
        ColourPanel = 1010,
        Polyurethane = 1030,
        UndercoatedOnly = 1070,
        SolidTimber = 1075,
        TimberVeneerNatural = 1080,
        TimberVeneerLaminex = 1120,
        TimberVeneerNewAge = 1140,
        SampleDoor = 1180,
        TimberVeneerEveneer = 1190,
        Navlam = 1195,
        TimberveneerCertified = 1200,
        TimberveenerFinished = 1205,
        Navurban = 1210,
        Navurban2 = 1215
    };


    public DoorTypes()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}