﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using BaseClasses.Data;
using Telerik.Web.UI;
using WKCRM.Business;
using System.Web.UI;


/// <summary>
/// Summary description for Contract
/// </summary>
public class MyContract
{
    public MyContract()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string SetSecondCheckMeasureFeeLabel()
    {
        return String.Format("Second Check Measure Fee ({0})", Globals.CrmOptions.secondCheckMeasureFee.ToString("$0.00"));
    }

    public static string  SetDifficultDeliveryFeeLabel()
    {
        return String.Format("Difficult/Hard Delivery upto 2 Levels ({0})", Globals.CrmOptions.difficultDeliveryFee.ToString("$0.00"));
    }

    public static string SetDualFinishesFeeLabel()
    {
        return String.Format("Dual Material/Colour Board ({0})", Globals.CrmOptions.dualFinishes.ToString("$0.00"));
    }

    public static string SetHighRiseFeeLabel()
    {
        return String.Format("High Rise ({0})", Globals.CrmOptions.highRise.ToString("$0.00"));
    }

    public static string SetDualFinishesLessThanFeeLabel()
    {
        return String.Format("Dual Material Poly/Veneer/Sandblasted/Gloss Colour Panel < {0}m ({1})", Globals.CrmOptions.dualFinishLessFeeThreshold, Globals.CrmOptions.dualFinishLessFee.ToString("$0.00"));
    }

    public static string SetKickboardsAfterTimberFeeLabel()
    {
        return String.Format("Kickboards fitted after Timber Floor Fee ({0})",
                             Globals.CrmOptions.kickboardsAfterTimber.ToString("$0.00"));
    }

    public static string SetKickboardsWasherAfterTimberFeeLabel()
    {
        return String.Format("Kickboards/integrated dishwasher fitted after Timber Floor Fee ({0})",
                             Globals.CrmOptions.kickboardsWasherAfterTimber.ToString("$0.00"));
    }


    public static string SetOtherItemEditLink(View_ContractOtherItemWithTypesRecord itemRec,Page page)
    {
        string itemParams = "&itemType=" + itemRec.contract_item_type.ToString()
                                    + "&itemId=" + itemRec.id0.ToString();
        string urlPage = "OtherSelection.aspx";

        switch (itemRec.contract_item_type)
        {
            case Globals.CONTRACTITEM_HANDLES:
                urlPage = "HandleSelection.aspx";
                break;
            case (Globals.CONTRACTITEM_ACCESSORIES):
            case (Globals.CONTRACTITEM_MISC):
            case (Globals.CONTRACTITEM_DRAWERS):
            case (Globals.CONTRACTITEM_FITTINGS):
            case (Globals.CONTRACTITEM_BLUMPRODUCTS):
            case (Globals.CONTRACTITEM_CABINETS):
            case (Globals.CONTRACTITEM_BINS):
            case (Globals.CONTRACTITEM_GLASS):
            case (Globals.CONTRACTITEM_WARDROBES):
                urlPage = "MiscSelection.aspx";
                break;
            default:
                urlPage = "OtherSelection.aspx";
                break;
        }

        string url = urlPage + "?Contract=" + itemRec.contract_id.ToString() + itemParams;
        string width = "400";
        RadWindowManager winManager = page.FindControl("RadWindowManager1") as RadWindowManager;
        if (winManager != null)
            width = winManager.Width.Value.ToString();
        string onClientClick = "EditWindow('" + url + "'," + width + ",'" + itemRec.Format(View_ContractOtherItemWithTypesView.contract_item_type) + "')";
        string finalurl = "<a href=\"javascript:" + onClientClick + "\"><img src=\"../Images/Icon_edit.gif\" border=0></a>";
        return finalurl;
    }
}
