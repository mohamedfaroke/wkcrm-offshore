﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseClasses.Utils;

/// <summary>
/// Summary description for MyRoles
/// </summary>
public class MyRoles
{
	public MyRoles()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public enum Roles
    {
        HOSTESS = 5010,
        DESIGNER = 5015,
        SALES_MANAGER = 5020,
        PROJECT_MANAGER = 5025,
        SUPER_ADMIN = 5035
    };


    public static bool IsHostess(string roles)
    {
        return IsRole(Roles.HOSTESS, roles);
    }

    public static bool IsDesigner(string roles)
    {
        return IsRole(Roles.DESIGNER, roles);
    }

    public static bool IsRole(Roles roleToCheck,string roles)
    {
        bool hasRole = false;
        if (!String.IsNullOrEmpty(roles))
        {
            string roleToCheckStr = Convert.ToString((int)roleToCheck);
            string[] delims = new string[] { ";" };
            string[] empRoles = roles.Split(delims, StringSplitOptions.RemoveEmptyEntries);
            hasRole = empRoles.Contains(roleToCheckStr);
        }
        return hasRole;
    }

}