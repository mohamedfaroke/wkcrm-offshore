﻿using System;
using System.Collections.Generic;
using System.Web;
using BaseClasses.Utils;
using WKCRM.Business;

/// <summary>
/// Summary description for PriceListImporter
/// </summary>
public class PriceListImporter : CSVImport
{
    private ColumnDefinition[] colNames = {
                                                    ColumnDefinition.Create("id",false),
                                                    ColumnDefinition.Create("contract_item_type",true),
                                                    ColumnDefinition.Create("name",true),
                                                    ColumnDefinition.Create("unit_price",true),
                                                    ColumnDefinition.Create("sub_category",false),
                                                    ColumnDefinition.Create("supplier_code",false),
                                                    ColumnDefinition.Create("status_id",false),
                                                };

    public PriceListImporter(string filepath)
        : base(filepath)
	{
	}

    protected override ColumnDefinition[] GetColumnNames()
    {
        return colNames;
    }

    protected override CSVImport.RecordImportStatus ProcessCSVLine()
    {
        RecordImportStatus status = RecordImportStatus.NONE;
        try
        {
            DbUtils.StartTransaction();

            #region Read fields from file

            string id = CSVReader["id"];
            int contract_item_type = Convert.ToInt32(CSVReader["contract_item_type"]);
            string name = CSVReader["name"];
            decimal unit_price = Convert.ToDecimal(CSVReader["unit_price"]);
            string sub_category = CSVReader["sub_category"];
            string supplier_code = CSVReader["supplier_code"];
            string status_id = CSVReader["status_id"];
            bool newItem = String.IsNullOrEmpty(id);
            #endregion

            switch (contract_item_type)
            {
                case (Globals.CONTRACTITEM_DOORS):
                    {
                        ContractDoorsRecord dRec = ContractDoorsTable.GetRecord(id, true);
                        dRec.unit_price = unit_price;
                        dRec.name = name;
                        if (status_id.ToLower() == "false")
                            dRec.status_id = false;
                        dRec.Save();
                        break;
                    }
                case (Globals.CONTRACTITEM_BENCHTOPS):
                    {
                        ContractBenchtopsRecord bRec = ContractBenchtopsTable.GetRecord(id, true);
                        bRec.unit_price = unit_price;
                        bRec.name = name;
                        if (status_id.ToLower() == "false")
                            bRec.status_id = false;
                        bRec.Save();
                        break;
                    }
                case (Globals.CONTRACTITEM_TRADES):
                    {
                        ContractTradeTasksRecord tRec = newItem ? new ContractTradeTasksRecord() : ContractTradeTasksTable.GetRecord(id, true);
                        if(newItem)
                        {
                            ContractTradesRecord rec = ContractTradesTable.GetRecord(String.Format("name='{0}'", sub_category));
                            if(rec != null)
                            {
                                tRec.contract_trade_type = rec.id0;
                                tRec.contract_payment_type_id = Globals.PRICETYPE_UNITS;
                                tRec.part_of_standard_price = false;
                            }
                            else
                            {
                                throw new Exception("Cannot find trade type: " + sub_category);
                            }
                        }
                        tRec.price = unit_price;
                        tRec.name = name;
                        tRec.Save();
                        break;
                    }
                default:
                    {
                        ContractOthersRecord oRec = newItem ? new ContractOthersRecord() : ContractOthersTable.GetRecord(id, true);
                        if (newItem)
                        {
                            oRec.contract_item_type = contract_item_type;
                            oRec.contract_payment_type = Globals.PRICETYPE_UNITS;
                            ContractItemTypeRecord itemTypeRec = ContractItemTypeTable.GetRecord(contract_item_type.ToString(), false);
                            oRec.category = itemTypeRec.name.ToUpper();
                        }
                        oRec.price = unit_price;
                        oRec.name = name;
                        oRec.sub_category = sub_category;
                        oRec.supplier_code = supplier_code;
                        if (!newItem && status_id.ToLower() == "false")
                            oRec.status_id = false;
                        oRec.Save();
                        break;
                    }
            }
            status = RecordImportStatus.UPDATED;
            DbUtils.CommitTransaction();
        }
        catch (Exception ex)
        {
            DbUtils.RollBackTransaction();
            ErrorMessage += String.Format("Record {0} - Error has occurred: {1}\n", CSVReader.CurrentRecordIndex, ex.Message);
            status = RecordImportStatus.FAILED;
        }
        finally
        {
            DbUtils.EndTransaction();
        }
        return status;
    }
}