using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;

/// <summary>
/// Summary description for Globals
/// </summary>
/// 
namespace EmpowerIT.Applications
{
    public class DataAccess
    {
        public DataAccess()
        {
        }

        public static string ConnectionString
        {
            get
            {
                string connectionString = "";

                try
                {
                    //Get the database connection
                    connectionString = ConfigurationManager.ConnectionStrings["WKCRMConnectionString"].ConnectionString;
                }
                catch (Exception ex)
                { }

                return connectionString;
            }


        }

        /// <summary>
        /// Populate by running an SQL query on the server with a default timeout of 30 seconds
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dataTable"></param>
        public static void PopulateDataTableByQuery(DataTable dataTable, string sqlStatement)
        {
            PopulateDataTableByQuery(dataTable, sqlStatement, 30);
        }

        /// <summary>
        /// Populate by running an SQL query on the server
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dataTable"></param>
        public static void PopulateDataTableByQuery(DataTable dataTable, string sqlStatement,int timeout)
        {
            try
            {
                //Get the database connection
                SqlConnection connection = new SqlConnection(DataAccess.ConnectionString);

                //Declare the select command for the data source
                SqlCommand selectCommand = new SqlCommand(sqlStatement, connection);
                selectCommand.CommandType = CommandType.Text;
                selectCommand.CommandTimeout = timeout;
                SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            { }
        }

        public static void PopulateDataTableByQuery(ref DataTable dataTable, string sqlStatement, int timeout)
        {
            try
            {
                if (BaseClasses.Data.SqlProvider.SqlTransaction.GetExistingTransaction() != null)
                {
                    SqlConnection connection = BaseClasses.Data.SqlProvider.SqlTransaction.GetExistingTransaction().GetADOConnectionByName("DatabaseLogicRoster1") as SqlConnection;
                    IDbTransaction transaction = BaseClasses.Data.SqlProvider.SqlTransaction.GetExistingTransaction().GetADOTransaction(connection, false);
                    IDbCommand selectCommand = BaseClasses.Data.SqlProvider.SqlProvider.GetCommand(sqlStatement, connection);
                    selectCommand.CommandType = CommandType.Text;
                    selectCommand.CommandTimeout = timeout;
                    selectCommand.Transaction = transaction;
                    IDataAdapter adapter = BaseClasses.Data.SqlProvider.SqlProvider.GetDataAdapter(selectCommand, connection);
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    dataTable = dataSet.Tables[0] as DataTable;
                }
                else
                {
                SqlConnection connection = new SqlConnection(DataAccess.ConnectionString);
                SqlCommand selectCommand = connection.CreateCommand();
                selectCommand.CommandText = sqlStatement;
                selectCommand.CommandType = CommandType.Text;
                selectCommand.CommandTimeout = timeout;
                SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                adapter.Fill(dataTable);
                }                       

            }
            catch (Exception ex)
            { }
        }



        public static bool ExecuteNonQuerySQLStatement(string sqlStatement)
        {
            bool success = false;
            SqlConnection connection = null;
            bool newConnection = false;
            try
            {
                if (BaseClasses.Data.SqlProvider.SqlTransaction.GetExistingTransaction() != null)
                {
                    newConnection = false;
                    connection = BaseClasses.Data.SqlProvider.SqlTransaction.GetExistingTransaction().GetADOConnectionByName("DatabaseLogicRoster1") as SqlConnection;
                    IDbTransaction transaction = BaseClasses.Data.SqlProvider.SqlTransaction.GetExistingTransaction().GetADOTransaction(connection, false);
                    IDbCommand selectCommand = BaseClasses.Data.SqlProvider.SqlProvider.GetCommand(sqlStatement, connection);
                    selectCommand.CommandType = CommandType.Text;
                    selectCommand.Transaction = transaction;
                    selectCommand.ExecuteNonQuery();
                    success = true;
                }
                else
                {
                    newConnection = true;
                    //Get the database connection
                    connection = new SqlConnection(DataAccess.ConnectionString);
                    connection.Open();
                    //Declare the select command for the data source
                    SqlCommand selectCommand = new SqlCommand(sqlStatement, connection);
                    selectCommand.CommandType = CommandType.Text;
                    selectCommand.ExecuteNonQuery();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                success = false;
            }
            finally
            {
                if (newConnection && connection != null)
                    connection.Close();
            }
            return success;
        }


        /// <summary>
        /// Populate by running an SQL Stored Procedure on the server with a default timeout of 30 seconds
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dataTable"></param>
        public static void PopulateDataTableByStoredProcedure(DataTable dataTable, string sqlStoredProcedure, ArrayList parameters)
        {
            PopulateDataTableByStoredProcedure(dataTable, sqlStoredProcedure, parameters, 30);
        }

        /// <summary>
        /// Populate by running an SQL Stored Procedure on the server
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dataTable"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        public static void PopulateDataTableByStoredProcedure(DataTable dataTable, string sqlStoredProcedure, 
                                                             ArrayList parameters,int timeout)
        {
            try
            {
                //Get the database connection
                SqlConnection connection = new SqlConnection(DataAccess.ConnectionString);

                //Declare the select command for the data source
                SqlCommand selectCommand = new SqlCommand(sqlStoredProcedure, connection);
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.CommandTimeout = timeout;
                foreach (SqlParameter param in parameters)
                {
                    selectCommand.Parameters.Add(param);
                }

                SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            { }
        }


        public static string AddToWhereCluase(string initialWhereClause, string toBeAdded)
        {
            string finalWhereClause = initialWhereClause;

            if (!String.IsNullOrEmpty(toBeAdded))
            {
                if (!String.IsNullOrEmpty(initialWhereClause))
                {
                    finalWhereClause += " AND ";
                }
                else
                {
                    finalWhereClause = " WHERE ";
                }

                finalWhereClause += toBeAdded;
            }

            return finalWhereClause;
        }

        /// <summary>
        /// Gets a record count for a give SELECT query
        /// </summary>
        /// <param name="sqlStatement"></param>
        /// <returns></returns>
        public static int GetQueryRecordCount(string sqlStatement)
        {
            int recordCount = 0;
            string recordCountColumnName = "RecordCount";

            try
            {
                sqlStatement = sqlStatement.ToUpper();
                if (sqlStatement.StartsWith("SELECT *"))
                {
                    sqlStatement = sqlStatement.Replace("SELECT *", "SELECT COUNT(*) AS " + recordCountColumnName);
                }

                DataTable dataTable = new DataTable();
                DataAccess.PopulateDataTableByQuery(dataTable, sqlStatement);

                if (dataTable != null && dataTable.Rows.Count > 0 )
                {
                    recordCount = Convert.ToInt32(dataTable.Rows[0][recordCountColumnName]);
                }

            }
            catch (Exception ex)
            { }

            return recordCount;
        }


    }
}