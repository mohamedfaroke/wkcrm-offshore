﻿<%@ Register Tagprefix="Selectors" Namespace="WKCRM" %>

<%@ Register Tagprefix="WKCRM" TagName="Header" Src="../Header and Footer/Header.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu" Src="../Menu Panels/Menu.ascx" %>

<%@ Register Tagprefix="WKCRM" Namespace="WKCRM.UI.Controls.AddLeadMeeting" %>

<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="false" CodeFile="AddLeadMeeting.aspx.cs" Inherits="WKCRM.UI.AddLeadMeeting" %>
<%@ Register Tagprefix="WKCRM" TagName="Footer" Src="../Header and Footer/Footer.ascx" %>

<%@ Register Tagprefix="BaseClasses" Namespace="BaseClasses.Web.UI.WebControls" Assembly="BaseClasses" %>
<%@ Register Tagprefix="WKCRM" TagName="ThemeButton" Src="../Shared/ThemeButton.ascx" %>

<%@ Register Tagprefix="WKCRM" TagName="Menu2" Src="../Menu Panels/Menu2.ascx" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Reference Control="../Shared/AppointmentToolTip.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<title> Wonderful Kitchens CRM v1.0 </title>
	<link rel="stylesheet" rev="stylesheet" type="text/css" href="../Styles/Style.css"/>
	</head>
	<body id="Body1" runat="server" class="pBack">
	<form id="Form1" method="post" runat="server">
		<BaseClasses:ScrollCoordinates id="ScrollCoordinates" runat="server"></BaseClasses:ScrollCoordinates>
		<BaseClasses:BasePageSettings id="PageSettings" runat="server" LoginRequired="NOT_ANONYMOUS" ></BaseClasses:BasePageSettings>
		<script language="JavaScript" type="text/javascript">clearRTL()</script>
		<asp:ScriptManager ID="scriptManager1" runat="server" EnablePartialRendering="True" EnablePageMethods="True" />
		
<!-- PAGE ALREADY PROCESSED - DO NOT REMOVE THIS LINE -->
<table width="100%"><tr><td style="width:15%"></td><td style="width:70%">
		<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
		<tr>
		<td class="pAlign">
		<table cellspacing="0" cellpadding="0" border="0" class="pbTable">
			<tr>
			<td class="pbTL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbT"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbTR"><img src="../Images/space.gif" alt=""/></td>
			</tr>
			<tr>
			<td class="pbL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbC">
			<table cellspacing="0" cellpadding="0" border="0" class="pcTable">
			<tr>
				<td class="pcTL"></td>
				<td class="pcT">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<asp:HyperLink runat="server" id="SkipNavigationLinks" CssClass="skipNavigationLinks" NavigateURL="#StartOfPageContent" Text="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>" ToolTip="&lt;%# GetResourceValue(&quot;Txt:SkipNavigation&quot;, &quot;WKCRM&quot;) %>">

		</asp:HyperLink>
				</td>
				</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>
					<WKCRM:Header runat="server" id="PageHeader">
		</WKCRM:Header>
				</td>
				</tr>
				</table>
				</td>
				<td class="pcTR"></td>
			</tr>
			<tr>
				<td class="pcL">
					
				</td>
				<td class="pcC">			
					<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
					<tr>
					<td>
						<WKCRM:Menu runat="server" id="Menu" HiliteSettings="Menu7MenuItem">
		</WKCRM:Menu>
					</td>
					</tr><tr>
					<td>&nbsp;<WKCRM:Menu2 runat="server" id="Menu1" HiliteSettings="Menu3MenuItem">
		</WKCRM:Menu2></td>
					</tr>
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						
						
						
                        <tr>
						<td class="pContent">
							<a name="StartOfPageContent"></a>
							<WKCRM:leadMeetingsRecordControl runat="server" id="leadMeetingsRecordControl">
					
<!-- Begin Record Panel.html -->

<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("SaveButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureBeginTag(FindControl("OKButton$_Button")) %>
 <table class="dv" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="dh">
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td class="dhel"><img src="../Images/space.gif" alt=""/></td>
	
	<td class="dhi"><asp:Image ImageUrl="../Images/icon_edit.gif" runat="server" id="leadMeetingsDialogIcon" AlternateText="">

					</asp:Image></td>
	<td class="dht" valign="middle">
	<asp:Literal runat="server" id="leadMeetingsDialogTitle" Text="Add Meetings">
					</asp:Literal>
	</td>
	<td class="dher"><img src="../Images/space.gif" alt=""/></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="CollapsibleRegion" style="display:block;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td class="dBody">
	<table cellpadding="0" cellspacing="3" border="0">
	
	<tr>
	<td class="fls">
		<asp:Literal runat="server" id="designer_idLabel" Text="Designer">
					</asp:Literal>
	</td>
	<td class="dfv"><asp:DropDownList runat="server" id="designer_id" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
					</asp:DropDownList>
					<Selectors:FvLlsHyperLink runat="server" id="designer_idFvLlsHyperLink" ControlToUpdate="designer_id" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="employee" Field="Employee_.id" DisplayField="Employee_.name">					</Selectors:FvLlsHyperLink>&nbsp;
					<asp:RequiredFieldValidator runat="server" id="designer_idRequiredFieldValidator" ControlToValidate="designer_id" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Designer&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">					</asp:RequiredFieldValidator>
	<asp:Label runat="server" id="Label" Text="&lt;a href=&quot;javascript:&quot;onclick=&quot;OpenDesignerSchedule(); return false;&quot;>Show Designer Schedules...&lt;/a>">
					</asp:Label></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label1" Text="Meeting Date">
					</asp:Label></td>
	<td class="dfv">
        <telerik:RadDatePicker ID="meetingDate" runat="server">
                   </telerik:RadDatePicker>

	
	</td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label2" Text="Start Time">
					</asp:Label></td>
	<td class="dfv">
<telerik:RadTimePicker ID="meetingTime" runat="server">
<TimeView Culture="English (Australia)" EndTime="20:00:00" Interval="00:30:00" StartTime="08:00:00" />
                   </telerik:RadTimePicker>


</td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="Label3" Text="End Time">
					</asp:Label></td>
	<td class="dfv"><telerik:RadTimePicker ID="meetingEndTime" runat="server">
<TimeView Culture="English (Australia)" EndTime="20:00:00" Interval="00:30:00" StartTime="08:00:00" />
                   </telerik:RadTimePicker></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="addressLabel" Text="Address">
					</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="address" MaxLength="50" Columns="60" CssClass="field_input" Rows="2" TextMode="MultiLine">
					</asp:TextBox>&nbsp;
					<BaseClasses:TextBoxMaxLengthValidator runat="server" id="addressTextBoxMaxLengthValidator" ControlToValidate="address" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Address&quot;) %>">					</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="onsiteLabel" Text="Appointment">
					</asp:Literal></td>
	<td class="dfv"><asp:DropDownList runat="server" id="onsite" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
					</asp:DropDownList>&nbsp;
					<asp:RequiredFieldValidator runat="server" id="onsiteRequiredFieldValidator" ControlToValidate="onsite" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueIsRequired&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Onsite&quot;) %>" Enabled="True" InitialValue="--PLEASE_SELECT--" Text="*">					</asp:RequiredFieldValidator></td>
	</tr><tr>
	<td class="fls"><asp:Label runat="server" id="locationLabel" Text="Location">
					</asp:Label>&nbsp;</td>
	<td class="dfv"><asp:DropDownList runat="server" id="location" CssClass="field_input" onkeypress="dropDownListTypeAhead(this,false)">
					</asp:DropDownList>
					<Selectors:FvLlsHyperLink runat="server" id="locationFvLlsHyperLink" ControlToUpdate="location" Text="&lt;%# GetResourceValue(&quot;LLS:Text&quot;, &quot;WKCRM&quot;) %>" Table="location" Field="Location_.id" DisplayField="Location_.name">					</Selectors:FvLlsHyperLink></td>
	</tr>
	
	<tr>
	<td class="fls"><asp:Literal runat="server" id="bench_and_benchtop_selectionLabel" Text="Notes">
					</asp:Literal></td>
	<td class="dfv"><asp:TextBox runat="server" id="bench_and_benchtop_selection" MaxLength="50" Columns="60" CssClass="field_input" Rows="3" TextMode="MultiLine">
					</asp:TextBox>&nbsp;
					<BaseClasses:TextBoxMaxLengthValidator runat="server" id="bench_and_benchtop_selectionTextBoxMaxLengthValidator" ControlToValidate="bench_and_benchtop_selection" ErrorMessage="&lt;%# GetResourceValue(&quot;Val:ValueTooLong&quot;, &quot;WKCRM&quot;).Replace(&quot;{FieldName}&quot;, &quot;Notes&quot;) %>">					</BaseClasses:TextBoxMaxLengthValidator></td>
	</tr>
	
	
	
	
	
	
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
 </table>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("OKButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("CancelButton$_Button")) %>
<%= SystemUtils.GenerateEnterKeyCaptureEndTag(FindControl("SaveButton$_Button")) %>
<!-- End Record Panel.html -->

		</WKCRM:leadMeetingsRecordControl>



<table cellpadding="0" cellspacing="0" border="0" id="Table1">
	<tr>
	<td class="rpbAlign">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="SaveButton" Button-CausesValidation="True" Button-CommandName="UpdateData" Button-RedirectArgument="updatedata" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Save&quot;, &quot;WKCRM&quot;) %>">
			</WKCRM:ThemeButton></td>
	<td><img src="../Images/space.gif" height="6" width="3" alt=""/></td>
	<td><WKCRM:ThemeButton runat="server" id="CancelButton" Button-CausesValidation="False" Button-CommandName="Redirect" Button-RedirectURL="Back" Button-Text="&lt;%# GetResourceValue(&quot;Btn:Cancel&quot;, &quot;WKCRM&quot;) %>">
			</WKCRM:ThemeButton></td>
	</tr>
	</table>
	</td>
	</tr>
</table>



							<div id="detailPopup" style="z-index:2; visibility:visible; position:absolute;"></div>
							<div id="detailPopupDropShadow" class="detailPopupDropShadow" style="z-index:1; visibility:visible; position:absolute;"></div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</td>
				<td class="pcR"></td>
			</tr>
			<tr>
				<td class="pcBL"></td>
				<td class="pcB">
				<WKCRM:Footer runat="server" id="PageFooter">
		</WKCRM:Footer>
				</td>
				<td class="pcBR"></td>
			</tr>
			</table>
			</td>
			<td class="pbR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		<tr>
			<td class="pbBL"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbB"><img src="../Images/space.gif" alt=""/></td>
			<td class="pbBR"><img src="../Images/space.gif" alt=""/></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
</td><td style="width:15%"></td></tr></table>
		<asp:ValidationSummary id="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server"></asp:ValidationSummary>
	</form>
	</body>
</html>



