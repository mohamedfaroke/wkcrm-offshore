

DELETE FROM taskNotification;
DBCC CHECKIDENT ( 'taskNotification', RESEED, 1 )
DELETE FROM attachment;
DBCC CHECKIDENT ( 'attachment', RESEED, 1 )
DELETE FROM contractBenchtopOptionItem;
DBCC CHECKIDENT ( 'contractBenchtopOptionItem', RESEED, 1 )
DELETE FROM contractBenchtopItem;
DBCC CHECKIDENT ( 'contractBenchtopItem', RESEED, 1 )
DELETE FROM contractDoorItem;
DBCC CHECKIDENT ( 'contractDoorItem', RESEED, 1 )
DELETE FROM contractOtherItem;
DBCC CHECKIDENT ( 'contractOtherItem', RESEED, 1 )
DELETE FROM contractTradeItem;
DBCC CHECKIDENT ( 'contractTradeItem', RESEED, 1 )
DELETE FROM tasks;
DBCC CHECKIDENT ( 'tasks', RESEED, 1 )

DELETE FROM ProjectEvaluationAnswers;


DELETE FROM ProjectEvaluations;
DBCC CHECKIDENT ( 'ProjectEvaluations', RESEED, 1 )
DELETE FROM project;
DBCC CHECKIDENT ( 'project', RESEED, 1 )
DELETE FROM payments;
DBCC CHECKIDENT ( 'payments', RESEED, 1 )
DELETE FROM paymentVariations;
DBCC CHECKIDENT ( 'paymentVariations', RESEED, 1 )

DELETE FROM paymentScheduleStages;

DELETE FROM paymentSchedule;
DBCC CHECKIDENT ( 'paymentSchedule', RESEED, 1 )
DELETE FROM contractMain;
DBCC CHECKIDENT ( 'contractMain', RESEED, 999 )
DELETE FROM leadMeetings;
DBCC CHECKIDENT ( 'leadMeetings', RESEED, 1 )
DELETE FROM communication;
DBCC CHECKIDENT ( 'communication', RESEED, 1 )
DELETE FROM account;
DBCC CHECKIDENT ( 'account', RESEED, 1000 )
DELETE FROM opportunity;
DBCC CHECKIDENT ( 'opportunity', RESEED, 1 )
