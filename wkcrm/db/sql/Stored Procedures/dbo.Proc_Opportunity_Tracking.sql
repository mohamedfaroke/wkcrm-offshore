SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Proc_Opportunity_Tracking]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime, @DateTo datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
CREATE TABLE #OppTracking
(location_id int,[month] INT,[year] INT, opportunity nvarchar(50), opportunity_id int, opportunity_count int)

CREATE TABLE #MonthRange
([month] INT,[year] INT)

DECLARE @CurrDate DateTime;
DECLARE @CurrMonth INT;
DECLARE @CurrYear INT;

SET @CurrMonth = 0;
SET @CurrYear = 0;

SET @CurrDate = @DateFrom
WHILE(@CurrDate <= @DateTo)
BEGIN
	DECLARE @iMonth INT;
	DECLARE @iYear INT;
	
	SET @iMonth = DATEPART(m,@CurrDate);
	SET @iYear = DATEPART(yy,@CurrDate);
	
	IF (@iMonth != @CurrMonth OR @iYear != @CurrYear)
	BEGIN
		SET @CurrMonth = @iMonth;
		SET @CurrYear = @iYear;
		INSERT INTO #MonthRange (month,year) VALUES (@CurrMonth,@CurrYear)
	END
	SET @CurrDate = DATEADD(d,1,@CurrDate)
END


-- For each bloody location, 

DECLARE @LocationId INT; 
DECLARE @LocationCursor CURSOR


SET @LocationCursor = CURSOR FAST_FORWARD
FOR
SELECT id FROM dbo.location WHERE status_id=1

OPEN @LocationCursor
FETCH NEXT FROM @LocationCursor INTO @LocationId

WHILE @@FETCH_STATUS = 0
BEGIN
-- for each month in the date range, 
	DECLARE @DateCursor CURSOR
	SET @DateCursor = CURSOR FAST_FORWARD
	FOR
	SELECT [month],[year] FROM #MonthRange

	OPEN @DateCursor
	FETCH NEXT FROM @DateCursor INTO @CurrMonth, @CurrYear
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- for each opportunity source, go through them all
		DECLARE @OppSourceId INT
		DECLARE @OppSourceName NVARCHAR(50)
		DECLARE @OppCursor CURSOR
		SET @OppCursor = CURSOR FAST_FORWARD
		FOR
		SELECT dbo.opportunitysource.id, dbo.opportunitySource.name FROM dbo.opportunitySource WHERE status_id=1
		OPEN @OppCursor
		FETCH NEXT FROM @OppCursor  INTO @OppSourceId,@OppSourceName
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @OppCount INT
			SET @OppCount = (SELECT COUNT(*) FROM dbo.opportunity 
								WHERE location_id=@LocationId AND opportunity_source_id = @OppSourceId AND @DateFrom <= [datetime] AND @DateTo >= [datetime] 
									AND DATEPART(m,[datetime])= @CurrMonth AND DATEPART(yy,[datetime])= @CurrYear);
			INSERT INTO #OppTracking
			        ( location_id ,
			          [month] ,
			          [year] ,
			          opportunity ,
			          opportunity_id ,
			          opportunity_count
			        )
			VALUES  ( @LocationId , -- location_id - int
			          @CurrMonth , -- month - int
			          @CurrYear , -- year - int
			          @OppSourceName , -- opportunity - varchar(50)
			          @OppSourceId , -- opportunity_id - int
			          @OppCount  -- opportunity_count - int
			        )
		FETCH NEXT FROM @OppCursor  INTO @OppSourceId,@OppSourceName	        
		END
		CLOSE  @OppCursor
		DEALLOCATE @OppCursor
	
		FETCH NEXT FROM @DateCursor INTO @CurrMonth, @CurrYear	
	
	END
	
	CLOSE @DateCursor
	DEALLOCATE @DateCursor
	
	FETCH NEXT FROM @LocationCursor INTO @LocationId
END
CLOSE @LocationCursor
DEALLOCATE @LocationCursor

SELECT * FROM #OppTracking;

END

GO
