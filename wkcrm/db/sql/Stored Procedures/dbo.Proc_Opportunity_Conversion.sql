
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Fayez Moussa
-- Create date: 14/12/2011
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Proc_Opportunity_Conversion]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime, @DateTo datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
CREATE TABLE #OppConversion
(location_id int,[month] INT,[year] INT, hostess_id int, hostess nvarchar(100), opportunity_count int, lead_count int)

CREATE TABLE #MonthRange
([month] INT,[year] INT)

DECLARE @CurrDate DateTime;
DECLARE @CurrMonth INT;
DECLARE @CurrYear INT;

SET @CurrMonth = 0;
SET @CurrYear = 0;

SET @CurrDate = @DateFrom
WHILE(@CurrDate <= @DateTo)
BEGIN
	DECLARE @iMonth INT;
	DECLARE @iYear INT;
	
	SET @iMonth = DATEPART(m,@CurrDate);
	SET @iYear = DATEPART(yy,@CurrDate);
	
	IF (@iMonth != @CurrMonth OR @iYear != @CurrYear)
	BEGIN
		SET @CurrMonth = @iMonth;
		SET @CurrYear = @iYear;
		INSERT INTO #MonthRange (month,year) VALUES (@CurrMonth,@CurrYear)
	END
	SET @CurrDate = DATEADD(d,1,@CurrDate)
END


DECLARE @LocationId INT; 
DECLARE @LocationCursor CURSOR


SET @LocationCursor = CURSOR FAST_FORWARD
FOR
SELECT id FROM dbo.location WHERE status_id=1

OPEN @LocationCursor
FETCH NEXT FROM @LocationCursor INTO @LocationId

WHILE @@FETCH_STATUS = 0
BEGIN
	-- for each month in the date range, 
	DECLARE @DateCursor CURSOR
	SET @DateCursor = CURSOR FAST_FORWARD
	FOR
	SELECT [month],[year] FROM #MonthRange

	OPEN @DateCursor
	FETCH NEXT FROM @DateCursor INTO @CurrMonth, @CurrYear
	WHILE @@FETCH_STATUS = 0
	BEGIN
	-- get all the hostesses for this location
		DECLARE @EmployeeId INT
		DECLARE @EmployeeName NVARCHAR(100)
		DECLARE @EmployeeCursor CURSOR
		SET @EmployeeCursor = CURSOR FAST_FORWARD
		FOR
		SELECT DISTINCT dbo.employee.id,dbo.employee.name 
		FROM dbo.employee,dbo.employeeRoles 
		WHERE (dbo.employee.location_id=@LocationId AND dbo.employee.id = dbo.employeeRoles.employee_id 
													AND dbo.employeeRoles.role_id=5010 AND dbo.employee.status_id=1)
													OR dbo.employee.id IN (SELECT DISTINCT employee_id FROM opportunity WHERE 
																			location_id=@LocationId AND @DateFrom <= [datetime] 
																			AND @DateTo >= [datetime])
		OPEN @EmployeeCursor
		FETCH NEXT FROM @EmployeeCursor  INTO @EmployeeId,@EmployeeName
		WHILE @@FETCH_STATUS = 0
		BEGIN
		DECLARE @OppCount INT
		DECLARE @LeadCount INT
		SET @OppCount = (SELECT COUNT(*) FROM dbo.opportunity,opportunitySource 
								WHERE opportunity_source_id=dbo.opportunitySource.id AND status_id=1 AND location_id=@LocationId AND employee_id = @EmployeeId AND @DateFrom <= [datetime] AND @DateTo >= [datetime] 
									AND DATEPART(m,[datetime])= @CurrMonth AND DATEPART(yy,[datetime])= @CurrYear);
		-- The following just counts the number of meetings organised for that month, not incorporating the number of unique accounts
		--SET @LeadCount = (SELECT COUNT(*) FROM dbo.leadMeetings
		--						WHERE hostess_id = @EmployeeId AND @DateFrom <= [datetime] AND @DateTo >= [datetime] 
		--							AND DATEPART(m,[datetime])= @CurrMonth AND DATEPART(yy,[datetime])= @CurrYear);
		
		SET @LeadCount = (SELECT COUNT(*) FROM dbo.account WHERE created_by_id=@EmployeeId AND @DateFrom <= [date_created] AND @DateTo >= [date_created] 
									AND DATEPART(m,[date_created])= @CurrMonth AND DATEPART(yy,[date_created])= @CurrYear);							
		INSERT INTO #OppConversion
					( location_id ,
			          [month] ,
			          [year] ,
			          hostess_id ,
			          hostess ,
			          opportunity_count,
			          lead_count
			        )
			VALUES  ( @LocationId , -- location_id - int
			          @CurrMonth , -- month - int
			          @CurrYear , -- year - int
			          @EmployeeId , -- hostess_id - int
			          @EmployeeName , -- hostess - varchar(100)
			          @OppCount ,  -- opportunity_count - int
			          @LeadCount  -- lead_count - int
			        )
		FETCH NEXT FROM @EmployeeCursor  INTO @EmployeeId,@EmployeeName     
		END
		CLOSE  @EmployeeCursor
		DEALLOCATE @EmployeeCursor
	
		FETCH NEXT FROM @DateCursor INTO @CurrMonth, @CurrYear	
	END
	CLOSE @DateCursor
	DEALLOCATE @DateCursor
	
	FETCH NEXT FROM @LocationCursor INTO @LocationId
END
CLOSE @LocationCursor
DEALLOCATE @LocationCursor

SELECT * FROM #OppConversion;
  
END

GO
