SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Fayez Moussa
-- Create date: 14/12/2011
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Proc_Lead_Conversion]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime, @DateTo datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
CREATE TABLE #LeadConversion
(location_id int,[month] INT,[year] INT, designer_id int, designer nvarchar(100), lead_count int, sale_count int)

CREATE TABLE #MonthRange
([month] INT,[year] INT)

DECLARE @CurrDate DateTime;
DECLARE @CurrMonth INT;
DECLARE @CurrYear INT;

SET @CurrMonth = 0;
SET @CurrYear = 0;

SET @CurrDate = @DateFrom
WHILE(@CurrDate <= @DateTo)
BEGIN
	DECLARE @iMonth INT;
	DECLARE @iYear INT;
	
	SET @iMonth = DATEPART(m,@CurrDate);
	SET @iYear = DATEPART(yy,@CurrDate);
	
	IF (@iMonth != @CurrMonth OR @iYear != @CurrYear)
	BEGIN
		SET @CurrMonth = @iMonth;
		SET @CurrYear = @iYear;
		INSERT INTO #MonthRange (month,year) VALUES (@CurrMonth,@CurrYear)
	END
	SET @CurrDate = DATEADD(d,1,@CurrDate)
END


DECLARE @LocationId INT; 
DECLARE @LocationCursor CURSOR


SET @LocationCursor = CURSOR FAST_FORWARD
FOR
SELECT id FROM dbo.location WHERE status_id=1

OPEN @LocationCursor
FETCH NEXT FROM @LocationCursor INTO @LocationId

WHILE @@FETCH_STATUS = 0
BEGIN
	-- for each month in the date range, 
	DECLARE @DateCursor CURSOR
	SET @DateCursor = CURSOR FAST_FORWARD
	FOR
	SELECT [month],[year] FROM #MonthRange

	OPEN @DateCursor
	FETCH NEXT FROM @DateCursor INTO @CurrMonth, @CurrYear
	WHILE @@FETCH_STATUS = 0
	BEGIN
	-- get all the hostesses for this location
		DECLARE @EmployeeId INT
		DECLARE @EmployeeName NVARCHAR(100)
		DECLARE @EmployeeCursor CURSOR
		SET @EmployeeCursor = CURSOR FAST_FORWARD
		FOR
		SELECT dbo.employee.id,dbo.employee.name 
		FROM dbo.employee,dbo.employeeRoles 
		WHERE dbo.employee.location_id=@LocationId AND dbo.employee.id = dbo.employeeRoles.employee_id AND dbo.employeeRoles.role_id=5015 AND dbo.employee.status_id=1
		OPEN @EmployeeCursor
		FETCH NEXT FROM @EmployeeCursor  INTO @EmployeeId,@EmployeeName
		WHILE @@FETCH_STATUS = 0
		BEGIN
		DECLARE @SaleCount INT
		DECLARE @LeadCount INT
		-- Sales are based on all accounts that are in project phase and have their sale date within the specified range
		-- Also sales have to be OPEN or COMPLETED
		SET @SaleCount = (SELECT COUNT(*) FROM dbo.account
								WHERE designer_id = @EmployeeId AND @DateFrom <= sale_date AND @DateTo >= sale_date AND account_type_id=1005 AND account_status_id IN (1000,1020)
									AND DATEPART(m,sale_date)= @CurrMonth AND DATEPART(yy,sale_date)= @CurrYear);
		-- Leads are based on all accounts that have been created within the specified range							
		SET @LeadCount = (SELECT COUNT(*) FROM dbo.account
								WHERE designer_id = @EmployeeId AND @DateFrom <= date_created AND @DateTo >= date_created 
									AND DATEPART(m,date_created)= @CurrMonth AND DATEPART(yy,date_created)= @CurrYear);
		INSERT INTO #LeadConversion
		        ( location_id ,
		          [month] ,
		          [year] ,
		          designer_id ,
		          designer ,
		          lead_count ,
		          sale_count
		        )
		VALUES  ( @LocationId , -- location_id - int
		          @CurrMonth , -- month - int
		          @CurrYear , -- year - int
		          @EmployeeId , -- designer_id - int
		          @EmployeeName , -- designer - nvarchar(100)
		          @LeadCount , -- lead_count - int
		          @SaleCount  -- sale_count - int
		        )
							
		FETCH NEXT FROM @EmployeeCursor  INTO @EmployeeId,@EmployeeName     
		END
		CLOSE  @EmployeeCursor
		DEALLOCATE @EmployeeCursor
	
		FETCH NEXT FROM @DateCursor INTO @CurrMonth, @CurrYear	
	END
	CLOSE @DateCursor
	DEALLOCATE @DateCursor
	
	FETCH NEXT FROM @LocationCursor INTO @LocationId
END
CLOSE @LocationCursor
DEALLOCATE @LocationCursor

SELECT * FROM #LeadConversion;
  
END

GO
