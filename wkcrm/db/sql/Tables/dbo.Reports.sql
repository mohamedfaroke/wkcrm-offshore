CREATE TABLE [dbo].[Reports]
(
[id] [int] NOT NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[filename] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[viewname] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[params] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[description] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[displayOrder] [int] NULL,
[status] [bit] NULL,
[requiredRole] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Reports] ADD CONSTRAINT [PK_Reports] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
