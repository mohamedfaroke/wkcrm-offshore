CREATE TABLE [dbo].[EvaluationQuestion]
(
[QuestionID] [int] NOT NULL IDENTITY(1, 1),
[EvaluationID] [int] NOT NULL,
[Question] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[TypeID] [int] NOT NULL,
[OrderNumber] [int] NOT NULL,
[associated_task] [int] NULL,
[designer_question] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EvaluationQuestion] ADD CONSTRAINT [PK_EvaluationQuestion] PRIMARY KEY CLUSTERED  ([QuestionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EvaluationQuestion] ADD CONSTRAINT [FK_EvaluationQuestion_Evaluations] FOREIGN KEY ([EvaluationID]) REFERENCES [dbo].[Evaluations] ([EvaluationID])
GO
ALTER TABLE [dbo].[EvaluationQuestion] ADD CONSTRAINT [FK_EvaluationQuestion_QuestionType] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[QuestionType] ([typeId])
GO
ALTER TABLE [dbo].[EvaluationQuestion] ADD CONSTRAINT [FK_EvaluationQuestion_tradeTaskTypes] FOREIGN KEY ([associated_task]) REFERENCES [dbo].[tradeTaskTypes] ([id])
GO
