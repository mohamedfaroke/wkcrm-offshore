CREATE TABLE [dbo].[contactSource]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[display_order] [int] NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contactSource] ADD CONSTRAINT [PK_opportunityType] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
