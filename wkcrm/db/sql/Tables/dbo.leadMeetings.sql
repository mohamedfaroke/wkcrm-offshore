CREATE TABLE [dbo].[leadMeetings]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[account_id] [numeric] (18, 0) NOT NULL,
[designer_id] [numeric] (18, 0) NOT NULL,
[datetime] [datetime] NOT NULL,
[end_datetime] [datetime] NULL,
[address] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[onsite] [bit] NOT NULL,
[location_id] [int] NULL,
[hostess_id] [numeric] (18, 0) NOT NULL,
[notes] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[leadMeetings] ADD
CONSTRAINT [FK_leadMeetings_account] FOREIGN KEY ([account_id]) REFERENCES [dbo].[account] ([id])
ALTER TABLE [dbo].[leadMeetings] ADD
CONSTRAINT [FK_leadMeetings_location] FOREIGN KEY ([location_id]) REFERENCES [dbo].[location] ([id])
GO
ALTER TABLE [dbo].[leadMeetings] ADD CONSTRAINT [PK_leadMeetings] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[leadMeetings] ADD CONSTRAINT [FK_leadMeetings_employee] FOREIGN KEY ([designer_id]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[leadMeetings] ADD CONSTRAINT [FK_leadMeetings_employee1] FOREIGN KEY ([hostess_id]) REFERENCES [dbo].[employee] ([id])
GO
