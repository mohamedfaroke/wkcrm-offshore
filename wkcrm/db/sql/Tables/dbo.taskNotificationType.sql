CREATE TABLE [dbo].[taskNotificationType]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[taskNotificationType] ADD CONSTRAINT [PK_taskNotificationType] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
