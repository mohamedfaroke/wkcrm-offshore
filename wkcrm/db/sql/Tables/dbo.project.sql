CREATE TABLE [dbo].[project]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[contract_id] [numeric] (18, 0) NOT NULL,
[date_created] [datetime] NOT NULL,
[created_by] [numeric] (18, 0) NOT NULL,
[date_modified] [datetime] NOT NULL,
[modified_by] [numeric] (18, 0) NOT NULL,
[project_manager] [numeric] (18, 0) NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[project] ADD
CONSTRAINT [FK_project_contractMain] FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contractMain] ([id])
GO
ALTER TABLE [dbo].[project] ADD CONSTRAINT [PK_project] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[project] ADD CONSTRAINT [FK_project_created_by] FOREIGN KEY ([created_by]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[project] ADD CONSTRAINT [FK_project_modified_by] FOREIGN KEY ([modified_by]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[project] ADD CONSTRAINT [FK_project_project_manager] FOREIGN KEY ([project_manager]) REFERENCES [dbo].[employee] ([id])
GO
