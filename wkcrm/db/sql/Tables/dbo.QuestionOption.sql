CREATE TABLE [dbo].[QuestionOption]
(
[OptionID] [int] NOT NULL IDENTITY(1, 1),
[OptionText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[OptionValue] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DisplayOrder] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QuestionOption] ADD CONSTRAINT [PK_QuestionOption] PRIMARY KEY CLUSTERED  ([OptionID]) ON [PRIMARY]
GO
