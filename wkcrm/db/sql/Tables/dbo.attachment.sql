CREATE TABLE [dbo].[attachment]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[account_id] [numeric] (18, 0) NOT NULL,
[attachmentType_id] [int] NOT NULL,
[fileName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[attachment] ADD
CONSTRAINT [FK_attachment_account] FOREIGN KEY ([account_id]) REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[attachment] ADD CONSTRAINT [PK_attachment] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[attachment] ADD CONSTRAINT [FK_attachment_attachmentType] FOREIGN KEY ([attachmentType_id]) REFERENCES [dbo].[attachmentType] ([id])
GO
