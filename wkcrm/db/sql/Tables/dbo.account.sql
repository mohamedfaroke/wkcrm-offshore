CREATE TABLE [dbo].[account]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[FirstName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[LastName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Name] AS (([FirstName]+' ')+[LastName]),
[home_phone] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[business_phone] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[mobile] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[mobile2] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[business_phone2] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[builders_phone] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[postal_address] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[builders_name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[designer_id] [numeric] (18, 0) NOT NULL,
[address] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[suburb] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[postcode] [nvarchar] (4) COLLATE Latin1_General_CI_AS NOT NULL,
[email] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[email2] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[fax] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[account_type_id] [int] NOT NULL,
[account_status_id] [int] NOT NULL,
[has_been_revived] [bit] NULL,
[next_review_date] [datetime] NULL,
[contact_source_id] [int] NOT NULL,
[opportunity_source_id] [int] NOT NULL,
[opportunity_details] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[revived_reason] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[left_reason] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[date_created] [datetime] NOT NULL,
[created_by_id] [numeric] (18, 0) NULL,
[sale_date] [datetime] NULL,
[weeks_open] AS (datediff(week,[date_created],getdate())),
[door_type] [int] NULL,
[benchtop_type] [int] NULL,
[price_range] [money] NULL,
[time_frame] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[date_died] [datetime] NULL,
[home_warranty_policy] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[home_warranty_date] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[account] ADD 
CONSTRAINT [PK_account] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_employee] FOREIGN KEY ([designer_id]) REFERENCES [dbo].[employee] ([id])
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_accountType] FOREIGN KEY ([account_type_id]) REFERENCES [dbo].[accountType] ([id])
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_accountStatus] FOREIGN KEY ([account_status_id]) REFERENCES [dbo].[accountStatus] ([id])
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_advertisingSource] FOREIGN KEY ([contact_source_id]) REFERENCES [dbo].[contactSource] ([id])
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_opportunitySource] FOREIGN KEY ([opportunity_source_id]) REFERENCES [dbo].[opportunitySource] ([id])
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_employee1] FOREIGN KEY ([created_by_id]) REFERENCES [dbo].[employee] ([id])
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_AccountDoorTypes] FOREIGN KEY ([door_type]) REFERENCES [dbo].[AccountDoorType] ([AccountDoorTypeId])
ALTER TABLE [dbo].[account] ADD
CONSTRAINT [FK_account_AccountBenchtopType] FOREIGN KEY ([benchtop_type]) REFERENCES [dbo].[AccountBenchtopType] ([AccountBenchtopTypeId])



GO
