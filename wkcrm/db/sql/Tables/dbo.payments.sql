CREATE TABLE [dbo].[payments]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[payment_schedule_id] [numeric] (18, 0) NOT NULL,
[payment_stages_id] [int] NOT NULL,
[payment_type_id] [int] NOT NULL,
[datetime] [datetime] NOT NULL,
[employee_id] [numeric] (18, 0) NOT NULL,
[amount] [money] NOT NULL,
[notes] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[payments] ADD CONSTRAINT [PK_payments] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[payments] ADD CONSTRAINT [FK_payments_employee] FOREIGN KEY ([employee_id]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[payments] ADD CONSTRAINT [FK_payments_paymentSchedule] FOREIGN KEY ([payment_schedule_id]) REFERENCES [dbo].[paymentSchedule] ([id])
GO
ALTER TABLE [dbo].[payments] ADD CONSTRAINT [FK_payments_paymentStages] FOREIGN KEY ([payment_stages_id]) REFERENCES [dbo].[paymentStages] ([id])
GO
ALTER TABLE [dbo].[payments] ADD CONSTRAINT [FK_payments_paymentTypes] FOREIGN KEY ([payment_type_id]) REFERENCES [dbo].[paymentTypes] ([id])
GO
