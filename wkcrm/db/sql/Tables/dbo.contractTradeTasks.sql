CREATE TABLE [dbo].[contractTradeTasks]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[contract_trade_type] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[price] [money] NOT NULL,
[part_of_standard_price] [bit] NOT NULL,
[contract_payment_type_id] [int] NOT NULL,
[notes] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[IsDefaultItem] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractTradeTasks] ADD CONSTRAINT [PK_tradeElectricity] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractTradeTasks] ADD CONSTRAINT [FK_contractTradeTasks_contractTrades] FOREIGN KEY ([contract_trade_type]) REFERENCES [dbo].[contractTrades] ([id])
GO
ALTER TABLE [dbo].[contractTradeTasks] ADD CONSTRAINT [FK_tradeTasks_contractPaymentType] FOREIGN KEY ([contract_payment_type_id]) REFERENCES [dbo].[contractPaymentType] ([int])
GO
