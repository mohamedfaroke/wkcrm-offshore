CREATE TABLE [dbo].[contractTradeItem]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[contract_id] [numeric] (18, 0) NOT NULL,
[trade_task_id] [int] NOT NULL,
[units] [numeric] (18, 2) NOT NULL,
[price] [money] NOT NULL,
[amount] AS ([units]*[price]),
[notes] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[contractTradeItem] ADD
CONSTRAINT [FK_contractTradeItem_contractMain] FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contractMain] ([id])
GO
ALTER TABLE [dbo].[contractTradeItem] ADD CONSTRAINT [PK_contractTradeItem] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[contractTradeItem] ADD CONSTRAINT [FK_contractTradeItem_contractTradeTasks] FOREIGN KEY ([trade_task_id]) REFERENCES [dbo].[contractTradeTasks] ([id])
GO
