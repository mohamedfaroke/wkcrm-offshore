CREATE TABLE [dbo].[location]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[address] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[phone] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[location] ADD CONSTRAINT [PK_location] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
