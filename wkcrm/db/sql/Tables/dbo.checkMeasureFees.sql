CREATE TABLE [dbo].[checkMeasureFees]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[lower_limit] [decimal] (18, 2) NOT NULL,
[upper_limit] [decimal] (18, 2) NULL,
[checkmeasure_fee] [decimal] (18, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[checkMeasureFees] ADD CONSTRAINT [PK_checkMeasureFees] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
