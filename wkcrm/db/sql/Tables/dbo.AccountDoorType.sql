CREATE TABLE [dbo].[AccountDoorType]
(
[AccountDoorTypeId] [int] NOT NULL,
[AccountDoorType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccountDoorType] ADD CONSTRAINT [PK_AccountDoorType] PRIMARY KEY CLUSTERED  ([AccountDoorTypeId]) ON [PRIMARY]
GO
