CREATE TABLE [dbo].[contractDoorTypes]
(
[id] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoorTypes] ADD CONSTRAINT [PK_doorTypes] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
