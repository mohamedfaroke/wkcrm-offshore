CREATE TABLE [dbo].[contractDoorFaces]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[door_type_id] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[contractDoorFaces] WITH NOCHECK ADD
CONSTRAINT [FK_doorFaces_doorTypes] FOREIGN KEY ([door_type_id]) REFERENCES [dbo].[contractDoorTypes] ([id])
GO
ALTER TABLE [dbo].[contractDoorFaces] ADD CONSTRAINT [PK_doorFaces] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
