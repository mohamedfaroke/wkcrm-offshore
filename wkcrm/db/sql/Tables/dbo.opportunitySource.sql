CREATE TABLE [dbo].[opportunitySource]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[opportunity_category_id] [int] NOT NULL,
[display_order] [int] NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[opportunitySource] ADD CONSTRAINT [PK_opportunitySource] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[opportunitySource] ADD CONSTRAINT [FK_opportunitySource_opportunityCategory] FOREIGN KEY ([opportunity_category_id]) REFERENCES [dbo].[opportunityCategory] ([id])
GO
