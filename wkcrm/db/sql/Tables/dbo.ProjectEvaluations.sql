CREATE TABLE [dbo].[ProjectEvaluations]
(
[ProjectEvaluationId] [int] NOT NULL IDENTITY(1, 1),
[project_id] [numeric] (18, 0) NOT NULL,
[EvaluationId] [int] NOT NULL,
[EvaluationDate] [datetime] NULL,
[CreationDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectEvaluations] ADD CONSTRAINT [PK_ProjectEvaluations] PRIMARY KEY CLUSTERED  ([ProjectEvaluationId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectEvaluations] ADD CONSTRAINT [FK_ProjectEvaluations_Evaluations] FOREIGN KEY ([EvaluationId]) REFERENCES [dbo].[Evaluations] ([EvaluationID])
GO
ALTER TABLE [dbo].[ProjectEvaluations] ADD CONSTRAINT [FK_ProjectEvaluations_project] FOREIGN KEY ([project_id]) REFERENCES [dbo].[project] ([id])
GO
