CREATE TABLE [dbo].[contractMain]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(5000, 1),
[account_id] [numeric] (18, 0) NOT NULL,
[date_created] [datetime] NOT NULL,
[created_by] [numeric] (18, 0) NOT NULL,
[date_modified] [datetime] NOT NULL,
[modified_by] [numeric] (18, 0) NOT NULL,
[signed_date] [datetime] NULL,
[original_total] [money] NULL,
[check_measure_fee] [money] NOT NULL,
[second_check_measure_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_second_check_measure_fee] DEFAULT ((0)),
[bench_threshold_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_bench_threshold_fee] DEFAULT ((0)),
[door_threshold_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_door_threshold_fee] DEFAULT ((0)),
[difficult_delivery_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_difficult_delivery_fee] DEFAULT ((0)),
[non_metro_delivery_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_non_metro_delivery_fee] DEFAULT ((0)),
[home_insurance_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_home_insurance_fee] DEFAULT ((0)),
[dual_finishes_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_dual_finishes_fee] DEFAULT ((0)),
[dual_finishes_lessthan_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_dual_finishes_lessthan_fee] DEFAULT ((0)),
[highrise_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_highrise_fee] DEFAULT ((0)),
[kickboardsAfterTimber_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_kickboardsAfterTimber_fee] DEFAULT ((0)),
[kickboardsWasherAfterTimber_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_kickboardsWasherAfterTimber_fee] DEFAULT ((0)),
[contract_type_id] [int] NOT NULL,
[estimated_completion_date] [datetime] NULL,
[home_insurance_sent] [bit] NULL,
[home_insurance_policy] [nchar] (50) COLLATE Latin1_General_CI_AS NULL,
[designer_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_designer_fee] DEFAULT ((0)),
[consultation_fee] [money] NOT NULL CONSTRAINT [DF_contractMain_consultation_fee] DEFAULT ((0)),
[height_ceiling] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractMain_height_ceiling] DEFAULT ((0)),
[height_cabinet] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractMain_height_cabinet] DEFAULT ((0)),
[height_bench] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractMain_height_bench] DEFAULT ((0)),
[height_splashback] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractMain_height_splashback] DEFAULT ((0)),
[height_wallcabinet] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractMain_height_wallcabinet] DEFAULT ((0)),
[height_microwave] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractMain_height_microwave] DEFAULT ((0)),
[height_walloven] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractMain_height_walloven] DEFAULT ((0)),
[status_id] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[contractMain] ADD
CONSTRAINT [FK_contractMain_account] FOREIGN KEY ([account_id]) REFERENCES [dbo].[account] ([id])
ALTER TABLE [dbo].[contractMain] ADD 
CONSTRAINT [PK_contractMain] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]

ALTER TABLE [dbo].[contractMain] ADD
CONSTRAINT [FK_contractMain_employee] FOREIGN KEY ([created_by]) REFERENCES [dbo].[employee] ([id])
ALTER TABLE [dbo].[contractMain] ADD
CONSTRAINT [FK_contractMain_employee1] FOREIGN KEY ([modified_by]) REFERENCES [dbo].[employee] ([id])
ALTER TABLE [dbo].[contractMain] ADD
CONSTRAINT [FK_contractMain_contractType] FOREIGN KEY ([contract_type_id]) REFERENCES [dbo].[contractType] ([id])
GO
