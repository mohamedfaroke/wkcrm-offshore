CREATE TABLE [dbo].[paymentStages]
(
[id] [int] NOT NULL IDENTITY(1100, 5),
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[percentage] [numeric] (18, 2) NULL,
[threshold] [money] NULL,
[alt_percentage] [numeric] (18, 2) NULL,
[status_id] [bit] NOT NULL,
[displayOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paymentStages] ADD CONSTRAINT [PK_paymentStages] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
