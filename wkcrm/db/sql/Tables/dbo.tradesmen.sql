CREATE TABLE [dbo].[tradesmen]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[TradesmanTypeId] [int] NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ABN] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[dob] [datetime] NULL,
[phone] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[mobile] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[fax] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[address] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[email] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[send_email_notifications] [bit] NOT NULL,
[entity_type_id] [int] NOT NULL,
[public_liability_company] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[public_liability_number] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[public_liability_expiry_date] [datetime] NULL,
[workers_comp_company] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[workers_comp_number] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[workers_comp_expiry_date] [datetime] NULL,
[trade_licence_number] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[trade_licence_expiry_date] [datetime] NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[tradesmen] ADD
CONSTRAINT [FK_tradesmen_TradesmanType] FOREIGN KEY ([TradesmanTypeId]) REFERENCES [dbo].[TradesmanType] ([TradesmanTypeId])
GO
ALTER TABLE [dbo].[tradesmen] ADD CONSTRAINT [PK_tradesmen] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tradesmen] ADD CONSTRAINT [FK_tradesmen_tradeEntityTypes] FOREIGN KEY ([entity_type_id]) REFERENCES [dbo].[tradeEntityTypes] ([id])
GO
