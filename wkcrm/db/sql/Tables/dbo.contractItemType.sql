CREATE TABLE [dbo].[contractItemType]
(
[id] [int] NOT NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL,
[order_by] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractItemType] ADD CONSTRAINT [PK_contractItemType] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
