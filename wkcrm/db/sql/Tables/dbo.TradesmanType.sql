CREATE TABLE [dbo].[TradesmanType]
(
[TradesmanTypeId] [int] NOT NULL,
[TradesmanType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_TradesmanType_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TradesmanType] ADD CONSTRAINT [PK_TradesmanType] PRIMARY KEY CLUSTERED  ([TradesmanTypeId]) ON [PRIMARY]
GO
