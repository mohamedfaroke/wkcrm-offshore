CREATE TABLE [dbo].[communication]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[account_id] [numeric] (18, 0) NOT NULL,
[datetime] [datetime] NOT NULL,
[employee_id] [numeric] (18, 0) NOT NULL,
[notes] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[communication] ADD
CONSTRAINT [FK_communication_account] FOREIGN KEY ([account_id]) REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[communication] ADD CONSTRAINT [PK_communication] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[communication] ADD CONSTRAINT [FK_communication_employee] FOREIGN KEY ([employee_id]) REFERENCES [dbo].[employee] ([id])
GO
