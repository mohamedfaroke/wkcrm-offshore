CREATE TABLE [dbo].[paymentVariations]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[employee_id] [numeric] (18, 0) NOT NULL,
[datetime] [datetime] NOT NULL,
[amount] [money] NOT NULL,
[payment_stage_id] [int] NOT NULL,
[payment_schedule_id] [numeric] (18, 0) NOT NULL,
[notes] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paymentVariations] ADD CONSTRAINT [PK_paymentVariations] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paymentVariations] ADD CONSTRAINT [FK_paymentVariations_employee] FOREIGN KEY ([employee_id]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[paymentVariations] ADD CONSTRAINT [FK_paymentVariations_paymentSchedule] FOREIGN KEY ([payment_schedule_id]) REFERENCES [dbo].[paymentSchedule] ([id])
GO
ALTER TABLE [dbo].[paymentVariations] ADD CONSTRAINT [FK_paymentVariations_paymentStages] FOREIGN KEY ([payment_stage_id]) REFERENCES [dbo].[paymentStages] ([id])
GO
