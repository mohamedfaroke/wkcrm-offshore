CREATE TABLE [dbo].[contractBenchtopOptionItem]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[contract_benchtop_item] [numeric] (18, 0) NOT NULL,
[contract_benchtop_option_item] [numeric] (18, 0) NOT NULL,
[units] [int] NOT NULL,
[price] [money] NOT NULL,
[amount] AS ([units]*[price])
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtopOptionItem] ADD CONSTRAINT [PK_contractBenchtopOptionItem] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtopOptionItem] ADD CONSTRAINT [FK_contractBenchtopOptionItem_contractBenchtopItem] FOREIGN KEY ([contract_benchtop_item]) REFERENCES [dbo].[contractBenchtopItem] ([id])
GO
ALTER TABLE [dbo].[contractBenchtopOptionItem] ADD CONSTRAINT [FK_contractBenchtopOptionItem_contractBenchtopOptions] FOREIGN KEY ([contract_benchtop_option_item]) REFERENCES [dbo].[contractBenchtopOptions] ([id])
GO
