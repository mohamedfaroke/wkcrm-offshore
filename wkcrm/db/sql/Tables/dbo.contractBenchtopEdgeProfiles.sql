CREATE TABLE [dbo].[contractBenchtopEdgeProfiles]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[benchtop_id] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtopEdgeProfiles] ADD CONSTRAINT [PK_contractBenchtopEdgeProfiles] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtopEdgeProfiles] ADD CONSTRAINT [FK_contractBenchtopEdgeProfiles_contractBenchtops] FOREIGN KEY ([benchtop_id]) REFERENCES [dbo].[contractBenchtops] ([id])
GO
