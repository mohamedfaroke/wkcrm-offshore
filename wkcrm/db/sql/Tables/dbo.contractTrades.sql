CREATE TABLE [dbo].[contractTrades]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[default_amount] [money] NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractTrades] ADD CONSTRAINT [PK_contractTradeAmounts] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
