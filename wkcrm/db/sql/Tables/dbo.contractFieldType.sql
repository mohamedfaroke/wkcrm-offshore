CREATE TABLE [dbo].[contractFieldType]
(
[id] [int] NOT NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractFieldType] ADD CONSTRAINT [PK_fieldType] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
