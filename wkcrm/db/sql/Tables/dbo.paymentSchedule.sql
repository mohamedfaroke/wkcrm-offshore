CREATE TABLE [dbo].[paymentSchedule]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[contract_id] [numeric] (18, 0) NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[paymentSchedule] ADD
CONSTRAINT [FK_paymentSchedule_contractMain] FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contractMain] ([id])
GO
ALTER TABLE [dbo].[paymentSchedule] ADD CONSTRAINT [PK_paymentSchedule] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
