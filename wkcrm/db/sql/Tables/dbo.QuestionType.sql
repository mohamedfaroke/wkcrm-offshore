CREATE TABLE [dbo].[QuestionType]
(
[typeId] [int] NOT NULL IDENTITY(1, 1),
[Type] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QuestionType] ADD CONSTRAINT [PK_QuestionType] PRIMARY KEY CLUSTERED  ([typeId]) ON [PRIMARY]
GO
