CREATE TABLE [dbo].[contractOthers]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[contract_item_type] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[price] [money] NOT NULL,
[notes] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[contract_payment_type] [int] NOT NULL,
[category] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[status_id] [bit] NOT NULL CONSTRAINT [DF_contractOthers_status_id] DEFAULT ((1)),
[supplier_code] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[sub_category] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractOthers] ADD CONSTRAINT [PK_contractOthers] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractOthers] ADD CONSTRAINT [FK_contractOthers_contractItemType] FOREIGN KEY ([contract_item_type]) REFERENCES [dbo].[contractItemType] ([id])
GO
ALTER TABLE [dbo].[contractOthers] ADD CONSTRAINT [FK_contractOthers_contractPaymentType] FOREIGN KEY ([contract_payment_type]) REFERENCES [dbo].[contractPaymentType] ([int])
GO
