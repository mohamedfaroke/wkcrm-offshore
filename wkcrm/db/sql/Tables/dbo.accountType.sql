CREATE TABLE [dbo].[accountType]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[accountType] ADD CONSTRAINT [PK_accountType] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
