CREATE TABLE [dbo].[Evaluations]
(
[EvaluationID] [int] NOT NULL IDENTITY(1, 1),
[EvaluationName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Evaluations] ADD CONSTRAINT [PK_Evaluations] PRIMARY KEY CLUSTERED  ([EvaluationID]) ON [PRIMARY]
GO
