CREATE TABLE [dbo].[ProjectEvaluationAnswers]
(
[ProjectEvaluationId] [int] NOT NULL,
[QuestionId] [int] NOT NULL,
[OptionId] [int] NULL,
[Answer] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[designerId] [numeric] (18, 0) NULL,
[tradesmanId] [numeric] (18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectEvaluationAnswers] ADD CONSTRAINT [PK_EventEvaluationAnswers] PRIMARY KEY CLUSTERED  ([ProjectEvaluationId], [QuestionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectEvaluationAnswers] ADD CONSTRAINT [FK_ProjectEvaluationAnswers_employee] FOREIGN KEY ([designerId]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[ProjectEvaluationAnswers] ADD CONSTRAINT [FK_ProjectEvaluationAnswers_EvaluationQuestion] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[EvaluationQuestion] ([QuestionID])
GO
ALTER TABLE [dbo].[ProjectEvaluationAnswers] ADD CONSTRAINT [FK_ProjectEvaluationAnswers_ProjectEvaluations] FOREIGN KEY ([ProjectEvaluationId]) REFERENCES [dbo].[ProjectEvaluations] ([ProjectEvaluationId])
GO
ALTER TABLE [dbo].[ProjectEvaluationAnswers] ADD CONSTRAINT [FK_ProjectEvaluationAnswers_QuestionOption] FOREIGN KEY ([OptionId]) REFERENCES [dbo].[QuestionOption] ([OptionID])
GO
ALTER TABLE [dbo].[ProjectEvaluationAnswers] ADD CONSTRAINT [FK_ProjectEvaluationAnswers_tradesmen] FOREIGN KEY ([tradesmanId]) REFERENCES [dbo].[tradesmen] ([id])
GO
