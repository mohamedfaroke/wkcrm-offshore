CREATE TABLE [dbo].[CrmOptions]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[doorQtyThreshold] [decimal] (18, 2) NOT NULL,
[doorQtyExtraCost] [money] NOT NULL,
[difficultDeliveryFee] [money] NOT NULL,
[secondCheckMeasureFee] [money] NOT NULL,
[GST] [decimal] (18, 2) NOT NULL,
[insuranceThreshold] [money] NOT NULL,
[insuranceFee] [money] NOT NULL,
[benchtopQtyThreshold] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_CrmOptions_benchtopQtyThreshold] DEFAULT ((0)),
[benchtopQtyExtraCost] [money] NOT NULL CONSTRAINT [DF_CrmOptions_benchtopQtyExtraCost] DEFAULT ((0)),
[dualFinishes] [money] NOT NULL CONSTRAINT [DF_CrmOptions_dualFinishes] DEFAULT ((0)),
[dualFinishLessFee] [money] NOT NULL CONSTRAINT [DF_CrmOptions_dualFinishLessFee] DEFAULT ((0)),
[dualFinishLessFeeThreshold] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_CrmOptions_dualFinishLessFeeThreshold] DEFAULT ((0)),
[highRise] [money] NOT NULL CONSTRAINT [DF_CrmOptions_highRise] DEFAULT ((0)),
[kickboardsAfterTimber] [money] NOT NULL CONSTRAINT [DF_CrmOptions_kickboardsAfterTimber] DEFAULT ((0)),
[kickboardsWasherAfterTimber] [money] NOT NULL CONSTRAINT [DF_CrmOptions_kickboardsWasherAfterTimber] DEFAULT ((0)),
[completionThreshold] [money] NOT NULL CONSTRAINT [DF_CrmOptions_completionThreshold] DEFAULT ((0))
) ON [PRIMARY]
ALTER TABLE [dbo].[CrmOptions] ADD 
CONSTRAINT [PK_CrmOptions] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]

GO
