CREATE TABLE [dbo].[tasks]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[project_id] [numeric] (18, 0) NOT NULL,
[tradesman_id] [numeric] (18, 0) NOT NULL,
[task_type_id] [int] NOT NULL,
[notes] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[datetime] [datetime] NOT NULL,
[datetime_end] [datetime] NULL,
[acknowledged] [bit] NOT NULL CONSTRAINT [DF_tasks_acknowledged] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tasks] ADD CONSTRAINT [PK_tasks] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tasks] ADD CONSTRAINT [FK_tasks_project] FOREIGN KEY ([project_id]) REFERENCES [dbo].[project] ([id])
GO
ALTER TABLE [dbo].[tasks] ADD CONSTRAINT [FK_tasks_taskTypes] FOREIGN KEY ([task_type_id]) REFERENCES [dbo].[tradeTaskTypes] ([id])
GO
ALTER TABLE [dbo].[tasks] ADD CONSTRAINT [FK_tasks_tradesmen] FOREIGN KEY ([tradesman_id]) REFERENCES [dbo].[tradesmen] ([id])
GO
