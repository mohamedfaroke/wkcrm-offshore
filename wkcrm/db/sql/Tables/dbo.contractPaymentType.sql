CREATE TABLE [dbo].[contractPaymentType]
(
[int] [int] NOT NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractPaymentType] ADD CONSTRAINT [PK_contractPaymentType] PRIMARY KEY CLUSTERED  ([int]) ON [PRIMARY]
GO
