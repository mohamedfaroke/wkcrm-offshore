CREATE TABLE [dbo].[contractDoorItem]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[contract_id] [numeric] (18, 0) NOT NULL,
[item_id] [int] NOT NULL,
[unit_price] [money] NOT NULL,
[additional_cost] [money] NOT NULL CONSTRAINT [DF_contractDoorItem_additional_cost] DEFAULT ((0)),
[units] [decimal] (18, 2) NOT NULL,
[amount] AS ([unit_price]*[units]+[additional_cost]),
[style] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[edge_profile] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[face] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[finish] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[colour] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[veneer] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[supplier] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[notes] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[contractDoorItem] ADD
CONSTRAINT [FK_contractDoorItem_contractMain] FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contractMain] ([id])
GO
ALTER TABLE [dbo].[contractDoorItem] ADD CONSTRAINT [PK_contractDoorItem] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoorItem] ADD CONSTRAINT [FK_contractDoorItem_contractDoors] FOREIGN KEY ([item_id]) REFERENCES [dbo].[contractDoors] ([id])
GO
