CREATE TABLE [dbo].[contractDoorStyles]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[door_type_id] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoorStyles] ADD CONSTRAINT [PK_doorStyles] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoorStyles] ADD CONSTRAINT [FK_doorStyles_doorTypes] FOREIGN KEY ([door_type_id]) REFERENCES [dbo].[contractDoorTypes] ([id])
GO
