CREATE TABLE [dbo].[AccountBenchtopType]
(
[AccountBenchtopTypeId] [int] NOT NULL,
[AccountBenchtopType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccountBenchtopType] ADD CONSTRAINT [PK_AccountBenchtopType] PRIMARY KEY CLUSTERED  ([AccountBenchtopTypeId]) ON [PRIMARY]
GO
