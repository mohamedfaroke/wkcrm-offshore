CREATE TABLE [dbo].[tradeEntityTypes]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[enforce_legal_fields] [bit] NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tradeEntityTypes] ADD CONSTRAINT [PK_tradeEntityTypes] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
