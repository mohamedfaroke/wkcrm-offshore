CREATE TABLE [dbo].[EvaluationQuestionOptions]
(
[QuestionId] [int] NOT NULL,
[OptionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EvaluationQuestionOptions] ADD CONSTRAINT [PK_EvaluationQuestionOptions] PRIMARY KEY CLUSTERED  ([QuestionId], [OptionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EvaluationQuestionOptions] ADD CONSTRAINT [FK_EvaluationQuestionOptions_EvaluationQuestion] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[EvaluationQuestion] ([QuestionID])
GO
ALTER TABLE [dbo].[EvaluationQuestionOptions] ADD CONSTRAINT [FK_EvaluationQuestionOptions_QuestionOption] FOREIGN KEY ([OptionId]) REFERENCES [dbo].[QuestionOption] ([OptionID])
GO
