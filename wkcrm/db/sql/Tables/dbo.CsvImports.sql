CREATE TABLE [dbo].[CsvImports]
(
[CSVImportId] [int] NOT NULL IDENTITY(1, 1),
[CreationDateTime] [datetime] NOT NULL CONSTRAINT [DF_CsvImports_CreationDateTime] DEFAULT (getdate()),
[EmployeeId] [int] NOT NULL,
[ImportFileContents] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CsvImports] ADD CONSTRAINT [PK_CsvImports] PRIMARY KEY CLUSTERED  ([CSVImportId]) ON [PRIMARY]
GO
