CREATE TABLE [dbo].[contractBenchtopOptions]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[name] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[contract_payment_type] [int] NOT NULL,
[price] [money] NOT NULL,
[status_id] [nchar] (10) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtopOptions] ADD CONSTRAINT [PK_contractBenchtopOptions] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtopOptions] ADD CONSTRAINT [FK_contractBenchtopOptions_contractPaymentType] FOREIGN KEY ([contract_payment_type]) REFERENCES [dbo].[contractPaymentType] ([int])
GO
