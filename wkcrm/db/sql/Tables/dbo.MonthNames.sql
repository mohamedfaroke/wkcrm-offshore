CREATE TABLE [dbo].[MonthNames]
(
[monthNum] [int] NOT NULL,
[monthName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[monthShortName] [nchar] (3) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonthNames] ADD CONSTRAINT [PK_MonthNames] PRIMARY KEY CLUSTERED  ([monthNum]) ON [PRIMARY]
GO
