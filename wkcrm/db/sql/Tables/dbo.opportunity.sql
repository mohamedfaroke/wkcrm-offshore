CREATE TABLE [dbo].[opportunity]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[employee_id] [numeric] (18, 0) NOT NULL,
[datetime] [datetime] NOT NULL,
[location_id] [int] NOT NULL,
[contact_source_id] [int] NULL,
[opportunity_source_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[opportunity] ADD CONSTRAINT [PK_opportunity] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[opportunity] ADD CONSTRAINT [FK_opportunity_advertisingSource] FOREIGN KEY ([contact_source_id]) REFERENCES [dbo].[contactSource] ([id])
GO
ALTER TABLE [dbo].[opportunity] ADD CONSTRAINT [FK_opportunity_employee] FOREIGN KEY ([employee_id]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[opportunity] ADD CONSTRAINT [FK_opportunity_location] FOREIGN KEY ([location_id]) REFERENCES [dbo].[location] ([id])
GO
ALTER TABLE [dbo].[opportunity] ADD CONSTRAINT [FK_opportunity_opportunitySource] FOREIGN KEY ([opportunity_source_id]) REFERENCES [dbo].[opportunitySource] ([id])
GO
