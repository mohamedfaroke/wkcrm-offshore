CREATE TABLE [dbo].[contractDoorEdgeProfiles]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[door_type_id] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoorEdgeProfiles] ADD CONSTRAINT [PK_doorEdgeProfiles] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoorEdgeProfiles] ADD CONSTRAINT [FK_doorEdgeProfiles_doorTypes] FOREIGN KEY ([door_type_id]) REFERENCES [dbo].[contractDoorTypes] ([id])
GO
