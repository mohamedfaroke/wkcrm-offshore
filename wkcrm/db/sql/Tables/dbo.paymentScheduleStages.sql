CREATE TABLE [dbo].[paymentScheduleStages]
(
[payment_schedule_id] [numeric] (18, 0) NOT NULL,
[payment_stage_id] [int] NOT NULL,
[amount] [money] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paymentScheduleStages] ADD CONSTRAINT [PK_paymentScheduleStages] PRIMARY KEY CLUSTERED  ([payment_schedule_id], [payment_stage_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paymentScheduleStages] ADD CONSTRAINT [FK_paymentScheduleStages_paymentSchedule] FOREIGN KEY ([payment_schedule_id]) REFERENCES [dbo].[paymentSchedule] ([id])
GO
ALTER TABLE [dbo].[paymentScheduleStages] ADD CONSTRAINT [FK_paymentScheduleStages_paymentStages] FOREIGN KEY ([payment_stage_id]) REFERENCES [dbo].[paymentStages] ([id])
GO
