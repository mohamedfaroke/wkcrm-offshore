CREATE TABLE [dbo].[taskNotification]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[reference_id] [numeric] (18, 0) NULL,
[taskNotificationType] [int] NOT NULL,
[date_created] [datetime] NOT NULL,
[TaskDescription] [text] COLLATE Latin1_General_CI_AS NOT NULL,
[employee_id] [numeric] (18, 0) NOT NULL,
[TaskLink] [text] COLLATE Latin1_General_CI_AS NULL,
[date_viewed] [datetime] NULL,
[date_completed] [datetime] NULL,
[completed] [bit] NOT NULL CONSTRAINT [DF_taskNotification_completed] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[taskNotification] ADD CONSTRAINT [PK_taskNotification] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[taskNotification] ADD CONSTRAINT [FK_taskNotification_employee] FOREIGN KEY ([employee_id]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[taskNotification] ADD CONSTRAINT [FK_taskNotification_taskNotificationType] FOREIGN KEY ([taskNotificationType]) REFERENCES [dbo].[taskNotificationType] ([id])
GO
