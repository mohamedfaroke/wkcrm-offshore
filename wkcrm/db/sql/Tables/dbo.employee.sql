CREATE TABLE [dbo].[employee]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[location_id] [int] NOT NULL,
[dob] [datetime] NULL,
[phone] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[mobile] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[address] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[email] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[username] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[password] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[status_id] [bit] NOT NULL CONSTRAINT [DF_employee_status_id] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[employee] ADD CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[employee] ADD CONSTRAINT [FK_employee_location] FOREIGN KEY ([location_id]) REFERENCES [dbo].[location] ([id])
GO
