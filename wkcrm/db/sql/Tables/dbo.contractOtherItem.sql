CREATE TABLE [dbo].[contractOtherItem]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[contract_id] [numeric] (18, 0) NOT NULL,
[item_id] [int] NOT NULL,
[supplier] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[unit_price] [money] NOT NULL,
[units] [decimal] (18, 2) NOT NULL,
[markup] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractOtherItem_markup] DEFAULT ((0)),
[amount] AS (([unit_price]*[units])*((1)+[markup])),
[notes] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[contractOtherItem] ADD
CONSTRAINT [FK_contractOtherItem_contractMain] FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contractMain] ([id])
GO
ALTER TABLE [dbo].[contractOtherItem] ADD CONSTRAINT [PK_contractOtherItem] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[contractOtherItem] ADD CONSTRAINT [FK_contractOtherItem_contractOthers] FOREIGN KEY ([item_id]) REFERENCES [dbo].[contractOthers] ([id])
GO
