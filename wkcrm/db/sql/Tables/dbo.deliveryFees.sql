CREATE TABLE [dbo].[deliveryFees]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[lower_limit] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_deliveryFees_lower_limit] DEFAULT ((0)),
[upper_limit] [decimal] (18, 2) NULL,
[delivery_fee] [decimal] (18, 2) NOT NULL,
[sortOrder] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[deliveryFees] ADD 
CONSTRAINT [PK_deliveryFees] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
