CREATE TABLE [dbo].[contractDoors]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[name] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[door_type_id] [int] NOT NULL,
[style] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[edge_profile] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[face] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[finish] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[colour] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[unit_price] [money] NOT NULL,
[style_type] [int] NOT NULL,
[edge_type] [int] NOT NULL,
[face_type] [int] NOT NULL,
[finish_type] [int] NOT NULL,
[colour_type] [int] NOT NULL,
[show_extra_fields] [bit] NOT NULL,
[notes] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[status_id] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [PK_doors] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [FK_doors_doorTypes] FOREIGN KEY ([door_type_id]) REFERENCES [dbo].[contractDoorTypes] ([id])
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [FK_doors_fieldType] FOREIGN KEY ([style_type]) REFERENCES [dbo].[contractFieldType] ([id])
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [FK_doors_fieldType1] FOREIGN KEY ([edge_type]) REFERENCES [dbo].[contractFieldType] ([id])
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [FK_doors_fieldType2] FOREIGN KEY ([face_type]) REFERENCES [dbo].[contractFieldType] ([id])
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [FK_doors_fieldType3] FOREIGN KEY ([finish_type]) REFERENCES [dbo].[contractFieldType] ([id])
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [FK_doors_fieldType4] FOREIGN KEY ([finish_type]) REFERENCES [dbo].[contractFieldType] ([id])
GO
ALTER TABLE [dbo].[contractDoors] ADD CONSTRAINT [FK_doors_fieldType5] FOREIGN KEY ([colour_type]) REFERENCES [dbo].[contractFieldType] ([id])
GO
