CREATE TABLE [dbo].[company]
(
[id] [int] NOT NULL,
[Name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Address] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[phone] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[fax] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[email] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[logo] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[abn] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[company] ADD CONSTRAINT [PK_company] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
