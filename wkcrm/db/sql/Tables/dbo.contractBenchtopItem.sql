CREATE TABLE [dbo].[contractBenchtopItem]
(
[id] [numeric] (18, 0) NOT NULL IDENTITY(1000, 5),
[contract_id] [numeric] (18, 0) NOT NULL,
[item_id] [int] NOT NULL,
[supplier] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[unit_price] [money] NOT NULL,
[units] [decimal] (18, 2) NOT NULL,
[markup] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_contractBenchtopItem_markup] DEFAULT ((0)),
[amount] AS (([units]*[unit_price])*((1)+[markup])+[options_amount]),
[options_amount] [money] NOT NULL,
[edge_profile] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[colour] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[finish] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[edge_thickness] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[notes] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[contractBenchtopItem] ADD
CONSTRAINT [FK_contractBenchtopItem_contractMain] FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contractMain] ([id])
GO
ALTER TABLE [dbo].[contractBenchtopItem] ADD CONSTRAINT [PK_contractBenchtopItem_1] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[contractBenchtopItem] ADD CONSTRAINT [FK_contractBenchtopItem_contractBenchtops] FOREIGN KEY ([item_id]) REFERENCES [dbo].[contractBenchtops] ([id])
GO
