CREATE TABLE [dbo].[employeeRoles]
(
[id] [int] NOT NULL IDENTITY(1000, 5),
[employee_id] [numeric] (18, 0) NOT NULL,
[role_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[employeeRoles] ADD CONSTRAINT [PK_employeeRoles] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[employeeRoles] ADD CONSTRAINT [FK_employeeRoles_employee] FOREIGN KEY ([employee_id]) REFERENCES [dbo].[employee] ([id])
GO
ALTER TABLE [dbo].[employeeRoles] ADD CONSTRAINT [FK_employeeRoles_role] FOREIGN KEY ([role_id]) REFERENCES [dbo].[role] ([id])
GO
