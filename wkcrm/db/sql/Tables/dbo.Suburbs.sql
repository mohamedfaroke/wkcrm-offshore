CREATE TABLE [dbo].[Suburbs]
(
[SuburbId] [int] NOT NULL,
[Suburb] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Postcode] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Suburbs] ADD CONSTRAINT [PK_Suburbs] PRIMARY KEY CLUSTERED  ([SuburbId]) ON [PRIMARY]
GO
