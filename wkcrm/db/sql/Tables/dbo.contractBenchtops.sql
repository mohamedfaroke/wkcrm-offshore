CREATE TABLE [dbo].[contractBenchtops]
(
[id] [int] NOT NULL IDENTITY(1085, 5),
[contract_payment_type_id] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[unit_price] [money] NOT NULL,
[status_id] [bit] NOT NULL,
[order_by] [int] NOT NULL,
[BenchtopThresholdApplies] [bit] NOT NULL CONSTRAINT [DF_contractBenchtops_BenchtopThresholdApplies] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtops] ADD CONSTRAINT [PK_contractBenchtopItem] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contractBenchtops] ADD CONSTRAINT [FK_contractBenchtops_contractPaymentType] FOREIGN KEY ([contract_payment_type_id]) REFERENCES [dbo].[contractPaymentType] ([int])
GO
