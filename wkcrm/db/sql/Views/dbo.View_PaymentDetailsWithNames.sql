SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.View_PaymentDetailsWithNames
AS
SELECT     dbo.View_PaymentScheduleOverview.payment_schedule_id, dbo.View_PaymentScheduleOverview.payment_stage_id, 
                      dbo.View_PaymentScheduleOverview.TotalAmount, dbo.View_PaymentScheduleOverview.TotalReceived, 
                      dbo.View_PaymentScheduleOverview.Outstanding, dbo.payments.id AS PaymentsID, dbo.payments.payment_type_id, dbo.payments.datetime, 
                      dbo.payments.employee_id, dbo.payments.amount, dbo.payments.notes, dbo.paymentTypes.name AS PaymentType, 
                      dbo.paymentStages.name AS PaymentStage, dbo.employee.name AS EmployeeName, dbo.View_AccountContractProject.AccID, 
                      dbo.account.Name AS AccountName, dbo.View_AccountContractProject.ContractID, dbo.View_AccountContractProject.AccID AS id
FROM         dbo.paymentStages RIGHT OUTER JOIN
                      dbo.View_PaymentScheduleOverview ON dbo.paymentStages.id = dbo.View_PaymentScheduleOverview.payment_stage_id LEFT OUTER JOIN
                      dbo.employee INNER JOIN
                      dbo.payments INNER JOIN
                      dbo.paymentTypes ON dbo.payments.payment_type_id = dbo.paymentTypes.id ON dbo.employee.id = dbo.payments.employee_id ON 
                      dbo.View_PaymentScheduleOverview.payment_schedule_id = dbo.payments.payment_schedule_id AND 
                      dbo.View_PaymentScheduleOverview.payment_stage_id = dbo.payments.payment_stages_id LEFT OUTER JOIN
                      dbo.account INNER JOIN
                      dbo.View_AccountContractProject ON dbo.account.id = dbo.View_AccountContractProject.AccID ON 
                      dbo.View_PaymentScheduleOverview.payment_schedule_id = dbo.View_AccountContractProject.PaymentScheduleID
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "paymentStages"
            Begin Extent = 
               Top = 192
               Left = 20
               Bottom = 307
               Right = 174
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_PaymentScheduleOverview"
            Begin Extent = 
               Top = 194
               Left = 255
               Bottom = 309
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "employee"
            Begin Extent = 
               Top = 191
               Left = 700
               Bottom = 306
               Right = 852
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "payments"
            Begin Extent = 
               Top = 27
               Left = 485
               Bottom = 179
               Right = 670
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "paymentTypes"
            Begin Extent = 
               Top = 23
               Left = 703
               Bottom = 117
               Right = 855
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "account"
            Begin Extent = 
               Top = 34
               Left = 225
               Bottom = 149
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_AccountContractProject"
            Begin Extent = 
               Top = 31
               Left = 16
               Bottom = 146
               Right ', 'SCHEMA', N'dbo', 'VIEW', N'View_PaymentDetailsWithNames', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'= 193
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1860
         Width = 1425
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_PaymentDetailsWithNames', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'View_PaymentDetailsWithNames', NULL, NULL
GO
