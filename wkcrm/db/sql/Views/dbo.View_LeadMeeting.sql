
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.View_LeadMeeting
AS
SELECT     dbo.account.Name, dbo.account.home_phone, dbo.account.business_phone, dbo.account.mobile, dbo.account.mobile2, dbo.account.business_phone2, 
                      dbo.account.builders_name, dbo.account.address, dbo.account.suburb, dbo.account.postcode, dbo.leadMeetings.account_id, dbo.leadMeetings.datetime, 
                      dbo.leadMeetings.address AS MeetingAddress, dbo.leadMeetings.onsite, dbo.leadMeetings.notes, dbo.employee.name AS Designer, 
                      dbo.employee.mobile AS DesignerMobile, dbo.leadMeetings.id, dbo.employee.phone AS DesignerPhone, dbo.employee.email AS DesignerEmail, dbo.account.email, 
                      dbo.account.id AS LeadId, dbo.leadMeetings.location_id, dbo.location.name AS Location, dbo.location.address AS LocationAddress, 
                      dbo.location.phone AS LocationPhone
FROM         dbo.account INNER JOIN
                      dbo.leadMeetings ON dbo.account.id = dbo.leadMeetings.account_id INNER JOIN
                      dbo.employee ON dbo.leadMeetings.designer_id = dbo.employee.id LEFT OUTER JOIN
                      dbo.location ON dbo.leadMeetings.location_id = dbo.location.id
GO

EXEC sp_addextendedproperty N'MS_DiagramPane2', N'd
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_LeadMeeting', NULL, NULL
GO

EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "account"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 308
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "leadMeetings"
            Begin Extent = 
               Top = 19
               Left = 431
               Bottom = 215
               Right = 583
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "employee"
            Begin Extent = 
               Top = 35
               Left = 621
               Bottom = 260
               Right = 773
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "location"
            Begin Extent = 
               Top = 219
               Left = 248
               Bottom = 383
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 25
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   En', 'SCHEMA', N'dbo', 'VIEW', N'View_LeadMeeting', NULL, NULL
GO

EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'View_LeadMeeting', NULL, NULL
GO
