
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.View_AccountsProjectPhase
AS
SELECT     dbo.account.id, dbo.account.FirstName, dbo.account.LastName, dbo.account.Name, dbo.account.home_phone, dbo.account.business_phone, dbo.account.mobile, 
                      dbo.account.mobile2, dbo.account.business_phone2, dbo.account.builders_phone, dbo.account.postal_address, dbo.account.builders_name, dbo.account.designer_id, 
                      dbo.account.address, dbo.account.suburb, dbo.account.postcode, dbo.account.email, dbo.account.fax, dbo.account.account_type_id, dbo.account.account_status_id, 
                      dbo.account.has_been_revived, dbo.account.next_review_date, dbo.account.contact_source_id, dbo.account.opportunity_source_id, dbo.account.revived_reason, 
                      dbo.account.left_reason, dbo.account.date_created, dbo.account.weeks_open, dbo.account.door_type, dbo.account.benchtop_type, dbo.account.price_range, 
                      dbo.account.time_frame, dbo.account.sale_date, dbo.contractMain.id AS ContractNumber
FROM         dbo.account INNER JOIN
                      dbo.accountType ON dbo.account.account_type_id = dbo.accountType.id INNER JOIN
                      dbo.contractMain ON dbo.account.id = dbo.contractMain.account_id
WHERE     (dbo.accountType.id = 1005)
GO

EXEC sp_addextendedproperty N'MS_DiagramPane2', N'     Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_AccountsProjectPhase', NULL, NULL
GO

EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "account"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 321
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "accountType"
            Begin Extent = 
               Top = 176
               Left = 289
               Bottom = 417
               Right = 581
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "contractMain"
            Begin Extent = 
               Top = 4
               Left = 423
               Bottom = 179
               Right = 648
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 35
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
 ', 'SCHEMA', N'dbo', 'VIEW', N'View_AccountsProjectPhase', NULL, NULL
GO

EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'View_AccountsProjectPhase', NULL, NULL
GO
