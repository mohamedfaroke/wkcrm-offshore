SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.View_AccountContractProject
AS
SELECT     dbo.account.id AS AccID, dbo.contractMain.id AS ContractID, dbo.project.id AS ProjID, dbo.paymentSchedule.id AS PaymentScheduleID
FROM         dbo.account INNER JOIN
                      dbo.contractMain ON dbo.account.id = dbo.contractMain.account_id LEFT OUTER JOIN
                      dbo.paymentSchedule ON dbo.contractMain.id = dbo.paymentSchedule.contract_id LEFT OUTER JOIN
                      dbo.project ON dbo.contractMain.id = dbo.project.contract_id
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "account"
            Begin Extent = 
               Top = 55
               Left = 30
               Bottom = 170
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "contractMain"
            Begin Extent = 
               Top = 41
               Left = 280
               Bottom = 156
               Right = 479
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "project"
            Begin Extent = 
               Top = 180
               Left = 519
               Bottom = 295
               Right = 682
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "paymentSchedule"
            Begin Extent = 
               Top = 32
               Left = 559
               Bottom = 132
               Right = 711
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1875
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350', 'SCHEMA', N'dbo', 'VIEW', N'View_AccountContractProject', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_AccountContractProject', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'View_AccountContractProject', NULL, NULL
GO
