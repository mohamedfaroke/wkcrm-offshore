SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.View_TotalSalesLeadsByDesigner
AS
SELECT     dbo.View_TotalLeadsByDesigner.id, dbo.View_TotalLeadsByDesigner.Leads, CASE WHEN (dbo.View_TotalSalesByDesigner.Sales IS NULL) 
                      THEN 0 ELSE dbo.View_TotalSalesByDesigner.Sales END AS Sales, dbo.View_TotalLeadsByDesigner.Month, dbo.View_TotalLeadsByDesigner.Year, 
                      dbo.employee.name, dbo.MonthNames.monthName, dbo.MonthNames.monthShortName, dbo.employee.location_id, 
                      dbo.location.name AS LocationName
FROM         dbo.View_TotalLeadsByDesigner INNER JOIN
                      dbo.employee ON dbo.View_TotalLeadsByDesigner.id = dbo.employee.id INNER JOIN
                      dbo.MonthNames ON dbo.View_TotalLeadsByDesigner.Month = dbo.MonthNames.monthNum INNER JOIN
                      dbo.location ON dbo.employee.location_id = dbo.location.id LEFT OUTER JOIN
                      dbo.View_TotalSalesByDesigner ON dbo.View_TotalLeadsByDesigner.Year = dbo.View_TotalSalesByDesigner.Year AND 
                      dbo.View_TotalLeadsByDesigner.Month = dbo.View_TotalSalesByDesigner.Month AND 
                      dbo.View_TotalLeadsByDesigner.id = dbo.View_TotalSalesByDesigner.id
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "View_TotalLeadsByDesigner"
            Begin Extent = 
               Top = 19
               Left = 30
               Bottom = 134
               Right = 182
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "View_TotalSalesByDesigner"
            Begin Extent = 
               Top = 20
               Left = 259
               Bottom = 135
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "employee"
            Begin Extent = 
               Top = 201
               Left = 211
               Bottom = 316
               Right = 363
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MonthNames"
            Begin Extent = 
               Top = 151
               Left = 445
               Bottom = 251
               Right = 609
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "location"
            Begin Extent = 
               Top = 233
               Left = 439
               Bottom = 333
               Right = 591
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         C', 'SCHEMA', N'dbo', 'VIEW', N'View_TotalSalesLeadsByDesigner', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'olumn = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_TotalSalesLeadsByDesigner', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'View_TotalSalesLeadsByDesigner', NULL, NULL
GO
