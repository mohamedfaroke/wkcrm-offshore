SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.View_ContractReportBenchtops
AS
SELECT     dbo.contractBenchtopItem.id, dbo.contractBenchtopItem.contract_id, dbo.contractBenchtopItem.item_id, dbo.contractBenchtopItem.supplier, 
                      dbo.contractBenchtopItem.units, dbo.contractBenchtops.name, dbo.contractBenchtopOptionItem.id AS OptionId, dbo.contractBenchtopOptionItem.units AS OptionUnits, 
                      dbo.contractBenchtopOptionItem.contract_benchtop_option_item AS BenctopOptionId, dbo.contractBenchtopOptions.name AS BenctopOptionName, 
                      dbo.contractBenchtopItem.edge_profile, dbo.contractBenchtopItem.colour, dbo.contractBenchtopItem.finish, dbo.contractBenchtopItem.notes, 
                      dbo.contractBenchtopItem.edge_thickness
FROM         dbo.contractBenchtops INNER JOIN
                      dbo.contractBenchtopItem ON dbo.contractBenchtops.id = dbo.contractBenchtopItem.item_id LEFT OUTER JOIN
                      dbo.contractBenchtopOptions INNER JOIN
                      dbo.contractBenchtopOptionItem ON dbo.contractBenchtopOptions.id = dbo.contractBenchtopOptionItem.contract_benchtop_option_item ON 
                      dbo.contractBenchtopItem.id = dbo.contractBenchtopOptionItem.contract_benchtop_item
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "contractBenchtops"
            Begin Extent = 
               Top = 187
               Left = 227
               Bottom = 302
               Right = 438
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "contractBenchtopItem"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 315
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "contractBenchtopOptions"
            Begin Extent = 
               Top = 34
               Left = 608
               Bottom = 188
               Right = 805
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "contractBenchtopOptionItem"
            Begin Extent = 
               Top = 14
               Left = 288
               Bottom = 184
               Right = 544
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy', 'SCHEMA', N'dbo', 'VIEW', N'View_ContractReportBenchtops', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_ContractReportBenchtops', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'View_ContractReportBenchtops', NULL, NULL
GO
