SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.View_ProjectScheduleSummary
AS
SELECT     dbo.tasks.id AS TaskId, dbo.tasks.project_id AS id, dbo.tasks.tradesman_id, dbo.tasks.task_type_id, dbo.tasks.notes, dbo.tasks.datetime, dbo.tasks.datetime_end, 
                      dbo.tasks.acknowledged, dbo.tradesmen.name AS TradesmanName, dbo.tradeTaskTypes.name AS TaskType, dbo.project.project_manager, 
                      dbo.employee.name AS ProjectManagerName, dbo.project.contract_id, dbo.contractMain.account_id, dbo.account.Name AS AccountName
FROM         dbo.tasks INNER JOIN
                      dbo.tradeTaskTypes ON dbo.tasks.task_type_id = dbo.tradeTaskTypes.id INNER JOIN
                      dbo.tradesmen ON dbo.tasks.tradesman_id = dbo.tradesmen.id INNER JOIN
                      dbo.project ON dbo.tasks.project_id = dbo.project.id INNER JOIN
                      dbo.employee ON dbo.project.project_manager = dbo.employee.id INNER JOIN
                      dbo.contractMain ON dbo.project.contract_id = dbo.contractMain.id INNER JOIN
                      dbo.account ON dbo.contractMain.account_id = dbo.account.id
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -465
      End
      Begin Tables = 
         Begin Table = "tasks"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 209
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tradeTaskTypes"
            Begin Extent = 
               Top = 191
               Left = 227
               Bottom = 295
               Right = 387
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tradesmen"
            Begin Extent = 
               Top = 8
               Left = 225
               Bottom = 127
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "project"
            Begin Extent = 
               Top = 6
               Left = 485
               Bottom = 175
               Right = 656
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "employee"
            Begin Extent = 
               Top = 172
               Left = 685
               Bottom = 291
               Right = 845
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "contractMain"
            Begin Extent = 
               Top = 22
               Left = 697
               Bottom = 162
               Right = 922
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "account"
            Begin Extent = 
               Top = 62
               Left = 983
               Bottom = 181
               Right = 1180
            End
            Disp', 'SCHEMA', N'dbo', 'VIEW', N'View_ProjectScheduleSummary', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'layFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_ProjectScheduleSummary', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'View_ProjectScheduleSummary', NULL, NULL
GO
