IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'wkuser')
CREATE LOGIN [wkuser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [wkuser] FOR LOGIN [wkuser] WITH DEFAULT_SCHEMA=[dbo]
GO
GRANT CONNECT TO [wkuser]
