SET IDENTITY_INSERT [dbo].[accountType] ON
INSERT INTO [dbo].[accountType] ([id], [name], [status_id]) VALUES (1000, N'Lead Phase', 1)
INSERT INTO [dbo].[accountType] ([id], [name], [status_id]) VALUES (1005, N'Project Phase', 1)
SET IDENTITY_INSERT [dbo].[accountType] OFF
