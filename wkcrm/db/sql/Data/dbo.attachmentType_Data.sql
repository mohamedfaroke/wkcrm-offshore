SET IDENTITY_INSERT [dbo].[attachmentType] ON
INSERT INTO [dbo].[attachmentType] ([id], [name], [status_id]) VALUES (1000, N'Floor Plans & Elevations', 1)
INSERT INTO [dbo].[attachmentType] ([id], [name], [status_id]) VALUES (1005, N'Other', 1)
SET IDENTITY_INSERT [dbo].[attachmentType] OFF
