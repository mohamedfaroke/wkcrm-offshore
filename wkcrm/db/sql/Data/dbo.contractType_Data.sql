SET IDENTITY_INSERT [dbo].[contractType] ON
INSERT INTO [dbo].[contractType] ([id], [name], [status_id]) VALUES (1000, N'Quote Phase', 1)
INSERT INTO [dbo].[contractType] ([id], [name], [status_id]) VALUES (1005, N'Project Phase', 1)
SET IDENTITY_INSERT [dbo].[contractType] OFF
