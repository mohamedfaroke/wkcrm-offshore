IF %1 == "" GOTO HELP
IF %2 == "" GOTO HELP
IF %3 == "" GOTO HELP

IF %1 == %2 GOTO HELP1

:STEP1
ECHO.
ECHO.
echo ****************************************************
echo ***       DEPLOYING A CLIENT ASP PROJECT         ***
echo ***                                              ***
echo *** Your source directory is                     ***
echo ***       %1                                     
echo ***                                              ***
echo *** Your final directory is                      ***
echo ***       %2                                     
echo ***                                              ***
echo *** WEB APPLICATION NAME IS                      ***
echo ***       %3                                     
echo ***                                              ***
echo *** Your Temporary directory is                  ***
echo ***       "%temp%\%3"                                    
echo ***                                              ***
echo ****************************************************
echo Step 1/4
echo Executing command
echo RMDIR /S /Q "%temp%\%3"
RMDIR /S /Q "%temp%\%3"

if errorlevel 4 goto lowmemory 
if errorlevel 2 goto abort 
if errorlevel 0 goto STEP2
goto STEP2


:STEP2
ECHO.
ECHO.
echo ****************************************************
echo ***            ASP PRECOMPILATION                ***
echo ***                                              ***
echo *** Your source directory From                   ***
echo ***       %1
echo ***                   TO                         ***
echo ***                                              ***
echo ***  Your temporary Directory                    ***
echo ***       %2                                     
echo ****************************************************
Echo Step 2/4 
echo Executing command
echo C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\aspnet_compiler -p %1 -v /%3 -u -fixednames "%temp%\%3" echo CHECK compiler.output.txt

C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\aspnet_compiler -p %1 -v /%3 -u -fixednames "%temp%\%3" >compiler.output.txt

if errorlevel 5 goto diskerror 
if errorlevel 4 goto lowmemory 
if errorlevel 2 goto abort 
if errorlevel 1 goto notfound 
if errorlevel 0 goto STEP3


:STEP3
ECHO.
ECHO.
echo ****************************************************
echo ***            ASP MERGE INTO ONE DLL            ***
echo ***                                              ***
echo *** Your source directory From                   ***
echo ***       %1                                     
echo *** DLL NAME                                     ***
echo ***       %2                                     
echo ***                                              ***
echo ****************************************************
Echo Step 3/4
echo Executing command
echo "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0a\Bin\x64\aspnet_merge.exe" "%temp%\%3" -o %3.dll -copyattrs   

"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0a\Bin\x64\aspnet_merge.exe" "%temp%\%3" -o %3.dll  -copyattrs -a

if errorlevel 5 goto diskerror 
if errorlevel 4 goto lowmemory 
if errorlevel 2 goto abort 
if errorlevel 1 goto notfound 
if errorlevel 0 goto STEP4


:STEP4
ECHO.
ECHO.
Echo Cleaning temporary location %2
RMDIR /S /Q %2
ECHO.
ECHO.
echo ****************************************************
echo ***            You are about to copy             ***
echo ***                                              ***
echo *** Your Development Application Directory From  ***
echo ***       %1
echo ***                   TO                         ***
echo ***                                              ***
echo ***  Your Deployment Application Directory       ***
echo ***       %2                                     
echo ***                                              ***
echo ****************************************************
Echo Step 4/4
echo Executing commands
echo    xcopy "%temp%\%3" %2 /I /S /R /Y /Q /EXCLUDE:IS_EXCLUDE_Some.TXT
xcopy "%temp%\%3" %2 /I /S /R /Y  /Q /EXCLUDE:IS_EXCLUDE_Some.TXT

ECHO.
echo    xcopy %1\Shared\Menu.xml %2\Shared\ /I /S /R /Y
xcopy %1\Shared\Menu.xml %2\Shared\ /I /S /R /Y

ECHO.
echo    xcopy %1\Shared\Mailouts\EditorTools.xml %2\Shared\Mailouts\ /I /S /R /Y
xcopy %1\Shared\Mailouts\EditorTools.xml %2\Shared\Mailouts\ /I /S /R /Y

ECHO.
echo xcopy %1\Shared\Admin\AdminTree.xml %2\Shared\Admin\ /I /S /R /Y
xcopy %1\Shared\Admin\AdminTree.xml %2\Shared\Admin\ /I /S /R /Y

ECHO.
echo xcopy %1\Styles\ %2\Styles\ /I /S /R /Y /Q /EXCLUDE:VSS_EXCLUDE.txt
xcopy %1\Styles\*.* %2\Styles\ /I /S /R /Y /Q /EXCLUDE:VSS_EXCLUDE.txt

ECHO.
ECHO.
echo ****************************************************
echo ***          COPYING XML REVISION DOCS           ***
echo ****************************************************

ECHO.
echo    xcopy M:\Impowa\Dev\bin\Release\*.xml %2\Bin\ /I /S /R /Y 
xcopy M:\Impowa\Dev\bin\Release\*.xml %2\Bin\ /I /S /R /Y 

if errorlevel 0 goto FINISHED

ECHO ****************************************************
ECHO *    _   _                                         *
ECHO *   (_)_(_)                                        *
ECHO *    (o o)    ------ ERRORS OCCURED   --------     *
ECHO *   ==\o/==                                        *
ECHO *                                                  *
ECHO ****************************************************
if errorlevel 5 goto diskerror 
if errorlevel 4 goto lowmemory 
if errorlevel 2 goto abort 
if errorlevel 1 goto notfound 

:FINISHED
ECHO.  
ECHO. 
ECHO           BUILDING STAGE COMPLETE OK .....
ECHO. 
ECHO.
ECHO.
goto end

:notfound
echo File NOT FOUND.
goto end 
:diskerror 
echo Disk Write Error Occured.
goto end 
:lowmemory 
echo One of the following errors Occured.
echo a.  Initialization Error.
echo b.  Not Enough Disk Space.
echo c.  Insufficient Memory Available.
echo d.  Invalid Drive Name.
echo e.  Invalid Syntax Was Used.
goto end 
:abort 
echo You pressed CTRL+C to end the copy operation. 
goto end


:HELP
cls
echo **********************************************************************
echo *                 HELP SCREEN
echo *                                                                
echo *  Check the number of inputs
echo *                                                                
echo **********************************************************************
goto end


:HELP1
cls
echo ***********************************************************************
echo ***                 HELP SCREEN                                     ***
echo ***                                                                 ***  
echo ***       Your "source Application directory" is named              ***
echo ***       the same as your "Deployment Application directory"       ***
echo ***                                                                 ***
echo ***********************************************************************
goto end
 
 
:CHECKSOURCEFILES
echo *
echo *
echo -------       
echo Please that all DLL's have been compiled and checked into the source control!
echo -------


:END

